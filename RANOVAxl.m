%% repeated measures ANOVA on Excel data
% copy and paste values for each phase and rat into variable 'xl' with rats
% as rows and phases as columns, in the specified order:

phases = {'Habituation','Acquisition','Recall','Extinction'};
ratnums = {'216';'221';'254';'312';'219';'220';'258';...
    '311';'313'};
groups = {'Saline';'Saline';'Saline';'Saline';...
    'D1 Antagonist';'D1 Antagonist';'D1 Antagonist';...
    'D1 Antagonist';'D1 Antagonist'};

t = table(groups,xl(:,1),xl(:,2),xl(:,3),xl(:,4),'VariableNames',...
    [{'DrugGroup'},phases],'RowNames',ratnums);

rm = fitrm(t,[phases{1} '-' phases{4} '~DrugGroup']);

tbl1 = mauchly(rm);

if tbl1.pValue < 0.05
    disp('sphericity assumption does not hold');
    tbl2 = epsilon(rm);
else
    disp('sphericity assumption holds');
end

ranovatbl = ranova(rm);
disp(ranovatbl);

if any(any(table2array(ranovatbl(1:2,5:8)) < 0.05))
    
    tbl3 = multcompare(rm,'DrugGroup','By','Time');
    for row = 1:size(tbl3,1)
        if tbl3.pValue(row) < 0.05 && tbl3.Difference(row) > 0
            disp(['During ' phases{tbl3.Time(row)} ', the ' ...
                tbl3.DrugGroup_1{row} ...
                ' group had significantly higher values than the ' ...
                tbl3.DrugGroup_2{row} ' group (p = ' ...
                num2str(tbl3.pValue(row)) ').']);
        end
    end
    
    tbl4 = multcompare(rm,'Time','By','DrugGroup');
    for row = 1:size(tbl4,1)
        if tbl4.pValue(row) < 0.05 && tbl4.Difference(row) > 0
            disp(['The ' tbl4.DrugGroup{row} ...
                ' group had significantly higher values during '  ...
                phases{tbl4.Time_1(row)} ' than during ' ...
                phases{tbl4.Time_2(row)} ' (p = ' ...
                num2str(tbl4.pValue(row)) ').']);
        end
    end
    
else
    disp('No significant effects found');
end
