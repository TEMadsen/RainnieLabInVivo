function [ tdir ] = tserver()
%TSERVER Returns a string representing the address of Teresa's folder on
% the shared Rainnie Lab server, as appropriate to a Mac or PC.
% The S drive should be mapped to \\yrknas.yerkes.emory.edu\yerkes2\drlab

if ispc
    tdir = 'S:\Teresa\';
elseif ismac
    tdir = '/Volumes/yerkes2/DRLab/Teresa/';
else
  error('operating system not supported')
end

end

