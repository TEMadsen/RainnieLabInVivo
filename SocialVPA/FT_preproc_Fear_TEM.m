%% Load all data from -aligned.nex files into FieldTrip format & merge
% into 1 dataset per rat, using new trialfun_TEM.m and supporting a cell
% array of strings for multiple filenames within one phase (to avoid merged
% NEX files, which are hard to read correctly).

info_fear   % load all background info (filenames, etc)
%#ok<*SAGROW> many variables seem to grow when they're preallocated here

%% full loop

for r = 1:size(ratT,1)
  % Raw LFP data divided into long, non-overlapping tone-triggered
  % trials, merged across all recordings for each rat
  outputfile{2} = [datafolder 'RawLFPTrialData_' ...
    int2str(ratT.ratnums(r)) 'Merged.mat'];
  
  if exist(outputfile{2},'file')
    disp([outputfile{2} ' already exists.  Skipping rat.'])
  else
    data2merge = cell(1);
    next = 1;   % put 1st recording's data in 1st cell
    for p = 1:numel(phases)
      if ~isempty(nexfile{r,p})   % in case of missing files
        if ~iscell(nexfile{r,p})  % in case of multiple files per phase
          nexfile{r,p} = nexfile(r,p);  % convert to cellstr
        end
        for fn = 1:numel(nexfile{r,p})  % file number w/in a phase
          %% define output filenames, etc.
          
          % Raw LFP data divided into long, non-overlapping tone-triggered
          % trials, only NaNs removed
          outputfile{1} = [datafolder 'RawLFPTrialData_' ...
            int2str(ratT.ratnums(r)) phases{p} num2str(fn) '.mat'];
          
          %% load file if it exists
          
          if ~exist(outputfile{1},'file')
            disp('starting from scratch');
            bestfile = 0;
            clearvars data
          else
            disp(['loading file ' outputfile{1}]);
            load(outputfile{1});
            bestfile = 1;
          end
          
          %% no processing done yet
          if bestfile == 0
            if ~exist(nexfile{r,p}{fn},'file')
              error(['Input file ' nexfile{r,p}{fn} ' does not exist.']);
            end
            
            %% define long tone-triggered trials that cover whole recording
            
            cfg                         = [];
            cfg.dataset                 = nexfile{r,p}{fn};
            cfg.trialfun                = 'trialfun_TEM';
            
            if isempty(toneTS{r,p})
              cfg.trialdef.eventtype    = 'Event006'; % {'Event007','Event008'};
            else
              cfg.trialdef.eventtype    = 'manual';
              cfg.trialdef.eventTS      = toneTS{r,p};
            end
            
            cfg.trialdef.minsep         = 60;   % all in seconds
            cfg.trialdef.prestim        = 60;
            cfg.trialdef.poststim       = 90;
            cfg.trialdef.expandtrials   = true;
            
            cfg = ft_definetrial(cfg);
            
            %% load continuous LFP data, all channels
            
            cfg.channel     = 'AD*';
            
            data = ft_preprocessing(cfg);
            
            %% add elec positions
            
            elecfile = [configfolder 'elec_' MWA '_' ratT.side{r} '.mat'];
            load(elecfile);
            data.elec = elec;
            
            %% mark NaNs as artifacts
            
            artifact.nan = [];
            
            for tr = 1:numel(data.trial)
              % identify whether there are any NaNs at each timepoint
              tnan = any(isnan(data.trial{tr}));
              
              if any(tnan)
                % determine the file sample #s for this trial
                trsamp = data.sampleinfo(tr,1):data.sampleinfo(tr,2);
                
                while any(tnan)
                  % start from the end so sample #s don't shift
                  endnan = find(tnan,1,'last');
                  tnan = tnan(1:endnan);  % remove any non-NaNs after this
                  
                  % find last non-NaN before the NaNs
                  beforenan = find(~tnan,1,'last');
                  
                  if isempty(beforenan)   % if no more non-NaNs
                    begnan = 1;
                    tnan = false;   % while loop ends
                  else  % still more to remove - while loop continues
                    begnan = beforenan + 1;
                    tnan = tnan(1:beforenan);  % remove the identified NaNs
                  end
                  
                  % identify file sample #s that correspond to beginning
                  % and end of this chunk of NaNs and append to
                  % artifact.nan
                  artifact.nan = [artifact.nan; ...
                    trsamp(begnan) trsamp(endnan)];
                end   % while any(tnan)
              end   % if any(tnan)
            end   % for tr = 1:numel(data.trial)
            
            %% remove NaNs from data & save 1st FT file
            
            cfg                         = [];
            cfg.artfctdef.nan.artifact  = artifact.nan;
            cfg.artfctdef.reject        = 'partial';
            cfg.outputfile              = outputfile{1};
            
            data = ft_rejectartifact(cfg,data);
            
            bestfile = 1;
          end
          
          %% tone-triggered trials saved
          if bestfile == 1
            if ~all(data.trialinfo(:,end) == p)
              data.trialinfo(:,end+1) = p;  % add the phase #
            end
            if fn > 1   % if this isn't the first file in this phase
              if p == data2merge{end}.trialinfo(end,end) % and trials were included from the previous file
                data.trialinfo(:,end-1) = data.trialinfo(:,end-1) + ...
                  data2merge{end}.trialinfo(end,end-1);   % adjust trial #s
              end
            end
            data2merge{next} = data;
            next = next + 1;  % next recording will be saved in next cell
            clearvars data
          end
        end
      end
    end
    
    %% merge the data from all recordings
    
    cfg             = [];
    cfg.outputfile  = outputfile{2};
    
    if isempty(data2merge{1})
      warning(['No data present for rat #' int2str(ratT.ratnums(r)) ...
        '.  No merge performed.']);
    elseif numel(data2merge) == 1
      warning(['Only 1 phase of data present for rat #' ...
        int2str(ratT.ratnums(r)) '.  No merge performed.']);
    else
      while numel(data2merge) > 1
        data2merge{2} = ft_appenddata(cfg, data2merge{1}, data2merge{2});
        data2merge = data2merge(2:end);
      end
    end
    
    clearvars data2merge
  end
end
