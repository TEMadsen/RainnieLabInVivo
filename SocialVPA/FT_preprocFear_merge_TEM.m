%% Load all data into FieldTrip format & merge into 1 dataset per rat

info_fear                   % load all background info (filenames, etc)

%% full loop

for r = 1:size(ratT,1)
  % Raw LFP data divided into long, non-overlapping tone-triggered
  % trials, merged across all recordings for each rat
  outputfile{3} = [datafolder 'RawLFPTrialData_' ...
    int2str(ratT.ratnums(r)) 'Merged.mat'];
  
  if ~exist(outputfile{3},'file')
    data2merge = cell(1);
    next = 1;   % put 1st recording's data in 1st cell
    for p = 1:numel(phases)
      if ~isempty(nexfile{r,p})   % in case of missing files
        if ~iscell(nexfile{r,p})  % in case of multiple files per phase
          nexfile{r,p} = nexfile(r,p); %#ok<SAGROW> defined in info_fear
        end
        for fn = 1:numel(nexfile{r,p})
          %% define output filenames, etc.
          
          % Raw continuous LFP data with t=0 at 1st tone onset, only NaNs
          % removed
          outputfile{1} = [datafolder 'RawLFPContData_' ...
            int2str(ratT.ratnums(r)) phases{p} num2str(fn) '.mat'];
          
          % Raw LFP data divided into long, non-overlapping tone-triggered
          % trials
          outputfile{2} = [datafolder 'RawLFPTrialData_' ...
            int2str(ratT.ratnums(r)) phases{p} num2str(fn) '.mat'];
          
          %% check whether files exist
          
          filecorrect = false(size(outputfile));
          
          for file = 1:2
            if exist(outputfile{file},'file')
              filecorrect(file) = true;
            end
          end
          
          %% keep loading last file and checking for correctness
          
          bestfile = numel(outputfile)+1;
          
          while bestfile > numel(outputfile)
            if ~any(filecorrect)
              disp('starting from scratch');
              bestfile = 0;
              % make sure there's no old data or artifact definitions
              clearvars data artifact
              break   % jump past while loop
            end
            
            %% load and check data
            
            lastfile = find(filecorrect,1,'last');
            disp(['loading file ' outputfile{lastfile}]);
            load(outputfile{lastfile});
            
            % check for electrode locations
            if isfield(data, 'elec') && isfield(data.elec, 'tra')
              disp([outputfile{lastfile} ' already contains data.elec'])
            else  % add electrode locations
              elecfile = [configfolder 'elec_' MWA '_' ratT.side{r} '.mat'];
              load(elecfile);
              data.elec = elec;
              disp([elecfile ' added as data.elec'])
              s = whos('data');
              if (s.bytes < 500000000)  % if variable < ~500 MB,
                % store it in old (uncompressed) format, which is faster
                save(outputfile{lastfile}, 'data', '-v6');
              else  % larger variables must be stored in new compressed format
                save(outputfile{lastfile}, 'data', '-v7.3');
              end
              disp([outputfile{lastfile} ' resaved'])
            end
            
            if data.time{1}(1) >= 0
              warning('no pre-tone baseline found');
              filecorrect(lastfile) = false;
              continue  % returns to next iteration of while loop
            end
            
            if filecorrect(lastfile)
              disp(['continuing from file ' outputfile{lastfile}]);
              bestfile = lastfile;
              break  % jumps past while loop
            end
          end
          
          %% no processing done yet
          if bestfile == 0
            if ~exist(nexfile{r,p}{fn},'file')
              error(['Input file ' nexfile{r,p}{fn} ' does not exist.']);
            end
            
            %% define single trial of continuous data with offset to 1st tone
            
            cfg                         = [];
            cfg.dataset                 = nexfile{r,p}{fn};
            cfg.trialfun                = 'my_trialfun';
            
            if isempty(toneTS{r,p})
              cfg.trialdef.eventtype    = 'Event006'; % {'Event007','Event008'};
            else
              cfg.trialdef.eventtype    = 'manual';
              cfg.trialdef.eventTS      = toneTS{r,p};
            end
            
            cfg.trialdef.prestim        = Inf;
            cfg.trialdef.poststim       = Inf;
            
            cfg = ft_definetrial(cfg);
            ctrl = cfg.trl;
            
            %% load continuous LFP data, all channels
            
            cfg.channel     = 'AD*';
            
            data = ft_preprocessing(cfg);
            
            %% add elec positions
            
            elecfile = [configfolder 'elec_' MWA '_' ratT.side{r} '.mat'];
            load(elecfile);
            data.elec = elec;
            
            %% mark NaNs as artifacts
            
            cfg                         = [];
            cfg.artfctdef.nan.channel   = 'AD*';
            
            [~, artifact.nan] = artifact_nan_TEM(cfg,data);
            
            %% remove NaNs from data
            
            cfg                         = [];
            cfg.artfctdef.nan.artifact  = artifact.nan;
            cfg.artfctdef.reject        = 'partial';
            cfg.outputfile              = outputfile{1};
            
            data = ft_rejectartifact(cfg,data);
            
            bestfile = 1;
          end
          
          %% raw data loaded
          if bestfile == 1
            %% mark NaNs as artifacts
            
            cfg                         = [];
            cfg.artfctdef.nan.channel   = 'AD*';
            
            [~, artifact.nan] = artifact_nan_TEM(cfg,data);
            
            if ~isempty(artifact.nan)
              %% remove NaNs from data
              
              cfg                         = [];
              cfg.artfctdef.nan.artifact  = artifact.nan;
              cfg.artfctdef.reject        = 'partial';
              cfg.outputfile              = outputfile{1};
              
              data = ft_rejectartifact(cfg,data);
            end
            
            otrl = data.cfg.trl;   % original trl structure
            
            %% define tone-triggered trials
            
            cfg                         = [];
            cfg.dataset                 = nexfile{r,p};
            cfg.trialfun                = 'my_trialfun';
            
            if isempty(toneTS{r,p})
              cfg.trialdef.eventtype    = 'Event006';
            else
              cfg.trialdef.eventtype    = 'manual';
              cfg.trialdef.eventTS      = toneTS{r,p};
            end
            
            cfg.trialdef.prestim        = 60;   % in seconds
            cfg.trialdef.poststim       = 90;   % in seconds
            
            cfg = ft_definetrial(cfg);
            ttrl = cfg.trl;                     % tone-triggered trials
            
            %% adjust to long trials that cover whole recording
            
            % adjusts beginning of 1st trial to beginning of continuous data
            ttrl(1,1) = otrl(1,1);
            ttrl(1,3) = otrl(1,3);  % corrects offset to 1st tone
            
            for tr = 1:(size(ttrl,1)-1)
              ttrl(tr,2) = ttrl(tr+1,1) - 1;
            end
            
            % adjusts end of last trial to end of continous data
            ttrl(end,2) = otrl(end,2);
            
%             if r == 1 && p == 1   % headstage came unplugged after 2nd shock
%               ttrl(12,2) = ttrl(12,1) + 90000;    % [-60 30]
%               ttrl(13,1) = ttrl(13,1) - 178000;   % [-238 90]
%               ttrl(13,3) = ttrl(13,3) - 178000;   % adjust offset by same amt
%             end
            
            %% arrange continuous raw data into long, tone-triggered trials
            
            cfg             = [];
            cfg.trl         = ttrl;
            cfg.outputfile  = outputfile{2};
            
            data = ft_redefinetrial(cfg, data);
            
            bestfile = 2;
          end
          
          %% tone-triggered trials saved
          if bestfile == 2
            data2merge{next} = data;
            next = next + 1;  % next recording will be saved in next cell
            clearvars data
          end
        end
      end
    end
    
    %% merge the data from all recordings
    
    for fn = 1:numel(data2merge)
      % cut off all trials at max time [-600 630]
        cfg = [];
        cfg.toilim    = [-600 630];
        
        data2merge{fn} = ft_redefinetrial(cfg, data2merge{fn});
        
        if ~all(data2merge{fn}.trialinfo(:,end) == p)
          data2merge{fn}.trialinfo(:,end+1) = p;
        end
    end
    
    cfg             = [];
    cfg.outputfile  = outputfile{3};
    
    if isempty(data2merge{1})
      warning(['No data present for rat #' int2str(ratT.ratnums(r)) ...
        '.  No merge performed.']);
    elseif numel(data2merge) == 1
      warning(['Only 1 phase of data present for rat #' ...
        int2str(ratT.ratnums(r)) '.  No merge performed.']);
    else
      while numel(data2merge) > 1
        data2merge{2} = ft_appenddata(cfg, data2merge{1}, data2merge{2});
        data2merge = data2merge(2:end);
      end
    end
    
    clearvars data2merge
  end
end
