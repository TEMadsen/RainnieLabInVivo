function MagEtyDiff = MagEtyDiffCal( MagEtyBef, MagEtyDur )
% Calculate the difference of magazine entries for reinforcement conditioning
%   Magazine Enrty= ME
%   MagEtyDiff difined as MEdur-MRbef
MagEtyDiff = NaN;
    MagEtyDiff = MagEtyDur - MagEtyBef;
end