%% Load all data into FieldTrip format & mark artifacts

close all; clearvars; clc;  % clean slate
info_fear                   % load all background info (filenames, etc)

%% full loop

for r = 1:size(ratT,1)
  for p = 1:numel(phases)
    if ~isempty(nexfile{r,p})
      %% define output filenames, etc.
      
      % Raw continuous LFP data with t=0 at 1st tone onset, only NaNs
      % removed
      outputfile{1} = [datafolder 'RawLFPContData_'...
        int2str(ratT.ratnums(r)) phases{p} '.mat'];
      
      % Raw LFP data divided into long, non-overlapping tone-triggered
      % trials
      outputfile{2} = [datafolder 'RawLFPTrialData_'...
        int2str(ratT.ratnums(r)) phases{p} '.mat'];
      
%       % Clean LFP data, broken into arbitrary "trials" to remove chunks of
%       % time containing artifacts (NaNs, low freq, theta freq, and large),
%       % and filtered to remove high frequency chewing artifacts
%       outputfile{3} = [datafolder 'CleanLFPContData_'...
%         int2str(ratT.ratnums(r)) phases{p} '.mat'];
%       artfile = [datafolder 'Artifacts_'...
%         int2str(ratT.ratnums(r)) phases{p} '.mat'];
%       
%       % Clean LFP data in 3s chunks, for behavioral correlation
%       outputfile{4} = [datafolder 'CleanLFP3sChunkData_'...
%         int2str(ratT.ratnums(r)) phases{p} '.mat'];
%       
%       % Tone-triggered trials with artifacts removed (except for fake tone
%       % on/off artifacts).  Best for spectrograms & coherograms.
%       outputfile{5} = [datafolder 'CleanLFPTrialData_'...
%         int2str(ratT.ratnums(r)) phases{p} '.mat'];
%       
%       % Artifact-free 3-second subtrials marked with data.trialinfo(:,1) =
%       % original trial number, and data.trialinfo(:,2) = 1, 2, or 3 for
%       % before, during, or after tones.  Best for spectra, coherence, and
%       % statistics.
%       outputfile{6} = [datafolder 'CleanLFP3sSubtrialData_'...
%         int2str(ratT.ratnums(r)) phases{p} '.mat'];
%       
      %% check whether files exist
      
      filecorrect = false(size(outputfile));
      
      for file = 1:numel(outputfile)
        if exist(outputfile{file},'file')
          filecorrect(file) = true;
        end
      end
      
      %% keep loading last file and checking for correctness
      
      bestfile = numel(outputfile)+1;
      
      while bestfile > numel(outputfile)
        if ~any(filecorrect)
          disp('starting from scratch');
          bestfile = 0;
          % make sure there's no old data or artifact definitions
          clearvars data artifact
          break   % jump past while loop
        end
        
        %% load and check data
        
        lastfile = find(filecorrect,1,'last');
        disp(['loading file ' outputfile{lastfile}]);
        load(outputfile{lastfile});
        
        % check for electrode locations
        if isfield(data, 'elec') && isfield(data.elec, 'tra')   
          disp([outputfile{lastfile} ' already contains data.elec'])
        else  % add electrode locations
          elecfile = [configfolder 'elec_' MWA '_' ratT.side{r} '.mat'];
          load(elecfile);
          data.elec = elec;
          disp([elecfile ' added as data.elec'])
          s = whos('data');
          if (s.bytes < 500000000)  % if variable < ~500 MB,
                % store it in old (uncompressed) format, which is faster
            save(outputfile{lastfile}, 'data', '-v6');
          else  % larger variables must be stored in new compressed format
            save(outputfile{lastfile}, 'data', '-v7.3');
          end
          disp([outputfile{lastfile} ' resaved'])
        end
        
        if data.time{1}(1) >= 0
          warning('no pre-tone baseline found');
          filecorrect(lastfile) = false;
          continue  % returns to next iteration of while loop
        end
        
        if lastfile > 2   % all channels used for outputfile{1} & {2}
          if strcmp(goodchs{r}{1},'AD*')
            if numel(data.label) ~= (numel(lftchs) + numel(rtchs) + 1 ...
                - numel(goodchs{r}))
              warning('different channels used than planned');
              filecorrect(lastfile) = false;
              continue  % returns to next iteration of while loop
            else
              for ch = 1:numel(data.label)
                k = strfind(goodchs{r}, data.label{ch});
                if any([k{:}])
                  warning('channels used that should be excluded');
                  filecorrect(lastfile) = false;
                  break  % jumps to end of for ch loop
                else
                  disp([data.label{ch} ' is not excluded']);
                end
              end
            end
          elseif numel(data.label) ~= numel(goodchs{r}) || ...
              any(any(~strcmp(data.label,goodchs{r})))
            warning('different channels used than planned');
            filecorrect(lastfile) = false;
            continue  % returns to next iteration of while loop
          else
            disp([data.label{:} ' are correct']);
          end
        end
        
        if filecorrect(lastfile)
          disp(['continuing from file ' outputfile{lastfile}]);
          bestfile = lastfile;
          break  % jumps past while loop
        end
      end
      
%       %       %% comment this out! - just to use databrowser to display fully processed data
%       %       if bestfile == 5
%       %         bestfile = 1;
%       %         disp(['loading file ' outputfile{bestfile}]);
%       %         load(outputfile{bestfile});
%       %       else
%       %         clearvars data cfg out* art* trl
%       %         break   % jumps past for p loop (to next rat)
%       %       end
%       
%       %% check for saved artifact definitions
%       if bestfile < 6   % last file not processed correctly
%         if exist(artfile,'file')
%           disp(['Artifacts for Rat # ' int2str(ratT.ratnums(r)) ...
%             ', ' phases{p} ' already identified; ' ...
%             'using saved artifact definition']);
%           load(artfile);
%           if bestfile > 5 && ~isfield(artifact,'toneOnOff')
%             bestfile = 5; % redo this step
%           end
%         elseif bestfile > 2
%           warning('Artifact definitions not saved! Starting over.');
%           bestfile = 1;
%         end
%         
%         %% make sure correct data is in memory
%         if bestfile == 4                  % this data saved to file, but
%           disp(['loading file ' outputfile{3}]);
%           load(outputfile{3});              % this data continues in memory
%         elseif bestfile == 2              % this data saved to file, but
%           disp(['loading file ' outputfile{1}]);
%           load(outputfile{1});              % this data continues in memory
%         elseif bestfile == 0
%           %         clearvars artifact % make sure there's no old artifact definition
%         else  % bestfile = 1, 3, or 5
%           disp(['loading file ' outputfile{bestfile}]);
%           load(outputfile{bestfile});
%         end
%       end
%       
%       %% no processing done yet
%       if bestfile == 0
%         if ~exist(nexfile{r,p},'file')
%           error(['Input file ' nexfile{r,p} ' does not exist.']);
%         end
%         
%         %% define single trial of continuous data with offset to 1st tone
%         
%         cfg                         = [];
%         cfg.dataset                 = nexfile{r,p};
%         cfg.trialfun                = 'my_trialfun';
%         
%         if isempty(toneTS{r,p})
%           cfg.trialdef.eventtype    = 'Event006'; % {'Event007','Event008'};
%         else
%           cfg.trialdef.eventtype    = 'manual';
%           cfg.trialdef.eventTS      = toneTS{r,p};
%         end
%         
%         cfg.trialdef.prestim        = Inf;
%         cfg.trialdef.poststim       = Inf;
%         
%         cfg = ft_definetrial(cfg);
%         ctrl = cfg.trl;
%         
%         %% load continuous LFP data, all channels
%         
%         cfg.channel     = 'AD*';
%         
%         data = ft_preprocessing(cfg);
%         
%         % BUG IN ft_read_data (line 1099) CHANGE to use offsets given by
%         % NEX file instead of rounding from timestamps, which may not be
%         % accurate if any samples are lost between file fragments:
%         %
%         % offset     = nex.indx;
%         %
%         % follow bug resolution here:
%         % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=1825
%         %
%         % TO DO: check how this change impacts timestamps, as nex.ts data
%         % is now ignored
%         
%         %% mark NaNs as artifacts
%         
%         cfg                         = [];
%         cfg.artfctdef.nan.channel   = 'AD*';
%         
%         [~, artifact.nan] = artifact_nan_TEM(cfg,data);
%         
%         %% remove NaNs from data
%         
%         cfg                         = [];
%         cfg.artfctdef.nan.artifact  = artifact.nan;
%         cfg.artfctdef.reject        = 'partial';
%         cfg.outputfile              = outputfile{1};
%         
%         data = ft_rejectartifact(cfg,data);
%         
%         bestfile = 1;
%       end
%       
%       %% raw data loaded
%       if bestfile == 1
%         %% mark NaNs as artifacts
%         
%         cfg                         = [];
%         cfg.artfctdef.nan.channel   = 'AD*';
%         
%         [~, artifact.nan] = artifact_nan_TEM(cfg,data);
%         
%         if ~isempty(artifact.nan)
%           %% remove NaNs from data
%           
%           cfg                         = [];
%           cfg.artfctdef.nan.artifact  = artifact.nan;
%           cfg.artfctdef.reject        = 'partial';
%           cfg.outputfile              = outputfile{1};
%           
%           data = ft_rejectartifact(cfg,data);
%         end
%         
%         otrl = data.cfg.trl;   % original trl structure
%         
%         %% define tone-triggered trials
%         
%         cfg                         = [];
%         cfg.dataset                 = nexfile{r,p};
%         cfg.trialfun                = 'my_trialfun';
%         
%         if isempty(toneTS{r,p})
%           cfg.trialdef.eventtype    = 'Event006';
%         else
%           cfg.trialdef.eventtype    = 'manual';
%           cfg.trialdef.eventTS      = toneTS{r,p};
%         end
%         
%         cfg.trialdef.prestim        = 60;   % in seconds
%         cfg.trialdef.poststim       = 90;   % in seconds
%         
%         cfg = ft_definetrial(cfg);
%         ttrl = cfg.trl;                     % tone-triggered trials
%         
%         %% adjust to long trials that cover whole recording
%         
%         % adjusts beginning of 1st trial to beginning of continuous data
%         ttrl(1,1) = otrl(1,1);  
%         ttrl(1,3) = otrl(1,3);  % corrects offset to 1st tone
%         
%         for tr = 1:(size(ttrl,1)-1)
%           ttrl(tr,2) = ttrl(tr+1,1) - 1;
%         end
%         
%         % adjusts end of last trial to end of continous data
%         ttrl(end,2) = otrl(end,2);  
%         
%         %% arrange continuous raw data into long, tone-triggered trials
%         
%         cfg             = [];
%         cfg.trl         = ttrl;
%         cfg.outputfile  = outputfile{2};
%         
%         ft_redefinetrial(cfg, data);
%         
%         bestfile = 2;
%       end
%       
%       %% tone-triggered trials saved
%       if bestfile == 2
%         %% check whether artifacts have already been defined
%         
%         if exist('artifact','var') && ...
%             all(isfield(artifact,{'clip','thetafreq','lowfreq','large'}))
%           disp('using existing artifact definitions');
%         else
%           
%           %% divide continuous data into ~30s chunks,
%           % with 2s "trials" at beginning and end (and around NaNs) that
%           % will be lost to padding with NaNs
%           
%           otrl = data.cfg.trl;        % original trl structure
%           padsec = 2;                 % seconds of padding to add
%           padsamp = ceil(padsec*data.fsample);  % in # of samples
%           trlsec = 30;                % minimum length of trials in seconds
%           trlsamp = ceil(trlsec*data.fsample);  % in # of samples
%           ntrl = cell(size(otrl,1),1);
%           ctrl = otrl;                % new continuous trial structure
%           
%           for ot = 1:size(otrl,1)               % original trial #
%             otlen = otrl(ot,2)-otrl(ot,1);      % ot length
%             if otlen < (2*padsamp + trlsamp)
%               warning(['Skipping original trial # ' int2str(ot) ...
%                 ' because of insufficient length.  otlen = ' ...
%                 int2str(otlen)]);
%               ntrl{ot} = [];
%               ctrl(ot,:) = NaN;
%             else
%               nnt = floor((otlen-1 ...    % number of new trials
%                 - 2*padsamp) ...          % minus 4 seconds lost to padding
%                 /trlsamp);                % divided by min trial length
%               ntlen = floor((otlen-1 ...  % new trial length
%                 - 2*padsamp) ...          % minus 4 seconds lost to padding
%                 /nnt);                    % divided by # of new trials
%               flen = 2*padsamp + nnt*ntlen + 1; % full length of new data
%               lostsamp = otlen - flen;          % samples lost to rounding
%               
%               if lostsamp < 0
%                 error('full length of new data longer than original data');
%               elseif lostsamp > data.fsample
%                 warning([num2str(lostsamp/data.fsample) 's lost to rounding']);
%               end
%               
%               ntrl{ot} = nan([nnt+2 size(otrl,2)]);   % preallocate space
%               ntrl{ot}(1,:) = [otrl(ot,1), otrl(ot,1)+padsamp, ...
%                 otrl(ot,3:end)];              % offset and any trialinfo
%               
%               for nt = 1:nnt                  % new trial #
%                 nstart = ntrl{ot}(nt,2)+1;    % next start sample
%                 offoff = nstart-otrl(ot,1);   % offset from old offset
%                 ntrl{ot}(nt+1,:) = [nstart, nstart+ntlen-1, ...
%                   otrl(ot,3)+offoff, otrl(ot,4:end)];   % keep old trialinfo
%               end
%               
%               nstart = ntrl{ot}(nnt+1,2)+1;   % next start sample
%               offoff = nstart-otrl(ot,1);     % offset from old offset
%               ntrl{ot}(nnt+2,:) = [nstart, nstart+padsamp, ...
%                 otrl(ot,3)+offoff, otrl(ot,4:end)];   % keep old trialinfo
%               
%               if ntrl{ot}(1,1) ~= otrl(ot,1) ...
%                   || ntrl{ot}(end,2) > otrl(ot,2) ...
%                   || any(any(isnan(ntrl{ot})))
%                 error(['something is wrong with ntrl{' ...
%                   int2str(ot) '}']);
%               end
%               
%               ctrl(ot,1:2) = [ntrl{ot}(1,1) ntrl{ot}(end,2)];
%             end
%           end
%           
%           if any(isnan(ctrl))
%             badtrl = any(isnan(ctrl'));
%             ctrl = ctrl(~badtrl',:);  % continuous trl
%           end
%           trl = vertcat(ntrl{:});
%           % trl = [ttrl zeros(size(ttrl,1),1)]; % add offset = 0
%           
%           % if offset is from the beginning of the file or any single
%           % event, rejectvisual_summary>display_trial crashes because
%           % of bug 2978 (see line 196)
%           
%           % TO DO:  change to offset from nearest tone (probably by
%           % adding this cell's function to my_trialfun)
%           
%           %% restructure data into those chunks
%           
%           cfg         = [];
%           cfg.trl     = trl;
%           
%           data = ft_redefinetrial(cfg, data);
%           
%           %% preview data & note which channels look bad,
%           % but DON'T INVALIDATE TRIALS, just mark visual artifacts
%           % as necessary in the data browser
%           
%           cfg         = [];
%           % cfg.layout  = 'vertical';    % ignored anyway
%           cfg.method  = 'trial';
%           
%           data = ft_rejectvisual(cfg,data);
%           
%           %% reject bad channels based on extreme kurtosis
%           % don't toggle trials to display less than 2, and remember
%           % to toggle them back on before quitting - DON'T INVALIDATE
%           % TRIALS, just mark visual artifacts as necessary in the
%           % data browser
%           
%           cfg                         = [];
%           cfg.layout                  = 'vertical';
%           cfg.method                  = 'summary';
%           
%           data = ft_rejectvisual(cfg,data);
%           % BUG IN rejectvisual_summary>redraw (line 279) CHANGE TO
%           % axis([xmin-0.5 xmax+0.5 0.5 ymax+0.5]); and follow bug
%           % resolution here:
%           % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=1474
%           
%           % BUG IN rejectvisual_summary>redraw (line 309) CHANGE TO
%           % axis([0.5 xmax+0.5 ymin-0.5 ymax+0.5]); and follow bug
%           % resolution here:
%           % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=3005
%           
%           % BUG IN rejectvisual_summary>display_trial (line 610)
%           % where it interprets the data as timelock and tries to
%           % average across trials instead of displaying the one
%           % selected.  "plot trial" box in GUI is useless until this
%           % is fixed. Follow bug resolution here:
%           % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=2978
%           
%           goodchs{r} = data.label; %#ok<SAGROW> preallocated in info_fear
%           
%           %% define clipping artifacts
%           
%           cfg                                 = [];
%           
%           cfg.artfctdef.clip.channel          = 'AD*';
%           cfg.artfctdef.clip.pretim           = 0.25;
%           cfg.artfctdef.clip.psttim           = 0.25;
%           cfg.artfctdef.clip.timethreshold    = 0.05;
%           cfg.artfctdef.clip.amplthreshold    = '5%';
%           
%           [~, artifact.clip] = ft_artifact_clip(cfg,data);
%           
%           artifact.visual = []; % in case no databrowser is run
%           
%           %% all z-score based artifact rejection methods should
%           % be done one region at a time, so it can pick up artifacts
%           % that only affected one or the other
%           for t = 1:numel(regions)    % target regions
%             if (t==1 && ~strcmpi(ratT.chmPFC{r},'none')) ...
%                 || (t==2 && ~strcmpi(ratT.chBLA{r},'none'))
%               if strcmpi(ratT.side{r},'right')
%                 if t == 1   % mPFC
%                   channels = intersect(goodchs{r},lftchs);
%                 else        % BLA
%                   channels = intersect(goodchs{r},rtchs);
%                 end
%               else    % if rat implanted on left side
%                 if t == 1   % mPFC
%                   channels = intersect(goodchs{r},rtchs);
%                 else        % BLA
%                   channels = intersect(goodchs{r},lftchs);
%                 end
%               end
%               
%               %% define theta frequency artifacts
%               
%               cfg                               = [];
%               cfg.continuous                    = 'no';
%               cfg.trl                           = trl;
%               
%               % channel selection, cutoff and padding
%               cfg.artfctdef.zvalue.channel      = channels;
%               cfg.artfctdef.zvalue.cutoff       = 10;
%               cfg.artfctdef.zvalue.trlpadding   = 0;
%               cfg.artfctdef.zvalue.fltpadding   = 2;
%               cfg.artfctdef.zvalue.artpadding   = 0.25;
%               
%               % algorithmic parameters
%               cfg.artfctdef.zvalue.bpfilter     = 'yes';
%               cfg.artfctdef.zvalue.bpfreq       = [8 16];
%               cfg.artfctdef.zvalue.hilbert      = 'yes';
%               cfg.artfctdef.zvalue.boxcar       = 0.5;
%               
%               % make the process interactive
%               cfg.artfctdef.zvalue.interactive  = 'yes';
%               disp(['Theta frequency artifact detection for ' ...
%                 regions{t} '.']);
%               
%               [~, artifact.thetafreq{t}] = ft_artifact_zvalue(cfg,data);
%               
%               % BUG!  add tilde on line 478
%               % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=3093
%               
%               %% define low frequency artifacts
%               
%               cfg                               = [];
%               cfg.trl                           = trl;
%               
%               % channel selection, cutoff and padding
%               cfg.artfctdef.zvalue.channel      = channels;
%               cfg.artfctdef.zvalue.cutoff       = 10;
%               cfg.artfctdef.zvalue.trlpadding   = 0;
%               cfg.artfctdef.zvalue.fltpadding   = 2;
%               cfg.artfctdef.zvalue.artpadding   = 0.25;
%               
%               % algorithmic parameters
%               cfg.artfctdef.zvalue.lpfilter     = 'yes';
%               cfg.artfctdef.zvalue.lpfreq       = 1;
%               cfg.artfctdef.zvalue.rectify      = 'yes';
%               cfg.artfctdef.zvalue.boxcar       = 1;
%               
%               % make the process interactive
%               cfg.artfctdef.zvalue.interactive  = 'yes';
%               disp(['Low frequency artifact detection for ' ...
%                 regions{t} '.']);
%               
%               [~, artifact.lowfreq{t}] = ft_artifact_zvalue(cfg,data);
%               
%               %% define large artifacts
%               
%               cfg                             = [];
%               cfg.continuous                  = 'no';
%               cfg.trl                         = trl;
%               
%               % channel selection, cutoff and padding
%               cfg.artfctdef.zvalue.channel = channels;
%               cfg.artfctdef.zvalue.cutoff     = 10;
%               cfg.artfctdef.zvalue.trlpadding = 0;
%               cfg.artfctdef.zvalue.fltpadding = 0;
%               cfg.artfctdef.zvalue.artpadding = 0.25;
%               
%               % algorithmic parameters
%               cfg.artfctdef.zvalue.rectify   = 'yes';
%               
%               % make the process interactive
%               cfg.artfctdef.zvalue.interactive = 'yes';
%               disp(['Large artifact detection for ' ...
%                 regions{t} '.']);
%               
%               [~, artifact.large{t}] = ft_artifact_zvalue(cfg,data);
%               
%             end
%           end
%           
%           %% re-merge continuous data before filtering
%           
%           cfg     = [];
%           cfg.trl = ctrl; % new continuous trial structure
%           
%           data = ft_redefinetrial(cfg, data);
%           
%         end
%         
%         %% low-pass filter to remove HF chewing artifacts
%         % left in until here to be able to see them when deciding if
%         % something else is an artifact or not
%         
%         cfg             = [];
%         cfg.lpfilter    = 'yes';
%         cfg.lpfreq      = 110;
%         cfg.channel     = goodchs{r};
%         
%         data = ft_preprocessing(cfg, data);
%         
%         %% merge artifacts
%         
%         cfg                               = [];
%         % cfg.continuous                    = 'yes';
%         cfg.blocksize                     = 30;
%         cfg.artfctdef.nan.artifact        = artifact.nan;
%         cfg.artfctdef.clip.artifact       = artifact.clip;
%         cfg.artfctdef.thetafreq.artifact  = vertcat(artifact.thetafreq{:});
%         cfg.artfctdef.lowfreq.artifact    = vertcat(artifact.lowfreq{:});
%         cfg.artfctdef.large.artifact      = vertcat(artifact.large{:});
%         cfg.artfctdef.visual.artifact     = artifact.visual;
%         
%         %% review artifacts
%         % IF ONE CHANNEL DOMINATES, GO BACK AND INVALIDATE IT, THEN
%         % RERUN AUTOMATIC ARTIFACT DETECTION
%         
% %         cfg = ft_databrowser(cfg,data);
% %         artifact.visual = cfg.artfctdef.visual.artifact;
% %         
% %         warning('keyboard instructions here');
% %         keyboard;
%         % (1) To redo all artifact rejection:  delete artfile if it
%         % exists, edit channels and set ratT.finalch(r) = false in info_fear,
%         % type "dbquit" to stop execution, and rerun full script.
%         % (2) To change channels, but use existing artifact definitions:  run
%         % next cell to save these artifact definitions, edit channels and set
%         % ratT.finalch(r) = false in info_fear, type "dbquit" to stop
%         % execution, and rerun full script.
%         % (3) To redo specific parts of artifact rejection:
%         % load(outputfile{1}), remove NaNs, chunk into mini-trials, select
%         % channels, rerun desired artifact rejection cells, re-merge
%         % continuous data, filter, merge artifacts, review again, then go to
%         % option 4.
%         % (4) When happy with the results:  type "dbcont" to continue.
%         
%         %% save artifact definition
%         
%         disp(['writing artifacts to file ' artfile]);
%         save(artfile, 'artifact', '-v6');
%         
%         %% remove artifacts from data
%         
%         cfg.artfctdef.reject        = 'partial';
%         cfg.artfctdef.minaccepttim  = 0.25;
%         cfg.outputfile              = outputfile{3};
%         
%         data = ft_rejectartifact(cfg,data);
%         
%         bestfile = 3;
%         
%       end
%       
%       %% artifacts rejected
%       if bestfile == 3;
%         %% parse continuous data into 3 second subtrials, but
%         % don't overwrite continuous artifact-free 'data' in workspace
%         
%         cfg             = [];
%         cfg.length      = 3;
%         cfg.overlap     = 0;
%         cfg.outputfile  = outputfile{4};
%         
%         ft_redefinetrial(cfg, data);
%         
%         bestfile = 4;
%       end
%       
%       %% 3s chunks of clean data throughout file saved for behavioral correlation
%       if bestfile == 4
%         if ~exist('ttrl','var')
%           % if we don't have the long version, this will do
%           %% define tone-triggered trials
%           
%           cfg                         = [];
%           cfg.dataset                 = nexfile{r,p};
%           cfg.trialfun                = 'my_trialfun';
%           
%           if isempty(toneTS{r,p})
%             cfg.trialdef.eventtype  = 'Event006';
%           else
%             cfg.trialdef.eventtype  = 'manual';
%             cfg.trialdef.eventTS    = toneTS{r,p};
%           end
%           
%           cfg.trialdef.prestim        = 60; % in seconds
%           cfg.trialdef.poststim       = 90; % in seconds
%           
%           cfg = ft_definetrial(cfg);
%           ttrl = cfg.trl;
%         end
%         
%         %% arrange data into tone-triggered trials
%         
%         cfg         = [];
%         cfg.trl     = ttrl;
%         
%         data = ft_redefinetrial(cfg, data);
%         
%         %% add fake artifacts at tone onset and offset
%         % so subtrials won't cross conditions NOTE THAT THESE ARE DEFINED
%         % HERE (WHILE ALL TRIALS CONTAIN A TONE), BUT NOT REMOVED FROM
%         % THE DATA UNTIL AFTER SAVING OUTPUTFILE4!
%         
%         artifact.toneOnOff = [];
%         
%         for k = 1:numel(data.trial)
%           samples = data.sampleinfo(k,1):data.sampleinfo(k,2);
%           % find index for last sample before tone onset
%           endlastbefore = find(data.time{k}<0,1,'last');
%           next = size(artifact.toneOnOff,1)+1;
%           artifact.toneOnOff(next,1:2) = ...
%             [samples(endlastbefore)+1 ...
%             samples(endlastbefore)+1];
%           
%           % find index for last sample during tone
%           endlastduring = find(data.time{k}<30,1,'last');
%           next = size(artifact.toneOnOff,1)+1;
%           artifact.toneOnOff(next,1:2) = ...
%             [samples(endlastduring)+1 ...
%             samples(endlastduring)+1];
%         end
%         
%         %% mark NaNs as artifacts
%         % these are the previously rejected artifacts
%         
%         cfg                         = [];
%         cfg.artfctdef.nan.channel   = 'AD*';
%         
%         [~, artifact.nan] = ft_artifact_nan(cfg,data);
%         
%         %% remove NaNs from data
%         
%         cfg                         = [];
%         cfg.artfctdef.nan.artifact  = artifact.nan;
%         cfg.artfctdef.reject        = 'partial';
%         cfg.artfctdef.minaccepttim  = 0.25;
%         cfg.outputfile              = outputfile{5};
%         
%         data = ft_rejectartifact(cfg,data);
%         
%         bestfile = 5;
%       end
%       
%       %% artifacts removed from tone-triggered trials
%       if bestfile == 5
%         %% remove fake artifacts from data
%         
%         cfg                                 = [];
%         cfg.artfctdef.toneOnOff.artifact    = artifact.toneOnOff;
%         cfg.artfctdef.reject                = 'partial';
%         cfg.artfctdef.minaccepttim          = 0.25;
%         
%         data = ft_rejectartifact(cfg,data);
%         
%         %% parse trials into 3 second subtrials
%         
%         cfg             = [];
%         cfg.length      = 3;
%         cfg.overlap     = 0;
%         
%         data = ft_redefinetrial(cfg, data);
%         
%         %% add before/during/after code to data.trialinfo(:,2),
%         % check subtrial length, and check for NaNs
%         
%         for k = 1:numel(data.time) % each subtrial
%           if numel(data.time{k}) == round(3*data.fsample)
%             if data.time{k}(end)<0
%               data.trialinfo(k,2) = 1; % before tone
%             elseif data.time{k}(1)>0 && data.time{k}(end)<30
%               data.trialinfo(k,2) = 2; % during tone
%             elseif data.time{k}(1)>30
%               data.trialinfo(k,2) = 3; % after tone
%             else
%               error(['Subtrial not confined to before, '...
%                 'during, or after tone.']);
%             end
%           else
%             error('Subtrial not 3 seconds long.');
%           end
%           if any(isnan(data.trial{k}))
%             error('Data contains NaNs.');
%           end
%         end
%         
%         %% save output
%         
%         disp(['writing data to file ' outputfile{5}]);
%         
%         s = whos('data');
%         
%         % if variable < ~500 MB, store it in old (uncompressed) format,
%         % which is faster
%         if (s.bytes < 500000000)
%           save(outputfile{6}, 'data', '-v6');
%         else
%           save(outputfile{6}, 'data', '-v7.3');
%         end
%         
%         bestfile = 6;
%         
%         %% confirm final channel selection
%         % rerun all phases for this rat with the same channel selection
%         % first, also remember to add ratT.finalch(r) = true to info_fear
%         
%         if p == 3 && ~ratT.finalch(r)
%           ratT.finalch(r) = input(['This channel selection '...
%             'should be finalized (1=true/0=false).  ']);
%         end
%       end
%       
%       %% last output file is done
%       if bestfile == 6
%         disp('all files good');
%         %% clear variables
%         
%         clearvars data cfg out* art* trl
%       else
%         error('end rat/phase loop with bestfile ~= 6');
%       end
    end
  end
end
