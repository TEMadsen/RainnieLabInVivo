function BER = BERCal( MagEtyBef, MagEtyDur )
% Calculate Magazine Enhancing Ratio (BER) for reinforcement conditioning
%   Magazine Enrty= ME
%   BER difined as (MEdur-MRbef)/(MEdur+MEbef). If BER is NANm, BER= 0. If
%   BER < 0, BER =0
BER = NaN;
if isnan((MagEtyDur - MagEtyBef)./(MagEtyDur + MagEtyBef)) == 1
    BER = 0;
elseif (MagEtyDur - MagEtyBef)/(MagEtyDur + MagEtyBef) < 0
    BER = 0;
else
    BER = (MagEtyDur - MagEtyBef)./(MagEtyDur + MagEtyBef);
end

