% This script merges data that contains multiple recording files in a day.
% The script also calculates and plot pre-tone normalized power and
% coherence values with group plotting.

%% Housekeeping
clearvars; close all force;

set(0,'DefaultFigureWindowStyle','docked') % dock figures

clc
info_VPA_RC   % load all background info (filenames, etc)

%% Parameters
foi = 1.5:0.5:100;   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%

day = 1:9;

toiBaseline = find(toi < -0.5 & toi > -1);

highfoi2plot = find(foi <110 & foi > 30);
lowfoi2plot = find(foi <40 & foi > 1.5);
foi2plot = {lowfoi2plot;highfoi2plot};
foiLabel = {'low';'high'};
%% Preallocation
NAcPowAllBL = NaN(size(ratT,1),numel(phases),numel(foi));
BLAPowAllBL = NaN(size(ratT,1),numel(phases),numel(foi));
CohAllBL = NaN(size(ratT,1),numel(phases),numel(foi));

NAcPowAllNorm = NaN(size(ratT,1),numel(phases),numel(foi),numel(toi));
BLAPowAllNorm = NaN(size(ratT,1),numel(phases),numel(foi),numel(toi));
CohAllNorm = NaN(size(ratT,1),numel(phases),numel(foi),numel(toi));
CohAllNormPct = NaN(size(ratT,1),numel(phases),numel(foi),numel(toi));

%% calculate normalized power and coherence
inputfile = [datafolder 'FullData' filesep 'FC' filesep 'FullTFanalysis.mat'];
outputfile = [datafolder 'FullData' filesep 'FC' filesep 'FullTFanalysisNorm.mat'];

load(inputfile)

for ii = 1:size(ratT,1)
    for jj = 1:numel(phases)
        NAcPowAllBL(ii,jj,:) = nanmean(NAcPowAll(ii,jj,:,toiBaseline),4);
        BLAPowAllBL(ii,jj,:) = nanmean(BLAPowAll(ii,jj,:,toiBaseline),4);
        CohAllBL(ii,jj,:) = nanmean(CohAll(ii,jj,:,toiBaseline),4);
        for kk = 1:numel(foi)
        NAcPowAllNorm(ii,jj,kk,:) = NAcPowAll(ii,jj,kk,:)./(NAcPowAllBL(ii,jj,kk));
        BLAPowAllNorm(ii,jj,kk,:) = BLAPowAll(ii,jj,kk,:)./(BLAPowAllBL(ii,jj,kk));
        CohAllNorm(ii,jj,kk,:) = CohAll(ii,jj,kk,:) - (CohAllBL(ii,jj,kk));
        end
    end
end

for ii = 1:size(ratT,1)
    for jj = 1:numel(phases)
        for kk = 1:numel(foi)
        CohAllNormPct(ii,jj,kk,:) = ((CohAll(ii,jj,kk,:) - (CohAllBL(ii,jj,kk))) ... 
            ./CohAllBL(ii,jj,kk));
        end
    end
end

save(outputfile, 'NAcPowAllBL','BLAPowAllBL','CohAllBL' ...
    ,'NAcPowAllNorm','BLAPowAllNorm','CohAllNorm')

%% plot normalized power and coherence for individual rats

% double check how to normalize power and coherence before proceed
% for power, decible conversion -> division or the other way
NAcPowAllNormOrig = NAcPowAllNorm;
BLAPowAllNormOrig = BLAPowAllNorm;
CohAllNormOrig = CohAllNorm;

NAcPowAllNorm = 10.*log10(NAcPowAllNorm);
BLAPowAllNorm = 10.*log10(BLAPowAllNorm);


for ii = 1:size(ratT,1)
    for jj = 1:numel(phases)
        for kk = 1:2 % high and low frequency bands
            figure(1)
            clf
            imagesc(squeeze(squeeze(NAcPowAllNorm(ii,jj,foi2plot{kk},:))),'YData', foi(foi2plot{kk}), 'XData', toi)
            axis xy
            ylabel('Frequency (Hz)')
            xlabel('seconds')
            title([ratT.groups{ii} 'Rat # ' int2str(ratT{ii,1}) ' NAc Power ' phases{jj} ', pretone-normalized'])
            colormap jet
            colorbar
            caxis([-2 2]);
            hcol = colorbar;
            hcol.Label.String = 'dB';
            set(gca,'fontsize',18)
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            print([figurefolder ratT.groups{ii} foiLabel{kk} 'Freq_NAcPow' int2str(ratT{ii,1}) '_D' int2str(jj) 'Norm'],'-dpng');
            
            figure(2)
            clf
            imagesc(squeeze(squeeze(BLAPowAllNorm(ii,jj,foi2plot{kk},:))),'YData', foi(foi2plot{kk}), 'XData', toi)
            axis xy
            ylabel('Frequency (Hz)')
            xlabel('seconds')
            title([ratT.groups{ii} 'Rat # ' int2str(ratT{ii,1}) ' BLA Power ' phases{jj} ', pretone-normalized'])
            colormap jet
            colorbar
            caxis([-2 2]);
            hcol = colorbar;
            hcol.Label.String = 'dB';
            set(gca,'fontsize',18)
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            print([figurefolder ratT.groups{ii} foiLabel{kk} 'Freq_BLAPow' int2str(ratT{ii,1}) '_D' int2str(jj) 'Norm'],'-dpng');
            
            figure(3)
            clf
            imagesc(squeeze(squeeze(CohAllNorm(ii,jj,foi2plot{kk},:))),'YData', foi(foi2plot{kk}), 'XData', toi)
            axis xy
            ylabel('Frequency (Hz)')
            xlabel('seconds')
            title([ratT.groups{ii} 'Rat # ' int2str(ratT{ii,1}) ' BLA Power ' phases{jj} ', pretone-normalized'])
            colormap jet
            colorbar
            caxis([-0.2 0.2])
%             if kk == 1
%                 caxis([-0.2 0.2]);
%             elseif kk == 2
%                 caxis([-0.1 0.1]);
%             end
            set(gca,'fontsize',18)
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            print([figurefolder ratT.groups{ii} foiLabel{kk} 'Freq_Coh' int2str(ratT{ii,1}) '_D' int2str(jj) 'Norm'],'-dpng');
        end
    end
end


%% plot baseline power and coherence for individual rats
NAcAllBLdB = 10.*log10(NAcPowAllBL);
BLAAllBLdB = 10.*log10(BLAPowAllBL);
CohAllBL;

highfoi2plot = find(foi <100 & foi > 20);
lowfoi2plot = find(foi <20 & foi > 1.5);
foi2plot = {lowfoi2plot;highfoi2plot};
foiLabel = {'low';'high'};

for ii = 1:size(ratT,1)
        for kk = 1:2 % high and low frequency bands
            figure(1)
            clf
            imagesc(squeeze(NAcAllBLdB(ii,:,foi2plot{kk}))','YData', foi(foi2plot{kk}))
            axis xy
            ylabel('Frequency (Hz)','FontSize',18,'FontWeight','bold')
            xlabel('Days','FontSize',18,'FontWeight','bold')
            title([ratT.groups{ii} 'Rat # ' int2str(ratT{ii,1}) ' Baseline NAc Power across Days'])
            colormap jet
            colorbar
            hcol = colorbar;
            hcol.Label.String = 'dB';
            set(gca,'fontsize',18)
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            print([figurefolder ratT.groups{ii} foiLabel{kk} 'Freq_BL_NAcPow' int2str(ratT{ii,1})],'-dpng');
            
            figure(2)
            clf
            imagesc(squeeze(BLAAllBLdB(ii,:,foi2plot{kk}))','YData', foi(foi2plot{kk}))
            axis xy
            ylabel('Frequency (Hz)')
            xlabel('seconds')
            title([ratT.groups{ii} 'Rat # ' int2str(ratT{ii,1}) ' Baseline BLA Power across Days'])
            colormap jet
            colorbar
            hcol = colorbar;
            hcol.Label.String = 'dB';
            set(gca,'fontsize',18)
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            print([figurefolder ratT.groups{ii} foiLabel{kk} 'Freq_BL_BLAPow' int2str(ratT{ii,1})],'-dpng');
            
            figure(3)
            clf
            imagesc(squeeze(CohAllBL(ii,:,foi2plot{kk}))','YData', foi(foi2plot{kk}))
            axis xy
            ylabel('Frequency (Hz)')
            xlabel('seconds')
            title([ratT.groups{ii} 'Rat # ' int2str(ratT{ii,1}) ' Baseline Coherence across Days'])
            colormap jet
            colorbar
            set(gca,'fontsize',18)
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            print([figurefolder ratT.groups{ii} foiLabel{kk} 'Freq_BL_Coh' int2str(ratT{ii,1})],'-dpng');
        end
end


%% group analysis
groupSize = [sum(SALInd); sum(VPAInd)];

SALBLAPowNorm = NaN(groupSize(1), numel(phases), numel(foi),numel(toi));
SALNAcPowNorm = NaN(groupSize(1), numel(phases),numel(foi),numel(toi));
SALCohNorm = NaN(groupSize(1), numel(phases), numel(foi),numel(toi));
SALCoh = NaN(groupSize(1), numel(phases), numel(foi),numel(toi));
SALBLAPow = NaN(groupSize(1), numel(phases), numel(foi),numel(toi));
SALNAcPow = NaN(groupSize(1), numel(phases),numel(foi),numel(toi));

VPABLAPowNorm = NaN(groupSize(2), numel(phases), numel(foi),numel(toi));
VPANAcPowNorm = NaN(groupSize(2), numel(phases), numel(foi),numel(toi));
VPACohNorm = NaN(groupSize(2), numel(phases), numel(foi),numel(toi));
VPACoh = NaN(groupSize(2), numel(phases), numel(foi),numel(toi));
VPABLAPow = NaN(groupSize(2), numel(phases), numel(foi),numel(toi));
VPANAcPow = NaN(groupSize(2), numel(phases), numel(foi),numel(toi));
%% reorganize data by group

% CohAllNormOrig = CohAllNorm;
% NAcPowAllNorm = 10.*log10(NAcPowAllNorm);
% BLAPowAllNorm = 10.*log10(BLAPowAllNorm);

SALcount = 1;
VPAcount = 1;
for ii = 1:size(ratT,1)
    if SALInd(ii) == 1
%         SALNAcPowNorm(SALcount,:,:,:) = NAcPowAllNorm(ii,:,:,:);
%         SALBLAPowNorm(SALcount,:,:,:) = BLAPowAllNorm(ii,:,:,:);
%         SALCohNorm(SALcount,:,:,:) = CohAllNorm(ii,:,:,:);
%         SALCoh(SALcount,:,:,:) = CohAll(ii,:,:,:);
        SALNAcPow(SALcount,:,:,:) = NAcPowAll(ii,:,:,:);
        SALBLAPow(SALcount,:,:,:) = BLAPowAll(ii,:,:,:);
        SALcount = SALcount +1;
    elseif VPAInd(ii) ==1
%         VPANAcPowNorm(VPAcount,:,:,:) = NAcPowAllNorm(ii,:,:,:);
%         VPABLAPowNorm(VPAcount,:,:,:) = BLAPowAllNorm(ii,:,:,:);
%         VPACohNorm(VPAcount,:,:,:) = CohAllNorm(ii,:,:,:);
%         VPACoh(VPAcount,:,:,:) = CohAll(ii,:,:,:);
        VPANAcPow(VPAcount,:,:,:) = NAcPowAll(ii,:,:,:);
        VPABLAPow(VPAcount,:,:,:) = BLAPowAll(ii,:,:,:);
        VPAcount = VPAcount +1;
    else
        error(["Rat " int2str(ii) " has error on treatment labeling"]);
    end
end


%% plot averaged group results
SALNAcPowNormAvg = squeeze(nanmean(SALNAcPowNorm,1));
SALBLAPowNormAvg = squeeze(nanmean(SALBLAPowNorm,1));
SALCohNormAvg = squeeze(nanmean(SALCohNorm,1));

VPANAcPowNormAvg = squeeze(nanmean(VPANAcPowNorm,1));
VPABLAPowNormAvg = squeeze(nanmean(VPABLAPowNorm,1));
VPACohNormAvg = squeeze(nanmean(VPACohNorm,1));

SALAvg = {SALNAcPowNormAvg; SALBLAPowNormAvg; SALCohNormAvg};
VPAAvg = {VPANAcPowNormAvg; VPABLAPowNormAvg; VPACohNormAvg};


highfoi2plot = find(foi <100 & foi > 20);
lowfoi2plot = find(foi <20 & foi > 1.5);
foi2plot = {lowfoi2plot;highfoi2plot};
foiLabel = {'low';'high'};


for ii = 1:numel(treatment)
    if ii == 1
        data2plot =SALAvg;
    elseif ii == 2
        data2plot = VPAAvg;
    end
    for jj = 1:numel(phases)
        for kk = 1:numel(data2plot)
            for mm = 1:2 % low and high freq
                figure(numel(data2plot)*(kk-1)+mm)
                clf
                imagesc(squeeze(data2plot{kk}(jj,foi2plot{mm},:)),'YData', foi(foi2plot{mm}), 'XData', toi)
                axis xy
                ylabel('Frequency (Hz)')
                xlabel('seconds')
                colormap jet
                colorbar
                
                if kk == 3
                    title(['Averaged ' treatment{ii} ' coh ' phases{jj} ', pretone-normalized'])                  
                    caxis([-0.1 0.1]);
                else
                    title(['Averaged ' treatment{ii} ' ' regions{kk} ' Power ' phases{jj} ...
                        ', pretone-normalized'])                  
                    hcol = colorbar;
                    hcol.Label.String = 'dB';
                    caxis([-1.5 1.5]); 
                end
                
                set(gca,'fontsize',18)
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                
                if kk == 3
                print([figurefolder treatment{ii} foiLabel{mm} 'Freq_Coh_D' ...
                    int2str(jj) '_Norm'],'-dpng');
                else
                print([figurefolder treatment{ii} foiLabel{mm} 'Freq_' regions{kk} 'Pow_D' ...
                    int2str(jj) '_Norm'],'-dpng');
                end
            end
        end
    end
end

%% Plot percent changes in coherence at group level
CohAllNormPct;

SALCohNormPct = NaN(groupSize(1), numel(phases), numel(foi),numel(toi));
VPACohNormPct = NaN(groupSize(2), numel(phases), numel(foi),numel(toi));

SALcount = 1;
VPAcount = 1;
for ii = 1:size(ratT,1)
    if SALInd(ii) == 1
        SALCohNormPct(SALcount,:,:,:) = CohAllNormPct(ii,:,:,:);
        SALcount = SALcount +1;
    elseif VPAInd(ii) ==1
        VPACohNormPct(VPAcount,:,:,:) = CohAllNormPct(ii,:,:,:);
        VPAcount = VPAcount +1;
    else
        error(["Rat " int2str(ii) " has error on treatment labeling"]);
    end
end

for ii = 1:numel(treatment)
    if ii == 1
        data2plot = squeeze(nanmean(SALCohNormPct,1))*100;
    elseif ii == 2
        data2plot = squeeze(nanmean(VPACohNormPct,1))*100;
    end
    for jj = 1:numel(phases)
        for mm = 1:2 % low and high freq
            figure(numel(data2plot)*(kk-1)+mm)
            clf
            imagesc(squeeze(data2plot(jj,foi2plot{mm},:)),'YData', foi(foi2plot{mm}), 'XData', toi)
            axis xy
            ylabel('Frequency (Hz)')
            xlabel('seconds')
            colormap jet
            colorbar
            title(['Averaged ' treatment{ii} ' Coherence ' phases{jj} ', pretone-normalized'])
            caxis([-20 20]);
            hcol = colorbar;
            hcol.Label.String = '% change from baseline';
            set(gca,'fontsize',18)
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            print([figurefolder treatment{ii} foiLabel{mm} 'Freq_Coh_D' ...
                int2str(jj) '_NormPct'],'-dpng');
        end
    end
end


%% Reorganize data by group
SALcount = 1;
VPAcount = 1;

SALNAcBL = NaN(groupSize(1), numel(phases), numel(foi));
SALBLABL = NaN(groupSize(1), numel(phases), numel(foi));
SALCohBL = NaN(groupSize(1), numel(phases), numel(foi));
VPANAcBL = NaN(groupSize(2), numel(phases), numel(foi));
VPABLABL = NaN(groupSize(2), numel(phases), numel(foi));
VPACohBL = NaN(groupSize(2), numel(phases), numel(foi));

for ii = 1:size(ratT,1)
    if SALInd(ii) == 1
        SALNAcBL(SALcount,:,:) = NAcPowAllBL(ii,:,:);
        SALBLABL(SALcount,:,:) = BLAPowAllBL(ii,:,:);
        SALCohBL(SALcount,:,:) = CohAllBL(ii,:,:);
        SALcount = SALcount +1;
    elseif VPAInd(ii) ==1
        VPANAcBL(VPAcount,:,:) = NAcPowAllBL(ii,:,:);
        VPABLABL(VPAcount,:,:) = BLAPowAllBL(ii,:,:);
        VPACohBL(VPAcount,:,:) = CohAllBL(ii,:,:);
        VPAcount = VPAcount +1;
    else
        error(["Rat " int2str(ii) " has error on treatment labeling"]);
    end
end

%% Plot averaged baseline power and coherence across days
SALNAcBL = 10.*log10(SALNAcBL);
SALBLABL = 10.*log10(SALBLABL);
VPANAcBL = 10.*log10(VPANAcBL);
VPABLABL = 10.*log10(VPABLABL);

SALNAcBLAvg = squeeze(nanmean(SALNAcBL,1));
SALBLABLAvg = squeeze(nanmean(SALBLABL,1));
SALCohBLAvg = squeeze(nanmean(SALCohBL,1));

VPANAcBLAvg = squeeze(nanmean(VPANAcBL,1));
VPABLABLAvg = squeeze(nanmean(VPABLABL,1));
VPACohBLAvg = squeeze(nanmean(VPACohBL,1));

SALBLAvg = {SALNAcBLAvg; SALBLABLAvg; SALCohBLAvg};
VPABLAvg = {VPANAcBLAvg; VPABLABLAvg; VPACohBLAvg};

for ii = 1:numel(treatment)
    if ii == 1
        data2plot =SALBLAvg;
    elseif ii == 2
        data2plot = VPABLAvg;
    end
    for kk = 1:numel(data2plot)
        for mm = 1:2 % low and high freq
            figure(numel(data2plot)*(kk-1)+mm)
            clf
            imagesc(data2plot{kk}(:,foi2plot{mm})','YData', foi(foi2plot{mm}))
            axis xy
            ylabel('Frequency (Hz)')
            xlabel('Days')
            colormap jet
            colorbar
            
            if kk == 3
                title(['Averaged ' treatment{ii} ' Baseline Coherence'])
            else
                title(['Averaged ' treatment{ii} ' ' regions{kk} ' Baseline Power'])
                hcol = colorbar;
                hcol.Label.String = 'dB';
            end
            
            set(gca,'fontsize',18)
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            
            if kk == 3
                print([figurefolder treatment{ii} foiLabel{mm} ...
                    'Freq_BL_Coh_Norm'],'-dpng');
            else
                print([figurefolder treatment{ii} foiLabel{mm} 'Freq_BL_' ...
                    regions{kk} 'Pow_D_Norm'],'-dpng');
            end
        end
    end
end

outputfile1 = [datafolder 'FullData' filesep 'FC' filesep 'FullGroupTFanalysisNorm.mat'];
save(outputfile1, 'SALNAcBL','SALBLABL','SALCohBL','VPANAcBL','VPABLABL','VPACohBL', ...
    'SALCohNormPct', 'VPACohNormPct', 'SALBLAPowNorm', 'SALNAcPowNorm', ...
    'SALCohNorm', 'VPABLAPowNorm', 'VPANAcPowNorm', 'VPACohNorm')

load(outputfile1)
load(outputfile)
load(inputfile)
%%
toiDur = find(toi > 0.1 & toi <5.9);
foiTheta = find(foi >= 5 & foi <= 8);
SALCohPctThetaDur = squeeze(mean(mean(SALCohNormPct(:,:,foiTheta,toiDur),4),3)).*100;
VPACohPctThetaDur = squeeze(mean(mean(VPACohNormPct(:,:,foiTheta,toiDur),4),3)).*100;

SALCohThetaBL = squeeze(mean(SALCohBL(:,:,foiTheta),3));
SALCohThetaDur = squeeze(mean(mean(SALCoh(:,:,foiTheta,toiDur),4),3));

VPACohThetaBL = squeeze(mean(VPACohBL(:,:,foiTheta),3));
VPACohThetaDur = squeeze(mean(mean(VPACoh(:,:,foiTheta,toiDur),4),3));

%% NAc power

SALNAcPowNormThetaDur = squeeze(mean(mean(SALNAcPowNorm(:,:,foiTheta,toiDur),4),3));
VPANAcPowNormThetaDur = squeeze(mean(mean(VPANAcPowNorm(:,:,foiTheta,toiDur),4),3));

SALNAcPowThetaBL = squeeze(mean(SALNAcBL(:,:,foiTheta),3));
SALNAcPowThetaDur = squeeze(mean(mean(SALNAcPow(:,:,foiTheta,toiDur),4),3));

VPANAcPowThetaBL = squeeze(mean(VPANAcBL(:,:,foiTheta),3));
VPANAcPowThetaDur = squeeze(mean(mean(VPANAcPow(:,:,foiTheta,toiDur),4),3));

SALNAcPowThetaBL = 10.*log10(SALNAcPowThetaBL);
SALNAcPowThetaDur = 10.*log10(SALNAcPowThetaDur);

VPANAcPowThetaBL = 10.*log10(VPANAcPowThetaBL);
VPANAcPowThetaDur = 10.*log10(VPANAcPowThetaDur);
%% BLA power

SALBLAPowNormThetaDur = squeeze(mean(mean(SALBLAPowNorm(:,:,foiTheta,toiDur),4),3));
VPABLAPowNormThetaDur = squeeze(mean(mean(VPABLAPowNorm(:,:,foiTheta,toiDur),4),3));

SALBLAPowThetaBL = squeeze(mean(SALBLABL(:,:,foiTheta),3));
SALBLAPowThetaDur = squeeze(mean(mean(SALBLAPow(:,:,foiTheta,toiDur),4),3));

VPABLAPowThetaBL = squeeze(mean(VPABLABL(:,:,foiTheta),3));
VPABLAPowThetaDur = squeeze(mean(mean(VPABLAPow(:,:,foiTheta,toiDur),4),3));

SALBLAPowThetaBL = 10.*log10(SALBLAPowThetaBL);
SALBLAPowThetaDur = 10.*log10(SALBLAPowThetaDur);

VPABLAPowThetaBL = 10.*log10(VPABLAPowThetaBL);
VPABLAPowThetaDur = 10.*log10(VPABLAPowThetaDur);