%% calculates 2d cross-frequency phase-amplitude modulation
% for each interval type of multiple social interaction tests
% across multiple rats

close all; clearvars; clc;  % clean slate

info_VPA_RC % load all background info (filenames, etc)

plt = 'n';                  % plot CFC
hx = waitbar(0,'Getting started...');

%% set analysis parameters

foi_phase = 1.5:0.5:20.5;   % central frequencies for phase modulation
w_phase = 0.5;              % width of phase band (foi +/- w)
foi_amp = 20:4:132;         % central frequencies for modulated amplitude
w_amp = 8;                  % width of amplitude band (foi +/- w)


bk2cal = [1, 14, 15, 18];
bk2calD = [1, 7, 8, 9];
%% use try-catch when running unsupervised batches

for r = 1:size(ratT,1)
  for b = 1:numel(bk2cal)
    try
      if ~isempty(nexfile{r})
        % Tone-triggered trials with artifacts removed (except for fake
        % tone on/off artifacts).  Best for spectrograms & coherograms.
        inputfile = [datafolder 'LFPTrialData_' ...
          int2str(ratT.ratnums(r)) 'Merged.mat'];
        % Each tone will have its own output file.  Checking here for one
        % of the last tones.
        outputfile = [datafolder 'KL_MI2d_' ...
          int2str(ratT.ratnums(r)) blocks{bk2cal(b)} '.mat'];
        
        %% check existing data
        
        filecorrect = false; % start false in case file doesn't exist
        
        if exist(outputfile,'file')
          filecorrect = true; % true unless demonstrated to be false
          disp(['Data for Rat # ' int2str(ratT.ratnums(r)) ', ' ...
            blocks{bk2cal(b)} ' already analyzed! Check for accuracy.']);
          load(outputfile);
          
          if ~any(any(any(~isnan(freq.powspctrm))))
            warning('Only NaNs output from freqanalysis');
            filecorrect = false;
          end
          
          if freq.time(1) >= 0
            warning('no pre-tone baseline found');
            filecorrect = false;
          else
            disp(['first timestamp is ' num2str(freq.time(1))]);
          end
          
          %           if strcmp(goodchs{r}{1},'AD*')
          %             if numel(freq.label) ~= (numel(lftchs) + numel(rtchs) + 1 ...
          %                 - numel(goodchs{r}))
          %               warning('different channels used than planned');
          %               filecorrect = false;  % reprocess this file
          %               %             else
          %               %               for ch = 1:numel(freq.label)
          %               %                 k = strfind(goodchs{r}, freq.label{ch});
          %               %                 if any([k{:}])
          %               %                   warning([freq.label{ch} ' should be excluded']);
          %               %                   filecorrect = false;  % reprocess this file
          %               %                   break  % jumps to end of for ch loop
          %               %                 else
          %               %                   disp([freq.label{ch} ' is not excluded']);
          %               %                 end
          %               %               end   % still true if no excluded channels
          %             end
          %           elseif numel(freq.label) ~= numel(goodchs{r}) || ...
          %               any(any(~strcmp(freq.label,goodchs{r})))
          %             warning('different channels used than planned');
          %             filecorrect = false;  % reprocess this file
          %           else
          %             disp([freq.label{:} ' are correct']);   % still true
          %           end
        end
        
        %% reprocess if necessary
        
        if filecorrect
          disp('checked file seems correct');
        else
                                %% preallocate memory for variables
                    
                    MI2d_NAcc = cell(30,1);
                    MI2d_BLA = cell(30,1);
                    cmax_NAcc = zeros(30,1);
                    cmax_BLA = zeros(30,1);
                    
          %% load data
          
          load(inputfile);
          
          %% loop through tones
          t = find(data.trialinfo(:,2) == bk2calD(b))';
          try
            m = data.trialinfo(t(1):t(end),1);
            o = find(m > 30*(bk2cal(b)-bk2calD(b)*2+1) & m < (30*(bk2cal(b)-bk2calD(b)*2+2)+1));
            trials = t(o);
            if isempty(trials)
              warning(['No trials found for ' ...
                num2str(ratT.ratnums(r)) '''s ' blocks{bk2cal(b)}]);
            else
              disp(['Calculating spectrograms & coherograms for trials of '...
                num2str(ratT.ratnums(r)) '''s ' blocks{bk2cal(b)} ]);
              
              %% calculate spectrograms & coherograms
              channel = cell(2,1);
              channel{1} = ratT.chNAc{r};
              channel{2} = ratT.chBLA{r};
              cfg             = [];
              
              cfg.method      = 'mtmconvol';
              cfg.output      = 'powandcsd';
              cfg.keeptrials  = 'no';
              cfg.trials      = trials;
              cfg.channel     = channel;
              cfg.foi         = foi;
              cfg.tapsmofrq   = tapsmofrq;
              cfg.t_ftimwin   = t_ftimwin;
              cfg.toi         = toi;
              cfg.pad = 'nextpow2'; % assumes longest trial is 500s
              
              cfg.outputfile  = [datafolder 'SpecCoherograms_' ...
                int2str(ratT.ratnums(r)) blocks{bk2cal(b)} '.mat'];
              
              freq = ft_freqanalysis(cfg,data);
              
              if ~any(any(any(~isnan(freq.powspctrm))))
                error('Only NaNs output from freqanalysis');
              end
            end
          catch ME1
            errcnt = errcnt + 1;
            errloc(errcnt,:) = [r b t]; %#ok<SAGROW>
            warning(['Error (' ME1.message ') while processing Rat # ' ...
              int2str(ratT.ratnums(r)) ', ' blocks{bk2cal(b)} ...
              '! Continuing with next in line.']);
          end
          
          
        end
        %% clear variables
        
        clearvars data cfg freq;
        
      end
    catch ME2
      errcnt = errcnt + 1;
      errloc(errcnt,:) = [r b 0]; %#ok<SAGROW> % not during a specific tone
      warning(['Error (' ME2.message ') while processing Rat # ' ...
        int2str(ratT.ratnums(r)) ', ' blocks{bk2cal(b)} ...
        '! Continuing with next in line.']);
    end
    
  end
end
%% make 2d Cross-Frequency Modulation plots for each rat/session/interval

for r = 1:numel(ratnums)
    chNAc = ratT{r,4};
    chBLA = ratT{r,5};
    for p = 1:numel(phases)
        try   % use try-catch when running unsupervised batches
            if ~isempty(nexfile{r,p})
                % Artifact-free 3-second subtrials marked with
                % data.trialinfo(:,1) = original trial number, and
                % data.trialinfo(:,2) = number representing condition
                inputfile = [datafolder 'CleanLFPSubtrialData_'...
                    num2str(ratnums(r)) phases{p} '.mat'];
                outputfile = [datafolder 'KL_MI2d_'...
                    num2str(ratnums(r)) phases{p} '.mat'];
                
                if ~exist(inputfile,'file')
                    error('inputfile does not exist');
                end
                
                if exist(outputfile,'file')
                    disp(['Data for Rat # ' int2str(ratnums(r)) ', ' ...
                        phases{p} ' already analyzed! Skipping.']);
                else
                    %% preallocate memory for variables
                    
                    MI2d_NAcc = cell(numel(intervals),1);
                    MI2d_BLA = cell(numel(intervals),1);
                    cmax_NAcc = zeros(numel(intervals),1);
                    cmax_BLA = zeros(numel(intervals),1);
                    
                    %% load data
                    load(inputfile);
                    
                    %% analyze data
                    for int = 1:numel(intervals)
                        trials = find(data.trialinfo(:,2) == int);
                        if isempty(trials)
                            warning(['No ' intervals{int} ...
                                ' intervals found for Rat # ' ...
                                num2str(ratnums(r)) ', ' phases{p} ...
                                '.  Skipping.']);
                        else
                            for t = 1:numel(trials)
                                waitbar((1+((t-1)+((int-1)+((p-1)+(r-1)*numel(phases))*...
                                    numel(intervals))*numel(trials))*2)/(2*...
                                    numel(ratnums)*numel(phases)*numel(intervals)*...
                                    numel(trials)),hx,...
                                    ['Calculating 2d MI for Rat # ' num2str(ratnums(r)) ...
                                    ', ' phases{p} ', ' intervals{int} ', NAcc']);
                                [MI2d_NAcc{int}(:,:,t)] = KL_MI2d_TEM(...
                                    data.trial{trials(t)}(chNAc,:),foi_phase,...
                                    w_phase,foi_amp,w_amp,plt);
                                
                                waitbar((2+((t-1)+((int-1)+((p-1)+(r-1)*numel(phases))*...
                                    numel(intervals))*numel(trials))*2)/(2*...
                                    numel(ratnums)*numel(phases)*numel(intervals)*...
                                    numel(trials)),hx,...
                                    ['Calculating 2d MI for Rat # ' num2str(ratnums(r)) ...
                                    ', ' phases{p} ', ' intervals{int} ', BLA']);
                                [MI2d_BLA{int}(:,:,t)] = KL_MI2d_TEM(...
                                    data.trial{trials(t)}(chBLA,:),foi_phase,...
                                    w_phase,foi_amp,w_amp);
                                
                            end
                            if plt == 'y'
                                figure(1+(int-1)*2);
                                %%% plot mean!!!
                                title({['Rat # ' num2str(ratnums(r)) ', ' ...
                                    char(phases(p)) ', ' intervals{int}]; ...
                                    ['NAcc, Average of  ' ...
                                    num2str(numel(trials)) ' Trials']});
                                figure(int*2);
                                %%% plot mean!!!
                                title({['Rat # ' num2str(ratnums(r)) ', ' ...
                                    char(phases(p)) ', ' intervals{int}]; ...
                                    ['BLA, Average of  ' ...
                                    num2str(numel(trials)) ' Trials']});
                            end
                            cmax_NAcc(int)=max(max(max(mean(...
                                MI2d_NAcc{int},3))),cmax_NAcc(int));
                            cmax_BLA(int)=max(max(max(mean(...
                                MI2d_BLA{int},3))),cmax_BLA(int));
                        end
                    end
                    if plt == 'y'
                        for fig = 1:2:(int*2)
                            figure(fig); caxis([0 cmax_NAcc(int)]);
                            set(gcf,'WindowStyle','docked');
                        end
                        for fig = 2:2:(int*2)
                            figure(fig); caxis([0 cmax_BLA(int)]);
                            set(gcf,'WindowStyle','docked');
                        end
                        % pause execution to save & close figures (crashes otherwise)
                        keyboard; % type "return" to continue loop or "dbquit" to exit
                    end
                    %% save analysis results
                    
                    disp(['writing results to file ' outputfile]);
                    
                    save(outputfile, 'MI2d*', 'cmax*', 'foi*', 'w*', '-v6');
                    
                end
            end
        catch ME
            warning(['Error (' ME.message ') while processing Rat # ' ...
                int2str(ratnums(r)) ', ' phases{p} ...
                '! Continuing with next in line.']);
        end
    end
end
close(hx)
