% This script merges data that contains multiple recording files in a day.
% The script also calculates and plot pre-tone normalized power and
% coherence values with group plotting.

%% Housekeeping
clearvars; close all force;
clc
info_VPA_RC   % load all background info (filenames, etc)

%% Parameters
foi = 1.5:0.5:100;   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%

%% Preallocate spaces
NAcPowAll = NaN(size(ratT,1),numel(phases),numel(foi),numel(toi));
BLAPowAll = NaN(size(ratT,1),numel(phases),numel(foi),numel(toi));
CohAll = NaN(size(ratT,1),numel(phases),numel(foi),numel(toi));

%% merge
outputfile = [datafolder 'FullData' filesep 'FC' filesep 'FullTFanalysis.mat'];

for ii = 1:size(ratT,1)
    for jj = 1:numel(phases)
        if size(nexfile{ii,jj},1) > 1
            % preallocate space for merge
            NumofTrls = NaN(size(nexfile{ii,jj},1),1);
            pow2combine = NaN(size(nexfile{ii,jj},1),2,numel(foi),numel(toi));
            crsspctrm2combine = NaN(size(nexfile{ii,jj},1),1,numel(foi),numel(toi));
            for kk = 1:size(nexfile{ii,jj},1)
                inputfile1 = [datafolder filesep 'FullData' filesep 'clean' filesep ...
                    'CleanLFPTrialData_' int2str(ratT.ratnums(ii)) phases{jj} num2str(kk) '.mat'];
                inputfile2 = [datafolder 'FullData' filesep 'FC' filesep 'TFAnalysis' filesep ...
                    'SpecCoherograms_' int2str(ratT.ratnums(ii)) phases{jj} num2str(kk) '.mat'];
                if ~exist(inputfile1,'file')
                    error([inputfile1 ' does not exist. Please check.'])
                else
                    load(inputfile1)
                    NumofTrls(kk) = data.trialinfo(end);
                    clear data
                end
                
                if ~exist(inputfile2,'file')
                    error([inputfile2 ' does not exist. Please check.'])
                else
                    load(inputfile2)
                    pow2combine(kk,:,:,:) = NumofTrls(kk).*freq.powspctrm;
                    crsspctrm2combine(kk,:,:,:) = NumofTrls(kk).*freq.crsspctrm;
                    clear freq
                end
            end
            totalTrl = sum(NumofTrls);
            powCombined = squeeze(nansum(pow2combine,1))./totalTrl;
            crsspctrmCombined = squeeze(nansum(crsspctrm2combine,1))./totalTrl;
            crsspctrmCombined = reshape(crsspctrmCombined,1,numel(foi),numel(toi));
            
            NAcPowAll(ii,jj,:,:) = powCombined(1,:,:);
            BLAPowAll(ii,jj,:,:) = powCombined(2,:,:);
            CohAll(ii,jj,:,:) = squeeze(abs(crsspctrmCombined(1,:,:)./ ...
                sqrt(powCombined(1,:,:).* ...
                powCombined(2,:,:))));
            
            clear pow2combine crsspctrm2combine totalTrl powCombined ...
                crsspctrmCombined
            
            % when no merge is needed
        elseif size(nexfile{ii,jj},1) == 1
            kk = 1;
            inputfile2 = [datafolder 'FullData' filesep 'FC' filesep 'TFAnalysis' filesep ...
                'SpecCoherograms_' int2str(ratT.ratnums(ii)) phases{jj} num2str(kk) '.mat'];
            if ~exist(inputfile2,'file')
                error([inputfile2 ' does not exist. Please check.'])
            else
                load(inputfile2)
                NAcPowAll(ii,jj,:,:) = freq.powspctrm(1,:,:);
                BLAPowAll(ii,jj,:,:) = freq.powspctrm(2,:,:);
                CohAll(ii,jj,:,:) = abs(freq.crsspctrm(1,:,:)./ ...
                    sqrt(freq.powspctrm(1,:,:).* ...
                    freq.powspctrm(2,:,:)));
                clear freq
            end
        end
    end
end

save(outputfile, 'NAcPowAll','BLAPowAll','CohAll')
check = sum(sum(sum(sum(isnan(NAcPowAll)))));
check1 = sum(sum(sum(isnan(NAcPowAll))));
check2 = sum(sum(sum(isnan(NAcPowAll))));