% This script merges data that contains multiple recording files in a day.
% The script also calculates and plot pre-tone normalized power and
% coherence values with group plotting.

%% Housekeeping
clearvars; close all force;

set(0,'DefaultFigureWindowStyle','docked') % dock figures

clc
info_VPA_RC   % load all background info (filenames, etc)

%% Parameters
foi = 1.5:0.5:100;   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%

day = 1:9;

toiBaseline = find(toi < -0.5 & toi > -1);

highfoi2plot = find(foi <110 & foi > 30);
lowfoi2plot = find(foi <40 & foi > 1.5);
foi2plot = {lowfoi2plot;highfoi2plot};
foiLabel = {'low';'high'};
day2cal = [1,8];


% squeeze(sum(sum(isnan(NAcPowAllBL))));

for dd = 1:numel(day2cal)
%% Preallocation
NAcPowAllBL = NaN(size(ratT,1),3,numel(foi));
BLAPowAllBL = NaN(size(ratT,1),3,numel(foi));
CohAllBL = NaN(size(ratT,1),3,numel(foi));

NAcPowAllNorm = NaN(size(ratT,1),3,numel(foi),numel(toi));
BLAPowAllNorm = NaN(size(ratT,1),3,numel(foi),numel(toi));
CohAllNorm = NaN(size(ratT,1),3,numel(foi),numel(toi));
CohAllNormPct = NaN(size(ratT,1),3,numel(foi),numel(toi));

%% calculate normalized power and coherence
inputfile = [datafolder 'FullData' filesep 'FC' filesep ...
        'FullTFanalysis20TrlD' int2str(day2cal(dd)) '.mat'];
outputfile = [datafolder 'FullData' filesep 'FC' filesep ...
    'FullTFanalysisNorm20TrlD' int2str(day2cal(dd)) '.mat'];

load(inputfile)

for ii = 1:size(ratT,1)
    for jj = 1:3
        NAcPowAllBL(ii,jj,:) = nanmean(NAcPowAll(ii,jj,:,toiBaseline),4);
        BLAPowAllBL(ii,jj,:) = nanmean(BLAPowAll(ii,jj,:,toiBaseline),4);
        CohAllBL(ii,jj,:) = nanmean(CohAll(ii,jj,:,toiBaseline),4);
        for kk = 1:numel(foi)
        NAcPowAllNorm(ii,jj,kk,:) = NAcPowAll(ii,jj,kk,:)./(NAcPowAllBL(ii,jj,kk));
        BLAPowAllNorm(ii,jj,kk,:) = BLAPowAll(ii,jj,kk,:)./(BLAPowAllBL(ii,jj,kk));
        CohAllNorm(ii,jj,kk,:) = CohAll(ii,jj,kk,:) - (CohAllBL(ii,jj,kk));
        end
    end
end

for ii = 1:size(ratT,1)
    for jj = 1:3
        for kk = 1:numel(foi)
        CohAllNormPct(ii,jj,kk,:) = ((CohAll(ii,jj,kk,:) - (CohAllBL(ii,jj,kk))) ... 
            ./CohAllBL(ii,jj,kk));
        end
    end
end

save(outputfile, 'NAcPowAllBL','BLAPowAllBL','CohAllBL' ...
    ,'NAcPowAllNorm','BLAPowAllNorm','CohAllNorm',...
    'CohAllNormPct')

NAcPowAllNorm = 10.*log10(NAcPowAllNorm);
BLAPowAllNorm = 10.*log10(BLAPowAllNorm);



%% group analysis
groupSize = [sum(SALInd); sum(VPAInd)];

SALBLAPowNorm = NaN(groupSize(1),3, numel(foi),numel(toi));
SALNAcPowNorm = NaN(groupSize(1), 3,numel(foi),numel(toi));
SALCohNorm = NaN(groupSize(1),3, numel(foi),numel(toi));
SALCoh = NaN(groupSize(1), 3, numel(foi),numel(toi));
SALBLAPow = NaN(groupSize(1),3, numel(foi),numel(toi));
SALNAcPow = NaN(groupSize(1), 3,numel(foi),numel(toi));

VPABLAPowNorm = NaN(groupSize(2), 3, numel(foi),numel(toi));
VPANAcPowNorm = NaN(groupSize(2),3, numel(foi),numel(toi));
VPACohNorm = NaN(groupSize(2), 3, numel(foi),numel(toi));
VPACoh = NaN(groupSize(2), 3, numel(foi),numel(toi));
VPABLAPow = NaN(groupSize(2), 3, numel(foi),numel(toi));
VPANAcPow = NaN(groupSize(2), 3, numel(foi),numel(toi));
%% reorganize data by group

CohAllNormOrig = CohAllNorm;


SALcount = 1;
VPAcount = 1;
for ii = 1:size(ratT,1)
    if SALInd(ii) == 1
        SALNAcPowNorm(SALcount,:,:,:) = NAcPowAllNorm(ii,:,:,:);
        SALBLAPowNorm(SALcount,:,:,:) = BLAPowAllNorm(ii,:,:,:);
        SALCohNorm(SALcount,:,:,:) = CohAllNorm(ii,:,:,:);
        SALCoh(SALcount,:,:,:) = CohAll(ii,:,:,:);
        SALNAcPow(SALcount,:,:,:) = NAcPowAll(ii,:,:,:);
        SALBLAPow(SALcount,:,:,:) = BLAPowAll(ii,:,:,:);
        SALcount = SALcount +1;
    elseif VPAInd(ii) ==1
        VPANAcPowNorm(VPAcount,:,:,:) = NAcPowAllNorm(ii,:,:,:);
        VPABLAPowNorm(VPAcount,:,:,:) = BLAPowAllNorm(ii,:,:,:);
        VPACohNorm(VPAcount,:,:,:) = CohAllNorm(ii,:,:,:);
        VPACoh(VPAcount,:,:,:) = CohAll(ii,:,:,:);
        VPANAcPow(VPAcount,:,:,:) = NAcPowAll(ii,:,:,:);
        VPABLAPow(VPAcount,:,:,:) = BLAPowAll(ii,:,:,:);
        VPAcount = VPAcount +1;
    else
        error(["Rat " int2str(ii) " has error on treatment labeling"]);
    end
end


%% plot averaged group results
SALNAcPowNormAvg = squeeze(nanmean(SALNAcPowNorm,1));
SALBLAPowNormAvg = squeeze(nanmean(SALBLAPowNorm,1));
SALCohNormAvg = squeeze(nanmean(SALCohNorm,1));

VPANAcPowNormAvg = squeeze(nanmean(VPANAcPowNorm,1));
VPABLAPowNormAvg = squeeze(nanmean(VPABLAPowNorm,1));
VPACohNormAvg = squeeze(nanmean(VPACohNorm,1));

SALAvg = {SALNAcPowNormAvg; SALBLAPowNormAvg; SALCohNormAvg};
VPAAvg = {VPANAcPowNormAvg; VPABLAPowNormAvg; VPACohNormAvg};


highfoi2plot = find(foi <100 & foi > 20);
lowfoi2plot = find(foi <20 & foi > 1.5);
foi2plot = {lowfoi2plot;highfoi2plot};
foiLabel = {'low';'high'};


for ii = 1:numel(treatment)
    if ii == 1
        data2plot =SALAvg;
    elseif ii == 2
        data2plot = VPAAvg;
    end
    for jj = 1:3
        for kk = 1:numel(data2plot)
            for mm = 1:2 % low and high freq
                figure(numel(data2plot)*(kk-1)+mm)
                clf
                imagesc(squeeze(data2plot{kk}(jj,foi2plot{mm},:)),'YData', foi(foi2plot{mm}), 'XData', toi)
                axis xy
                ylabel('Frequency (Hz)')
                xlabel('seconds')
                colormap jet
                colorbar
                
                if kk == 3
                    title(['Averaged ' treatment{ii} ' coh D' ...
                        int2str(day2cal(dd)) ' ' int2str(jj) '/3, pretone-normalized'])                  
                    caxis([-0.1 0.1]);
                else
                    title(['Averaged ' treatment{ii} ' ' regions{kk} ...
                        ' Power ' int2str(day2cal(dd)) ' ' int2str(jj) ...
                        '/3, pretone-normalized'])                  
                    hcol = colorbar;
                    hcol.Label.String = 'dB';
                    caxis([-1.5 1.5]); 
                end
                
                set(gca,'fontsize',18)
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                
                if kk == 3
                print([figurefolder treatment{ii} foiLabel{mm} 'Freq_Coh_D' ...
                    int2str(day2cal(dd)) '_Norm20trl' int2str(jj)],'-dpng');
                else
                print([figurefolder treatment{ii} foiLabel{mm} 'Freq_' regions{kk} 'Pow_D' ...
                    int2str(day2cal(dd)) '_Norm20trl' int2str(jj)],'-dpng');
                end
            end
        end
    end
end

%% Plot percent changes in coherence at group level
SALCohNormPct = NaN(groupSize(1), 3, numel(foi),numel(toi));
VPACohNormPct = NaN(groupSize(2), 3, numel(foi),numel(toi));

SALcount = 1;
VPAcount = 1;
for ii = 1:size(ratT,1)
    if SALInd(ii) == 1
        SALCohNormPct(SALcount,:,:,:) = CohAllNormPct(ii,:,:,:);
        SALcount = SALcount +1;
    elseif VPAInd(ii) ==1
        VPACohNormPct(VPAcount,:,:,:) = CohAllNormPct(ii,:,:,:);
        VPAcount = VPAcount +1;
    else
        error(["Rat " int2str(ii) " has error on treatment labeling"]);
    end
end

for ii = 1:numel(treatment)
    if ii == 1
        data2plot = squeeze(nanmean(SALCohNormPct,1))*100;
    elseif ii == 2
        data2plot = squeeze(nanmean(VPACohNormPct,1))*100;
    end
    for jj = 1:3
        for mm = 1:2 % low and high freq
            figure(numel(data2plot)*(kk-1)+mm)
            clf
            imagesc(squeeze(data2plot(jj,foi2plot{mm},:)),'YData', foi(foi2plot{mm}), 'XData', toi)
            axis xy
            ylabel('Frequency (Hz)')
            xlabel('seconds')
            colormap jet
            colorbar
            title(['Averaged ' treatment{ii} ' Coherence ' phases{day2cal(dd)} ' '...
                int2str(jj) ' of 3, pretone-normalized'])
            caxis([-20 20]);
            hcol = colorbar;
            hcol.Label.String = '% change from baseline';
            set(gca,'fontsize',18)
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            print([figurefolder treatment{ii} foiLabel{mm} 'Freq_Coh_D' ...
                int2str(day2cal(dd)) '20trl_' int2str(jj) '_NormPct'],'-dpng');
        end
    end
end


%% Reorganize data by group
SALcount = 1;
VPAcount = 1;

SALNAcBL = NaN(groupSize(1), 3, numel(foi));
SALBLABL = NaN(groupSize(1), 3, numel(foi));
SALCohBL = NaN(groupSize(1), 3, numel(foi));
VPANAcBL = NaN(groupSize(2), 3, numel(foi));
VPABLABL = NaN(groupSize(2), 3, numel(foi));
VPACohBL = NaN(groupSize(2), 3, numel(foi));

for ii = 1:size(ratT,1)
    if SALInd(ii) == 1
        SALNAcBL(SALcount,:,:) = NAcPowAllBL(ii,:,:);
        SALBLABL(SALcount,:,:) = BLAPowAllBL(ii,:,:);
        SALCohBL(SALcount,:,:) = CohAllBL(ii,:,:);
        SALcount = SALcount +1;
    elseif VPAInd(ii) ==1
        VPANAcBL(VPAcount,:,:) = NAcPowAllBL(ii,:,:);
        VPABLABL(VPAcount,:,:) = BLAPowAllBL(ii,:,:);
        VPACohBL(VPAcount,:,:) = CohAllBL(ii,:,:);
        VPAcount = VPAcount +1;
    else
        error(["Rat " int2str(ii) " has error on treatment labeling"]);
    end
end

%% Plot averaged baseline power and coherence across days
SALNAcBL = 10.*log10(SALNAcBL);
SALBLABL = 10.*log10(SALBLABL);
VPANAcBL = 10.*log10(VPANAcBL);
VPABLABL = 10.*log10(VPABLABL);

SALNAcBLAvg = squeeze(nanmean(SALNAcBL,1));
SALBLABLAvg = squeeze(nanmean(SALBLABL,1));
SALCohBLAvg = squeeze(nanmean(SALCohBL,1));

VPANAcBLAvg = squeeze(nanmean(VPANAcBL,1));
VPABLABLAvg = squeeze(nanmean(VPABLABL,1));
VPACohBLAvg = squeeze(nanmean(VPACohBL,1));

SALBLAvg = {SALNAcBLAvg; SALBLABLAvg; SALCohBLAvg};
VPABLAvg = {VPANAcBLAvg; VPABLABLAvg; VPACohBLAvg};

for ii = 1:numel(treatment)
    if ii == 1
        data2plot =SALBLAvg;
    elseif ii == 2
        data2plot = VPABLAvg;
    end
    for kk = 1:numel(data2plot)
        for mm = 1:2 % low and high freq
            figure(numel(data2plot)*(kk-1)+mm)
            clf
            imagesc(data2plot{kk}(:,foi2plot{mm})','YData', foi(foi2plot{mm}))
            axis xy
            ylabel('Frequency (Hz)')
            xlabel('Blocks of 20 trials')
            colormap jet
            colorbar
            
            if kk == 3
                title(['Averaged ' treatment{ii} ' Baseline Coherence' phases{day2cal(dd)} ' 20trl'])
            else
                title(['Averaged ' treatment{ii} ' ' regions{kk} ' Baseline Power' phases{day2cal(dd)} ' 20trl'])
                hcol = colorbar;
                hcol.Label.String = 'dB';
            end
            
            set(gca,'fontsize',18)
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            
            if kk == 3
                print([figurefolder treatment{ii} foiLabel{mm} ...
                    'Freq_BL_Coh_Norm' phases{day2cal(dd)} '_20trl'],'-dpng');
            else
                print([figurefolder treatment{ii} foiLabel{mm} 'Freq_BL_' ...
                    regions{kk} 'Pow_D_Norm' phases{day2cal(dd)} '_20trl'],'-dpng');
            end
        end
    end
end



% load(outputfile1)
% load(outputfile)
% load(inputfile)
%%
toiDur = find(toi > 0.1 & toi <5.9);
foiTheta = find(foi >= 5 & foi <= 8);
SALCohPctThetaDur = squeeze(nanmean(nanmean(SALCohNormPct(:,:,foiTheta,toiDur),4),3)).*100;
VPACohPctThetaDur = squeeze(nanmean(nanmean(VPACohNormPct(:,:,foiTheta,toiDur),4),3)).*100;

SALCohThetaBL = squeeze(nanmean(SALCohBL(:,:,foiTheta),3));
SALCohThetaDur = squeeze(nanmean(nanmean(SALCoh(:,:,foiTheta,toiDur),4),3));

VPACohThetaBL = squeeze(nanmean(VPACohBL(:,:,foiTheta),3));
VPACohThetaDur = squeeze(nanmean(nanmean(VPACoh(:,:,foiTheta,toiDur),4),3));

%% NAc power

SALNAcPowNormThetaDur = squeeze(nanmean(nanmean(SALNAcPowNorm(:,:,foiTheta,toiDur),4),3));
VPANAcPowNormThetaDur = squeeze(nanmean(nanmean(VPANAcPowNorm(:,:,foiTheta,toiDur),4),3));

SALNAcPowThetaBL = squeeze(nanmean(SALNAcBL(:,:,foiTheta),3));
SALNAcPowThetaDur = squeeze(nanmean(nanmean(SALNAcPow(:,:,foiTheta,toiDur),4),3));

VPANAcPowThetaBL = squeeze(nanmean(VPANAcBL(:,:,foiTheta),3));
VPANAcPowThetaDur = squeeze(nanmean(nanmean(VPANAcPow(:,:,foiTheta,toiDur),4),3));

%% BLA power

SALBLAPowNormThetaDur = squeeze(nanmean(nanmean(SALBLAPowNorm(:,:,foiTheta,toiDur),4),3));
VPABLAPowNormThetaDur = squeeze(nanmean(nanmean(VPABLAPowNorm(:,:,foiTheta,toiDur),4),3));

SALBLAPowThetaBL = squeeze(nanmean(SALBLABL(:,:,foiTheta),3));
SALBLAPowThetaDur = squeeze(nanmean(nanmean(SALBLAPow(:,:,foiTheta,toiDur),4),3));

VPABLAPowThetaBL = squeeze(nanmean(VPABLABL(:,:,foiTheta),3));
VPABLAPowThetaDur = squeeze(nanmean(nanmean(VPABLAPow(:,:,foiTheta,toiDur),4),3));

outputfile1 = [datafolder 'FullData' filesep 'FC' filesep ...
    'FullGroupTFanalysisNormD' int2str(day2cal(dd)) '_20trl.mat'];
save(outputfile1, 'SALNAcBL','SALBLABL','SALCohBL','VPANAcBL','VPABLABL','VPACohBL', ...
    'SALCohNormPct', 'VPACohNormPct', 'SALBLAPowNorm', 'SALNAcPowNorm', ...
    'SALCohNorm', 'VPABLAPowNorm', 'VPANAcPowNorm', 'VPACohNorm','SALCohPctThetaDur',...
    'VPACohPctThetaDur','SALCohThetaBL','SALCohThetaDur','VPACohThetaBL','VPACohThetaDur', ...
'SALNAcPowNormThetaDur','VPANAcPowNormThetaDur','SALNAcPowThetaBL','SALNAcPowThetaDur',...
    'VPANAcPowThetaBL','VPANAcPowThetaDur','SALNAcPowThetaBL','SALNAcPowThetaDur',...
    'VPANAcPowThetaBL','VPANAcPowThetaDur','SALBLAPowNormThetaDur',...
    'VPABLAPowNormThetaDur','SALBLAPowThetaBL','SALBLAPowThetaDur',...
    'VPABLAPowThetaBL','VPABLAPowThetaDur','SALBLAPowThetaBL',...
    'SALBLAPowThetaDur','VPABLAPowThetaBL','VPABLAPowThetaDur');




end