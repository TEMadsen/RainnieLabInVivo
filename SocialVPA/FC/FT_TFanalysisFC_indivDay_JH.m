%% Calculate raw spectrograms and coherograms for each trial

close all; clearvars; clc;  % clean slate
set(0,'DefaultFigureWindowStyle','docked') % dock figures

info_VPA_RC               % load all background info (filenames, etc)

foi = 1.5:0.5:100;   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%

day = 1:9;

toiBaseline = find(toi < -0.5 & toi > -1);

highfoi2plot = find(foi <110 & foi > 30);
lowfoi2plot = find(foi <40 & foi > 1.5);

%% use try-catch when running unsupervised batches
for r = 1:size(ratT,1)
    for dd = 1:numel(phases)
        for fn = 1:size(nexfile{r,dd},1)  % file number w/in a phase
            % Tone-triggered trials with artifacts removed (except for fake
            % tone on/off artifacts).  Best for spectrograms & coherograms.
            inputfile = [datafolder filesep 'FullData' filesep 'clean' filesep ...
                'CleanLFPTrialData_' int2str(ratT.ratnums(r)) phases{dd} num2str(fn) '.mat'];
            
            % Each tone will have its own output file.  Checking here for one
            % of the last tones.
            outputfile = [datafolder 'FullData' filesep 'FC' filesep 'TFAnalysis' filesep ...
                'SpecCoherograms_' int2str(ratT.ratnums(r)) phases{dd} num2str(fn) '.mat'];
            
            if exist(outputfile,'file')
                disp([outputfile ' already exists.  Skipping rat.'])
            else
                disp(['Calculating spectrograms & coherograms for trials of '...
                    num2str(ratT.ratnums(r)) '''s Day ' int2str(day(dd)) ]);
                load(inputfile);
                
                %% calculate spectrograms & coherograms
                channel = cell(2,1);
                channel{1} = ratT.chNAc{r};
                channel{2} = ratT.chBLA{r};
                cfg             = [];
                
                cfg.method      = 'wavelet';
                cfg.output      = 'powandcsd';
                cfg.keeptrials  = 'no';
                cfg.channel     = channel;
                cfg.foi         = foi;
                cfg.tapsmofrq   = tapsmofrq;
                cfg.t_ftimwin   = t_ftimwin;
                cfg.toi         = toi;
                cfg.pad = 'nextpow2'; % assumes longest trial is 500s
                
                cfg.outputfile  = outputfile;
                
                freq = ft_freqanalysis(cfg,data);
                
                if ~any(any(any(~isnan(freq.powspctrm))))
                    error('Only NaNs output from freqanalysis');
                end
                
                freq.coherence      = NaN(size(freq.crsspctrm));
                freq.coherence(1,:,:) = abs(freq.crsspctrm(1,:,:)./ ...
                    sqrt(freq.powspctrm(1,:,:).* ...
                    freq.powspctrm(2,:,:)));
                
                %% low freq
                imagesc(10.*log10(squeeze(freq.powspctrm(1,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
                axis xy
                ylabel('Frequency (Hz)')
                xlabel('seconds')
                colormap jet
                colorbar
                title(['Rat # ' int2str(ratT.ratnums(r)) ' NAc Power ' phases{dd}])
                colormap jet
                colorbar
                hcol = colorbar;
                hcol.Label.String = 'dB';
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                print([figurefolder 'LF_Rat' int2str(ratT.ratnums(r)) '_NAc_D' phases{dd} num2str(fn)],'-dpng');
                
                imagesc(10.*log10(squeeze(freq.powspctrm(2,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
                axis xy
                ylabel('Frequency (Hz)')
                xlabel('seconds')
                colormap jet
                colorbar
                title(['Rat # ' int2str(ratT.ratnums(r)) ' BLA Power ' phases{dd}])
                colormap jet
                colorbar
                hcol = colorbar;
                hcol.Label.String = 'dB';
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                print([figurefolder 'LF_Rat' int2str(ratT.ratnums(r)) '_BLA_D' phases{dd} num2str(fn)],'-dpng');
                
                
                imagesc(squeeze(freq.coherence(1,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
                axis xy
                ylabel('Frequency (Hz)')
                xlabel('seconds')
                colormap jet
                colorbar
                title(['Rat # ' int2str(ratT.ratnums(r)) ' Coherence ' phases{dd}])
                colormap jet
                colorbar
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                print([figurefolder 'LF_Rat' int2str(ratT.ratnums(r)) '_Coherence_D' phases{dd} num2str(fn)],'-dpng');
                
                %% high freq
                imagesc(10.*log10(squeeze(freq.powspctrm(1,highfoi2plot,:))),'YData', foi(highfoi2plot), 'XData', toi)
                axis xy
                ylabel('Frequency (Hz)')
                xlabel('seconds')
                colormap jet
                colorbar
                title(['Rat # ' int2str(ratT.ratnums(r)) ' NAc Power ' phases{dd}])
                colormap jet
                colorbar
                hcol = colorbar;
                hcol.Label.String = 'dB';
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                print([figurefolder 'HF_Rat' int2str(ratT.ratnums(r)) '_NAc_D' phases{dd} num2str(fn)],'-dpng');
                
                imagesc(10.*log10(squeeze(freq.powspctrm(2,highfoi2plot,:))),'YData', foi(highfoi2plot), 'XData', toi)
                axis xy
                ylabel('Frequency (Hz)')
                xlabel('seconds')
                colormap jet
                colorbar
                title(['Rat # ' int2str(ratT.ratnums(r)) ' BLA Power ' phases{dd}])
                colormap jet
                colorbar
                hcol = colorbar;
                hcol.Label.String = 'dB';
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                print([figurefolder 'HF_Rat' int2str(ratT.ratnums(r)) '_BLA_D' phases{dd} num2str(fn)],'-dpng');
                
                
                imagesc(squeeze(freq.coherence(1,highfoi2plot,:)),'YData', foi(highfoi2plot), 'XData', toi)
                axis xy
                ylabel('Frequency (Hz)')
                xlabel('seconds')
                colormap jet
                colorbar
                title(['Rat # ' int2str(ratT.ratnums(r)) ' Coherence ' phases{dd}])
                colormap jet
                colorbar
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                print([figurefolder 'HF_Rat' int2str(ratT.ratnums(r)) '_Coherence_D' phases{dd} num2str(fn)],'-dpng');
                
                clearvars data cfg freq
            end
        end
    end
end

% imagesc(squeeze(freq.powspctrm(1,:,:)),'YData', foi, 'XData', toi)
% axis xy
% ylabel('Frequency (Hz)')
% xlabel('seconds')
% colormap jet
% colorbar