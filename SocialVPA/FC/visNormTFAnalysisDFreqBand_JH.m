%% House keeping
close all; clearvars; clc;    % clean slate
set(0,'DefaultFigureWindowStyle','docked') % dock figures

%% parameters
info_VPA_RC                    % load all background info (filenames, etc)
foi = 1.5:0.5:120;   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%

plt = true;                  % if plots are desired for each tone and rat
errcnt = 0;                   % error count
errloc = [];                  % error location (r,p,t)

days = 1:9;
toiBaseline = find(toi < -0.5 & toi > -1);
toiTone = find(toi > 0.2 & toi < 5.8);

foiDelta = find(foi > 2 & foi < 5); %10-30
foiTheta = find(foi > 5 & foi < 8); %10-30
foiAlpha = find(foi > 8 & foi < 14); %10-30
foiHGamma = find(foi > 50 & foi < 100); %80-110
fboi = {foiDelta;foiTheta;foiAlpha;foiHGamma};

SALsize = sum(strcmp(ratT{:,7},'Saline'));
VPAsize = sum(strcmp(ratT{:,7},'VPA'));

%% preallocation

SALNAcPowNorm = NaN(numel(days),SALsize,numel(foi),numel(toi));
SALBLAPowNorm = NaN(numel(days),SALsize,numel(foi),numel(toi));
SALCohNorm = NaN(numel(days),SALsize,numel(foi),numel(toi));
VPANAcPowNorm = NaN(numel(days),VPAsize,numel(foi),numel(toi));
VPABLAPowNorm = NaN(numel(days),VPAsize,numel(foi),numel(toi));
VPACohNorm = NaN(numel(days),VPAsize,numel(foi),numel(toi));

SALNAcPowFreqNorm = NaN(numel(days),SALsize,4,numel(toi)); %2nd dim = 4 freq bands
SALBLAPowFreqNorm = NaN(numel(days),SALsize,4,numel(toi)); %2nd dim = 4 freq bands
SALCohFreqNorm = NaN(numel(days),SALsize,4,numel(toi)); %2nd dim = 4 freq bands
VPANAcPowFreqNorm = NaN(numel(days),VPAsize,4,numel(toi)); %2nd dim = 4 freq bands
VPABLAPowFreqNorm = NaN(numel(days),VPAsize,4,numel(toi)); %2nd dim = 4 freq bands
VPACohFreqNorm = NaN(numel(days),VPAsize,4,numel(toi)); %2nd dim = 4 freq bands

%% reorganize and normalize data
for bb = 1:numel(days)
    inputfile1 = [datafolder 'FC' filesep 'SpecCoherograms_' ...
        'VPA_D' int2str(days(bb)) '.mat'];
    inputfile2 = [datafolder 'FC' filesep 'SpecCoherograms_' ...
        'SAL_D' int2str(days(bb)) '.mat'];

    load(inputfile1)
    load(inputfile2)
    
    for rr = 1:size(VPAPow,1)
        for ff = 1:size(VPAPow,3)
            SALNAcPowNorm(bb,rr,ff,:) = SALPow(rr,1,ff,:)./SALPowbaseline(rr,1,ff);
            SALBLAPowNorm(bb,rr,ff,:) = SALPow(rr,2,ff,:)./SALPowbaseline(rr,2,ff);
            VPANAcPowNorm(bb,rr,ff,:) = VPAPow(rr,1,ff,:)./VPAPowbaseline(rr,1,ff);
            VPABLAPowNorm(bb,rr,ff,:) = VPAPow(rr,2,ff,:)./VPAPowbaseline(rr,2,ff);
            VPACohNorm(bb,rr,ff,:) = VPACoh(rr,ff,:) - VPACohbaseline(rr,ff);
            SALCohNorm(bb,rr,ff,:) = SALCoh(rr,ff,:) - SALCohbaseline(rr,ff);
        end
    end
end

% converting power to dB
SALNAcPowNorm = 10.*log10(SALNAcPowNorm);
SALBLAPowNorm = 10.*log10(SALBLAPowNorm);
VPANAcPowNorm = 10.*log10(VPANAcPowNorm);
VPABLAPowNorm = 10.*log10(VPABLAPowNorm);

outputfile1 = [datafolder 'FC' filesep 'NormalizedPowCoh_LinScale.mat'];

save(outputfile1, 'SALNAcPowNorm','SALBLAPowNorm','VPANAcPowNorm',...
    'VPABLAPowNorm','VPACohNorm','SALCohNorm');

% %% use this to 
% plot(mean(squeeze(mean(VPACohNorm(1,:,1:50,toiTone),2)),2))
% hold on
% plot(mean(squeeze(mean(VPACohNorm(3,:,1:50,toiTone),2)),2))
% plot(mean(squeeze(mean(VPACohNorm(5,:,1:50,toiTone),2)),2))
% plot(mean(squeeze(mean(VPACohNorm(7,:,1:50,toiTone),2)),2))
% plot(mean(squeeze(mean(VPACohNorm(9,:,1:50,toiTone),2)),2))
% foi(14)
% legend('show')
% 
% foi(95)
% foi(201)


%% average across rats
for fb = 1:size(fboi,1)
SALNAcPowFreqNorm(:,:,fb,:) = nanmean(SALNAcPowNorm(:,:,fboi{fb},:),3);
SALBLAPowFreqNorm(:,:,fb,:) = nanmean(SALBLAPowNorm(:,:,fboi{fb},:),3);
SALCohFreqNorm(:,:,fb,:) = nanmean(SALCohNorm(:,:,fboi{fb},:),3);
VPANAcPowFreqNorm(:,:,fb,:) = nanmean(VPANAcPowNorm(:,:,fboi{fb},:),3);
VPABLAPowFreqNorm(:,:,fb,:) = nanmean(VPABLAPowNorm(:,:,fboi{fb},:),3);
VPACohFreqNorm(:,:,fb,:) = nanmean(VPACohNorm(:,:,fboi{fb},:),3);
end


% sum(sum(sum(sum(isnan(SALNAcPowNorm)))))
% sum(sum(sum(sum(isnan(SALBLAPowNorm)))))
% sum(sum(sum(sum(isnan(VPANAcPowNorm)))))
% sum(sum(sum(sum(isnan(VPABLAPowNorm)))))
% sum(sum(sum(sum(isnan(VPACohNorm)))))
% sum(sum(sum(sum(isnan(SALCohNorm)))))
% 
% sum(sum(sum(sum(isnan(SALPow)))))
% sum(sum(sum(sum(isnan(VPAPow)))))
% sum(sum(sum(sum(isnan(SALCoh)))))
% sum(sum(sum(sum(isnan(VPACoh)))))
% 
% sum(sum(sum(isnan(SALPowbaseline))))
% sum(sum(sum(isnan(VPAPowbaseline))))
% sum(sum(isnan(VPACohbaseline)))
% sum(sum(isnan(SALCohbaseline)))

% SALNAcPowNormdurtone = NaN(numel(days),SALsize,4);
% SALBLAPowNormdurtone = NaN(numel(days),SALsize,4);
% SALCohNormdurtone = NaN(numel(days),SALsize,4);
% VPANAcPowNormdurtone = NaN(numel(days),VPAsize,4);
% VPABLAPowNormdurtone = NaN(numel(days),VPAsize,4);
% VPACohNormdurtone = NaN(numel(days),VPAsize,4);

SALNAcPowNormdurtone = squeeze(nanmean(SALNAcPowFreqNorm(:,:,:,toiTone),4));
SALBLAPowNormdurtone = squeeze(nanmean(SALBLAPowFreqNorm(:,:,:,toiTone),4));
SALCohNormdurtone = squeeze(nanmean(SALCohFreqNorm(:,:,:,toiTone),4));
VPANAcPowNormdurtone = squeeze(nanmean(VPANAcPowFreqNorm(:,:,:,toiTone),4));
VPABLAPowNormdurtone = squeeze(nanmean(VPABLAPowFreqNorm(:,:,:,toiTone),4));
VPACohNormdurtone = squeeze(nanmean(VPACohFreqNorm(:,:,:,toiTone),4));
dimOrd = {'day','rats','fboi'};
outputfile2 = [datafolder 'FC' filesep 'NormalizedPowCohdurTone.mat'];
save(outputfile2,'SALNAcPowNormdurtone','SALBLAPowNormdurtone','SALCohNormdurtone', ...
    'VPANAcPowNormdurtone','VPABLAPowNormdurtone','VPACohNormdurtone','fboi','dimOrd')


NAcVPADelta = squeeze(VPANAcPowNormdurtone(:,:,1));
NAcSALDelta = squeeze(SALNAcPowNormdurtone(:,:,1));
NAcVPATheta = squeeze(VPANAcPowNormdurtone(:,:,2));
NAcSALTheta = squeeze(SALNAcPowNormdurtone(:,:,2));
NAcVPAAlpha = squeeze(VPANAcPowNormdurtone(:,:,3));
NAcSALAlPha = squeeze(SALNAcPowNormdurtone(:,:,3));
NAcVPAHGamma = squeeze(VPANAcPowNormdurtone(:,:,4));
NAcSALHGamma = squeeze(SALNAcPowNormdurtone(:,:,4));

BLAVPADelta = squeeze(VPABLAPowNormdurtone(:,:,1));
BLASALDelta = squeeze(SALBLAPowNormdurtone(:,:,1));
BLAVPATheta = squeeze(VPABLAPowNormdurtone(:,:,2));
BLASALTheta = squeeze(SALBLAPowNormdurtone(:,:,2));
BLAVPAAlpha = squeeze(VPABLAPowNormdurtone(:,:,3));
BLASALAlPha = squeeze(SALBLAPowNormdurtone(:,:,3));
BLAVPAHGamma = squeeze(VPABLAPowNormdurtone(:,:,4));
BLASALHGamma = squeeze(SALBLAPowNormdurtone(:,:,4));

CohVPADelta = squeeze(VPACohNormdurtone(:,:,1));
CohSALDelta = squeeze(SALCohNormdurtone(:,:,1));
CohVPATheta = squeeze(VPACohNormdurtone(:,:,2));
CohSALTheta = squeeze(SALCohNormdurtone(:,:,2));
CohVPAAlpha = squeeze(VPACohNormdurtone(:,:,3));
CohSALAlPha = squeeze(SALCohNormdurtone(:,:,3));
CohVPAHGamma = squeeze(VPACohNormdurtone(:,:,4));
CohSALHGamma = squeeze(SALCohNormdurtone(:,:,4));


CohVPAContrast = squeeze(VPACohNormdurtone(:,:,2))...
    +abs(squeeze(VPACohNormdurtone(:,:,1)))++abs(squeeze(VPACohNormdurtone(:,:,3)));
CohSALContrast = squeeze(SALCohNormdurtone(:,:,2))...
    +abs(squeeze(SALCohNormdurtone(:,:,1)))++abs(squeeze(SALCohNormdurtone(:,:,3)));
BLAVPAContrast = squeeze(VPABLAPowNormdurtone(:,:,2))...
    +abs(squeeze(VPABLAPowNormdurtone(:,:,1)))++abs(squeeze(VPABLAPowNormdurtone(:,:,3)));
BLASALContrast = squeeze(SALBLAPowNormdurtone(:,:,2))...
    +abs(squeeze(SALBLAPowNormdurtone(:,:,1)))++abs(squeeze(SALBLAPowNormdurtone(:,:,3)));
NAcVPAContrast = squeeze(VPANAcPowNormdurtone(:,:,2))...
    +abs(squeeze(VPANAcPowNormdurtone(:,:,1)))++abs(squeeze(VPANAcPowNormdurtone(:,:,3)));
NAcSALContrast = squeeze(SALNAcPowNormdurtone(:,:,2))...
    +abs(squeeze(SALNAcPowNormdurtone(:,:,1)))++abs(squeeze(SALNAcPowNormdurtone(:,:,3)));

dimOrd2 = {'day','fboi'};
outputfile3 = [datafolder 'FC' filesep 'NormalizedPowCohdurTone2d_Acq10trl.mat'];
save(outputfile3,'NAc*','BLA*','Coh*','fboi','dimOrd2')
% 
% 
% plot(squeeze(mean(VPANAcPowNormdurtone(:,:,1),2)))
% plot(squeeze(mean(SALNAcPowNormdurtone(:,:,1),2)))
% 
% 
% a = (squeeze(mean(SALCohNormdurtone(:,:,2),2)) + abs(squeeze(mean(SALCohNormdurtone(:,:,3),2)))...
%     +abs(squeeze(mean(SALCohNormdurtone(:,:,1),2))));
% plot(a)
% b1 = squeeze(mean(SALCohFreqNorm(1,:,2,:),1));
% b7 = squeeze(mean(SALCohFreqNorm(7,:,2,:),1));
% plot(b1)
% hold on
% plot(b7)
% 
% b1 = squeeze(mean(VPANAcPowFreqNorm(1,:,2,:),2));
% b7 = squeeze(mean(VPANAcPowFreqNorm(7,:,2,:),2));
% plot(b1)
% hold on
% plot(b7)
% 
% plot(squeeze(mean(SALCohNormdurtone(:,:,2),2)))
% plot(squeeze(mean(VPACohNormdurtone(:,:,1),2)))
% plot(squeeze(mean(SALCohNormdurtone(:,:,3),2)))
% plot(squeeze(mean(SALCohNormdurtone(:,:,4),2)))
% 
% squeeze(mean(SALNAcPowNormdurtone(:,:,2),2))
% 
% outputfile = [];
% 

%     
%     VPAPowAvg = squeeze(nanmean(VPAPow,1));
%     VPAPowNormAvg = squeeze(nanmean(VPAPowNorm,1));
%     VPACohAvg = squeeze(nanmean(VPACoh,1));
%     VPACohNormAvg = squeeze(nanmean(VPACohNorm,1));
%     
%     SALPowAvg = squeeze(nanmean(SALPow,1));
%     SALPowNormAvg = squeeze(nanmean(SALPowNorm,1));
%     SALCohAvg = squeeze(nanmean(SALCoh,1));
%     SALCohNormAvg = squeeze(nanmean(SALCohNorm,1));
%     
%     save(outputfileVPA, 'VPAPow','VPAPowbaseline','VPACoh','VPACohbaseline')
%     save(outputfileSAL, 'SALPow','SALPowbaseline','SALCoh','SALCohbaseline')