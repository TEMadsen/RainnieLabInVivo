close all force; clearvars; clc;  % clean slate

info_VPA_RC                       % load all background info (filenames, etc)

foi_phase = 3:0.25:18.0;  % central frequencies for phase modulation
w_phase = 0.25;              % width of phase band (foi +/- w)
foi_amp = 9:3:141;         % central frequencies for modulated amplitude
w_amp = 3;                  % width of amplitude band (foi +/- w)

foiPhase2Plot = find(foi_phase > 4);
foiAmp2Plot = find(foi_amp > 8);

SALsize = sum(strcmp(ratT.groups,'Saline'));
VPAsize = sum(strcmp(ratT.groups,'VPA'));

MI2dFull = NaN(size(ratT,1),numel(phases),2,2,numel(foi_phase),numel(foi_amp));
phasepref2dFull = NaN(size(ratT,1),numel(phases),2,2,numel(foi_phase),numel(foi_amp));

%% reorganize data

outputfile = [datafolder filesep 'FullData' filesep 'CFC' filesep ...
                    'crossKLMI2d_durTone200msTrim_NewPar_Full.mat'];

for iR = 1:size(ratT,1)
    for dd = 1:numel(phases)
        if ~iscell(nexfile{iR,dd})  % in case of multiple files per phase
            nexfile{iR,dd} = nexfile(iR,dd);  % convert to cellstr
        end
        if numel(nexfile{iR,dd}) > 1
            MI2dStack = [];
            phasepref2dStack = [];
            for fn = 1:numel(nexfile{iR,dd})  % file number w/in a phase
                inputfile = [datafolder filesep 'FullData' filesep 'CFC' filesep ...
                    'crossKLMI2d_durTone200msTrim_NewPar' int2str(ratT.ratnums(iR)) phases{dd} num2str(fn) '.mat'];
                load(inputfile)
                MI2dStack = cat(3,MI2dStack,MI2d);
                phasepref2dStack = cat(3,phasepref2dStack,phasepref2d);
                clearvars MI2d phasepref2d
            end
            MI2dStack = squeeze(nanmean(MI2dStack,3));
            phasepref2dStack = squeeze(nanmean(phasepref2dStack,3));
        elseif numel(nexfile{iR,dd}) == 1
            inputfile = [datafolder filesep 'FullData' filesep 'CFC' filesep ...
                'crossKLMI2d_durTone200msTrim_NewPar' int2str(ratT.ratnums(iR)) phases{dd} '_Full.mat'];
            load(inputfile)
            MI2dStack = MI2d;
            phasepref2dStack = phasepref2d;
            clearvars MI2d phasepref2d
            MI2dStack = squeeze(nanmean(MI2dStack,3));
            phasepref2dStack = squeeze(nanmean(phasepref2dStack,3));
        end
        MI2dFull(iR,dd,:,:,:,:) = MI2dStack;
        phasepref2dFull(iR,dd,:,:,:,:) = phasepref2dStack;
        clearvars MI2dStack phasepref2dStack
    end
end
axisLabel = {'Rat#','Day', axisLabel{1},axisLabel{2},axisLabel{4},axisLabel{5}};
save(outputfile, 'MI2dFull', 'phasepref2dFull', 'axisLabel', 'foi_amp','foi_phase','w_amp','w_phase')

%% reorganize data by treatment

SALMI2d = NaN(SALsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));
SALphasepref2d = NaN(SALsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));

VPAMI2d = NaN(VPAsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));
VPAphasepref2d = NaN(VPAsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));

for iR = 1:height(ratT)
    gr = ratT.groups{iR};
    for iDay = 1:numel(phases)
        if strcmp(gr, 'VPA') ==1
            grID = sum(VPAInd(1:iR));
            VPAMI2d(grID,:,:,:,:,:) = ...
                MI2dFull(iR,:,:,:,:,:);
            VPAphasepref2d(grID,:,:,:,:,:) = ...
                phasepref2dFull(iR,:,:,:,:,:);
        elseif strcmp(gr, 'Saline') ==1
            grID = sum(SALInd(1:iR));
            SALMI2d(grID,:,:,:,:,:) = ...
                MI2dFull(iR,:,:,:,:,:);
            SALphasepref2d(grID,:,:,:,:,:) = ...
                phasepref2dFull(iR,:,:,:,:,:);
        end
    end
end

SALMI2dGroup = squeeze(nanmean(SALMI2d,1));
SALphasepref2dGroup = squeeze(nanmean(SALphasepref2d,1));
VPAMI2dGroup = squeeze(nanmean(VPAMI2d,1));
VPAphasepref2dGroup = squeeze(nanmean(VPAphasepref2d,1));

for group = 1:numel(treatment)
    if group == 1
        data2plot = SALMI2dGroup;
        grp = 'SAL';
    elseif group == 2
        data2plot = VPAMI2dGroup;
        grp = 'VPA';
    end
    for iDay = 1:numel(phases)
        for pr = 1:2
            if pr == 1
                prlabel = 'NAc';
            else
                prlabel = 'BLA';
            end
            for ar = 1:2
                if ar == 1
                    arlabel = 'NAc';
                else
                    arlabel = 'BLA';
                end
                clf
                obj2plot = squeeze(data2plot(iDay,pr,ar,:,:));
                imagesc(obj2plot(foiPhase2Plot,foiAmp2Plot)','XData',foi_phase(foiPhase2Plot),'YData',foi_amp(foiAmp2Plot))
%                 imagesc(obj2plot(foiPhase2Plot,:),'XData',foi_phase(foiPhase2Plot),'YData',foi_amp)

                axis xy
                colormap jet
                colorbar
                caxis([0 3*10^(-3)]);
                
                set(gca, 'FontName', 'Arial', 'FontSize', 16);
                xlabel('Phase Frequency (Hz)');
                ylabel('Envelope Frequency (Hz)');
                
                title({[grp ' D' int2str(iDay)];[ ...
                    prlabel ' phase modulating ' ...
                    arlabel ' amplitude']});
                if pr == 1 && ar == 1
                saveas(gcf,[figurefolder 'NAcmodNAc' filesep 'KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod' arlabel 'amp.png'])
                elseif pr == 1 && ar == 2
                saveas(gcf,[figurefolder 'NAcmodBLA' filesep 'KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod' arlabel 'amp.png'])
                elseif pr == 2 && ar == 1
                saveas(gcf,[figurefolder 'BLAmodNAc' filesep 'KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod' arlabel 'amp.png'])
                elseif pr == 2 && ar == 2
                saveas(gcf,[figurefolder 'BLAmodBLA' filesep 'KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod' arlabel 'amp.png'])
                end
            end
        end
    end
end

% %% plot results of individual rats
% 
% for group = 1:numel(treatment)
%     for iDay = 1:numel(phases)
%         for pr = 1:2
%             if pr == 1
%                 prlabel = 'NAc';
%             else
%                 prlabel = 'BLA';
%             end
%             for ar = 1:2
%                 if ar == 1
%                     arlabel = 'NAc';
%                 else
%                     arlabel = 'BLA';
%                 end
%                 clf
%                 obj2plot = squeeze(data2plot(iDay,pr,ar,:,:));
%                 imagesc(obj2plot(foiPhase2Plot,foiAmp2Plot)','XData',foi_phase(foiPhase2Plot),'YData',foi_amp(foiAmp2Plot))
% %                 imagesc(obj2plot(foiPhase2Plot,:),'XData',foi_phase(foiPhase2Plot),'YData',foi_amp)
% 
%                 axis xy
%                 colormap jet
%                 colorbar
%                 caxis([0 4*10^(-3)]);
%                 
%                 set(gca, 'FontName', 'Arial', 'FontSize', 16);
%                 xlabel('Phase Frequency (Hz)');
%                 ylabel('Envelope Frequency (Hz)');
%                 
%                 title({[grp ' D' int2str(iDay)];[ ...
%                     prlabel ' phase modulating ' ...
%                     arlabel ' amplitude']});
%                 if pr == 1 && ar == 1
%                 saveas(gcf,[figurefolder 'NAcmodNAc' filesep 'KLMI' grp '_D' int2str(iDay) ...
%                     prlabel 'mod' arlabel 'amp.png'])
%                 elseif pr == 1 && ar == 2
%                 saveas(gcf,[figurefolder 'NAcmodBLA' filesep 'KLMI' grp '_D' int2str(iDay) ...
%                     prlabel 'mod' arlabel 'amp.png'])
%                 elseif pr == 2 && ar == 1
%                 saveas(gcf,[figurefolder 'BLAmodNAc' filesep 'KLMI' grp '_D' int2str(iDay) ...
%                     prlabel 'mod' arlabel 'amp.png'])
%                 elseif pr == 2 && ar == 2
%                 saveas(gcf,[figurefolder 'BLAmodBLA' filesep 'KLMI' grp '_D' int2str(iDay) ...
%                     prlabel 'mod' arlabel 'amp.png'])
%                 end
%             end
%         end
%     end
% end

%% load pre-tone MI2d and phasepref

MI2dFull_BL = NaN(size(ratT,1),numel(phases),2,2,numel(foi_phase),numel(foi_amp));
phasepref2dFull_BL = NaN(size(ratT,1),numel(phases),2,2,numel(foi_phase),numel(foi_amp));

%% reorganize data

outputfile = [datafolder filesep 'FullData' filesep 'CFC' filesep ...
                    'crossKLMI2d_durTone200msTrim_Full_NewPar_BL.mat'];

for iR = 1:size(ratT,1)
    for dd = 1:numel(phases)
        if ~iscell(nexfile{iR,dd})  % in case of multiple files per phase
            nexfile{iR,dd} = nexfile(iR,dd);  % convert to cellstr
        end
        if numel(nexfile{iR,dd}) > 1
            MI2dStack = [];
            phasepref2dStack = [];
            for fn = 1:numel(nexfile{iR,dd})  % file number w/in a phase
                inputfile = [datafolder filesep 'FullData' filesep 'clean' filesep ...
                    'crossKLMI2d_preTone200msTrim_NewPar' int2str(ratT.ratnums(iR)) phases{dd} num2str(fn) '.mat'];
                load(inputfile)
                MI2dStack = cat(3,MI2dStack,MI2d);
                phasepref2dStack = cat(3,phasepref2dStack,phasepref2d);
                clearvars MI2d phasepref2d
            end
            MI2dStack = squeeze(nanmean(MI2dStack,3));
            phasepref2dStack = squeeze(nanmean(phasepref2dStack,3));
        elseif numel(nexfile{iR,dd}) == 1
            inputfile = [datafolder filesep 'FullData' filesep 'clean' filesep ...
                'crossKLMI2d_preTone200msTrim_NewPar' int2str(ratT.ratnums(iR)) phases{dd} '_Full.mat'];
            load(inputfile)
            MI2dStack = MI2d;
            phasepref2dStack = phasepref2d;
            clearvars MI2d phasepref2d
            MI2dStack = squeeze(nanmean(MI2dStack,3));
            phasepref2dStack = squeeze(nanmean(phasepref2dStack,3));
        end
        MI2dFull_BL(iR,dd,:,:,:,:) = MI2dStack;
        phasepref2dFull_BL(iR,dd,:,:,:,:) = phasepref2dStack;
        clearvars MI2dStack phasepref2dStack
    end
end
axisLabel = {'Rat#','Day', axisLabel{1},axisLabel{2},axisLabel{4},axisLabel{5}};
save(outputfile, 'MI2dFull_BL', 'phasepref2dFull_BL', 'axisLabel')
%% plot pre-tone MI2d and phasepref

SALMI2d_BL = NaN(SALsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));
SALphasepref2d_BL = NaN(SALsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));

VPAMI2d_BL = NaN(VPAsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));
VPAphasepref2d_BL = NaN(VPAsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));

for iR = 1:height(ratT)
    gr = ratT.groups{iR};
    for iDay = 1:numel(phases)
        if strcmp(gr, 'VPA') ==1
            grID = sum(VPAInd(1:iR));
            VPAMI2d_BL(grID,:,:,:,:,:) = ...
                MI2dFull_BL(iR,:,:,:,:,:);
            VPAphasepref2d_BL(grID,:,:,:,:,:) = ...
                phasepref2dFull_BL(iR,:,:,:,:,:);
        elseif strcmp(gr, 'Saline') ==1
            grID = sum(SALInd(1:iR));
            SALMI2d_BL(grID,:,:,:,:,:) = ...
                MI2dFull_BL(iR,:,:,:,:,:);
            SALphasepref2d_BL(grID,:,:,:,:,:) = ...
                phasepref2dFull_BL(iR,:,:,:,:,:);
        end
    end
end

SALMI2dGroup_BL = squeeze(nanmean(SALMI2d,1));
SALphasepref2dGroup_BL = squeeze(nanmean(SALphasepref2d,1));
VPAMI2dGroup_BL = squeeze(nanmean(VPAMI2d,1));
VPAphasepref2dGroup_BL = squeeze(nanmean(VPAphasepref2d,1));

for group = 1:numel(treatment)
    if group == 1
        data2plot = SALMI2dGroup_BL;
        grp = 'SAL';
    elseif group == 2
        data2plot = VPAMI2dGroup_BL;
        grp = 'VPA';
    end
    for iDay = 1:numel(phases)
        for pr = 1:2
            if pr == 1
                prlabel = 'NAc';
            else
                prlabel = 'BLA';
            end
            for ar = 1:2
                if ar == 1
                    arlabel = 'NAc';
                else
                    arlabel = 'BLA';
                end
                clf
                obj2plot = squeeze(data2plot(iDay,pr,ar,:,:));
                imagesc(obj2plot(foiPhase2Plot,foiAmp2Plot)','XData',foi_phase(foiPhase2Plot),'YData',foi_amp(foiAmp2Plot))
%                 imagesc(obj2plot(foiPhase2Plot,:),'XData',foi_phase(foiPhase2Plot),'YData',foi_amp)

                axis xy
                colormap jet
                colorbar
                caxis([0 3*10^(-3)]);
                
                set(gca, 'FontName', 'Arial', 'FontSize', 16);
                xlabel('Phase Frequency (Hz)');
                ylabel('Envelope Frequency (Hz)');
                
                title({[grp ' D' int2str(iDay)];[ ...
                    prlabel ' phase modulating ' ...
                    arlabel ' amplitude']; 'pre-tone Baseline'});
                if pr == 1 && ar == 1
                saveas(gcf,[figurefolder 'NAcmodNAc' filesep 'BL_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                elseif pr == 1 && ar == 2
                saveas(gcf,[figurefolder 'NAcmodBLA' filesep 'BL_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                elseif pr == 2 && ar == 1
                saveas(gcf,[figurefolder 'BLAmodNAc' filesep 'BL_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                elseif pr == 2 && ar == 2
                saveas(gcf,[figurefolder 'BLAmodBLA' filesep 'BL_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                end
            end
        end
    end
end

%% calculate pre-tone normalized MI2d and phasepref
MI2dAbsNorm = MI2dFull - MI2dFull_BL;
MI2dPctNorm = MI2dFull ./ MI2dFull_BL;

%% plot pre-tone normalized MI2d and phasepref


SALMI2dAbsNorm = NaN(SALsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));
SALMI2dPctNorm = NaN(SALsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));

VPAMI2dAbsNorm = NaN(VPAsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));
VPAMI2dPctNorm = NaN(VPAsize,numel(phases),2,2,numel(foi_phase),numel(foi_amp));

for iR = 1:height(ratT)
    gr = ratT.groups{iR};
    for iDay = 1:numel(phases)
        if strcmp(gr, 'VPA') ==1
            grID = sum(VPAInd(1:iR));
            VPAMI2dAbsNorm(grID,:,:,:,:,:) = ...
                MI2dAbsNorm(iR,:,:,:,:,:);
            VPAMI2dPctNorm(grID,:,:,:,:,:) = ...
                MI2dPctNorm(iR,:,:,:,:,:);
        elseif strcmp(gr, 'Saline') ==1
            grID = sum(SALInd(1:iR));
            SALMI2dAbsNorm(grID,:,:,:,:,:) = ...
                MI2dAbsNorm(iR,:,:,:,:,:);
            SALMI2dPctNorm(grID,:,:,:,:,:) = ...
                MI2dPctNorm(iR,:,:,:,:,:);
        end
    end
end


outputfile = [datafolder filesep 'FullData' filesep 'CFC' filesep ...
                    'crossKLMI2d_durTone200msTrim_Normalized.mat'];

save(outputfile, 'SALMI2dAbsNorm', 'SALMI2dPctNorm', ...
    'VPAMI2dAbsNorm','VPAMI2dPctNorm','axisLabel','foi_phase','foi_amp','w_phase','w_amp')



SALMI2dAbsNorm = squeeze(nanmean(SALMI2dAbsNorm,1));
SALMI2dPctNorm = squeeze(nanmean(SALMI2dPctNorm,1));
VPAMI2dAbsNorm = squeeze(nanmean(VPAMI2dAbsNorm,1));
VPAMI2dPctNorm = squeeze(nanmean(VPAMI2dPctNorm,1));

for group = 1:numel(treatment)
    if group == 1
        data2plot1 = SALMI2dAbsNorm;
        data2plot2 = SALMI2dPctNorm;

        grp = 'SAL';
    elseif group == 2
        data2plot1 = VPAMI2dAbsNorm;
        data2plot2 = VPAMI2dPctNorm;
        grp = 'VPA';
    end
    for iDay = 1:numel(phases)
        for pr = 1:2
            if pr == 1
                prlabel = 'NAc';
            else
                prlabel = 'BLA';
            end
            for ar = 1:2
                if ar == 1
                    arlabel = 'NAc';
                else
                    arlabel = 'BLA';
                end
                clf
                obj2plot1 = squeeze(data2plot1(iDay,pr,ar,:,:));
                imagesc(obj2plot1(foiPhase2Plot,foiAmp2Plot)','XData',foi_phase(foiPhase2Plot),'YData',foi_amp(foiAmp2Plot))

                axis xy
                colormap jet
                colorbar
                caxis([-25*10^(-4) 25*10^(-4)]);
                
                set(gca, 'FontName', 'Arial', 'FontSize', 16);
                xlabel('Phase Frequency (Hz)');
                ylabel('Envelope Frequency (Hz)');
                
                title({[grp ' D' int2str(iDay)];[ ...
                    prlabel ' phase modulating ' ...
                    arlabel ' amplitude'];['absolute change from pre-tone']});
                if pr == 1 && ar == 1
                saveas(gcf,[figurefolder 'NAcmodNAc' filesep 'AbsNorm_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                elseif pr == 1 && ar == 2
                saveas(gcf,[figurefolder 'NAcmodBLA' filesep 'AbsNorm_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                elseif pr == 2 && ar == 1
                saveas(gcf,[figurefolder 'BLAmodNAc' filesep 'AbsNorm_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                elseif pr == 2 && ar == 2
                saveas(gcf,[figurefolder 'BLAmodBLA' filesep 'AbsNorm_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                end
                
                clf
                obj2plot2 = squeeze(data2plot2(iDay,pr,ar,:,:));
                imagesc(obj2plot2(foiPhase2Plot,foiAmp2Plot)','XData',foi_phase(foiPhase2Plot),'YData',foi_amp(foiAmp2Plot))
                axis xy
                colormap jet
                colorbar
                caxis([0.5 2]);
                
                set(gca, 'FontName', 'Arial', 'FontSize', 16);
                xlabel('Phase Frequency (Hz)');
                ylabel('Envelope Frequency (Hz)');
                
                title({[grp ' D' int2str(iDay)];[ ...
                    prlabel ' phase modulating ' ...
                    arlabel ' amplitude'];'precentage change from pre-tone'});
                if pr == 1 && ar == 1
                saveas(gcf,[figurefolder 'NAcmodNAc' filesep 'PctNorm_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                elseif pr == 1 && ar == 2
                saveas(gcf,[figurefolder 'NAcmodBLA' filesep 'PctNorm_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                elseif pr == 2 && ar == 1
                saveas(gcf,[figurefolder 'BLAmodNAc' filesep 'PctNorm_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                elseif pr == 2 && ar == 2
                saveas(gcf,[figurefolder 'BLAmodBLA' filesep 'PctNorm_KLMI' grp '_D' int2str(iDay) ...
                    prlabel 'mod ' arlabel 'amp.png'])
                end
                
                
            end
        end
    end
end


%% calculate Net modulation: (BLA->NAc) - (NAc->BLA)
MI2dNet = squeeze(MI2dFull(:,:,2,1,:,:)-MI2dFull(:,:,1,2,:,:));

SALMI2dNet = NaN(SALsize,numel(phases),numel(foi_phase),numel(foi_amp));
VPAMI2dNet = NaN(VPAsize,numel(phases),numel(foi_phase),numel(foi_amp));

for iR = 1:height(ratT)
    gr = ratT.groups{iR};
    for iDay = 1:numel(phases)
        if strcmp(gr, 'VPA') ==1
            grID = sum(VPAInd(1:iR));
            VPAMI2dNet(grID,:,:,:) = ...
                MI2dNet(iR,:,:,:);
        elseif strcmp(gr, 'Saline') ==1
            grID = sum(SALInd(1:iR));
            SALMI2dNet(grID,:,:,:) = ...
                MI2dNet(iR,:,:,:);
        end
    end
end


SALMI2dNet = squeeze(nanmean(SALMI2dNet,1));
VPAMI2dNet = squeeze(nanmean(VPAMI2dNet,1));

for group = 1:numel(treatment)
    if group == 1
        data2plot = SALMI2dNet;
        grp = 'SAL';
    elseif group == 2
        data2plot = VPAMI2dNet;
        grp = 'VPA';
    end
    for iDay = 1:numel(phases)
        
        clf
        obj2plot = squeeze(data2plot(iDay,:,:));
        imagesc(obj2plot(foiPhase2Plot,foiAmp2Plot)','XData',foi_phase(foiPhase2Plot),'YData',foi_amp(foiAmp2Plot))     
        axis xy
        colormap jet
        colorbar
        caxis([-15*10^(-4) 15*10^(-4)]);
        
        set(gca, 'FontName', 'Arial', 'FontSize', 16);
        xlabel('Phase Frequency (Hz)');
        ylabel('Envelope Frequency (Hz)');
        
        title({[grp ' D' int2str(iDay)]; ...
            ' NAc to BLA net modulation' });
            saveas(gcf,[figurefolder 'NetNAc2BLA_KLMI' grp '_D' int2str(iDay) ...
                '.png'])     
    end
end

%% calculate average
foiPhase2Cal = find(foi_phase >= 5 & foi_phase <= 8);
foiAmp2Cal = find(foi_amp >= 70 & foi_amp <= 110);

SALNAcNAcAbsNorm = squeeze(SALMI2dAbsNorm(:,:,1,1,foiPhase2Cal,foiAmp2Cal));
VPANAcNAcAbsNorm = squeeze(VPAMI2dAbsNorm(:,:,1,1,foiPhase2Cal,foiAmp2Cal));

SALNAcNAcAbsNorm = mean(mean(SALNAcNAcAbsNorm,4),3);
VPANAcNAcAbsNorm = mean(mean(VPANAcNAcAbsNorm,4),3);