%% calculates 2d cross-frequency phase-amplitude modulation
% for each trials of food conditioning across multiple rats

close all; clearvars; clc;  % clean slate

info_VPA_RC                 % load all background info (filenames, etc)

plt = 'y';                  % plot CFC
hx = waitbar(0,'Getting started...');

%% set analysis parameters
foi_phase = 2:0.25:15.0;  % central frequencies for phase modulation
w_phase = 0.5;              % width of phase band (foi +/- w)
foi_amp = 12:3:132;         % central frequencies for modulated amplitude
w_amp = 6;                  % width of amplitude band (foi +/- w)

%% preallocate variables for logging trial info
trials2Cal = NaN(size(ratT,1),size(abbr,2));
trialswArt = cell(size(ratT,1),size(abbr,2)); % the ID trials with artifact

%% make 2d Cross-Frequency Modulation plots for each rat/session/trial

for iR = 1:size(ratT,1)
    
    inputfile = [datafolder  'fcLFPTrialData_poststim5sw500msDelay_' ...
    int2str(ratT.ratnums(iR)) 'Merged.mat'];
    
    if ~exist(inputfile,'file')
        error('inputfile does not exist');
    end
    
    outputfile = [datafolder 'crossKLMI2d_poststim5sw500msDelay'...
        num2str(ratT.ratnums(iR)) '.mat'];
    
    if exist(outputfile,'file')
        disp(['Data for Rat # ' int2str(ratT.ratnums(iR)) ', ' ...
            ' already analyzed!']);
        if plt == 'y'
            disp('Loading previous analysis results from file.');
            load(outputfile);
        end
        
    else
        
        %% preallocate memory for variables
        phasepref2d = cell(size(abbr,2),TotalTrial);
        MI2d = cell(size(abbr,2),TotalTrial);
        %         cmax = zeros(size(abbr,2),TotalTrial);
        
        %% load data
        load(inputfile)
        
        %% identify channels to compute
        chNAc = ratT{iR,4};
        chNAcIdx = find(strcmp(data.label,chNAc)==1);
        chNAc = char(chNAc);
        chNAc = str2double(chNAc(3:4));
        
        chBLA = ratT{iR,5};
        chBLAIdx = find(strcmp(data.label,chBLA)==1);
        chBLA = char(chBLA);
        chBLA = str2double(chBLA(3:4));
        
        for iDay = 1:size(abbr,2)
            %% Count trials to include
            trialIdx = data.trialinfo((data.trialinfo(:,2)==iDay),1);
            dtrialIdx = diff(trialIdx);
            trialswArt{iR,iDay} = unique(trialIdx(dtrialIdx==0));
            [~,trialIdx] = setdiff(trialIdx, unique(trialIdx(dtrialIdx==0)));
            trials2Cal(iR,iDay)= numel(trialIdx);
            trialIdx = trialIdx + find(data.trialinfo(:,2)==iDay,1) -1;
            
            %% waitbar
            waitbar(iDay/size(abbr,2),hx,...
                {['Calculating 2d cross-region MI z-score for'];...
                ['Rat #' num2str(ratT.ratnums(iR))]});
            
            %% analyze data
            parfor iTrl = 1:numel(trialIdx)
                
                % compute CFC for all the trials within a phase
                [~,~,MI2dtemp,phasepref2dtemp] = crossKLMI2d_JH(...
                    data.trial{trialIdx(iTrl)}(chNAcIdx,:),...
                    data.trial{trialIdx(iTrl)}(chBLAIdx,:),...
                    foi_phase,w_phase,foi_amp,w_amp);
                
                for pr = 1:2 % each phase modulating region
                    for ar = 1:2 % each amp modulated region
                        MI2d{iDay,iTrl,1}{pr,ar}(:,:) = ...
                            MI2dtemp{pr,ar};
                        phasepref2d{iDay,iTrl,1}{pr,ar}(:,:) = ...
                            phasepref2dtemp{pr,ar};
                    end
                end
            end
        end
        
        %% compute cmax for plotting
        
        cmax = -100; % an impossible value for MI
        
        for iDay = 1:numel(abbr)
            for iTrl = 1:TotalTrial
                if isempty(MI2d{iDay,iTrl})
                else
                    for pr = 1:2 % each phase modulating region
                        for ar = 1:2 % each amp modulated region
                            cmax = max(max(max(nanmean(MI2d{iDay,iTrl}{pr,ar}))),cmax);
                        end
                    end
                end
            end
        end
        
        %% reorganize MI2d for plotting and stats
        MI2dStack = cell(numel(abbr),1);
        phasePref2dStack = cell(numel(abbr),1);
        for iDay = 1:numel(abbr)
            nTrial = trials2Cal(iR,iDay);
            for iTrl = 1:nTrial
                for pr = 1:2 % each phase modulating region
                    for ar = 1:2 % each amp modulated region
                        MI2dStack{iDay}{pr,ar}(:,:,iTrl) = MI2d{iDay,iTrl}{pr,ar};
                        phasePref2dStack{iDay}{pr,ar}(:,:,iTrl) = phasepref2d{iDay,iTrl}{pr,ar};
                    end
                end
            end
        end
        
        %% save analysis results
        disp(['writing results to file ' outputfile]);
        save(outputfile, 'MI2d*', 'phase*', 'cmax*', 'foi*', 'w*', 'trials*', '-v6');
    end
    
    %% Remove foi_phase under 2 Hz
    freq2Exclude = foi_phase(foi_phase <2);
    foi_phase = foi_phase(numel(freq2Exclude)+1:end);
    for iDay = 1:numel(abbr)
        for pr = 1:2 % each phase modulating region
            for ar = 1:2 % each amp modulated region
                
                MI2dStack{iDay}{pr,ar} = ...
                    MI2dStack{iDay}{pr,ar}...
                    (numel(freq2Exclude)+1:end,:,:);
                phasePref2dStack{iDay}{pr,ar} = ...
                    phasePref2dStack{iDay}{pr,ar}...
                    (numel(freq2Exclude)+1:end,:,:);
            end
        end
    end
   
    
    %% Recalculate cmax
    
    cmax = -100; % an impossible value for MI
    
    for iDay = 1:numel(abbr)
        for iTrl = 1:TotalTrial
            if isempty(MI2d{iDay,iTrl})
            else
                for pr = 1:2 % each phase modulating region
                    for ar = 1:2 % each amp modulated region
                        cmax = max(max(max(nanmean(MI2d{iDay,iTrl}{pr,ar}))),cmax);
                    end
                end
            end
        end
    end
    
    %% plot graphs
    if plt == 'y'
        for pr = 1:2 % each phase modulating region
            if pr == 1
                prlabel = 'NAc';
            else
                prlabel = 'BLA';
            end
            
            for ar = 1:2 % each amp modulated region
                if ar == 1
                    arlabel = 'NAc';
                else
                    arlabel = 'BLA';
                end
                
                figure(2*(ar-1)+pr);
                set(gcf,'WindowStyle','docked');
                
                for iDay = 1:numel(abbr)
                    %% plot modulation index
                    subplot(3,3,iDay)
                    imagesc(foi_phase, foi_amp, mean(...
                        MI2dStack{iDay}{pr,ar}(:,:,...
                        1:trials2Cal(iR,iDay)),3)');
                    axis xy;
                    set(gca, 'FontName', 'Arial', 'FontSize', 8);
                    xlabel('Phase Frequency (Hz)');
                    ylabel('Envelope Frequency (Hz)');
                    title([ 'D' int2str(iDay) ', '...
                        num2str(trials2Cal(iR,iDay)) ' trials']);
                    caxis([0 cmax]);
                    pos = get(gca,'position');
                    pos(3)= 0.23;
                    set(gca,'Position',pos)
                    
                    colormap(jet);
                    if iDay == 9
                        colorbar
                        cb = get(colorbar,'Position');
                        cb(3) = 0.01;
                        set(colorbar,'Position',cb)
                    end
                end
                suptitle(['Rat #' num2str(ratT.ratnums(iR)) ', ' ...
                    prlabel ' phase modulating ' ...
                    arlabel ' amplitude KL-modulation index'])
                saveas(gcf,[figurefolder 'KL_MI2d_'...
                    num2str(ratT.ratnums(iR)) '_' prlabel 'PhaseMod' ...
                    arlabel 'Amp.png'])
                
%                 %% plot phase preference
%                 
%                 figure(2*(ar-1)+pr+4);
%                 for iDay = 1:numel(abbr)
%                     subplot(3,3,iDay)
%                     imagesc(foi_phase, foi_amp, circ_mean(circ_ang2rad(...
%                         phasePref2dStack{iDay}{pr,ar}(:,:, ...
%                         1:trials2Cal(iR,iDay))),[],3)');
%                     axis xy;
%                     set(gca, 'FontName', 'Arial', 'FontSize', 11);
%                     xlabel('Phase Frequency (Hz)');
%                     ylabel('Envelope Frequency (Hz)');
%                     title([ 'D' int2str(iDay) ', '...
%                         num2str(trials2Cal(iR,iDay)) ' trials']);
%                     caxis([-pi-0.1 pi]); colormap(hsv);
%                     
%                     if iDay == 9
%                         colorbar
%                         cb = get(colorbar,'Position');
%                         cb(3) = 0.01;
%                         set(colorbar,'Position',cb)
%                     end
%                 end
%                 
%                 set(gcf,'WindowStyle','docked');
%                 suptitle(['Phase Preference Rat #' num2str(ratT.ratnums(iR)) ', ' ...
%                     prlabel ' phase modulating ' ...
%                     arlabel ' amplitude'])
%                 saveas(gcf,[figurefolder 'phaseKL_MI2d_'...
%                     num2str(ratT.ratnums(iR)) '_' prlabel 'PhaseMod' ...
%                     arlabel 'Amp.png'])
            end
        end
    end
    
end

close(hx)
