%% calculates 2d cross-frequency phase-amplitude modulation
% for each trials of food conditioning across multiple rats

close all; clearvars; clc;  % clean slate

info_VPA_RC                 % load all background info (filenames, etc)

plt = 'y';                  % plot CFC
hx = waitbar(0,'Getting started...');

%% set analysis parameters
foi_phase = 3:0.25:18.0;  % central frequencies for phase modulation
w_phase = 0.12;              % width of phase band (foi +/- w)
foi_amp = 9:3:141;         % central frequencies for modulated amplitude
w_amp = 3;                  % width of amplitude band (foi +/- w)

%% preallocate variables for logging trial info
trials2Cal = NaN(size(ratT,1),size(abbr,2));
trialswArt = cell(size(ratT,1),size(abbr,2)); % the ID trials with artifact

%% make 2d Cross-Frequency Modulation plots for each rat/session/trial

for iR = 1:size(ratT,1)
    for dd = 1:numel(phases)
        if ~iscell(nexfile{iR,dd})  % in case of multiple files per phase
            nexfile{iR,dd} = nexfile(iR,dd);  % convert to cellstr
        end
        trialNum = zeros(numel(nexfile{iR,dd}),1);
        for fn = 1:numel(nexfile{iR,dd})  % file number w/in a phase
            
            inputfile = [datafolder filesep 'FullData' filesep 'clean' filesep ...
                'CleanLFPTrialData_' int2str(ratT.ratnums(iR)) phases{dd} num2str(fn) 'CFCdurTone.mat'];
            if ~exist(inputfile,'file')
                error('inputfile does not exist');
            end
            if numel(nexfile{iR,dd}) == 1
            outputfile = [datafolder filesep 'FullData' filesep 'CFC' filesep ...
                'crossKLMI2d_durTone200msTrim_NarrowWidth' int2str(ratT.ratnums(iR)) phases{dd} '_Full.mat'];
            else
            outputfile = [datafolder filesep 'FullData' filesep 'CFC' filesep ...
                'crossKLMI2d_durTone200msTrim_NarrowWidth' int2str(ratT.ratnums(iR)) phases{dd} num2str(fn) '.mat'];    
            end
            
            if exist(outputfile,'file')
                disp(['Data for Rat # ' int2str(ratT.ratnums(iR)) ', ' ...
                    ' already analyzed!']);
                if plt == 'y'
                    disp('Loading previous analysis results from file.');
                    load(outputfile);
                end
                
            else
                
                
                %% load data
                load(inputfile)
                
                %% preallocate memory for variables
                phasepref2d = NaN(2,2,numel(data.trial),...
                    numel(foi_phase),numel(foi_amp));
                MI2d =  NaN(2,2,numel(data.trial),...
                    numel(foi_phase),numel(foi_amp));
                %         cmax = zeros(size(abbr,2),TotalTrial);
                
                %% identify channels to compute
                chNAc = ratT.chNAc{iR};
                chNAcIdx = find(strcmp(data.label,chNAc)==1);
                chNAc = char(chNAc);
                chNAc = str2double(chNAc(3:4));
                
                chBLA = ratT.chBLA{iR};
                chBLAIdx = find(strcmp(data.label,chBLA)==1);
                chBLA = char(chBLA);
                chBLA = str2double(chBLA(3:4));
                
                
                %% analyze data
                parfor iTrl = 1:numel(data.trial)
                    
                    % compute CFC for all the trials within a phase
                    [~,~,MI2dtemp,phasepref2dtemp] = crossKLMI2d_JH(...
                        data.trial{iTrl}(chNAcIdx,:),...
                        data.trial{iTrl}(chBLAIdx,:),...
                        foi_phase,w_phase,foi_amp,w_amp);
                    
                    for pr = 1:2 % each phase modulating region
                        for ar = 1:2 % each amp modulated region
                            MI2d(pr,ar,iTrl,:,:) = ...
                                MI2dtemp{pr,ar};
                            phasepref2d(pr,ar,iTrl,:,:) = ...
                                phasepref2dtemp{pr,ar};
                        end
                    end
                end
                axisLabel = {'phase modulating', 'amp modulated' ...
                    'trial', 'foi_phase', 'foi_amp'};
                
                save(outputfile, 'MI2d','phasepref2d','axisLabel',...
                    'foi_phase','foi_amp','w_amp','w_phase')
%                 close(hx)
            end
        end
    end
end