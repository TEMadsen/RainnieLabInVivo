% This script normalizes KL-MI of CFC during tone by pretone KL-MI in food
% conditioning test

%% housekeeping
close all; clearvars; clc;  % clean slate

info_VPA_RC                 % load all background info (filenames, etc)

plt = 'n';                  % plot CFC

%% preallocate space
crossKLMI2DivNorm = cell(height(ratT),1);
crossKLMI2SubNorm = cell(height(ratT),1);

outputfile = [datafolder 'FC' filesep 'crossKLMI2dNormalized.mat'];
%% load files
for ir = 1:size(ratT,1)
    
    inputfile1 = [datafolder 'crossKLMI2dPretone_'...
        num2str(ratT.ratnums(ir)) '.mat'];
    inputfile2 = [datafolder 'crossKLMI2d_'...
        num2str(ratT.ratnums(ir)) '.mat'];
    
    dataPre = struct;
    dataDur = struct;
    
    load(inputfile1)
    
    dataPre.cmax = cmax;
    dataPre.foi_amp = foi_amp;
    dataPre.foi_phase =foi_phase;
    dataPre.MI2d = MI2d;
    dataPre.MI2dStack = MI2dStack;
    dataPre.phasepref2d = phasepref2d;
    dataPre.phasePref2dStack = phasePref2dStack;
    dataPre.phases = phases;
    dataPre.trials2Cal = trials2Cal;
    dataPre.trialswArt = trialswArt;
    dataPre.w_amp = w_amp;
    dataPre.w_phase =w_phase;
    
    clear cmax foi_amp foi_phase MI2d MI2dStack ...
        phasepref2d phasePref2dStack phases trials2Cal ...
        trialswArt w_amp w_phase
    
    load(inputfile2)
    
    dataDur.cmax = cmax;
    dataDur.foi_amp = foi_amp;
    dataDur.foi_phase =foi_phase;
    dataDur.MI2d = MI2d;
    dataDur.MI2dStack = MI2dStack;
    dataDur.phasepref2d = phasepref2d;
    dataDur.phasePref2dStack = phasePref2dStack;
    dataDur.phases = phases;
    dataDur.trials2Cal = trials2Cal;
    dataDur.trialswArt = trialswArt;
    dataDur.w_amp = w_amp;
    dataDur.w_phase =w_phase;
    
    clear cmax MI2d MI2dStack ...
        phasepref2d phasePref2dStack phases trials2Cal ...
        trialswArt w_amp w_phase
    
    if sum(dataPre.foi_amp == dataDur.foi_amp) == numel(dataPre.foi_amp) && ...
            sum(dataPre.foi_phase == dataDur.foi_phase) == numel(dataPre.foi_phase) &&...
            dataPre.w_amp == dataDur.w_amp &&...
            dataPre.w_phase == dataDur.w_phase
        disp ('foi and smoothing width matched. continue')
    else
        error(['foi and smoothing width does not match.' ...
            'Please check!'])
    end
    foi_phase = dataDur.foi_phase;
    
    %% compute normalized KL-MI by day
    
    MI2dSubtractNorm = cell(numel(abbr),1);
    MI2dDivideNorm = cell(numel(abbr),1);
    
    freq2Exclude = foi_phase(foi_phase <2);
    foi_phase = foi_phase(numel(freq2Exclude)+1:end);
    
    for iDay = 1:numel(abbr)
        for pr = 1:2 % each phase modulating region
            for ar = 1:2 % each amp modulated region
                MI2dSubtractNorm{iDay}{pr,ar} = mean(...
                    dataDur.MI2dStack{iDay}{pr,ar}(:,:,...
                    1:dataDur.trials2Cal(ir,iDay)),3)'-...
                    mean(dataPre.MI2dStack{iDay}{pr,ar}(:,:,...
                    1:dataPre.trials2Cal(ir,iDay)),3)';
                
                MI2dDivideNorm{iDay}{pr,ar} = mean(...
                    dataDur.MI2dStack{iDay}{pr,ar}(:,:,...
                    1:dataDur.trials2Cal(ir,iDay)),3)'./...
                    mean(dataPre.MI2dStack{iDay}{pr,ar}(:,:,...
                    1:dataPre.trials2Cal(ir,iDay)),3)';
                % remove data under 2 Hz
                MI2dSubtractNorm{iDay}{pr,ar} = ...
                    MI2dSubtractNorm{iDay}{pr,ar}...
                    (:,numel(freq2Exclude)+1:end);
                MI2dDivideNorm{iDay}{pr,ar} = ...
                    MI2dDivideNorm{iDay}{pr,ar}...
                    (:,numel(freq2Exclude)+1:end);
            end
        end
    end
    
    crossKLMI2DivNorm{ir,1} = MI2dDivideNorm;
    crossKLMI2DivNorm{ir,1} = MI2dSubtractNorm;
    %% calculate cmax and cmin
    cmaxSub = -100; % an impossible value for MI to start with
    cminSub = 100; % an impossible value for MI to start with
    cmaxDev = -100; % an impossible value for MI to start with
    cminDev = 100; % an impossible value for MI to start with
    
    
    for iDay = 1:numel(abbr)
        for iTrl = 1:TotalTrial
            for pr = 1:2 % each phase modulating region
                for ar = 1:2 % each amp modulated region
                    cmaxSub = max(max(max(max(MI2dSubtractNorm{iDay}{pr,ar}))),cmaxSub);
                    cmaxDev = max(max(max(max(MI2dDivideNorm{iDay}{pr,ar}))),cmaxDev);
                    cminSub = min(min(min(min(MI2dSubtractNorm{iDay}{pr,ar}))),cminSub);
                    cminDev = min(min(min(min(MI2dDivideNorm{iDay}{pr,ar}))),cminDev);
                end
            end
        end
    end
    
    %% plot the result
    if plt == 'y'
        for pr = 1:2 % each phase modulating region
            if pr == 1
                prlabel = 'NAc';
            else
                prlabel = 'BLA';
            end
            
            for ar = 1:2 % each amp modulated region
                if ar == 1
                    arlabel = 'NAc';
                else
                    arlabel = 'BLA';
                end
                
                figure(2*(ar-1)+pr);
                set(gcf,'WindowStyle','docked');
                
                for iDay = 1:numel(abbr)
                    %% plot KL-MI normalized by substraction
                    subplot(3,3,iDay)
                    imagesc(foi_phase, foi_amp, MI2dSubtractNorm{iDay}{pr,ar});
                    axis xy;
                    set(gca, 'FontName', 'Arial', 'FontSize', 8);
                    xlabel('Phase Frequency (Hz)');
                    ylabel('Envelope Frequency (Hz)');
                    title([ 'D' int2str(iDay)]);
                    caxis([-cmaxSub cmaxSub]);
                    %                     pos = get(gca,'position');
                    %                     pos(3)= 0.23;
                    %                     set(gca,'Position',pos)
                    
                    colormap(jet);
                    if iDay == 9
                        colorbar
                        cb = get(colorbar,'Position');
                        cb(3) = 0.01;
                        set(colorbar,'Position',cb)
                    end
                end
                suptitle({['Rat #' num2str(ratT.ratnums(ir)) ', ' ...
                    prlabel ' phase modulating ' ...
                    arlabel ' amplitude KL-modulation index'], ...
                    'normalized by substarction to pretone'})
                saveas(gcf,[figurefolder 'KL_MI2d_SubNor'...
                    num2str(ratT.ratnums(ir)) '_' prlabel 'PhaseMod' ...
                    arlabel 'Amp.png'])
                
                %% plot KL-MI normalized by division
                
                figure(2*(ar-1)+pr+4);
                for iDay = 1:numel(abbr)
                    subplot(3,3,iDay)
                    imagesc(foi_phase, foi_amp, MI2dDivideNorm{iDay}{pr,ar});
                    axis xy;
                    set(gca, 'FontName', 'Arial', 'FontSize', 11);
                    xlabel('Phase Frequency (Hz)');
                    ylabel('Envelope Frequency (Hz)');
                    title([ 'D' int2str(iDay)]);
                    caxis([0 cmaxDev]);
                    colormap(jet);
                    
                    if iDay == 9
                        colorbar
                        cb = get(colorbar,'Position');
                        cb(3) = 0.01;
                        set(colorbar,'Position',cb)
                    end
                end
                
                set(gcf,'WindowStyle','docked');
                suptitle({['Rat #' num2str(ratT.ratnums(ir)) ', ' ...
                    prlabel ' phase modulating ' ...
                    arlabel ' amplitude KL-modulation index'], ...
                    'normalized by division to pretone'})
                saveas(gcf,[figurefolder 'KL_MI2d_DivNor'...
                    num2str(ratT.ratnums(ir)) '_' prlabel 'PhaseMod' ...
                    arlabel 'Amp.png'])
            end
        end
    end
end

save(outputfile,'crossKLMI2DivNorm','crossKLMI2DivNorm')