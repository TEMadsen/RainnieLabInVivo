close all force; clearvars; clc;  % clean slate

info_VPA_RC                       % load all background info (filenames, etc)

foi_phase = 2:0.25:15.0;  % central frequencies for phase modulation
w_phase = 0.5;              % width of phase band (foi +/- w)
foi_amp = 12:4:130;         % central frequencies for modulated amplitude
w_amp = 8;                  % width of amplitude band (foi +/- w)


load([datafolder 'crossKLMI2d.mat'])

%% preallocate space

SALCFCsubNorm = NaN(sum(SALInd),numel(phases),2,2,numel(foi_amp),numel(foi_phase));
VPACFCsubNorm = NaN(sum(VPAInd),numel(phases),2,2,numel(foi_amp),numel(foi_phase));

for iR = 1:height(ratT)
    gr = ratT{iR,7};
    for iDay = 1:numel(phases)
        if strcmp(gr, 'VPA') ==1
            grID = sum(VPAInd(1:iR));
            for pr = 1:2
                for ar = 1:2
                    VPACFCsubNorm(grID,iDay,pr,ar,:,:) = ...
                        MI2dSubNorGroup{iR,1}{iDay,1}{pr,ar};
                end
            end
            
        elseif strcmp(gr, 'Saline') ==1
            grID = sum(SALInd(1:iR));
            for pr = 1:2
                for ar = 1:2
                    SALCFCsubNorm(grID,iDay,pr,ar,:,:) = ...
                        MI2dSubNorGroup{iR,1}{iDay,1}{pr,ar};
                end
            end
        end
    end
end

SALgroupCFCsubNorm = squeeze(mean(SALCFCsubNorm,1));
VPAgroupCFCsubNorm = squeeze(mean(VPACFCsubNorm,1));

for group = 1:numel(treatment)
    if group == 1
        data2plot = SALgroupCFCsubNorm;
        grp = 'SAL';
    elseif group == 2
        data2plot = VPAgroupCFCsubNorm;
                grp = 'VPA';
    end
    for iDay = 1:numel(phases)
        for pr = 1:2
            if pr == 1
                prlabel = 'NAc';
            else
                prlabel = 'BLA';
            end
            for ar = 1:2
                if ar == 1
                    arlabel = 'NAc';
                else
                    arlabel = 'BLA';
                end
                clf
                obj2plot = squeeze(data2plot(iDay,pr,ar,:,:));
                imagesc(obj2plot','XData',foi_phase,'YData',foi_amp)
                axis xy
                colormap jet
                colorbar
                set(gca, 'FontName', 'Arial', 'FontSize', 16);
                xlabel('Phase Frequency (Hz)');
                ylabel('Envelope Frequency (Hz)');
                title({[grp ' D' int2str(iDay)];[ ...
                     prlabel ' phase modulating ' ...
                    arlabel ' amplitude']});
                saveas(gcf,[figurefolder 'KLMI' grp '_D' int2str(iDay) ...
                     prlabel 'mod ' arlabel 'amp_Norm.png'])
            end
        end
    end
end
