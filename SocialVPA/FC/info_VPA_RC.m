%% info needed for analyzing fear data, including all rats filenames
%#ok<*SNASGU> just provides background info for other analysis protocols

datafolder    = [tserver 'Analyses' filesep 'TempDataVPA' filesep];
configfolder  = [tserver 'Analyses' filesep 'MWAconfigs' filesep];

% put new figures in a folder named by date, in the format 'yyyy-mm-dd'
figurefolder  = [tserver 'Figures' filesep 'TempFiguresVPA' filesep ...
 datestr(now,29) filesep];
if ~exist(figurefolder,'dir')
  mkdir(figurefolder)
end

%% define MWA configuration(s)

% MWA = '16chSocialRight';      % name for this microwire array configuration
% 
% lftchs = {'AD01','AD05'; ...  % mPFC array configuration, where increasing
%   'AD02','AD06'; ...  % rows indicate electrodes ordered A -> P,
%   'AD03','AD07'; ...  % and increasing columns indicate those
%   'AD04','AD08'};     % ordered L -> R
% 
% rtchs = {'AD09','AD13'; ...   % BLA array configuration, where increasing
%   'AD10','AD14'; ...   % rows indicate electrodes ordered A -> P,
%   'AD11','AD15'; ...   % and increasing columns indicate those
%   'AD12','AD16'};      % ordered L -> R
% 
% if exist([configfolder 'elec_' MWA '_Right.mat'],'file') && ...
%     exist([configfolder 'layout_' MWA '_Right.mat'],'file')
%   disp('using existing elec & layout files')
% else
%   cfg = [];   % all arrays were configured & implanted the same for this expt
%   
%   % array & target info
%   cfg.atlascoord = [1.7, 1.60, -7.20; ... 	% AP/ML/DV relative to bregma
%     -3.0, 4.80, -8.60];      % dim 1/2/3 for below
%   cfg.angle     = [-6; 6];  % + angle means approach from medial, in degrees
%   cfg.angledim  = [2; 2];   % both probes angled along ML dim
%   cfg.elecspac  = [0.25, 0.25; ...  % in AP dim, then ML dim, for each probe
%     0.25, 0.25];      % in mm
%   
%   cfg.label = {lftchs; rtchs}; 	% 2 cell arrays nested in another cell array
%   cfg.side = {'Right';'Right'}; % which side of the brain each probe was on
%   
%   % calculate sensor positions & define layout
%   MWAlayout(cfg,[MWA '_Right'],configfolder);
% end
% 
% % left
% MWA = '16chSocialLeft';      % name for this microwire array configuration
% 
% lftchs = {'AD01','AD05'; ...  % BLA array configuration, where increasing
%   'AD02','AD06'; ...  % rows indicate electrodes ordered A -> P,
%   'AD03','AD07'; ...  % and increasing columns indicate those
%   'AD04','AD08'};     % ordered L -> R
% 
% rtchs = {'AD09','AD13'; ...   % NAc array configuration, where increasing
%   'AD10','AD14'; ...   % rows indicate electrodes ordered A -> P,
%   'AD11','AD15'; ...   % and increasing columns indicate those
%   'AD12','AD16'};      % ordered L -> R
% 
% if exist([configfolder 'elec_' MWA '_Left.mat'],'file') && ...
%     exist([configfolder 'layout_' MWA '_Left.mat'],'file')
%   disp('using existing elec & layout files')
% else
%   cfg = [];   % all arrays were configured & implanted the same for this expt
%   
%   % array & target info
%   cfg.atlascoord = [ -3.0, 4.80, -8.60; ... 	% AP/ML/DV relative to bregma
%      1.7, 1.60, -7.20];      % dim 1/2/3 for below
%   cfg.angle     = [6; -6];  % + angle means approach from medial, in degrees
%   cfg.angledim  = [2; 2];   % both probes angled along ML dim
%   cfg.elecspac  = [0.25, 0.25; ...  % in AP dim, then ML dim, for each probe
%     0.25, 0.25];      % in mm
%   
%   cfg.label = {lftchs; rtchs}; 	% 2 cell arrays nested in another cell array
%   cfg.side = {'Left';'Left'}; % which side of the brain each probe was on
%   
%   % calculate sensor positions & define layout
%   MWAlayout(cfg,[MWA '_Left'],configfolder);
% end

MWA = '16chSocial';

%% info needed for analyzing RC data, including all rats filenames
% RC = food reinforcement conditioning
ratnums = [361; 362; 363; 364; 377; 378; 379; 380; 391; 394; ...
    395; 396; 397; 398; 416; 417; 418; 419; 458; 459; 460];
excluded = logical([0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; ...
    0; 0; 0; 0; 0; 0; 0;]);   % see notes for why
treatment = {'SAL';'VPA'};
groups = {'VPA';'Saline';'VPA';'Saline';'VPA';'Saline';'VPA';'Saline'; ...
    'VPA';'Saline';'VPA';'Saline';'VPA';'Saline'; ...
    'Saline';'VPA';'Saline';'VPA';'Saline';'VPA';'Saline'};
side = {'Right';'Left';'Left';'Right';'Right';'Left';'Left';'Right'; ...
    'Right';'Left';'Right';'Left';'Left';'Right'; ...
    'Right';'Right';'Left';'Left';'Right';'Right';'Left'};

% channels to use for preproc. 
preprocChan = cell(numel(ratnums),1); 

% default is to include all channels
for ii = 1:numel(preprocChan)
   preprocChan{ii} = 'AD*'; 
end

% Exclude any channel that was not active during any recording files.
preprocChan{9} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD16';'AD17';...
    'AD18';'AD19';'AD20';'AD21';'AD22';'AD23';'AD24';'AD25';'AD26';...
    'AD27';'AD28';'AD29';'AD30';'AD31';'AD32'};
preprocChan{10} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15';'AD16';'AD17';...
    'AD18';'AD19';'AD20';'AD21';'AD22';'AD23';'AD24';'AD25';'AD26';...
    'AD27';'AD28';'AD29';'AD31'};
preprocChan{11} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD11';'AD13';'AD14';'AD15';'AD16';'AD17';...
    'AD18';'AD19';'AD20';'AD21';'AD22';'AD23';'AD24';'AD25';'AD26';...
    'AD27';'AD28';'AD29';'AD30';'AD31'};
preprocChan{13} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD12';'AD13';'AD14';'AD15';'AD16';'AD17';...
    'AD18';'AD19';'AD21';'AD23';'AD24';'AD25';...
    'AD27';'AD28';'AD29';'AD30';'AD31'};
preprocChan{17} = {'AD01';'AD03';'AD04';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD12';'AD13';'AD14';'AD15';'AD16';'AD17';...
    'AD18';'AD19';'AD21';'AD22';'AD23';'AD24';'AD25';'AD26';...
    'AD31'};
preprocChan{20} = {'AD01';'AD02';'AD03';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15';'AD16';'AD17';...
    'AD18';'AD19';'AD20';'AD21';'AD22';'AD23';'AD24';'AD25';'AD26';...
    'AD27';'AD28';'AD29';'AD30';'AD31';'AD32'};

chNAc = cell(numel(ratnums),1);    % best channel to use for CFC
chBLA = cell(numel(ratnums),1);     % best channel to use for CFC
chNAcsh = cell(numel(ratnums),1);     % best channel to use for CFC
chHipp = cell(numel(ratnums),1);     % best channel to use for CFC
% chNAc{1} = 'AD02';
% chNAc{2} = 'AD13';
% chNAc{3} = 'AD12';
% chNAc{4} = 'AD07';
% chNAc{5} = 'AD06';
% chNAc{6} = 'AD13';
% chNAc{7} = 'AD12';
% chNAc{8} = 'AD07';
% 
% chBLA{1} = 'AD12';
% chBLA{2} = 'AD05';
% chBLA{3} = 'AD02';
% chBLA{4} = 'AD13';
% chBLA{5} = 'AD10';
% chBLA{6} = 'AD05';
% chBLA{7} = 'AD05';
% chBLA{8} = 'AD15';

chNAc{1} = 'AD05';
chNAc{2} = 'AD10';
chNAc{3} = 'AD13';
chNAc{4} = 'AD02';
chNAc{5} = 'AD01';
chNAc{6} = 'AD13';
chNAc{7} = 'AD13';
chNAc{8} = 'AD05';
chNAc{9} = 'AD21';
chNAc{10} = 'AD05';
chNAc{11} = 'AD21';
chNAc{12} = 'AD14';
chNAc{13} = 'AD03';
chNAc{14} = 'AD21';
chNAc{15} = 'AD21';
chNAc{16} = 'AD21';
chNAc{17} = 'AD05';
chNAc{18} = 'AD05';
chNAc{19} = 'AD18';
chNAc{20} = 'AD21';
chNAc{21} = 'AD05';

chBLA{1} = 'AD13';
chBLA{2} = 'AD02';
chBLA{3} = 'AD05';
chBLA{4} = 'AD10';
chBLA{5} = 'AD13';
chBLA{6} = 'AD05';
chBLA{7} = 'AD05';
chBLA{8} = 'AD13';
chBLA{9} = 'AD04';
chBLA{10} = 'AD29';
chBLA{11} = 'AD06';
chBLA{12} = 'AD24';
chBLA{13} = 'AD24';
chBLA{14} = 'AD04';
chBLA{15} = 'AD08';
chBLA{16} = 'AD11';
chBLA{17} = 'AD22';
chBLA{18} = 'AD24';
chBLA{19} = 'AD08';
chBLA{20} = 'AD08';
chBLA{21} = 'AD24';

chHipp{9} = 'AD14';
chHipp{10} = 'AD21';
chHipp{11} = 'AD14';
chHipp{12} = 'AD30';
chHipp{13} = 'AD19';
chHipp{14} = 'AD05';
chHipp{15} = 'AD14';
chHipp{16} = 'AD05';
chHipp{17} = 'AD23';
chHipp{18} = 'AD28';
chHipp{19} = 'AD05';
chHipp{20} = 'AD14';
chHipp{21} = 'AD21';

chNAcsh{10} = 'AD01';
chNAcsh{11} = 'AD23';
chNAcsh{12} = 'AD11';
chNAcsh{13} = 'AD07';
chNAcsh{14} = 'AD20';
chNAcsh{15} = 'AD20';
chNAcsh{16} = 'AD20';
chNAcsh{17} = 'AD01';
chNAcsh{18} = 'AD16';
chNAcsh{19} = 'AD26';
chNAcsh{20} = 'AD20';
chNAcsh{21} = 'AD10';

finalch = false([numel(ratnums),1]);
ratT = table(ratnums, excluded, side, chNAc, chBLA,chNAcsh,chHipp, finalch, groups);

VPAInd = false(size(groups));
SALInd = false(size(groups));

for r = 1:numel(ratnums)
    VPAInd(r,1) = strcmp(ratT.groups{r},'VPA');  
end


for r = 1:numel(ratnums)
    SALInd(r,1) = strcmp(ratT.groups{r},'Saline');
end

clearvars ratnums excluded side chNAc chBLA finalch groups

% each column # here corresponds to cell column (2nd dim) in other variables

phases = {'ConditioningDay1', 'ConditioningDay2', 'ConditioningDay3', ...
    'ConditioningDay4', 'ConditioningDay5', 'ConditioningDay6', ...
    'ConditioningDay7', 'ExtinctionDay1', 'ExtinctionDay2'};

abbr = {'RC1', 'RC2', 'RC3', 'RC4', 'RC5', 'RC6', 'RC7', 'Ext1', 'Ext2'};
  
blocks = {'RC1_1', 'RC1_2', 'RC2_1', 'RC2_2', 'RC3_1', 'RC3_2', 'RC4_1',...
    'RC4_2', 'RC5_1', 'RC5_2', 'RC6_0', 'RC6_2', 'RC7_1', 'RC7_2', ...
    'Ext1_1', 'Ext1_2', 'Ext2_1', 'Ext2_2'};

TLT = numel(blocks); %total # of sessions of training
TotalTrial = 60; % total # of trail per session

% each row # here corresponds to the numerical code that will be saved in
% trl and data.trialinfo to identify which subtrials correspond to which
% interval types
intervals = {'Before'; 'During'; 'After'};

% each row # here corresponds to the numerical code that will represent
% which region's data is being used for phase or amplitude source data
regions = {'NAc'; 'BLA'};

% initialize variables
goodchs   = cell(size(ratT,1),1);  % cellstr can't fit in 1 cell of a table
notdeadchs = cell(size(ratT,1),1);
notes     = cell(size(ratT,1),numel(phases));
toneTS    = cell(size(ratT,1),numel(phases));
nexfile   = cell(size(ratT,1),numel(phases));
BLnexfile   = cell(size(ratT,1),1);
BLtoneTS   = cell(size(ratT,1),1);

% any extra comments about each 
load([datafolder 'MisTS.mat'])
for r = 1:4
    for p = 1:4
        toneTS{r,p+2} = MisTS {r,p};
    end
end

notes{1,3} = 'tone timestamps for 361''s RC3 are in "toneTS{1,3}"';
% toneTS{1,3} = [];
notes{1,4} = 'tone timestamps for 361''s RC4 are in "toneTS{1,4}"';
% toneTS{1,4} = [];
notes{1,5} = 'tone timestamps for 361''s RC5 are in "toneTS{1,5}"';
% toneTS{1,5} = [];
notes{1,6} = 'tone timestamps for 361''s RC6 are in "toneTS{1,6}"';
% toneTS{1,6} = [];
notes{2,3} = 'tone timestamps for 362''s RC3 are in "toneTS{2,3}"';
% toneTS{2,3} = [];
notes{2,4} = 'tone timestamps for 362''s RC4 are in "toneTS{2,4}"';
% toneTS{2,4} = [];
notes{2,5} = 'tone timestamps for 362''s RC5 are in "toneTS{2,5}"';
% toneTS{2,5} = [];
notes{2,5} = 'tone timestamps for 362''s RC6 are in "toneTS{2,6}"';
% toneTS{2,6} = [];
notes{3,3} = 'tone timestamps for 363''s RC3 are in "toneTS{3,3}"';
% toneTS{3,3} = [];
notes{3,4} = 'tone timestamps for 363''s RC4 are in "toneTS{3,4}"';
% toneTS{3,4} = [];
notes{3,5} = 'tone timestamps for 363''s RC5 are in "toneTS{3,5}"';
% toneTS{3,5} = [];
notes{3,6} = 'tone timestamps for 363''s RC6 are in "toneTS{3,6}"';
% toneTS{3,6} = [];
notes{4,3} = 'tone timestamps for 363''s RC3 are in "toneTS{4,3}"';
% toneTS{4,3} = [];
notes{4,4} = 'tone timestamps for 364''s RC4 are in "toneTS{4,4}"';
% toneTS{4,4} = [];
notes{4,5} = 'tone timestamps for 364''s RC5 are in "toneTS{4,5}"';
% toneTS{4,5} = [];
notes{4,6} = 'tone timestamps for 364''s RC6 are in "toneTS{4,6}"';
% toneTS{4,6} = [];
notes{6,3} = 'headstage unplugged once';
notes{7,2} = 'headstage unplugged once';
notes{7,4} = 'headstage unplugged three times';
notes{7,5} = 'headstage unplugged once';
notes{7,6} = 'headstage unplugged twice';
notes{7,7} = 'headstage unplugged twice';

BLtoneTS{1} = 725000;
BLtoneTS{2} = 619000;
BLtoneTS{3} = 741000;
BLtoneTS{4} = 854000;
BLtoneTS{5} = 55000;
BLtoneTS{6} = 236000;
BLtoneTS{7} = 12000;
BLtoneTS{8} = 28000;


%% channels to use for each rat, defaults to 'AD*' if empty
goodchs{1} = {'AD*'};
goodchs{2} = {'AD*'};
goodchs{3} = {'AD*'};
goodchs{4} = {'AD*'};
goodchs{5} = {'AD*'};
goodchs{6} = {'AD*'};
goodchs{7} = {'AD*'};
goodchs{8} = {'AD*'};
goodchs{9} = {'AD*'};
goodchs{10} = {'AD*'};
goodchs{11} = {'AD*'};
goodchs{12} = {'AD*'};
goodchs{13} = {'AD*'};
goodchs{14} = {'AD*'};
goodchs{15} = {'AD*'};
goodchs{16} = {'AD*'};
goodchs{17} = {'AD*'};
goodchs{18} = {'AD*'};
goodchs{19} = {'AD*'};
goodchs{20} = {'AD*'};
goodchs{21} = {'AD*'};

%% filenames
% Can't use PlexUtil or NeuroExplorer merged files because either FieldTrip
% crashes or the timestamps are off after the merge.
% FT_preproc_Fear_TEM.m can handle this cellstr.

% Baseline recording in social chamber
BLnexfile{1,1} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160323_0000-aligned.nex'];
BLnexfile{2,1} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160323_0001-aligned.nex'];
BLnexfile{3,1} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160323_0002-aligned.nex'];
BLnexfile{4,1} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160323_0003-aligned.nex'];
BLnexfile{5,1} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160629_0001-aligned.nex'];
BLnexfile{6,1} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160629_0002-aligned.nex'];
BLnexfile{7,1} = [tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160629_0003-aligned.nex'];
BLnexfile{8,1} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160629_0004-aligned.nex'];
BLnexfile{9,1} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160913_0002-aligned.nex'];
%  '391_160914_0000-aligned.nex']; 0914-0922: 0 0 0 0 0 0 0 0 0
BLnexfile{10,1} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160913_0004-aligned.nex'];
%  '394_160914_0002-aligned.nex']; 0914-0922: 2 1 2 0 2 2 3 2 2
BLnexfile{11,1} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161126_0005-aligned.nex'];
% 1127-1205: 1 5 9 [1 2] 2 2 1 1 [2 3]
BLnexfile{12,1} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161126_0006-aligned.nex'];
% 1127-1205: 2 6 10 3 3 1 2 2 [4 5]
BLnexfile{13,1} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161126_0007-aligned.nex'];
% 1127-1205: 3 7 11 [4 5] 4 3 3 3 [6 7]
BLnexfile{14,1} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161126_0004-aligned.nex'];
% 1127-1205: 0 4 8 0 [0 1] 0 0 0 [0 1]
BLnexfile{15,1} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170307_0000-aligned.nex'];
% 0308-0316: 0 0 0 0 [0 1] 2 2 3 [4 5]
BLnexfile{16,1} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170307_0005-aligned_sub.nex'];
% 0308-0316: 1 1 0 1 2 1 1 2 [2 3]
BLnexfile{17,1} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170307_0006-aligned_sub.nex'];
% 0308-0316: 2 2 2 2 [3 4] 0 0 [0 1] [0 1]  
BLnexfile{18,1} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170307_0007-aligned_sub.nex'];
% 0308-0316: 3 3 3 3 5 3 3 4 [6 7]
BLnexfile{19,1} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170606_0009-aligned_sub.nex'];
% 0607-0615: 2 2 2 2 2 2 2 2 [4 5 6]
BLnexfile{20,1} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170606_0008-aligned_sub.nex'];
% 0607-0615: 1 1 1 1 1 1 1 1 [2 3]
BLnexfile{21,1} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170606_0007-aligned_sub.nex'];
% 0607-0615: 0 0 0 0 0 0 0 0 [0 1]



% Conditioning Day 1
nexfile{1,1} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160330_0001-aligned.nex'];
nexfile{2,1} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160330_0000-aligned.nex'];
nexfile{3,1} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160330_0002-aligned.nex'];
nexfile{4,1} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160330_0003-aligned.nex'];
nexfile{5,1} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160706_0000-aligned.nex'];
nexfile{6,1} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160706_0001-aligned.nex'];
nexfile{7,1} = [tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160707_0002-aligned_TSm.nex'];
nexfile{8,1} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160707_0003-aligned.nex'];
nexfile{9,1} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160914_0000-aligned.nex']; 
% 0915-0922: 0 0 0 0 0 0 0
nexfile{10,1} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160914_0002-aligned.nex'];
% 0915-0922: 1 2 0 2 2 3 2 2
nexfile{11,1} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161127_0001-aligned.nex'];
% 1128-1205: 5 9 [1 2] 2 2 1 1 [2 3]
nexfile{12,1} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161127_0002-aligned.nex'];
% 1128-1205: 6 10 3 3 1 2 2 [4 5]
nexfile{13,1} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161127_0003-aligned.nex'];
% 1128-1205: 7 11 [4 5] 4 3 3 3 [6 7]
nexfile{14,1} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161127_0000-aligned.nex'];
% 1128-1205: 4 8 0 [0 1] 0 0 0 [0 1]
nexfile{15,1} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170308_0000-aligned.nex'];
% 0309-0316: 0 0 0 [0 1] 2 2 3 [4 5]
nexfile{16,1} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170308_0001-aligned_sub.nex'];
% 0309-0316: 1 0 1 2 1 1 2 [2 3]
nexfile{17,1} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170308_0002-aligned.nex'];
% 0309-0316: 2 2 2 [3 4] 0 0 [0 1] [0 1] 
nexfile{18,1} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170308_0003-aligned_sub.nex'];
% 0309-0316: 3 3 3 5 3 3 4 [6 7]
nexfile{19,1} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170607_0002-aligned_sub.nex'];
% 0608-0615: 2 2 2 2 2 2 2 [4 5 6]
nexfile{20,1} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170607_0001-aligned_sub.nex'];
% 0608-0615: 1 1 1 1 1 1 1 [2 3]
nexfile{21,1} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170607_0000-aligned_sub.nex'];
% 0608-0615: 0 0 0 0 0 0 0 [0 1]



% Conditioning Day 2
nexfile{1,2} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160331_0000-aligned.nex'];
nexfile{2,2} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160331_0001-aligned.nex'];
nexfile{3,2} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160331_0002-aligned.nex'];
nexfile{4,2} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160331_0003-aligned.nex'];
nexfile{5,2} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160707_0000-aligned.nex'];
nexfile{6,2} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160707_0001-aligned.nex'];
nexfile{7,2} = {[tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160708_0003-aligned_TSm.nex']; [tserver 'PlexonData Raw' ...
  filesep '379' filesep '379_160708_0004-aligned_TSm.nex']};
nexfile{8,2} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160708_0005-aligned.nex'];
nexfile{9,2} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160915_0000-aligned.nex']; 
% 0916-0922: 0 0 0 0 0 0 0
nexfile{10,2} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160915_0001-aligned.nex'];
% 0916-0922: 2 0 2 2 3 2 2
nexfile{11,2} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161128_0005-aligned.nex'];
% 1129-1205: 9 [1 2] 2 2 1 1 [2 3]
nexfile{12,2} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161128_0006-aligned.nex'];
% 1129-1205: 10 3 3 1 2 2 [4 5]
nexfile{13,2} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161128_0007-aligned.nex'];
% 1129-1205: 11 [4 5] 4 3 3 3 [6 7]
nexfile{14,2} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161128_0004-aligned.nex'];
% 1129-1205: 8 0 [0 1] 0 0 0 [0 1]
nexfile{15,2} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170309_0000-aligned.nex'];
% 0310-0316: 0 0 [0 1] 2 2 3 [4 5]
nexfile{16,2} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170309_0001-aligned_sub.nex'];
% 0310-0316: 0 1 2 1 1 2 [2 3]
nexfile{17,2} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170309_0002-aligned_sub.nex'];
% 0310-0316: 2 2 [3 4] 0 0 [0 1] [0 1] 
nexfile{18,2} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170309_0003-aligned_sub.nex'];
% 0310-0316: 3 3 5 3 3 4 [6 7]
nexfile{19,2} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170608_0002-aligned_sub.nex'];
% 0609-0615: 2 2 2 2 2 2 [4 5 6]
nexfile{20,2} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170608_0001-aligned_sub.nex'];
% 0609-0615: 1 1 1 1 1 1 [2 3]
nexfile{21,2} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170608_0000-aligned_sub.nex'];
% 0609-0615: 0 0 0 0 0 0 [0 1]


% Conditioning Day 3
nexfile{1,3} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160401_0000-aligned.nex'];
nexfile{2,3} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160401_0001-aligned.nex'];
nexfile{3,3} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160401_0002-aligned.nex'];
nexfile{4,3} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160401_0003-aligned.nex'];
nexfile{5,3} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160708_0000-aligned.nex'];
nexfile{6,3} = {[tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160708_0001-aligned_TSm.nex'];[tserver 'PlexonData Raw' filesep ...
  '378' filesep '378_160708_0002-aligned_TSm.nex']};
nexfile{7,3} = [tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160709_0002-aligned.nex'];
nexfile{8,3} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160709_0003-aligned.nex'];
nexfile{9,3} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160916_0000-aligned.nex']; 
% 0917-0922: 0 0 0 0 0 0
nexfile{10,3} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160916_0002-aligned.nex'];
% 0917-0922: 0 2 2 3 2 2
nexfile{11,3} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161129_0009-aligned.nex'];
% 1130-1205: [1 2] 2 2 1 1 [2 3]
nexfile{12,3} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161129_0010-aligned.nex'];
% 1130-1205: 3 3 1 2 2 [4 5]
nexfile{13,3} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161129_0011-aligned.nex'];
% 1130-1205: [4 5] 4 3 3 3 [6 7]
nexfile{14,3} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161129_0008-aligned.nex'];
% 1130-1205: 0 [0 1] 0 0 0 [0 1] 
nexfile{15,3} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170310_0000-aligned.nex'];
% 0311-0316: 0 [0 1] 2 2 3 [4 5]
nexfile{16,3} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170310_0000-aligned_sub.nex'];
% 0311-0316: 1 2 1 1 2 [2 3]
nexfile{17,3} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170310_0002-aligned_sub.nex'];
% 0311-0316: 2 [3 4] 0 0 [0 1] [0 1] 
nexfile{18,3} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170310_0003-aligned_sub.nex'];
% 0311-0316: 3 5 3 3 4 [6 7]
nexfile{19,3} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170609_0002-aligned_sub.nex'];
% 0610-0615: 2 2 2 2 2 [4 5 6]
nexfile{20,3} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170609_0001-aligned_sub.nex'];
% 0610-0615: 1 1 1 1 1 [2 3]
nexfile{21,3} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170609_0000-aligned_sub.nex'];
% 0610-0615: 0 0 0 0 0 [0 1]


% Conditioning Day 4
nexfile{1,4} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160402_0000-aligned.nex'];
nexfile{2,4} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160402_0001-aligned.nex'];
nexfile{3,4} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160402_0002-aligned.nex'];
nexfile{4,4} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160402_0003-aligned.nex'];
nexfile{5,4} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160709_0000-aligned.nex'];
nexfile{6,4} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160709_0001-aligned.nex'];
nexfile{7,4} = {[tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160710_0002-aligned_TSm.nex']; [tserver 'PlexonData Raw' ...
  filesep '379' filesep '379_160710_0003-aligned_TSm.nex']; [tserver ...
  'PlexonData Raw' filesep '379' filesep ...
  '379_160710_0004-aligned_TSm.nex']; [tserver 'PlexonData Raw' ...
  filesep '379' filesep '379_160710_0005-aligned_TSm.nex']};
nexfile{8,4} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160710_0006-aligned.nex'];
nexfile{9,4} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160917_0000-aligned.nex']; 
% 0918-0922: 0 0 0 0 0
nexfile{10,4} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160917_0000-aligned.nex'];
% 0918-0922: 2 2 3 2 2
nexfile{11,4} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161130_0002-aligned.nex'];
% 1201-1205: 2 2 1 1 [2 3]
nexfile{12,4} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161130_0003-aligned.nex'];
% 1201-1205: 3 1 2 2 [4 5]
nexfile{13,4} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161130_0005-aligned.nex'];
% 1201-1205: 4 3 3 3 [6 7]
nexfile{14,4} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161130_0000-aligned.nex'];
% 1201-1205: [0 1] 0 0 0 [0 1]
nexfile{15,4} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170311_0000-aligned.nex'];
% 0312-0316: [0 1] 2 2 3 [4 5]
nexfile{16,4} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170311_0001-aligned_sub.nex'];
% 0312-0316: 2 1 1 2 [2 3]
nexfile{17,4} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170311_0002-aligned_sub.nex'];
% 0312-0316: [3 4] 0 0 [0 1] [0 1] 
nexfile{18,4} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170311_0003-aligned_sub.nex'];
% 0312-0316: 5 3 3 4 [6 7]
nexfile{19,4} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170610_0002-aligned_sub.nex'];
% 0611-0615: 2 2 2 2 [4 5 6]
nexfile{20,4} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170610_0001-aligned_sub.nex'];
% 0611-0615: 1 1 1 1 [2 3]
nexfile{21,4} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170610_0000-aligned_sub.nex'];
% 0611-0615: 0 0 0 0 [0 1]

% Conditioning Day 5
nexfile{1,5} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160403_0000-aligned.nex'];
nexfile{2,5} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160403_0001-aligned.nex'];
nexfile{3,5} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160403_0002-aligned.nex'];
nexfile{4,5} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160403_0003-aligned.nex'];
nexfile{5,5} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160710_0000-aligned.nex'];
nexfile{6,5} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160710_0001-aligned.nex'];
nexfile{7,5} = {[tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160711_0002-aligned.nex']; [tserver 'PlexonData Raw' filesep ...
  '379' filesep '379_160711_0003-aligned.nex']};
nexfile{8,5} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160711_0004-aligned.nex'];
nexfile{9,5} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160918_0000-aligned.nex']; 
% 0919-0922: 0 0 0 0
nexfile{10,5} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160918_0002-aligned.nex'];
% 0919-0922: 2 3 2 2
nexfile{11,5} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161201_0002-aligned.nex'];
% 1202-1205: 2 1 1 [2 3]
nexfile{12,5} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161201_0003-aligned.nex'];
% 1202-1205: 1 2 2 [4 5]
nexfile{13,5} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161201_0004-aligned.nex'];
% 1202-1205: 3 3 3 [6 7]
nexfile{14,5} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161201_0001-aligned.nex'];
% 1202-1205: 0 0 0 [0 1]
% nexfile{15,5} = {[tserver 'PlexonData Raw' filesep '416' filesep ...
%   '416_170312_0000-aligned.nex'];[tserver 'PlexonData Raw' filesep ...
%   '416' filesep '416_170312_0001-aligned_sub.nex']};
nexfile{15,5} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170312_0000-aligned.nex'];
% 0313-0316: 2 2 3 [4 5]
nexfile{16,5} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170312_0002-aligned_sub.nex'];
% 0313-0316: 1 1 2 [2 3]
nexfile{17,5} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170312_0004-aligned_sub.nex'];
% 0313-0316: [0 0 [0 1] [0 1] 
nexfile{18,5} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170312_0005-aligned_sub.nex'];
% 0313-0316: 3 3 4 [6 7]
nexfile{19,5} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170611_0002-aligned_sub.nex'];
% 0612-0615: 2 2 2 [4 5 6]
nexfile{20,5} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170611_0001-aligned_sub.nex'];
% 0612-0615: 1 1 1 [2 3]
nexfile{21,5} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170611_0000-aligned_sub.nex'];
% 0612-0615: 0 0 0 [0 1]



% Conditioning Day 6
nexfile{1,6} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160404_0001-aligned.nex'];
nexfile{2,6} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160404_0002-aligned.nex'];
nexfile{3,6} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160404_0003-aligned.nex'];
nexfile{4,6} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160404_0004-aligned.nex'];
nexfile{5,6} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160711_0000-aligned.nex'];
nexfile{6,6} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160711_0001-aligned.nex'];
nexfile{7,6} = {[tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160712_0002-aligned_TSm.nex']; [tserver 'PlexonData Raw' ...
  filesep '379' filesep '379_160712_0003-aligned_TSm.nex']; ...
  [tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160712_0004-aligned_TSm.nex']};
nexfile{8,6} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160712_0005-aligned.nex'];
nexfile{9,6} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160919_0000-aligned.nex']; 
% 0920-0922: 0 0 0
nexfile{10,6} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160919_0002-aligned.nex'];
% 0920-0922: 3 2 2
nexfile{11,6} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161202_0002-aligned.nex'];
% 1203-1205: 1 1 [2 3]
nexfile{12,6} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161202_0001-aligned.nex'];
% 1203-1205: 2 2 [4 5]
nexfile{13,6} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161202_0003-aligned.nex'];
% 1203-1205: 3 3 [6 7]
nexfile{14,6} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161202_0000-aligned.nex'];
% 1203-1205: 0 0 [0 1]
nexfile{15,6} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170313_0002-aligned_sub.nex'];
% 0314-0316: 2 3 [4 5]
nexfile{16,6} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170313_0001-aligned_sub.nex'];
% 0314-0316: 1 2 [2 3]
nexfile{17,6} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170313_0000-aligned_sub.nex'];
% 0314-0316: 0 [0 1] [0 1] 
nexfile{18,6} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170313_0003-aligned_sub.nex'];
% 0314-0316: 3 4 [6 7]
nexfile{19,6} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170612_0002-aligned_sub.nex'];
% 0613-0615: 2 2 [4 5 6]
nexfile{20,6} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170612_0001-aligned_sub.nex'];
% 0613-0615: 1 1 [2 3]
nexfile{21,6} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170612_0000-aligned_sub.nex'];
% 0613-0615: 0 0 [0 1]


% Conditioning Day 7
nexfile{1,7} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160405_0000-aligned.nex'];
nexfile{2,7} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160405_0001-aligned.nex'];
nexfile{3,7} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160405_0002-aligned.nex'];
nexfile{4,7} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160405_0003-aligned.nex'];
nexfile{5,7} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160712_0000-aligned.nex'];
nexfile{6,7} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160712_0001-aligned.nex'];
nexfile{7,7} = {[tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160713_0002-aligned_TSm.nex']; [tserver 'PlexonData Raw' ...
  filesep '379' filesep '379_160713_0003-aligned_TSm.nex']; ...
  [tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160713_0004-aligned_TSm.nex']};
nexfile{8,7} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160713_0005-aligned.nex'];
nexfile{9,7} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160920_0000-aligned.nex']; 
% 0921-0922: 0 0
nexfile{10,7} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160920_0003-aligned.nex'];
% 0921-0922: 2 2
nexfile{11,7} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161203_0001-aligned.nex'];
% 1204-1205: 1 [2 3]
nexfile{12,7} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161203_0002-aligned.nex'];
% 1204-1205: 2 [4 5]
nexfile{13,7} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161203_0003-aligned.nex'];
% 1204-1205: 3 [6 7]
nexfile{14,7} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161203_0000-aligned.nex'];
% 1204-1205: 0 [0 1]
nexfile{15,7} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170314_0002-aligned_sub.nex'];
% 0315-0316: 3 [4 5]
nexfile{16,7} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170314_0001-aligned_sub.nex'];
% 0315-0316: 2 [2 3]
nexfile{17,7} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170314_0000-aligned_sub.nex'];
% 0315-0316: [0 1] [0 1] 
nexfile{18,7} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170314_0003-aligned_sub.nex'];
% 0315-0316: 4 [6 7]
nexfile{19,7} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170613_0002-aligned_sub.nex'];
% 0614-0615: 2 [4 5 6]
nexfile{20,7} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170613_0001-aligned_sub.nex'];
% 0614-0615: 1 [2 3]
nexfile{21,7} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170613_0000-aligned_sub.nex'];
% 0614-0615: 0 [0 1]


% Extinction Day 1
nexfile{1,8} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160406_0000-aligned.nex'];
nexfile{2,8} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160406_0001-aligned.nex'];
nexfile{3,8} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160406_0002-aligned.nex'];
nexfile{4,8} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160406_0003-aligned.nex'];
nexfile{5,8} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160713_0000-aligned.nex'];
nexfile{6,8} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160713_0001-aligned.nex'];
nexfile{7,8} = [tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160714_0002-aligned.nex'];
nexfile{8,8} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160714_0003-aligned.nex'];
nexfile{9,8} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160921_0000-aligned.nex']; 
% 0922: 0
nexfile{10,8} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160921_0002-aligned.nex'];
% 0922: 2
nexfile{11,8} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161204_0001-aligned.nex'];
% 1205: [2 3]
nexfile{12,8} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161204_0002-aligned.nex'];
% 1205: [4 5]
nexfile{13,8} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161204_0003-aligned.nex'];
% 1205: [6 7]
nexfile{14,8} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161204_0000-aligned.nex'];
% 1205: [0 1]
nexfile{15,8} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170315_0003-aligned_sub.nex'];
% 0316: [4 5]
nexfile{16,8} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170315_0002-aligned_sub.nex'];
% 0316: [2 3]
nexfile{17,8} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170315_0001-aligned_sub.nex'];
% 0316: [0 1] 
nexfile{18,8} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170315_0004-aligned_sub.nex'];
% 0316: [6 7]
nexfile{19,8} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170614_0002-aligned_sub.nex'];
% 0615: [4 5 6]
nexfile{20,8} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170614_0001-aligned_sub.nex'];
% 0615: [2 3]
nexfile{21,8} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170614_0000-aligned_sub.nex'];
% 0615: [0 1]

% Extinction Day 2
nexfile{1,9} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160407_0004-aligned.nex'];
nexfile{2,9} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160407_0005-aligned.nex'];
nexfile{3,9} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160407_0006-aligned.nex'];
nexfile{4,9} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160407_0007-aligned.nex'];
nexfile{5,9} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160714_0000-aligned.nex'];
nexfile{6,9} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160714_0001-aligned.nex'];
nexfile{7,9} = [tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160715_0001-aligned.nex'];
nexfile{8,9} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160715_0000-aligned.nex'];
nexfile{9,9} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160922_0000-aligned.nex']; 
nexfile{10,9} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160922_0002-aligned.nex'];
nexfile{11,9} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161205_0002-aligned.nex'];
nexfile{12,9} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161205_0004-aligned.nex'];
nexfile{13,9} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161205_0006-aligned.nex'];
nexfile{14,9} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161205_0000-aligned.nex'];
nexfile{15,9} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170316_0004-aligned_sub.nex'];
nexfile{16,9} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170316_0002-aligned_sub.nex'];
nexfile{17,9} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170316_0000-aligned_sub.nex'];
nexfile{18,9} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170316_0006-aligned_sub.nex'];
nexfile{19,9} = {[tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170615_0004-aligned_sub.nex'];[tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170615_0005-aligned_sub.nex']};
nexfile{20,9} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170615_0002-aligned_sub.nex'];
nexfile{21,9} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170615_0000-aligned_sub.nex'];


clear r p chBLA chNAc TLT BLnexfile 
%% complete workflow:
% 0) align original .plx files, save as .nex
% 1) FT_preproc_Fear_TEM - Load all data from -aligned.nex files into
% FieldTrip format & merge into 1 dataset per rat, using new trialfun_TEM.m
% and supporting a cell array of strings for multiple filenames within one
% phase (to avoid merged NEX files, which are hard to read correctly).
% 2) FT_rejectArt_Fear_TEM
% 3) FT_ica_Fear_TEM
% 4) spectra/coher
% 5) cross-frequency
% 6) compare to behavior
% 7) granger causality?