%% Visualize normalized time-frequency analysis results

close all; clearvars; clc;    % clean slate

info_VPA_RC                     % load all background info (filenames, etc)

foi = 1.3.^(-2:20);         % freqs from 1-103 Hz progressing up by 5% each
toi = -6:0.5:12;  % all tones + before & after times
plt = true;                  % if plots are desired for each tone and rat
errcnt = 0;                   % error count
errloc = [];                  % error location (r,p,t)

bk2cal = [1, 14, 15, 18];       %block numbers to be plotted for each rat
bk2calD = [1, 7, 8, 9];     %the day of sesstion that assigned block is in
elcnum = cell(16,1);
for n= 1:size(elcnum,1)
elcnum{n} = num2str([n].','%02d');
end
%% preallocate space to save data over rats/times (BDA each tone)

% BLnormPow_mPFC = NaN(numel(foi),numel(toi),size(ratT,1));
% BLnormPow_BLA = NaN(numel(foi),numel(toi),size(ratT,1));
% BLnormCoh = NaN(numel(foi),numel(toi),size(ratT,1));

PTnormPow_NAc = NaN(numel(foi),numel(toi),size(ratT,1));
PTnormPow_BLA = NaN(numel(foi),numel(toi),size(ratT,1));
PTnormCoh = NaN(numel(foi),numel(toi),size(ratT,1));

%% use try-catch when running unsupervised batches

for g = 1:2
    for tr = 3:numel(eval([treatment{g} 'group']))
        gr = eval([treatment{g} 'group' '(' int2str(tr) ')']);
        r = find(ratT.ratnums == gr);
        try
            %     t1 = 1;  % start before 1st Habituation tone for new rat (BLnorm)
            t2 = 1;  % start during 1st Habituation tone for new rat (PTnorm)
            
            %     %% gather baseline data
            %     baselinefile = [datafolder 'SpecCoherograms_' int2str(ratT.ratnums(r)) ...
            %       'Baseline.mat'];
            %
            %     if ~exist(baselinefile,'file')
            %       error([baselinefile ' does not exist!']);
            %     end
            %
            %     %% load data
            %
            %     BL = load(baselinefile);
            %
            %     %% check for accuracy
            %
            %     if BL.freq.time(1) >= 0
            %       error('no pre-tone baseline found');
            %     else
            %       disp(['first timestamp is ' num2str(BL.freq.time(1))]);
            %     end
            %
            %     if strcmp(goodchs{r}{1},'AD*')
            %       if numel(BL.freq.label) ~= (17 - numel(goodchs{r}))
            %         error('different channels used than planned');
            %       else
            %         for ch = 1:numel(BL.freq.label)
            %           k = strfind(goodchs{r}, BL.freq.label{ch});
            %           if any([k{:}])
            %             error([BL.freq.label{ch} ' should be excluded']);
            %           else
            %             disp([BL.freq.label{ch} ' is not excluded']);
            %           end
            %         end
            %       end
            %     elseif numel(BL.freq.label) ~= numel(goodchs{r}) || ...
            %         any(any(~strcmp(BL.freq.label,goodchs{r})))
            %       error('different channels used than planned');
            %     else
            %       disp([BL.freq.label{:} ' are correct']);   % still true
            %     end
            %
            %     maxdiff = max(abs(BL.freq.freq-foi));
            %     if maxdiff > 0.02
            %       error([baselinefile ' does not have the same ' ...
            %         'frequency bins as foi']);
            %     else
            %       disp(['max difference from foi = ' num2str(maxdiff)]);
            %     end
            %
            
            %
            %             indx = NaN(size(freq.labelcmb));
            %
            %             for i = 1:size(freq.labelcmb,1)
            %               % find 1st ch index
            %               indx(i,1) = find(strcmp(freq.labelcmb(i,1),freq.label));
            %
            %               % find 2nd ch index
            %               indx(i,2) = find(strcmp(freq.labelcmb(i,2),freq.label));
            %             end
            %
            %     %% calculate coherence
            %
            %     % preallocate space
            %     BL.freq.coherence   = NaN(size(BL.freq.crsspctrm));
            %
            %     for i = 1:size(indx,1)
            %       BL.freq.coherence(i,:,:) = abs(BL.freq.crsspctrm(i,:,:)./ ...
            %         sqrt(BL.freq.powspctrm(indx(i,1),:,:).* ...
            %         BL.freq.powspctrm(indx(i,2),:,:)));
            %     end
            %
            %     %% average baseline data
            %
            %     meanBLpow = mean(BL.freq.powspctrm,3,'omitnan');
            %     meanBLcoh = mean(BL.freq.coherence,3,'omitnan');
            %
            %% normalize all other spec/coherograms by baseline
            
            for b = 1:numel(bk2cal)
                for elc = 1:8
                    if ~isempty(nexfile{r}) % no need for an error if there was no recording
                        try
                            inputfile  = [datafolder 'SpecCoherograms_' ...
                                int2str(gr) blocks{bk2cal(b)} '.mat'];
                            if ~exist(inputfile,'file')
                                error([inputfile ' does not exist!']);
                            end
                            %% load data
                            
                            load(inputfile);
                            %% translate channel names into indices
                            
                            indx = NaN(size(freq.labelcmb));
                            
                            for i = 1:size(freq.labelcmb,1)
                                % find 1st ch index
                                indx(i,1) = find(strcmp(freq.labelcmb(i,1),freq.label));
                                
                                % find 2nd ch index
                                indx(i,2) = find(strcmp(freq.labelcmb(i,2),freq.label));
                            end
                            %% check for accuracy
                            
                            if freq.time(1) >= 0
                                error('no pre-tone baseline found');
                            else
                                disp(['first timestamp is ' num2str(freq.time(1))]);
                            end
                            
                            if strcmp(goodchs{r}{1},'AD*')
                                if numel(freq.label) ~= (17 - numel(goodchs{r}))
                                    error('different channels used than planned');
                                else
                                    for ch = 1:numel(freq.label)
                                        k = strfind(goodchs{r}, freq.label{ch});
                                        if any([k{:}])
                                            error([freq.label{ch} ' should be excluded']);
                                        else
                                            disp([freq.label{ch} ' is not excluded']);
                                        end
                                    end
                                end
                            elseif numel(freq.label) ~= numel(goodchs{r}) || ...
                                    any(any(~strcmp(freq.label,goodchs{r})))
                                error('different channels used than planned');
                            else
                                disp([freq.label{:} ' are correct']);   % still true
                            end
                            
                            maxdiff = max(abs(freq.freq-foi));
                            if maxdiff > 0.01
                                error([inputfile ' does not have the same ' ...
                                    'frequency bins as foi']);
                            else
                                disp(['max difference from foi = ' num2str(maxdiff)]);
                            end
                            
                            %% check input compatibility
                            %
                            %                     if numel(freq.labelcmb) ~= numel(BL.freq.labelcmb) || ...
                            %                             any(any(~strcmp(freq.labelcmb,BL.freq.labelcmb)))
                            %                         error([inputfile ' does not have the same ' ...
                            %                             'channel combinations as ' baselinefile]);
                            %                     end
                            %
                            %% calculate coherence
                            
                            % preallocate space
                            freq.coherence      = NaN(size(freq.crsspctrm));
                            
                            for i = 1:size(indx,1)
                                freq.coherence(i,:,:) = abs(freq.crsspctrm(i,:,:)./ ...
                                    sqrt(freq.powspctrm(indx(i,1),:,:).* ...
                                    freq.powspctrm(indx(i,2),:,:)));
                            end
                            
                            %% normalize by baseline period
                            %
                            %                     normfreq = freq;   % duplicate variable
                            %
                            %                     % normalization method 'db'
                            %                     normfreq.powspctrm = 10*log10(freq.powspctrm ./ ...
                            %                         repmat(meanBLpow,[1 1 numel(freq.time)]));
                            %
                            %                     % normalization method 'vssum'
                            %                     normfreq.coherence = (freq.coherence - repmat(meanBLcoh,...
                            %                         [1 1 numel(freq.time)])) ./ ...
                            %                         (freq.coherence + repmat(meanBLcoh,[1 1 numel(freq.time)]));
                            %
                            %% plot raw, BLnorm, & PTnorm spectrograms & coherograms
                            
                            if plt
                                %% display raw spectrograms & coherograms
                                % pick channel AD06 and AD14 for default channel
                                % for NAc and BLA (06 for NAc 14 for BLA if right;
                                % 14 for NAc and 06 for BLA if left)
                                for target = 1:3 %#ok<UNRCH> can be reached if plt = true is set in top cell
                                    cfg                 = [];
                                    
                                    if target == 1     %NAc
                                        cfg.parameter       = 'powspctrm';
                                        if strcmp(ratT{r,3},'Right') == 1
                                            cfg.channel = ['AD' elcnum{elc}];
                                        else
                                            cfg.channel = ['AD' elcnum{elc+8}];
                                        end
                                    elseif target == 2 %BLA
                                        cfg.parameter       = 'powspctrm';
                                        if strcmp(ratT{r,3},'Right') == 1
                                            cfg.channel = ['AD' elcnum{elc+8}];
                                        else
                                            cfg.channel = ['AD' elcnum{elc}];
                                        end
                                    else
                                        cfg.parameter       = 'coherence';
                                        cfg.zlim            = 'zeromax';
                                        if strcmp(ratT{r,3},'Right') == 1
                                            cfg.channel = ['AD' elcnum{elc}];
                                            cfg.refchannel ='AD14';
                                        else
                                            cfg.channel = ['AD' elcnum{elc+8}];
                                            cfg.refchannel ='AD06';
                                        end
                                    end
                                    
                                    if ~strcmp('none',cfg.channel) && ...
                                            (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
                                        figure(target);  clf;
                                        ft_singleplotTFR(cfg, freq);
                                        colormap(jet);
                                        xlabel('Time from Tone Onset (seconds)');
                                        ylabel('Frequency (Hz)');
                                        if target < 3
                                            title([treatment{g} 'Rat ' ...
                                                int2str(gr) ...
                                                blocks{bk2cal(b)} ', ' ...
                                                regions{target} cfg.channel ' Power']);
                                            print([figurefolder treatment{g}...
                                                int2str(gr) ...
                                                blocks{bk2cal(b)} '_Spectrogram_'...
                                                regions{target} cfg.channel],'-dpng');
                                        else
                                            title([treatment{g} 'Rat ' int2str(gr) ...
                                                blocks{bk2cal(b)} cfg.channel ', Coherence']);
                                            print([figurefolder treatment{g} int2str(gr) ...
                                                blocks{bk2cal(b)} cfg.channel '_Coherogram'],'-dpng');
                                        end
                                    end
                                    
                                end
                                %% display baseline normalized spectrograms & coherograms
                                %
                                %                         for target = 1:3
                                %                             cfg                 = [];
                                %                             cfg.zlim            = 'maxabs';
                                %
                                %                             if target == 1
                                %                                 cfg.parameter       = 'powspctrm';
                                %                                 cfg.channel         = ratT.chNAc{r};
                                %                             elseif target == 2
                                %                                 cfg.parameter       = 'powspctrm';
                                %                                 cfg.channel         = ratT.chBLA{r};
                                %                             else
                                %                                 cfg.parameter       = 'coherence';
                                %                                 cfg.channel         = ratT.chNAc{r};
                                %                                 cfg.refchannel      = ratT.chBLA{r};
                                %                             end
                                %
                                %                             if ~strcmp('none',cfg.channel) && ...
                                %                                     (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
                                %                                 figure(target+3);  clf;
                                %                                 ft_singleplotTFR(cfg, normfreq);
                                %                                 colormap(jet);
                                %                                 xlabel('Time from Tone Onset (seconds)');
                                %                                 ylabel('Frequency (Hz)');
                                %
                                %                                 if target < 3
                                %                                     title(['Rat ' int2str(gr) ', Tone #' ...
                                %                                         int2str(tone) ' of ' phases{p} ', ' ...
                                %                                         regions{target} ' Power (dB change from baseline)']);
                                %                                     print([figurefolder int2str(gr) ...
                                %                                         phases{p} int2str(tone) '_BLnormSpectrogram_' ...
                                %                                         regions{target} ],'-dpng');
                                %                                 else
                                %                                     title(['Rat ' int2str(gr) ', Tone #' ...
                                %                                         int2str(tone) ' of ' phases{p} ...
                                %                                         ', Coherence (change from baseline)']);
                                %                                     print([figurefolder int2str(gr) phases{p} ...
                                %                                         int2str(tone) '_BLnormCoherogram'],'-dpng');
                                %                                 end
                                %                             end
                                %                         end
                                %
                                %% display pre-tone normalized spectrograms & coherograms
                                
                                for target = 1:3
                                    cfg                 = [];
                                    cfg.baseline        = [-6 0];
                                    cfg.zlim            = 'maxabs';
                                    
                                    if target == 1  %NAc
                                        cfg.baselinetype    = 'db';
                                        cfg.parameter       = 'powspctrm';
                                        if strcmp(ratT{r,3},'Right') == 1
                                            cfg.channel = ['AD' elcnum{elc}];
                                        else
                                            cfg.channel = ['AD' elcnum{elc+8}];
                                            
                                        end
                                    elseif target == 2 %BLA
                                        cfg.baselinetype    = 'db';
                                        cfg.parameter       = 'powspctrm';
                                        if strcmp(ratT{r,3},'Right') == 1
                                            cfg.channel = ['AD' elcnum{elc+8}];
                                        else
                                            cfg.channel = ['AD' elcnum{elc}];
                                        end
                                    else
                                        cfg.baselinetype    = 'vssum';
                                        cfg.parameter       = 'coherence';
                                        if strcmp(ratT{r,3},'Right') == 1
                                            cfg.channel = ['AD' elcnum{elc}];
                                            cfg.refchannel ='AD14';
                                        else
                                            cfg.channel =  ['AD' elcnum{elc+8}];
                                            cfg.refchannel ='AD06';
                                        end
                                    end
                                    
                                    if ~strcmp('none',cfg.channel) && ...
                                            (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
                                        figure(target+6);  clf;
                                        ft_singleplotTFR(cfg, freq);
                                        colormap(jet);
                                        xlabel('Time from Tone Onset (seconds)');
                                        ylabel('Frequency (Hz)');
                                        
                                        if target < 3
                                            title([treatment{g} 'Rat ' int2str(gr) ...
                                                blocks{bk2cal(b)} ', ' ...
                                                regions{target} cfg.channel ' Power (dB change from pre-tone)']);
                                            print([figurefolder treatment{g} int2str(gr) ...
                                                blocks{bk2cal(b)}  '_PTnormSpectrogram_' ...
                                                regions{target} cfg.channel],'-dpng');
                                        else
                                            title([treatment{g} 'Rat ' int2str(gr) ...
                                                blocks{bk2cal(b)} cfg.channel
                                                ', Coherence (change from pre-tone)']);
                                            print([figurefolder treatment{g} int2str(gr) ...
                                                blocks{bk2cal(b)} cfg.channel '_PTnormCoherogram'],'-dpng');
                                        end
                                    end
                                end
                            end
                            
                            %% save BLnorm BDA tone data to display over trials
                            %
                            %                         if strcmp(ratT{r,3},'Right') == 1
                            %                             iNAc = strcmp('AD06',freq.label);
                            %                             iBLA = strcmp('AD14',freq.label);
                            %                             iC = sum([strcmp('AD06',freq.labelcmb) ...
                            %                                 strcmp('AD14',freq.labelcmb)],2) == 2;
                            %                         else
                            %                             iNAc = strcmp('AD14',freq.label);
                            %                             iBLA = strcmp('AD06',freq.label);
                            %                             iC = sum([strcmp('AD14',freq.labelcmb) ...
                            %                                 strcmp('AD06',freq.labelcmb)],2) == 2;
                            %                         end
                            %
                            %                         tB = freq.time>-6 & freq.time<=0;
                            %                         tD = freq.time>0 & freq.time<=6;
                            %                         tA = freq.time>6 & freq.time<=12;
                            %
                            %                     % before
                            %                     BLnormPow_NAc(:,t1,r) = mean(normfreq.powspctrm(...
                            %                         iNAc,:,tB),3,'omitnan');
                            %                     BLnormPow_BLA(:,t1,r) = mean(normfreq.powspctrm(...
                            %                         iBLA,:,tB),3,'omitnan');
                            %                     BLnormCoh(:,t1,r) = mean(normfreq.coherence(...
                            %                         iC,:,tB),3,'omitnan');
                            %                     t1 = t1 + 1;
                            %
                            %                     % during
                            %                     BLnormPow_NAc(:,t1,r) = mean(normfreq.powspctrm(...
                            %                         iNAc,:,tD),3,'omitnan');
                            %                     BLnormPow_BLA(:,t1,r) = mean(normfreq.powspctrm(...
                            %                         iBLA,:,tD),3,'omitnan');
                            %                     BLnormCoh(:,t1,r) = mean(normfreq.coherence(...
                            %                         iC,:,tD),3,'omitnan');
                            %                     t1 = t1 + 1;
                            %
                            %                     % after
                            %                     BLnormPow_NAc(:,t1,r) = mean(normfreq.powspctrm(...
                            %                         iNAc,:,tA),3,'omitnan');
                            %                     BLnormPow_BLA(:,t1,r) = mean(normfreq.powspctrm(...
                            %                         iBLA,:,tA),3,'omitnan');
                            %                     BLnormCoh(:,t1,r) = mean(normfreq.coherence(...
                            %                         iC,:,tA),3,'omitnan');
                            %                     t1 = t1 + 1;  % move to next tone
                            %
                            %% average pre-tone data
                            
                            %                         meanPTpow = mean(freq.powspctrm(:,:,tB),3,'omitnan');
                            %                         meanPTcoh = mean(freq.coherence(:,:,tB),3,'omitnan');
                            %
                            %% normalize by pre-tone period
                            %
                            %                         % normalization method 'db'
                            %                         normPTpowspctrm = 10*log10(freq.powspctrm ./ ...
                            %                             repmat(meanPTpow,[1 1 numel(freq.time)]));
                            %
                            %                         % normalization method 'vssum'
                            %                         normPTcoherence = (freq.coherence - repmat(meanPTcoh,...
                            %                             [1 1 numel(freq.time)])) ./ (freq.coherence ...
                            %                             + repmat(meanPTcoh,[1 1 numel(freq.time)]));
                            %
                            %% save PTnorm during tone data to display over trials
                            %
                            %                         % during
                            %                         PTnormPow_NAc(:,t2,r) = mean(normPTpowspctrm(...
                            %                             iNAc,:,tD),3,'omitnan');
                            %                         PTnormPow_BLA(:,t2,r) = mean(normPTpowspctrm(...
                            %                             iBLA,:,tD),3,'omitnan');
                            %                         PTnormCoh(:,t2,r) = mean(normPTcoherence(...
                            %                             iC,:,tD),3,'omitnan');
                            %                         t2 = t2 + 1;
                            %
                        catch ME1
                            errcnt = errcnt + 1;
                            errloc(errcnt,:) = [r b];
                            warning(['Error (' ME1.message ') while processing Rat # ' ...
                                int2str(gr) ', ' blocks{bk2cal(b)}...
                                '! Continuing with next in line.']);
                        end
                        %% clear data for next file
                        clearvars cfg freq normfreq normPT*
                    end
                end
            end
        catch ME2
            errcnt = errcnt + 1;
            errloc(errcnt,:) = [r 1 0];   % phase 1, before tone baseline
            warning(['Error (' ME2.message ') while processing Rat # ' ...
                int2str(gr) '! Continuing with next in line.']);
        end
        %% clear data for next rat
        clearvars BL
    end
    %% save aggregated data
    
    % save([datafolder 'meanBLnormSpecCoh.mat'], 'BLnorm*', '-v6');
    %     save([datafolder treatment{g} 'meanPTnormSpecCoh.mat'], 'PTnorm*', '-v6');
end


%% use singleplotTFR to visualize mean BLnorm data
%
% meanBLnorm.powspctrm(1,:,:) = mean(BLnormPow_NAc,3,'omitnan');
% meanBLnorm.powspctrm(2,:,:) = mean(BLnormPow_BLA,3,'omitnan');
% meanBLnorm.powspctrm(3,:,:) = mean(BLnormCoh,3,'omitnan');
% meanBLnorm.dimord = 'chan_freq_time';
% meanBLnorm.freq = foi;
% meanBLnorm.time = toi;
% meanBLnorm.label = {'NAc Power'; 'BLA Power'; 'Coherence'};
%
% cfg                 = [];
% cfg.zlim            = 'maxabs';
% cfg.parameter       = 'powspctrm';
%
% for c = 1:numel(meanBLnorm.label)
%     cfg.channel = meanBLnorm.label{c};
%     figure;
%     ft_singleplotTFR(cfg, meanBLnorm);
%     colormap(jet);
%     xlabel('Tone #');
%     ylabel('Frequency (Hz)');
%     %   title(['BLnorm ' meanBLnorm.label{c} ' for Rat #' int2str(gr) ' Over Time']);
%     title(['Mean BLnorm ' meanBLnorm.label{c} ' for All Rats Over Time']);
%     %   print([figurefolder int2str(gr) 'BLnorm_' meanBLnorm.label{c}],'-dpng');
%     print([figurefolder 'meanBLnorm_' meanBLnorm.label{c} '_all'],'-dpng');
% end

%% use singleplotTFR to visualize mean PTnorm data

% meanPTnorm.powspctrm(1,:,:) = mean(PTnormPow_NAc,3,'omitnan');
% meanPTnorm.powspctrm(2,:,:) = mean(PTnormPow_BLA,3,'omitnan');
% meanPTnorm.powspctrm(3,:,:) = mean(PTnormCoh,3,'omitnan');
% meanPTnorm.dimord = 'chan_freq_time';
% meanPTnorm.freq = foi;
% meanPTnorm.time = 1:(17+2*15);
% meanPTnorm.label = {'NAc Power'; 'BLA Power'; 'Coherence'};
%
% cfg                 = [];
% cfg.zlim            = 'maxabs';
% cfg.parameter       = 'powspctrm';
%
% for c = 1:numel(meanPTnorm.label)
%     cfg.channel = meanPTnorm.label{c};
%     figure;
%     ft_singleplotTFR(cfg, meanPTnorm);
%     colormap(jet);
%     xlabel('Tone #');
%     ylabel('Frequency (Hz)');
%     %   title(['PTnorm ' meanPTnorm.label{c} ' for Rat #' int2str(gr) ' Over Time']);
%     title(['Mean PTnorm ' meanPTnorm.label{c} ' for All Rats Over Time']);
%     %   print([figurefolder int2str(gr) 'PTnorm_' meanPTnorm.label{c}],'-dpng');
%     print([figurefolder 'meanPTnorm_' meanPTnorm.label{c} '_all'],'-dpng');
% end

%% exclude "bad" rats from mean BLnorm data
%
% meanBLnorm.powspctrm(1,:,:) = mean(BLnormPow_NAc(:,:,~ratT.excluded),3,'omitnan');
% meanBLnorm.powspctrm(2,:,:) = mean(BLnormPow_BLA(:,:,~ratT.excluded),3,'omitnan');
% meanBLnorm.powspctrm(3,:,:) = mean(BLnormCoh(:,:,~ratT.excluded),3,'omitnan');
% meanBLnorm.dimord = 'chan_freq_time';
% meanBLnorm.freq = foi;
% meanBLnorm.time = toi;
% meanBLnorm.label = {'NAc Power'; 'BLA Power'; 'Coherence'};
%
% cfg                 = [];
% cfg.zlim            = 'maxabs';
% cfg.parameter       = 'powspctrm';
%
% for c = 1:numel(meanBLnorm.label)
%     cfg.channel = meanBLnorm.label{c};
%     figure;
%     ft_singleplotTFR(cfg, meanBLnorm);
%     colormap(jet);
%     xlabel('Tone #');
%     ylabel('Frequency (Hz)');
%     title(['Mean BLnorm ' meanBLnorm.label{c} ' for 5 Rats Over Time']);
%     print([figurefolder 'meanBLnorm_' meanBLnorm.label{c} '_good'],'-dpng');
% end

%% exclude "bad" rats from mean PTnorm data

% meanPTnorm.powspctrm(1,:,:) = mean(PTnormPow_NAc(:,:,~ratT.excluded),3,'omitnan');
% meanPTnorm.powspctrm(2,:,:) = mean(PTnormPow_BLA(:,:,~ratT.excluded),3,'omitnan');
% meanPTnorm.powspctrm(3,:,:) = mean(PTnormCoh(:,:,~ratT.excluded),3,'omitnan');
% meanPTnorm.dimord = 'chan_freq_time';
% meanPTnorm.freq = foi;
% meanPTnorm.time = 1:(17+2*15);
% meanPTnorm.label = {'NAc Power'; 'BLA Power'; 'Coherence'};
%
% cfg                 = [];
% cfg.zlim            = 'maxabs';
% cfg.parameter       = 'powspctrm';
%
% for c = 1:numel(meanPTnorm.label)
%     cfg.channel = meanPTnorm.label{c};
%     figure;
%     ft_singleplotTFR(cfg, meanPTnorm);
%     colormap(jet);
%     xlabel('Tone #');
%     ylabel('Frequency (Hz)');
%     title(['Mean PTnorm ' meanPTnorm.label{c} ' for 5 Rats Over Time']);
%     print([figurefolder 'meanPTnorm_' meanPTnorm.label{c} '_good'],'-dpng');
% end
