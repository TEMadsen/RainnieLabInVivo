%% Average and merge spectrograms and coherograms from different subjects
% input can be raw/PTnorm/BLnorm
close all; clearvars; clc;    % clean slate

info_VPA_RC               % load all background info (filenames, etc)

plt = true;

bk2cal = [1, 14, 15, 18];
bk2calD = [1, 7, 8, 9];

% %% Average and Merge BL normalized data
% for g = 1:size(treatment,1)
%   for b = 1:numel(bk2cal)
%     for rn = 1:numel(eval([treatment{g} 'group']))
%       gr = eval([treatment{g} 'group' '(' int2str(rn) ')']);
%       freq2merge = cell(1, numel(gr));
%       r = find(ratT.ratnums == gr);
%       inputfile = [datafolder 'SpecCoherograms_' ...
%         int2str(ratT.ratnums(r)) blocks{bk2cal(b)} '_BLnorm.mat'];
%       load(inputfile);
%       if ratT.side{r} == 'Right'
%         freq.label{1} = 'NAc';
%         freq.label{2} = 'BLA';
%       elseif ratT.side{r} == 'Left'
%         freq.label{1} = 'BLA';
%         freq.label{2} = 'NAc';
%       end
%       freq2merge{gr} = freq;
%       clear freq
%     end
%     cfg = [];
%     cfg.parameter      = {'powspectrm';'coherence'};
%     freq = ft_freqgrandaverage(cfg, freq2merge{1},freq2merge{2},freq2merge{3},freq2merge{4});
%     outputfile = [datafolder 'AvgSpecCoherograms_' ...
%       treatment{g} blocks{bk2cal(b)} '_BLnorm.mat'];
%     save([datafolder outputfile '.mat'], freq, '-v6');
%     if plt
%       %% display raw spectrograms & coherograms
%
%       for target = 1:3 %#ok<UNRCH> can be reached if plt = true is set in top cell
%         cfg                 = [];
%
%         if target == 1
%           cfg.parameter       = 'powspctrm';
%           cfg.channel         = ratT.chNAc{r};
%         elseif target == 2
%           cfg.parameter       = 'powspctrm';
%           cfg.channel         = ratT.chBLA{r};
%         else
%           cfg.parameter       = 'coherence';
%           cfg.zlim            = 'zeromax';
%           cfg.channel         = ratT.chNAc{r};
%           cfg.refchannel      = ratT.chBLA{r};
%         end
%
%         if ~strcmp('none',cfg.channel) && ...
%             (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
%           figure(target);  clf;
%           ft_singleplotTFR(cfg, freq);
%           colormap(jet);
%           xlabel('Time from Tone Onset (seconds)');
%           ylabel('Frequency (Hz)');
%           if target < 3
%             title([treatment{g} 'Averaged ' ...
%               blocks{bk2cal(b)} ', ' ...
%               regions{target} ' BLnorm_Power']);
%             print([figurefolder treatment{g}...
%               blocks{bk2cal(b)} '_Averaged_BLnorm_Spectrogram_'...
%               regions{target}],'-dpng');
%           else
%             title([treatment{g} 'Averaged' ...
%               blocks{bk2cal(b)}  ', BLnorm_Coherence']);
%             print([figurefolder treatment{g} int2str(gr) ...
%               blocks{bk2cal(b)} cfg.channel '_Averaged_BLnorm_Coherogram'],'-dpng');
%           end
%         end
%       end
%     end
%     clear freq2merge freq
%   end
% end

%% Average and Merge PT normalized data
for g = 1:size(treatment,1)
  for b = 1:numel(bk2cal)
    freq2merge = cell(numel(eval([treatment{g} 'group'])),1);
    for rn = 1:numel(eval([treatment{g} 'group']))
      gr = eval([treatment{g} 'group' '(' int2str(rn) ')']);
      r = find(ratT.ratnums == gr);
      inputfile = [datafolder 'SpecCoherograms_' ...
        int2str(ratT.ratnums(r)) blocks{bk2cal(b)} '_PTnorm.mat'];
      load(inputfile);
      if strcmp(ratT.side{r},'Right') == 1
        freq.label{1} = 'NAc';
        freq.label{2} = 'BLA';
      elseif strcmp(ratT.side{r},'Left') == 1
        freq.label{1} = 'BLA';
        freq.label{2} = 'NAc';
      end
      freq.label{3} = 'BLA-NAc';
      freq.powspctrm = cat(1, freq.powspctrm, freq.coherence);
      outputfile = [datafolder 'SpecCoherograms_' ...
        int2str(ratT.ratnums(r)) blocks{bk2cal(b)} '_PTnorm_ElcTargetC.mat'];
      freq2merge{rn} = outputfile;
      save(outputfile, 'freq', '-v6');
      clear freq
    end
    %% grand average of spectrograms
    
    cfg                = [];
    cfg.parameter      = {'powspctrm'};
    cfg.inputfile      = freq2merge;
    %     cfg.channel        = {'NAc';'BLA'};
    freq = ft_freqgrandaverage(cfg);
    outputfile = [datafolder 'AvgSpectrograms_' ...
      treatment{g} blocks{bk2cal(b)} '_PTnorm.mat'];
    save(outputfile, 'freq', '-v6');
    %     freq1 = freq;
    %     clear freq
    %     %% grand average of coherograms
    %
    %     cfg                = [];
    %     cfg.parameter      = {'coherence'};
    %     cfg.inputfile      = freq2merge;
    %     cfg.channel        = {'NAc';'BLA'};
    %     freq = ft_freqgrandaverage(cfg);
    %     outputfile = [datafolder 'AvgSpectrograms_' ...
    %       treatment{g} blocks{bk2cal(b)} '_PTnorm.mat'];
    %     save(outputfile, 'freq', '-v6');
    %     freq2 = freq;
    %     clear file2merge freq
    
    if plt
      %% display raw spectrograms & coherograms
      
      for target = 1:3 %#ok<UNRCH> can be reached if plt = true is set in top cell
        cfg                 = [];
        
        cfg.parameter       = 'powspctrm';
        cfg.channel         = target;
        cfg.zlim           = 'maxabs';
        figure(target);  clf;
        ft_singleplotTFR(cfg, freq);
        colormap(jet);
        xlabel('Time from Tone Onset (seconds)');
        ylabel('Frequency (Hz)');
        
        if target < 3
          title([treatment{g} 'Averaged ' ...
            blocks{bk2cal(b)} ', ' ...
            regions{target} ' PTnorm_Power']);
          print([figurefolder treatment{g}...
            blocks{bk2cal(b)} '_Averaged_PTnorm_Spectrogram_'...
            regions{target}],'-dpng');
        else
          title([treatment{g} 'Averaged' ...
            blocks{bk2cal(b)}  ', PTnorm_Coherence']);
          print([figurefolder treatment{g} ...
            blocks{bk2cal(b)} freq.label{target} '_Averaged_PTnorm_Coherogram'],'-dpng');
        end
      end
      clear freq %1 freq2
    end
  end
  clear freq2merge freq
end