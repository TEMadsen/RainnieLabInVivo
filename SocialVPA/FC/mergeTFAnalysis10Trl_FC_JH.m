% This script merges data that contains multiple recording files in a day.
% The script also calculates and plot pre-tone normalized power and
% coherence values with group plotting.

%% Housekeeping
clearvars; close all force;
clc
info_VPA_RC   % load all background info (filenames, etc)

%% Parameters
foi = 1.5:0.5:100;   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%

day2cal = [1,8];

%% merge

for jj = 1:numel(day2cal)
    outputfile = [datafolder 'FullData' filesep 'FC' filesep ...
        'FullTFanalysis10TrlD' int2str(day2cal(jj)) '.mat'];
    %% Preallocate spaces
    NAcPowAll = NaN(size(ratT,1),6,numel(foi),numel(toi));
    BLAPowAll = NaN(size(ratT,1),6,numel(foi),numel(toi));
    CohAll = NaN(size(ratT,1),6,numel(foi),numel(toi));
    if exist(outputfile,'file')
        disp([outputfile ' already exists.  Skipping rat.'])
    else
        for ii = 1:size(ratT,1)
            for kk = 1:6
                inputfile = [datafolder 'FullData' filesep 'FC' filesep 'TFAnalysis' filesep ...
                    'SpecCoherograms_' int2str(ratT.ratnums(ii)) phases{day2cal(jj)} '10trl_' int2str(kk) '.mat'];
                if ~exist(inputfile,'file')
                    error([inputfile ' does not exist. Please check.'])
                else
                    load(inputfile)
                    NAcPowAll(ii,kk,:,:) = freq.powspctrm(1,:,:);
                    BLAPowAll(ii,kk,:,:) = freq.powspctrm(2,:,:);
                    CohAll(ii,kk,:,:) = abs(freq.crsspctrm(1,:,:)./ ...
                        sqrt(freq.powspctrm(1,:,:).* ...
                        freq.powspctrm(2,:,:)));
                    clear freq
                end
            end
        end
        save(outputfile, 'NAcPowAll','BLAPowAll','CohAll')
    end
end