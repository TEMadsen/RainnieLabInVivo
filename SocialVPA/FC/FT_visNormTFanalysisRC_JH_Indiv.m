%% Visualize normalized time-frequency analysis results

close all; clearvars; clc;    % clean slate

info_VPA_RC                    % load all background info (filenames, etc)

foi = 1.20.^(-1:28);   % freqs from 1-103 Hz progressing up by 20% each
toi = -6:0.5:12;  % all tones + before & after times
plt = true;                  % if plots are desired for each tone and rat
errcnt = 0;                   % error count
errloc = [];                  % error location (r,p,t)

% bk2cal = [1,2,3,4,5,6,7,8,9,10,11,12,19,20,21,22,23,24,25,26,27];
% bk2calD = [1,1,1,2,2,2,3,3,3,4,4,4,7,7,7,8,8,8,9,9,9];

%% use try-catch when running unsupervised batches

for g = 1:size(treatment,1)
    for tr = 1:sum(eval([treatment{g} 'Ind']))
        gr = groups
        gr = eval([treatment{g} 'group' '(' int2str(tr) ')']);
        r = find(ratT.ratnums == gr);
        %% gather baseline data
        baselinefile = [datafolder 'SpecCoherograms_' int2str(ratT.ratnums(r)) ...
            'Baseline.mat'];
        
        if ~exist(baselinefile,'file')
            error([baselinefile ' does not exist!']);
        end
        
        %% load data
        
        BL = load(baselinefile);
        
        %% check for accuracy
        
        %     if BL.freq.time(1) >= 0
        %       error('no pre-tone baseline found');
        %     else
        %       disp(['first timestamp is ' num2str(BL.freq.time(1))]);
        %     end
        
        if strcmp(goodchs{r}{1},'AD*')
            if numel(BL.freq.label) ~= 2
                error('different channels used than planned');
            else
                for ch = 1:numel(BL.freq.label)
                    k = strfind(goodchs{r}, BL.freq.label{ch});
                    if any([k{:}])
                        error([BL.freq.label{ch} ' should be excluded']);
                    else
                        disp([BL.freq.label{ch} ' is not excluded']);
                    end
                end
            end
        elseif numel(BL.freq.label) ~= numel(goodchs{r}) || ...
                any(any(~strcmp(BL.freq.label,goodchs{r})))
            error('different channels used than planned');
        else
            disp([BL.freq.label{:} ' are correct']);   % still true
        end
        
        maxdiff = max(abs(BL.freq.freq-foi));
        if maxdiff > 0.02
            error([baselinefile ' does not have the same ' ...
                'frequency bins as foi']);
        else
            disp(['max difference from foi = ' num2str(maxdiff)]);
        end
        
        %% translate channel names into indices
        
        indx = NaN(size(BL.freq.labelcmb));
        
        for i = 1:size(BL.freq.labelcmb,1)
            % find 1st ch index
            indx(i,1) = find(strcmp(BL.freq.labelcmb(i,1),BL.freq.label));
            
            % find 2nd ch index
            indx(i,2) = find(strcmp(BL.freq.labelcmb(i,2),BL.freq.label));
        end
        
        %% calculate coherence
        
        % preallocate space
        BL.freq.coherence   = NaN(size(BL.freq.crsspctrm));
        
        for i = 1:size(indx,1)
            BL.freq.coherence(i,:,:) = abs(BL.freq.crsspctrm(i,:,:)./ ...
                sqrt(BL.freq.powspctrm(indx(i,1),:,:).* ...
                BL.freq.powspctrm(indx(i,2),:,:)));
        end
        
        %% average baseline data
        
        meanBLpow = mean(BL.freq.powspctrm,3,'omitnan');
        meanBLcoh = mean(BL.freq.coherence,3,'omitnan');
        
        %% normalize all other spec/coherograms by baseline
        
        for b = 1:numel(bk2cal)
            if ~isempty(nexfile{r,1}) % no need for an error if there was no recording
                
                inputfile  = [datafolder 'SpecCoherograms_' ...
                    int2str(gr) blocks{bk2cal(b)} '.mat'];
                
                if ~exist(inputfile,'file')
                    error([inputfile ' does not exist!']);
                end
                %% load data
                load(inputfile);
                
                %% check for accuracy
                
                if freq.time(1) >= 0
                    error('no pre-tone baseline found');
                else
                    disp(['first timestamp is ' num2str(freq.time(1))]);
                end
                
                if strcmp(goodchs{r}{1},'AD*')
                    if numel(freq.label) ~= 2
                        error('different channels used than planned');
                    else
                        for ch = 1:numel(freq.label)
                            k = strfind(goodchs{r}, freq.label{ch});
                            if any([k{:}])
                                error([freq.label{ch} ' should be excluded']);
                            else
                                disp([freq.label{ch} ' is not excluded']);
                            end
                        end
                    end
                elseif numel(freq.label) ~= numel(goodchs{r}) || ...
                        any(any(~strcmp(freq.label,goodchs{r})))
                    error('different channels used than planned');
                else
                    disp([freq.label{:} ' are correct']);   % still true
                end
                
                maxdiff = max(abs(freq.freq-foi));
                if maxdiff > 0.01
                    error([inputfile ' does not have the same ' ...
                        'frequency bins as foi']);
                else
                    disp(['max difference from foi = ' num2str(maxdiff)]);
                end
                
                %% check input compatibility
                
                if numel(freq.labelcmb) ~= numel(BL.freq.labelcmb) || ...
                        any(any(~strcmp(freq.labelcmb,BL.freq.labelcmb)))
                    error([inputfile ' does not have the same ' ...
                        'channel combinations as ' baselinefile]);
                end
                
                %% calculate coherence
                
                % preallocate space
                freq.coherence      = NaN(size(freq.crsspctrm));
                
                for i = 1:size(indx,1)
                    freq.coherence(i,:,:) = abs(freq.crsspctrm(i,:,:)./ ...
                        sqrt(freq.powspctrm(indx(i,1),:,:).* ...
                        freq.powspctrm(indx(i,2),:,:)));
                end
                
                %% duplicate variable for storePTnormfreq
                
                PTnormfreq = freq;   % duplicate variable
                
                %% normalize by baseline period
                
                
                BLnormfreq = freq;   % duplicate variable
                
                % normalization method 'db'
                BLnormfreq.powspctrm = 10*log10(freq.powspctrm ./ ...
                    repmat(meanBLpow,[1 1 numel(freq.time)]));
                
                % normalization method 'vssum'
                BLnormfreq.coherence = (freq.coherence - repmat(meanBLcoh,...
                    [1 1 numel(freq.time)])) ./ ...
                    (freq.coherence + repmat(meanBLcoh,[1 1 numel(freq.time)]));
                
                %% plot raw, BLnorm, & PTnorm spectrograms & coherograms
                
                if plt
                    %% display raw spectrograms & coherograms
                    
                    for target = 1:3 %#ok<UNRCH> can be reached if plt = true is set in top cell
                        cfg                 = [];
                        
                        if target == 1
                            cfg.parameter       = 'powspctrm';
                            cfg.channel         = ratT.chNAc{r};
                        elseif target == 2
                            cfg.parameter       = 'powspctrm';
                            cfg.channel         = ratT.chBLA{r};
                        else
                            cfg.parameter       = 'coherence';
                            cfg.zlim            = 'zeromax';
                            cfg.channel         = ratT.chNAc{r};
                            cfg.refchannel      = ratT.chBLA{r};
                        end
                        
                        if ~strcmp('none',cfg.channel) && ...
                                (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
                            figure(target);  clf;
                            ft_singleplotTFR(cfg, freq);
                            colormap(jet);
                            xlabel('Time from Tone Onset (seconds)');
                            ylabel('Frequency (Hz)');
                            if target < 3
                                title([treatment{g} 'Rat ' ...
                                    int2str(gr) ...
                                    blocks{bk2cal(b)} ', ' ...
                                    regions{target} cfg.channel ' Power']);
                                print([figurefolder treatment{g}...
                                    int2str(gr) ...
                                    blocks{bk2cal(b)} '_Spectrogram_'...
                                    regions{target} cfg.channel],'-dpng');
                            else
                                title([treatment{g} 'Rat ' int2str(gr) ...
                                    blocks{bk2cal(b)} cfg.channel ', Coherence']);
                                print([figurefolder treatment{g} int2str(gr) ...
                                    blocks{bk2cal(b)} cfg.channel '_Coherogram'],'-dpng');
                            end
                        end
                    end
                    
%                     %% display baseline normalized spectrograms & coherograms
%                     
%                     for target = 1:3
%                         cfg                 = [];
%                         cfg.zlim            = 'maxabs';
%                         
%                         if target == 1
%                             cfg.parameter       = 'powspctrm';
%                             cfg.channel         = ratT.chNAc{r};
%                         elseif target == 2
%                             cfg.parameter       = 'powspctrm';
%                             cfg.channel         = ratT.chBLA{r};
%                         else
%                             cfg.parameter       = 'coherence';
%                             cfg.channel         = ratT.chNAc{r};
%                             cfg.refchannel      = ratT.chBLA{r};
%                         end
%                         
%                         if ~strcmp('none',cfg.channel) && ...
%                                 (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
%                             figure(target+3);  clf;
%                             ft_singleplotTFR(cfg, BLnormfreq);
%                             colormap(jet);
%                             xlabel('Time from Tone Onset (seconds)');
%                             ylabel('Frequency (Hz)');
%                             if target < 3
%                                 title([treatment{g} 'Rat ' ...
%                                     int2str(gr) ...
%                                     blocks{bk2cal(b)} ', ' ...
%                                     regions{target} cfg.channel ' Power(dB change from baseline) ']);
%                                 print([figurefolder treatment{g}...
%                                     int2str(gr) ...
%                                     blocks{bk2cal(b)} '_BLnormSpectrogram_'...
%                                     regions{target} cfg.channel],'-dpng');
%                             else
%                                 title([treatment{g} 'Rat ' int2str(gr) ...
%                                     blocks{bk2cal(b)} cfg.channel ', Coherence']);
%                                 print([figurefolder treatment{g} int2str(gr) ...
%                                     blocks{bk2cal(b)} cfg.channel '_Coherogram (change from baseline)'],'-dpng');
%                             end
%                         end
%                     end
                    
                    
                    %% display pre-tone normalized spectrograms & coherograms
                    
                    for target = 1:3
                        cfg                 = [];
                        cfg.baseline        = [-6 0];
                        cfg.zlim            = 'maxabs';
                        
                        if target == 1
                            cfg.baselinetype    = 'db';
                            cfg.parameter       = 'powspctrm';
                            cfg.channel         = ratT.chNAc{r};
                        elseif target == 2
                            cfg.baselinetype    = 'db';
                            cfg.parameter       = 'powspctrm';
                            cfg.channel         = ratT.chBLA{r};
                        else
                            cfg.baselinetype    = 'vssum';
                            cfg.parameter       = 'coherence';
                            cfg.channel         = ratT.chNAc{r};
                            cfg.refchannel      = ratT.chBLA{r};
                        end
                        
                        if ~strcmp('none',cfg.channel) && ...
                                (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
                            figure(target+6);  clf;
                            ft_singleplotTFR(cfg, freq);
                            colormap(jet);
                            xlabel('Time from Tone Onset (seconds)');
                            ylabel('Frequency (Hz)');
                            
                            if target < 3
                                title([treatment{g} 'Rat ' ...
                                    int2str(gr) ...
                                    blocks{bk2cal(b)} ', ' ...
                                    regions{target} cfg.channel ' Power(dB change from baseline) ']);
                                print([figurefolder treatment{g}...
                                    int2str(gr) ...
                                    blocks{bk2cal(b)} '_PLnormSpectrogram_'...
                                    regions{target} cfg.channel],'-dpng');
                            else
                                title([treatment{g} 'Rat ' int2str(gr) ...
                                    blocks{bk2cal(b)} cfg.channel ', Coherence']);
                                print([figurefolder treatment{g} int2str(gr) ...
                                    blocks{bk2cal(b)} cfg.channel '_PLnormoherogram'],'-dpng');
                            end
                        end
                    end
                end
            end
            %% resave BL and PL normalized data
            for NML = 1:2
                if NML == 1
                    %% average pre-tone data
                    tB = freq.time<=0 & freq.time> -6;
                    meanPTpow = mean(freq.powspctrm(:,:,tB),3,'omitnan');
                    meanPTcoh = mean(freq.coherence(:,:,tB),3,'omitnan');
                    
                    %% normalize by pre-tone period
                    PTnormfreq = freq;
                    % normalization method 'db'
                    PTnormfreq.powspctrm = 10*log10(freq.powspctrm ./ ...
                        repmat(meanPTpow,[1 1 numel(freq.time)]));
                    
                    % normalization method 'vssum'
                    PTnormfreq.coherence = (freq.coherence - repmat(meanPTcoh,...
                        [1 1 numel(freq.time)])) ./ (freq.coherence ...
                        + repmat(meanPTcoh,[1 1 numel(freq.time)]));
                    
                    %% resave as _PTnorm.mat
                    
                    freq = PTnormfreq;
                    outputfile = [datafolder 'SpecCoherograms_' ...
                        int2str(ratT.ratnums(r)) blocks{bk2cal(b)} '_PTnorm.mat'];
                    save(outputfile, 'freq', '-v6');
                    clear freq PTnormfreq
                elseif NML == 2
                    %% resave as _BLnorm.mat
                    freq = BLnormfreq;   % assign back to freq
                    outputfile = [datafolder 'SpecCoherograms_' ...
                        int2str(ratT.ratnums(r)) blocks{bk2cal(b)} '_BLnorm.mat'];
                    save(outputfile, 'freq', '-v6');
                    clear freq BLnormfreq
                end
            end
        end
        %% clear data for next file
        clearvars cfg BL
    end
end

