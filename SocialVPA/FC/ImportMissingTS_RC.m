% The function of this script is to import missing timestamps from RC D3-6
% of rats TEM361-364 to a cell structure and save the variable
% 361to4D3to6TS.mat

%% import missing timestamps
ratnums = [361; 362; 363; 364];
phases = {'ConditioningDay3', 'ConditioningDay4', 'ConditioningDay5', ...
    'ConditioningDay6'};
TSRaw = cell(numel(ratnums),numel(phases));

for r = 1:numel(ratnums)
    for p = 1:numel(phases)
        [~, ~, raw] = xlsread([tserver 'Behavior' filesep ...
            'Reinforcement conditioning' filesep ...
            'missing tone timestamps' filesep num2str(ratnums(r)) ...
            '_RC_D' num2str(p+2) '_tone.xlsx'],[num2str(ratnums(r)) ...
            '_RC_D' num2str(p+2) '_tone']);
        raw = raw(21:end,12);
        TSRaw{r,p} = reshape([raw{:}],size(raw));
        clearvars raw;
    end
end


%% process TS
% Convert the unit of TS from seconds to miliseconds
% Round all TS to milisecond

TS = cell(numel(ratnums),numel(phases));
for r = 1:numel(ratnums)
    for p = 1:numel(phases)
        TS{r,p} = round(TSRaw{r,p}*1000);
    end
end

MisTS = TS;
%% save missing timestamps to .mat
save MisTS.mat  MisTS