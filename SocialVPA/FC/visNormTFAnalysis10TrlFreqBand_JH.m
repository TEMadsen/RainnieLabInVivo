%% House keeping
close all; clearvars; clc;    % clean slate
set(0,'DefaultFigureWindowStyle','docked') % dock figures

%% parameters
info_VPA_RC                    % load all background info (filenames, etc)
foi = 1.5:0.5:120;   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%

plt = true;                  % if plots are desired for each tone and rat
errcnt = 0;                   % error count
errloc = [];                  % error location (r,p,t)

bk2cal = 1:9;
bk2calD = [1,1,1,2,2,2,3,3,3];
days = 1:9;
toiBaseline = find(toi < -0.5 & toi > -1);
toiTone = find(toi > 0.2 & toi < 5.8);

foiDelta = find(foi > 2 & foi < 5); %10-30
foiTheta = find(foi > 5 & foi < 8); %10-30
foiAlpha = find(foi > 8 & foi < 14); %10-30
foiHGamma = find(foi > 50 & foi < 100); %80-110
fboi = {foiDelta;foiTheta;foiAlpha;foiHGamma};

SALsize = sum(strcmp(ratT{:,7},'Saline'));
VPAsize = sum(strcmp(ratT{:,7},'VPA'));



%% preallocation

SALNAcPowNorm = NaN(numel(bk2cal),SALsize,numel(foi),numel(toi));
SALBLAPowNorm = NaN(numel(bk2cal),SALsize,numel(foi),numel(toi));
SALCohNorm = NaN(numel(bk2cal),SALsize,numel(foi),numel(toi));
VPANAcPowNorm = NaN(numel(bk2cal),VPAsize,numel(foi),numel(toi));
VPABLAPowNorm = NaN(numel(bk2cal),VPAsize,numel(foi),numel(toi));
VPACohNorm = NaN(numel(bk2cal),VPAsize,numel(foi),numel(toi));

SALNAcPowFreqNorm = NaN(numel(bk2cal),SALsize,4,numel(toi)); %2nd dim = 4 freq bands
SALBLAPowFreqNorm = NaN(numel(bk2cal),SALsize,4,numel(toi)); %2nd dim = 4 freq bands
SALCohFreqNorm = NaN(numel(bk2cal),SALsize,4,numel(toi)); %2nd dim = 4 freq bands
VPANAcPowFreqNorm = NaN(numel(bk2cal),VPAsize,4,numel(toi)); %2nd dim = 4 freq bands
VPABLAPowFreqNorm = NaN(numel(bk2cal),VPAsize,4,numel(toi)); %2nd dim = 4 freq bands
VPACohFreqNorm = NaN(numel(bk2cal),VPAsize,4,numel(toi)); %2nd dim = 4 freq bands

for bb = 1:numel(bk2cal)
    outputfileVPA = [datafolder 'FC' filesep 'SpecCoherograms_' ...
        'VPA_' int2str(bk2cal(bb)) 'of27_20trl.mat'];
    outputfileSAL = [datafolder 'FC' filesep 'SpecCoherograms_' ...
        'SAL_' int2str(bk2cal(bb)) 'of27_20trl.mat'];
    VPACohbaseline =  NaN(VPAsize,numel(foi));
    VPAPowbaseline = NaN(VPAsize,2,numel(foi));
    VPAPow = NaN(VPAsize,2,numel(foi),numel(toi));
    VPACoh = NaN(VPAsize, numel(foi),numel(toi));
    
    SALCohbaseline =  NaN(SALsize,numel(foi));
    SALPowbaseline =  NaN(SALsize,2,numel(foi));
    SALPow = NaN(SALsize,2,numel(foi),numel(toi));
    SALCoh = NaN(SALsize, numel(foi),numel(toi));
    
    for rr = 1:size(ratT,1)
        gr = ratT{rr,7};
        
        inputfile = [datafolder 'FC' filesep 'SpecCoherograms_' ...
            int2str(ratT.ratnums(rr)) '_' int2str(bk2cal(bb)) 'of27_linscale_ext.mat'];
        
        load(inputfile)
        
        % calculate coherence
        freq.coherence      = NaN(size(freq.crsspctrm));
        freq.coherence(1,:,:) = abs(freq.crsspctrm(1,:,:)./ ...
            sqrt(freq.powspctrm(1,:,:).* ...
            freq.powspctrm(2,:,:)));

        % reorganize data to new structure
        if strcmp(gr, 'VPA') ==1
            grID = sum(VPAInd(1:rr));
            
            VPAPow(grID,:,:,:) = freq.powspctrm;
            VPAPowbaseline(grID,:,:) = nanmean(freq.powspctrm(:,:,toiBaseline),3);
            VPACoh(grID,:,:) = squeeze(freq.coherence);
            VPACohbaseline(grID,:) = nanmean(squeeze(freq.coherence(:,:,toiBaseline)),2);
            
        elseif strcmp(gr, 'Saline') ==1
            grID = sum(SALInd(1:rr));
            
            SALPow(grID,:,:,:) = freq.powspctrm;
            SALPowbaseline(grID,:,:) = nanmean(freq.powspctrm(:,:,toiBaseline),3);
            SALCoh(grID,:,:) = squeeze(freq.coherence);
            SALCohbaseline(grID,:) = nanmean(squeeze(freq.coherence(:,:,toiBaseline)),2);
            
        end
        clear freq
    end
    
    save(outputfileVPA, 'VPAPow','VPAPowbaseline','VPACoh','VPACohbaseline')
    save(outputfileSAL, 'SALPow','SALPowbaseline','SALCoh','SALCohbaseline')
    
end

sum(sum(sum(sum(isnan(VPACoh)))))

for bb = 1:numel(bk2cal)
    inputfile1 = [datafolder 'FC' filesep 'SpecCoherograms_' ...
        'VPA_' int2str(bk2cal(bb)) 'of27_20trl.mat'];
    inputfile2 = [datafolder 'FC' filesep 'SpecCoherograms_' ...
        'SAL_' int2str(bk2cal(bb)) 'of27_20trl.mat'];
    
    load(inputfile1)
    load(inputfile2)
    for irr = 1:size(VPAPow,1)
        for ff = 1:size(VPAPow,3)
            SALNAcPowNorm(bb,irr,ff,:) = SALPow(irr,1,ff,:)./SALPowbaseline(irr,1,ff);
            SALBLAPowNorm(bb,irr,ff,:) = SALPow(irr,2,ff,:)./SALPowbaseline(irr,2,ff);
            VPANAcPowNorm(bb,irr,ff,:) = VPAPow(irr,1,ff,:)./VPAPowbaseline(irr,1,ff);
            VPABLAPowNorm(bb,irr,ff,:) = VPAPow(irr,2,ff,:)./VPAPowbaseline(irr,2,ff);
            VPACohNorm(bb,irr,ff,:) = VPACoh(irr,ff,:) - VPACohbaseline(irr,ff);
            SALCohNorm(bb,irr,ff,:) = SALCoh(irr,ff,:) - SALCohbaseline(irr,ff);
        end
    end
end
sum(sum(sum(sum(isnan(SALNAcPowNorm)))))
sum(sum(sum(isnan(freq.powspctrm))))

% converting power to dB
SALNAcPowNorm = 10.*log10(SALNAcPowNorm);
SALBLAPowNorm = 10.*log10(SALBLAPowNorm);
VPANAcPowNorm = 10.*log10(VPANAcPowNorm);
VPABLAPowNorm = 10.*log10(VPABLAPowNorm);

outputfile1 = [datafolder 'FC' filesep 'NormalizedPowCoh_LinScale_Acq20trl.mat'];

save(outputfile1, 'SALNAcPowNorm','SALBLAPowNorm','VPANAcPowNorm',...
    'VPABLAPowNorm','VPACohNorm','SALCohNorm');


%% average across rats
for fb = 1:size(fboi,1)
    SALNAcPowFreqNorm(:,:,fb,:) = nanmean(SALNAcPowNorm(:,:,fboi{fb},:),3);
    SALBLAPowFreqNorm(:,:,fb,:) = nanmean(SALBLAPowNorm(:,:,fboi{fb},:),3);
    SALCohFreqNorm(:,:,fb,:) = nanmean(SALCohNorm(:,:,fboi{fb},:),3);
    VPANAcPowFreqNorm(:,:,fb,:) = nanmean(VPANAcPowNorm(:,:,fboi{fb},:),3);
    VPABLAPowFreqNorm(:,:,fb,:) = nanmean(VPABLAPowNorm(:,:,fboi{fb},:),3);
    VPACohFreqNorm(:,:,fb,:) = nanmean(VPACohNorm(:,:,fboi{fb},:),3);
end

SALNAcPowNormdurtone = squeeze(nanmean(SALNAcPowFreqNorm(:,:,:,toiTone),4));
SALBLAPowNormdurtone = squeeze(nanmean(SALBLAPowFreqNorm(:,:,:,toiTone),4));
SALCohNormdurtone = squeeze(nanmean(SALCohFreqNorm(:,:,:,toiTone),4));
VPANAcPowNormdurtone = squeeze(nanmean(VPANAcPowFreqNorm(:,:,:,toiTone),4));
VPABLAPowNormdurtone = squeeze(nanmean(VPABLAPowFreqNorm(:,:,:,toiTone),4));
VPACohNormdurtone = squeeze(nanmean(VPACohFreqNorm(:,:,:,toiTone),4));
dimOrd = {'day','rats','fboi'};
outputfile2 = [datafolder 'FC' filesep 'NormalizedPowCohdurTone_Acq20Trl.mat'];
save(outputfile2,'SALNAcPowNormdurtone','SALBLAPowNormdurtone','SALCohNormdurtone', ...
    'VPANAcPowNormdurtone','VPABLAPowNormdurtone','VPACohNormdurtone','fboi','dimOrd')


NAcVPADelta = squeeze(VPANAcPowNormdurtone(:,:,1));
NAcSALDelta = squeeze(SALNAcPowNormdurtone(:,:,1));
NAcVPATheta = squeeze(VPANAcPowNormdurtone(:,:,2));
NAcSALTheta = squeeze(SALNAcPowNormdurtone(:,:,2));
NAcVPAAlpha = squeeze(VPANAcPowNormdurtone(:,:,3));
NAcSALAlPha = squeeze(SALNAcPowNormdurtone(:,:,3));
NAcVPAHGamma = squeeze(VPANAcPowNormdurtone(:,:,4));
NAcSALHGamma = squeeze(SALNAcPowNormdurtone(:,:,4));

BLAVPADelta = squeeze(VPABLAPowNormdurtone(:,:,1));
BLASALDelta = squeeze(SALBLAPowNormdurtone(:,:,1));
BLAVPATheta = squeeze(VPABLAPowNormdurtone(:,:,2));
BLASALTheta = squeeze(SALBLAPowNormdurtone(:,:,2));
BLAVPAAlpha = squeeze(VPABLAPowNormdurtone(:,:,3));
BLASALAlPha = squeeze(SALBLAPowNormdurtone(:,:,3));
BLAVPAHGamma = squeeze(VPABLAPowNormdurtone(:,:,4));
BLASALHGamma = squeeze(SALBLAPowNormdurtone(:,:,4));

CohVPADelta = squeeze(VPACohNormdurtone(:,:,1));
CohSALDelta = squeeze(SALCohNormdurtone(:,:,1));
CohVPATheta = squeeze(VPACohNormdurtone(:,:,2));
CohSALTheta = squeeze(SALCohNormdurtone(:,:,2));
CohVPAAlpha = squeeze(VPACohNormdurtone(:,:,3));
CohSALAlPha = squeeze(SALCohNormdurtone(:,:,3));
CohVPAHGamma = squeeze(VPACohNormdurtone(:,:,4));
CohSALHGamma = squeeze(SALCohNormdurtone(:,:,4));


CohVPAContrast = squeeze(VPACohNormdurtone(:,:,2))...
    +abs(squeeze(VPACohNormdurtone(:,:,1)))++abs(squeeze(VPACohNormdurtone(:,:,3)));
CohSALContrast = squeeze(SALCohNormdurtone(:,:,2))...
    +abs(squeeze(SALCohNormdurtone(:,:,1)))++abs(squeeze(SALCohNormdurtone(:,:,3)));
BLAVPAContrast = squeeze(VPABLAPowNormdurtone(:,:,2))...
    +abs(squeeze(VPABLAPowNormdurtone(:,:,1)))++abs(squeeze(VPABLAPowNormdurtone(:,:,3)));
BLASALContrast = squeeze(SALBLAPowNormdurtone(:,:,2))...
    +abs(squeeze(SALBLAPowNormdurtone(:,:,1)))++abs(squeeze(SALBLAPowNormdurtone(:,:,3)));
NAcVPAContrast = squeeze(VPANAcPowNormdurtone(:,:,2))...
    +abs(squeeze(VPANAcPowNormdurtone(:,:,1)))++abs(squeeze(VPANAcPowNormdurtone(:,:,3)));
NAcSALContrast = squeeze(SALNAcPowNormdurtone(:,:,2))...
    +abs(squeeze(SALNAcPowNormdurtone(:,:,1)))++abs(squeeze(SALNAcPowNormdurtone(:,:,3)));

dimOrd2 = {'day','fboi'};
outputfile3 = [datafolder 'FC' filesep 'NormalizedPowCohdurTone2d_Acq20Trl.mat'];
save(outputfile3,'NAc*','BLA*','Coh*','fboi','dimOrd2')
