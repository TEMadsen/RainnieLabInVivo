%% Redefine trails to 6 s bins for Cross-Frequency Coupling
% The redefination is needed for an unbiased comparison of modulation index
% between interaction targets or phases
% Use FFT-friendly time length (sample number should be power of 2 (eg.
% 4.096s can be used when sampling rate is 1000 Hz).

close all force; clearvars; clc;  % clean slate

info_VPA_RC                       % load all background info (filenames, etc)

%% full loop

for ii = 1:size(ratT,1)
    
    inputfile = [datafolder 'FullData' filesep 'test' filesep 'RawLFPTrialData_' ...
        int2str(ratT.ratnums(r)) 'Merged.mat'];
    outputfile = [datafolder 'FullData' filesep 'test' filesep 'RawLFPTrialData_' ...
        int2str(ratT.ratnums(r)) 'Merged_NoExtend.mat'];
    
    cfg = [];
    cfg.toilim      = [-9 15];
    cfg.inputfile   = inputfile;
    cfg.outputfile  = outputfile;
    % Use as: data = ft_redefinetrial(cfg, data)
    
    ft_redefinetrial(cfg);
    
end