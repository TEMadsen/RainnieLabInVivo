%% Redefine trails to 6 s bins for Cross-Frequency Coupling
% The redefination is needed for an unbiased comparison of modulation index
% between interaction targets or phases
% Use FFT-friendly time length (sample number should be power of 2 (eg.
% 4.096s can be used when sampling rate is 1000 Hz).

close all force; clearvars; clc;  % clean slate

info_VPA_RC                       % load all background info (filenames, etc)

%% full loop

for r = 1:size(ratT,1)
    for dd = 1:numel(phases)
        if ~iscell(nexfile{r,dd})  % in case of multiple files per phase
            nexfile{r,dd} = nexfile(r,dd);  % convert to cellstr
        end
        for fn = 1:numel(nexfile{r,dd})  % file number w/in a phase
            inputfile = [datafolder filesep 'FullData' filesep 'clean' filesep ...
                'CleanLFPTrialData_' int2str(ratT.ratnums(r)) phases{dd} num2str(fn) '.mat'];
            outputfile = [datafolder filesep 'FullData' filesep 'clean' filesep ...
                'CleanLFPTrialData_' int2str(ratT.ratnums(r)) phases{dd} num2str(fn) 'CFCdurTone.mat'];;
            
            cfg = [];
            cfg.toilim      = [0.2 5.8];
            cfg.inputfile   = inputfile;
            cfg.outputfile  = outputfile;
            % Use as: data = ft_redefinetrial(cfg, data)
            
            ft_redefinetrial(cfg);
        end
    end
end