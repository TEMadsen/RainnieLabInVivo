%% Visualize normalized time-frequency analysis results

close all; clearvars; clc;    % clean slate

info_VPA_RC                    % load all background info (filenames, etc)

foi = 1.3.^(1.5:0.25:19);   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -6:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%

plt = true;                  % if plots are desired for each tone and rat
errcnt = 0;                   % error count
errloc = [];                  % error location (r,p,t)

bk2cal = 1:27;
bk2calD = [1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8,9,9,9];

SALsize = sum(strcmp(ratT{:,7},'Saline'));
VPAsize = sum(strcmp(ratT{:,7},'VPA'));

VPAPowNormBeta = NaN(VPAsize,2,numel(bk2cal));
VPAPowNormLGamma = NaN(VPAsize,2,numel(bk2cal));
VPAPowNormHGamma = NaN(VPAsize,2,numel(bk2cal));
SALPowNormBeta =NaN(VPAsize,2,numel(bk2cal));
SALPowNormLGamma = NaN(VPAsize,2,numel(bk2cal));
SALPowNormHGamma = NaN(VPAsize,2,numel(bk2cal));
SALCohBeta = NaN(SALsize,2,numel(bk2cal));
SALCohLGamma = NaN(SALsize,2,numel(bk2cal));
SALCohHGamma = NaN(SALsize,2,numel(bk2cal));
VPACohBeta = NaN(SALsize,numel(bk2cal));
VPACohLGamma = NaN(SALsize,2,numel(bk2cal));
VPACohHGamma = NaN(SALsize,2,numel(bk2cal));

%% use try-catch when running unsupervised batches
for bb = 1:numel(bk2cal)
    VPAPowbaseline = NaN(VPAsize,2,numel(foi));
    VPAPow = NaN(VPAsize,2,numel(foi),numel(toi));
    VPACoh = NaN(VPAsize, numel(foi),numel(toi));
    
    SALPowbaseline =  NaN(SALsize,2,numel(foi));
    SALPow = NaN(SALsize,2,numel(foi),numel(toi));
    SALCoh = NaN(SALsize, numel(foi),numel(toi));
    
    for rr = 1:size(ratT,1)
        gr = ratT{rr,7};
        
        inputfile = [datafolder 'FC' filesep 'SpecCoherograms_' ...
            int2str(ratT.ratnums(rr)) '_' int2str(bk2cal(bb)) 'of27_ext.mat'];
        
        load(inputfile)
        
        % calculate coherence
        freq.coherence      = NaN(size(freq.crsspctrm));
        freq.coherence(1,:,:) = abs(freq.crsspctrm(1,:,:)./ ...
            sqrt(freq.powspctrm(1,:,:).* ...
            freq.powspctrm(2,:,:)));
        
        
        % reorganize data to new structure
        if strcmp(gr, 'VPA') ==1
            grID = sum(VPAInd(1:rr));
            outputfileVPA = [datafolder 'FC' filesep 'SpecCoherograms_' ...
                'VPA_' int2str(bk2cal(bb)) 'of27.mat'];
            VPAPow(grID,:,:,:) = freq.powspctrm;
            VPAPowbaseline(grID,:,:) = nanmean(freq.powspctrm(:,:,195:203),3);
            VPACoh(grID,:,:) = squeeze(freq.coherence);
            
        elseif strcmp(gr, 'Saline') ==1
            grID = sum(SALInd(1:rr));
            outputfileSAL = [datafolder 'FC' filesep 'SpecCoherograms_' ...
                'SAL_' int2str(bk2cal(bb)) 'of27.mat'];
            SALPow(grID,:,:,:) = freq.powspctrm;
            SALPowbaseline(grID,:,:) = nanmean(freq.powspctrm(:,:,195:203),3);
            SALCoh(grID,:,:) = squeeze(freq.coherence);
            
        end
        clear freq
    end
    
    save(outputfileVPA, 'VPAPow','VPAPowbaseline','VPACoh')
    save(outputfileSAL, 'SALPow','SALPowbaseline','SALCoh')
    
    VPAPowNorm = NaN(size(VPAPow,1),size(VPAPow,2),size(VPAPow,3),size(VPAPow,4));
    SALPowNorm = NaN(size(SALPow,1),size(SALPow,2),size(SALPow,3),size(SALPow,4));
    
    for rr = 1:size(VPAPow,1)
        for rgn = 1:size(VPAPow,2)
            for ff = 1:size(VPAPow,3)
                VPAPowNorm(rr,rgn,ff,:) = VPAPow(rr,rgn,ff,:)./VPAPowbaseline(rr,rgn,ff);
                SALPowNorm(rr,rgn,ff,:) = SALPow(rr,rgn,ff,:)./SALPowbaseline(rr,rgn,ff);
            end
        end
    end
    % converting power to dB
    VPAPowNorm = 10.*log10(VPAPowNorm);
    VPAPow = 10.*log10(VPAPow);
    SALPowNorm = 10.*log10(SALPowNorm);
    SALPow = 10.*log10(SALPow);
    
    VPAPowNormBeta(:,:,bb) = nanmean(nanmean(VPAPowNorm(:,:,23:27,234:458),4),3);
    VPAPowNormLGamma(:,:,bb) = nanmean(nanmean(VPAPowNorm(:,:,26:29,234:458),4),3);
    VPAPowNormHGamma(:,:,bb) = nanmean(nanmean(VPAPowNorm(:,:,30:33,234:458),4),3);
    SALPowNormBeta(:,:,bb) = nanmean(nanmean(SALPowNorm(:,:,23:27,234:458),4),3);
    SALPowNormLGamma(:,:,bb) = nanmean(nanmean(SALPowNorm(:,:,26:29,234:458),4),3);
    SALPowNormHGamma(:,:,bb) = nanmean(nanmean(SALPowNorm(:,:,30:33,234:458),4),3);
    SALCohBeta(:,bb) = nanmean(nanmean(SALCoh(:,23:27,234:458),3),2);
    SALCohLGamma(:,bb) = nanmean(nanmean(SALCoh(:,26:29,234:458),3),2);
    SALCohHGamma(:,bb) = nanmean(nanmean(SALCoh(:,30:33,234:458),3),2);
    VPACohBeta(:,bb) = nanmean(nanmean(VPACoh(:,23:27,234:458),3),2);
    VPACohLGamma(:,bb) = nanmean(nanmean(VPACoh(:,26:29,234:458),3),2);
    VPACohHGamma(:,bb) = nanmean(nanmean(VPACoh(:,30:33,234:458),3),2);
    
        VPAPowAvg = squeeze(nanmean(VPAPow,1));
        VPAPowNormAvg = squeeze(nanmean(VPAPowNorm,1));
        VPACohAvg = squeeze(nanmean(VPACoh,1));
        SALPowAvg = squeeze(nanmean(SALPow,1));
        SALPowNormAvg = squeeze(nanmean(SALPowNorm,1));
        SALCohAvg = squeeze(nanmean(SALCoh,1));
    
    
        figure(1)
        clf
        imagesc(squeeze(VPAPowAvg(1,:,:)),'YData', foi, 'XData', toi)
        axis xy
        ylabel('Frequency (Hz)')
        xlabel('seconds')
        title(['VPA NAc Power block ' int2str(bk2cal(bb)) '/27'])
        colormap jet
        colorbar
        hcol = colorbar;
        hcol.Label.String = 'dB';
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
        print([figurefolder 'VPA_NAc_block_' int2str(bk2cal(bb)) 'of27'...
            ],'-dpng');
    
        figure(2)
        clf
        imagesc(squeeze(VPAPowAvg(2,:,:)),'YData', foi, 'XData', toi)
        axis xy
        ylabel('Frequency (Hz)')
        xlabel('seconds')
        title(['VPA BLA Power block ' int2str(bk2cal(bb)) '/27'])
        colormap jet
        colorbar
        hcol = colorbar;
        hcol.Label.String = 'dB';
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
        print([figurefolder 'VPA_NAc_block_' int2str(bk2cal(bb)) 'of27'...
            ],'-dpng');
    
        figure(3)
        clf
        imagesc(squeeze(VPAPowNormAvg(1,:,:)),'YData', foi, 'XData', toi)
        axis xy
        ylabel('Frequency (Hz)')
        xlabel('seconds')
        title(['VPA NAc Power block ' int2str(bk2cal(bb)) '/27, pre-tone normalized'])
        colormap jet
        colorbar
        caxis([-5 5]);
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
        print([figurefolder 'VPA_NAcNorm_block_' int2str(bk2cal(bb)) 'of27'...
            ],'-dpng');
    
        figure(4)
        clf
        imagesc(squeeze(VPAPowNormAvg(2,:,:)),'YData', foi, 'XData', toi)
        axis xy
        ylabel('Frequency (Hz)')
        xlabel('seconds')
        title(['VPA BLA Power block ' int2str(bk2cal(bb)) '/27, pre-tone normalized'])
        colormap jet
        colorbar
        caxis([-5 5]);
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
        print([figurefolder 'VPA_BLANorm_block_' int2str(bk2cal(bb)) 'of27'...
            ],'-dpng');
    
        figure(5)
        clf
        imagesc(VPACohAvg(:,:),'YData', foi, 'XData', toi)
        axis xy
        ylabel('Frequency (Hz)')
        xlabel('seconds')
        title(['VPA NAc-BLA coherence block ' int2str(bk2cal(bb)) '/27'])
        colormap jet
        colorbar
        caxis([0 1]);
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
        print([figurefolder 'VPA_coherence_block_' int2str(bk2cal(bb)) 'of27'...
            ],'-dpng');
    
    
        figure(1)
        clf
        imagesc(squeeze(SALPowAvg(1,:,:)),'YData', foi, 'XData', toi)
        axis xy
        ylabel('Frequency (Hz)')
        xlabel('seconds')
        title(['SAL NAc Power block ' int2str(bk2cal(bb)) '/27'])
        colormap jet
        colorbar
        hcol = colorbar;
        hcol.Label.String = 'dB';
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
        print([figurefolder 'SAL_NAc_block_' int2str(bk2cal(bb)) 'of27'...
            ],'-dpng');
    
        figure(2)
        clf
        imagesc(squeeze(SALPowAvg(2,:,:)),'YData', foi, 'XData', toi)
        axis xy
        ylabel('Frequency (Hz)')
        xlabel('seconds')
        title(['SAL BLA Power block ' int2str(bk2cal(bb)) '/27'])
        colormap jet
        colorbar
        hcol = colorbar;
        hcol.Label.String = 'dB';
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
        print([figurefolder 'SAL_NAc_block_' int2str(bk2cal(bb)) 'of27'...
            ],'-dpng');
    
        figure(3)
        clf
        imagesc(squeeze(SALPowNormAvg(1,:,:)),'YData', foi, 'XData', toi)
        axis xy
        ylabel('Frequency (Hz)')
        xlabel('seconds')
        title(['SAL NAc Power block ' int2str(bk2cal(bb)) '/27, pre-tone normalized'])
        colormap jet
        colorbar
        caxis([-5 5]);
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
        print([figurefolder 'SAL_NAcNorm_block_' int2str(bk2cal(bb)) 'of27'...
            ],'-dpng');
    
        figure(4)
        clf
        imagesc(squeeze(SALPowNormAvg(2,:,:)),'YData', foi, 'XData', toi)
        axis xy
        ylabel('Frequency (Hz)')
        xlabel('seconds')
        title(['SAL BLA Power block ' int2str(bk2cal(bb)) '/27, pre-tone normalized'])
        colormap jet
        colorbar
        caxis([-5 5]);
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
        print([figurefolder 'SAL_BLANorm_block_' int2str(bk2cal(bb)) 'of27'...
            ],'-dpng');
    
        figure(5)
        clf
        imagesc(SALCohAvg(:,:),'YData', foi, 'XData', toi)
        axis xy
        ylabel('Frequency (Hz)')
        xlabel('seconds')
        title(['SAL NAc-BLA coherence block ' int2str(bk2cal(bb)) '/27'])
        colormap jet
        colorbar
        caxis([0 1]);
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
        print([figurefolder 'SAL_coherence_block_' int2str(bk2cal(bb)) 'of27'...
            ],'-dpng');
    
end


VPAPowNormBetaNAc = squeeze(VPAPowNormBeta(:,1,:,:));
VPAPowNormLGammaNAc = squeeze(VPAPowNormLGamma(:,1,:,:));
VPAPowNormHGammaNAc = squeeze(VPAPowNormHGamma(:,1,:,:));
SALPowNormBetaNAc = squeeze(SALPowNormBeta(:,1,:,:));
SALPowNormLGammaNAc = squeeze(SALPowNormLGamma(:,1,:,:));
SALPowNormHGammaNAc = squeeze(SALPowNormHGamma(:,1,:,:));
VPAPowNormBetaBLA = squeeze(VPAPowNormBeta(:,2,:,:));
VPAPowNormLGammaBLA = squeeze(VPAPowNormLGamma(:,2,:,:));
VPAPowNormHGammaBLA = squeeze(VPAPowNormHGamma(:,2,:,:));
SALPowNormBetaBLA = squeeze(SALPowNormBeta(:,2,:,:));
SALPowNormLGammaBLA = squeeze(SALPowNormLGamma(:,2,:,:));
SALPowNormHGammaBLA = squeeze(SALPowNormHGamma(:,2,:,:));


VPAPowNormBetaNAcD = NaN(VPAsize,7);
VPAPowNormLGammaNAcD = NaN(VPAsize,7);
VPAPowNormHGammaNAcD = NaN(VPAsize,7);
SALPowNormBetaNAcD = NaN(SALsize,7);
SALPowNormLGammaNAcD = NaN(SALsize,7);
SALPowNormHGammaNAcD = NaN(SALsize,7);
VPAPowNormBetaBLAD = NaN(VPAsize,7);
VPAPowNormLGammaBLAD = NaN(VPAsize,7);
VPAPowNormHGammaBLAD = NaN(VPAsize,7);
SALPowNormBetaBLAD = NaN(SALsize,7);
SALPowNormLGammaBLAD = NaN(SALsize,7);
SALPowNormHGammaBLAD = NaN(SALsize,7);

for dd = 1:7
    kk = 3*(dd-1)+1:3*(dd-1)+3;
VPAPowNormBetaNAcD(:,dd) = squeeze(mean(VPAPowNormBeta(:,1,kk),3));
VPAPowNormLGammaNAcD(:,dd) = squeeze(mean(VPAPowNormLGamma(:,1,kk),3));
VPAPowNormHGammaNAcD(:,dd) = squeeze(mean(VPAPowNormHGamma(:,1,kk),3));
SALPowNormBetaNAcD(:,dd) = squeeze(mean(SALPowNormBeta(:,1,kk),3));
SALPowNormLGammaNAcD(:,dd) = squeeze(mean(SALPowNormLGamma(:,1,kk),3));
SALPowNormHGammaNAcD(:,dd) = squeeze(mean(SALPowNormHGamma(:,1,kk),3));
VPAPowNormBetaBLAD(:,dd) = squeeze(mean(VPAPowNormBeta(:,2,kk),3));
VPAPowNormLGammaBLAD(:,dd) = squeeze(mean(VPAPowNormLGamma(:,2,kk),3));
VPAPowNormHGammaBLAD(:,dd) = squeeze(mean(VPAPowNormHGamma(:,2,kk),3));
SALPowNormBetaBLAD(:,dd) = squeeze(mean(SALPowNormBeta(:,2,kk),3));
SALPowNormLGammaBLAD(:,dd) = squeeze(mean(SALPowNormLGamma(:,2,kk),3));
SALPowNormHGammaBLAD(:,dd) = squeeze(mean(SALPowNormHGamma(:,2,kk),3));
end

SALCohBetaD = NaN(SALsize,7);
SALCohLGammaD = NaN(SALsize,7);
SALCohHGammaD = NaN(SALsize,7);
VPACohBetaD = NaN(VPAsize,7);
VPACohLGammaD = NaN(VPAsize,7);
VPACohHGammaD = NaN(VPAsize,7);

for dd = 1:7
    kk = 3*(dd-1)+1:3*(dd-1)+3;
    SALCohBetaD(:,dd) = mean(SALCohBeta(:,kk));
    SALCohLGammaD(:,dd) = mean(SALCohLGamma(:,kk));
    SALCohHGammaD(:,dd) = mean(SALCohHGamma(:,kk));
    VPACohBetaD(:,dd) = mean(VPACohBeta(:,kk));
    VPACohLGammaD(:,dd) = mean(VPACohLGamma(:,kk));
    VPACohHGammaD(:,dd) = mean(VPACohHGamma(:,kk));
end

