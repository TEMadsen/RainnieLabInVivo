%% Calculate raw spectrograms and coherograms for each trial

close all; clearvars; clc;  % clean slate
set(0,'DefaultFigureWindowStyle','docked') % dock figures

info_VPA_RC               % load all background info (filenames, etc)

%% old params

% foi = 1.5:0.5:100;
% foi = 2:1:30;

tapsmofrq = 0.5;

t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = 12:min(t_ftimwin)/2:22;    % time windows overlap by at least 50%

day = 1:9;

% toiBaseline = find(toi < -0.5 & toi > -1);
% 
% highfoi2plot = find(foi <110 & foi > 30);
% lowfoi2plot = find(foi <40 & foi > 1.5);

%% use try-catch when running unsupervised batches
for r = 1:size(ratT,1)
    for dd = 1:numel(phases)
        for fn = 1:size(nexfile{r,dd},1)  % file number w/in a phase
            % Tone-triggered trials with artifacts removed (except for fake
            % tone on/off artifacts).  Best for spectrograms & coherograms.
            inputfile = [datafolder 'FullData' filesep 'clean' filesep 'NonActive' filesep...
                'CleanLFPTrialData_' int2str(ratT.ratnums(r)) phases{dd} num2str(fn) '_nonActive.mat'];
            
            % Each tone will have its own output file.  Checking here for one
            % of the last tones.
            if size(nexfile{r,dd},1) == 1  
                outputfile = [datafolder 'FullData' filesep 'FC' filesep 'TFAnalysis' filesep 'NonActive' filesep ...
                    'RedoPowCoh_' int2str(ratT.ratnums(r)) phases{dd} 'Full.mat'];
            else
                outputfile = [datafolder 'FullData' filesep 'FC' filesep 'TFAnalysis' filesep 'NonActive' filesep ...
                    'RedoPowCoh_' int2str(ratT.ratnums(r)) phases{dd} num2str(fn) '.mat'];
            end
            if exist(outputfile,'file')
                disp([outputfile ' already exists.  Skipping rat.'])
            else
                disp(['Calculating spectra & coherence during non-active period for trials of '...
                    num2str(ratT.ratnums(r)) '''s Day ' int2str(day(dd)) ' file '  int2str(fn)]);
                load(inputfile);
                
                %% find trials that's too short
                minLen = 3;
                data.length = (data.sampleinfo(:,2)-data.sampleinfo(:,1))/1000;
                TrlIdx = find(data.length >= minLen);
                
                %                 cfg                                 = [];
                %                 cfg.artfctdef.tooShort.artifact     = data.sampleinfo(shortTrlIdx,:);
                %                 cfg.artfctdef.reject                = 'partial';
                %
                %                 data = ft_rejectartifact(cfg,data);
                
                cfg = [];
                cfg.channel   = [ratT.chNAc(r) ratT.chBLA(r)];
                cfg.trials    = TrlIdx;
                
                data = ft_selectdata(cfg,data);
                
                %% calculate spectrograms & coherograms
                %                 if mod(numel(data.trialinfo),80) ~= 0
                %                     SubSession = floor(numel(data.trialinfo)/80)+1;
                %                 else
                %                     SubSession = floor(numel(data.trialinfo)/80);
                %                 end
                %                 for subs = 1:SubSession
                
                cfg = get_cfg_TEM('mtmconvol','foodNonActive');
                cfg.output      = 'powandcsd';
                cfg.keeptrials  = 'yes';
                %                 cfg.outputfile  = outputfile;
                
                freq = ft_freqanalysis(cfg,data);
                %                 end
                max = 0;
                for trls = 1:size(data.time,1)
                    temp = size(data.time{trls},2);
                    if temp > max
                        max = temp;
                    end
                end
                if ~any(any(any(~isnan(freq.powspctrm))))
                    error('Only NaNs output from freqanalysis');
                end
                
                freq.coherence      = NaN(size(freq.crsspctrm));
                freq.coherence(:,1,:,:) = abs(freq.crsspctrm(:,1,:,:)./ ...
                    sqrt(freq.powspctrm(:,1,:,:).* ...
                    freq.powspctrm(:,2,:,:)));
                freq.totaltrial = data.trialinfo(end,1);
                freq.trialtime = data.time';
                freq.triallen = zeros(size(freq.trialtime,1),1);
                for trl = 1:numel(freq.triallen)
                    freq.triallen(trl) = size(freq.trialtime{trl},2);
                end
                freq.powspctrmAVG = zeros(numel(freq.trialinfo),2,numel(freq.freq));
                freq.coherenceAVG = zeros(numel(freq.trialinfo),numel(freq.freq));

                powtrl = nanmean(freq.powspctrm,4);
                cohtrl = squeeze(nanmean(freq.coherence,4));
                freq.nanidx = true(numel(freq.triallen),1);
                for trl = 1:numel(freq.triallen)
                    freq.powspctrmAVG(trl,:,:) = (squeeze(powtrl(trl,:,:)) * freq.triallen(trl));
                    freq.coherenceAVG(trl,:) = cohtrl(trl,:) * freq.triallen(trl);
                    if any(any(isnan(freq.powspctrmAVG(trl,:,:)))) == 1
                            freq.nanidx(trl) = false;
                    end
                end
                freq.powspctrmAVG = squeeze(nansum(freq.powspctrmAVG(freq.nanidx,:,:),1)./sum(freq.triallen(freq.nanidx)));
                freq.coherenceAVG = squeeze(nansum(freq.coherenceAVG(freq.nanidx,:),1)./sum(freq.triallen(freq.nanidx)));
                
                save(outputfile,'freq','-v7.3')

                %% plotting

                figure(1)
                plot(10.*log10(freq.powspctrmAVG)','XData', freq.freq)
                xlabel('Frequency (Hz)')
                ylabel('dB')
                 title(['Rat # ' int2str(ratT.ratnums(r)) ' Power ' phases{dd} ', Non-Active Period'])
                 legend('NAc', 'BLA')
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                print([figurefolder 'NonActive_Rat' int2str(ratT.ratnums(r)) '_D' phases{dd} num2str(fn)],'-dpng');

                figure(2)
                plot(freq.coherenceAVG','XData', freq.freq)
                xlabel('Frequency (Hz)')
                ylabel('Coherence')
                title(['Rat # ' int2str(ratT.ratnums(r)) ' Coherence ' phases{dd} ', Non-Active Period'])
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                print([figurefolder 'NonActive_Rat' int2str(ratT.ratnums(r)) '_Coherence_D' phases{dd} num2str(fn)],'-dpng')

                
                %% append results to freqFull if this session contains multiple recordings
                if size(nexfile{r,dd},1) > 1 && fn == 1
                    freqFull = freq;
                    freqFull.NAcPow = freq.powspctrmAVG (1,:);
                    freqFull.BLAPow = freq.powspctrmAVG (2,:);
                elseif size(nexfile{r,dd},1) > 1 && fn == size(nexfile{r,dd},1)
                    freqFull.NAcPow(fn,:) = freq.powspctrmAVG (1,:);
                    freqFull.BLAPow(fn,:) = freq.powspctrmAVG (2,:);
                    freqFull.coherence(fn,:) = freq.coherenceAVG(1,:);
                    freqFull.totaltrial(fn) = freq.totaltrial;
                    
                    % compute weighted average
                    
                    freqFull.powspctrm(1,:) = sum(freqFull.NAcPow.*freqFull.totaltrial',1)/sum(freqFull.totaltrial);
                    freqFull.powspctrm(2,:) = sum(freqFull.BLAPow.*freqFull.totaltrial',1)/sum(freqFull.totaltrial);
                    freqFull.coherence = sum(freqFull.coherence.*freqFull.totaltrial',1)/sum(freqFull.totaltrial);
                    
                    freq = freqFull;
                    
                    outputfile = [datafolder 'FullData' filesep 'FC' filesep 'TFAnalysis' filesep 'NonActive' filesep ...
                        'PowCoh_' int2str(ratT.ratnums(r)) phases{dd} 'Full.mat'];
                    save(outputfile,'freq','-v7.3')
                    
                    clearvars freqFull
                    
                elseif size(nexfile{r,dd},1) > 1 && fn > 1
                    freqFull.NAcPow(fn,:) = freq.powspctrmAVG (1,:);
                    freqFull.BLAPow(fn,:) = freq.powspctrmAVG (2,:);
                    freqFull.coherence(fn,:) = freq.coherenceAVG(1,:);
                    freqFull.totaltrial(fn) = freq.totaltrial;
                end
                
                clearvars data cfg freq
                
            end
        end
    end
end

% imagesc(squeeze(freq.powspctrm(1,:,:)),'YData', foi, 'XData', toi)
% axis xy
% ylabel('Frequency (Hz)')
% xlabel('seconds')
% colormap jet
% colorbar