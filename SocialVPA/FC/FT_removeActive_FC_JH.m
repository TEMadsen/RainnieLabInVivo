%% Remove active period via NEW METHOD:
% 1)  remove times during active period (6 seconds during tone and 6 seconds post tone).
% 2)  remove times before first trial and after last trial of each file.

close all force; clearvars; clc;  % clean slate

info_VPA_RC                   % load all background info (filenames, etc)

%% full loop

for r = 1:size(ratT,1)
    for dd = 1:numel(phases)
        if ~iscell(nexfile{r,dd})  % in case of multiple files per phase
            nexfile{r,dd} = nexfile(r,dd);  % convert to cellstr
        end
        for fn = 1:numel(nexfile{r,dd})  % file number w/in a phase
            % Raw LFP data divided into long, non-overlapping tone-triggered
            % trials, merged across all recordings for each rat
            
            inputfile = [datafolder filesep 'FullData' filesep 'RawLFPTrialData_' ...
                int2str(ratT.ratnums(r)) phases{dd} num2str(fn) '_extended.mat'];
            outputfile = [datafolder 'FullData' filesep 'clean' filesep 'NonActive' filesep...
                'CleanLFPTrialData_' int2str(ratT.ratnums(r)) phases{dd} num2str(fn) '_nonActive.mat'];
            if ~exist(outputfile,'file')
                %% check for input data
                
                if ~exist(inputfile,'file')
                    warning(['Skipping rat #' int2str(ratT.ratnums(r)) ' because ' ...
                        inputfile ' was not found. Run FT_preproc...new_TEM.m first.'])
                    continue
                end
                
                %% load input data
                
                load(inputfile);
                    %% reject dead channels based on extreme variance other than shocks:
                    % 1/var much greater than most channels (cutoff between 2 & 4e-4 per rat)
                    % or var frequently > var across channels during shock trials
                    % or unstable across recordings
                    
                    cfg             = [];
                    cfg.method      = 'summary';
                    cfg.keeptrial   = 'yes';  % won't invalidate trials marked "bad"
                    cfg.channel = [ratT.chNAc(r); ratT.chBLA(r); ratT.chNAcsh(r); ratT.chHipp(r)];
                    emptyChan = zeros(numel(cfg.channel),1);
                    for ii = 1:numel(cfg.channel)
                        if isempty(cfg.channel{ii})
                            emptyChan(ii) = 1;
                        end
                    end
                    cfg.channel(logical(emptyChan)) = [];

                    notdeadchs{r} = data.label;  %#ok<*SAGROW>
                    
                    %       keyboard  % if satisfied, dbcont
                    clearvars data1 d1dat diff* dur ampl ident trltime clip shocktime dw up labels
%                end
                                
                artifact = [];
                %% mark active, begining and end period
                TrlNum = data.trialinfo(end,1);
                Trl2Remove = zeros(TrlNum,2);
                TrlStart = zeros(numel(data.time),1);
                count = 1;
                
                for ii = 1:numel(data.time)
                    if ~isempty(find(data.time{1,ii} == 0,1))
                        tempTS = find(data.time{1,ii} == 0,1);
                        Trl2Remove(count,1) = data.sampleinfo(ii,1) + tempTS -1;
                        Trl2Remove(count,2) = Trl2Remove(count,1) + 12000;
                        count = count + 1;
                    end
                end
                
                artifact.tone   = Trl2Remove;
                artifact.start  = [1 Trl2Remove(1,1)];
                artifact.end    = [Trl2Remove(end,2) data.sampleinfo(end,2)];
               
                %% identify large artifacts
                
                % set threshold at twice the height of the largest fear-related delta
                % oscillation
                cfg                               = [];
                cfg.artfctdef.zvalue.channel      = 'AD*';
                cfg.artfctdef.zvalue.cutoff       = 10; %was 16
                cfg.artfctdef.zvalue.trlpadding   = 0;
                cfg.artfctdef.zvalue.fltpadding   = 0;
                cfg.artfctdef.zvalue.artpadding   = 0.1;
                
                cfg.artfctdef.zvalue.cumulative   = 'no';  % This uses max z-value
                % across channels, rather than sum, which can smooth out single-channel
                % artifacts, and ones with variable timing on different channels.
                
                cfg.artfctdef.zvalue.rectify      = 'yes';
                  cfg.artfctdef.zvalue.interactive  = 'no';
                
                [~, artifact.large] = ft_artifact_zvalue(cfg,data);
                
                %% identify sudden "jumps" in the data
                
                % set threshold at twice the height of the largest fear-related delta
                % oscillation
                cfg                                 = [];
                
                % channel selection, cutoff and padding
                cfg.artfctdef.zvalue.channel        = 'AD*';
                cfg.artfctdef.zvalue.cutoff         = 20; % was 36
                cfg.artfctdef.zvalue.trlpadding     = 0;
                cfg.artfctdef.zvalue.artpadding     = 0.1;
                cfg.artfctdef.zvalue.fltpadding     = 0;
                
                % algorithmic parameters
                cfg.artfctdef.zvalue.cumulative     = 'no';  % uses max z-value across
                % channels, rather than sum, which can smooth out single-channel artifacts
                
                cfg.artfctdef.zvalue.medianfilter   = 'yes';
                cfg.artfctdef.zvalue.medianfiltord  = 15;  % wide enough not to catch too many short blips
                cfg.artfctdef.zvalue.absdiff        = 'yes';
                cfg.artfctdef.zvalue.interactive    = 'no';

                [~, artifact.jump] = ft_artifact_zvalue(cfg,data);
                
                %% remove artifacts from data
                
                cfg                             = [];
                cfg.artfctdef.large.artifact    = artifact.large;
                cfg.artfctdef.jump.artifact     = artifact.jump;
                cfg.artfctdef.tone.artifact     = artifact.tone;
                cfg.artfctdef.start.artifact    = artifact.start;
                cfg.artfctdef.end.artifact      = artifact.end;
                cfg.artfctdef.reject            = 'partial';
                cfg.artfctdef.minaccepttim      = 0.25;
                cfg.outputfile                  = outputfile;
                
                data = ft_rejectartifact(cfg,data);
                
                %     keyboard
                clearvars data
            end
        end
    end
end
% for r = 1:size(ratT,1)
%     for dd = 1:numel(phases)
%         if ~iscell(nexfile{r,dd})  % in case of multiple files per phase
%             nexfile{r,dd} = nexfile(r,dd);  % convert to cellstr
%         end
%         for fn = 1:numel(nexfile{r,dd})  % file number w/in a phase
%             % Raw LFP data divided into long, non-overlapping tone-triggered
%             % trials, merged across all recordings for each rat
%             
%             inputfile = [datafolder 'FullData' filesep 'clean' filesep ...
%                 'CleanLFPTrialData_' int2str(ratT.ratnums(r)) phases{dd} num2str(fn) '_extended.mat'];
%             outputfile = [datafolder 'FullData' filesep 'clean' filesep 'NonActive' filesep...
%                 'CleanLFPTrialData_' int2str(ratT.ratnums(r)) phases{dd} num2str(fn) '_nonActive.mat'];
%             
%             if ~exist(outputfile,'file')
%                 %% check for input data
%                 
%                 if ~exist(inputfile,'file')
%                     warning(['Skipping rat #' int2str(ratT.ratnums(r)) 'Day' int2str(dd) ' file' ...
%                         int2str(fn) ' because ' inputfile ' was not found. Run FT_preproc...new_TEM.m first.'])
%                     continue
%                 end
%                 
%                 %% load input data
%                 
%                 load(inputfile);
%                 
%                 TrlNum = data.trialinfo(end,1);
%                 Trl2Remove = zeros(TrlNum,2);
%                 TrlStart = zeros(numel(data.time),1);
%                 count = 1;
%                 
%                 for ii = 1:numel(data.time)
%                     if ~isempty(find(data.time{1,ii} == 0,1))
%                         tempTS = find(data.time{1,ii} == 0,1);
%                         Trl2Remove(count,1) = data.sampleinfo(ii,1) + tempTS -1;
%                         Trl2Remove(count,2) = Trl2Remove(count,1) + 12000;
%                         count = count + 1;
%                     end
%                 end
%                 
%                 artifact.tone   = Trl2Remove;
%                 artifact.start  = [1 Trl2Remove(1,1)];
%                 artifact.end    = [Trl2Remove(end,2) data.sampleinfo(end,2)];
%                 
%                 cfg                             = [];
%                 cfg.artfctdef.tone.artifact     = artifact.tone;
%                 cfg.artfctdef.start.artifact    = artifact.start;
%                 cfg.artfctdef.end.artifact      = artifact.end;
%                 cfg.artfctdef.reject            = 'partial';
%                 cfg.outputfile                  = outputfile;
%                 
%                 data = ft_rejectartifact(cfg,data);
%                 clearvars data
%                 
%                 
%             end
%         end
%     end
% end