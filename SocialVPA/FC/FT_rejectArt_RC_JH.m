%% Remove artifacts via NEW METHOD:
% 1)  remove channels with more noise than neural signal (extreme variance
%     between motion artifacts and/or account for disproportionate amount
%     of clipping)
% 2)  remove times containing large, jump, and/or clipping artifacts
close all force; clearvars; clc;  % clean slate

info_VPA_RC                   % load all background info (filenames, etc)

%% full loop

minampl = NaN(16,size(ratT,1));
maxdur = NaN(16,size(ratT,1));
totaldur = NaN(16,size(ratT,1));
z = NaN(16,size(ratT,1));
next = 1;   % figure #

for r = 9
  % Raw LFP data divided into long, non-overlapping tone-triggered
  % trials, merged across all recordings for each rat
% inputfile = [datafolder filesep 'FullData' filesep 'RawLFPTrialData_' ...
%     int2str(ratT.ratnums(r)) 'Merged.mat'];
  inputfile = [datafolder filesep 'FullData' filesep ...
      'RawLFPTrialData_391ConditioningDay11.mat'];
  %   % merged LFPs with dead channels & clipping artifacts removed
  %   outputfile{1} = [datafolder 'noClipLFPTrialData_' ...
  %     int2str(ratT.ratnums(r)) 'Merged.mat'];
  %
  % merged LFPs with dead channels, large & clipping artifacts removed
%   outputfile = [datafolder 'FullData'  filesep 'CleanLFPTrialData_' ...
%     int2str(ratT.ratnums(r)) 'Merged.mat'];
  outputfile = [datafolder filesep 'FullData' filesep ...
      'CleanLFPTrialData_391ConditioningDay11.mat'];
  %filesep 'FC'
  if ~exist(outputfile,'file')
    %       if ~exist(outputfile{1},'file')
    %% check for input data
    
    if ~exist(inputfile,'file')
      warning(['Skipping rat #' int2str(ratT.ratnums(r)) ' because ' ...
        inputfile ' was not found. Run FT_preproc...new_TEM.m first.'])
      continue
    end
    
    %% load input data
    
    load(inputfile);
    
    if ratT.finalch(r)
      disp('Using previously selected not-dead channels')
    else
      %% divide 154s core of each trial into 22s chunks
      % keeping shock artifact separate from tone response
      
%       cfg           = [];
%       cfg.toilim    = [-60 94];
%       data1 = ft_redefinetrial(cfg, data);
%       
%       cfg           = [];
%       cfg.length    = 22;
%       cfg.overlap   = 0;
%       data1 = ft_redefinetrial(cfg, data1);
      
      %   %% take 1st derivative of signal
      %
      %   cfg = [];
      %   cfg.absdiff = 'yes';
      %
      %   d1dat = ft_preprocessing(cfg,data1);
      %
      %   %   %% label shock trials
      %   %
      %   %   alltrials = 1:numel(data1.trial);
      %   %   shocktr = false(size(alltrials));
      %   %
      %   %   for tone = 11:17  % in phase 1
      %   %     trls = find(data1.trialinfo(:,1) == tone & data1.trialinfo(:,2) == 1);
      %   %     for tr = trls'
      %   %       if data1.time{tr}(1) == 28
      %   %         shocktr(tr) = true;
      %   %       end
      %   %     end
      %   %   end
      %
      %   %% set amplthreshold for clipping artifacts
      %
      %   trltime = vertcat(data1.time{:})';  % time x trials
      %   shocktime = abs(32-trltime) < 2.5; % 29.5-34.5 s  % time x trials
      %   diff1 = cat(3,d1dat.trial{:});  % channels x time x trials
      %   [nch, nt, ntrl] = size(diff1);
      %   diff2 = [diff(diff1,1,2) zeros(nch,1,ntrl)];
      %   ampl = nan(size(diff2));
      %   for tp = 25:(nt-25)  % sliding 50 ms time windows
      %     ampl(:,tp,:) = max(abs(diff2(:,(tp-24):(tp+25),:)),[],2);
      %   end
      %
      %   fig = figure(next); clf; fig.WindowStyle = 'docked'; hold all;
      %   next = next + 1;
      %   for ch = 1:nch
      %     minampl(ch,r) = min(ampl(ch,~shocktime),[],2,'omitnan');
      %     histogram(ampl(ch,~shocktime),0:0.4883:20);
      %   end
      %   axis('tight');
      %   title(['Non-Shock Clipping for Rat #' int2str(ratT.ratnums(r))]);
      %   legend(d1dat.label, 'Location','eastoutside');
      %   xlabel('diff(diff(Amplitude)) between Samples (Max in 50 ms Window)');
      %   ylabel('# of Samples (50 ms Sliding Windows)');
      %
      %   %% set timethreshold for clipping artifacts
      %
      %   ident = abs(diff2) <= 3;
      %   % determine the number of consecutively identical samples
      %   dur = zeros(size(ident));
      %   for sgnlop=1:nch
      %     for trlop = 1:ntrl
      %       up = find(diff([0 ident(sgnlop,:,trlop)])== 1);
      %       dw = find(diff([ident(sgnlop,:,trlop) 0])==-1);
      %       for k=1:length(up)
      %         dur(sgnlop,up(k):dw(k),trlop) = dw(k)-up(k);
      %       end
      %     end
      %   end
      %
      %   fig = figure(next); clf; fig.WindowStyle = 'docked'; hold all;
      %   next = next + 1;
      %   for ch = 1:nch
      %     maxpertrl = max(dur(ch,~shocktime),[],2,'omitnan');
      %     maxdur(ch,r) = max(maxpertrl(:));
      %     sumpertrl = sum(dur(ch,~shocktime) > 50,'omitnan');
      %     totaldur(ch,r) = sum(sumpertrl(:));
      %     h = histogram(dur(ch,~shocktime));
      %     h.Normalization = 'probability';
      %     h.BinWidth = 25;
      %   end
      %   title(['Non-Shock Clipping for Rat #' int2str(ratT.ratnums(r))]);
      %   xlim([25 25+max(maxdur(:,r))]);
      %   ylim([0 2*(25+max(maxdur(:,r)))/sum(~shocktime(:))]);
      %   legend(d1dat.label, 'Location','eastoutside');
      %   xlabel('Duration of "Clipping" (Less than 3 uV Diff2) in Samples');
      %   ylabel('Probability of "Clipping" per Sample');
      %
      %   %% check "clipping" artifacts on worst channel(s)
      %   %%% revert to just showing the detected artifacts, not the whole trial
      %   %%% (gets too confusing), then BE DONE WITH IT ALREADY!!!
      %
      %   z(1:nch,r) = (totaldur(1:nch,r) - mean(totaldur(1:nch,r))) / ...
      %     std(totaldur(1:nch,r));
      %   badch = find(z(1:nch,r) > 1.5);
      %   if isempty(badch)
      %     [~, badch] = max(z(1:nch,r));
      %   end
      %   nextfig = next;
      %   for ch = badch'
      %     clip = squeeze(dur(ch,:,:) > 50);   % time x trial
      %     fig = figure(next); clf; fig.WindowStyle = 'docked'; hold all;
      %     next = next + 1; labels = {};
      %     for tr = find(any(clip))
      %       yyaxis left
      %       plot(trltime(clip(:,tr),tr), data1.trial{tr}(ch,(clip(:,tr))),'.');
      %       labels{end+1} = ['Trial #' num2str(tr)];
      %       yyaxis right
      %       plot(trltime(clip(:,tr),tr), diff2(ch,clip(:,tr),tr),'.');
      %     end
      %     axis('tight'); % ylim([-5 5]);
      %     ylabel('diff(diff(Amplitude)) between Samples (uV)');
      %     yyaxis left; axis('tight'); ylabel('Raw Data (uV)');
      %     xlabel('Time from Tone Onset (seconds)');
      %     legend(labels, 'Location','eastoutside');
      %     title(['Rat #' int2str(ratT.ratnums(r)) ...
      %       ', Bad Channel: ' d1dat.label{ch}]);
      %   end
      %   next = nextfig; % overwrite these figures after...
      %   keyboard  % pause & check that detected artifacts are really clipping
      % end
      % %% stop
      % keyboard
      % %     What value is just lower (amplthreshold) or higher (timethreshold) than
      % %     most of the non-artifactual data? Set both threshold in the section that
      % %     runs ft_artifact_clip. If any channels stand out from the rest,
      % %     invalidate them in the next section.
      
      %% reject dead channels based on extreme variance other than shocks:
      % 1/var much greater than most channels (cutoff between 2 & 4e-4 per rat)
      % or var frequently > var across channels during shock trials
      % or unstable across recordings
      
      cfg             = [];
      cfg.method      = 'summary';
      %   cfg.metric      = '1/var';
      %   cfg.trials      = alltrials(~shocktr);
      cfg.keeptrial   = 'yes';  % won't invalidate trials marked "bad"
      
      %   cfg.layout  = [configfolder 'layout_' MWA '_' ratT.side{r} '.mat'];
      % leave layout out to disable trial plotting because of
      % BUG IN rejectvisual_summary>display_trial (line 610)
      % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=2978
      
      % d1dat = ft_rejectvisual(cfg,d1dat);
      data = ft_rejectvisual(cfg,data);
      
      % BUG IN rejectvisual_summary>redraw (line 279) - DON'T USE MIN
      % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=1474
      
      % BUG IN rejectvisual_summary>redraw (line 309) - looks fixed at line 304
      % (same would probably work at line 279)
      % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=3005
      
      notdeadchs{r} = data.label;  %#ok<*SAGROW>
      
      keyboard  % if satisfied, dbcont
      clearvars data1 d1dat diff* dur ampl ident trltime clip shocktime dw up labels
    end
    
    %% select good channels in full dataset & start w/ empty artifact struct
    
%     cfg           = [];
%     cfg.channel   = notdeadchs{r};
%     
%     data = ft_selectdata(cfg,data);
    
    artifact = [];
    
    %% take 1st derivative of signal
    % input to ft_artifact_clip is 1st derivative, so it's actually
    % thresholding the 2nd derivative, which is more consistent in clipping
    % artifacts
    
    cfg = [];
    cfg.absdiff = 'yes';
    
    d1dat = ft_preprocessing(cfg,data);
    
    %% define clipping artifacts
    
    cfg                                 = [];
    
    cfg.artfctdef.clip.channel          = 'AD*';
    cfg.artfctdef.clip.pretim           = 0.1;
    cfg.artfctdef.clip.psttim           = 0.1;
    cfg.artfctdef.clip.timethreshold    = 0.05;   % s
    cfg.artfctdef.clip.amplthreshold    = 3;      % uV
    %   cfg.artfctdef.clip.blame            = true;
    
    [~, artifact.clip] = ft_artifact_clip(cfg,d1dat);
    
    % bug in ft_artifact_clip adds time after end of file, triggers bug in
    % convert_event that breaks ft_databrowser as noted below
    endsamp = sum(numel([d1dat.time{:}]));
    if isempty(artifact.clip) == 1 
        
    elseif artifact.clip(end,end) > endsamp
 
      artifact.clip(end,end) = endsamp;
    end
    
    %% identify large artifacts
    
    % set threshold at twice the height of the largest fear-related delta
    % oscillation
    cfg                               = [];
    cfg.artfctdef.zvalue.channel      = 'AD*';
    cfg.artfctdef.zvalue.cutoff       = 16;
    cfg.artfctdef.zvalue.trlpadding   = 0;
    cfg.artfctdef.zvalue.fltpadding   = 0;
    cfg.artfctdef.zvalue.artpadding   = 0.1;
    
    cfg.artfctdef.zvalue.cumulative   = 'no';  % This uses max z-value
    % across channels, rather than sum, which can smooth out single-channel
    % artifacts, and ones with variable timing on different channels.
    
    cfg.artfctdef.zvalue.rectify      = 'yes';
    cfg.artfctdef.zvalue.interactive  = 'yes';
    %   cfg.artfctdef.zvalue.interactive  = 'no';
    
    [~, artifact.large] = ft_artifact_zvalue(cfg,data);
    
    %% identify sudden "jumps" in the data
    
    % set threshold at twice the height of the largest fear-related delta
    % oscillation
    cfg                                 = [];
    
    % channel selection, cutoff and padding
    cfg.artfctdef.zvalue.channel        = 'AD*';
    cfg.artfctdef.zvalue.cutoff         = 36;
    cfg.artfctdef.zvalue.trlpadding     = 0;
    cfg.artfctdef.zvalue.artpadding     = 0.1;
    cfg.artfctdef.zvalue.fltpadding     = 0;
    
    % algorithmic parameters
    cfg.artfctdef.zvalue.cumulative     = 'no';  % uses max z-value across
    % channels, rather than sum, which can smooth out single-channel artifacts
    
    cfg.artfctdef.zvalue.medianfilter   = 'yes';
    cfg.artfctdef.zvalue.medianfiltord  = 15;  % wide enough not to catch too many short blips
    cfg.artfctdef.zvalue.absdiff        = 'yes';
    
    % make the process interactive
    cfg.artfctdef.zvalue.interactive    = 'yes';
    
    [~, artifact.jump] = ft_artifact_zvalue(cfg,data);
    
    %% mark any disconnection events as visual artifacts
    % If any clipping artifacts in this trial, zoom to Horizontal = 1 second.
    % Skip to next clipping artifact and page through it one Segment at a
    % time to verify that it's truly clipping.  See if it can be blamed on
    % one or two channels that should be invalidated instead.  When you reach
    % the end of the artifact, hit the next clipping artifact button again.
    % If there are no more in the trial, it won't respond (command window
    % reads "no later trialsegment with "clip" artifact found").  Zoom out to
    % full trial by setting Horizontal = total # of current trialsegments.
    % Skip to next trial with clipping artifacts and repeat.
    
    cfg                               = [];
    cfg.artfctdef.clip.artifact       = artifact.clip;
    cfg.artfctdef.large.artifact      = artifact.large;
    cfg.artfctdef.jump.artifact       = artifact.jump;
    
    % bug in convert_event, lines 180 & 181 should be swapped - prevents
    % plotting of all but the first large & jump artifacts
    cfg = ft_databrowser(cfg,data);
    
    artifact.visual   = cfg.artfctdef.visual.artifact;
    keyboard  % dbcont when satisfied
    clearvars d1dat
    
    %% remove artifacts from data
    
    cfg                             = [];
    cfg.artfctdef.clip.artifact     = artifact.clip;
    cfg.artfctdef.visual.artifact 	= artifact.visual;
    cfg.artfctdef.large.artifact  = artifact.large;
    cfg.artfctdef.jump.artifact   = artifact.jump;
    cfg.artfctdef.reject          = 'partial';
    cfg.artfctdef.minaccepttim    = 0.25;
    cfg.outputfile                = outputfile;
    
    data = ft_rejectartifact(cfg,data);
    
    keyboard
    clearvars data
  end
end