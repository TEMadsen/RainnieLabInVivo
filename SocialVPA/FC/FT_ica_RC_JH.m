%% perform ICA decomposition of old fear data & assess results

info_VPA_RC                  % load all background info (filenames, etc)

foi = (1.5).^(0:(1/4):12);   % 1/4-powers of 1.5 from 1-130 Hz
tapsmofrq = diff(foi);  % smooth each band by diff from next higher
foi = foi(1:end-1);     % trim highest freq to have same numel as tapsmofrq
for f = find(tapsmofrq < 1/3)   % don't care about narrower bands than this
  tapsmofrq(f) = 1/3;           % keeps time windows under 6 seconds
end
t_ftimwin = 2./tapsmofrq;       % for 3 tapers (K=3), T=2/W

% for smoothed visualization & less memory use
tshift = max(t_ftimwin)/2;  % shift time windows by tshift
newtime = -6:tshift:12;  % average powspctrm within +/- tshift of each


bk2cal = [1, 11,12,13,14];
bk2calD = [1, 6,6,7,7];

%% full loop

for r = 1:size(ratT,1)
  %% define input & output filenames
  
  % merged LFPs with dead channels & clipping artifacts removed
  inputfile = [datafolder '4CommitteeMeeting' filesep 'LFPTrialData_' ...
    num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  if exist(inputfile,'file')
    % initial ICA components
    outputfile{1} = [datafolder '4CommitteeMeeting' filesep 'ICAcomp' ...
      num2str(ratT.ratnums(r)) 'Merged.mat'];
    
    if exist(outputfile{1},'file')
      disp(['loading ICA components from file: ' outputfile{1}])
      load(outputfile{1})
    else
      %% perform ica decomposition
      
      cfg = [];
      
      cfg.inputfile   = inputfile;
      cfg.outputfile  = outputfile{1};
      
      [comp] = ft_componentanalysis(cfg);
    end
    
    %% visualize results (spatial topography)
    
    fig1 = figure(1); clf;
    cfg = [];
    cfg.component = 1:numel(comp.label);
    cfg.layout    = [configfolder 'layout_' MWA ratT.side{r} '_' ratT.side{r} '.mat'];
    cfg.colormap  = jet;
    cfg.zlim      = 'maxabs';
    cfg.comment   = 'no';
    
    ft_topoplotIC(cfg, comp)
    fig1.WindowStyle = 'docked';
    print(fig1,[figurefolder num2str(ratT.ratnums(r)) ...
      'Merged_topoplotIC'],'-dpng','-r300')
    
    %     %% visualize results (over time)
    %
    %     cfg = [];
    %     cfg.viewmode    = 'component';
    %     cfg.continuous  = 'yes';      % instead of commented cell above
    %     cfg.blocksize   = 180;
    %     cfg.layout      = [configfolder 'layout_' MWA '_' ratT.side{r} '.mat'];
    %     cfg.colormap    = jet;        % ignored?
    %     cfg.zlim        = 'maxabs';   % ignored?
    %
    %     %   cfg.event = struct('type',repmat('ToneOnset',47,1), ...
    %     %     'sample',unique(comp.sampleinfo(:,1)-round(comp.time{:}(1)*comp.fsample)), ...
    %     %     'value',unique(comp.trialinfo,'rows','stable'), ...
    %     %     'duration',ones(47,1), 'offset',zeros(47,1));
    %     %   This doesn't work, but someday I should go back and restore the events
    %     %   after merging files.
    %
    %     cfg = ft_databrowser(cfg, comp);
    %
    %     artifact.visual   = cfg.artfctdef.visual.artifact;
    %     keyboard  % dbcont when satisfied
    %
    %% calculate spectrograms
    

    powrange = NaN(numel(bk2cal),2);
    
    for b = 1:numel(bk2cal)
      % spectrograms of ICA components
      outputfile{2} = [datafolder 'ICAcomp_Spectrograms_' ...
        num2str(ratT.ratnums(r)) 'Merged_' blocks{bk2cal(b)} '.mat'];
 t = find(comp.trialinfo(:,2) == bk2calD(b))';

            m = comp.trialinfo(t(1):t(end),1);
            o = find(m > 30*(bk2cal(b)-bk2calD(b)*2+1) & m < (30*(bk2cal(b)-bk2calD(b)*2+2)+1));
            trials = t(o);
%       if mod(b,2) == 0
%         d = b/2;
%       elseif mod(b,2) == 1
%         d = (b+1)/2;
%       end
%               t = find(comp.trialinfo(:,2) == d)';
%             m = comp.trialinfo(t(1):t(end),1);
%             o = find(m > 30*(b-d*2+1) & m < (30*(b-d*2+2))+1);
%             trials = t(o);
      if exist(outputfile{2},'file')
        disp(['loading spectrograms from file: ' outputfile{2}])
        load(outputfile{2})
      else
        cfg             = [];
        
        cfg.method      = 'mtmconvol';
        cfg.output      = 'pow';
        cfg.keeptrials  = 'no';   % groups subtrials back together
        cfg.trials      = trials;
        
        cfg.foi         = foi;
        cfg.tapsmofrq   = tapsmofrq;
        cfg.t_ftimwin   = t_ftimwin;
        cfg.toi         = '50%'; 	% overlap between smallest time windows
        cfg.pad   = 'nextpow2'; % max # samples, rounded up to next power of 2
        
        cfg.outputfile  = outputfile{2};
        
        freq = ft_freqanalysis(cfg,comp);
      end
      
      [ncomp, nf, nt] = size(freq.powspctrm);
      
      %% average max(t_ftimwin) chunks & convert to dB for easier visualization
      
      newpowspctrm = nan(ncomp, nf, numel(newtime));
      
      for ch = 1:ncomp
        for f = 1:nf
          for t = 1:numel(newtime)
            newpowspctrm(ch, f, t) = 10*log10(mean(freq.powspctrm(...
              ch, f, abs(freq.time - newtime(t)) < tshift), ...
              'omitnan'));
          end
        end
      end
      
      freq.time = newtime;
      freq.powspctrm = newpowspctrm;
%       if tr > 10 && tr < 18   % during shocks
%         powrange(tr,:) = [min(min(newpowspctrm(1,:,[-6:0 6:12]))) ...
%           max(max(newpowspctrm(1,:,[-6:0 6:12])))];
%       else
        powrange(b,:) = [min(min(newpowspctrm(1,:,:))) ...
          max(max(newpowspctrm(1,:,:)))];
%       end
      
      %% visualize results (by frequency)
      
      nw = ceil(sqrt(ncomp));  % # of plots wide
      nh = ceil(ncomp/nw);     % # of plots high
      
      cfg                 = [];
      cfg.parameter       = 'powspctrm';
      cfg.colormap        = jet;
      cfg.fontsize        = 10;
      
      fig = figure(b+1); clf; fig.WindowStyle = 'docked';
      
      for ch = 1:ncomp
        cfg.channel        = ch;
        subplot(nw,nh,ch); cla
        
        ft_singleplotTFR(cfg, freq);
      end
    end
    
    %% normalize scales across trials
    
    powrange = [min(powrange(:,1)) max(powrange(:,2))];
    for b = 1:numel(bk2cal)
      fig = figure(b+1);
      for ch = 1:ncomp
        subplot(nw,nh,ch);
        ax = gca;
        ax.FontSize = cfg.fontsize;
        ax.CLim = powrange;
%         ax.YScale = 'log';  % changes axis, but not image data
%         ax.YTick = foi(1:4:end);
        ax.XTick = newtime(1:5:end);
        xlabel('Time (s)')
        ylabel('Frequency (Hz)')
        title([freq.label{ch} ' Power (dB), Trial #' num2str(b)])
      end
      
      %% save figure
      
      print(fig,[figurefolder num2str(ratT.ratnums(r)) 'Merged_' ...
        blocks{bk2cal(b)} '_ICA_Spectrograms'],'-dpng','-r300')
    end
    
    %     %% reject components
    %
    %     cfg.method    = 'summary';
    %
    %     % don't know what this does, but it's supposed to be the default and
    %     % seems to avoid an error!
    %     cfg.viewmode  = 'remove';
    %
    %     comp = ft_rejectvisual(cfg,comp);
    %
    %     keyboard
    %     %% reject artifactual components?
    %
    %     %       ft_rejectcomponent
    %
  end
end
