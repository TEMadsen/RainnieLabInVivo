%% Calculate raw spectrograms and coherograms for each trial

close all; clearvars; clc;  % clean slate
set(0,'DefaultFigureWindowStyle','docked') % dock figures

info_VPA_RC               % load all background info (filenames, etc)

foi = 1.5:0.5:100;   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%

day = 1:9;
day2cal = 4:7;
% bk2cal = 1:9;
% bk2calD = [1,1,1,2,2,2,3,3,3];

plot = false;

toiBaseline = find(toi < -0.5 & toi > -1);

highfoi2plot = find(foi <110 & foi > 30);
lowfoi2plot = find(foi <40 & foi > 1.5);

%% use try-catch when running unsupervised batches
for r = 16:size(ratT,1)
    if r == 7
    else
    for ii = 3:numel(day2cal)
        for tt = 1:6 % every 10 trials
            dd = day2cal(ii);
                % Tone-triggered trials with artifacts removed (except for fake
                % tone on/off artifacts).  Best for spectrograms & coherograms.
                inputfile = [datafolder filesep 'FullData' filesep 'clean' filesep ...
                    'CleanLFPTrialData_' int2str(ratT.ratnums(r)) phases{dd} '1.mat'];
                
                % Each tone will have its own output file.  Checking here for one
                % of the last tones.
                outputfile = [datafolder 'FullData' filesep 'FC' filesep 'TFAnalysis' filesep ...
                    'SpecCoherograms_' int2str(ratT.ratnums(r)) phases{dd} '10trl_' int2str(tt) '.mat'];
                
                if exist(outputfile,'file')
                    disp([outputfile ' already exists.  Skipping rat.'])
                else
                    disp(['Calculating spectrograms & coherograms for trials of '...
                        num2str(ratT.ratnums(r)) '''s Day ' int2str(day(dd)) ]);
                    load(inputfile);
                    
                    trials = find(data.trialinfo > ((tt-1)*10) & data.trialinfo < (tt*10+1));

                    if isempty(trials)
                        warning(['No trials found for ' ...
                            num2str(ratT.ratnums(r)) '''s ' int2str(dd) 'block']);
                    else
                        disp(['Calculating spectrograms & coherograms for trials of '...
                            num2str(ratT.ratnums(r)) '''s ' int2str(dd) ]);
                        
                        %% calculate spectrograms & coherograms
                        channel = cell(2,1);
                        channel{1} = ratT.chNAc{r};
                        channel{2} = ratT.chBLA{r};
                        cfg             = [];
                        cfg.trials      = trials;
                        cfg.method      = 'wavelet';
                        cfg.output      = 'powandcsd';
                        cfg.keeptrials  = 'no';
                        cfg.channel     = channel;
                        cfg.foi         = foi;
                        cfg.tapsmofrq   = tapsmofrq;
                        cfg.t_ftimwin   = t_ftimwin;
                        cfg.toi         = toi;
                        cfg.pad = 'nextpow2'; % assumes longest trial is 500s
                        
                        cfg.outputfile  = outputfile;
                        
                        freq = ft_freqanalysis(cfg,data);
                    end
                    if ~any(any(any(~isnan(freq.powspctrm))))
                        error('Only NaNs output from freqanalysis');
                    end
                    
                    if plot == true
                        
                        freq.coherence      = NaN(size(freq.crsspctrm));
                        freq.coherence(1,:,:) = abs(freq.crsspctrm(1,:,:)./ ...
                            sqrt(freq.powspctrm(1,:,:).* ...
                            freq.powspctrm(2,:,:)));
                        
                        %% low freq
                        imagesc(10.*log10(squeeze(freq.powspctrm(1,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
                        axis xy
                        ylabel('Frequency (Hz)')
                        xlabel('seconds')
                        colormap jet
                        colorbar
                        title(['Rat # ' int2str(ratT.ratnums(r)) ' NAc Power ' phases{dd}])
                        colormap jet
                        colorbar
                        hcol = colorbar;
                        hcol.Label.String = 'dB';
                        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                        print([figurefolder 'LF_Rat' int2str(ratT.ratnums(r)) '_NAc_D' phases{dd} num2str(fn)],'-dpng');
                        
                        imagesc(10.*log10(squeeze(freq.powspctrm(2,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
                        axis xy
                        ylabel('Frequency (Hz)')
                        xlabel('seconds')
                        colormap jet
                        colorbar
                        title(['Rat # ' int2str(ratT.ratnums(r)) ' BLA Power ' phases{dd}])
                        colormap jet
                        colorbar
                        hcol = colorbar;
                        hcol.Label.String = 'dB';
                        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                        print([figurefolder 'LF_Rat' int2str(ratT.ratnums(r)) '_BLA_D' phases{dd} num2str(fn)],'-dpng');
                        
                        
                        imagesc(squeeze(freq.coherence(1,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
                        axis xy
                        ylabel('Frequency (Hz)')
                        xlabel('seconds')
                        colormap jet
                        colorbar
                        title(['Rat # ' int2str(ratT.ratnums(r)) ' Coherence ' phases{dd}])
                        colormap jet
                        colorbar
                        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                        print([figurefolder 'LF_Rat' int2str(ratT.ratnums(r)) '_Coherence_D' phases{dd} num2str(fn)],'-dpng');
                        
                        %% high freq
                        imagesc(10.*log10(squeeze(freq.powspctrm(1,highfoi2plot,:))),'YData', foi(highfoi2plot), 'XData', toi)
                        axis xy
                        ylabel('Frequency (Hz)')
                        xlabel('seconds')
                        colormap jet
                        colorbar
                        title(['Rat # ' int2str(ratT.ratnums(r)) ' NAc Power ' phases{dd}])
                        colormap jet
                        colorbar
                        hcol = colorbar;
                        hcol.Label.String = 'dB';
                        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                        print([figurefolder 'HF_Rat' int2str(ratT.ratnums(r)) '_NAc_D' phases{dd} num2str(fn)],'-dpng');
                        
                        imagesc(10.*log10(squeeze(freq.powspctrm(2,highfoi2plot,:))),'YData', foi(highfoi2plot), 'XData', toi)
                        axis xy
                        ylabel('Frequency (Hz)')
                        xlabel('seconds')
                        colormap jet
                        colorbar
                        title(['Rat # ' int2str(ratT.ratnums(r)) ' BLA Power ' phases{dd}])
                        colormap jet
                        colorbar
                        hcol = colorbar;
                        hcol.Label.String = 'dB';
                        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                        print([figurefolder 'HF_Rat' int2str(ratT.ratnums(r)) '_BLA_D' phases{dd} num2str(fn)],'-dpng');
                        
                        
                        imagesc(squeeze(freq.coherence(1,highfoi2plot,:)),'YData', foi(highfoi2plot), 'XData', toi)
                        axis xy
                        ylabel('Frequency (Hz)')
                        xlabel('seconds')
                        colormap jet
                        colorbar
                        title(['Rat # ' int2str(ratT.ratnums(r)) ' Coherence ' phases{dd}])
                        colormap jet
                        colorbar
                        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                        print([figurefolder 'HF_Rat' int2str(ratT.ratnums(r)) '_Coherence_D' phases{dd} num2str(fn)],'-dpng');
                        
                    end
                    
                    clearvars data cfg freq
                end
        end
    end
    end
end

    % imagesc(squeeze(freq.powspctrm(1,:,:)),'YData', foi, 'XData', toi)
    % axis xy
    % ylabel('Frequency (Hz)')
    % xlabel('seconds')
    % colormap jet
    % colorbar
    
    % %% Calculate raw spectrograms and coherograms for each trial
    %
    % close all; clearvars; clc;  % clean slate
    %
    % info_VPA_RC               % load all background info (filenames, etc)
    %
    % foi = 1.5:0.5:120;   % freqs from 0.75-150 Hz progressing up by 20% each
    % tapsmofrq = foi/5;             % smooth each band by +/- 20%
    % for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    %     tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
    % end
    % t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
    % toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%
    % errcnt = 0;                       % error count
    % errloc = [];                      % error location (r,p,t)
    % bk2cal = 1:9;
    % bk2calD = [1,1,1,2,2,2,3,3,3];
    %
    % %% use try-catch when running unsupervised batches
    %
    % for r = 1:size(ratT,1)
    %     % Tone-triggered trials with artifacts removed (except for fake
    %     % tone on/off artifacts).  Best for spectrograms & coherograms.
    %     inputfile = [datafolder 'FC' filesep 'fcLFPTrialData_extended' ...
    %     int2str(ratT.ratnums(r)) '.mat'];
    %
    %
    %     %% load data
    %
    %     load(inputfile);
    %
    %     parfor b = 1:numel(bk2cal)
    %
    %
    %
    %         % Each tone will have its own output file.  Checking here for one
    %         % of the last tones.
    %         outputfile = [datafolder 'FC' filesep 'SpecCoherograms_' ...
    %             int2str(ratT.ratnums(r)) '_' int2str(bk2cal(b)) 'of27_linscale_ext.mat'];
    %
    %         %% check existing data
    %
    %         %         filecorrect = false; % start false in case file doesn't exist
    %         %
    %         %         if exist(outputfile,'file')
    %         %           filecorrect = true; % true unless demonstrated to be false
    %         %           disp(['Data for Rat # ' int2str(ratT.ratnums(r)) ', ' ...
    %         %             blocks{bk2cal(b)} ' already analyzed! Check for accuracy.']);
    %         %           load(outputfile);
    %         %
    %         %           if ~any(any(any(~isnan(freq.powspctrm))))
    %         %             warning('Only NaNs output from freqanalysis');
    %         %             filecorrect = false;
    %         %           end
    %         %
    %         %           if freq.time(1) >= 0
    %         %             warning('no pre-tone baseline found');
    %         %             filecorrect = false;
    %         %           else
    %         %             disp(['first timestamp is ' num2str(freq.time(1))]);
    %         %           end
    %         %
    %         %           %           if strcmp(goodchs{r}{1},'AD*')
    %         %           %             if numel(freq.label) ~= (numel(lftchs) + numel(rtchs) + 1 ...
    %         %           %                 - numel(goodchs{r}))
    %         %           %               warning('different channels used than planned');
    %         %           %               filecorrect = false;  % reprocess this file
    %         %           %               %             else
    %         %           %               %               for ch = 1:numel(freq.label)
    %         %           %               %                 k = strfind(goodchs{r}, freq.label{ch});
    %         %           %               %                 if any([k{:}])
    %         %           %               %                   warning([freq.label{ch} ' should be excluded']);
    %         %           %               %                   filecorrect = false;  % reprocess this file
    %         %           %               %                   break  % jumps to end of for ch loop
    %         %           %               %                 else
    %         %           %               %                   disp([freq.label{ch} ' is not excluded']);
    %         %           %               %                 end
    %         %           %               %               end   % still true if no excluded channels
    %         %           %             end
    %         %           %           elseif numel(freq.label) ~= numel(goodchs{r}) || ...
    %         %           %               any(any(~strcmp(freq.label,goodchs{r})))
    %         %           %             warning('different channels used than planned');
    %         %           %             filecorrect = false;  % reprocess this file
    %         %           %           else
    %         %           %             disp([freq.label{:} ' are correct']);   % still true
    %         %           %           end
    %         %         end
    %         %
    %         %         %% reprocess if necessary
    %         %
    %         %         if filecorrect
    %         %           disp('checked file seems correct');
    %         %         else
    %
    %         %% loop through tones
    %          t = find(data.trialinfo(:,2) == bk2calD(b))';
    %         try
    %             m = data.trialinfo(t(1):t(end),1);
    %             if mod(bk2cal(b),3) == 0
    %                 o = find(m > 40 & m <61);
    %             else
    %                 o = find(m > (mod(bk2cal(b),3)-1)*20 & m < (mod(bk2cal(b),3)*20+1));
    %             end
    %             trials = t(o);
    %             if isempty(trials)
    %                 warning(['No trials found for ' ...
    %                     num2str(ratT.ratnums(r)) '''s ' int2str(bk2cal(b)) 'block']);
    %             else
    %                 disp(['Calculating spectrograms & coherograms for trials of '...
    %                     num2str(ratT.ratnums(r)) '''s ' int2str(bk2cal(b)) ]);
    %
    %                 %% calculate spectrograms & coherograms
    %                 channel = cell(2,1);
    %                 channel{1} = ratT.chNAc{r};
    %                 channel{2} = ratT.chBLA{r};
    %                 cfg             = [];
    %
    %                 cfg.method      = 'wavelet';
    %                 cfg.output      = 'powandcsd';
    %                 cfg.keeptrials  = 'no';
    %                 cfg.trials      = trials;
    %                 cfg.channel     = channel;
    %                 cfg.foi         = foi;
    %                 cfg.tapsmofrq   = tapsmofrq;
    %                 cfg.t_ftimwin   = t_ftimwin;
    %                 cfg.toi         = toi;
    %                 cfg.pad = 'nextpow2'; % assumes longest trial is 500s
    %
    %                 cfg.outputfile  = outputfile;
    %
    %                 freq = ft_freqanalysis(cfg,data);
    %
    %                 if ~any(any(any(~isnan(freq.powspctrm))))
    %                     error('Only NaNs output from freqanalysis');
    %                 end
    %             end
    %         catch ME1
    %             errcnt = errcnt + 1;
    %             %                     errloc(errcnt,:) = [r b t];
    %             warning(['Error (' ME1.message ') while processing Rat # ' ...
    %                 int2str(ratT.ratnums(r)) ', ' int2str(bk2cal(b)) ...
    %                 '! Continuing with next in line.']);
    %         end
    %
    %
    %         %         end
    %         %% clear variables
    %
    %         %         clearvars data cfg freq;
    %     end
    % end