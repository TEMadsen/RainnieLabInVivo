%% Visualize normalized time-frequency analysis results

close all; clearvars; clc;    % clean slate
set(0,'DefaultFigureWindowStyle','docked') % dock figures

info_VPA_RC                    % load all background info (filenames, etc)

foi = 1.5:0.5:120;   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%

plt = true;                  % if plots are desired for each tone and rat
errcnt = 0;                   % error count
errloc = [];                  % error location (r,p,t)

days = 1:9;
toiBaseline = find(toi < -0.5 & toi > -1.5);
% toiBaseline = find(toi < -0.5 & toi > -2.5);

foi2plot = find(foi <120 & foi > 30);
lowfoi2plot = find(foi <40 & foi > 1.5);

toiTone = find(toi < 5.8 & toi > 0.2);

% beta =
% bk2cal = 1:27;
% bk2calD = [1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8,9,9,9];
%
SALsize = sum(strcmp(ratT.groups,'Saline'));
VPAsize = sum(strcmp(ratT.groups,'VPA'));

foiTheta = find(foi < 10 & foi > 4);
foiBeta = find(foi < 30 & foi > 10);
foiGamma = find(foi < 80 & foi > 30);


VPAPowNormTheta = NaN(VPAsize,2,numel(days));
VPAPowNormBeta = NaN(VPAsize,2,numel(days));
VPAPowNormGamma = NaN(VPAsize,2,numel(days));
SALPowNormTheta =NaN(SALsize,2,numel(days));
SALPowNormBeta = NaN(SALsize,2,numel(days));
SALPowNormGamma = NaN(SALsize,2,numel(days));
SALCohTheta = NaN(SALsize,numel(days));
SALCohBeta = NaN(SALsize,numel(days));
SALCohGamma = NaN(SALsize,numel(days));
VPACohTheta = NaN(VPAsize,numel(days));
VPACohBeta = NaN(VPAsize,numel(days));
VPACohGamma = NaN(VPAsize,numel(days));

%10-30
%30-55
%55-80
%80-110

VPACohbaseline = NaN(numel(days),VPAsize,numel(foi));
VPAPowbaseline = NaN(numel(days),VPAsize,2,numel(foi));
SALCohbaseline =  NaN(numel(days),SALsize,numel(foi));
SALPowbaseline =  NaN(numel(days),SALsize,2,numel(foi));
%% use try-catch when running unsupervised batches
for bb = 1:numel(days)
    
    VPAPow = NaN(VPAsize,2,numel(foi),numel(toi));
    VPACoh = NaN(VPAsize, numel(foi),numel(toi));
    
    
    SALPow = NaN(SALsize,2,numel(foi),numel(toi));
    SALCoh = NaN(SALsize, numel(foi),numel(toi));
    
    for rr = 1:size(ratT,1)
        gr = ratT{rr,9};
        
        inputfile = [datafolder 'FullData' filesep 'FC' filesep 'SpecCoherograms_' ...
            int2str(ratT.ratnums(rr)) '-4to12_linScale_D' int2str(day(bb)) '.mat'];
        load(inputfile)
        
        % calculate coherence
        freq.coherence      = NaN(size(freq.crsspctrm));
        freq.coherence(1,:,:) = abs(freq.crsspctrm(1,:,:)./ ...
            sqrt(freq.powspctrm(1,:,:).* ...
            freq.powspctrm(2,:,:)));
        
        
        % reorganize data to new structure
        if strcmp(gr, 'VPA') ==1
            grID = sum(VPAInd(1:rr));
            outputfileVPA = [datafolder 'FullData' filesep 'FC' filesep 'SpecCoherograms_' ...
                'VPA_D' int2str(days(bb)) '.mat'];
            VPAPow(grID,:,:,:) = freq.powspctrm;
            VPAPowbaseline(bb,grID,:,:) = nanmean(freq.powspctrm(:,:,toiBaseline),3);
            VPACoh(grID,:,:) = squeeze(freq.coherence);
            VPACohbaseline(bb,grID,:) = nanmean(squeeze(freq.coherence(:,:,toiBaseline)),2);
            
        elseif strcmp(gr, 'Saline') ==1
            grID = sum(SALInd(1:rr));
            outputfileSAL = [datafolder 'FullData' filesep 'FC' filesep 'SpecCoherograms_' ...
                'SAL_D' int2str(days(bb)) '.mat'];
            SALPow(grID,:,:,:) = freq.powspctrm;
            SALPowbaseline(bb,grID,:,:) = nanmean(freq.powspctrm(:,:,toiBaseline),3);
            SALCoh(grID,:,:) = squeeze(freq.coherence);
            SALCohbaseline(bb,grID,:) = nanmean(squeeze(freq.coherence(:,:,toiBaseline)),2);
            
        end
        clear freq
    end
    
    save(outputfileVPA, 'VPAPow','VPAPowbaseline','VPACoh','VPACohbaseline')
    save(outputfileSAL, 'SALPow','SALPowbaseline','SALCoh','SALCohbaseline')
    
    VPAPowNorm = NaN(size(VPAPow,1),size(VPAPow,2),size(VPAPow,3),size(VPAPow,4));
    SALPowNorm = NaN(size(SALPow,1),size(SALPow,2),size(SALPow,3),size(SALPow,4));
    VPACohNorm = NaN(size(VPACoh,1),size(VPACoh,2),size(VPACoh,3));
    SALCohNorm = NaN(size(SALCoh,1),size(SALCoh,2),size(SALCoh,3));
    
    for rr = 1:size(VPAPow,1)
        for ff = 1:size(VPAPow,3)
            for rgn = 1:size(VPAPow,2)
                VPAPowNorm(rr,rgn,ff,:) = VPAPow(rr,rgn,ff,:)./squeeze(VPAPowbaseline(bb,rr,rgn,ff));
            end
            VPACohNorm(rr,ff,:) = VPACoh(rr,ff,:) - squeeze(VPACohbaseline(bb,rr,ff));
        end
    end
    
    for rr = 1:size(SALPow,1)
        for ff = 1:size(SALPow,3)
            for rgn = 1:size(SALPow,2)
                SALPowNorm(rr,rgn,ff,:) = SALPow(rr,rgn,ff,:)./squeeze(SALPowbaseline(bb,rr,rgn,ff));
            end
            SALCohNorm(rr,ff,:) = SALCoh(rr,ff,:) - squeeze(SALCohbaseline(bb,rr,ff));
        end
    end
    % converting power to dB
    VPAPowNorm = 10.*log10(VPAPowNorm);
    VPAPow = 10.*log10(VPAPow);
    SALPowNorm = 10.*log10(SALPowNorm);
    SALPow = 10.*log10(SALPow);
    
    VPAPowNormTheta(:,:,bb) = nanmean(nanmean(VPAPowNorm(:,:,foiTheta(1):foiTheta(end),toiTone(1):toiTone(end)),4),3);
    VPAPowNormBeta(:,:,bb) = nanmean(nanmean(VPAPowNorm(:,:,foiBeta(1):foiBeta(end),toiTone(1):toiTone(end)),4),3);
    VPAPowNormGamma(:,:,bb) = nanmean(nanmean(VPAPowNorm(:,:,foiGamma(1):foiGamma(end),toiTone(1):toiTone(end)),4),3);
    SALPowNormTheta(:,:,bb) = nanmean(nanmean(SALPowNorm(:,:,foiTheta(1):foiTheta(end),toiTone(1):toiTone(end)),4),3);
    SALPowNormBeta(:,:,bb) = nanmean(nanmean(SALPowNorm(:,:,foiBeta(1):foiBeta(end),toiTone(1):toiTone(end)),4),3);
    SALPowNormGamma(:,:,bb) = nanmean(nanmean(SALPowNorm(:,:,foiGamma(1):foiGamma(end),toiTone(1):toiTone(end)),4),3);
    
    VPACohTheta(:,bb) = nanmean(nanmean(VPACohNorm(:,foiTheta(1):foiTheta(end),toiTone(1):toiTone(end)),3),2);
    VPACohBeta(:,bb) = nanmean(nanmean(VPACohNorm(:,foiBeta(1):foiBeta(end),toiTone(1):toiTone(end)),3),2);
    VPACohGamma(:,bb) = nanmean(nanmean(VPACohNorm(:,foiGamma(1):foiGamma(end),toiTone(1):toiTone(end)),3),2);
    SALCohTheta(:,bb) = nanmean(nanmean(SALCohNorm(:,foiTheta(1):foiTheta(end),toiTone(1):toiTone(end)),3),2);
    SALCohBeta(:,bb) = nanmean(nanmean(SALCohNorm(:,foiBeta(1):foiBeta(end),toiTone(1):toiTone(end)),3),2);
    SALCohGamma(:,bb) = nanmean(nanmean(SALCohNorm(:,foiGamma(1):foiGamma(end),toiTone(1):toiTone(end)),3),2);
    
    VPAPowAvg = squeeze(nanmean(VPAPow,1));
    VPAPowNormAvg = squeeze(nanmean(VPAPowNorm,1));
    VPACohAvg = squeeze(nanmean(VPACoh,1));
    VPACohNormAvg = squeeze(nanmean(VPACohNorm,1));
    
    SALPowAvg = squeeze(nanmean(SALPow,1));
    SALPowNormAvg = squeeze(nanmean(SALPowNorm,1));
    SALCohAvg = squeeze(nanmean(SALCoh,1));
    SALCohNormAvg = squeeze(nanmean(SALCohNorm,1));
    
    for rr = 1:size(SALPow,1)
    figure(1)
    clf
    imagesc(squeeze(squeeze(SALPow(rr,1,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL Rat # ' int2str(rr) ' NAc Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_R' int2str(rr) '_NAc_D' int2str(days(bb))],'-dpng');
    
    figure(2)
    clf
    imagesc(squeeze(squeeze(SALPow(rr,2,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL Rat # ' int2str(rr) ' BLA Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_R' int2str(rr) '_NAc_D' int2str(days(bb))],'-dpng');
    
    figure(3)
    clf
    imagesc(squeeze(squeeze(SALPowNorm(rr,1,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL Rat # ' int2str(rr) ' NAc Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_R' int2str(rr) 'NAcNorm_day_' int2str(days(bb))],'-dpng');
    
    figure(4)
    clf
    imagesc(squeeze(squeeze(SALPowNorm(rr,2,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL Rat # ' int2str(rr) ' BLA Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_R' int2str(rr) 'BLANorm_Day ' int2str(days(bb))],'-dpng');
    
    figure(5)
    clf
    imagesc(squeeze(SALCoh(rr,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL R' int2str(rr) ' NAc-BLA coherence Day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_R' int2str(rr) 'coherence_day_' int2str(days(bb))],'-dpng');
    
    figure(6)
    clf
    imagesc(squeeze(SALCohNorm(rr,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL R' int2str(rr) ' NAc-BLA normalized coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([-0.2 0.2]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_R' int2str(rr) 'coherenceNorm_day_' int2str(days(bb))],'-dpng');
    end
    
    for rr = 1:size(VPAPow,1)
    figure(1)
    clf
    imagesc(squeeze(squeeze(VPAPow(rr,1,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA Rat # ' int2str(rr) ' NAc Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_R' int2str(rr) '_NAc_D' int2str(days(bb))],'-dpng');
    
    figure(2)
    clf
    imagesc(squeeze(squeeze(VPAPow(rr,2,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA Rat # ' int2str(rr) ' BLA Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_R' int2str(rr) '_NAc_D' int2str(days(bb))],'-dpng');
    
    figure(3)
    clf
    imagesc(squeeze(squeeze(VPAPowNorm(rr,1,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA Rat # ' int2str(rr) ' NAc Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_R' int2str(rr) 'NAcNorm_day_' int2str(days(bb))],'-dpng');
    
    figure(4)
    clf
    imagesc(squeeze(squeeze(VPAPowNorm(rr,2,lowfoi2plot,:))),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA Rat # ' int2str(rr) ' BLA Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_R' int2str(rr) 'BLANorm_Day ' int2str(days(bb))],'-dpng');
    
    figure(5)
    clf
    imagesc(squeeze(VPACoh(rr,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA R' int2str(rr) ' NAc-BLA coherence Day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_R' int2str(rr) 'coherence_day_' int2str(days(bb))],'-dpng');
    
    figure(6)
    clf
    imagesc(squeeze(VPACohNorm(rr,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA R' int2str(rr) ' NAc-BLA normalized coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([-0.2 0.2]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_R' int2str(rr) 'coherenceNorm_day_' int2str(days(bb))],'-dpng');
    end
    %% plot high freq gfor individual rats
    for rr = 1:size(SALPow,1)
    figure(1)
    clf
    imagesc(squeeze(squeeze(SALPow(rr,1,foi2plot,:))),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL Rat # ' int2str(rr) ' NAc Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_R' int2str(rr) '_NAc_D' int2str(days(bb))],'-dpng');
    
    figure(2)
    clf
    imagesc(squeeze(squeeze(SALPow(rr,2,foi2plot,:))),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL Rat # ' int2str(rr) ' BLA Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_R' int2str(rr) '_NAc_D' int2str(days(bb))],'-dpng');
    
    figure(3)
    clf
    imagesc(squeeze(squeeze(SALPowNorm(rr,1,foi2plot,:))),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL Rat # ' int2str(rr) ' NAc Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_R' int2str(rr) 'NAcNorm_day_' int2str(days(bb))],'-dpng');
    
    figure(4)
    clf
    imagesc(squeeze(squeeze(SALPowNorm(rr,2,foi2plot,:))),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL Rat # ' int2str(rr) ' BLA Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_R' int2str(rr) 'BLANorm_Day ' int2str(days(bb))],'-dpng');
    
    figure(5)
    clf
    imagesc(squeeze(SALCoh(rr,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL R' int2str(rr) ' NAc-BLA coherence Day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_R' int2str(rr) 'coherence_day_' int2str(days(bb))],'-dpng');
    
    figure(6)
    clf
    imagesc(squeeze(SALCohNorm(rr,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL R' int2str(rr) ' NAc-BLA normalized coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([-0.2 0.2]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_R' int2str(rr) 'coherenceNorm_day_' int2str(days(bb))],'-dpng');
    end
    
    for rr = 1:size(VPAPow,1)
    figure(1)
    clf
    imagesc(squeeze(squeeze(VPAPow(rr,1,foi2plot,:))),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA Rat # ' int2str(rr) ' NAc Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_R' int2str(rr) '_NAc_D' int2str(days(bb))],'-dpng');
    
    figure(2)
    clf
    imagesc(squeeze(squeeze(VPAPow(rr,2,foi2plot,:))),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA Rat # ' int2str(rr) ' BLA Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_R' int2str(rr) '_NAc_D' int2str(days(bb))],'-dpng');
    
    figure(3)
    clf
    imagesc(squeeze(squeeze(VPAPowNorm(rr,1,foi2plot,:))),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA Rat # ' int2str(rr) ' NAc Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_R' int2str(rr) 'NAcNorm_day_' int2str(days(bb))],'-dpng');
    
    figure(4)
    clf
    imagesc(squeeze(squeeze(VPAPowNorm(rr,2,foi2plot,:))),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA Rat # ' int2str(rr) ' BLA Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_R' int2str(rr) 'BLANorm_Day ' int2str(days(bb))],'-dpng');
    
    figure(5)
    clf
    imagesc(squeeze(VPACoh(rr,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA R' int2str(rr) ' NAc-BLA coherence Day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_R' int2str(rr) 'coherence_day_' int2str(days(bb))],'-dpng');
    
    figure(6)
    clf
    imagesc(squeeze(VPACohNorm(rr,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA R' int2str(rr) ' NAc-BLA normalized coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([-0.2 0.2]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_R' int2str(rr) 'coherenceNorm_day_' int2str(days(bb))],'-dpng');
    end
    
    
    figure(1)
    clf
    imagesc(squeeze(VPAPowAvg(1,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA NAc Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep  'VPA_NAc_D_' int2str(days(bb))],'-dpng');
    
    figure(2)
    clf
    imagesc(squeeze(VPAPowAvg(2,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA BLA Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_BLA_D_' int2str(days(bb))],'-dpng');
    
    figure(3)
    clf
    imagesc(squeeze(VPAPowNormAvg(1,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA NAc Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_NAcNorm_day_' int2str(days(bb))],'-dpng');
    
    figure(4)
    clf
    imagesc(squeeze(VPAPowNormAvg(2,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA BLA Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_BLANorm_day_' int2str(days(bb))],'-dpng');
    
    figure(5)
    clf
    imagesc(VPACohAvg(foi2plot,:),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA NAc-BLA coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_coherence_day_' int2str(days(bb))],'-dpng');
    
    figure(6)
    clf
    imagesc(VPACohNormAvg(foi2plot,:),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA NAc-BLA normalized coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([-0.2 0.2]);
    
    %     %% choose ticks for freqency axis
    % %     cfg.fticks = [9 15];
    %     cfg.fticks = 2.^unique(nextpow2(foi));
    %     nticks = cfg.fticks;
    %     linspc = diff(foi);
    %     logspc = foi(2:end)./foi(1:end-1);
    %     tol = 0.01;
    %     faxis = 1:numel(foi);
    %
    %     if ~any(abs(logspc-mean(logspc)) > tol) % log spacing
    %         P = unique(nextpow2(foi));
    %         while numel(P) < nticks(1)
    %             P = unique([P-diff(P(1:2))/2; P]);
    %         end
    %         while numel(P) > nticks(2)
    %             P = P(1:2:end);
    %         end
    %         cfg.fticks = unique(round(2.^P,3,'significant'));
    %     else  % linearly space nticks indices throughout freq.freq
    %         ftickI = round(linspace(nearest(foi,1),numel(foi), ...
    %             round(mean(nticks))));
    %         cfg.fticks = unique(round(foi(ftickI),3,'significant'));
    %     end
    %
    %     ftickI = zeros(size(cfg.fticks));
    %     for f = 1:numel(cfg.fticks)
    %         [~,ftickI(f)] = min(abs(foi - cfg.fticks(f)));
    % %         ftickI(f) = nearest(foi,cfg.fticks(f));
    %     end
    %     cfg.fticks = unique(round(foi(ftickI),3,'significant'));
    %     ftickI = unique(ftickI);
    %
    %
    %     linY = linspace(86/round(1.3^19-1.3^2));
    %     fTickI = downsample(1:numel(linY),18,1);
    %     fTicks = downsample(foi,18,1);
    %
    %     yticks(fTickI)
    %     yticklabels(cellstr(num2str(fTicks')));
    
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_coherenceNorm_day_' int2str(days(bb))],'-dpng');
    
    
    VPACohAvg(VPACohAvg<0.6) = 0;
    figure(7)
    clf
    imagesc(VPACohAvg(foi2plot,:),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA NAc-BLA masked coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0.4 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'VPA_coherenceMsk_day_' int2str(days(bb))],'-dpng');
    
    figure(1)
    clf
    imagesc(squeeze(SALPowAvg(1,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL NAc Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_NAc_day_' int2str(days(bb))],'-dpng');
    
    figure(2)
    clf
    imagesc(squeeze(SALPowAvg(2,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL BLA Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_BLA_day_' int2str(days(bb))],'-dpng');
    
    figure(3)
    clf
    imagesc(squeeze(SALPowNormAvg(1,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL NAc Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_NAcNorm_day_' int2str(days(bb))],'-dpng');
    
    figure(4)
    clf
    imagesc(squeeze(SALPowNormAvg(2,foi2plot,:)),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL BLA Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_BLANorm_Day ' int2str(days(bb))],'-dpng');
    
    figure(5)
    clf
    imagesc(SALCohAvg(foi2plot,:),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL NAc-BLA coherence Day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_coherence_day_' int2str(days(bb))],'-dpng');
    
    figure(6)
    clf
    imagesc(SALCohNormAvg(foi2plot,:),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL NAc-BLA normalized coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([-0.2 0.2]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_coherenceNorm_day_' int2str(days(bb))],'-dpng');
    
    SALCohAvg(SALCohAvg<0.6) = 0;
    figure(7)
    clf
    imagesc(SALCohAvg(foi2plot,:),'YData', foi(foi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL NAc-BLA masked coherence Day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0.4 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'highFreq' filesep 'SAL_coherenceMsk_day_' int2str(days(bb))],'-dpng');
    
    
    
    %% Plot Low Frequencies
    figure(1)
    clf
    imagesc(squeeze(VPAPowAvg(1,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA NAc Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep  'VPA_NAc_D_' int2str(days(bb))],'-dpng');
    
    figure(2)
    clf
    imagesc(squeeze(VPAPowAvg(2,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA BLA Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_BLA_D_' int2str(days(bb))],'-dpng');
    
    figure(3)
    clf
    imagesc(squeeze(VPAPowNormAvg(1,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA NAc Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_NAcNorm_day_' int2str(days(bb))],'-dpng');
    
    figure(4)
    clf
    imagesc(squeeze(VPAPowNormAvg(2,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA BLA Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_BLANorm_day_' int2str(days(bb))],'-dpng');
    
    figure(5)
    clf
    imagesc(VPACohAvg(lowfoi2plot,:),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA NAc-BLA coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_coherence_day_' int2str(days(bb))],'-dpng');
    
    figure(6)
    clf
    imagesc(VPACohNormAvg(lowfoi2plot,:),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA NAc-BLA normalized coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([-0.2 0.2]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_coherenceNorm_day_' int2str(days(bb))],'-dpng');
    
    
    VPACohAvg(VPACohAvg<0.6) = 0;
    figure(7)
    clf
    imagesc(VPACohAvg(lowfoi2plot,:),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['VPA NAc-BLA masked coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0.4 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'VPA_coherenceMsk_day_' int2str(days(bb))],'-dpng');
    
    figure(1)
    clf
    imagesc(squeeze(SALPowAvg(1,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL NAc Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_NAc_day_' int2str(days(bb))],'-dpng');
    
    figure(2)
    clf
    imagesc(squeeze(SALPowAvg(2,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL BLA Power day ' int2str(days(bb))])
    colormap jet
    colorbar
    hcol = colorbar;
    hcol.Label.String = 'dB';
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_BLA_day_' int2str(days(bb))],'-dpng');
    
    figure(3)
    clf
    imagesc(squeeze(SALPowNormAvg(1,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL NAc Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_NAcNorm_day_' int2str(days(bb))],'-dpng');
    
    figure(4)
    clf
    imagesc(squeeze(SALPowNormAvg(2,lowfoi2plot,:)),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL BLA Power day ' int2str(days(bb)) ', pre-tone normalized'])
    colormap jet
    colorbar
    caxis([-1 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_BLANorm_Day ' int2str(days(bb))],'-dpng');
    
    figure(5)
    clf
    imagesc(SALCohAvg(lowfoi2plot,:),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL NAc-BLA coherence Day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_coherence_day_' int2str(days(bb))],'-dpng');
    
    figure(6)
    clf
    imagesc(SALCohNormAvg(lowfoi2plot,:),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL NAc-BLA normalized coherence day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([-0.2 0.2]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_coherenceNorm_day_' int2str(days(bb))],'-dpng');
    
    SALCohAvg(SALCohAvg<0.6) = 0;
    figure(7)
    clf
    imagesc(SALCohAvg(lowfoi2plot,:),'YData', foi(lowfoi2plot), 'XData', toi)
    axis xy
    ylabel('Frequency (Hz)')
    xlabel('seconds')
    title(['SAL NAc-BLA masked coherence Day ' int2str(days(bb))])
    colormap jet
    colorbar
    caxis([0.4 1]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
    print([figurefolder 'lowFreq' filesep 'SAL_coherenceMsk_day_' int2str(days(bb))],'-dpng');
end

VPAPowNormThetaNAc = squeeze(VPAPowNormTheta(:,1,:,:));
VPAPowNormBetaNAc = squeeze(VPAPowNormBeta(:,1,:,:));
VPAPowNormGammaNAc = squeeze(VPAPowNormGamma(:,1,:,:));
SALPowNormThetaNAc = squeeze(SALPowNormTheta(:,1,:,:));
SALPowNormBetaNAc = squeeze(SALPowNormBeta(:,1,:,:));
SALPowNormGammaNAc = squeeze(SALPowNormGamma(:,1,:,:));

VPAPowNormThetaBLA = squeeze(VPAPowNormTheta(:,2,:,:));
VPAPowNormBetaBLA = squeeze(VPAPowNormBeta(:,2,:,:));
VPAPowNormGammaBLA = squeeze(VPAPowNormGamma(:,2,:,:));
SALPowNormThetaBLA = squeeze(SALPowNormTheta(:,2,:,:));
SALPowNormBetaBLA = squeeze(SALPowNormBeta(:,2,:,:));
SALPowNormGammaBLA = squeeze(SALPowNormGamma(:,2,:,:));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% still work in progress
VPAPowBaselineThetaNAc = squeeze(squeeze(mean(VPAPowbaseline(:,:,1,foiTheta(1):foiTheta(end)),4)));
VPAPowBaselineBetaNAc = squeeze(squeeze(mean(VPAPowbaseline(:,:,1,foiBeta(1):foiBeta(end)),4)));
VPAPowBaselineGammaNAc = squeeze(squeeze(mean(VPAPowbaseline(:,:,1,foiGamma(1):foiGamma(end)),4)));
SALPowBaselineThetaNAc = squeeze(squeeze(mean(SALPowbaseline(:,:,1,foiTheta(1):foiTheta(end)),4)));
SALPowBaselineBetaNAc = squeeze(squeeze(mean(SALPowbaseline(:,:,1,foiBeta(1):foiBeta(end)),4)));
SALPowBaselineGammaNAc = squeeze(squeeze(mean(SALPowbaseline(:,:,1,foiGamma(1):foiGamma(end)),4)));

VPAPowBaselineThetaBLA = squeeze(squeeze(mean(VPAPowbaseline(:,:,2,foiTheta(1):foiTheta(end)),4)));
VPAPowBaselineBetaBLA = squeeze(squeeze(mean(VPAPowbaseline(:,:,2,foiBeta(1):foiBeta(end)),4)));
VPAPowBaselineGammaBLA = squeeze(squeeze(mean(VPAPowbaseline(:,:,2,foiGamma(1):foiGamma(end)),4)));
SALPowBaselineThetaBLA = squeeze(squeeze(mean(SALPowbaseline(:,:,2,foiTheta(1):foiTheta(end)),4)));
SALPowBaselineBetaBLA = squeeze(squeeze(mean(SALPowbaseline(:,:,2,foiBeta(1):foiBeta(end)),4)));
SALPowBaselineGammaBLA = squeeze(squeeze(mean(SALPowbaseline(:,:,2,foiGamma(1):foiGamma(end)),4)));


VPACohBaselineTheta = squeeze(mean(VPACohbaseline(:,:,foiTheta(1):foiTheta(end)),3));
VPACohBaselineBeta = squeeze(mean(VPACohbaseline(:,:,foiBeta(1):foiBeta(end)),3));
VPACohBaselineGamma = squeeze(mean(VPACohbaseline(:,:,foiGamma(1):foiGamma(end)),3));
SALCohBaselineTheta = squeeze(mean(SALCohbaseline(:,:,foiTheta(1):foiTheta(end)),3));
SALCohBaselineBeta = squeeze(mean(SALCohbaseline(:,:,foiBeta(1):foiBeta(end)),3));
SALCohBaselineGamma = squeeze(mean(SALCohbaseline(:,:,foiGamma(1):foiGamma(end)),3));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% VPAPowNormThetaNAcD = NaN(VPAsize,7);
% VPAPowNormBetaNAcD = NaN(VPAsize,7);
% VPAPowNormGammaNAcD = NaN(VPAsize,7);
% SALPowNormThetaNAcD = NaN(SALsize,7);
% SALPowNormBetaNAcD = NaN(SALsize,7);
% SALPowNormGammaNAcD = NaN(SALsize,7);
% VPAPowNormThetaBLAD = NaN(VPAsize,7);
% VPAPowNormBetaBLAD = NaN(VPAsize,7);
% VPAPowNormGammaBLAD = NaN(VPAsize,7);
% SALPowNormThetaBLAD = NaN(SALsize,7);
% SALPowNormBetaBLAD = NaN(SALsize,7);
% SALPowNormGammaBLAD = NaN(SALsize,7);

% for dd = 1:7
%     kk = 3*(dd-1)+1:3*(dd-1)+3;
% VPAPowNormThetaNAcD(:,dd) = squeeze(mean(VPAPowNormTheta(:,1,kk),3));
% VPAPowNormBetaNAcD(:,dd) = squeeze(mean(VPAPowNormBeta(:,1,kk),3));
% VPAPowNormGammaNAcD(:,dd) = squeeze(mean(VPAPowNormGamma(:,1,kk),3));
% SALPowNormThetaNAcD(:,dd) = squeeze(mean(SALPowNormTheta(:,1,kk),3));
% SALPowNormBetaNAcD(:,dd) = squeeze(mean(SALPowNormBeta(:,1,kk),3));
% SALPowNormGammaNAcD(:,dd) = squeeze(mean(SALPowNormGamma(:,1,kk),3));
% VPAPowNormThetaBLAD(:,dd) = squeeze(mean(VPAPowNormTheta(:,2,kk),3));
% VPAPowNormBetaBLAD(:,dd) = squeeze(mean(VPAPowNormBeta(:,2,kk),3));
% VPAPowNormGammaBLAD(:,dd) = squeeze(mean(VPAPowNormGamma(:,2,kk),3));
% SALPowNormThetaBLAD(:,dd) = squeeze(mean(SALPowNormTheta(:,2,kk),3));
% SALPowNormBetaBLAD(:,dd) = squeeze(mean(SALPowNormBeta(:,2,kk),3));
% SALPowNormGammaBLAD(:,dd) = squeeze(mean(SALPowNormGamma(:,2,kk),3));
% end

% SALCohThetaD = NaN(SALsize,7);
% SALCohBetaD = NaN(SALsize,7);
% SALCohGammaD = NaN(SALsize,7);
% VPACohThetaD = NaN(VPAsize,7);
% VPACohBetaD = NaN(VPAsize,7);
% VPACoGammaD = NaN(VPAsize,7);

% for dd = 1:7
%     kk = 3*(dd-1)+1:3*(dd-1)+3;
%     SALCohThetaD(:,dd) = mean(SALCohTheta(:,kk));
%     SALCohBetaD(:,dd) = mean(SALCohBeta(:,kk));
%     SALCohGammaD(:,dd) = mean(SALCoGamma(:,kk));
%     VPACohThetaD(:,dd) = mean(VPACohTheta(:,kk));
%     VPACohBetaD(:,dd) = mean(VPACohBeta(:,kk));
%     VPACoGammaD(:,dd) = mean(VPACoGamma(:,kk));
% end

