%% Calculate raw spectrograms and coherograms for each trial

close all; clearvars; clc;  % clean slate

info_VPA_RC               % load all background info (filenames, etc)

foi = 1.5:0.5:120;   % freqs from 0.75-150 Hz progressing up by 20% each
tapsmofrq = foi/5;             % smooth each band by +/- 20%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -4:min(t_ftimwin)/2:12;    % time windows overlap by at least 50%
errcnt = 0;                       % error count
errloc = [];                      % error location (r,p,t)
bk2cal = 1:27;
bk2calD = [1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8,9,9,9];
day = 1:9;
%% use try-catch when running unsupervised batches

for r = 1:size(ratT,1)
    % Tone-triggered trials with artifacts removed (except for fake
    % tone on/off artifacts).  Best for spectrograms & coherograms.
    inputfile = [datafolder 'FullData' filesep 'FC' filesep 'CleanLFPTrialData_' ...
    int2str(ratT.ratnums(r)) 'Merged.mat'];


    %% load data
    
    load(inputfile);
    
    parfor b = 1:numel(day)
        
        
        
        % Each tone will have its own output file.  Checking here for one
        % of the last tones.
        outputfile = [datafolder 'FullData' filesep 'FC' filesep 'SpecCoherograms_' ...
            int2str(ratT.ratnums(r)) '-4to12_linScale_D' int2str(day(b)) '.mat'];
        
        %% check existing data
        
        %         filecorrect = false; % start false in case file doesn't exist
        %
        %         if exist(outputfile,'file')
        %           filecorrect = true; % true unless demonstrated to be false
        %           disp(['Data for Rat # ' int2str(ratT.ratnums(r)) ', ' ...
        %             blocks{bk2cal(b)} ' already analyzed! Check for accuracy.']);
        %           load(outputfile);
        %
        %           if ~any(any(any(~isnan(freq.powspctrm))))
        %             warning('Only NaNs output from freqanalysis');
        %             filecorrect = false;
        %           end
        %
        %           if freq.time(1) >= 0
        %             warning('no pre-tone baseline found');
        %             filecorrect = false;
        %           else
        %             disp(['first timestamp is ' num2str(freq.time(1))]);
        %           end
        %
        %           %           if strcmp(goodchs{r}{1},'AD*')
        %           %             if numel(freq.label) ~= (numel(lftchs) + numel(rtchs) + 1 ...
        %           %                 - numel(goodchs{r}))
        %           %               warning('different channels used than planned');
        %           %               filecorrect = false;  % reprocess this file
        %           %               %             else
        %           %               %               for ch = 1:numel(freq.label)
        %           %               %                 k = strfind(goodchs{r}, freq.label{ch});
        %           %               %                 if any([k{:}])
        %           %               %                   warning([freq.label{ch} ' should be excluded']);
        %           %               %                   filecorrect = false;  % reprocess this file
        %           %               %                   break  % jumps to end of for ch loop
        %           %               %                 else
        %           %               %                   disp([freq.label{ch} ' is not excluded']);
        %           %               %                 end
        %           %               %               end   % still true if no excluded channels
        %           %             end
        %           %           elseif numel(freq.label) ~= numel(goodchs{r}) || ...
        %           %               any(any(~strcmp(freq.label,goodchs{r})))
        %           %             warning('different channels used than planned');
        %           %             filecorrect = false;  % reprocess this file
        %           %           else
        %           %             disp([freq.label{:} ' are correct']);   % still true
        %           %           end
        %         end
        %
        %         %% reprocess if necessary
        %
        %         if filecorrect
        %           disp('checked file seems correct');
        %         else
        
        %% loop through tones
        try
           
            trials = find(data.trialinfo(:,2) == day(b))';
            if isempty(trials)
                warning(['No trials found for ' ...
                    num2str(ratT.ratnums(r)) '''s ' int2str(bk2cal(b)) 'block']);
            else
                disp(['Calculating spectrograms & coherograms for trials of '...
                    num2str(ratT.ratnums(r)) '''s ' int2str(bk2cal(b)) ]);
                
                %% calculate spectrograms & coherograms
                channel = cell(2,1);
                channel{1} = ratT.chNAc{r};
                channel{2} = ratT.chBLA{r};
                cfg             = [];
                
                cfg.method      = 'wavelet';
                cfg.output      = 'powandcsd';
                cfg.keeptrials  = 'no';
                cfg.trials      = trials;
                cfg.channel     = channel;
                cfg.foi         = foi;
                cfg.tapsmofrq   = tapsmofrq;
                cfg.t_ftimwin   = t_ftimwin;
                cfg.toi         = toi;
                cfg.pad = 'nextpow2'; % assumes longest trial is 500s
                
                cfg.outputfile  = outputfile;
                
                freq = ft_freqanalysis(cfg,data);
                
                if ~any(any(any(~isnan(freq.powspctrm))))
                    error('Only NaNs output from freqanalysis');
                end
            end
        catch ME1
            errcnt = errcnt + 1;
            %                     errloc(errcnt,:) = [r b t];
            warning(['Error (' ME1.message ') while processing Rat # ' ...
                int2str(ratT.ratnums(r)) ', ' int2str(bk2cal(b)) ...
                '! Continuing with next in line.']);
        end
        
        
        %         end
        %% clear variables
        
        %         clearvars data cfg freq;
    end
end