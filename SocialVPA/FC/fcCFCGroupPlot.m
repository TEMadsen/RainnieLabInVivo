% merge MI2dstack and phaseprefstack of all rats and plot group figure

close all; clearvars; clc;  % clean slate

info_VPA_RC                 % load all background info (filenames, etc)
plt = 'y';                  % plot CFC

%% find side index
sides = {'left','right'};                 % plot by laterality or treatment

rightInd = false(numel(sides),1);
leftInd = false(numel(sides),1);
for rr = 1:numel(ratT(:,1))
    rightInd(rr,1) = strcmp(ratT.side{rr},'Right');  
end

for rr = 1:numel(ratT(:,1))
    leftInd(rr,1) = strcmp(ratT.side{rr},'Left');
end

%% plot

inputfile1 = [datafolder 'crossKLMI2dPretone.mat'];
inputfile2 = [datafolder 'crossKLMI2d.mat'];
inputfile3 = [datafolder 'crossKLMI2dDivNor.mat'];
inputfile4 = [datafolder 'crossKLMI2dSubNor.mat'];


for ii = 1:4
load(['inputfile' num2str(ii)])

for ig = 1:numel(group) 
    
    


load(inputfile1)
end
end
