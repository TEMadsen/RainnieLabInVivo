info_VPA_RC

set(0,'DefaultFigureWindowStyle','docked')

%% import data
% Import behavioral data from ABET exported file. Data includes: Magazine
% Entry (transit on) and tone timestamps. Data includes 7 days of
% conditioning training and 2 days on extinction training.

% Rat info and preallocate space into a structure
RatID = [361, 362, 363, 364, 377, 378, 379, 380, 391, 392, 394, ...
    395, 396, 397, 398, 416, 417, 418, 419, 458, 459, 460];
Groups = {'VPA';'Saline';'VPA';'Saline';'VPA';'Saline';'VPA';'Saline';...
    'VPA';'Saline';'Saline';'VPA';'Saline';'VPA';'Saline';'Saline';'VPA';...
    'Saline';'VPA';'Saline';'VPA';'Saline'};
Phases = {'conditioning', 7; 'extinction', 2};
TLT = Phases{3} + Phases{4}; %total # of sessions of training
TotalTrial = 60; % total # of trail per session

% 7 days of conditioning and 2 days of extinction

for r = 1:numel(RatID)
    BehavRaw(r) = struct('RatID',[],'treatment',[], 'Phases', []);
    BehavRaw(r).RatID = RatID(r);
    BehavRaw(r).treatment = Groups(r);
    BehavRaw(r).Phases = struct('conditioning', [], 'extinction', []);
    BehavRaw(r).Phases.conditioning = struct('TS', [], 'MagEvent', [], ...
        'MagEtry', [], 'MagExit', []);
    BehavRaw(r).Phases.conditioning(Phases{3}) = struct('TS', [], ...
        'MagEvent', [], 'MagEtry', [], 'MagExit', []);
    BehavRaw(r).Phases.extinction= struct('TS', [], ...
        'MagEvent', [], 'MagEtry', [], 'MagExit', []);
    BehavRaw(r).Phases.extinction(Phases{4})= struct('TS', [], ...
        'MagEvent', [], 'MagEtry', [], 'MagExit', []);
end

% import Magazine entries and time stamps for conditioning training days

NotExist = false(numel(RatID),numel(Phases), Phases{3}, 2);
% 3rd days
% 4th dimension: [Magazine Entry, Tone TS]
% to be continued, how to change 0 to 1 in logical array?

for r = 1:numel(RatID)
    for d = 1:Phases{3}
        
        RC = [tserver 'Behavior' filesep 'Reinforcement conditioning' filesep ...
            int2str(RatID(r)) filesep int2str(RatID(r)) '_RC_D' int2str(d) '.csv'];
        RC_tone = [tserver 'Behavior' filesep 'Reinforcement conditioning' ...
            filesep int2str(RatID(r)) filesep int2str(RatID(r)) '_RC_D' ...
            int2str(d) '_tone.csv'];
        
        if ~exist(RC,'file')
            msgbox(['Input file MagEtr' int2str(RatID(r)) '_RC_D' int2str(d) ...
                'does not exist.'])
            NotExist(r, 1, d, 1) = true;
            BehavRaw(r).Phases.conditioning(d).MagEvent = 0; % add one entry TS at 0 second
        else BehavRaw(r).Phases.conditioning(d).MagEvent = ImportMagazineEntries(RC);
        end
        
        if ~exist(RC_tone,'file')
            msgbox(['Input file MagEtr' int2str(RatID(r)) '_RC_D' int2str(d) ...
                '_tone does not exist.'])
            NotExist(r, 1, d, Phases{4}) = true;
        else BehavRaw(r).Phases.conditioning(d).TS = Import1stColNumCSV(RC_tone);
        end
        
        if numel(BehavRaw(r).Phases.conditioning(d).TS) ~= TotalTrial
            error('tone TS less or more than 60')
        end
        
    end
end


% import Magazine entries and time stamps for extinction training days
for r = 1:numel(RatID)
    for d = 1:Phases{4}
        EXT = [tserver 'Behavior' filesep 'Reinforcement conditioning' filesep ...
            int2str(RatID(r)) filesep int2str(RatID(r)) '_EXT_D' int2str(d) '.csv'];
        EXT_tone = [tserver 'Behavior' filesep 'Reinforcement conditioning' ...
            filesep int2str(RatID(r)) filesep int2str(RatID(r)) '_EXT_D' ...
            int2str(d) '_tone.csv'];
        
        if RatID(r) == 394 && d == 1
            BehavRaw(r).Phases.extinction(d).MagEvent = 0;
        elseif ~exist(EXT,'file')
            msgbox(['Input file MagEtr' int2str(RatID(r)) '_EXT_D' int2str(d) ...
                'does not exist.'])
            NotExist(r, 2, d, 1) = true;
            BehavRaw(r).Phases.extinction(d).MagEvent = 0; % add one entry TS at 0 second
        else BehavRaw(r).Phases.extinction(d).MagEvent = ImportMagazineEntries(EXT);
        end
        
        if ~exist(EXT_tone,'file')
            msgbox(['Input file MagEtr' int2str(RatID(r)) '_EXT_D' int2str(d) ...
                '_tone does not exist.'])
            NotExist(r, 2, d, 2) = true;
            
        else BehavRaw(r).Phases.extinction(d).TS = Import1stColNumCSV(EXT_tone);
        end
        
        if numel(BehavRaw(r).Phases.extinction(d).TS) ~= TotalTrial
            error('tone TS less or more than 60')
        end
    end
end


for r = 1:numel(RatID)
    for d = 1:Phases{3}
        BehavRaw(r).Phases.conditioning(d).EtryIdx = ...
            zeros(size(BehavRaw(r).Phases.conditioning(d).MagEvent,1),1);
        for idx = 1:numel(BehavRaw(r).Phases.conditioning(d).EtryIdx)
            BehavRaw(r).Phases.conditioning(d).EtryIdx(idx) = ...
                contains(BehavRaw(r).Phases.conditioning(d).MagEvent{idx,2},'Entry');
        end
        temp = cell2table(BehavRaw(r).Phases.conditioning(d).MagEvent);
        if isvector(temp{logical(BehavRaw(r).Phases.conditioning(d).EtryIdx),1}) == 1
            BehavRaw(r).Phases.conditioning(d).MagEtry = ...
            temp{logical(BehavRaw(r).Phases.conditioning(d).EtryIdx),1};
        else
        BehavRaw(r).Phases.conditioning(d).MagEtry = ...
            table2array(cell2table(temp{logical(BehavRaw(r).Phases.conditioning(d).EtryIdx),1}));
        end
    end
end

for r = 1:numel(RatID)
    for d = 1:Phases{4}
        BehavRaw(r).Phases.extinction(d).EtryIdx = ...
            zeros(size(BehavRaw(r).Phases.extinction(d).MagEvent,1),1);
        if isvector(BehavRaw(r).Phases.extinction(d).MagEvent) == 1
            BehavRaw(r).Phases.extinction(d).MagEntry = [];
        else
            for idx = 1:numel(BehavRaw(r).Phases.extinction(d).EtryIdx)
                BehavRaw(r).Phases.extinction(d).EtryIdx(idx) = ...
                    contains(BehavRaw(r).Phases.extinction(d).MagEvent{idx,2},'Entry');
            end
            
            temp = cell2table(BehavRaw(r).Phases.extinction(d).MagEvent);
            if isvector(temp{logical(BehavRaw(r).Phases.extinction(d).EtryIdx),1}) == 1
                BehavRaw(r).Phases.extinction(d).MagEtry = ...
                    temp{logical(BehavRaw(r).Phases.extinction(d).EtryIdx),1};
            else
                BehavRaw(r).Phases.extinction(d).MagEtry = ...
                    table2array(cell2table(temp{logical(BehavRaw(r).Phases.extinction(d).EtryIdx),1}));
            end
        end
    end
end

save([datafolder 'RCBehavRaw.mat'],'BehavRaw','RatID','Groups','Phases','TLT'...
    ,'TotalTrial');

%% Reorganize data by magazine entry and timestamp
% Merge two phases, conditioning and extinction, and assign Magazine entry
% and TS data to new structure.
% Conditioning : day 1-7. Extinction: day 8-9

% Preallocation
MagEtyRaw(numel(RatID)) = struct('RatID',[],'treatment',[], 'day', []);
for r = 1:numel(RatID)
    MagEtyRaw(r).day = cell(9,1);
end

TSRaw(numel(RatID)) = struct('RatID',[],'treatment',[], 'day', []);
for r = 1:numel(RatID)
    TSRaw(r).day = cell(9,1);
end

% Assign value
for r = 1:numel(RatID)
    MagEtyRaw(r).RatID = RatID(r);
    MagEtyRaw(r).treatment = Groups(r);
    for d = 1:Phases{3}
        MagEtyRaw(r).day{d,1} = ...
            table2array(cell2table(BehavRaw(r).Phases.conditioning(d).MagEtry));
    end
    for d = 1:Phases{4}
        if isempty(BehavRaw(r).Phases.extinction(d).MagEtry) == 1
              MagEtyRaw(r).day{d+Phases{3},1} = [];
        else
        MagEtyRaw(r).day{d+Phases{3},1} = ...
            table2array(cell2table(BehavRaw(r).Phases.extinction(d).MagEtry));
        end
    end
end

for r = 1:numel(RatID)
    TSRaw(r).RatID = RatID(r);
    TSRaw(r).treatment = Groups(r);
    for d = 1:Phases{3}
        TSRaw(r).day{d} = BehavRaw(r).Phases.conditioning(d).TS;
    end
    for d = 1:Phases{4}
        TSRaw(r).day{d+Phases{3}} = ...
            BehavRaw(r).Phases.extinction(d).TS;
    end
end

%% filter too short ITIs
% filter too short ITIs. Set Min ITI at 200ms or 500ms.

% calculate the interval of between magazine entries of all data from one
% rat combined and plot a histogram for each rat.

MagEtyDiff = cell(numel(RatID),1);

for r = 1:numel(RatID)
    size = 0;
    for t = 1:TLT
        size = size + numel(MagEtyRaw(r).day{t,1});
        MagEtyDiff{r} = NaN([size, 1]);
        temp = 0;
    end
    for d = 1:TLT
        temp = [temp; diff(MagEtyRaw(r).day{d,1})]; % how to preallocate?
        temp(1) = [];
        MagEtyDiff{r} = temp;
    end
end

histogram(MagEtyDiff{r}, 0:0.05:3)
hold on
for r = 1:(numel(RatID)-1)
    histogram(MagEtyDiff{r+1}, 0:0.05:3)
end
ylabel('couuts')
xlabel('ITIs of Magazine Entry (seconds)')

% use while loop to make sure that there's no more too short ITIs?

MagEty250 = MagEtyRaw;
for r = 1:numel(RatID)
    for d = 1:TLT
        session = MagEty250(r).day{d,1};
        session([false(1); diff(MagEty250(r).day{d,1}) < 0.25]) = [];
        MagEty250(r).day{d,1} = session;
    end
end

MagEty500 = MagEtyRaw;
for r = 1:numel(RatID)
    for d = 1:TLT
        session = MagEty500(r).day{d,1};
        session([false(1); diff(MagEty500(r).day{d,1}) < 0.5]) = [];
        MagEty500(r).day{d,1} = session;
    end
end

MagEtyRound = MagEtyRaw;
for r = 1:numel(RatID)
    for d = 1:TLT
        session = unique(round(MagEtyRound(r).day{d,1}));
        MagEtyRound(r).day{d,1} = session;
    end
end


%% Count magazine entries during tone

% (RatID, [Before,During,After tone],Total # of trials)
MagEtyCt = cell(numel(RatID),TLT,3); % no minimum ITI
MagEtyCt250 = cell(numel(RatID),TLT,3); % 250ms minimum ITI
MagEtyCt500 = cell(numel(RatID),TLT,3); % 500ms minimum ITI
MagEtyCtRound = cell(numel(RatID),TLT,3); % rounding method

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:TotalTrial
            E = MagEtyRaw(r).day{d,1};
            MagEtyCt{r,d,1} = [MagEtyCt{r,d,1}; sum(TSRaw(r).day{d}(t,1) >= E & E >= ...
                TSRaw(r).day{d}(t,1)-6)];
            MagEtyCt{r,d,2} = [MagEtyCt{r,d,2}; sum(TSRaw(r).day{d}(t,1)+6 >= E & E >= ...
                TSRaw(r).day{d}(t,1))];
            MagEtyCt{r,d,3} = [MagEtyCt{r,d,3}; sum(TSRaw(r).day{d}(t,1)+12 >= E & E >= ...
                TSRaw(r).day{d}(t,1)+6)];
        end
    end
end

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:TotalTrial
            E = MagEty250(r).day{d,1};
            MagEtyCt250{r,d,1} = [MagEtyCt250{r,d,1}; sum(TSRaw(r).day{d}(t,1) >= E & E >= ...
                TSRaw(r).day{d}(t,1)-6)];
            MagEtyCt250{r,d,2} = [MagEtyCt250{r,d,2}; sum(TSRaw(r).day{d}(t,1)+6 >= E & E >= ...
                TSRaw(r).day{d}(t,1))];
            MagEtyCt250{r,d,3} = [MagEtyCt250{r,d,3}; sum(TSRaw(r).day{d}(t,1)+12 >= E & E >= ...
                TSRaw(r).day{d}(t,1)+6)];
        end
    end
end

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:TotalTrial
            E = MagEty500(r).day{d,1};
            MagEtyCt500{r,d,1} = [MagEtyCt500{r,d,1}; sum(TSRaw(r).day{d}(t,1) >= E & E >= ...
                TSRaw(r).day{d}(t,1)-6)];
            MagEtyCt500{r,d,2} = [MagEtyCt500{r,d,2}; sum(TSRaw(r).day{d}(t,1)+6 >= E & E >= ...
                TSRaw(r).day{d}(t,1))];
            MagEtyCt500{r,d,3} = [MagEtyCt500{r,d,3}; sum(TSRaw(r).day{d}(t,1)+12 >= E & E >= ...
                TSRaw(r).day{d}(t,1)+6)];
        end
    end
end

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:TotalTrial
            E = MagEtyRound(r).day{d,1};
            MagEtyCtRound{r,d,1} = [MagEtyCtRound{r,d,1}; sum(TSRaw(r).day{d}(t,1) >= E & E >= ...
                TSRaw(r).day{d}(t,1)-6)];
            MagEtyCtRound{r,d,2} = [MagEtyCtRound{r,d,2}; sum(TSRaw(r).day{d}(t,1)+6 >= E & E >= ...
                TSRaw(r).day{d}(t,1))];
            MagEtyCtRound{r,d,3} = [MagEtyCtRound{r,d,3}; sum(TSRaw(r).day{d}(t,1)+12 >= E & E >= ...
                TSRaw(r).day{d}(t,1)+6)];
        end
    end
end

%% calculate average # of magazine entry

% every 60 trials (1 session)
MagEtyCtAvg = cell(numel(RatID),TLT,3); % no minimum ITI
MagEtyCt250Avg = cell(numel(RatID),TLT,3); % 250ms minimum ITI
MagEtyCt500Avg = cell(numel(RatID),TLT,3); % 500ms minimum ITI
MagEtyCtRoundAvg = cell(numel(RatID),TLT,3); % rounding method

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:3
            MagEtyCtAvg{r,d,t} = mean(MagEtyCt{r,d,t});
        end
    end
end

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:3
            MagEtyCt250Avg{r,d,t} = mean(MagEtyCt250{r,d,t});
        end
    end
end

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:3
            MagEtyCt500Avg{r,d,t} = mean(MagEtyCt500{r,d,t});
        end
    end
end

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:3
            MagEtyCtRoundAvg{r,d,t} = mean(MagEtyCtRound{r,d,t});
        end
    end
end

% every 30 trials
MagEtyCtAvgSplt = cell(numel(RatID),TLT*2,3); % no minimum ITI
MagEtyCt250AvgSplt = cell(numel(RatID),TLT*2,3); % 250ms minimum ITI
MagEtyCt500AvgSplt = cell(numel(RatID),TLT*2,3); % 500ms minimum ITI
MagEtyCtRoundAvgSplt = cell(numel(RatID),TLT*2,3); % rounding method

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:3
            d1 = 2*d-1;
            d2 = 2*d;
            MagEtyCtAvgSplt{r,d1,t} = mean(MagEtyCt{r,d,t}(1:TotalTrial/2));
            MagEtyCtAvgSplt{r,d2,t} = mean(MagEtyCt{r,d,t}(TotalTrial/2+1:TotalTrial));
        end
    end
end

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:3
            d1 = 2*d-1;
            d2 = 2*d;
            MagEtyCt250AvgSplt{r,d1,t} = mean(MagEtyCt250{r,d,t}(1:TotalTrial/2));
            MagEtyCt250AvgSplt{r,d2,t} = mean(MagEtyCt250{r,d,t}(TotalTrial/2+1:TotalTrial));
        end
    end
end

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:3
            d1 = 2*d-1;
            d2 = 2*d;
            MagEtyCt500AvgSplt{r,d1,t} = mean(MagEtyCt500{r,d,t}(1:TotalTrial/2));
            MagEtyCt500AvgSplt{r,d2,t} = mean(MagEtyCt500{r,d,t}(TotalTrial/2+1:TotalTrial));
        end
    end
end

for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:3
            d1 = 2*d-1;
            d2 = 2*d;
            MagEtyCtRoundAvgSplt{r,d1,t} = mean(MagEtyCtRound{r,d,t}(1:TotalTrial/2));
            MagEtyCtRoundAvgSplt{r,d2,t} = mean(MagEtyCtRound{r,d,t}(TotalTrial/2+1:TotalTrial));
        end
    end
end
% reorganize the data by group (VPA Saline)

% Find the index of each group
SAL = [];
VPA = [];

for n = 1:numel(RatID)
    if strcmpi(Groups{n}, 'Saline') == true
        SAL = [SAL n];
    end
end

for n = 1:numel(RatID)
    if strcmpi(Groups{n}, 'VPA') == true
        VPA = [VPA n];
    end
end

% reorganizing by average entries of 60 trials (1 session/day) as an unit

SALMagEtyDur = NaN(numel(SAL),TLT);
VPAMagEtyDur = NaN(numel(VPA),TLT);
SALMagEtyDur250 = NaN(numel(SAL),TLT);
VPAMagEtyDur250 = NaN(numel(VPA),TLT);
SALMagEtyDur500 = NaN(numel(SAL),TLT);
VPAMagEtyDur500 = NaN(numel(VPA),TLT);
SALMagEtyDurRound = NaN(numel(SAL),TLT);
VPAMagEtyDurRound = NaN(numel(VPA),TLT);

for n = 1:numel(SAL)
    for d = 1:TLT
        SALMagEtyDur(n,d) =  MagEtyCtAvg{SAL(n),d,2};
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        VPAMagEtyDur(n,d) =  MagEtyCtAvg{VPA(n),d,2};
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT
        SALMagEtyDur250(n,d) =  MagEtyCt250Avg{SAL(n),d,2};
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        VPAMagEtyDur250(n,d) =  MagEtyCt250Avg{VPA(n),d,2};
    end
end


for n = 1:numel(SAL)
    for d = 1:TLT
        SALMagEtyDur500(n,d) =  MagEtyCt500Avg{SAL(n),d,2};
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        VPAMagEtyDur500(n,d) =  MagEtyCt500Avg{VPA(n),d,2};
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT
        SALMagEtyDurRound(n,d) =  MagEtyCtRoundAvg{SAL(n),d,2};
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        VPAMagEtyDurRound(n,d) =  MagEtyCtRoundAvg{VPA(n),d,2};
    end
end

% reorganizing by average entries of 30 trials (2 sessions/day) as an unit

SALMagEtyDurSplt = NaN(numel(SAL),TLT*2);
VPAMagEtyDurSplt = NaN(numel(VPA),TLT*2);
SALMagEtyDur250Splt = NaN(numel(SAL),TLT*2);
VPAMagEtyDur250Splt = NaN(numel(VPA),TLT*2);
SALMagEtyDur500Splt = NaN(numel(SAL),TLT*2);
VPAMagEtyDur500Splt = NaN(numel(VPA),TLT*2);
SALMagEtyDurRoundSplt = NaN(numel(SAL),TLT*2);
VPAMagEtyDurRoundSplt = NaN(numel(VPA),TLT*2);

for n = 1:numel(SAL)
    for d = 1:TLT*2
        SALMagEtyDurSplt(n,d) =  MagEtyCtAvgSplt{SAL(n),d,2};
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT*2
        VPAMagEtyDurSplt(n,d) =  MagEtyCtAvgSplt{VPA(n),d,2};
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT*2
        SALMagEtyDur500Splt(n,d) =  MagEtyCt500AvgSplt{SAL(n),d,2};
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT*2
        VPAMagEtyDur500Splt(n,d) =  MagEtyCt500AvgSplt{VPA(n),d,2};
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT*2
        SALMagEtyDur250Splt(n,d) =  MagEtyCt250AvgSplt{SAL(n),d,2};
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT*2
        VPAMagEtyDur250Splt(n,d) =  MagEtyCt250AvgSplt{VPA(n),d,2};
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT*2
        SALMagEtyDurRoundSplt(n,d) =  MagEtyCtRoundAvgSplt{SAL(n),d,2};
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT*2
        VPAMagEtyDurRoundSplt(n,d) =  MagEtyCtRoundAvgSplt{VPA(n),d,2};
    end
end


save([datafolder 'MagEtyDur.mat'],'SALMagEtyDurSplt','VPAMagEtyDurSplt','SALMagEtyDur500Splt', ...
    'VPAMagEtyDur500Splt','SALMagEtyDur250Splt','VPAMagEtyDur250Splt', ...
    'SALMagEtyDurRoundSplt','VPAMagEtyDurRoundSplt','SALMagEtyDur', ...
    'VPAMagEtyDur','SALMagEtyDur500','VPAMagEtyDur500','SALMagEtyDur250', ...
    'VPAMagEtyDur250','SALMagEtyDurRound','VPAMagEtyDurRound')

%% Calculate the difference of magazine entries between during and before
SALDiff = NaN(numel(SAL),TLT,TotalTrial);
VPADiff = NaN(numel(VPA),TLT,TotalTrial);
SALDiffRound = NaN(numel(SAL),TLT,TotalTrial);
VPADiffRound = NaN(numel(VPA),TLT,TotalTrial);

for n = 1:numel(SAL)
    for d = 1:TLT
        for t = 1:TotalTrial
            SALDiff(n,d,t) =  MagEtyDiffCal ...
                (MagEtyCt{SAL(n),d,1}(t),MagEtyCt{SAL(n),d,2}(t));
        end
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        for t = 1:TotalTrial
            VPADiff(n,d,t) =  MagEtyDiffCal ...
                (MagEtyCt{VPA(n),d,1}(t),MagEtyCt{VPA(n),d,2}(t));
        end
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT
        for t = 1:TotalTrial
            SALDiffRound(n,d,t) =  MagEtyDiffCal ...
                (MagEtyCtRound{SAL(n),d,1}(t),MagEtyCtRound{SAL(n),d,2}(t));
        end
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        for t = 1:TotalTrial
            VPADiffRound(n,d,t) =  MagEtyDiffCal ...
                (MagEtyCtRound{VPA(n),d,1}(t),MagEtyCtRound{VPA(n),d,2}(t));
        end
    end
end

save([datafolder 'MagEtyDiffRaw.mat'],'SALDiff','VPADiff','SALDiffRound','VPADiffRound')

% reorganizing by average entries of 60 trials (1 session/day) as an unit
SALDiffAvg = NaN(numel(SAL),TLT);
VPADiffAvg = NaN(numel(VPA),TLT);
SALDiffRoundAvg = NaN(numel(SAL),TLT);
VPADiffRoundAvg = NaN(numel(VPA),TLT);

for n = 1:numel(SAL)
    for d = 1:TLT
        SALDiffAvg(n,d) =  mean(SALDiff(n,d,1:60));
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        VPADiffAvg(n,d) =  mean(VPADiff(n,d,1:60));
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT
        SALDiffRoundAvg(n,d) =  mean(SALDiffRound(n,d,1:60));
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        VPADiffRoundAvg(n,d) =  mean(VPADiffRound(n,d,1:60));
    end
end

% reorganizing by average entries of 30 trials (2 sessions/day) as an unit

SALDiffSpltAvg = NaN(numel(SAL),TLT*2);
VPADiffSpltAvg = NaN(numel(VPA),TLT*2);
SALDiffRoundSpltAvg = NaN(numel(SAL),TLT*2);
VPADiffRoundSpltAvg = NaN(numel(VPA),TLT*2);

for r = 1:numel(SAL)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        SALDiffSpltAvg(r,d1) = mean(SALDiff(r,d,1:TotalTrial/2));
        SALDiffSpltAvg(r,d2) = mean(SALDiff(r,d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(VPA)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        VPADiffSpltAvg(r,d1) = mean(VPADiff(r,d,1:TotalTrial/2));
        VPADiffSpltAvg(r,d2) = mean(VPADiff(r,d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(SAL)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        SALDiffRoundSpltAvg(r,d1) = mean(SALDiffRound(r,d,1:TotalTrial/2));
        SALDiffRoundSpltAvg(r,d2) = mean(SALDiffRound(r,d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(VPA)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        VPADiffRoundSpltAvg(r,d1) = mean(VPADiffRound(r,d,1:TotalTrial/2));
        VPADiffRoundSpltAvg(r,d2) = mean(VPADiffRound(r,d,TotalTrial/2+1:TotalTrial));
    end
end

save([datafolder 'MagEtyDiffAvg.mat'],'SALDiffAvg','VPADiffAvg', ...
    'SALDiffRoundAvg','VPADiffRoundAvg')

%% Calaulate Behavioral Enhancing Ratio (BER)
% Use BERCal function

SALBER = NaN(numel(SAL),TLT,TotalTrial);
VPABER = NaN(numel(VPA),TLT,TotalTrial);
SALBER500 = NaN(numel(SAL),TLT,TotalTrial);
VPABER500 = NaN(numel(VPA),TLT,TotalTrial);
SALBER250 = NaN(numel(SAL),TLT,TotalTrial);
VPABER250 = NaN(numel(VPA),TLT,TotalTrial);
SALBERRound = NaN(numel(SAL),TLT,TotalTrial);
VPABERRound = NaN(numel(VPA),TLT,TotalTrial);

for n = 1:numel(SAL)
    for d = 1:TLT
        for t = 1:TotalTrial
            SALBER(n,d,t) =  BERCal(MagEtyCt{SAL(n),d,1}(t),MagEtyCt{SAL(n),d,2}(t));
        end
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        for t = 1:TotalTrial
            VPABER(n,d,t) =  BERCal(MagEtyCt{VPA(n),d,1}(t),MagEtyCt{VPA(n),d,2}(t));
        end
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT
        for t = 1:TotalTrial
            SALBER500(n,d,t) =  BERCal(MagEtyCt500{SAL(n),d,1}(t),MagEtyCt500{SAL(n),d,2}(t));
        end
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        for t = 1:TotalTrial
            VPABER500(n,d,t) =  BERCal(MagEtyCt500{VPA(n),d,1}(t),MagEtyCt500{VPA(n),d,2}(t));
        end
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT
        for t = 1:TotalTrial
            SALBER250(n,d,t) =  BERCal(MagEtyCt250{SAL(n),d,1}(t),MagEtyCt250{SAL(n),d,2}(t));
        end
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        for t = 1:TotalTrial
            VPABER250(n,d,t) =  BERCal(MagEtyCt250{VPA(n),d,1}(t),MagEtyCt250{VPA(n),d,2}(t));
        end
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT
        for t = 1:TotalTrial
            SALBERRound(n,d,t) =  BERCal(MagEtyCtRound{SAL(n),d,1}(t),MagEtyCtRound{SAL(n),d,2}(t));
        end
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        for t = 1:TotalTrial
            VPABERRound(n,d,t) =  BERCal(MagEtyCtRound{VPA(n),d,1}(t),MagEtyCtRound{VPA(n),d,2}(t));
        end
    end
end


save([datafolder 'BERRaw.mat'],'SALBER','VPABER','SALBER500','VPABER500',...
    'SALBER250','VPABER250','SALBERRound','VPABERRound')

% reorganizing by average entries of 60 trials (1 session/day) as an unit
SALBERAvg = NaN(numel(SAL),TLT);
VPABERAvg = NaN(numel(VPA),TLT);
SALBER500Avg = NaN(numel(SAL),TLT);
VPABER500Avg = NaN(numel(VPA),TLT);
SALBER250Avg = NaN(numel(SAL),TLT);
VPABER250Avg = NaN(numel(VPA),TLT);
SALBERRoundAvg = NaN(numel(SAL),TLT);
VPABERRoundAvg = NaN(numel(VPA),TLT);

for n = 1:numel(SAL)
    for d = 1:TLT
        SALBERAvg(n,d) =  mean(SALBER(n,d,1:60));
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        VPABERAvg(n,d) =  mean(VPABER(n,d,1:60));
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT
        SALBER500Avg(n,d) =  mean(SALBER500(n,d,1:60));
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        VPABER500Avg(n,d) =  mean(VPABER500(n,d,1:60));
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT
        SALBER250Avg(n,d) =  mean(SALBER250(n,d,1:60));
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        VPABER250Avg(n,d) =  mean(VPABER250(n,d,1:60));
    end
end

for n = 1:numel(SAL)
    for d = 1:TLT
        SALBERRoundAvg(n,d) =  mean(SALBERRound(n,d,1:60));
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        VPABERRoundAvg(n,d) =  mean(VPABERRound(n,d,1:60));
    end
end

% reorganizing by average entries of 30 trials (2 sessions/day) as an unit

SALBERSpltAvg = NaN(numel(SAL),TLT*2);
VPABERSpltAvg = NaN(numel(VPA),TLT*2);
SALBER500SpltAvg = NaN(numel(SAL),TLT*2);
VPABER500SpltAvg = NaN(numel(VPA),TLT*2);
SALBER250SpltAvg = NaN(numel(SAL),TLT*2);
VPABER250SpltAvg = NaN(numel(VPA),TLT*2);
SALBERRoundSpltAvg = NaN(numel(SAL),TLT*2);
VPABERRoundSpltAvg = NaN(numel(VPA),TLT*2);

for r = 1:numel(SAL)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        SALBERSpltAvg(r,d1) = mean(SALBER(r,d,1:TotalTrial/2));
        SALBERSpltAvg(r,d2) = mean(SALBER(r,d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(VPA)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        VPABERSpltAvg(r,d1) = mean(VPABER(r,d,1:TotalTrial/2));
        VPABERSpltAvg(r,d2) = mean(VPABER(r,d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(SAL)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        SALBER500SpltAvg(r,d1) = mean(SALBER500(r,d,1:TotalTrial/2));
        SALBER500SpltAvg(r,d2) = mean(SALBER500(r,d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(VPA)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        VPABER500SpltAvg(r,d1) = mean(VPABER500(r,d,1:TotalTrial/2));
        VPABER500SpltAvg(r,d2) = mean(VPABER500(r,d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(SAL)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        SALBER250SpltAvg(r,d1) = mean(SALBER250(r,d,1:TotalTrial/2));
        SALBER250SpltAvg(r,d2) = mean(SALBER250(r,d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(VPA)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        VPABER250SpltAvg(r,d1) = mean(VPABER250(r,d,1:TotalTrial/2));
        VPABER250SpltAvg(r,d2) = mean(VPABER250(r,d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(SAL)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        SALBERRoundSpltAvg(r,d1) = mean(SALBERRound(r,d,1:TotalTrial/2));
        SALBERRoundSpltAvg(r,d2) = mean(SALBERRound(r,d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(VPA)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        VPABERRoundSpltAvg(r,d1) = mean(VPABERRound(r,d,1:TotalTrial/2));
        VPABERRoundSpltAvg(r,d2) = mean(VPABERRound(r,d,TotalTrial/2+1:TotalTrial));
    end
end


save([datafolder 'BERAvg.mat'],'SALBERSpltAvg','VPABERSpltAvg','SALBER500SpltAvg', ...
    'VPABER500SpltAvg','SALBER250SpltAvg','VPABER250SpltAvg', ...
    'SALBERRoundSpltAvg','VPABERRoundSpltAvg','SALBERAvg','VPABERAvg',...
    'SALBER500Avg','VPABER500Avg','SALBER250Avg','VPABER250Avg', ...
    'SALBERRoundAvg','VPABERRoundAvg')

% plot

%% Find Latency of magazine entry for each tone
% %%%% Has PROBLEM NOT WORKING 10/10/2016 %%
MagEtyLt = NaN(numel(RatID),TLT,TotalTrial); % no maximum latency
% there are some sissions the rats did not enter the magazine at all...
% what should I do with it?
MagEtyLt6 = NaN(numel(RatID),TLT,TotalTrial); % 6 seconds maximum latency
MagEtyLt12 = NaN(numel(RatID),TLT,TotalTrial); % 12 seconds maximum latency

% Lt6
for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:TotalTrial
            I = find(MagEtyRaw(r).day{d,1} > TSRaw(r).day{d}(t,1), ...
                1, 'first');
            if isempty(I) == 1
                MagEtyLt6(r,d,t) = 6;
            elseif MagEtyRaw(r).day{d,1}(I)-TSRaw(r).day{d}(t,1) > 6
                MagEtyLt6(r,d,t) = 6;
            else
                MagEtyLt6(r,d,t) = MagEtyRaw(r).day{d,1}(I)- ...
                    TSRaw(r).day{d}(t,1);
            end
        end
    end
end

% Lt12
for r = 1:numel(RatID)
    for d = 1:TLT
        for t = 1:TotalTrial
            I = find(MagEtyRaw(r).day{d,1} > TSRaw(r).day{d}(t,1), ...
                1, 'first');
            if isempty(I) == 1
                MagEtyLt12(r,d,t) = 12;
            elseif MagEtyRaw(r).day{d,1}(I)-TSRaw(r).day{d}(t,1) > 12
                MagEtyLt12(r,d,t) = 12;
            else
                MagEtyLt12(r,d,t) = MagEtyRaw(r).day{d,1}(I)- ...
                    TSRaw(r).day{d}(t,1);
            end
        end
    end
end

% calcute average latency, 1 block of 60 trials per session(day)
SALMagEtyLtAvg = NaN(numel(SAL),TLT); % no maximum latency
VPAMagEtyLtAvg = NaN(numel(VPA),TLT); % no maximum latency
SALMagEtyLt6Avg = NaN(numel(SAL),TLT); % 6 seconds maximum latency
VPAMagEtyLt6Avg = NaN(numel(VPA),TLT); % 6 seconds maximum latency
SALMagEtyLt12Avg = NaN(numel(SAL),TLT); % 12 seconds maximum latency
VPAMagEtyLt12Avg = NaN(numel(VPA),TLT); % 12 seconds maximum latency

% for r = 1:numel(SAL)
%     for d = 1:TLT
%         SALMagEtyLtAvg(r,d) = mean(MagEtyLt(SAL(r),d,1:TotalTrial));
%     end
% end
%
% for r = 1:numel(VPA)
%     for d = 1:TLT
%         VPAMagEtyLtAvg(r,d) = mean(MagEtyLt(VPA(r),d,1:TotalTrial));
%     end
% end

for r = 1:numel(SAL)
    for d = 1:TLT
        SALMagEtyLt6Avg(r,d) = mean(MagEtyLt6(SAL(r),d,1:TotalTrial));
    end
end

for r = 1:numel(VPA)
    for d = 1:TLT
        VPAMagEtyLt6Avg(r,d) = mean(MagEtyLt6(VPA(r),d,1:TotalTrial));
    end
end

for r = 1:numel(SAL)
    for d = 1:TLT
        SALMagEtyLt12Avg(r,d) = mean(MagEtyLt12(SAL(r),d,1:TotalTrial));
    end
end

for r = 1:numel(VPA)
    for d = 1:TLT
        VPAMagEtyLt12Avg(r,d) = mean(MagEtyLt12(VPA(r),d,1:TotalTrial));
    end
end

% calcute average latency, 2 blocks of 30 trials per session(day)
SALMagEtyLtSpltAvg = NaN(numel(SAL),TLT); % no maximum latency
VPAMagEtyLtSpltAvg = NaN(numel(VPA),TLT); % no maximum latency
SALMagEtyLt6SpltAvg = NaN(numel(SAL),TLT); % 6 seconds maximum latency
VPAMagEtyLt6SpltAvg = NaN(numel(VPA),TLT); % 6 seconds maximum latency
SALMagEtyLt12SpltAvg = NaN(numel(SAL),TLT); % 12 seconds maximum latency
VPAMagEtyLt12SpltAvg = NaN(numel(VPA),TLT); % 12 seconds maximum latency

SALMagEtyLt6Avg(r,d) = mean(MagEtyLt6(SAL(r),d,1:TotalTrial));

for r = 1:numel(SAL)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        SALMagEtyLt6SpltAvg(r,d1) = mean(MagEtyLt6(SAL(r),d,1:TotalTrial/2));
        SALMagEtyLt6SpltAvg(r,d2) = mean(MagEtyLt6(SAL(r),d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(VPA)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        VPAMagEtyLt6SpltAvg(r,d1) = mean(MagEtyLt6(VPA(r),d,1:TotalTrial/2));
        VPAMagEtyLt6SpltAvg(r,d2) = mean(MagEtyLt6(VPA(r),d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(SAL)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        SALMagEtyLt12SpltAvg(r,d1) = mean(MagEtyLt12(SAL(r),d,1:TotalTrial/2));
        SALMagEtyLt12SpltAvg(r,d2) = mean(MagEtyLt12(SAL(r),d,TotalTrial/2+1:TotalTrial));
    end
end

for r = 1:numel(VPA)
    for d = 1:TLT
        d1 = 2*d-1;
        d2 = 2*d;
        VPAMagEtyLt12SpltAvg(r,d1) = mean(MagEtyLt12(VPA(r),d,1:TotalTrial/2));
        VPAMagEtyLt12SpltAvg(r,d2) = mean(MagEtyLt12(VPA(r),d,TotalTrial/2+1:TotalTrial));
    end
end

save([datafolder 'MagEtyLt.mat'],'VPAMagEtyLt6SpltAvg','SALMagEtyLt6SpltAvg', ...
    'VPAMagEtyLt12SpltAvg','SALMagEtyLt12SpltAvg','VPAMagEtyLt6Avg', ...
    'SALMagEtyLt6Avg','VPAMagEtyLt12Avg','SALMagEtyLt12Avg')


%% magazine entries between tones

% (RatID, [Before,During,After tone],Total # of trials)
MagEtyCtBetween = zeros(numel(RatID),TLT); % no minimum ITI
MagEtyCt250Between = zeros(numel(RatID),TLT); % 250ms minimum ITI
MagEtyCt500Between = zeros(numel(RatID),TLT); % 500ms minimum ITI
MagEtyCtRoundBetween = zeros(numel(RatID),TLT); % rounding method
MagEtyRateBetween = zeros(numel(RatID),TLT); % no minimum ITI
MagEtyRate250Between = zeros(numel(RatID),TLT); % 250ms minimum ITI
MagEtyRate500Between = zeros(numel(RatID),TLT); % 500ms minimum ITI
MagEtyRateRoundBetween = zeros(numel(RatID),TLT); % rounding method

DurBetween = zeros(numel(RatID),TLT);

for r = 1:numel(RatID)
    for d = 1:TLT
        E = MagEtyRaw(r).day{d,1};
        E250 = MagEty250(r).day{d,1};
        E500 = MagEty500(r).day{d,1};
        ERound = MagEtyRound(r).day{d,1};
        for t = 1:TotalTrial-1
            MagEtyCtBetween(r,d) = (MagEtyCtBetween(r,d) + sum(TSRaw(r).day{d}(t+1,1) >= E & E >= ...
                TSRaw(r).day{d}(t,1)+12));
            MagEtyCt250Between(r,d) = (MagEtyCt250Between(r,d) + sum(TSRaw(r).day{d}(t+1,1) >= E250 & E250 >= ...
                TSRaw(r).day{d}(t,1)+12));
            MagEtyCt500Between(r,d) = (MagEtyCt500Between(r,d) + sum(TSRaw(r).day{d}(t+1,1) >= E500 & E500 >= ...
                TSRaw(r).day{d}(t,1)+12));
            MagEtyCtRoundBetween(r,d) = (MagEtyCtRoundBetween(r,d) + sum(TSRaw(r).day{d}(t+1,1) >= ERound & ERound >= ...
                TSRaw(r).day{d}(t,1)+12));
        end
        DurBetween(r,d) = TSRaw(r).day{d}(end,1) - TSRaw(r).day{d}(1,1) - 12*(TotalTrial-1);
        MagEtyRateBetween = MagEtyCtBetween./DurBetween;
        MagEtyRate250Between = MagEtyCt250Between./DurBetween;
        MagEtyRate500Between = MagEtyCt500Between./DurBetween;
        MagEtyRateRoundBetween = MagEtyCtRoundBetween./DurBetween;
    end
end

% Find the index of each group
SAL = [];
VPA = [];

for n = 1:numel(RatID)
    if strcmpi(Groups{n}, 'Saline') == true
        SAL = [SAL n];
    end
end

for n = 1:numel(RatID)
    if strcmpi(Groups{n}, 'VPA') == true
        VPA = [VPA n];
    end
end

if numel(VPA) > numel(SAL)
    groupsize = numel(VPA);
elseif numel(VPA) < numel(SAL)
    groupsize = numel(SAL);
elseif    numel(VPA) == numel(SAL)
    groupsize = numel(SAL);
end

% group 1: SAL, 2:VPA
GroupMagEtyRateBetween = zeros(numel(treatment),groupsize,TLT); % no minimum ITI
GroupMagEtyRate250Between = zeros(numel(treatment),groupsize,TLT); % 250ms minimum ITI
GroupMagEtyRate500Between = zeros(numel(treatment),groupsize,TLT); % 500ms minimum ITI
GroupMagEtyRateRoundBetween = zeros(numel(treatment),groupsize,TLT); % rounding method

for n = 1:numel(SAL)
    for d = 1:TLT
        GroupMagEtyRateBetween(1,n,d) =  MagEtyRateBetween(SAL(n),d).*60; % transform to count/min
        GroupMagEtyRate250Between(1,n,d) =  MagEtyRate250Between(SAL(n),d).*60;
        GroupMagEtyRate500Between(1,n,d) =  MagEtyRate500Between(SAL(n),d).*60;
        GroupMagEtyRateRoundBetween(1,n,d) =  MagEtyRateRoundBetween(SAL(n),d).*60;
    end
end

for n = 1:numel(VPA)
    for d = 1:TLT
        GroupMagEtyRateBetween(2,n,d) =  MagEtyRateBetween(VPA(n),d).*60;
        GroupMagEtyRate250Between(2,n,d) =  MagEtyRate250Between(VPA(n),d).*60;
        GroupMagEtyRate500Between(2,n,d) =  MagEtyRate500Between(VPA(n),d).*60;
        GroupMagEtyRateRoundBetween(2,n,d) =  MagEtyRateRoundBetween(VPA(n),d).*60;
    end
end


%% look more closely to extinction
% calcute average latency, 2 blocks of 30 trials per session(day)
MagEtyCt250Ext10t = NaN(numel(RatID),12); % 250ms minimum ITI
MagEtyLt6Ext10t = NaN(numel(RatID),12);

for r = 1:numel(RatID)
    for d = 1:2
        for t = 1:6
            MagEtyCt250Ext10t(r,((d-1)*6+mod(t+5,6)+1)) = mean(MagEtyCt250{r,d+7,2}...
                (1+TotalTrial/6*mod(t+5,6):1+TotalTrial/6*mod(t+5,6)+9));
            MagEtyLt6Ext10t(r,((d-1)*6+mod(t+5,6)+1)) = mean(MagEtyLt6...
                (r,d+7,1+TotalTrial/6*mod(t+5,6):1+TotalTrial/6*mod(t+5,6)+9));
        end
    end
end

SALMagEtyDur250Ext10t = NaN(numel(SAL),2*6);
VPAMagEtyDur250Ext10t = NaN(numel(VPA),2*6);
SALMagEtyLt6Ext10t = NaN(numel(SAL),2*6); % 6 seconds maximum latency 2 sessions and every 10 trials
VPAMagEtyLt6Ext10t = NaN(numel(VPA),2*6); % 6 seconds maximum latency

for n = 1:numel(SAL)
    for d = 1:12
        SALMagEtyDur250Ext10t(n,d) =  MagEtyCt250Ext10t(SAL(n),d);
        SALMagEtyLt6Ext10t(n,d) = MagEtyLt6Ext10t(SAL(n),d);
    end
end

for n = 1:numel(VPA)
    for d = 1:12
        VPAMagEtyDur250Ext10t(n,d) =  MagEtyCt250Ext10t(VPA(n),d);
        VPAMagEtyLt6Ext10t(n,d) = MagEtyLt6Ext10t(VPA(n),d);    
    end
end

SALBER250Ext10t = NaN(numel(SAL),2*6);
VPABER250Ext10t = NaN(numel(VPA),2*6);

for r = 1:numel(SAL)
    for d = 1:2
        for t = 1:6
            SALBER250Ext10t(r,((d-1)*6+mod(t+5,6)+1)) = mean( ...
                SALBER250(r,d+7,1+TotalTrial/6*mod(t+5,6):1+TotalTrial/6*mod(t+5,6)+9));
        end
    end
end

for r = 1:numel(VPA)
    for d = 1:2
        for t = 1:6
            VPABER250Ext10t(r,((d-1)*6+mod(t+5,6)+1)) = mean( ...
                VPABER250(r,d+7,1+TotalTrial/6*mod(t+5,6):1+TotalTrial/6*mod(t+5,6)+9));
        end
    end
end

%% look more closely to acquisition
% calcute average latency, 2 blocks of 30 trials per session(day)
MagEtyCt250Acq10t = NaN(numel(RatID),24); % 250ms minimum ITI
MagEtyLt6Acq10t = NaN(numel(RatID),24);

for r = 1:numel(RatID)
    for d = 1:4
        for t = 1:6
            MagEtyCt250Acq10t(r,((d-1)*6+mod(t+5,6)+1)) = mean(MagEtyCt250{r,d,2}...
                (1+TotalTrial/6*mod(t+5,6):1+TotalTrial/6*mod(t+5,6)+9));
            MagEtyLt6Acq10t(r,((d-1)*6+mod(t+5,6)+1)) = mean(MagEtyLt6...
                (r,d,1+TotalTrial/6*mod(t+5,6):1+TotalTrial/6*mod(t+5,6)+9));
        end
    end
end

SALMagEtyDur250Acq10t = NaN(numel(SAL),4*6);
VPAMagEtyDur250Acq10t = NaN(numel(VPA),4*6);
SALMagEtyLt6Acq10t = NaN(numel(SAL),4*6); % 6 seconds maximum latency 2 sessions and every 10 trials
VPAMagEtyLt6Acq10t = NaN(numel(VPA),4*6); % 6 seconds maximum latency

for n = 1:numel(SAL)
    for d = 1:24
        SALMagEtyDur250Acq10t(n,d) =  MagEtyCt250Acq10t(SAL(n),d);
        SALMagEtyLt6Acq10t(n,d) = MagEtyLt6Acq10t(SAL(n),d);
    end
end

for n = 1:numel(VPA)
    for d = 1:24
        VPAMagEtyDur250Acq10t(n,d) =  MagEtyCt250Acq10t(VPA(n),d);
        VPAMagEtyLt6Acq10t(n,d) = MagEtyLt6Acq10t(VPA(n),d);    
    end
end

SALBER250Acq10t = NaN(numel(SAL),4*6);
VPABER250Acq10t = NaN(numel(VPA),4*6);

for r = 1:numel(SAL)
    for d = 1:4
        for t = 1:6
            SALBER250Acq10t(r,((d-1)*6+mod(t+5,6)+1)) = mean( ...
                SALBER250(r,d,1+TotalTrial/6*mod(t+5,6):1+TotalTrial/6*mod(t+5,6)+9));
        end
    end
end

for r = 1:numel(VPA)
    for d = 1:4
        for t = 1:6
            VPABER250Acq10t(r,((d-1)*6+mod(t+5,6)+1)) = mean( ...
                VPABER250(r,d,1+TotalTrial/6*mod(t+5,6):1+TotalTrial/6*mod(t+5,6)+9));
        end
    end
end
%% Reorganize data and plot
% Reorganize data by data type
% rm = fitrm(t,'t0-t8 ~ Gender','WithinDesign',Time,'WithinModel','orthogonalcontrasts')
%% Statistic tests