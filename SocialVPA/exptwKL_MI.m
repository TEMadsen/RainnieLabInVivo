%% explore CFC in Social Interaction data

data1 = data.trial{94}(2,:);
data2 = data.trial{94}(12,:);

%% this takes too long

% movingwin = [30 3];
% video = 'y';
% 
% [z3d,flow,fhigh,times,pval3d,MI3d,pref3d] = ...
%     sig_crossKL_MI2dvTime_TEM(data1,data2,movingwin);

%% try without time dim or across regions

foi_phase   = 1.5:14.5;
w_phase     = 1;
foi_amp     = 20:10:180;
w_amp       = 10;
plt         = 'y';

[zscore2d,pval2d,h2d,MI2d,phasepref2d] = sig_KL_MI2d_TEM(data1,foi_phase,w_phase,foi_amp,w_amp,plt);

%% follow one hot spot over time

movingwin = [30 3];
Pf = [11 13];
Af = [90 110];

[MI,t,Z,P,H,PP]=KL_MIvTime_TEM(data1,movingwin,Pf,Af,plt);