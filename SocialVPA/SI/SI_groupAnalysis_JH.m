%% Calculate baseline spectrograms and coherograms for each rat

close all; clearvars; clc;  % clean slate

set(0,'DefaultFigureWindowStyle','docked')

ft_defaults                 % set FieldTrip defaults
info_VPA_SI                 % load all background info (filenames, etc)

load([datafolder 'NeuralPower04302018.mat'])
load([datafolder 'TFanalysisFoIToI.mat'])
target = {'RatSide', 'EmptySide'};
measurement = {'NAc Power','BLA Power','Coherence'};

thetaIdx = find(cfg.freq >= 5 & cfg.freq <= 8);
gammaIdx = find(cfg.freq >= 50 & cfg.freq <= 100);

%% group analysis
groupSize = [numel(SALgroup); numel(VPAgroup)];

SALCoh = NaN(groupSize(1), numel(sessions), numel(phases), numel(target), numel(cfg.freq));
SALBLAPow = NaN(groupSize(1), numel(sessions), numel(phases), numel(target), numel(cfg.freq));
SALNAcPow = NaN(groupSize(1), numel(sessions), numel(phases), numel(target), numel(cfg.freq));

VPACoh = NaN(groupSize(2), numel(sessions), numel(phases), numel(target), numel(cfg.freq));
VPABLAPow = NaN(groupSize(2), numel(sessions), numel(phases), numel(target), numel(cfg.freq));
VPANAcPow = NaN(groupSize(2), numel(sessions), numel(phases), numel(target), numel(cfg.freq));


%% reorganize data by group

SALcount = 1;
VPAcount = 1;
dim = size(SumTarget);

% SumTargetLog = log10(SumTarget).*10;
% plot(squeeze(nanmean(SumTargetLog(:,1,1,1,1,:),1)))

for ii = 1:size(ratT,1)
    if SALInd(ii) == 1
        
        SALCoh(SALcount,:,:,:,:) = reshape(SumTarget(ii,:,:,3,:,:), 1, dim(2), dim(3) ,dim(5),dim(6));
        SALNAcPow(SALcount,:,:,:,:) = reshape(SumTarget(ii,:,:,1,:,:), 1, dim(2), dim(3) ,dim(5),dim(6));
        SALBLAPow(SALcount,:,:,:,:) = reshape(SumTarget(ii,:,:,2,:,:), 1, dim(2), dim(3) ,dim(5),dim(6));
        SALcount = SALcount +1;
        
    elseif VPAInd(ii) ==1
        
        VPACoh(VPAcount,:,:,:,:) = reshape(SumTarget(ii,:,:,3,:,:), 1, dim(2), dim(3) ,dim(5),dim(6));
        VPANAcPow(VPAcount,:,:,:,:) = reshape(SumTarget(ii,:,:,1,:,:), 1, dim(2), dim(3) ,dim(5),dim(6));
        VPABLAPow(VPAcount,:,:,:,:) = reshape(SumTarget(ii,:,:,2,:,:), 1, dim(2), dim(3) ,dim(5),dim(6));
        VPAcount = VPAcount +1;
    else
        error(["Rat " int2str(ii) " has error on treatment labeling"]);
    end
end
SALCoh(3,:,:,:,:) = [];
SALNAcPow(3,:,:,:,:) = [];
SALBLAPow(3,:,:,:,:) = [];
%% plot averaged group results
SALNAcPowAvg = squeeze(nanmean(SALNAcPow,1));
SALBLAPowAvg = squeeze(nanmean(SALBLAPow,1));
SALCohAvg = squeeze(nanmean(SALCoh,1));

VPANAcPowAvg = squeeze(nanmean(VPANAcPow,1));
VPABLAPowAvg = squeeze(nanmean(VPABLAPow,1));
VPACohAvg = squeeze(nanmean(VPACoh,1));
for tt = 1:numel(treatment)
    for mm = 1:3
        if tt == 1 && mm == 1
            data2plot = SALNAcPowAvg;
        elseif tt == 1 && mm == 2
            data2plot = SALBLAPowAvg;
        elseif tt == 1 && mm == 3
            data2plot = SALCohAvg;  
        elseif tt == 2 && mm == 1
            data2plot = VPANAcPowAvg;
        elseif tt == 2 && mm == 2
            data2plot = VPABLAPowAvg;
        elseif tt == 2 && mm == 3
            data2plot = VPACohAvg;
        end
        for session = 1:numel(sessions)
            figure(session)
            clf
            hold on
            label = cell(6,1);
            for pp = 1:numel(phases)
                for tgt = 1:numel(target)
                    p1 = plot(squeeze(data2plot(session,pp,tgt,:)),'XData',cfg.freq);
                    label{((pp-1)*2+tgt)} = ['Phase ' int2str(pp) ', ' target{tgt}];
                    set(p1,'LineWidth',2)
                end
            end
            hold off
            legend(label)
            xlabel('Frequency (Hz)','FontWeight','bold')
            if mm < 3
               ylabel('dB','FontWeight','bold')
            else
               ylabel('Coherence','FontWeight','bold')
               ylim([0.5 1])
            end
            title(['Average ' measurement{mm} ' of ' treatment{tt} ...
                ' group, Day ' int2str(session)])
            set(gca, 'FontName', 'Arial', 'FontSize', 16,'FontWeight','bold');
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            print([figurefolder 'Avg ' measurement{mm} '_' treatment{tt} ...
                '_Day' int2str(session)],'-dpng');
        end
    end
end
dimord = 'day_phase_target';
SALCohTheta = squeeze(nanmean(SALCoh(:,:,:,:,thetaIdx),5));
SALBLAPowTheta = squeeze(nanmean(SALBLAPow(:,:,:,:,thetaIdx),5));
SALNAcPowTheta = squeeze(nanmean(SALNAcPow(:,:,:,:,thetaIdx),5));

VPACohTheta = squeeze(nanmean(VPACoh(:,:,:,:,thetaIdx),5));
VPABLAPowTheta = squeeze(nanmean(VPABLAPow(:,:,:,:,thetaIdx),5));
VPANAcPowTheta = squeeze(nanmean(VPANAcPow(:,:,:,:,thetaIdx),5));

SALCohGamma = squeeze(nanmean(SALCoh(:,:,:,:,gammaIdx),5));
SALBLAPowGamma = squeeze(nanmean(SALBLAPow(:,:,:,:,gammaIdx),5));
SALNAcPowGamma = squeeze(nanmean(SALNAcPow(:,:,:,:,gammaIdx),5));

VPACohGamma = squeeze(nanmean(VPACoh(:,:,:,:,gammaIdx),5));
VPABLAPowGamma = squeeze(nanmean(VPABLAPow(:,:,:,:,gammaIdx),5));
VPANAcPowGamma = squeeze(nanmean(VPANAcPow(:,:,:,:,gammaIdx),5));

save([datafolder 'SIgroupThetaTarget.mat'], 'SALCohTheta' ,'SALBLAPowTheta' ,'SALNAcPowTheta' ...
    ,'VPACohTheta','VPABLAPowTheta','VPANAcPowTheta', 'dimord')
save([datafolder 'SIgroupGammaTarget.mat'], 'SALCohGamma' ,'SALBLAPowGamma' ,'SALNAcPowGamma' ...
    ,'VPACohGamma','VPABLAPowGamma','VPANAcPowGamma', 'dimord')

%% Theta

SALD1CohThetaRat = squeeze(SALCohTheta(:,1,:,1));
SALD1CohThetaEmpty = squeeze(SALCohTheta(:,1,:,2));
SALD2CohThetaRat = squeeze(SALCohTheta(:,2,:,1));
SALD2CohThetaEmpty = squeeze(SALCohTheta(:,2,:,2));

SALD1BLAPowThetaRat = squeeze(SALBLAPowTheta(:,1,:,1));
SALD1BLAPowThetaEmpty = squeeze(SALBLAPowTheta(:,1,:,2));
SALD2BLAPowThetaRat = squeeze(SALBLAPowTheta(:,2,:,1));
SALD2BLAPowThetaEmpty = squeeze(SALBLAPowTheta(:,2,:,2));

SALD1NAcPowThetaRat = squeeze(SALNAcPowTheta(:,1,:,1));
SALD1NAcPowThetaEmpty = squeeze(SALNAcPowTheta(:,1,:,2));
SALD2NAcPowThetaRat = squeeze(SALNAcPowTheta(:,2,:,1));
SALD2NAcPowThetaEmpty = squeeze(SALNAcPowTheta(:,2,:,2));

VPAD1CohThetaRat = squeeze(VPACohTheta(:,1,:,1));
VPAD1CohThetaEmpty = squeeze(VPACohTheta(:,1,:,2));
VPAD2CohThetaRat = squeeze(VPACohTheta(:,2,:,1));
VPAD2CohThetaEmpty = squeeze(VPACohTheta(:,2,:,2));

VPAD1BLAPowThetaRat = squeeze(VPABLAPowTheta(:,1,:,1));
VPAD1BLAPowThetaEmpty = squeeze(VPABLAPowTheta(:,1,:,2));
VPAD2BLAPowThetaRat = squeeze(VPABLAPowTheta(:,2,:,1));
VPAD2BLAPowThetaEmpty = squeeze(VPABLAPowTheta(:,2,:,2));

VPAD1NAcPowThetaRat = squeeze(VPANAcPowTheta(:,1,:,1));
VPAD1NAcPowThetaEmpty = squeeze(VPANAcPowTheta(:,1,:,2));
VPAD2NAcPowThetaRat = squeeze(VPANAcPowTheta(:,2,:,1));
VPAD2NAcPowThetaEmpty = squeeze(VPANAcPowTheta(:,2,:,2));

% save([datafolder 'SIgroupThetaTarget_FLAT.mat'], 'SALCohTheta' ,'SALBLAPowTheta' ,'SALNAcPowTheta' ...
%     ,'VPACohTheta','VPABLAPowTheta','VPANAcPowTheta', 'dimord')
% 

%% Gamma

GammaSALD1CohRat = squeeze(SALCohGamma(:,1,:,1));
GammaSALD1CohEmpty = squeeze(SALCohGamma(:,1,:,2));
GammaSALD2CohRat = squeeze(SALCohGamma(:,2,:,1));
GammaSALD2CohEmpty = squeeze(SALCohGamma(:,2,:,2));

GammaSALD1BLAPowRat = squeeze(SALBLAPowGamma(:,1,:,1));
GammaSALD1BLAPowEmpty = squeeze(SALBLAPowGamma(:,1,:,2));
GammaSALD2BLAPowRat = squeeze(SALBLAPowGamma(:,2,:,1));
GammaSALD2BLAPowEmpty = squeeze(SALBLAPowGamma(:,2,:,2));

GammaSALD1NAcPowRat = squeeze(SALNAcPowGamma(:,1,:,1));
GammaSALD1NAcPowEmpty = squeeze(SALNAcPowGamma(:,1,:,2));
GammaSALD2NAcPowRat = squeeze(SALNAcPowGamma(:,2,:,1));
GammaSALD2NAcPowEmpty = squeeze(SALNAcPowGamma(:,2,:,2));

GammaVPAD1CohRat = squeeze(VPACohGamma(:,1,:,1));
GammaVPAD1CohEmpty = squeeze(VPACohGamma(:,1,:,2));
GammaVPAD2CohRat = squeeze(VPACohGamma(:,2,:,1));
GammaVPAD2CohEmpty = squeeze(VPACohGamma(:,2,:,2));

GammaVPAD1BLAPowRat = squeeze(VPABLAPowGamma(:,1,:,1));
GammaVPAD1BLAPowEmpty = squeeze(VPABLAPowGamma(:,1,:,2));
GammaVPAD2BLAPowRat = squeeze(VPABLAPowGamma(:,2,:,1));
GammaVPAD2BLAPowEmpty = squeeze(VPABLAPowGamma(:,2,:,2));

GammaVPAD1NAcPowRat = squeeze(VPANAcPowGamma(:,1,:,1));
GammaVPAD1NAcPowEmpty = squeeze(VPANAcPowGamma(:,1,:,2));
GammaVPAD2NAcPowRat = squeeze(VPANAcPowGamma(:,2,:,1));
GammaVPAD2NAcPowEmpty = squeeze(VPANAcPowGamma(:,2,:,2));
% 
% save([datafolder 'SIgroupGammaTarget_FLAT.mat'], '' ,'' ,'' ...
%     ,'','','',...
%     ,'','','',...
%     ,'','','',...
%     ,'','','',...
%     ,'','','',...
%     ,'','','',...
%     ,'','','')

