%% calculates 2d cross-frequency phase-amplitude modulation
% for each interval type of multiple social interaction tests
% across multiple rats

close all force; clearvars; clc;  % clean slate

info_VPA_SI                       % load all background info (filenames, etc)
load([datafolder filesep 'SI_ts.mat']);

set(0,'DefaultFigureWindowStyle','docked')

%% set analysis parameters

foi_phase = 2:1:18;   % central frequencies for phase modulation
w_phase = 1;              % width of phase band (foi +/- w)
foi_amp = 30:10:150;         % central frequencies for modulated amplitude
w_amp = 10;                  % width of amplitude band (foi +/- w)

ratNum  = [361; 362; 364; 377; 379; 380];
ratGroup = {'VPA','SAL','SAL','VPA','VPA','SAL'};
ratIdx = [1;2;4;5;7;8];
groupnum = [1;1;2;2;3;3];

%% preallocation
SALCFC = NaN(3,numel(sessions),numel(phases),2,2,numel(foi_phase),numel(foi_amp));
VPACFC = NaN(3,numel(sessions),numel(phases),2,2,numel(foi_phase),numel(foi_amp));

for iR = 1:numel(ratNum)
    inputfile = [datafolderDefault 'SIcrossKL_MI2dz_'...
        num2str(ratNum(iR)) '_4s_noSig.mat'];
    load(inputfile)
    gr = ratGroup{iR};
    for iDay = 1:numel(sessions)
        for iPhase = 1:numel(phases)
            if strcmp(gr, 'VPA') ==1
                grID = groupnum(iR);
                for pr = 1:2
                    for ar = 1:2
                        VPACFC(grID,iDay,iPhase,pr,ar,:,:) = ...
                            squeeze(mean(MI2d{iDay,iPhase}{pr,ar},3));
                    end
                end
                
            elseif strcmp(gr, 'SAL') ==1
                grID = groupnum(iR);
                for pr = 1:2
                    for ar = 1:2
                        SALCFC(grID,iDay,iPhase,pr,ar,:,:) = ...
                            squeeze(mean(MI2d{iDay,iPhase}{pr,ar},3));
                    end
                end
            end
        end
    end
end

SALgroupCFCsubNorm = squeeze(mean(SALCFC,1));
VPAgroupCFCsubNorm = squeeze(mean(VPACFC,1));

for group = 1:numel(treatment)
    if group == 1
        data2plot = SALgroupCFCsubNorm;
        grp = 'SAL';
    elseif group == 2
        data2plot = VPAgroupCFCsubNorm;
                grp = 'VPA';
    end
    for iDay = 1:numel(phases)
        for pr = 1:2
            if pr == 1
                prlabel = 'NAc';
            else
                prlabel = 'BLA';
            end
            for ar = 1:2
                if ar == 1
                    arlabel = 'NAc';
                else
                    arlabel = 'BLA';
                end
                clf
                obj2plot = squeeze(data2plot(iDay,pr,ar,:,:));
                imagesc(obj2plot(foiPhase2Plot,:)','XData',foi_phase(foiPhase2Plot),'YData',foi_amp)
                axis xy
                colormap jet
                colorbar
                                    caxis([0 2*10^(-3)]);

                set(gca, 'FontName', 'Arial', 'FontSize', 16);
                xlabel('Phase Frequency (Hz)');
                ylabel('Envelope Frequency (Hz)');
                
                title({[grp ' D' int2str(iDay)];[ ...
                     prlabel ' phase modulating ' ...
                    arlabel ' amplitude']});
                saveas(gcf,[figurefolder 'KLMI' grp '_D' int2str(iDay) ...
                     prlabel 'mod ' arlabel 'amp.png'])
            end
        end
    end
end