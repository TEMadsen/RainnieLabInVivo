%% Calculate baseline spectrograms and coherograms for each rat

close all; clearvars; clc;  % clean slate

set(0,'DefaultFigureWindowStyle','docked')

ft_defaults                 % set FieldTrip defaults
info_VPA_SI                 % load all background info (filenames, etc)

foi       = 1.5:0.25:150;   % freqs from 1-103 Hz progressing up by 5% each
tapsmofrq = foi/10;         % smooth each band by +/- 5%

for f = find(tapsmofrq < 0.5)  % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;          % keeps time windows under 8 seconds
end

t_ftimwin = 2./tapsmofrq;   % for 3 tapers (K=3), T=2/W

% for smoothed visualization & less memory use
tshift = min(t_ftimwin)/2;
SumToI = 0:min(t_ftimwin)/2:900;    % time windows overlap by at least 50%


%% converting video frame numeber to seconds with 0 at the begining of the trial
load([datafolder 'SItimeStamp04282018.mat']);

% preallocating space
TSRatEmptyNeural = cell(size(SItimeStamp.TSRatEmpty));
TSRatEmptyNeural(1,:,:,:) = [];

SItimeStamp.TSRatEmpty(10,:,:,:) = []; % no neural recording for rat 392.

for r = 1:size(SItimeStamp.TSRatEmpty,1)
    for s = 1:size(SItimeStamp.TSRatEmpty,2)
        for p = 1:size(SItimeStamp.TSRatEmpty,3)
            if r == 6
            elseif r == 19 && s == 2
            else
                TSRatEmptyNeural{r,s,p,1} = SItimeStamp.TSRatEmpty{r,s,p,1}-(SI_ts{r,s}(p)/1000+0.0314);
                TSRatEmptyNeural{r,s,p,2} = SItimeStamp.TSRatEmpty{r,s,p,2}-(SI_ts{r,s}(p)/1000+0.0314);
            end
        end
    end
end

save([datafolder 'SItimeStamp_Neural04292018.mat'], 'TSRatEmptyNeural', 'SItimeStamp') 
