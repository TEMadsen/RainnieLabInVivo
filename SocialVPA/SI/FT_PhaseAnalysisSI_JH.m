%% Calculate baseline spectrograms and coherograms for each rat

close all; clearvars; clc;  % clean slate

ft_defaults                 % set FieldTrip defaults
info_VPA_SI                 % load all background info (filenames, etc)

foi       = 1.16.^(-1:33);   % freqs from 1-103 Hz progressing up by 5% each
tapsmofrq = foi/5;         % smooth each band by +/- 5%

for f = find(tapsmofrq < 0.5)  % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;          % keeps time windows under 8 seconds
end

t_ftimwin = 2./tapsmofrq;   % for 3 tapers (K=3), T=2/W

% for smoothed visualization & less memory use

toi = 0:min(t_ftimwin)/2:900;    % time windows overlap by at least 50%

% tshift = max(t_ftimwin)/2;  % shift time windows by tshift
% newtime = 0:tshift:900;  % average powspctrm within +/- tshift of each
powandcsd = cell(3,size(sessions,2),3); % save NAc power/BLA power/NAc-BLA CSD
plt       = false;           % whether or not to plot spec/coherograms

%% use try-catch when running unsupervised batches
for r = 7:8
% for r = 1:size(ratT,1)
    for s = 1:size(sessions,2)
        for p = 1:3 %numel of phases per day
            outputfile = [datafolder filesep 'Spectrogram_' ...
                int2str(ratT.ratnums(r)) 'D' int2str(s) '_P' int2str(p) '.mat'];
            %% gather baseline data
            % Clean LFP data, broken into arbitrary "trials" to remove
            % chunks of time containing artifacts (NaNs, low freq, theta
            % freq, and large), and filtered to remove high frequency
            % chewing artifacts
            inputfile = [datafolder filesep 'LFPTrialData_' ...
                int2str(ratT.ratnums(r)) 'Merged.mat'];
            
            if exist(inputfile,'file')
                %% load data
                
                load(inputfile);
                
                
                %% calculate baseline spectra & coherence
                t = find(data.trialinfo(:,2) == s)';
                tp = find(data.trialinfo(:,1) == p)';
                
                try
                    TrloI = ismember(t,tp);
                    trials = t(TrloI);
                    if isempty(trials)
                        warning(['No trials found for ' ...
                            num2str(ratT.ratnums(r)) '''s Day' int2str(s) '_P' int2str(p)]);
                    else
                        disp(['Calculating spectra & coherence for Rat # ' ...
                            int2str(ratT.ratnums(r)) 'Day' int2str(s) '_P' int2str(p)]);
                        
                        channel = cell(2,1);
                        channel{1} = ratT.chNAc{r};
                        channel{2} = ratT.chBLA{r};
                        
                        cfg             = [];
                        
                        
                        cfg.method      = 'mtmconvol';
                        cfg.output      = 'powandcsd';
                        cfg.keeptrials  = 'yes';
                        cfg.trials      = trials;
                        cfg.channel     = channel;
                        cfg.foi         = [1:0.25:150];
                        cfg.tapsmofrq   = tapsmofrq;
                        cfg.t_ftimwin   = t_ftimwin;
                        cfg.taper       = 'hanning';
                        cfg.toi         = newtime;
                        cfg.pad = 'nextpow2'; % assumes longest trial is 500s

                        %
                        %                         % time windows overlap by at least 50%
                        %                         cfg.toi         = '50%';
                        %                         % toi???? %
                        cfg.pad = 'nextpow2';
                        cfg.outputfile  = outputfile;
                        
                        freq = ft_freqanalysis(cfg,data);
                    end
                catch ME1
                    
                    warning(['Error while processing Rat # ' ...
                        int2str(ratT.ratnums(r)) ', ' 'Day' int2str(s) '_P' int2str(p) ...
                        '! Continuing with next in line.']);
                end
                powandcsd{1,s,p}(r,:) = freq.powspctrm(1,:); % NAc power
                powandcsd{2,s,p}(r,:) = freq.powspctrm(2,:); % BLA power
                powandcsd{3,s,p}(r,:) = abs(freq.crsspctrm(1,:)); % BLA power
%                 for i = 1:size(freq.labelcmb,1) %#ok<UNRCH> set in first cell
%                     % find 1st ch index
%                     indx1 = strcmp(freq.labelcmb(i,1),freq.label);
%                     
%                     % find 2nd ch index
%                     indx2 = strcmp(freq.labelcmb(i,2),freq.label);
%                     
%                     freq.coherence(i,:,:) = abs(freq.crsspctrm(i,:,:)./ ...
%                         sqrt(freq.powspctrm(indx1,:,:).* ...
%                         freq.powspctrm(indx2,:,:)));
%                 end
%                 

                %% plot, if desired
                
                if plt
                    %% calculate coherence
                    
                    for i = 1:size(freq.labelcmb,1) %#ok<UNRCH> set in first cell
                        % find 1st ch index
                        indx1 = strcmp(freq.labelcmb(i,1),freq.label);
                        
                        % find 2nd ch index
                        indx2 = strcmp(freq.labelcmb(i,2),freq.label);
                        
                        freq.coherence(i,:,:) = abs(freq.crsspctrm(i,:,:)./ ...
                            sqrt(freq.powspctrm(indx1,:,:).* ...
                            freq.powspctrm(indx2,:,:)));
                    end
                    
                    [nch, nf, nt] = size(freq.powspctrm);
                    ncmb = size(freq.coherence,1);
                    
                    %% average max(t_ftimwin) chunks & convert to dB for easier visualization
                    
                    newpowspctrm = nan(nch, nf, numel(newtime));
                    newcoherence = nan(ncmb, nf, numel(newtime));
                    
                    for ch = 1:nch
                        for f = 1:nf
                            for t = 1:numel(newtime)
                                newpowspctrm(ch, f, t) = 10*log10(mean(freq.powspctrm(...
                                    ch, f, abs(freq.time - newtime(t)) < tshift), ...
                                    'omitnan'));
                                if ch <= ncmb
                                    newcoherence(ch, f, t) = mean(freq.coherence(...
                                        ch, f, abs(freq.time - newtime(t)) < tshift), ...
                                        'omitnan');
                                end
                            end
                        end
                    end
                    
                    %% put back into freq structure
                    
                    freq.time = newtime;
                    freq.powspctrm = newpowspctrm;
                    freq.coherence = newcoherence;
                    
                    %% display raw spectrograms & coherograms
                    
                    for target = 1:3
                        cfg                 = [];
                        
                        if target == 1
                            cfg.parameter       = 'powspctrm';
                            cfg.channel         = ratT.chNAc{r};
                        elseif target == 2
                            cfg.parameter       = 'powspctrm';
                            cfg.channel         = ratT.chBLA{r};
                        else
                            cfg.parameter       = 'coherence';
                            cfg.zlim            = 'zeromax';
                            cfg.channel         = ratT.chNAc{r};
                            cfg.refchannel      = ratT.chBLA{r};
                        end
                        
                        if ~strcmp('none',cfg.channel) && ...
                                (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
                            figure(target);  clf;
                            ft_singleplotTFR(cfg, freq);
                            colormap(jet);
                            xlabel('Time (seconds)');
                            ylabel('Frequency (Hz)');
                            
                            if target < 3
                                title(['Rat ' int2str(ratT.ratnums(r)) ' Baseline ' ...
                                    regions{target} ' Power']);
                                print([figurefolder int2str(ratT.ratnums(r)) ...
                                    'BaselineSpectrogram_' regions{target}],'-dpng');
                            else
                                title(['Rat ' int2str(ratT.ratnums(r)) ...
                                    ' Baseline Coherence']);
                                print([figurefolder int2str(ratT.ratnums(r)) ...
                                    'BaselineCoherogram'],'-dpng');
                            end
                        end
                    end
                end
            end
            
            %% clear variables
            
            clearvars data cfg freq;
            
            %   catch ME
            %     warning(['Error (' ME.message ') while processing Rat # ' ...
            %       int2str(ratT.ratnums(r)) ', ' phases{p} ...
            %       '! Continuing with next in line.']);
            %   end
        end
    end
end
                save(powandcsd,[datafolder filesep 'PowandCsd_SI.mat']);
                
NAcPow = squeeze(powandcsd(1,:,:));
BLAPow = squeeze(powandcsd(2,:,:));
NAcBLAcsd = squeeze(powandcsd(3,:,:));