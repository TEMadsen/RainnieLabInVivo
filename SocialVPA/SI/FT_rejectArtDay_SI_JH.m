%% Remove artifacts via NEW METHOD:
% 1)  remove channels with more noise than neural signal (extreme variance
%     between motion artifacts and/or account for disproportionate amount
%     of clipping)
% 2)  remove times containing large, jump, and/or clipping artifacts

clearvars; close all force;
clc

info_VPA_SI                   % load all background info (filenames, etc)

%% full loop

minampl = NaN(16,size(ratT,1));
maxdur = NaN(16,size(ratT,1));
totaldur = NaN(16,size(ratT,1));
z = NaN(16,size(ratT,1));
next = 1;   % figure #

for r = 1:size(ratT,1)
    for p = 1:numel(sessions)
        % Raw LFP data divided into long, non-overlapping tone-triggered
        % trials, merged across all recordings for each rat
        inputfile = [datafolder filesep 'SI_RawLFPTrialData_' ...
            int2str(ratT.ratnums(r)) phases{p} '1.mat'];
        
        %   % merged LFPs with dead channels & clipping artifacts removed
        %   outputfile{1} = [datafolder 'noClipLFPTrialData_' ...
        %     int2str(ratT.ratnums(r)) 'Merged.mat'];
        %
        % merged LFPs with dead channels, large & clipping artifacts removed
        outputfile = [datafolder filesep 'cleanLFPTrialData_' ...
            int2str(ratT.ratnums(r)) phases{p} 'Merged.mat'];
        
        if ~exist(outputfile,'file')
            %       if ~exist(outputfile{1},'file')
            %% check for input data
            
            if ~exist(inputfile,'file')
                warning(['Skipping rat #' int2str(ratT.ratnums(r)) 'phase'  phases{p} ' because ' ...
                    inputfile ' was not found. Run FT_preproc...new_TEM.m first.'])
                continue
            end
            
            %% load input data
            
            load(inputfile);
            
%             if ratT.finalch(r)
%                 disp('Using previously selected not-dead channels')
%             else
%                 %% reject dead channels based on extreme variance other than shocks:
%                 % 1/var much greater than most channels (cutoff between 2 & 4e-4 per rat)
%                 % or var frequently > var across channels during shock trials
%                 % or unstable across recordings
%                 
%                 cfg             = [];
%                 cfg.method      = 'summary';
%                 %   cfg.metric      = '1/var';
%                 %   cfg.trials      = alltrials(~shocktr);
%                 cfg.keeptrial   = 'yes';  % won't invalidate trials marked "bad"
%                 
%                 % d1dat = ft_rejectvisual(cfg,d1dat);
%                 data = ft_rejectvisual(cfg,data);
%                 
%                 % BUG IN rejectvisual_summary>redraw (line 279) - DON'T USE MIN
%                 % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=1474
%                 
%                 % BUG IN rejectvisual_summary>redraw (line 309) - looks fixed at line 304
%                 % (same would probably work at line 279)
%                 % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=3005
%                 
%                 notdeadchs{r} = data.label;  %#ok<*SAGROW>
%                 
%                 keyboard  % if satisfied, dbcont
%                 clearvars data1 d1dat diff* dur ampl ident trltime clip shocktime dw up labels
%             end
            
            %% select good channels in full dataset & start w/ empty artifact struct
            
            %     cfg           = [];
            %     cfg.channel   = notdeadchs{r};
            %
            %     data = ft_selectdata(cfg,data);
                                cfg             = [];
                    cfg.method      = 'summary';
                    cfg.keeptrial   = 'yes';  % won't invalidate trials marked "bad"
                    cfg.channel = [ratT.chNAc(r); ratT.chBLA(r)];
                    emptyChan = zeros(numel(cfg.channel),1);
                    for ii = 1:numel(cfg.channel)
                        if isempty(cfg.channel{ii})
                            emptyChan(ii) = 1;
                        end
                    end
                    cfg.channel(logical(emptyChan)) = [];
                    data = ft_selectdata(cfg,data);
            artifact = [];
            
%             %% take 1st derivative of signal
%             % input to ft_artifact_clip is 1st derivative, so it's actually
%             % thresholding the 2nd derivative, which is more consistent in clipping
%             % artifacts
%             
%             cfg = [];
%             cfg.absdiff = 'yes';
%             
%             d1dat = ft_preprocessing(cfg,data);
%             
%             %% define clipping artifacts
%             
%             cfg                                 = [];
%             
%             cfg.artfctdef.clip.channel          = 'AD*';
%             cfg.artfctdef.clip.pretim           = 0.1;
%             cfg.artfctdef.clip.psttim           = 0.1;
%             cfg.artfctdef.clip.timethreshold    = 0.05;   % s
%             cfg.artfctdef.clip.amplthreshold    = 3;      % uV
%             %   cfg.artfctdef.clip.blame            = true;
%             
%             [~, artifact.clip] = ft_artifact_clip(cfg,d1dat);
%             
%             % bug in ft_artifact_clip adds time after end of file, triggers bug in
%             % convert_event that breaks ft_databrowser as noted below
%             endsamp = sum(numel([d1dat.time{:}]));
%             if artifact.clip(end,end) > endsamp
%                 artifact.clip(end,end) = endsamp;
%             end
            
            %% identify large artifacts
            
            % set threshold at twice the height of the largest fear-related delta
            % oscillation
            cfg                               = [];
            cfg.artfctdef.zvalue.channel      = 'AD*';
            cfg.artfctdef.zvalue.cutoff       = 10;
            cfg.artfctdef.zvalue.trlpadding   = 0;
            cfg.artfctdef.zvalue.fltpadding   = 0;
            cfg.artfctdef.zvalue.artpadding   = 0.1;
            
            cfg.artfctdef.zvalue.cumulative   = 'no';  % This uses max z-value
            % across channels, rather than sum, which can smooth out single-channel
            % artifacts, and ones with variable timing on different channels.
            
            cfg.artfctdef.zvalue.rectify      = 'yes';
            cfg.artfctdef.zvalue.interactive  = 'no';
            %   cfg.artfctdef.zvalue.interactive  = 'no';
            
            [~, artifact.large] = ft_artifact_zvalue(cfg,data);
            
            %% identify sudden "jumps" in the data
            
            % set threshold at twice the height of the largest fear-related delta
            % oscillation
            cfg                                 = [];
            
            % channel selection, cutoff and padding
            cfg.artfctdef.zvalue.channel        = 'AD*';
            cfg.artfctdef.zvalue.cutoff         = 24;
            cfg.artfctdef.zvalue.trlpadding     = 0;
            cfg.artfctdef.zvalue.artpadding     = 0.1;
            cfg.artfctdef.zvalue.fltpadding     = 0;
            
            % algorithmic parameters
            cfg.artfctdef.zvalue.cumulative     = 'no';  % uses max z-value across
            % channels, rather than sum, which can smooth out single-channel artifacts
            
            cfg.artfctdef.zvalue.medianfilter   = 'yes';
            cfg.artfctdef.zvalue.medianfiltord  = 15;  % wide enough not to catch too many short blips
            cfg.artfctdef.zvalue.absdiff        = 'yes';
            
            % make the process interactive
            cfg.artfctdef.zvalue.interactive    = 'no';
            
            [~, artifact.jump] = ft_artifact_zvalue(cfg,data);
            
            %% mark any disconnection events as visual artifacts
            % If any clipping artifacts in this trial, zoom to Horizontal = 1 second.
            % Skip to next clipping artifact and page through it one Segment at a
            % time to verify that it's truly clipping.  See if it can be blamed on
            % one or two channels that should be invalidated instead.  When you reach
            % the end of the artifact, hit the next clipping artifact button again.
            % If there are no more in the trial, it won't respond (command window
            % reads "no later trialsegment with "clip" artifact found").  Zoom out to
            % full trial by setting Horizontal = total # of current trialsegments.
            % Skip to next trial with clipping artifacts and repeat.
            
%             cfg                               = [];
% %             cfg.artfctdef.clip.artifact       = artifact.clip;
%             cfg.artfctdef.large.artifact      = artifact.large;
%             cfg.artfctdef.jump.artifact       = artifact.jump;
%             
%             % bug in convert_event, lines 180 & 181 should be swapped - prevents
%             % plotting of all but the first large & jump artifacts
%             cfg = ft_databrowser(cfg,data);
%             
%             artifact.visual   = cfg.artfctdef.visual.artifact;
%             keyboard  % dbcont when satisfied
%             clearvars d1dat
            
            %% remove artifacts from data
            
            cfg                             = [];
%             cfg.artfctdef.clip.artifact     = artifact.clip;
%             cfg.artfctdef.visual.artifact 	= artifact.visual;
            cfg.artfctdef.large.artifact  = artifact.large;
            cfg.artfctdef.jump.artifact   = artifact.jump;
            cfg.artfctdef.reject          = 'partial';
            cfg.artfctdef.minaccepttim    = 0.25;
            cfg.outputfile                = outputfile;
            
            data = ft_rejectartifact(cfg,data);
            
          
            clearvars data
        end
    end
end