%% calculates power amplitude and coherence during social interaction
% This script will use the power and coherence value calcualted from
% FT_TFanalysis_JH using cfg.method = 'mtmconvol' and
% cfg.output = 'powandcsd'

close all force; clearvars; clc;  % clean slate

info_VPA_SI                       % load all background info (filenames, etc)
load([datafolder 'SItimeStamp.mat']);
foi = [2 4; 8 12;60 120];

%% preallocate space
ratSideTime = cell(numel(ratT(:,1)),numel(sessions),numel(phases));
emptySideTime = cell(numel(ratT(:,1)),numel(sessions),numel(phases));

ratPowNAc = cell(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));
emptyPowNAc = cell(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));

ratPowBLA = cell(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));
emptyPowBLA = cell(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));

ratMeanPowNAc = nan(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));
emptyMeanPowNAc = nan(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));

ratMeanPowBLA = nan(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));
emptyMeanPowBLA = nan(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));

ratCoh = cell(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));
emptyCoh = cell(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));

ratMeanCoh = nan(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));
emptyMeanCoh = nan(numel(ratT(:,1)),numel(sessions),numel(phases),size(foi,1));


timeStampErrorLog = zeros(2,numel(ratT(:,1)),numel(sessions),numel(phases));
%dim1(sides): [rat empty]
timeStampErrorIndex = cell(2,numel(ratT(:,1)),numel(sessions),numel(phases));


%% reorganize timestamps
for r = 1:numel(ratT(:,1))
    for s = 1:numel(sessions)
        side = char(SItimeStamp.intTarget{r,s});
        if r == 6 && s == 1
        else
            for p = 1:numel(phases)
                if side == 'R'
                    % convert from absolute sides to targets and unit from
                    % frame number to miliseconds
                    ratSideTime{r,s,p} = SItimeStamp.woPassive{r,s,p,1}{:,2:3}./30;
                    emptySideTime{r,s,p} = SItimeStamp.woPassive{r,s,p,2}{:,2:3}./30;
                    % deduct the starting time of each phase
                    ratSideTime{r,s,p} = ratSideTime{r,s,p}-SI_ts{r,s}(p)./1000;
                    emptySideTime{r,s,p} = emptySideTime{r,s,p}-SI_ts{r,s}(p)./1000;
                    
                    timeStampErrorIndex{1,r,s,p} = ratSideTime{r,s,p}>9e+02;
                    timeStampErrorIndex{2,r,s,p} = emptySideTime{r,s,p}>9e+02;
                    
                    if ratSideTime{r,s,p}(ratSideTime{r,s,p}>9e+02)~= 0
                        timeStampErrorLog(1,r,s,p) = 1;
                    end
                    
                    if emptySideTime{r,s,p}(emptySideTime{r,s,p}>9e+02)~= 0
                        timeStampErrorLog(2,r,s,p) = 1;
                    end
                elseif side == 'L'
                    % convert from absolute sides to targets and unit from
                    % frame number to miliseconds
                    ratSideTime{r,s,p} = SItimeStamp.woPassive{r,s,p,2}{:,2:3}./30;
                    emptySideTime{r,s,p} = SItimeStamp.woPassive{r,s,p,1}{:,2:3}./30;
                    % deduct the starting time of each phase
                    ratSideTime{r,s,p} = ratSideTime{r,s,p}-SI_ts{r,s}(p)./1000;
                    emptySideTime{r,s,p} = emptySideTime{r,s,p}-SI_ts{r,s}(p)./1000;
                    timeStampErrorIndex{1,r,s,p} = ratSideTime{r,s,p}>9e+02;
                    timeStampErrorIndex{2,r,s,p} = emptySideTime{r,s,p}>9e+02;
                    
                    if ratSideTime{r,s,p}(ratSideTime{r,s,p}>9e+02)~= 0
                        timeStampErrorLog(1,r,s,p) = 1;
                    end
                    
                    if emptySideTime{r,s,p}(emptySideTime{r,s,p}>9e+02)~= 0
                        timeStampErrorLog(2,r,s,p) = 1;
                    end
                end
            end
        end
    end
end

save([datafolder filesep 'siTargetTimeStamp.mat'], 'ratSideTime', 'emptySideTime','timeStampErrorIndex');

%% extract data


ratIntIdx = cell(numel(ratT(:,1)),numel(sessions),numel(phases));
emptyIntIdx = cell(numel(ratT(:,1)),numel(sessions),numel(phases));

foiIdx = cell(size(foi,1),1);


for r = 7:numel(ratT(:,1))
    for s = 1:numel(sessions)
        if r == 6 
        else
            for p = 1:numel(phases)
                inputfile = [datafolder filesep 'mtmconvol_SpecCoherograms_' ...
                    int2str(ratT.ratnums(r)) 'D' int2str(s) '_P' int2str(p) '.mat'];
                
                load(inputfile)
                disp(['Status: Rat#' int2str(ratT{r,1}) ' Session ' int2str(s) ' Phase ' int2str(p)])
                % extract time indices for interaction intervals
                for iInt = 1:size(ratSideTime{r,s,p},1)
                    tempIdx = find(freq.time(1,:)>=ratSideTime{r,s,p}(iInt,1) & ...
                        freq.time(1,:)<=ratSideTime{r,s,p}(iInt,2));
                    ratIntIdx{r,s,p} = [ratIntIdx{r,s,p} tempIdx];
                end
                for iInt = 1:size(emptySideTime{r,s,p},1)
                    tempIdx = find(freq.time(1,:)>=emptySideTime{r,s,p}(iInt,1) & ...
                        freq.time(1,:)<=emptySideTime{r,s,p}(iInt,2));
                    emptyIntIdx{r,s,p} = [emptyIntIdx{r,s,p} tempIdx];
                end
                
                % extract indices for frequency bands of interest
                
                for iFoi = 1:size(foi,1)
                    tempIdx = find(freq.freq(1,:)>=foi(iFoi,1) & ...
                        freq.freq(1,:)<=foi(iFoi,2));
                    foiIdx{iFoi,1} = [foiIdx{iFoi,1} tempIdx];
                end
                
                % perform decible conversion on power values
                pow = freq.powspctrm;
                for iTrl = 1:size(pow,1)
                    for iCh = 1:size(pow,2)
                        for iF = 1:size(pow,3)
                            for iT = 1:1:size(pow,4)
                                pow(iTrl,iCh, iF, iT) = 10*log10(pow(...
                                    iTrl,iCh, iF, iT));
                            end
                        end
                    end
                end
                pow = squeeze(nansum(pow,1)); % merge all subtrials
                pow(pow==0) = NaN; %replace 0 from nansum with NaN to prevent biased averaging
                
                % compute coherence value
                rawCoh = NaN(size(freq.crsspctrm,1),size(freq.crsspctrm,3),size(freq.crsspctrm,4));
                
                for iTrl = 1:size(freq.crsspctrm,1)
                    rawCoh(iTrl,:,:) = abs(freq.crsspctrm(iTrl,1,:,:)./ ...
                        sqrt(freq.powspctrm(iTrl,1,:,:).* ...
                        freq.powspctrm(iTrl,2,:,:)));
                end
                
                rawCoh = squeeze(nansum(rawCoh,1)); % merge all subtrials
                rawCoh(rawCoh==0) = NaN; %replace 0 from nansum with NaN to prevent biased averaging
                
                
                % extract power of each foi during interaction
                for iFoi = 1:size(foi,1)
                    
                    ratPowNAc{r,s,p,iFoi} = ...
                        nanmean(squeeze(pow(1,foiIdx{iFoi,1},ratIntIdx{r,s,p})));
                    emptyPowNAc{r,s,p,iFoi} = ...
                        nanmean(squeeze(pow(1,foiIdx{iFoi,1},emptyIntIdx{r,s,p})));
                    
                    ratPowBLA{r,s,p,iFoi} = ...
                        nanmean(squeeze(pow(2,foiIdx{iFoi,1},ratIntIdx{r,s,p})));
                    emptyPowBLA{r,s,p,iFoi} = ...
                        nanmean(squeeze(pow(2,foiIdx{iFoi,1},emptyIntIdx{r,s,p})));
                    
                    ratMeanPowNAc(r,s,p,iFoi) = ...
                        nanmean(ratPowNAc{r,s,p,iFoi});
                    emptyMeanPowNAc(r,s,p,iFoi) = ...
                        nanmean(emptyPowNAc{r,s,p,iFoi});
                    
                    ratMeanPowBLA(r,s,p,iFoi) = ...
                        nanmean(ratPowBLA{r,s,p,iFoi});
                    emptyMeanPowBLA(r,s,p,iFoi) = ...
                        nanmean(emptyPowBLA{r,s,p,iFoi});
                end
                
                % extract coherence of each foi during interaction
                for iFoi = 1:size(foi,1)
                    
                    ratCoh{r,s,p,iFoi} = ...
                        nanmean(squeeze(rawCoh(foiIdx{iFoi,1},ratIntIdx{r,s,p})));
                    emptyCoh{r,s,p,iFoi} = ...
                        nanmean(squeeze(rawCoh(foiIdx{iFoi,1},emptyIntIdx{r,s,p})));
                    
                    ratMeanCoh(r,s,p,iFoi) = ...
                        nan
                    mean(ratCoh{r,s,p,iFoi});
                    emptyMeanCoh(r,s,p,iFoi) = ...
                        nanmean(emptyCoh{r,s,p,iFoi});
                    
                end
            end
        end
    end
end

save([datafolder filesep 'siPowCoh.mat'], 'ratPowNAc', 'emptyPowNAc', ...
    'ratPowBLA', 'emptyPowBLA', 'ratMeanPowNAc', 'emptyMeanPowNAc', ...
    'ratMeanPowBLA', 'emptyMeanPowBLA', 'ratCoh', 'emptyCoh', ...
    'ratMeanCoh', 'emptyMeanCoh');