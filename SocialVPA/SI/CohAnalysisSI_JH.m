%% Calculate baseline spectrograms and coherograms for each rat

close all; clearvars; clc;  % clean slate

set(0,'DefaultFigureWindowStyle','docked')

ft_defaults                 % set FieldTrip defaults
info_VPA_SI                 % load all background info (filenames, etc)

sampFreq = 1000;            % Sampling frequency
octave9 = 109;              % total number of frequencies for octave = 9
octave11 = 133;             % total number of frequencies for octave = 11
plt       = true;           % whether or not to plot spec/coherograms

%% use try-catch when running unsupervised batches
for r = 1:size(ratT,1)
    if r == 6
    else
        inputfile = [datafolder filesep 'LFPTrialData_' ...
            int2str(ratT.ratnums(r)) 'Merged.mat'];
        channel = zeros(2,1);
        channel(1) = str2double(ratT.chNAc{r}(3:4));
        channel(2) = str2double(ratT.chBLA{r}(3:4));
        if exist(inputfile,'file')
            %% load data
            
            load(inputfile);
            
        end
        for s = 1:size(sessions,2)
            t = find(data.trialinfo(:,2) == s)';
            for p = 1:3 %numel of phases per day
                outputfile = [datafolder filesep 'wavelet_Coh_' ...
                    int2str(ratT.ratnums(r)) 'D' int2str(s) '_P' int2str(p) '.mat'];
                
                coh9Sum = NaN(octave9, (900*1000)+1);
                coh11Sum = NaN(octave11, (900*1000)+1);
                
                tp = find(data.trialinfo(:,1) == p)';
                
                try
                    TrloI = ismember(t,tp);
                    trials = t(TrloI);
                    
                    if isempty(trials)
                        warning(['No trials found for ' ...
                            num2str(ratT.ratnums(r)) '''s Day' int2str(s) '_P' int2str(p)]);
                    else
                        disp(['Calculating coherence for Rat # ' ...
                            int2str(ratT.ratnums(r)) 'Day' int2str(s) '_P' int2str(p)]);
                        
                        nn = 1;
                        mm = 0;
                        while data.time{1,trials(nn)}(1)<=0
                            nn = nn+1;
                        end
                        while data.time{1,trials(end)-mm}(end) > 900
                            mm = mm+1;
                        end
                        nn = nn -1;
                        mm = mm -1;
                        for tt = nn:numel(trials)-mm
                            if numel(data.time{1,trials(tt)}) <2000
                            else
                                [cohTemp9,~,f9,coi9] = wcoherence(data.trial{1,trials(tt)}...
                                    (channel(1),:),data.trial{1,trials(tt)}(channel(2),:),sampFreq,'NumOctaves',9);
%                                 [cohTemp11,~,f11,coi11] = wcoherence(data.trial{1,trials(tt)}...
%                                     (channel(1),:),data.trial{1,trials(tt)}(channel(2),:),sampFreq,'NumOctaves',11);
%                                 
                                for ii = 1:numel(coi9)
                                    cohTemp9(f9<coi9(ii),ii) = NaN;
                                end
%                                 for ii = 1:numel(coi11)
%                                     cohTemp11(f11<coi11(ii),ii) = NaN;
%                                 end
                                
                                if data.time{1,trials(tt)}(1) < 0
                                    idxstart = find(data.time{1,trials(tt)} == 0);
                                    time2sample = 1:1+data.time{1,trials(tt)}(end)*1000;
                                    coh9Sum(:,time2sample)  = cohTemp9(:,idxstart:end);
%                                     coh11Sum(:,time2sample) = cohTemp11(:,idxstart:end);
                                elseif data.time{1,trials(tt)}(end) > 900
                                    idxend = find(data.time{1,trials(tt)} == 900);
                                    time2sample = (1+data.time{1,trials(tt)}(1)*1000):idxend+data.time{1,trials(tt)}(1)*1000;
                                    coh9Sum(:,time2sample)  = cohTemp9(:,1:idxend);
%                                     coh11Sum(:,time2sample) = cohTemp11(:,1:idxend);
                                else
                                    time2sample = 1+data.time{1,trials(tt)}(1)*1000:1+data.time{1,trials(tt)}(end)*1000;
                                    coh9Sum(:,time2sample)  = cohTemp9;
%                                     coh11Sum(:,time2sample) = cohTemp11;
                                end
                                clear cohTemp9 %cohTemp11
                            end
                        end
                    end
                catch ME1
                    
                    warning(['Error while processing Rat # ' ...
                        int2str(ratT.ratnums(r)) ', ' 'Day' int2str(s) '_P' int2str(p) ...
                        '! Continuing with next in line.']);
                end
                figure(p)
                imagesc(coh9Sum(20:end,:),'YData', f9(20:end), 'XData', 0:0.001:900)
                axis xy
                ylabel('Frequency (Hz)')
                xlabel('seconds')
                title(['Rat ' int2str(ratT.ratnums(r)) 'D' int2str(s) ...
                    'P' int2str(p) ' Coherence'])
                colormap jet
                colorbar
                hcol = colorbar;
                hcol.Label.String = 'Magnitude-Squared Coherence';
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                print([figurefolder int2str(ratT.ratnums(r)) ...
                    'CoherenceOctave9_D' int2str(s) ...
                    '_P' int2str(p)],'-dpng');
                
                
                coh9SumMasked = coh9Sum;
                coh9SumMasked(coh9SumMasked<0.6) = 0;
                
                figure(p+3)
                imagesc(coh9SumMasked(20:end,:),'YData', f9(20:end), 'XData', 0:0.001:900)
                axis xy
                ylabel('Frequency (Hz)')
                xlabel('seconds')
                title(['Rat ' int2str(ratT.ratnums(r)) 'D' int2str(s) ...
                    'P' int2str(p) ' Coherence (masked)'])
                colormap jet
                colorbar
                hcol = colorbar;
                hcol.Label.String = 'Magnitude-Squared Coherence';
                set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
                print([figurefolder int2str(ratT.ratnums(r)) ...
                    'CoherenceOctave9Masked_D' int2str(s) ...
                    '_P' int2str(p)],'-dpng');
%                 figure(2)
%                 imagesc(coh11Sum(16:end,:),'YData', f11(16:end), 'XData', 0:0.001:900)
%                 axis xy
%                 ylabel('Frequency (Hz)')
%                 xlabel('seconds')
%                 title(['Rat ' int2str(ratT.ratnums(r)) 'D' int2str(s) ...
%                     'P' int2str(p) ' Coherence'])
%                 colormap jet
%                 colorbar
%                 hcol = colorbar;
%                 hcol.Label.String = 'Magnitude-Squared Coherence';
%                 set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
%                 print([figurefolder int2str(ratT.ratnums(r)) ...
%                     'CoherenceOctave11_D' int2str(s) ...
%                     '_P' int2str(p)],'-dpng');
                
                save(outputfile, 'coh9Sum');
                clear coh9Sum coh9SumMasked
            end
        end
    end
end