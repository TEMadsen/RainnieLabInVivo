%% Calculate baseline spectrograms and coherograms for each rat

close all; clearvars; clc;  % clean slate

set(0,'DefaultFigureWindowStyle','docked')

ft_defaults                 % set FieldTrip defaults
info_VPA_SI                 % load all background info (filenames, etc)

foi       = 1.5:0.25:150;   % freqs of interest
tapsmofrq = foi/10;         % smooth each band by +/- 10%

for f = find(tapsmofrq < 0.5)  % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;          % keeps time windows under 4 seconds
end

t_ftimwin = 2./tapsmofrq;   % for 3 tapers (K=3), T=2/W

% for smoothed visualization & less memory use
tshift = min(t_ftimwin)/2;
toi = 0:tshift:900;    % time windows overlap by at least 50%

plt       = false;           % whether or not to plot spec/coherograms

%% use try-catch when running unsupervised batches
for r = 1:size(ratT,1)
    if r == 6
    else
        
        powSum = zeros(numel(sessions), numel(phases), 2, numel(foi), numel(toi));
        % 2 channels, 1: NAc, 2: BLA, 3: Coherence
    end
    for s = 1:numel(sessions)
        inputfile = [datafolder 'cleanLFPTrialData_' ...
            int2str(ratT.ratnums(r)) int2str(s) 'Merged.mat'];
        if exist(inputfile,'file')
            %% load data
            
            load(inputfile);
            for p = 1:3 %numel of phases per day
                outputfile = [datafolder 'mtmconvol_powandcoh_Linspace' ...
                    int2str(ratT.ratnums(r)) 'D' int2str(s) '_P' int2str(p) '.mat'];
                if exist(outputfile,'file')
                    disp(['Outputfile for ' int2str(ratT.ratnums(r)) 'D' ...
                        int2str(s) '_P' int2str(p) ' already exists. Skip!'])
                else
                    %% calculate baseline spectra & coherence
                    
                    try
                        trials = find(data.trialinfo(:,1) == p)';
                        for tr = 1:numel(trials)
                          if data.time{trials(tr)}(1) > toi(end) || ...
                              data.time{trials(tr)}(end) < toi(1)
                            trials(tr) = 0;
                          end
                        end
                        trials = trials(trials > 0);
                        if isempty(trials)
                            warning(['No trials found for ' ...
                                num2str(ratT.ratnums(r)) '''s Day' int2str(s) '_P' int2str(p)]);
                        else
                            disp(['Calculating spectra & coherence for Rat # ' ...
                                int2str(ratT.ratnums(r)) 'Day' int2str(s) '_P' int2str(p)]);
                            
                            channel = cell(2,1);
                            channel{1} = ratT.chNAc{r};
                            channel{2} = ratT.chBLA{r};
                            
                            cfg             = get_cfg_TEM('mtmconvol','social')
                            
%                             cfg.method      = 'wavelet';
                            cfg.output      = 'powandcsd';
                            cfg.keeptrials  = 'no';   % since these are actually subtrials that don't overlap in time, this just means squash all the time into one spectrogram
                            cfg.trials      = trials;
                            %                     cfg.taper       = 'hanning';
                            cfg.channel     = channel;
%                             cfg.foi         = foi;
%                             cfg.tapsmofrq   = tapsmofrq;
%                             cfg.t_ftimwin   = t_ftimwin;
%                             cfg.toi         = toi;
%                             cfg.pad = 2^nextpow2( (toi(end)-toi(1))*data.fsample ) ...
%                               / data.fsample; % might be more consistent than 'nextpow2' in case of many small chunks
%                             cfg.outputfile  = outputfile;
                            
                            freq = ft_freqanalysis(cfg,data);
                        end
                    catch ME1
                        
                        warning(['Error while processing Rat # ' ...
                            int2str(ratT.ratnums(r)) ', ' 'Day' int2str(s) '_P' int2str(p) ...
                            '! Continuing with next in line.']);
                    end
%                     figure(2);
%                     ft_multiplotTFR(struct('parameter','powspctrm', 'layout','horizontal'),freq)
%                     
%                     %             %% calculate coherence
%                     
%                     cfg            = [];
%                     cfg.method     = 'coh';
%                     cfg.channelcmb = freq.labelcmb;
%                     fd             = ft_connectivityanalysis(cfg, freq);
%                     
%                     figure(4);
%                     ft_singleplotTFR(struct('parameter','cohspctrm', 'channel',channel{1}, 'refchannel',channel{2}),fd)
%                     
                    freq.cohspctrm = abs(freq.crsspctrm) ./ ...
                      sqrt(freq.powspctrm(1,:,:) .* freq.powspctrm(2,:,:));
                    
%                     temp = squeeze(freq.cohspctrm); % check around col 3715
%                     figure; imagesc(temp,'YData',freq.freq);
                    save(outputfile,'freq')
                    %
                    %             for i = 1:size(freqTest.crsspctrm,1)
                    %                 freqTest.coherence1(i,:,:) = abs(freqTest.crsspctrm(i,1,:,:)./ ...
                    %                     sqrt(freqTest.powspctrm(i,1,:,:).* ...
                    %                     freqTest.powspctrm(i,2,:,:)));
                    %             end
                    %
                    %             for i = 1:size(freqTest.crsspctrm,1)
                    %                 freqTest.coherence(i,:,:) = (abs(squeeze(freqTest.crsspctrm(i,1,:,:))).^2)./ ...
                    %                     ((abs(squeeze(freqTest.powspctrm(i,1,:,:)))).^2).* ...
                    %                     ((abs(squeeze(freqTest.powspctrm(i,2,:,:)))).^2);
                    %             end
                    %
                    %             crsspctrmTest = squeeze(freqTestMtmconvol.crsspctrm(28,1,:,:));
                    %             crsspctrmTestReal = real(crsspctrmTest);
                    %             crsspctrmTestImag = imag(crsspctrmTest);
                    %             powspctrm1Test = squeeze(freqTestMtmconvol.powspctrm(28,1,:,:));
                    %             powspctrm2Test = squeeze(freqTestMtmconvol.powspctrm(28,2,:,:));
                    %
                    %             crsspctrmTestangle = angle(crsspctrmTest);
                    %             coh = abs(crsspctrmTest)./sqrt(powspctrm1Test.*powspctrm2Test);
                    % %             (abs(crsspctrmTest).^2)./(powspctrm1Test.*powspctrm2Test);
                    %             sqrt(crsspctrmTestReal.^2+crsspctrmTestImag.^2)
                    %             powDiff = powspctrm1Test-powspctrm2Test;
                    %
                    %             cohTemp = squeeze(sum(freq.coherence,1,'omitnan'));
                    
                    %% sum power and save
                    %
                    %                 [ntrl,nch, nf, nt] = size(freq.powspctrm);
                    %                 timeStart = find(freq.time >0,1);
                    %
                    %                 powTemp = squeeze(sum(freq.powspctrm,1,'omitnan'));
                    %                 powTemp = 10.*log10(powTemp);
                    %
                    %                 powSum(s,p,1:2,:,:) = powTemp(:,:,timeStart:timeStart+numel(SumToI)-1);
                    
                    %% plot, if desired
                    
                    if plt
                        figure(1)
                        imagesc(squeeze(powSum(s,p,1,:,:)),'YData', foi, 'XData', toi)
                        axis xy;
                        colormap(jet); colorbar;
                        xlabel('Time (seconds)');
                        ylabel('Frequency (Hz)');
                        caxis([20 max(max(squeeze(powTemp(1,:,:))))]);
                        colormap(jet); colorbar;
                        set(gca, 'FontName', 'Arial', 'FontSize', 12);
                        
                        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
                        
                        title(['Rat ' int2str(ratT.ratnums(r)) 'D' int2str(s) ...
                            '_P' int2str(p) ' NAc Power']);
                        
                        print([figurefolder int2str(ratT.ratnums(r)) ...
                            'Spectrogram_D' int2str(s) ...
                            '_P' int2str(p) '_NAc'],'-dpng');
                        
                        figure(2)
                        imagesc(squeeze(powSum(s,p,2,:,:)),'YData', foi, 'XData', toi)
                        axis xy;
                        colormap(jet); colorbar;
                        xlabel('Time (seconds)');
                        ylabel('Frequency (Hz)');
                        caxis([20 max(max(squeeze(powTemp(2,:,:))))]);
                        colormap(jet); colorbar;
                        set(gca, 'FontName', 'Arial', 'FontSize', 12);
                        
                        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
                        
                        title(['Rat ' int2str(ratT.ratnums(r)) 'D' int2str(s) ...
                            '_P' int2str(p) ' BLA Power']);
                        
                        print([figurefolder int2str(ratT.ratnums(r)) ...
                            'Spectrogram_D' int2str(s) ...
                            '_P' int2str(p) '_BLA'],'-dpng');
                        
                        
                        %                 %% put back into freq structure
                        %
                        %                 freq.time = toi;
                        %                 freq.powspctrm = newpowspctrm;
                        %
                        %
                        %
                        %                 powPlot = sum(freqDS.powspctrm,1,'omitnan');
                        %                 powPlot = squeeze(powPlot);
                        %
                        %                 imagesc(squeeze(powPlot(1,:,:)),'YData', foi, 'XData', toi)
                        %
                        %                 axis xy;
                        %
                        %                 print -djpeg test.jpg -r300
                        %
                        %                 set(gca, 'FontName', 'Arial', 'FontSize', 12);
                        %                 xlabel('Phase Frequency (Hz)');
                        %                 ylabel('Power (dB)');
                        %                 caxis([20 80]);
                        %                 colormap(jet); colorbar;
                        %                 saveas(gcf,[figurefolder 'crossKL_MI2dz_'...
                        %                     num2str(ratT.ratnums(r)) 'D' ...
                        %                     int2str(s) 'P' char(phases(p)) '_' prlabel 'PhaseMod' ...
                        %                     arlabel 'Amp' int2str(subt) '.png'])
                        %                 %% display raw spectrograms & coherograms
                        %
                        %                 for target = 1:3
                        %                     cfg                 = [];
                        %
                        %                     if target == 1
                        %                         cfg.parameter       = 'powspctrm';
                        %                         cfg.channel         = ratT.chNAc{r};
                        %                     elseif target == 2
                        %                         cfg.parameter       = 'powspctrm';
                        %                         cfg.channel         = ratT.chBLA{r};
                        %                     else
                        %                         cfg.parameter       = 'coherence';
                        %                         cfg.zlim            = 'zeromax';
                        %                         %                             cfg.channel         = ratT.chNAc{r};
                        %                         %                             cfg.refchannel      = ratT.chBLA{r};
                        %                     end
                        %
                        %                     if ~strcmp('none',cfg.channel) && ...
                        %                             (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
                        %                         figure(target);  clf;
                        %                         %                             ft_singleplotTFR(cfg, freq);
                        %                         ft_singleplotTFR(cfg, freqDS);
                        %                         colormap(jet);
                        %                         xlabel('Time (seconds)');
                        %                         ylabel('Frequency (Hz)');
                        %
                        %                         if target < 3
                        %                             set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
                        %
                        %                             title(['Rat ' int2str(ratT.ratnums(r)) 'D' int2str(s) ...
                        %                                 '_P' int2str(p) ' ' regions{target} ' Power']);
                        %                             print([figurefolder int2str(ratT.ratnums(r)) ...
                        %                                 'BaselineSpectrogram_D' int2str(s) ...
                        %                                 '_P' int2str(p) '_' regions{target}],'-dpng');
                        %                         else
                        %                             set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
                        %
                        %                             title(['Rat ' int2str(ratT.ratnums(r)) ...
                        %                                 'D' int2str(s) '_P' int2str(p) ' ' ...
                        %                                 ' Coherence']);
                        %                             print([figurefolder int2str(ratT.ratnums(r)) ...
                        %                                 'D' int2str(s) '_P' int2str(p) ...
                        %                                 '_Coherogram'],'-dpng');
                        %                         end
                        %                     end
                        %                 end
                    end
                    
                    
                    %% clear variables
                    
                    clearvars cfg freq;
                    
                    %   catch ME
                    %     warning(['Error (' ME.message ') while processing Rat # ' ...
                    %       int2str(ratT.ratnums(r)) ', ' phases{p} ...
                    %       '! Continuing with next in line.']);
                    %   end
                end
            end
        end
        %         save([datafolder filesep 'wavelet_PowSum_' int2str(ratT.ratnums(r)) '.mat'], 'powSum', '-v6');
    end
end
