%% Calculate baseline spectrograms and coherograms for each rat

close all; clearvars; clc;  % clean slate

set(0,'DefaultFigureWindowStyle','docked')

ft_defaults                 % set FieldTrip defaults
info_VPA_SI                 % load all background info (filenames, etc)

target = {'RatSide','EmptySide'};

load([datafolder 'SItimeStamp_Neural04292018.mat'])
load([datafolder 'TFanalysisFoIToI.mat'])

%% find indices that overlap with interaction intervals
% TSRatEmptyNeural;
%
% for rr = 1:size(TSRatEmpty,1)
%     for ss = 1:size(TSRatEmpty,2)
%         for p = 1:size(TSRatEmpty,3)
%
% idx = [];
% for ii = 1:size(TSRatEmptyNeural{r,s,p,1},1)
%     idx = [idx find(toi>= TSRatEmptyNeural{r,s,p,1}(ii,1) & toi <=TSRatEmptyNeural{r,s,p,1}(ii,2))];
% end
% powspctrm = squeeze(nansum(freq.powspctrm,1));
% NAcRatPow = squeeze(powspctrm(1,:,idx));
% NAcRatPow = 10.*log10(NAcRatPow);
%
% NAcRatPow(NAcRatPow == -inf) = NaN;
% NAcRatPowMean = nanmean(NAcRatPow,2);
% plot(NAcRatPowMean,'XData',foi)
%% use try-catch when running unsupervised batches

SumTarget = zeros(size(ratT,1), numel(sessions), numel(phases), 3, 2,numel(foi));
% dimord: [day, phase, brain region, interaction target, foi]
% brain regions: 1: NAc, 2: BLA, 3: Coherence
% interaction target: 1: Rat, 2: Empty
outputfile = [datafolder 'NeuralPower04302018.mat'];

for r = 1:size(ratT,1)
    if r == 6
    else
        %
        %
        %     end
        %     if exist(outputfile,'file')
        %         disp(['Outputfile for ' int2str(ratT.ratnums(r)) 'already exists. Skip!'])
        %     else
        for s = 1:numel(sessions)
            if r == 19 && s == 2
            else
                for p = 1:3 %numel of phases per day
                    
                    inputfile = [datafolder 'target' filesep 'mtmconvol_powandcoh_Linspace' ...
                        int2str(ratT.ratnums(r)) 'D' int2str(s) '_P' int2str(p) '.mat'];
                    
                    if exist(inputfile,'file')
                        %% load data
                        load(inputfile);
                        
                        %% find time indicies that matched with interaction intervals
                        for tgt = 1:2 % 1: rat. 2: empty cage
                            clear idx
                            idx = [];
                            temp = TSRatEmptyNeural{r,s,p,tgt};
                            for ii = 1:size(temp,1)
                                idx = [idx, find(freq.time >= temp(ii,1) & freq.time <=temp(ii,2))];
                            end
                            for mea = 1:3
                                if mea < 3
                                    figure(mea)
                                    SumTarget(r,s,p,mea,tgt,:) = squeeze(10.*log10(nanmean(freq.powspctrm(mea,:,idx),3)));
                                    plot(squeeze(SumTarget(r,s,p,mea,tgt,:)),'XData',foi)
                                    xlabel('Frequency (Hz)')
                                    ylabel('dB')
                                    title(['Rat #' int2str(ratT{r,1}) ' ' regions{mea} ' power, Day ' ...
                                        int2str(s) ' Phase ' int2str(p) ', ' target{tgt}])
                                    set(gca, 'FontName', 'Arial', 'FontSize', 12);
                                    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
                                    print([figurefolder int2str(ratT.ratnums(r)) ...
                                        'SI_PowerSpectra_' int2str(s) ...
                                        '_P' int2str(p) regions{mea} '_' target{tgt}],'-dpng');
                                    
                                elseif mea == 3
                                    figure(mea)
                                    SumTarget(r,s,p,mea,tgt,:) = squeeze(nanmean(freq.cohspctrm(1,:,idx),3));
                                    plot(squeeze(SumTarget(r,s,p,mea,tgt,:)),'XData',foi)
                                    xlabel('Frequency (Hz)')
                                    ylabel('Coherence')
                                    ylim([0.5 1])
                                    title(['Rat #' int2str(ratT{r,1}) ' Coherence, Day ' ...
                                        int2str(s) ' Phase ' int2str(p) ', ' target{tgt}])
                                    set(gca, 'FontName', 'Arial', 'FontSize', 12);
                                    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
                                    print([figurefolder int2str(ratT.ratnums(r)) ...
                                        'SI_Coherence_' int2str(s) ...
                                        '_P' int2str(p) '_' target{tgt}],'-dpng');
                                    
                                end
                                
                                
                            end
                            %% plot
                            
                            %
                        end
                        %                         for s = 1:numel(sessions)
                        %                             for rr = 1:2 % 1: NAc. 2: BLA
                        %                                 figure(rr)
                        %                                 clf
                        %                                 hold off
                        %                                 for p = 1:3 %numel of phases per day
                        %                                     figure(rr)
                        %                                     hold on
                        %                                     temp = powSum(s,p,rr,:,:);
                        %                                     temp(temp == -inf) = NaN;
                        %                                     plot(squeeze(nanmean(temp(1,1,1,:,:),5)),'XData',foi)
                        %                                 end
                        %                                 figure(rr)
                        %                                 legend('Phase 1', 'Phase 2', 'Phase 3')
                        %                                 title(['Rat ' int2str(ratT.ratnums(r)) 'D' int2str(s) ' ' regions{rr} ' Power']);
                        %                                 xlabel('Frequency (Hz)');
                        %                                 ylabel('dB')
                        %                                 set(gca, 'FontName', 'Arial', 'FontSize', 12);
                        %                                 set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
                        %                                 print([figurefolder int2str(ratT.ratnums(r)) ...
                        %                                     'PowerSpectra_D' int2str(s) regions{rr}],'-dpng');
                        %                             end
                        %                         end
                    end
                end
            end
        end
    end
    
end
cfg =[];
cfg.freq = freq.freq;
cfg.time = freq.time;
cfg.dimord = 'rat_session_phase_measurement_target_frequency';
cfg.labelcmb = freq.labelcmb;
save(outputfile,'SumTarget', 'cfg')