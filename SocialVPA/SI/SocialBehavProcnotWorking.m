% this script will calculate the mean time of the rat's nose interact
% with left cage, right cage, or center during 2 trials. The script will 
% also conduct basic statitical analysis and plots the data.

%% load the results and reorganize the cages as Rat/NoRat
load('SocialResult1.mat')

ProbeLeft = nan(4,2)

% use table

For i = 1:4
For j = 1:2
For k = 1:3
if ProbeLeft(i,j) = 1
    ResultLeft(i,j,k) = 