%% info needed for analyzing fear data, including all rats filenames
%#ok<*SNASGU> just provides background info for other analysis protocols

datafolder    = [tserver 'Analyses' filesep 'TempDataVPA' filesep ...
    'SI_Full' filesep];
configfolder  = [tserver 'Analyses' filesep 'MWAconfigs' filesep];

% put new figures in a folder named by date, in the format 'yyyy-mm-dd'
figurefolder  = [tserver 'Figures' filesep 'TempFiguresVPA' filesep ...
 datestr(now,29) filesep];
if ~exist(figurefolder,'dir')
  mkdir(figurefolder)
end

%% define MWA configuration(s)

% MWA = '16chSocialRight';      % name for this microwire array configuration
% 
% lftchs = {'AD01','AD05'; ...  % mPFC array configuration, where increasing
%   'AD02','AD06'; ...  % rows indicate electrodes ordered A -> P,
%   'AD03','AD07'; ...  % and increasing columns indicate those
%   'AD04','AD08'};     % ordered L -> R
% 
% rtchs = {'AD09','AD13'; ...   % BLA array configuration, where increasing
%   'AD10','AD14'; ...   % rows indicate electrodes ordered A -> P,
%   'AD11','AD15'; ...   % and increasing columns indicate those
%   'AD12','AD16'};      % ordered L -> R
% 
% if exist([configfolder 'elec_' MWA '_Right.mat'],'file') && ...
%     exist([configfolder 'layout_' MWA '_Right.mat'],'file')
%   disp('using existing elec & layout files')
% else
%   cfg = [];   % all arrays were configured & implanted the same for this expt
%   
%   % array & target info
%   cfg.atlascoord = [1.7, 1.60, -7.20; ... 	% AP/ML/DV relative to bregma
%     -3.0, 4.80, -8.60];      % dim 1/2/3 for below
%   cfg.angle     = [-6; 6];  % + angle means approach from medial, in degrees
%   cfg.angledim  = [2; 2];   % both probes angled along ML dim
%   cfg.elecspac  = [0.25, 0.25; ...  % in AP dim, then ML dim, for each probe
%     0.25, 0.25];      % in mm
%   
%   cfg.label = {lftchs; rtchs}; 	% 2 cell arrays nested in another cell array
%   cfg.side = {'Right';'Right'}; % which side of the brain each probe was on
%   
%   % calculate sensor positions & define layout
%   MWAlayout(cfg,[MWA '_Right'],configfolder);
% end
% 
% % left
% MWA = '16chSocialLeft';      % name for this microwire array configuration
% 
% lftchs = {'AD01','AD05'; ...  % BLA array configuration, where increasing
%   'AD02','AD06'; ...  % rows indicate electrodes ordered A -> P,
%   'AD03','AD07'; ...  % and increasing columns indicate those
%   'AD04','AD08'};     % ordered L -> R
% 
% rtchs = {'AD09','AD13'; ...   % NAc array configuration, where increasing
%   'AD10','AD14'; ...   % rows indicate electrodes ordered A -> P,
%   'AD11','AD15'; ...   % and increasing columns indicate those
%   'AD12','AD16'};      % ordered L -> R
% 
% if exist([configfolder 'elec_' MWA '_Left.mat'],'file') && ...
%     exist([configfolder 'layout_' MWA '_Left.mat'],'file')
%   disp('using existing elec & layout files')
% else
%   cfg = [];   % all arrays were configured & implanted the same for this expt
%   
%   % array & target info
%   cfg.atlascoord = [ -3.0, 4.80, -8.60; ... 	% AP/ML/DV relative to bregma
%      1.7, 1.60, -7.20];      % dim 1/2/3 for below
%   cfg.angle     = [6; -6];  % + angle means approach from medial, in degrees
%   cfg.angledim  = [2; 2];   % both probes angled along ML dim
%   cfg.elecspac  = [0.25, 0.25; ...  % in AP dim, then ML dim, for each probe
%     0.25, 0.25];      % in mm
%   
%   cfg.label = {lftchs; rtchs}; 	% 2 cell arrays nested in another cell array
%   cfg.side = {'Left';'Left'}; % which side of the brain each probe was on
%   
%   % calculate sensor positions & define layout
%   MWAlayout(cfg,[MWA '_Left'],configfolder);
% end

MWA = '16chSocial';

%% info needed for analyzing RC data, including all rats filenames
% RC = food reinforcement conditioning
ratnums = [361; 362; 363; 364; 377; 378; 379; 380; 391; 394; ...
    395; 396; 397; 398; 416; 417; 418; 419; 458; 459; 460];
excluded = logical([0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; ...
    0; 0; 0; 0; 0; 0; 0;]);   % see notes for why
treatment = {'SAL';'VPA'};
groups = {'VPA';'Saline';'VPA';'Saline';'VPA';'Saline';'VPA';'Saline'; ...
    'VPA';'Saline';'VPA';'Saline';'VPA';'Saline'; ...
    'Saline';'VPA';'Saline';'VPA';'Saline';'VPA';'Saline'};
side = {'Right';'Left';'Left';'Right';'Right';'Left';'Left';'Right'; ...
    'Right';'Left';'Right';'Left';'Left';'Right'; ...
    'Right';'Right';'Left';'Left';'Right';'Right';'Left'};

% channels to use for preproc. 
preprocChan = cell(numel(ratnums),1); 

% default is to include all channels
for ii = 1:numel(preprocChan)
   preprocChan{ii} = 'AD*'; 
end

% Exclude any channel that was not active during any recording files.
preprocChan{9} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD16';'AD17';...
    'AD18';'AD19';'AD20';'AD21';'AD22';'AD23';'AD24';'AD25';'AD26';...
    'AD27';'AD28';'AD29';'AD30';'AD31';'AD32'};
preprocChan{10} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15';'AD16';'AD17';...
    'AD18';'AD19';'AD20';'AD21';'AD22';'AD23';'AD24';'AD25';'AD26';...
    'AD27';'AD28';'AD29';'AD31'};
preprocChan{11} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD11';'AD13';'AD14';'AD15';'AD16';'AD17';...
    'AD18';'AD19';'AD20';'AD21';'AD22';'AD23';'AD24';'AD25';'AD26';...
    'AD27';'AD28';'AD29';'AD30';'AD31'};
preprocChan{13} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD12';'AD13';'AD14';'AD15';'AD16';'AD17';...
    'AD18';'AD19';'AD21';'AD23';'AD24';'AD25';...
    'AD27';'AD28';'AD29';'AD30';'AD31'};
preprocChan{17} = {'AD01';'AD03';'AD04';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD12';'AD13';'AD14';'AD15';'AD16';'AD17';...
    'AD18';'AD19';'AD21';'AD22';'AD23';'AD24';'AD25';'AD26';...
    'AD31'};
preprocChan{20} = {'AD01';'AD02';'AD03';'AD05';'AD06';'AD07';...
    'AD08';'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15';'AD16';'AD17';...
    'AD18';'AD19';'AD20';'AD21';'AD22';'AD23';'AD24';'AD25';'AD26';...
    'AD27';'AD28';'AD29';'AD30';'AD31';'AD32'};

chNAc = cell(numel(ratnums),1);    % best channel to use for CFC
chBLA = cell(numel(ratnums),1);     % best channel to use for CFC
chNAcsh = cell(numel(ratnums),1);     % best channel to use for CFC
chHipp = cell(numel(ratnums),1);     % best channel to use for CFC

chNAc{1} = 'AD05';
chNAc{2} = 'AD10';
chNAc{3} = 'AD13';
chNAc{4} = 'AD02';
chNAc{5} = 'AD01';
chNAc{6} = 'AD13';
chNAc{7} = 'AD13';
chNAc{8} = 'AD05';
chNAc{9} = 'AD21';
chNAc{10} = 'AD05';
chNAc{11} = 'AD21';
chNAc{12} = 'AD14';
chNAc{13} = 'AD03';
chNAc{14} = 'AD21';
chNAc{15} = 'AD21';
chNAc{16} = 'AD21';
chNAc{17} = 'AD05';
chNAc{18} = 'AD05';
chNAc{19} = 'AD18';
chNAc{20} = 'AD21';
chNAc{21} = 'AD05';

chBLA{1} = 'AD13';
chBLA{2} = 'AD02';
chBLA{3} = 'AD05';
chBLA{4} = 'AD10';
chBLA{5} = 'AD13';
chBLA{6} = 'AD05';
chBLA{7} = 'AD05';
chBLA{8} = 'AD13';
chBLA{9} = 'AD04';
chBLA{10} = 'AD29';
chBLA{11} = 'AD06';
chBLA{12} = 'AD24';
chBLA{13} = 'AD24';
chBLA{14} = 'AD04';
chBLA{15} = 'AD08';
chBLA{16} = 'AD11';
chBLA{17} = 'AD22';
chBLA{18} = 'AD24';
chBLA{19} = 'AD08';
chBLA{20} = 'AD08';
chBLA{21} = 'AD24';

chHipp{9} = 'AD14';
chHipp{10} = 'AD21';
chHipp{11} = 'AD14';
chHipp{12} = 'AD30';
chHipp{13} = 'AD19';
chHipp{14} = 'AD05';
chHipp{15} = 'AD14';
chHipp{16} = 'AD05';
chHipp{17} = 'AD23';
chHipp{18} = 'AD28';
chHipp{19} = 'AD05';
chHipp{20} = 'AD14';
chHipp{21} = 'AD21';

chNAcsh{10} = 'AD01';
chNAcsh{11} = 'AD23';
chNAcsh{12} = 'AD11';
chNAcsh{13} = 'AD07';
chNAcsh{14} = 'AD20';
chNAcsh{15} = 'AD20';
chNAcsh{16} = 'AD20';
chNAcsh{17} = 'AD01';
chNAcsh{18} = 'AD16';
chNAcsh{19} = 'AD26';
chNAcsh{20} = 'AD20';
chNAcsh{21} = 'AD10';

finalch = false([numel(ratnums),1]);
ratT = table(ratnums, excluded, side, chNAc, chBLA, finalch, groups);

VPAgroupInd = false(size(groups));
SALgroupInd = false(size(groups));
for r = 1:numel(ratnums)
    VPAgroupInd(r,1) = strcmp(ratT.groups{r},'VPA');  
end


for r = 1:numel(ratnums)
    SALgroupInd(r,1) = strcmp(ratT.groups{r},'Saline');
end

VPAInd = false(size(groups));
SALInd = false(size(groups));

for r = 1:numel(ratnums)
    VPAInd(r,1) = strcmp(ratT.groups{r},'VPA');  
end


for r = 1:numel(ratnums)
    SALInd(r,1) = strcmp(ratT.groups{r},'Saline');
end
VPAgroup = ratnums(VPAgroupInd);
SALgroup = ratnums(SALgroupInd);


clearvars ratnums excluded side chNAc chBLA finalch groups

% each column # here corresponds to cell column (2nd dim) in other variables
sessions = {'SIDay1', 'SIDay2'};


phases = {'1', '2', '3'};

abbr = {'SI1_1', 'SI1_2', 'SI1_3', 'SI2_1', 'SI2_2', 'SI2_3'};
  

blocks = {'SI1_1r', 'SI1_2r', 'SI1_3r', 'SI2_1r', 'SI2_2r', 'SI2_3r', ...
    'SI1_1o', 'SI1_2o', 'SI1_3o', 'SI2_1o', 'SI2_2o', 'SI2_3o'};

TLT = numel(blocks); %total # of sessions of training

% each row # here corresponds to the numerical code that will be saved in
% trl and data.trialinfo to identify which subtrials correspond to which
% interval types
intervals = {'Rat'; 'Empty'; 'After'};

% each row # here corresponds to the numerical code that will represent
% which region's data is being used for phase or amplitude source data
regions = {'NAc'; 'BLA'};



% initialize variables
goodchs   = cell(size(ratT,1),1);  % cellstr can't fit in 1 cell of a table
notdeadchs = cell(size(ratT,1),1);
notes     = cell(size(ratT,1),numel(sessions));
SI_ts    = cell(size(ratT,1),numel(sessions));
nexfile   = cell(size(ratT,1),numel(sessions));
BLnexfile   = cell(size(ratT,1),1);
BLtoneTS   = cell(size(ratT,1),1);
chNAc = cell(size(ratT,1),1);    % best channel to use for CFC
chBLA = cell(size(ratT,1),1);     % best channel to use for CFC

% any extra comments about each 

% notes{1,3} = 'tone timestamps for 361''s RC3 are in "toneTS{1,3}"';
% % toneTS{1,3} = [];
% notes{1,4} = 'tone timestamps for 361''s RC4 are in "toneTS{1,4}"';
% % toneTS{1,4} = [];
% notes{1,5} = 'tone timestamps for 361''s RC5 are in "toneTS{1,5}"';
% % toneTS{1,5} = [];
% notes{1,6} = 'tone timestamps for 361''s RC6 are in "toneTS{1,6}"';
% % toneTS{1,6} = [];
% notes{2,3} = 'tone timestamps for 362''s RC3 are in "toneTS{2,3}"';
% % toneTS{2,3} = [];
% notes{2,4} = 'tone timestamps for 362''s RC4 are in "toneTS{2,4}"';
% % toneTS{2,4} = [];
% notes{2,5} = 'tone timestamps for 362''s RC5 are in "toneTS{2,5}"';
% % toneTS{2,5} = [];
% notes{2,5} = 'tone timestamps for 362''s RC6 are in "toneTS{2,6}"';
% % toneTS{2,6} = [];
% notes{3,3} = 'tone timestamps for 363''s RC3 are in "toneTS{3,3}"';
% % toneTS{3,3} = [];
% notes{3,4} = 'tone timestamps for 363''s RC4 are in "toneTS{3,4}"';
% % toneTS{3,4} = [];
% notes{3,5} = 'tone timestamps for 363''s RC5 are in "toneTS{3,5}"';
% % toneTS{3,5} = [];
% notes{3,6} = 'tone timestamps for 363''s RC6 are in "toneTS{3,6}"';
% % toneTS{3,6} = [];
% notes{4,3} = 'tone timestamps for 363''s RC3 are in "toneTS{4,3}"';
% % toneTS{4,3} = [];
% notes{4,4} = 'tone timestamps for 364''s RC4 are in "toneTS{4,4}"';
% % toneTS{4,4} = [];
% notes{4,5} = 'tone timestamps for 364''s RC5 are in "toneTS{4,5}"';
% % toneTS{4,5} = [];
% notes{4,6} = 'tone timestamps for 364''s RC6 are in "toneTS{4,6}"';
% % toneTS{4,6} = [];
% notes{6,3} = 'headstage unplugged once';
% notes{7,2} = 'headstage unplugged once';
% notes{7,4} = 'headstage unplugged three times';
% notes{7,5} = 'headstage unplugged once';
% notes{7,6} = 'headstage unplugged twice';
% notes{7,7} = 'headstage unplugged twice';
% 

frameRate = 30.00003;

SI_ts{1,1} = [77000; 1050000; 2050000];
SI_ts{1,2} = [45000; 1055000; 2091000];
SI_ts{2,1} = [60000; 1024000; 1990000];
SI_ts{2,2} = [51000; 1051000; 2005000];
SI_ts{3,1} = [65000; 1030000; 2013000];
SI_ts{3,2} = [73000; 1047000; 2009000];
SI_ts{4,1} = [74000; 1057000; 2031000];
SI_ts{4,2} = [90000; 1101000; 2120000];
SI_ts{5,1} = [0; 1015000; 2040000];
SI_ts{5,2} = [0; 970000; 1920000];
SI_ts{6,1} = [];
SI_ts{6,2} = [0; 965000; 1935000];
SI_ts{7,1} = [0; 990000; 1960000];
SI_ts{7,2} = [0; 980000; 1945000];
SI_ts{8,1} = [0; 1037000; 2000000];
SI_ts{8,2} = [0; 970000; 1930000];

SI_ts{9,1} = [0; 962000; 1981000]; % 391
SI_ts{9,2} = [0; 960000; 1921000];
SI_ts{10,1} = [0; 960000; 1920000] ; % 394
SI_ts{10,2} = [0; 960000; 1945000]; 
SI_ts{11,1} = [0; 975000; 2030000]; % 395
SI_ts{11,2} = [0; 1005000; 2130000];
SI_ts{12,1} = [0; 1020000; 1983000]; % 396
SI_ts{12,2} = [0; 1037000; 2045000];
SI_ts{13,1} = [0; 1055000; 2040000]; % 397
SI_ts{13,2} = [0; 1036000; 2145000];
SI_ts{14,1} = [0; 990000; 2080000]; % 398
SI_ts{14,2} = [0; 993000; 1970000];
SI_ts{15,1} = [0; 960000; 1920000]; % 416
SI_ts{15,2} = [0; 967000; 1950000];
SI_ts{16,1} = [0; 1005000; 1975000]; % 417
SI_ts{16,2} = [0; 960000; 1953000]; 
SI_ts{17,1} = [0; 975000; 1980000]; % 418
SI_ts{17,2} = [0; 990000; 1960000];
SI_ts{18,1} = [0; 990000; 1960000]; % 419
SI_ts{18,2} = [0; 965000; 1930000];
% Scoring of 458-460 is yet checked
SI_ts{19,1} = [0; 990000; 1955000]; % 458
SI_ts{19,2} = [0; 960000; 1912000];
SI_ts{20,1} = [0; 975000; 1955000]; % 459
SI_ts{20,2} = [0; 975000; 1980000];
SI_ts{21,1} = [0; 975000; 1935000]; % 460
SI_ts{21,2} = [0; 975000; 1950000];


%% channels to use for each rat, defaults to 'AD*' if empty
goodchs{1} = {'AD*'};
goodchs{2} = {'AD*'};
goodchs{3} = {'AD*'};
goodchs{4} = {'AD*'};
goodchs{5} = {'AD*'};
goodchs{6} = {'AD*'};
goodchs{7} = {'AD*'};
goodchs{8} = {'AD*'};
goodchs{9} = {'AD*'};
goodchs{10} = {'AD*'};
goodchs{11} = {'AD*'};
goodchs{12} = {'AD*'};
goodchs{13} = {'AD*'};
goodchs{14} = {'AD*'};
goodchs{15} = {'AD*'};
goodchs{16} = {'AD*'};
goodchs{17} = {'AD*'};
goodchs{18} = {'AD*'};
goodchs{19} = {'AD*'};
goodchs{20} = {'AD*'};
goodchs{21} = {'AD*'};

%% filenames
% Can't use PlexUtil or NeuroExplorer merged files because either FieldTrip
% crashes or the timestamps are off after the merge.
% FT_preproc_Fear_TEM.m can handle this cellstr.

% Baseline recording in social chamber
BLnexfile{1,1} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160323_0000-aligned.nex'];
BLnexfile{2,1} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160323_0001-aligned.nex'];
BLnexfile{3,1} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160323_0002-aligned.nex'];
BLnexfile{4,1} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160323_0003-aligned.nex'];
BLnexfile{5,1} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160629_0001-aligned.nex'];
BLnexfile{6,1} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160629_0002-aligned.nex'];
BLnexfile{7,1} = [tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160629_0003-aligned.nex'];
BLnexfile{8,1} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160629_0004-aligned.nex'];
BLnexfile{9,1} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160907_0010-aligned.nex'];
BLnexfile{10,1} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160907_0013-aligned.nex'];
BLnexfile{11,1} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161120_0003-aligned.nex'];
BLnexfile{12,1} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161120_0004-aligned.nex'];
BLnexfile{13,1} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161120_0005-aligned.nex'];
BLnexfile{14,1} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161120_0006-aligned.nex'];
BLnexfile{15,1} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170301_0010-aligned.nex'];
BLnexfile{16,1} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170301_0011-aligned_sub.nex'];
BLnexfile{17,1} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170301_0012-aligned.nex'];
BLnexfile{18,1} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170301_0013-aligned_sub.nex'];
BLnexfile{19,1} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170531_0031-aligned_sub.nex'];
BLnexfile{20,1} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170531_0030-aligned_sub.nex'];
BLnexfile{21,1} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170531_0029-aligned_sub.nex'];

% Conditioning Day 1
nexfile{1,1} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160324_0000-aligned.nex'];
nexfile{2,1} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160324_0001-aligned.nex'];
nexfile{3,1} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160324_0002-aligned.nex'];
nexfile{4,1} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160324_0003-aligned.nex'];
nexfile{5,1} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160630_0001-aligned.nex'];
% nexfile{6,1} = {[tserver 'PlexonData Raw' filesep '378' filesep ...
%   '378_160630_0002-aligned.nex'];[tserver 'PlexonData Raw' filesep ...
%   '378' filesep '378_160630_0003-aligned.nex'];[tserver ...
%   'PlexonData Raw' filesep '378' filesep '378_160630_0004-aligned.nex']};
nexfile{7,1} = [tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160630_0005-aligned.nex'];
nexfile{8,1} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160630_0006-aligned.nex'];
nexfile{9,1} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160908_0000-aligned.nex'];
nexfile{10,1} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160908_0003-aligned.nex'];
nexfile{11,1} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161121_0001-aligned.nex'];
nexfile{12,1} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161121_0002-aligned.nex'];
nexfile{13,1} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161121_0003-aligned.nex'];
nexfile{14,1} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161121_0000-aligned.nex'];
nexfile{15,1} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170302_0000-aligned_sub.nex'];
nexfile{16,1} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170302_0001-aligned_sub.nex'];
nexfile{17,1} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170302_0003-aligned.nex'];
nexfile{18,1} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170302_0004-aligned_sub.nex'];
nexfile{19,1} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170601_0002-aligned_sub.nex'];
nexfile{20,1} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170601_0001-aligned_sub.nex'];
nexfile{21,1} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170601_0000-aligned_sub.nex'];

% Conditioning Day 2
nexfile{1,2} = [tserver 'PlexonData Raw' filesep '361' filesep ...
  '361_160325_0000-aligned.nex'];
nexfile{2,2} = [tserver 'PlexonData Raw' filesep '362' filesep ...
  '362_160325_0002-aligned.nex'];
nexfile{3,2} = [tserver 'PlexonData Raw' filesep '363' filesep ...
  '363_160325_0003-aligned.nex'];
nexfile{4,2} = [tserver 'PlexonData Raw' filesep '364' filesep ...
  '364_160325_0004-aligned.nex'];
nexfile{5,2} = [tserver 'PlexonData Raw' filesep '377' filesep ...
  '377_160701_0000-aligned.nex'];
nexfile{6,2} = [tserver 'PlexonData Raw' filesep '378' filesep ...
  '378_160701_0003-aligned.nex'];
nexfile{7,2} = [tserver 'PlexonData Raw' filesep '379' filesep ...
  '379_160701_0002-aligned.nex'];
nexfile{8,2} = [tserver 'PlexonData Raw' filesep '380' filesep ...
  '380_160701_0001-aligned.nex'];
nexfile{9,2} = [tserver 'PlexonData Raw' filesep '391' filesep ...
  '391_160909_0000-aligned.nex'];
nexfile{10,2} = [tserver 'PlexonData Raw' filesep '394' filesep ...
  '394_160909_0003-aligned.nex'];
nexfile{11,2} = [tserver 'PlexonData Raw' filesep '395' filesep ...
  '395_161122_0001-aligned.nex'];
nexfile{12,2} = [tserver 'PlexonData Raw' filesep '396' filesep ...
  '396_161122_0002-aligned.nex'];
nexfile{13,2} = [tserver 'PlexonData Raw' filesep '397' filesep ...
  '397_161122_0003-aligned.nex'];
nexfile{14,2} = [tserver 'PlexonData Raw' filesep '398' filesep ...
  '398_161122_0000-aligned.nex'];
nexfile{15,2} = [tserver 'PlexonData Raw' filesep '416' filesep ...
  '416_170303_0000-aligned.nex'];
nexfile{16,2} = [tserver 'PlexonData Raw' filesep '417' filesep ...
  '417_170303_0001-aligned_sub.nex'];
nexfile{17,2} = [tserver 'PlexonData Raw' filesep '418' filesep ...
  '418_170303_0002-aligned.nex'];
nexfile{18,2} = [tserver 'PlexonData Raw' filesep '419' filesep ...
  '419_170303_0003-aligned_sub.nex'];
nexfile{19,2} = [tserver 'PlexonData Raw' filesep '458' filesep ...
  '458_170602_0005-aligned_sub.nex'];
nexfile{20,2} = [tserver 'PlexonData Raw' filesep '459' filesep ...
  '459_170602_0004-aligned_sub.nex'];
nexfile{21,2} = [tserver 'PlexonData Raw' filesep '460' filesep ...
  '460_170602_0003-aligned_sub.nex'];
%% complete workflow:
% 0) align original .plx files, save as .nex
% 1) FT_preproc_Fear_TEM - Load all data from -aligned.nex files into
% FieldTrip format & merge into 1 dataset per rat, using new trialfun_TEM.m
% and supporting a cell array of strings for multiple filenames within one
% phase (to avoid merged NEX files, which are hard to read correctly).
% 2) FT_rejectArt_Fear_TEM
% 3) FT_ica_Fear_TEM
% 4) spectra/coher
% 5) cross-frequency
% 6) compare to behavior
% 7) granger causality?