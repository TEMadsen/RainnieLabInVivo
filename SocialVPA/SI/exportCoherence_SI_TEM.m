CleanSlate

info_SI2

cohT = table;

for r = 1:numel(ratnums)
  for d = 1:2 % days
    for p = 1:3 % phases
  cfg = [];
  cfg.method    =  'coh';
  cfg.inputfile = [datafolder 'PowSpectra_' num2str(ratnums(r)) 'D' ...
    num2str(d) '_P' num2str(p) '1.mat'];  % freq
  stat = ft_connectivityanalysis(cfg);
  if isempty(cohT)
    cohT = [cohT table(stat.freq','VariableNames',{'Freq'})];
  end
  cohT = [cohT table(stat.cohspctrm', ...
    'VariableNames',{['Rat' num2str(ratnums(r)) 'Day' num2str(d) 'Phase' num2str(p)]})];
    end
  end
end

writetable(cohT,[datafolder 'Coherence.xlsx'])