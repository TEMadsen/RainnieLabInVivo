%% Import data from spreadsheet
% Script for importing data from the following spreadsheet:
%
%    Workbook: /Volumes/yerkes2/DRLab/Teresa/Assistants/Liz/social interaction video 2016/Results_Center.xlsx
%    Worksheet: Trial 3
%
% To extend the code for use with different selected data or a different
% spreadsheet, generate a function instead of a script.

% Auto-generated by MATLAB on 2016/06/21 14:07:08

%% preallocation
RatID = [361, 362, 363, 364];
% trial = 1, 2
% phase = 1, 2, 3
% location = left, center, right

ResultRight = cell(numel(RatID),2,3) % preallocate a cell array to store 
                                     ... data by RatID, trial, phase
ResultLeft = cell(4,2,3)
ResultCenter = cell(4,2,3)

%% save center cage enterings             
i = 3
for k = 1:numel(RatID)
    for l = 1:2
        for m = 1:3
            TrialNum = sprintf('Trial %d',i)
            i = i+1;
            
            %% Import the data
            [~, ~, raw] = xlsread('/Volumes/yerkes2/DRLab/Teresa/Assistants/Liz/social interaction video 2016/Results_Center.xlsx',TrialNum);
            raw = raw(8:end,1:4);

            %% Create output variable
            data = reshape([raw{:}],size(raw));

            %% Create table
            ResultsCenter = table;

            %% Allocate imported array to column variable names
            ResultsCenter.ID = data(:,1);
            ResultsCenter.FromSecond = data(:,2);
            ResultsCenter.ToSecond = data(:,3);
            ResultsCenter.LengthSecond = data(:,4);

            ResultCenter{k,l,m} = ResultsCenter

            %% Clear temporary variables
            clearvars data raw;
            
        end
    end
end

%% save left cage enterings
clear i k l m

i = 3
for k = 1:4
    for l = 1:2
        for m = 1:3
            TrialNum = sprintf('Trial %d',i)
            i = i+1;
            
            %% Import the data
            [~, ~, raw] = xlsread('/Volumes/yerkes2/DRLab/Teresa/Assistants/Liz/social interaction video 2016/Results_Left.xlsx',TrialNum);
            raw = raw(8:end,1:4);

            %% Create output variable
            data = reshape([raw{:}],size(raw));

            %% Create table
            ResultsCenter = table;

            %% Allocate imported array to column variable names
            ResultsCenter.ID = data(:,1);
            ResultsCenter.FromSecond = data(:,2);
            ResultsCenter.ToSecond = data(:,3);
            ResultsCenter.LengthSecond = data(:,4);

            ResultLeft{k,l,m} = ResultsCenter

            %% Clear temporary variables
            clearvars data raw;
            
        end
    end
end

%% save right cage enterings

clear i k l m

i = 3
for k = 1:4
    for l = 1:2
        for m = 1:3
            TrialNum = sprintf('Trial %d',i)
            i = i+1;
            
            %% Import the data
            [~, ~, raw] = xlsread('/Volumes/yerkes2/DRLab/Teresa/Assistants/Liz/social interaction video 2016/Results_Right.xlsx',TrialNum);
            raw = raw(8:end,1:4);

            %% Create output variable
            data = reshape([raw{:}],size(raw));

            %% Create table
            ResultsCenter = table;

            %% Allocate imported array to column variable names
            ResultsCenter.ID = data(:,1);
            ResultsCenter.FromSecond = data(:,2);
            ResultsCenter.ToSecond = data(:,3);
            ResultsCenter.LengthSecond = data(:,4);

            ResultRight{k,l,m} = ResultsCenter

            %% Clear temporary variables
            clearvars data raw;
            
        end
    end
end

save ('SocialResult1.mat','ResultRight','ResultCenter','ResultLeft')

ResultRight{RatID == 363,l,m}
[0 0 1 0]