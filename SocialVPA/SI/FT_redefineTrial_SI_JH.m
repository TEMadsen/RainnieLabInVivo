%% Redefine trails to 4.096 s bins for Cross-Frequency Coupling
% The redefination is needed for an unbiased comparison of modulation index
% between interaction targets or phases
% Use FFT-friendly time length (sample number should be power of 2 (eg. 
% 4.096s can be used when sampling rate is 1000 Hz).

close all force; clearvars; clc;  % clean slate

info_VPA_SI                       % load all background info (filenames, etc)

%% full loop

for r = 8
 
inputfile = [datafolder filesep 'LFPTrialData_' ...
    int2str(ratT.ratnums(r)) 'Merged.mat'];
outputfile = [datafolder filesep 'LFPTrialData_4s_' ...
    int2str(ratT.ratnums(r)) 'Merged.mat'];


cfg = [];
cfg.length    = 4.096;
cfg.overlap   = 0;
cfg.inputfile   =  inputfile;
cfg.outputfile  =  outputfile;
% Use as: data = ft_redefinetrial(cfg, data)

ft_redefinetrial(cfg);

end