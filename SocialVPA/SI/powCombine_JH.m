 %% Calculate baseline spectrograms and coherograms for each rat

close all; clearvars; clc;  % clean slate

set(0,'DefaultFigureWindowStyle','docked')

ft_defaults                 % set FieldTrip defaults
info_VPA_SI                 % load all background info (filenames, etc)

foi       = 1.5:0.25:150;   % freqs from 1-103 Hz progressing up by 5% each
tapsmofrq = foi/10;         % smooth each band by +/- 5%

for f = find(tapsmofrq < 0.5)  % don't care about narrower bands than this
    tapsmofrq(f) = 0.5;          % keeps time windows under 8 seconds
end

t_ftimwin = 2./tapsmofrq;   % for 3 tapers (K=3), T=2/W

% for smoothed visualization & less memory use
tshift = min(t_ftimwin)/2;
SumToI = 0:min(t_ftimwin)/2:900;    % time windows overlap by at least 50%

%% use try-catch when running unsupervised batches
for r = 1:size(ratT,1)
    if r == 6
    else
        powSum = zeros(numel(sessions), numel(phases), 2, numel(foi), numel(SumToI));
        % 2 channels, 1: NAc, 2: BLA
        outputfile = [datafolder int2str(ratT.ratnums(r)) 'CombinedPower_noTime.mat'];
        
    end
    if exist(outputfile,'file')
        disp(['Outputfile for ' int2str(ratT.ratnums(r)) 'already exists. Skip!'])
    else
        
        for s = 1:numel(sessions)
            for p = 1:3 %numel of phases per day
                
                inputfile = [datafolder filesep 'wavelet_powandcsd_Linspace' ...
                    int2str(ratT.ratnums(r)) 'D' int2str(s) '_P' int2str(p) '.mat'];
                
                if exist(inputfile,'file')
                    %% load data
                    load(inputfile);
                    
                    %% sum across trials for power
                    powSum(s,p,:,:,:) = 10.*log10(squeeze(sum(freq.powspctrm,1,'omitnan')));
                end
            end
        end
    end
    powSum = nanmean(powSum,5);
    save(outputfile,'powSum')
    
    for s = 1:numel(sessions)
        for rr = 1:2 % 1: NAc. 2: BLA
            figure(rr)
            clf
            hold off
            for p = 1:3 %numel of phases per day
                figure(rr)
                hold on
                temp = powSum(s,p,rr,:,:);
                temp(temp == -inf) = NaN;
                plot(squeeze(nanmean(temp(1,1,1,:,:),5)),'XData',foi)
            end
            figure(rr)
            legend('Phase 1', 'Phase 2', 'Phase 3')
            title(['Rat ' int2str(ratT.ratnums(r)) 'D' int2str(s) ' ' regions{rr} ' Power']);
            xlabel('Frequency (Hz)');
            ylabel('dB')
            set(gca, 'FontName', 'Arial', 'FontSize', 12);
            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
            print([figurefolder int2str(ratT.ratnums(r)) ...
                'PowerSpectra_D' int2str(s) regions{rr}],'-dpng');
        end
    end
end