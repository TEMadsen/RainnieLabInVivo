%% calculates 2d cross-frequency phase-amplitude modulation
% for each interval type of multiple social interaction tests
% across multiple rats

close all force; clearvars; clc;  % clean slate

info_VPA_SI                       % load all background info (filenames, etc)
% load([datafolder filesep 'SI_ts.mat']);

set(0,'DefaultFigureWindowStyle','docked')

plt = 'y';                        % plot CFC
hx = waitbar(0,'Getting started...');

%% set analysis parameters
foi_phase = 4:0.25:14.0;  % central frequencies for phase modulation
w_phase = 0.5;              % width of phase band (foi +/- w)
foi_amp = 12:3:132;         % central frequencies for modulated amplitude
w_amp = 6;                  % width of amplitude band (foi +/- w)

%% make 2d Cross-Frequency Modulation plots for each rat/session/interval

for r = 1:height(ratT)
    % Full, artifact-free interaction intervals marked with
    % data.trialinfo(:,1) = original trial number, and
    % data.trialinfo(:,2) = number representing condition
    if r == 6
    else
        for s = 1:size(sessions,2)
            
            %         inputfile = [datafolder filesep 'LFPTrialData_4s_' ...
            %             int2str(ratT.ratnums(r)) 'Merged.mat'];
            inputfile = [datafolder 'cleanLFPTrialData_' ...
                int2str(ratT.ratnums(r)) int2str(s) 'Merged.mat'];
            
            outputfile = [datafolder 'crossKL_MI2dz_'...
                num2str(ratT.ratnums(r)) '_HiRes.mat'];
            
            if ~exist(inputfile,'file')
                error('inputfile does not exist');
            end
            
            if exist(outputfile,'file')
                disp(['Data for Rat # ' int2str(ratT.ratnums(r)) ', ' ...
                    ' already analyzed!']);
                if plt == 'y'
                    disp('Loading previous analysis results from file.');
                    load(outputfile);
                end
                
            else
                %% preallocate memory for variables
                
                zscore2d = cell(size(sessions,2),numel(phases),1);
                pval2d = cell(size(sessions,2),numel(phases),1);
                h2d = cell(size(sessions,2),numel(phases),1);
                MI2d = cell(size(sessions,2),numel(phases),1);
                phasepref2d = cell(size(sessions,2),numel(phases),1);
                
                cmax = 0;
                
                %% load data
                
                load(inputfile);
                
                chIdx = 1:16;
                chNAc = chIdx(strcmp(data.label,ratT{r,4}));
                chBLA = chIdx(strcmp(data.label,ratT{r,5}));
                
                %% extract trialinfo
                
                info = struct;
                info.rat = ratT(r,:);
                info.trialinfo = data.trialinfo;
                info.time = data.time;
                info.cfg = data.cfg;
                info.triallength = data.cfg.length;
                info.subtrial = cell(size(sessions,2),numel(phases),1);
                % the starting and end trial number of the first, second and third 5 minute of each phase.
                
                info.subtrialts = cell(size(sessions,2),numel(phases),1);
                
                %% analyze data
                
                for p = 1:numel(phases)
                    trials = find(data.trialinfo(:,2) == s & ...
                        data.trialinfo(:,1) == p);
                    trialts = NaN(numel(trials),2); %col 1/2 start/end ts of each trial
                    
                    % find start& end timestamp for each trials
                    for ts = 1:numel(trials)
                        trialts(ts,1) = info.time{1,trials(ts)}(1);
                        trialts(ts,2) = info.time{1,trials(ts)}(end);
                    end
                    info.subtrialts{s,p} = trialts;
                    
                    % find the trial # of 1st/2nd/3rd 5 mins of the phase
                    for subt = 1:3 % Three 5 minutes of each 15 min phase
                        info.subtrial{s,p}(subt,1) = find(trialts(:,1) >= 300*(subt-1),1);
                        info.subtrial{s,p}(subt,2) = find(trialts(:,2) < 300*(subt)+1,1,'last');
                    end
                    
                    if isempty(trials)
                        warning(['No data found for Rat # ' ...
                            num2str(ratT.ratnums(r)) ', Day' int2str(s) ...
                            ' Phase_' int2str(p) '.  Skipping.']);
                    else
                        for t = 1:numel(trials)
                            waitbar(t/numel(trials),hx,...
                                {['Calculating 2d cross-region MI z-score for Rat # ' ...
                                num2str(ratT.ratnums(r)) ',']; ...
                                ['Day' int2str(s) ...
                                ' Phase ' int2str(p) '.']});
                            % compute CFC for all the trials within a phase
                            [~,~,MI2dtemp,phasepref2dtemp] = crossKLMI2d_JH(...
                                data.trial{trials(t)}(chNAc,:),...
                                data.trial{trials(t)}(chBLA,:),...
                                foi_phase,w_phase,foi_amp,w_amp);
                            for pr = 1:2 % each phase modulating region
                                for ar = 1:2 % each amp modulated region
                                    MI2d{s,p}{pr,ar}(:,:,t) = ...
                                        MI2dtemp{pr,ar};
                                    phasepref2d{s,p}{pr,ar}(:,:,t) = ...
                                        phasepref2dtemp{pr,ar};
                                end
                            end
                        end
                        %                     cmax = max(max(max(mean(...
                        %                         zscore2d{s,p}{pr,ar},3))),cmax);
                    end
                end
            end
            for s = 1:size(sessions,2)
                for p = 1:numel(phases)
                    for pr = 1:2 % each phase modulating region
                        for ar = 1:2 % each amp modulated region
                            cmax = max(max(max(nanmean(...
                                MI2d{s,p}{pr,ar},3))),cmax);
                        end
                    end
                end
            end
            %% save analysis results
            
            disp(['writing results to file ' outputfile]);
            save(outputfile, '*2d', 'cmax', 'foi*', 'w*', 'info', '-v6');
            
        end
        
        %     %% compute mean phase preference
        %
        %     phasepref2dSig = phasepref2d;
        %     for s = 1:size(sessions,2)
        %         for p = 1:numel(phases)
        %             for pr = 1:2 % each phase modulating region
        %                 for ar = 1:2 % each amp modulated region
        %                     phasepref2dSig{s,p}{pr,ar}(h2d{s,p}{pr,ar} == 0) = NaN;
        %                 end
        %             end
        %         end
        %     end
        
        %% plot results
        
        if plt == 'y'
            for pr = 1:2 % each phase modulating region
                if pr == 1
                    prlabel = 'NAc';
                else
                    prlabel = 'BLA';
                end
                
                for ar = 1:2 % each amp modulated region
                    if ar == 1
                        arlabel = 'NAc';
                    else
                        arlabel = 'BLA';
                    end
                    for s = 1:size(sessions,2)
                        figure(pr+(ar-1)*2); clf;
                        for p = 1:numel(phases)
                            if isempty(MI2d{s,p})
                                warning(['No intervals found for Rat # ' ...
                                    num2str(ratT.ratnums(r)) ', Day' int2str(s) ...
                                    ' Phase_' int2str(p) '.  Skipping.']);
                            else
                                for subt = 1:3 % first/second/third 5 mins of phase
                                    %% plot average MI
                                    subplot(3,3,subt+(p-1)*3)
                                    imagesc(foi_phase, foi_amp, mean(...
                                        MI2d{s,p}{pr,ar}(:,:,...
                                        info.subtrial{s,p}(subt,1):...
                                        info.subtrial{s,p}(subt,2)),3)');
                                    axis xy;
                                    set(gca, 'FontName', 'Arial', 'FontSize', 8);
                                    xlabel('Phase Frequency (Hz)');
                                    ylabel('Envelope Frequency (Hz)');
                                    %                                 title({['D' int2str(s) 'P' char(phases(p)) ' ' ...
                                    %                                     int2str(subt) '/3']; ...
                                    %                                     ['Average of ' ...
                                    %                                     num2str(info.subtrial{s,p}(subt,2)...
                                    %                                     -info.subtrial{s,p}(subt,1)+1) ...
                                    %                                     ' Interactions']});
                                    title({['D' int2str(s) 'P' char(phases(p)) ' ' ...
                                        int2str(subt) '/3']});
                                    caxis([0 cmax]);
                                    pos = get(gca,'position');
                                    pos(3)= 0.23;
                                    set(gca,'Position',pos)
                                    colormap(jet);
                                    
                                    if subt+(p-1)*3 == 9
                                        colorbar
                                        cb = get(colorbar,'Position');
                                        cb(3) = 0.01;
                                        set(colorbar,'Position',cb)
                                    end
                                    
                                    %                                 %% plot phase preference
                                    %
                                    %                                 figure(pr+(ar-1)*2+4); clf;
                                    %
                                    %                                 imagesc(foi_phase, foi_amp, circ_mean(circ_ang2rad(...
                                    %                                     phasepref2d{s,p}{pr,ar}(:,:, ...
                                    %                                     info.subtrial{s,p}(subt,1):...
                                    %                                     info.subtrial{s,p}(subt,2))), ...
                                    %                                     h2d{s,p}{pr,ar}(:,:, ...
                                    %                                     info.subtrial{s,p}(subt,1):...
                                    %                                     info.subtrial{s,p}(subt,2)) .* ...
                                    %                                     zscore2d{s,p}{pr,ar}(:,:, ...
                                    %                                     info.subtrial{s,p}(subt,1):...
                                    %                                     info.subtrial{s,p}(subt,2)),3)');
                                    %                                 axis xy;
                                    %                                 set(gca, 'FontName', 'Arial', 'FontSize', 18);
                                    %                                 xlabel('Phase Frequency (Hz)');
                                    %                                 ylabel('Envelope Frequency (Hz)');
                                    %                                 title({['Rat # ' num2str(ratT.ratnums(r)) ', D' ...
                                    %                                     int2str(s) 'P' char(phases(p)) ' ' ...
                                    %                                     int2str(subt) '/3']; ...
                                    %                                     [prlabel ' phase modulating ' ...
                                    %                                     arlabel ' amplitude']; ...
                                    %                                     ['Average of ' ...
                                    %                                     num2str(info.subtrial{s,p}(subt,2)...
                                    %                                     -info.subtrial{s,p}(subt,1)+1) ...
                                    %                                     ' Interactions']});
                                    %                                 blknan = [0 0 0; hsv];
                                    %                                 caxis([-pi-0.1 pi]); colormap(blknan); colorbar;
                                    %                                 set(gcf,'WindowStyle','docked');
                                    %                                 saveas(gcf,[figurefolder 'Phase_crossKL_MI2dz_'...
                                    %                                     num2str(ratT.ratnums(r)) 'D' ...
                                    %                                     int2str(s) 'P' char(phases(p)) '_' prlabel 'PhaseMod' ...
                                    %                                     arlabel 'Amp' int2str(subt) '.png'])
                                    
                                end
                            end
                        end
                        suptitle(['Rat #' num2str(ratT.ratnums(r)) ', ' ...
                            prlabel ' phase modulating ' ...
                            arlabel ' amplitude KL-modulation index'])
                        saveas(gcf,[figurefolder 'crossKL_MI2dz_'...
                            num2str(ratT.ratnums(r)) 'D' ...
                            int2str(s) '_' prlabel 'PhaseMod' ...
                            arlabel 'Amp.png'])
                    end
                end
            end
        end
    end
end
close(hx)
