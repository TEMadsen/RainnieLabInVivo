%% Calculate baseline spectrograms and coherograms for each rat

close all; clearvars; clc;  % clean slate

set(0,'DefaultFigureWindowStyle','docked')

ft_defaults                 % set FieldTrip defaults
info_VPA_SI                 % load all background info (filenames, etc)
% 
% target = {'RatSide','EmptySide'};
% 
% load([datafolder 'SItimeStamp_Neural.mat'])
load([datafolder 'TFanalysisFoIToI.mat'])
measurement = {'NAc Power','BLA Power','Coherence'};

%% find indices that overlap with interaction intervals
% TSRatEmptyNeural;
%
% for rr = 1:size(TSRatEmpty,1)
%     for ss = 1:size(TSRatEmpty,2)
%         for p = 1:size(TSRatEmpty,3)
%
% idx = [];
% for ii = 1:size(TSRatEmptyNeural{r,s,p,1},1)
%     idx = [idx find(toi>= TSRatEmptyNeural{r,s,p,1}(ii,1) & toi <=TSRatEmptyNeural{r,s,p,1}(ii,2))];
% end
% powspctrm = squeeze(nansum(freq.powspctrm,1));
% NAcRatPow = squeeze(powspctrm(1,:,idx));
% NAcRatPow = 10.*log10(NAcRatPow);
%
% NAcRatPow(NAcRatPow == -inf) = NaN;
% NAcRatPowMean = nanmean(NAcRatPow,2);
% plot(NAcRatPowMean,'XData',foi)
%% use try-catch when running unsupervised batches

SumbyPhase = zeros(size(ratT,1), numel(sessions), numel(phases), 3, numel(foi));
% dimord: [day, phase, brain region, foi]
% brain regions: 1: NAc, 2: BLA, 3: Coherence
% interaction target: 1: Rat, 2: Empty
outputfile = [datafolder 'NeuralPower_noTarget.mat'];

for r = 1:size(ratT,1)
    if r == 6
    else
        %
        %
        %     end
        %     if exist(outputfile,'file')
        %         disp(['Outputfile for ' int2str(ratT.ratnums(r)) 'already exists. Skip!'])
        %     else
        for s = 1:numel(sessions)
            if r == 19 && s == 2
            else
                for p = 1:3 %numel of phases per day
                    
                    inputfile = [datafolder 'target' filesep 'mtmconvol_powandcoh_Linspace' ...
                        int2str(ratT.ratnums(r)) 'D' int2str(s) '_P' int2str(p) '.mat'];
                    
                    if exist(inputfile,'file')
                        %% load data
                        load(inputfile);
                        
                        %% find time indicies that matched with interaction intervals  
                            for mea = 1:3
                                if mea < 3
                                    figure(mea)
                                    SumbyPhase(r,s,p,mea,:) = 10.*log10(squeeze(nanmean(freq.powspctrm(mea,:,:),3)));
                                    plot(squeeze(SumbyPhase(r,s,p,mea,:)),'XData',foi,'LineWidth',2)
                                    xlabel('Frequency (Hz)')
                                    ylabel('dB')
                                    title(['Rat #' int2str(ratT{r,1}) ' ' regions{mea} ' power, Day ' ...
                                        int2str(s) ' Phase ' int2str(p)])
                                    set(gca, 'FontName', 'Arial', 'FontSize', 16,'FontWeight','bold');
                                    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
                                    print([figurefolder int2str(ratT.ratnums(r)) ...
                                        'SI_PowerSpectra_' int2str(s) ...
                                        '_P' int2str(p) regions{mea}],'-dpng');
                                    
                                elseif mea == 3
                                    figure(mea)
                                    SumbyPhase(r,s,p,mea,:) = squeeze(nanmean(freq.cohspctrm(1,:,:),3));
                                    plot(squeeze(SumbyPhase(r,s,p,mea,:)),'XData',foi,'LineWidth',2)
                                    xlabel('Frequency (Hz)')
                                    ylabel('Coherence')
                                    ylim([0.5 1])
                                    title(['Rat #' int2str(ratT{r,1}) ' Coherence, Day ' ...
                                        int2str(s) ' Phase ' int2str(p)])
                                    set(gca, 'FontName', 'Arial', 'FontSize', 16,'FontWeight','bold');
                                    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
                                    print([figurefolder int2str(ratT.ratnums(r)) ...
                                        'SI_Coherence_' int2str(s) ...
                                        '_P' int2str(p)],'-dpng');
                                    
                                end   
                            end
                            %% plot
                            
                            %
                        
                        %                         for s = 1:numel(sessions)
                        %                             for rr = 1:2 % 1: NAc. 2: BLA
                        %                                 figure(rr)
                        %                                 clf
                        %                                 hold off
                        %                                 for p = 1:3 %numel of phases per day
                        %                                     figure(rr)
                        %                                     hold on
                        %                                     temp = powSum(s,p,rr,:,:);
                        %                                     temp(temp == -inf) = NaN;
                        %                                     plot(squeeze(nanmean(temp(1,1,1,:,:),5)),'XData',foi)
                        %                                 end
                        %                                 figure(rr)
                        %                                 legend('Phase 1', 'Phase 2', 'Phase 3')
                        %                                 title(['Rat ' int2str(ratT.ratnums(r)) 'D' int2str(s) ' ' regions{rr} ' Power']);
                        %                                 xlabel('Frequency (Hz)');
                        %                                 ylabel('dB')
                        %                                 set(gca, 'FontName', 'Arial', 'FontSize', 12);
                        %                                 set(gcf,'PaperUnits','inches','PaperPosition',[0 0 16 12])
                        %                                 print([figurefolder int2str(ratT.ratnums(r)) ...
                        %                                     'PowerSpectra_D' int2str(s) regions{rr}],'-dpng');
                        %                             end
                        %                         end
                    end
                end
            end
        end
    end
    
end
cfg =[];
cfg.freq = freq.freq;
cfg.time = freq.time;
cfg.dimord = 'rat_session_phase_measurement_frequency';
cfg.labelcmb = freq.labelcmb;
save(outputfile,'SumbyPhase', 'cfg')


%% group by treatment

thetaIdx = find(cfg.freq >= 5 & cfg.freq <= 8);
gammaIdx = find(cfg.freq >= 50 & cfg.freq <= 100);

%% group analysis
groupSize = [numel(SALgroup); numel(VPAgroup)];

SALCoh = NaN(groupSize(1), numel(sessions), numel(phases), numel(cfg.freq));
SALBLAPow = NaN(groupSize(1), numel(sessions), numel(phases), numel(cfg.freq));
SALNAcPow = NaN(groupSize(1), numel(sessions), numel(phases), numel(cfg.freq));

VPACoh = NaN(groupSize(2), numel(sessions), numel(phases), numel(cfg.freq));
VPABLAPow = NaN(groupSize(2), numel(sessions), numel(phases), numel(cfg.freq));
VPANAcPow = NaN(groupSize(2), numel(sessions), numel(phases), numel(cfg.freq));


%% reorganize data by group

SALcount = 1;
VPAcount = 1;
dim = size(SumbyPhase);

% SumTargetLog = log10(SumTarget).*10;
% plot(squeeze(nanmean(SumTargetLog(:,1,1,1,1,:),1)))

for ii = 1:size(ratT,1)
    if SALInd(ii) == 1
        
        SALCoh(SALcount,:,:,:,:) = reshape(SumbyPhase(ii,:,:,3,:), 1, dim(2), dim(3) ,dim(5));
        SALNAcPow(SALcount,:,:,:,:) = reshape(SumbyPhase(ii,:,:,1,:), 1, dim(2), dim(3) ,dim(5));
        SALBLAPow(SALcount,:,:,:,:) = reshape(SumbyPhase(ii,:,:,2,:), 1, dim(2), dim(3) ,dim(5));
        SALcount = SALcount +1;
        
    elseif VPAInd(ii) ==1
        
        VPACoh(VPAcount,:,:,:,:) = reshape(SumbyPhase(ii,:,:,3,:), 1, dim(2), dim(3) ,dim(5));
        VPANAcPow(VPAcount,:,:,:,:) = reshape(SumbyPhase(ii,:,:,1,:), 1, dim(2), dim(3) ,dim(5));
        VPABLAPow(VPAcount,:,:,:,:) = reshape(SumbyPhase(ii,:,:,2,:), 1, dim(2), dim(3) ,dim(5));
        VPAcount = VPAcount +1;
    else
        error(["Rat " int2str(ii) " has error on treatment labeling"]);
    end
end
SALCoh(3,:,:,:,:) = [];
SALNAcPow(3,:,:,:,:) = [];
SALBLAPow(3,:,:,:,:) = [];

%% plot averaged group results
SALNAcPowAvg = squeeze(nanmean(SALNAcPow,1));
SALBLAPowAvg = squeeze(nanmean(SALBLAPow,1));
SALCohAvg = squeeze(nanmean(SALCoh,1));

VPANAcPowAvg = squeeze(nanmean(VPANAcPow,1));
VPABLAPowAvg = squeeze(nanmean(VPABLAPow,1));
VPACohAvg = squeeze(nanmean(VPACoh,1));
for tt = 1:numel(treatment)
    for mm = 1:3
        if tt == 1 && mm == 1
            data2plot = SALNAcPowAvg;
        elseif tt == 1 && mm == 2
            data2plot = SALBLAPowAvg;
        elseif tt == 1 && mm == 3
            data2plot = SALCohAvg;  
        elseif tt == 2 && mm == 1
            data2plot = VPANAcPowAvg;
        elseif tt == 2 && mm == 2
            data2plot = VPABLAPowAvg;
        elseif tt == 2 && mm == 3
            data2plot = VPACohAvg;
        end
        for session = 1:numel(sessions)
            figure(session)
            clf
            hold on
            label = cell(3,1);
            for pp = 1:numel(phases)
                    plot(squeeze(data2plot(session,pp,:)),'XData',cfg.freq,'LineWidth',2)
                    label{pp} = ['Phase ' int2str(pp)];
            end
            hold off
            legend(label)
            xlabel('Frequency (Hz)')
            if mm < 3
               ylabel('dB')
            else
               ylabel('Coherence')
               ylim([0.5 1])
            end
            
            title(['Average ' measurement{mm} ' of ' treatment{tt} ...
                ' group, Day ' int2str(session)])
            set(gca, 'FontName', 'Arial', 'FontSize', 16,'FontWeight','bold');

            set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 8])
            print([figurefolder 'Avg ' measurement{mm} '_' treatment{tt} ...
                '_Day' int2str(session) '_noTarget'],'-dpng');
        end
    end
end
dimord = 'day_phase_target';
SALCohTheta = squeeze(nanmean(SALCoh(:,:,:,thetaIdx),4));
SALBLAPowTheta = squeeze(nanmean(SALBLAPow(:,:,:,thetaIdx),4));
SALNAcPowTheta = squeeze(nanmean(SALNAcPow(:,:,:,thetaIdx),4));

VPACohTheta = squeeze(nanmean(VPACoh(:,:,:,thetaIdx),4));
VPABLAPowTheta = squeeze(nanmean(VPABLAPow(:,:,:,thetaIdx),4));
VPANAcPowTheta = squeeze(nanmean(VPANAcPow(:,:,:,thetaIdx),4));

SALCohGamma = squeeze(nanmean(SALCoh(:,:,:,gammaIdx),4));
SALBLAPowGamma = squeeze(nanmean(SALBLAPow(:,:,:,gammaIdx),4));
SALNAcPowGamma = squeeze(nanmean(SALNAcPow(:,:,:,gammaIdx),4));

VPACohGamma = squeeze(nanmean(VPACoh(:,:,:,gammaIdx),4));
VPABLAPowGamma = squeeze(nanmean(VPABLAPow(:,:,:,gammaIdx),4));
VPANAcPowGamma = squeeze(nanmean(VPANAcPow(:,:,:,gammaIdx),4));

save([datafolder 'SIgroupThetanoTarget.mat'], 'SALCohTheta' ,'SALBLAPowTheta' ,'SALNAcPowTheta' ...
    ,'VPACohTheta','VPABLAPowTheta','VPANAcPowTheta', 'dimord')
save([datafolder 'SIgroupGammanoTarget.mat'], 'SALCohGamma' ,'SALBLAPowGamma' ,'SALNAcPowGamma' ...
    ,'VPACohGamma','VPABLAPowGamma','VPANAcPowGamma', 'dimord')

%% Theta

SALD1CohTheta = squeeze(SALCohTheta(:,1,:));
SALD2CohTheta = squeeze(SALCohTheta(:,2,:));

SALD1BLAPowTheta = squeeze(SALBLAPowTheta(:,1,:));
SALD2BLAPowTheta = squeeze(SALBLAPowTheta(:,2,:));

SALD1NAcPowTheta = squeeze(SALNAcPowTheta(:,1,:));
SALD2NAcPowTheta = squeeze(SALNAcPowTheta(:,2,:));

VPAD1CohTheta = squeeze(VPACohTheta(:,1,:));
VPAD2CohTheta = squeeze(VPACohTheta(:,2,:));

VPAD1BLAPowTheta = squeeze(VPABLAPowTheta(:,1,:));
VPAD2BLAPowTheta = squeeze(VPABLAPowTheta(:,2,:));

VPAD1NAcPowTheta = squeeze(VPANAcPowTheta(:,1,:));
VPAD2NAcPowTheta = squeeze(VPANAcPowTheta(:,2,:));

save([datafolder 'SIgroupThetaTarget_FLAT.mat'], 'SALD1CohTheta' ,'SALD2CohTheta' ,...
    'SALD1BLAPowTheta','SALD2BLAPowTheta','SALD1NAcPowTheta','SALD2NAcPowTheta',...
    'VPAD1CohTheta','VPAD2CohTheta','VPAD1BLAPowTheta',...
    'VPAD2BLAPowTheta','VPAD2BLAPowTheta','VPAD2NAcPowTheta')

%% Gamma

GammaSALD1Coh = squeeze(SALCohGamma(:,1,:));
GammaSALD2Coh = squeeze(SALCohGamma(:,2,:));

GammaSALD1BLAPow = squeeze(SALBLAPowGamma(:,1,:));
GammaSALD2BLAPow = squeeze(SALBLAPowGamma(:,2,:));

GammaSALD1NAcPow = squeeze(SALNAcPowGamma(:,1,:));
GammaSALD2NAcPow = squeeze(SALNAcPowGamma(:,2,:));

GammaVPAD1Coh = squeeze(VPACohGamma(:,1,:));
GammaVPAD2Coh = squeeze(VPACohGamma(:,2,:));

GammaVPAD1BLAPow = squeeze(VPABLAPowGamma(:,1,:));
GammaVPAD2BLAPow = squeeze(VPABLAPowGamma(:,2,:));

GammaVPAD1NAcPow = squeeze(VPANAcPowGamma(:,1,:));
GammaVPAD2NAcPow = squeeze(VPANAcPowGamma(:,2,:));

save([datafolder 'SIgroupGammaTarget_FLAT.mat'], 'GammaSALD1Coh' ,'GammaSALD2Coh' ,...
    'GammaSALD1BLAPow','GammaSALD2BLAPow','GammaSALD1NAcPow','GammaSALD2NAcPow',...
    'GammaVPAD1Coh','GammaVPAD2Coh','GammaVPAD1BLAPow',...
    'GammaVPAD2BLAPow','GammaVPAD2BLAPow','GammaVPAD2NAcPow')