% %% Exploring the effect of different local refencing on SI data
% % look on the effect of social referencing on social interaction on power
% % coherence and CFC calculation.
% 
% inputfile = [datafolder filesep 'LFPTrialData_4s_' ...
% int2str(ratT.ratnums(r)) 'Merged.mat'];
% 
% load(inputfile)
% srate = 1000;
% trialLength = 4096;
% %% Plot raw data
% figure
% s1 = subplot(4,1,1);
% plot(data.trial{1,1}(2,:))
% title('Channel 2 alone')
% xlim([-0 4000])
% 
% s2 = subplot(4,1,2);
% plot(mean(data.trial{1,1}(1:8,:),1))
% title('averaage singal of channel 1-8')
% xlim([-0 4000])
% 
% s3 = subplot(4,1,3);
% plot(data.trial{1,1}(2,:)-mean(data.trial{1,1}(1:8,:),1))
% title('Channel 2 after local referencing')
% xlim([-0 4000])
% 
% s4 = subplot(4,1,4);
% plot(data.trial{1,1}(2,:)-mean(data.trial{1,1}(1:8,:),1))
% title('Channel 2 after local referencing')
% ylim([-100 100])
% xlim([-0 4000])
% 
% suptitle({['Local Referencing'];['^{Rat 361 first D1P1 first 4-s}']})
% 
% linkaxes([s1,s2,s3],'xy')
% 
% %% Plot spectrogram
% 
% % wavelet parameters
% min_frex  =   2;
% max_frex  = 140;
% num_frex  =  50;
% nCycRange = [4 20];
% nData = 4096;
% sRate = 1000;
% frex = logspace(log10(min_frex),log10(max_frex),num_frex);
% 
% 
% 
% % create wavelets
% cmwX = createMorletWavelet(min_frex,max_frex,num_frex,nCycRange,nData,sRate);
% 
% 
% 
% figure(1), clf
% subplot(211)
% contourf(squeeze(mean(tf(chans2plot,:,:))))
% set(gca,'clim',clim)
% 
% 
% %% setup convolution parameters
% 
% 
% channel2use = 2;
% 
% % baseline_window = [ -500 -200 ];
% 
% % set a few different wavelet widths (number of wavelet cycles)
% num_cycles = [ 2 6 8 25 ];
% 
% % other wavelet parameters
% frequencies = linspace(min_freq,max_freq,num_frex);
% time = -2:1/srate:2;
% half_wave_size = (length(time)-1)/2;
% 
% % FFT parameters
% n_wavelet     = length(time);
% n_data        = trialLength;
% n_convolution = n_wavelet+n_data-1;
% 
% % initialize output time-frequency data
% tf_data = zeros(length(num_cycles),length(frequencies),trialLength);
% 
% 
% % FFT of data (doesn't change on frequency iteration)
% fft_data = fft(reshape(data.trial{1,1}(2,:),1,trialLength),n_convolution);
% 
% % loop over cycles
% for cyclei=1:length(num_cycles)
%     
%     for fi=1:length(frequencies)
%         
%         % create wavelet and get its FFT
%         s = num_cycles(cyclei)/(2*pi*frequencies(fi));
%         
%         wavelet  = exp(2*1i*pi*frequencies(fi).*time) .* exp(-time.^2./(2*s^2));
%         waveletX = fft(wavelet,n_convolution);
%         waveletX = waveletX./max(waveletX);
%         
%         % run convolution
%         convolution_result_fft = ifft(waveletX.*fft_data,n_convolution);
%         convolution_result_fft = convolution_result_fft(half_wave_size+1:end-half_wave_size);
%         
%         % put power data into big matrix
%         tf_data(cyclei,fi,:) = abs(convolution_result_fft).^2;
%     end
%     
%     % db conversion
%     tf_data(cyclei,:,:) = 10*log10(squeeze(tf_data(cyclei,:,:)));
%     
% end
% 
% figure(1), clf
% for cyclei=1:length(num_cycles)
%     subplot(2,2,cyclei)
%     
%     contourf(1:4096,frequencies,squeeze(tf_data(cyclei,:,:)),40,'linecolor','none')
%     set(gca,'clim',[-1 1]*40,'ydir','normal')
%     title([ 'Wavelet with ' num2str(num_cycles(cyclei)) ' cycles' ])
% end
% colormap jet
% 
% 
% 
% 
% 
% % create Morlet wavelet
% time      = -1:1/1000:1;
% frequency = 10;
% wavelet   = exp(2*1i*pi*frequency.*time) .* exp(-time.^2./(2*(8/(2*pi*frequency))^2)) /frequency;
% 
% % define lengths of various vectors
% half_wave_size = (length(wavelet)-1)/2;
% n_wavelet      = length(wavelet);
% n_data         = length(data.trial{1,1}(2,:));
% n_convolution  = n_wavelet+n_data-1;
% 
% % FFT of wavelet and normalize in the frequency domain
% fft_wavelet = fft(wavelet,n_convolution);
% fft_wavelet = fft_wavelet./max(fft_wavelet);
% 
% % FFT of EEG data
% fft_data = fft(squeeze(data.trial{1,1}(2,:)),n_convolution);
% 
% % and take inverse FFT
% convolution_result =  ifft(fft_wavelet.*fft_data);
% 
% % cut off edges
% convolution_result = convolution_result(half_wave_size+1:end-half_wave_size);
% 
% xlim = [ 0 4096 ];
% 
% figure(1), clf
% 
% subplot(311)
% plot(data.time{1,1},real(convolution_result));
% % set(gca,'xlim',data.time{1,1})
% xlabel('Time (ms)'), ylabel('Voltage (\muV)')
% title([ 'Projection onto real axis is filtered data at ' num2str(frequency) ' Hz' ])
% 
% subplot(312)
% plot(data.time{1,1},abs(convolution_result).^2);
% % set(gca,'xlim',xlim)
% xlabel('Time (ms)'), ylabel('Voltage (\muV^2/Hz)')
% title([ 'Magnitude squared is power at ' num2str(frequency) ' Hz' ])
% 
% subplot(313)
% plot(data.time{1,1},angle(convolution_result));
% % set(gca,'xlim',xlim)
% xlabel('Time (ms)'), ylabel('Phase (radians)')
% title([ 'Vector angle is phase angle time series at ' num2str(frequency) ' Hz' ])
% 
% 
% %% Plot coherogram

%% Plot crosscorrelogram
plt = 'y';                        % plot CFC
hx = waitbar(0,'Getting started...');

% set analysis parameters

foi_phase = 2:1.5:15.5;   % central frequencies for phase modulation
w_phase = 1.5;              % width of phase band (foi +/- w)
foi_amp = 30:10:120;         % central frequencies for modulated amplitude
w_amp = 10;                  % width of amplitude band (foi +/- w)

%% make 2d Cross-Frequency Modulation plots for each rat/session/interval

for r = 1:6
    % Full, artifact-free interaction intervals marked with
    % data.trialinfo(:,1) = original trial number, and
    % data.trialinfo(:,2) = number representing condition
    inputfile = ['/Users/hurdleman/Documents/ePhysDataLocal' filesep ... 
        'LFPTrialData_4s_' int2str(ratT.ratnums(r)) 'Merged.mat'];
    
    outputfile = ['/Users/hurdleman/Documents/ePhysDataLocal' filesep ...
        'crossKL_MI2dz_' num2str(ratT.ratnums(r)) '_4s.mat'];
    
    if ~exist(inputfile,'file')
        error('inputfile does not exist');
    end
    
    if exist(outputfile,'file')
        disp(['Data for Rat # ' int2str(ratT.ratnums(r)) ', ' ...
            ' already analyzed!']);
        if plt == 'y'
            disp('Loading previous analysis results from file.');
            load(outputfile);
        end
        
    else
        %% preallocate memory for variables
        
        zscore2d = cell(size(sessions,2),numel(phases),1);
        pval2d = cell(size(sessions,2),numel(phases),1);
        h2d = cell(size(sessions,2),numel(phases),1);
        MI2d = cell(size(sessions,2),numel(phases),1);
        phasepref2d = cell(size(sessions,2),numel(phases),1);
        
        cmax = 0;
        
        %% load data
        
%         load(inputfile);
        
        chIdx = 1:16;
        chNAc = chIdx(strcmp(data.label,ratT{r,4}));
        chBLA = chIdx(strcmp(data.label,ratT{r,5}));
        
        %% extract trialinfo
        
        info = struct;
        info.rat = ratT(r,:);
        info.trialinfo = data.trialinfo;
        info.time = data.time;
        info.cfg = data.cfg;
        info.triallength = data.cfg.length;
        info.subtrial = cell(size(sessions,2),numel(phases),1);
        % the starting and end trial number of the first, second and third 5 minute of each phase.
        
        info.subtrialts = cell(size(sessions,2),numel(phases),1);
        
        %% analyze data
        
        for s = 1:size(sessions,2)
            for p = 1:numel(phases)
                trials = find(data.trialinfo(:,2) == s & ...
                    data.trialinfo(:,1) == p);
                trialts = NaN(numel(trials),2); %col 1/2 start/end ts of each trial
                
                % find start& end timestamp for each trials
                for ts = 1:numel(trials)
                    trialts(ts,1) = info.time{1,trials(ts)}(1);
                    trialts(ts,2) = info.time{1,trials(ts)}(end);
                end
                info.subtrialts{s,p} = trialts;
                
                % find the trial # of 1st/2nd/3rd 5 mins of the phase
                for subt = 1:3 % Three 5 minutes of each 15 min phase
                    info.subtrial{s,p}(subt,1) = find(trialts(:,1) >= 300*(subt-1),1);
                    info.subtrial{s,p}(subt,2) = find(trialts(:,2) < 300*(subt)+1,1,'last');
                end
                
                if isempty(trials)
                    warning(['No data found for Rat # ' ...
                        num2str(ratT.ratnums(r)) ', Day' int2str(s) ...
                        ' Phase_' int2str(p) '.  Skipping.']);
                else
                     for t = 1      %:numel(trials)
                        waitbar(t/numel(trials),hx,...
                            {['Calculating 2d cross-region MI z-score for Rat # ' ...
                            num2str(ratT.ratnums(r)) ',']; ...
                            ['Day' int2str(s) ...
                            ' Phase ' int2str(p) '.']});
                        % compute CFC for all the trials within a phase
                        [zscore2dtemp,pval2dtemp,h2dtemp,MI2dtemp,...
                            phasepref2dtemp] = sig_crossKL_MI2d_TEM(...
                            data.trial{trials(t)}(chNAc,:),...
                            data.trial{trials(t)}(chBLA,:),...
                            foi_phase,w_phase,foi_amp,w_amp);
                        for pr = 1:2 % each phase modulating region
                            for ar = 1:2 % each amp modulated region
                                zscore2d{s,p}{pr,ar}(:,:,t) = ...
                                    zscore2dtemp{pr,ar};
                                pval2d{s,p}{pr,ar}(:,:,t) = ...
                                    pval2dtemp{pr,ar};
                                h2d{s,p}{pr,ar}(:,:,t) = ...
                                    h2dtemp{pr,ar};
                                MI2d{s,p}{pr,ar}(:,:,t) = ...
                                    MI2dtemp{pr,ar};
                                phasepref2d{s,p}{pr,ar}(:,:,t) = ...
                                    phasepref2dtemp{pr,ar};
                            end
                        end
                    end
                    cmax = max(max(max(mean(...
                        zscore2d{s,p}{pr,ar},3))),cmax);
                end
            end
        end
        
        %% save analysis results
        
        disp(['writing results to file ' outputfile]);
        save(outputfile, '*2d', 'cmax', 'foi*', 'w*', 'info', '-v6');
        
    end
    
    %% plot results
    
    if plt == 'y'
        for s = 1:size(sessions,2)
            for p = 1:numel(phases)
                if isempty(zscore2d{int})
                    warning(['No ' intervals{int} ...
                        ' intervals found for Rat # ' ...
                        num2str(ratT.ratnums(r)) ', Day' int2str(s) ...
                        ' Phase_' int2str(p) '.  Skipping.']);
                else
                    for subt = 1:3 % first/second/third 5 mins of phase
                        for pr = 1:2 % each phase modulating region
                            if pr == 1
                                prlabel = 'NAc';
                            else
                                prlabel = 'BLA';
                            end
                            
                            for ar = 1:2 % each amp modulated region
                                if ar == 1
                                    arlabel = 'NAc';
                                else
                                    arlabel = 'BLA';
                                end
                                
                                %% plot average z-score
                                
                                figure(pr+(ar-1)*2); clf;
                                imagesc(foi_phase, foi_amp, mean(...
                                    zscore2d{s,p}{pr,ar}(:,:,...
                                    info.subtrial{s,p}(subt,1):...
                                    info.subtrial{s,p}(subt,2)),3)');
                                axis xy;
                                set(gca, 'FontName', 'Arial', 'FontSize', 18);
                                xlabel('Phase Frequency (Hz)');
                                ylabel('Envelope Frequency (Hz)');
                                title({['Rat # ' num2str(ratT.ratnums(r)) ', D' ...
                                    int2str(s) 'P' char(phases(p)) ' ' ...
                                    int2str(subt) '/3']; ...
                                    [prlabel ' phase modulating ' ...
                                    arlabel ' amplitude']; ...
                                    ['Average of ' ...
                                    num2str(info.subtrial{s,p}(subt,2)...
                                    -info.subtrial{s,p}(subt,1)+1) ...
                                    ' Interactions']});
                                caxis([0 cmax]); colormap(jet); colorbar;
                                set(gcf,'WindowStyle','docked');
                                saveas(gcf,[figurefolder 'crossKL_MI2dz_'...
                                    num2str(ratT.ratnums(r)) 'D' ...
                                    int2str(s) 'P' char(phases(p)) '_' prlabel 'PhaseMod' ...
                                    arlabel 'Amp' int2str(subt) '.png'])
                                
                                %% plot phase preference
                                
                                figure(pr+(ar-1)*2+4); clf;
                                imagesc(foi_phase, foi_amp, circ_mean(circ_ang2rad(...
                                    phasepref2d{int}{pr,ar}(:,:,...
                                    info.subtrial{s,p}(subt,1):...
                                    info.subtrial{s,p}(subt,2))),[],3)');
                                axis xy;
                                set(gca, 'FontName', 'Arial', 'FontSize', 18);
                                xlabel('Phase Frequency (Hz)');
                                ylabel('Envelope Frequency (Hz)');
                                title({['Rat # ' num2str(ratT.ratnums(r)) ', ' ...
                                    char(phases(p)) ', ' intervals{int}]; ...
                                    [prlabel ' phase modulating ' ...
                                    arlabel ' amplitude']; ...
                                    ['Average of  ' ...
                                    num2str(size(zscore2d{int}{pr,ar},3)) ...
                                    ' Interactions']});
                                caxis([-pi pi]); colormap(hsv); colorbar;
                                set(gcf,'WindowStyle','docked');
                                saveas(gcf,[figurefolder 'Phase_crossKL_MI2dz_'...
                                    num2str(ratT.ratnums(r)) 'D' ...
                                    int2str(s) 'P' char(phases(p)) '_' prlabel 'PhaseMod' ...
                                    arlabel 'Amp' int2str(subt) '.png'])
                                
                            end
                        end
                    end
                end
            end
        end
    end
end
close(hx)

