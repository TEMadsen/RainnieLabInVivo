%% experiment metadata

% each row # here corresponds to cell row (1st dim) in other variables
ratnums = [361; 362; 363; 364; 377; 378; 379; 380];
excluded = logical([0; 0; 0; 0; 0; 0; 0; 0]);   % see notes for why
RatSideD1 = {'Right';'Right';'Left';'Left';'Right';'Right';'Left';'Left'};
RatSideD2 = {'Left';'Left';'Right';'Right';'Left';'Left';'Right';'Right'};
ratT = table(ratnums, excluded, RatSideD1, RatSideD2);
clearvars ratnums excluded RatSideD1 RatSideD2

%% import data

% add information of rat/cage
{8, 2, 3}

%% calculate the length of each interaction interval

%% calculate sum of interaction intervals
%eliminate red cells ()
%% 
