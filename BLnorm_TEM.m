function freq = BLnorm_TEM(cfg,freq,BLfreq,compare)
% BLnorm_TEM - performs baseline "normalization" for time-frequency data
%   and/or transforms it according to preferences specified in cfg
%   structure as follows (similar to those of ft_freqbaseline):
%
%     cfg.parameter = field of freq for which to apply transformation
%       and/or baseline normalization, or cell array of strings to specify
%       multiple fields (default = 'powspctrm')
%
%   The time period to be used as baseline can be specified as:
%     cfg.baseline = [begin end] (time range within each trial, e.g.,
%           pre-stimulus period - will be passed to FieldTrip's
%           ft_freqbaseline for now and be fully implemented later),
%       'BLfreq' (all time in optional 3rd input variable, typically output
%           of ft_freqanalysis for a separate baseline recording), or
%       'no' (default)
%
%     cfg.tol = scalar in same units as frequency (e.g., Hz), max allowable
%           difference between baseline & freq frequency bins, defaults to
%           half the smallest distance between frequencies within either
%           scale
%
%   Different measures can be used to represent the central tendency of the
%       baseline freq data:
%     cfg.baselinecent = 'mean' (default) or 'median'
%
%   The type of baseline "normalization" can be specified as:
%     cfg.baselinetype = 'absolute' (default), 'relative', 'relchange', or
%           'vssum' (each as defined by FieldTrip), or
%       'z-score' (the output freq data is expressed relative to the
%           variance of the baseline freq data, in terms of standard
%           deviations above or below the mean within each frequency)
%         Note:  ft_freqbaseline's cfg.baselinetype = 'db' is equivalent to
%           this function's:                cfg.baselinecent = 'mean';
%                                           cfg.baselinetype = 'relative';
%                                           cfg.transtype = 'dB';
%                                           cfg.transorder = 'after';
%
%   The freq data can be optionally transformed using any of these methods:
%     cfg.transtype = 'no' (default), 'dB', 'sqrt' (experimental, since
%           units of power spectra are V^2, but it doesn't do a very good
%           job of normalizing the distribution), 'cuberoot' (TO BE ADDED -
%           or maybe change to scalar representing nthroot), or 'ln'
%           (experimental, as an arbitrary way to normalize the
%           distribution of power values over time before using mean and/or
%           std, could change to scalar log base anything)
%
%   Specify whether to perform the transformation before or after baseline
%       "normalization" (only relevant if both procedures are requested):
%     cfg.transorder = 'before' or 'after' (default) -- I think it makes
%           more sense to transform the data (to achieve a more normal
%           distribution of values) BEFORE "normalizing" it relative to
%           baseline because the mean is not a good representation of the
%           central tendency for the skewed distribution of power values
%           over time.  However, I understand that FieldTrip's
%           cfg.baselinetype = 'db'; is a standard decibel conversion of
%           the power ratio (V^2/V^2).
%
%   The 2nd input variable "freq" can be loaded from a file, using:
%     cfg.inputfile     = full path string
%   The 3rd input variable "BLfreq" can be loaded from a file, using:
%     cfg.baselinefile  = full path string
%   The output variable "freq" can be saved to a file, using:
%     cfg.outputfile    = full path string
%
%   If freq.(cfg.parameter) is complex, only the absolute value (e.g., magnitude
%       of coherence) will be retained.  (Should consider using mean resultant
%       vector or some similar circular statistic.)
%   The optional 4th input "compare" is a boolean flag to compare the
%       distributions of raw & normalized data.
%
% written Nov. 4, 2016, by Teresa E. Madsen, Ph.D.

%% check basic inputs, apply defaults

if nargin < 2 || isempty(freq)
  if isfield(cfg,'inputfile') && ~isempty(cfg.inputfile)
    if ~wait4server_TEM(cfg.inputfile)
      error('Server connection lost')
    end
    
    S = load(cfg.inputfile);
    varname = fieldnames(S);  % may be freq (for pow) or stat (for coh)
    freq = S.(varname{1});  % there should only be 1
    S = []; %#ok<NASGU> clearing memory
  else
    error('2nd input variable freq or cfg.inputfile is required')
  end
end

freq = rmvlargefields_TEM(freq);  % clear historical metadata

if ~isfield(cfg,'parameter') || isempty(cfg.parameter)
  cfg.parameter = 'powspctrm'; % default
end

assert(isfield(freq,cfg.parameter));
if ~isreal(freq.(cfg.parameter))
  warning('input freq.(cfg.parameter) is complex - only abs(freq.(cfg.parameter)) is normalized')
  freq.(cfg.parameter) = abs(freq.(cfg.parameter));
end

dimord = strsplit(freq.dimord,'_');
assert(isequal(numel(dimord),ndims(freq.(cfg.parameter))));
timedim = find(strcmpi('time',dimord));   % identify time dim
if isempty(timedim)
  error('no time dimension in freq input - implement alternative?')
end

if nargin < 4 || isempty(compare)
  compare = false;
else
  compare = istrue(compare);
end

%% check baseline related inputs

if ~isfield(cfg,'baseline') || isempty(cfg.baseline) || ...
    (ischar(cfg.baseline) && strncmpi(cfg.baseline,'no',2))
  BL = false;   % default
else
  BL = true;  % flag indicating baseline "normalization" is requested
  if ~isfield(cfg,'baselinecent') || isempty(cfg.baselinecent)
    cfg.baselinecent = 'mean';  % default
  end
  if ~isfield(cfg,'baselinetype') || isempty(cfg.baselinetype)
    cfg.baselinetype = 'absolute';  % default
  end
  if isnumeric(cfg.baseline) && ~isscalar(cfg.baseline)
    forward = true;  % flag indicating that ft_freqbaseline will be used
    warning('Processing will be forwarded to ft_freqbaseline.')
  else
    forward = false;
    if strcmp(cfg.baseline,'BLfreq')
      if ~isfield(cfg,'tol') || isempty(cfg.tol)
        cfg.tol = min(min(diff([freq.freq; BLfreq.freq],1,2)))/2; % default value
      end
      
      if nargin < 3 || isempty(BLfreq)
        if isfield(cfg,'baselinefile') && ~isempty(cfg.baselinefile)
          if ~wait4server_TEM(cfg.baselinefile)
            error('Server connection lost')
          end
          
          S = load(cfg.baselinefile);
          varname = fieldnames(S);  % may be freq (for pow) or stat (for coh)
          BLfreq = S.(varname{1});  % there should only be 1
          S = []; %#ok<NASGU> clearing memory
        else  % include [] as 2nd input arg if using cfg.inputfile & last input arg is BLfreq
          error('3rd input variable BLfreq or cfg.baselinefile is required when cfg.baselinetype = ''BLfreq'';')
        end
      end
      
      assert(isfield(BLfreq,cfg.parameter));
      if ~isreal(BLfreq.(cfg.parameter))
        warning('input BLfreq.(cfg.parameter) is complex - only abs(BLfreq.(cfg.parameter)) will be used')
        BLfreq.(cfg.parameter) = abs(BLfreq.(cfg.parameter));
      end
      
      if strcmp(cfg.parameter,'cohspctrm')
        assert(isequal(freq.labelcmb,BLfreq.labelcmb));
      else
        assert(isequal(freq.label,BLfreq.label));
      end
      assert(max(abs(diff([freq.freq; BLfreq.freq],1,1))) < cfg.tol)
      
      BLdimord = strsplit(BLfreq.dimord,'_');
      assert(isequal(numel(BLdimord),ndims(BLfreq.(cfg.parameter))));
      % make sure baseline is formatted as 1 long trial
      if any(strcmp(BLdimord,'rpt'))
        nBLt = size(BLfreq.(cfg.parameter),find(strcmp(BLdimord,'rpt')));
        if nBLt > 1
          assert(find(strcmp(BLdimord,'rpt')) == 1 && ...
            find(strcmp(BLdimord,'time')) == numel(BLdimord), ... % otherwise...
            'unexpected dimord')
          BLfreq.(cfg.parameter) = shiftdim(BLfreq.(cfg.parameter),1);
          dims = size(BLfreq.(cfg.parameter));
          BLfreq.(cfg.parameter) = reshape(BLfreq.(cfg.parameter), ...
            [dims(1:end-2), dims(end-1)*dims(end)]);
          BLfreq.time = BLfreq.time(1):mean(diff(BLfreq.time)):...
            dims(end-1)*dims(end)*mean(diff(BLfreq.time));
          BLdimord = BLdimord(2:end);
          BLfreq.dimord = strjoin(BLdimord,'_');
        end
      end
      
      switch numel(dimord) - numel(BLdimord)
        case 0
          assert(isequal(dimord,BLdimord));
          trldim = find(strcmpi('rpt',dimord));       % identify trial dim
          if ~isempty(trldim)
            assert(isequal(1,size(BLfreq.(cfg.parameter),trldim)));
          end   % there should only be one trial in BLfreq
        case 1  % data may have trials while baseline may not
          trldim = strcmpi('rpt',dimord);             % identify trial dim
          assert(isequal(dimord(~trldim),BLdimord));  % others should match
          trldim = find(trldim);
          
          % add a singleton dimension 1, shifting others right
          BLfreq.(cfg.parameter) = shiftdim(BLfreq.(cfg.parameter),-1);
          if trldim ~= 1  % if that's not the right place, fix it
            dims = 1 + (1:numel(BLdimord));   % new positions of each dim
            BLfreq.(cfg.parameter) = permute(BLfreq.(cfg.parameter), [dims(1:trldim-1) 1 dims(trldim:end)]);
          end   % BLfreq.(cfg.parameter) should now have the same dimord as freq
        otherwise
          error('freq and BLfreq should have the same dimord or differ only by trials')
      end
    end
  end
end

%% check transform related inputs

if ~isfield(cfg,'transtype') || isempty(cfg.transtype) ...
    || strncmpi(cfg.transtype,'no',2)
  trans = false; 	% default
else
  trans = true;
  if BL && (~isfield(cfg,'transorder') || isempty(cfg.transorder))
    cfg.transorder = 'after'; 	% default
  end
end

if ~BL && ~trans
  error('no normalization or transformation requested')
end

%% plot raw distributions, if desired

cfg.skewin = skewness(freq.(cfg.parameter),1,timedim);
% cfg.meanin = mean(freq.(cfg.parameter),timedim,'omitnan');
% cfg.stdin = std(freq.(cfg.parameter),0,timedim,'omitnan');
% if strcmp(cfg.baseline,'BLfreq')
%   cfg.BLskewin = skewness(BLfreq.(cfg.parameter),1,timedim);
%   cfg.BLmeanin = mean(BLfreq.(cfg.parameter),timedim,'omitnan');
%   cfg.BLstdin = std(BLfreq.(cfg.parameter),0,timedim,'omitnan');
% end

if compare
  [minskew,minsi] = min(cfg.skewin(:));
  [minI,minJ] = ind2sub(size(cfg.skewin),minsi);  % assumes timedim & ndims = 3
  subplot(2,2,1);
  histfit(squeeze(freq.(cfg.parameter)(minI,minJ,:)),100);
  title(['min raw skew = ' num2str(minskew) ' @ ' dimord{1} ' ' num2str(minI) ...
    ', ' dimord{2} ' ' num2str(minJ)])
  
  [maxskew,maxsi] = max(cfg.skewin(:));
  [maxI,maxJ] = ind2sub(size(cfg.skewin),maxsi);
  subplot(2,2,2);
  histfit(squeeze(freq.(cfg.parameter)(maxI,maxJ,:)),100); % assumes timedim & ndims = 3
  title(['max raw skew = ' num2str(maxskew) ' @ ' dimord{1} ' ' num2str(maxI) ...
    ', ' dimord{2} ' ' num2str(maxJ)])
  
%   fig1 = gcf;   % remember to fill in other subplots later
%   if strcmp(cfg.baseline,'BLfreq')
%     %   fig = figure; clf; fig.WindowStyle = 'docked';
%     %   fig.Name = 'Raw Baseline Data';
%     %   plotFTmatrix_TEM([],BLfreq);
%   end
%   % fig = figure; clf; fig.WindowStyle = 'docked';
%   % fig.Name = 'Trial Data Before Normalization';
%   % plotFTmatrix_TEM([],freq);
%   figure(fig1);
end

%% if different baselines are used for each trial, send to ft_freqbaseline

if forward  % BL already must be true
  if strncmpi(cfg.baselinetype, 'z', 1) || ...
      ~strncmpi(cfg.baselinecent,'mean',4)
    error('custom normalization options not yet implemented for within trial baselines')
  end
  if trans
    if strcmpi(cfg.baselinecent,'mean') && ...
        strcmpi(cfg.baselinetype,'relative') && ...
        strcmpi(cfg.transtype,'dB') && strcmpi(cfg.transorder,'after')
      % convert to ft_freqbaseline terms & remove trans fields
      cfg.baselinetype = 'db';
      cfg = rmfield(cfg,{'baselinecent','transtype','transorder'});
    else  % any transform but that
      error('data transforms not yet implemented for within trial baselines')
    end
  end
  freq = ft_freqbaseline(cfg, freq);
  warning('Output variable is freq from ft_freqbaseline.')
  return  % output of that function becomes output of this function
end

%% perform baseline "normalization" if desired

if BL
  %% if transform is desired before normalization, transform both now
  if trans && strncmpi(cfg.transorder,'b',1)
    %% transform data
    
    switch cfg.transtype
      case 'dB'
        freq.(cfg.parameter) = 10.*log10(freq.(cfg.parameter));   % transformed trial data
        BLfreq.(cfg.parameter) = 10.*log10(BLfreq.(cfg.parameter));   % transformed baseline data
      case 'sqrt'
        freq.(cfg.parameter) = sqrt(freq.(cfg.parameter));
        BLfreq.(cfg.parameter) = sqrt(BLfreq.(cfg.parameter));
      case 'ln'
        freq.(cfg.parameter) = log(freq.(cfg.parameter));
        BLfreq.(cfg.parameter) = log(BLfreq.(cfg.parameter));
      otherwise
        error('unsupported cfg.transtype')
    end
  end
  
  %% identify central tendencies of baseline data
  
  switch cfg.baselinecent
    case 'mean'
      cbld = mean(BLfreq.(cfg.parameter),timedim,'omitnan');
    case 'median'
      cbld = median(BLfreq.(cfg.parameter),timedim,'omitnan');
    otherwise
      error('unsupported measure for identifying the central tendency of the baseline data')
  end
  
  %% normalize each trial relative to central tendencies of baseline
  
  r = ones(1,numel(dimord));
  if ~isempty(trldim)
    r(trldim) = size(freq.(cfg.parameter),trldim);
  end
  r(timedim) = size(freq.(cfg.parameter),timedim);
  
  rcbld = repmat(cbld,r); % replicate mean baseline over trials & time
  
  %% normalize freq data
  
  switch cfg.baselinetype
    case 'absolute'
      freq.(cfg.parameter) = freq.(cfg.parameter) - rcbld;
    case 'relative'
      freq.(cfg.parameter) = freq.(cfg.parameter) ./ rcbld;
    case 'relchange'
      freq.(cfg.parameter) = (freq.(cfg.parameter) - rcbld) ./ rcbld;
    case {'normchange','vssum'}
      freq.(cfg.parameter) = (freq.(cfg.parameter) - rcbld) ./ (freq.(cfg.parameter) + rcbld);
    case 'z-score'
      freq.(cfg.parameter) = (freq.(cfg.parameter) - rcbld) ./ std(BLfreq.(cfg.parameter),[],timedim,'omitnan');
    otherwise
      error('unsupported cfg.baselinetype');
  end
end

%% if transform is desired after or without normalization, do it now

if trans
  if ~BL || (BL && strncmpi(cfg.transorder,'a',1))
    %% transform trial data
    
    switch cfg.transtype
      case 'dB'
        freq.(cfg.parameter) = 10.*log10(freq.(cfg.parameter));   % transformed trial data
      case 'sqrt'
        freq.(cfg.parameter) = sqrt(freq.(cfg.parameter));
      case 'ln'
        freq.(cfg.parameter) = log(freq.(cfg.parameter));
      otherwise
        error('unsupported cfg.transtype')
    end
  end
end

%% compare normalized distribution to raw, if desired

cfg.skewout = skewness(freq.(cfg.parameter),1,timedim);
% cfg.meanout = mean(freq.(cfg.parameter),timedim,'omitnan');
% cfg.stdout = std(freq.(cfg.parameter),0,timedim,'omitnan');
% cfg.BLskewout = skewness(BLfreq.(cfg.parameter),1,timedim);
% cfg.BLmeanout = mean(BLfreq.(cfg.parameter),timedim,'omitnan');
% cfg.BLstdout = std(BLfreq.(cfg.parameter),0,timedim,'omitnan');

if compare
  [minskew,minsi] = min(cfg.skewout(:));
  [minI,minJ] = ind2sub(size(cfg.skewout),minsi);  % assumes timedim & ndims = 3
  subplot(2,2,3);
  histfit(squeeze(freq.(cfg.parameter)(minI,minJ,:)),100);
  title(['min normalized skew = ' num2str(minskew) ' @ ' dimord{1} ' ' num2str(minI) ...
    ', ' dimord{2} ' ' num2str(minJ)])
  
  [maxskew,maxsi] = max(cfg.skewout(:));
  [maxI,maxJ] = ind2sub(size(cfg.skewout),maxsi);
  subplot(2,2,4);
  histfit(squeeze(freq.(cfg.parameter)(maxI,maxJ,:)),100); % assumes timedim & ndims = 3
  title(['max normalized skew = ' num2str(maxskew) ' @ ' dimord{1} ' ' num2str(maxI) ...
    ', ' dimord{2} ' ' num2str(maxJ)])

% fig = figure; clf; fig.WindowStyle = 'docked';
% fig.Name = 'Transformed Baseline Data';
% plotFTmatrix_TEM([],BLfreq);
% fig = figure; clf; fig.WindowStyle = 'docked';
% fig.Name = 'Trial Data After Normalization';
% plotFTmatrix_TEM([],freq);
% figure(fig1);
end

%% preserve cfg history

if isfield(freq,'cfg') && ~isfield(freq.cfg,'previous') && isfield(freq,'previous')
  if isfield(freq.previous,'cfg')
    freq.cfg.previous = freq.previous.cfg;
  else
    freq.cfg.previous = freq.previous;  % I think I did this wrong previously, so this is a fix
  end
end
if isfield(BLfreq,'cfg') && ~isfield(BLfreq.cfg,'previous') && isfield(BLfreq,'previous')
  if isfield(BLfreq.previous,'cfg')
    BLfreq.cfg.previous = BLfreq.previous.cfg;
  else
    BLfreq.cfg.previous = BLfreq.previous;
  end
end
cfg.previous = {freq.cfg, BLfreq.cfg};
freq.cfg = cfg;

%% save output, if desired

if isfield(cfg,'outputfile') && ~isempty(cfg.outputfile)
  disp(['Saving normalized data to ' cfg.outputfile])
  save(cfg.outputfile,'freq')
end

end
