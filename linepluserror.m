function [h, means, errs] = linepluserror(cfg, data)
%LINEPLUSERROR plots mean +/- error (SEM, CI, etc.) and returns stats
%   Configurable to different types of error calculations, uses shaded
%   lines to denote error on plot
%
%   by TEM & JH 5/2-25/16
%
%   INPUTS:
%     cfg = configuration structure including the following options:
%             cfg.errortype = 'STD', 'SEM', or 'CI'
%             cfg.alpha = 0.05 (for 95% confidence interval)
%             cfg.nboot = # of bootstrap samples (only for CI)
%             cfg.x = vector to use as x coordinates, should have same # of
%                     elements as rows in data{1} and all cells
%             cfg.xlabel = string for x-axis
%             cfg.ylabel = string for y-axis
%             cfg.groups = cell array of strings to use as legend entries
%                           for each cell of data
%             cfg.h = optional handle to axes
%             cfg.stattype = string representing Matlab function to use for
%                 statistical testing, only applicable with 2 input groups
%
%     data = matrix or table, where columns (2nd dim) are samples to be
%             averaged across and rows correspond to values of cfg.x, or a
%             cell for each group of samples to compare, where each cell
%             has the same # of rows as cfg.x
%
%   OUTPUTS:
%     h = handle to axes plotting smean +/- serr for each group
%
%     means = means for each group of data samples by column
%
%     errs = error estimate for each group of data samples by column
%
%     stats = Matlab generated structure output by statistical testing
%             (TBD)

%% check inputs

if nargin < 2
  error('Function requires both cfg and data inputs');
end

if ~iscell(data)
  data = {data};
end

if ~isstruct(cfg)
  error('First input should be configuration structure cfg.  See help for details.')
elseif ~isfield(cfg,'x') || ~isvector(cfg.x)
  warning('using row number as x coordinate')
  cfg.x = 1:size(data{1},1);
end

for g = 1:numel(data)   % groups
  if size(data{g},1) ~= numel(cfg.x)
    error(['data{' num2str(g) '} and all other cells must contain ' ...
      'the same number of rows, corresponding to elements in cfg.x']);
  end
end

if ~isfield(cfg,'groups')
  cfg.groups = {['Mean +/- ' cfg.errortype]};
elseif ~iscellstr(cfg.groups)
  if ischar(cfg.groups)
    cfg.groups = {cfg.groups};
  else
    error('cfg.groups should be a string or cell array of strings');
  end
end

if numel(cfg.groups) ~= numel(data)
  warning([num2str(numel(cfg.groups)) ' names assigned to ' ...
    num2str(numel(data)) ' groups of data']);
end

if ~isfield(cfg,'xlabel') || ~ischar(cfg.xlabel)
  cfg.xlabel = '';
end

if ~isfield(cfg,'ylabel') || ~ischar(cfg.ylabel)
  cfg.ylabel = '';
end

%% preallocate space for outputs

means = NaN(numel(cfg.x),numel(data));
errs = NaN(2,numel(cfg.x),numel(data));

%% average samples in each group and calculate desired error type

for g = 1:numel(data)   % groups
  means(:,g) = mean(data{g},2,'omitnan');
  switch cfg.errortype
    case 'STD'  % standard deviation
      err = std(data{g},0,2,'omitnan');
      errs(1,:,g) = means(:,g) - err;
      errs(2,:,g) = means(:,g) + err;
    case 'SEM'  % std/sqrt(# of observations)
      err = std(data{g},0,2,'omitnan')./sqrt(sum(~isnan(data{g}),2));
      errs(1,:,g) = means(:,g) - err;
      errs(2,:,g) = means(:,g) + err;
    case 'CI'   % bootstrap-based confidence interval around the mean
      if ~isfield(cfg,'alpha') || ~isscalar(cfg.alpha)
        warning('using default alpha = 0.05'); cfg.alpha = 0.05;
      end
      if ~isfield(cfg,'nboot') || ~isscalar(cfg.nboot)
        warning('using default nboot = 100'); cfg.nboot = 100;
      end
      errs(:,:,g) = bootci(cfg.nboot,{@nanmean,data{g}'}, 'type','cper', ...
        'alpha',cfg.alpha);  % bca calls jackknife, which fails on nanmean
    otherwise
      error(['cfg.errortype = ''' cfg.errortype ...
        ''' is not supported.  See help for options.']);
  end
end

%% plot shaded error

if isfield(cfg,'h')
  axes(cfg.h);
else
  figure; 
end

h = plot_vector_TEM(means,cfg.x,'n',errs);
xlabel(cfg.xlabel);
ylabel(cfg.ylabel);
legend(cfg.groups);

%% run stats

end

