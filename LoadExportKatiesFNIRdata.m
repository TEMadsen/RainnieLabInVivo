%% load Katie's FNIR data and export to Excel

clearvars; close all; clc;  % start with a clean slate

fS_Exported_all_TEC; % filename can't have anything but letters, numbers, and underscores

%% collect variable info & identify data matrices


vars = whos;  % all variables in workspace
% vars(1,:) = [];
numv = find(strcmp('double',{vars(:).class}));  % row #s for numerical data
sizs = vertcat(vars(numv).size);  % sizes of numerical variables
isdm = sizs(:,2) > 1;   % t/f: is data matrix (of numerical variables)
isdm1 = sizs(:,2) == 1;   % t/f: is data matrix (of numerical variables)
oxySig = sizs(isdm);
oxyRef = sizs(isdm1);
oxyRef = [oxyRef; 0];
timeDiff = oxyRef-oxySig;
missTrl = find(timeDiff ~= 0,1);

% found miss trial is locate at block 7, assess condition
idx7 = zeros(numel(vars),1);

for ii = 1:numel(vars)
    if numel(vars(ii).name) >24
        if strcmp('7',{vars(ii).name(25)}) == 1 ||...
           strcmp('7',{vars(ii).name(24)}) == 1 || ...
           strcmp('7',{vars(ii).name(23)}) == 1
           idx7(ii) = 1;
        end
    elseif numel(vars(ii).name) >23
        if strcmp('7',{vars(ii).name(24)}) == 1 || ...
           strcmp('7',{vars(ii).name(23)}) == 1
           idx7(ii) = 1;
        end
    elseif strcmp('7',{vars(ii).name(23)}) == 1
        idx7(ii) = 1;
    end
end

vars7 = vars(logical(idx7));
vars(numv(missTrl-1))
vars(numv(missTrl))

numv7 = find(strcmp('double',{vars7(:).class}));  % row #s for numerical data
vars7num = vars7(numv7);

% 'oxygraph1_ref_hbr_Time7_3' is missing and following 7_is missing

matv = numv(isdm);   % row #s of matrix variables (data)

if sum(sizs(isdm,1)) ~= sum(sizs(~isdm,1))
  error('number of data & time points are not equal')
end
%% loop through each data variable, creating table

data = table;

for dm = matv
  dmname = vars(dm).name;     % variable name for this data matrix
  eval(['label0 = ' dmname '_label0; label1 = ' dmname ...
    '_label1; label2 = ' dmname '_label2;']);   % corresponding labels
  dmtimv = strrep(dmname, 'Block', 'Time');     % variable name for time
  eval(['time = ' dmtimv ';']);   % timestamps for each data point
  eval(['opt = ' dmname ';']);   % optode data matrix
  if length(time) ~= length(opt)
    error('number of data & time points are not equal')
  end
  data = [data; repmat(cell2table({dmname,label0,label1,label2}),size(time)) ...
    table(time) array2table(opt)];
end

%% export table to Excel format

writetable(data,'S:\Katie\KatiesFNIRdataJH.xlsx')