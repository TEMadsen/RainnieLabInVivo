function [ montage ] = bipolar_montage( sens, type )
% BIPOLAR_MONTAGE automatically generates bipolar montage structure from
%   sensor (e.g., electrode) position data
%   
% 	INPUTS:
%     sens = structure as described in ft_datatype_sens
%     type = string describing montage type, current options include:
%       'allNN' = all nearest neighbors, each pair of electrodes with the
%         minimum geometric distance apart, may be equivalent to one of the
%         following, depending on electrode spacing in each dimension
%       'AP' = anterior-posterior pairs, assuming similar ML & DV coords
%       'ML' = medial-lateral pairs, assuming similar AP & DV coords
%       'both' = both AP & ML pairs
%
%   OUTPUT:
%     montage = structure as described in ft_apply_montage
%
% by TEM 2/14/17

%% verify inputs

if nargin ~= 2
  error('both inputs are required')
end

sens = ft_datatype_sens(sens);

%% calculate distances between each pair of electrodes in each dimension

dists = NaN(numel(sens.label),numel(sens.label),4);

for s = 1:(numel(sens.label)-1)
  for n = (s+1):numel(sens.label)
    dists(s,n,1) = sens.elecpos(s,1) - sens.elecpos(n,1);   % AP
    dists(s,n,2) = sens.elecpos(s,2) - sens.elecpos(n,2);   % ML
    dists(s,n,3) = sens.elecpos(s,3) - sens.elecpos(n,3);   % DV
    
    % Euclidean distance
    dists(s,n,4) = sqrt(dists(s,n,1)^2 + dists(s,n,2)^2 + dists(s,n,3)^2);
  end
end
    
%% create outputs

montage.labelold = sens.label;

switch type
  case 'allNN'
    
  case 'AP'
    
  case 'ML'
    
  case 'both'

end

