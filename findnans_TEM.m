function [uI, prop, nanI] = findnans_TEM(data, cfg)
% FINDNANS_TEM finds the unique indices of all NaNs in a multidimensional matrix
%   and can plot the distribution over the longest dimension if desired.
%
%     Use as [uI, prop, nanI] = findnans_TEM(mat, cfg);
%
%   INPUT:
%     data  = can be a matrix, a cell array of matrices (each must have the same
%             size), or a structure containing one of the above (if cells don't
%             have the same size, they can be concatenated along the time dim)
%     cfg   = can be [] or a struct containing any of these fields:
%       parameter   = field to search for NaNs if data is a struct
%                     (default = largest numeric matrix)
%       dimord      = string representing dimension order of matrix, if not
%                     included as a field of data
%       plt         = a boolean flag for whether you want a plot or not
%                     (default = false)
%
%   OUTPUTS:
%     uI    = a cell array of the unique indices containing any NaNs along each
%             dimension
%     prop  = a column vector with size [ndims(mat)+1, 1], giving the proportion
%             of elements containing any NaNs along each dim & in the whole mat
%     nanI  = optionally return full list of indices as a matrix with size
%             [sum(isnan(mat(:))) ndims(mat)]
%
% written 4/28/2017 by Teresa E. Madsen, Ph.D.

if nargin < 2 || isempty(cfg) || ~isfield(cfg,'plt') || isempty(cfg.plt)
  cfg.plt = false;
else
  cfg.plt = istrue(cfg.plt);
end

if isstruct(data)
  if ~isfield(cfg,'parameter') || isempty(cfg.parameter)
    cfg.parameter = fieldnames(data);
    fclass = cell(size(cfg.parameter));
    fbytes = nan(size(cfg.parameter));
    for f = 1:numel(cfg.parameter)
      tmpfield = data.(cfg.parameter{f});   %#ok<NASGU> passed as string
      S = whos('tmpfield');
      fclass{f} = S.class;
      fbytes(f) = S.bytes;
    end
    [~,bigI] = sort(fbytes,'descend');
    pI = find(ismember(fclass(bigI),{'double','cell'}),1,'first');
    cfg.parameter = cfg.parameter{bigI(pI)};
  end
  disp(['searching for NaNs in data.' cfg.parameter])
  mat = data.(cfg.parameter);
else
  mat = data;
end

if iscell(mat)
  if numel(mat) == 1
    mat = mat{1};
  else
    siz = cellfun(@size,mat, 'UniformOutput',false);
    if isequal(siz{:})
      siz = siz{1};
      catdim = find(siz == 1,1,'first');
      if isempty(catdim)
        catdim = numel(siz) + 1;
      end
    else
      ndims = cellfun(@numel,siz, 'UniformOutput',false);
      if isequal(ndims{:})
        siz = vertcat(siz{:});
        catdim = find(~all(siz == siz(1,:)));
        if numel(catdim) ~= 1
          error('cells cannot be concatenated due to variable sizes along more than 1 dim')
        end
      else
        error('cells cannot be concatenated due to different numbers of dimensions')
      end
    end
    disp(['concatenating ' num2str(numel(mat)) ' cells along dim ' ...
      num2str(catdim)])
    mat = cat(catdim,mat{:});
  end
end

if all(isnan(mat(:)))
  warning('all values in the matrix are NaNs')
  return
elseif ~any(isnan(mat(:)))
  warning('no NaNs were found in the matrix')
  return
end

siz = size(mat);
nanI = cell(size(siz));         % one index per dim per NaN
uI = cell(size(siz));
prop = zeros(numel(siz)+1, 1); 	% proportion of each dim & of whole array

nanmat = isnan(mat);                        % whole logical array
[nanI{:}] = ind2sub(siz, find(nanmat(:)));  % deal indices into cells
nanI = [nanI{:}];                           % concatenate cells into matrix

% use dimord to convert indices to values
if isfield(cfg,'dimord') && ~isempty(cfg.dimord)
  dimord = strsplit(cfg.dimord,'_');
  if exist('catdim','var')
    if catdim > numel(dimord)
      dimord{catdim} = 'cell';
    else
      dimord{catdim} = ['{' dimord{catdim} '}'];
    end
  end
elseif isstruct(data) && isfield(data,'dimord')
  dimord = strsplit(data.dimord,'_');
  if exist('catdim','var')
    if catdim > numel(dimord)
      dimord{catdim} = 'cell';
    else
      dimord{catdim} = ['{' dimord{catdim} '}'];
    end
  end
  % these won't find {*}, which is fine, as converting the concatenated axis would be difficult & confusing
  fdim = strcmp(dimord,'freq');
  tdim = strcmp(dimord,'time');
  if isfield(data,'freq') && any(fdim)
    logspc = any(abs(diff(data.freq)-mean(diff(data.freq))) > 0.01);
    if logspc   % really any nonlinear spacing will trigger this
      nanI(:,fdim) = log(data.freq(nanI(:,fdim)));  % use log for clearer plotting
    else
      nanI(:,fdim) = data.freq(nanI(:,fdim));
    end
  end
  if isfield(data,'time') && any(tdim)
    nanI(:,tdim) = data.time(nanI(:,tdim));
  end
else
  dimord = cell(size(siz));
end

for d = 1:numel(uI)
  uI{d} = unique(nanI(:,d));
  prop(d) = numel(uI{d})/siz(d);
end

prop(end) = size(nanI,1)/numel(mat);

if ~cfg.plt  % the above is all that's needed
  return
end

% downsample any large dimensions for easier plotting
dsfactor = ceil(siz/100);   % 1 for any dimensions w/ length <=100
matplt = mat;
for d = find(dsfactor > 1)
  switch d
    case 1
      matplt = matplt(1:dsfactor(d):end,:,:,:,:);
    case 2
      matplt = matplt(:,1:dsfactor(d):end,:,:,:);
    case 3
      matplt = matplt(:,:,1:dsfactor(d):end,:,:);
    case 4
      matplt = matplt(:,:,:,1:dsfactor(d):end,:);
    otherwise
      error('not expecting >4 dimensions!')
  end
end
sizplt = size(matplt);
nanIplt = cell(size(siz));

nanmatplt = isnan(matplt);                        % whole logical array
[nanIplt{:}] = ind2sub(sizplt, find(nanmatplt(:)));  % deal indices into cells
nanIplt = [nanIplt{:}];                           % concatenate cells into matrix

% convert indices to values if possible
if exist('fdim','var') && isstruct(data) && isfield(data,'freq') && any(fdim)
  if logspc   % really any nonlinear spacing will trigger this
    nanIplt(:,fdim) = log(data.freq(nanIplt(:,fdim)));  % use log for clearer plotting
  else
    nanIplt(:,fdim) = data.freq(nanIplt(:,fdim));
  end
end
if exist('tdim','var') && isstruct(data) && isfield(data,'time') && any(tdim)
  nanIplt(:,tdim) = data.time(nanIplt(:,tdim));
end

[~,AX,~,~,HAx] = plotmatrix(nanIplt);
drawnow; set(get(handle(gcf),'JavaFrame'),'Maximized',1);
dlim = nan(numel(HAx),2);
for d = 1:numel(HAx)
  axis(HAx(d),'tight'); drawnow; dlim(d,:) = HAx(d).XLim;
  xlabel(AX(end,d),dimord{d})
  ylabel(AX(d,1),dimord{d})
end
for x = 1:numel(HAx)
  for y = 1:numel(HAx)
    AX(y,x).XLim = dlim(x,:);
    AX(y,x).YLim = dlim(y,:);
  end
end
if exist('logspc','var') && logspc
  AX(end,fdim).XTickLabel = cellstr(num2str(...
    round(exp(AX(end,fdim).XTick(:)))));
  AX(fdim,1).YTickLabel = cellstr(num2str(...
    round(exp(AX(fdim,1).YTick(:)))));
end

end   % function