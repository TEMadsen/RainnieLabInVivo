function [zscore,pval,h,modS,ots] = sig_OTC_TEM(oscEvents,rawdata,winLen,fig)
%%  Compute the significance of the OTC
% using the same number of random timepoints as oscillatory peaks
%
%   INPUTS:
%    oscEvents  = sample numbers for peak oscillatory events
%    rawdata    = time series
%    winLen     = OTC window (+/- 0.6 seconds from oscillatory event)
%    fig        = 'y'/'n' to creating 3 figures to display the result
%
%   OUTPUTS:
%    zscore     = the z-score of the MS of the raw data versus surrogates
%    pval       = the corresponding p-value.
%    h          = the result of the z-test: 1 if the null hypothesis is
%                   rejected, 0 if it is not
%    modS       = modulation strength from Dvorak & Fenton, J. Neurosci. Methods 2014
%
%   by Teresa Madsen, 2015

if nargin < 4 || isempty(fig);    fig = 'n';      end

%% real data

ots = zeros(1,winLen*2); % oscillation-triggered sum

for sI = 1:length(oscEvents)
    chunk = rawdata(oscEvents(sI)-winLen+1:oscEvents(sI)+winLen);
    ots = ots + chunk;
end
modS  = range(ots);

%% surrogates

nsurrogate = 500;             % Use 500 surrogates.
ots_surrogate = zeros(nsurrogate,winLen*2);

surrogates = randi([winLen,length(rawdata)-winLen],numel(oscEvents),nsurrogate);

for k=1:nsurrogate                       % For each surrogate,
    for sI = 1:length(oscEvents)
        chunk = rawdata(surrogates(sI,k)-winLen+1:surrogates(sI,k)+winLen);
        ots_surrogate(k,:) = ots_surrogate(k,:) + chunk;
    end
end
modS_surrogate  = range(ots_surrogate,2);

if fig == 'y'
    figure; hist(modS_surrogate); line([modS modS],get(gca,'ylim'));
    figure; plot(ots_surrogate','color',[0.5 0.5 0.5]); hold on;
    plot(ots,'LineWidth',2);
end

[mean_surrogate, std_surrogate] = normfit(modS_surrogate); % Fit surr. data

% Compute z-score.  Right sided because we only care if the actual data is
% significantly MORE modulated than the surrogate data.
[h,pval,~,zscore] = ztest(modS,mean_surrogate,std_surrogate,'tail','right');

h = logical(h);

if h == 1 && zscore < 0
    error('Significant result for negative z-score');
end

if isnan(h)
    error('h is NaN');
end

end
