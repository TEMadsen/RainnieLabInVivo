function [freq] = mtmfft_movwin_TEM(cfg,data)
% MTMFFT_MOVWIN_TEM applies ft_freqanalysis with method 'mtmfft' to moving
%     windows (data restructured into overlapping sub-trials), turning a
%     time-free spectrum technique into a time-frequency analysis to
%     produce spectrograms and/or coherograms
%
%   INPUTS:
%     cfg structure should contain all info required for ft_freqanalysis
%       with cfg.method = 'mtmfft'; plus the field:
%         'movwin' = [window length, shift size]; both in units of time (s)
%
%     data should be in the format specified by ft_datatype_raw, already
%       trimmed to the time of interest +/- half a window length.  Real
%       trials (e.g., tone-triggered) that should be retained in output
%       must have unique identifiers in data.trialinfo.  Artifacts can be
%       replaced by NaNs on a per-channel basis.  Can be read from
%       cfg.inputfile.
%
%   OUTPUT:
%     freq will be in the format usually output from ft_freqanalysis with
%       cfg.method = 'mtmconvol'; with the fake sub-trials converted back
%       into a time axis.  Can be written to cfg.outputfile.
%
% written 3/7/17 by Teresa E. Madsen

%% check inputs & load data if needed

if nargin == 2
  assert(~isfield(cfg,'inputfile') || isempty(cfg.inputfile), ...
    'either pass data as the 2nd input or include cfg.inputfile, but not both')
else
  load(cfg.inputfile)
  cfg = rmfield(cfg,'inputfile');   % so it isn't passed to ft_freqanalysis
end

if isfield(cfg,'outputfile') && ~isempty(cfg.outputfile)
  outputfile = cfg.outputfile;
  cfg = rmfield(cfg,'outputfile');  % so it isn't passed to ft_freqanalysis
else
  outputfile = [];
end

if isfield(cfg,'method') && ~isempty(cfg.method)
  assert(strcmp(cfg.method,'mtmfft'), ...
    'this function is designed for cfg.method = ''mtmfft'';');
else
  cfg.method = 'mtmfft';
end

if isfield(cfg,'output') && ~isempty(cfg.output)
  switch cfg.output
    case 'powandcsd'
      assert(numel(data.label) > 1, ...
        'more than 1 channel required for cfg.output = ''powandcsd''');
      csd = true;
    case 'pow'
      csd = false;
    otherwise
      error('this function is designed for cfg.output = ''powandcsd'' or ''pow''');
  end
elseif numel(data.label) > 1  % default for multiple channels:
  cfg.output = 'powandcsd';
  csd = true;
else  % default for 1 channel:
  cfg.output = 'pow';
  csd = false;
end

if isfield(cfg,'keeptrials') && ~isempty(cfg.keeptrials)
  keeptrials = istrue(cfg.keeptrials);  % Save original value for output.
  % If false, output dimord will be 'chan_freq_time' as if it were an
  % average across trials.  This makes other functions unable to determine
  % the dimord of freq.trialinfo, so they remove it.
else  % use default:
  keeptrials = true;  % Retain any trials with unique rows of trialinfo
  % and leave a singleton 'rpt' dim if all trialinfo are identical.  This
  % makes trialinfo more clearly interpretable by other FT functions.
end
cfg.keeptrials = 'yes';   % trials passed to ft_freqanalysis must be kept

%% restructure into moving windows as trials

cfgtmp = [];
cfgtmp.length  = cfg.movwin(1);
cfgtmp.overlap = (cfg.movwin(1)-cfg.movwin(2))/cfg.movwin(1);

if isfield(cfg,'trials') && ~isempty(cfg.trials)
  cfgtmp.trials = cfg.trials;
  cfg = rmfield(cfg,'trials');  % so it isn't passed to ft_freqanalysis
end

tmpdat = ft_redefinetrial(cfgtmp, data);

%% calculate spectra using input configuration

freq = ft_freqanalysis(cfg, tmpdat);

%% convert trial spectra to timebins of a spectrogram

dimtok_old = strsplit(freq.dimord, '_');
dimtok_old = strrep(dimtok_old, 'rpt', 'time');
freq.dimord = 'chan_freq_time';  % standard for single-trial/average TFAs
freq.time = cellfun(@median, tmpdat.time);
  
if keeptrials
  % add another rpt on the end (assume there's a singleton dim there)
  dimtok_old = [dimtok_old {'rpt'}];
  freq.dimord = 'rpt_chan_freq_time';   % standard for output of other TFAs
end

dimtok_new = strsplit(freq.dimord, '_');
dimord = zeros(size(dimtok_new));
for dim = 1:numel(dimtok_new)
  dimord(dim) = find(strcmp(dimtok_new{dim},dimtok_old));
end
freq.powspctrm = permute(freq.powspctrm, dimord);
if csd
  freq.crsspctrm = permute(freq.crsspctrm, dimord);
end

%% check and correct for multiple trials

if size(unique(freq.trialinfo,'rows'),1) == 1
  assert(numel(unique(freq.time)) == numel(freq.time), ...
    'timebins repeat in spite of single trial identifier')
  freq.trialinfo = freq.trialinfo(1,:);
else % multiple trial IDs
  warning('this code has not been tested!') % it is too memory intensive to run multiple trials at once anyway
  trlinfo = unique(freq.trialinfo,'rows','stable');
  tmptime = unique(freq.time);
  assert(size(freq.trialinfo,1) == numel(tmptime)*size(trlinfo,1), ...
    'something went wrong with trial-timebins')
  if keeptrials
    tmppow = nan(size(trlinfo,1), numel(freq.label), numel(freq.freq), ...
      numel(tmptime));
    assert(numel(tmppow) == numel(freq.powspctrm), 'dims are messed up')
    if csd
      tmpcsd = nan(size(trlinfo,1), size(freq.labelcmb,1), ...
        numel(freq.freq), numel(tmptime));
      assert(numel(tmpcsd) == numel(freq.crsspctrm), 'dims are messed up')
    end
  else % ~keeptrials
    tmppow = nan(numel(freq.label), numel(freq.freq), numel(tmptime));
    if csd
      tmpcsd = nan(size(freq.labelcmb,1), numel(freq.freq), numel(tmptime));
    end
  end
  
  for tr = 1:size(trlinfo,1) % real trials
    sts = ismember(freq.trialinfo,trlinfo(tr,:),'rows'); % subtrials
    for t = 1:numel(tmptime) % timebins
      if keeptrials
        tmppow(tr,:,:,t) = freq.powspctrm(1,:,:,sts);
        if csd
          tmpcsd(tr,:,:,t) = freq.crsspctrm(1,:,:,sts);
        end
      else % average across trials
        tmppow(:,:,t) = mean(freq.powspctrm(:,:,sts),3,'omitnan');
        if csd
          tmpcsd(:,:,t) = mean(freq.crsspctrm(:,:,sts),3,'omitnan');
        end
      end
    end % for timebins
  end % for real trials
end % if # of trials
  
%% save output if necessary

if ~isempty(outputfile)
  disp(['saving freq to ' outputfile])
  save(outputfile, 'freq', '-v7.3');
end

end

