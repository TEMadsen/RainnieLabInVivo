function [zscore,pval,h,modS,ota] = sig_OTCave_TEM(oscEvents,rawdata,winLen,alpha,fig)
%%  Compute the significance of the OTC
% using the same number of random timepoints as oscillatory peaks
%
%   INPUTS:
%    oscEvents  = sample numbers for peak oscillatory events
%    rawdata    = time series
%    winLen     = OTC window (+/- 0.6 seconds from oscillatory event)
%    alpha      = corrected for # of comparisons
%    fig        = 'y'/'n' to creating 3 figures to display the result
%
%   OUTPUTS:
%    zscore     = the z-score of the MS of the raw data versus surrogates
%    pval       = the corresponding p-value.
%    h          = the result of the z-test: 1 if the null hypothesis is
%                   rejected, 0 if it is not
%    modS       = modulation strength from Dvorak & Fenton, J. Neurosci. Methods 2014
%
%   by Teresa Madsen, 2015

if nargin < 5 || isempty(fig);    fig = 'n';      end

%% real data

chunk = zeros(length(oscEvents),winLen*2); % chunks of data around oscillatory peaks

for sI = 1:length(oscEvents)
    chunk(sI,:) = rawdata(oscEvents(sI)-winLen+1:oscEvents(sI)+winLen);
end
    ota = mean(chunk,1);    % oscillation-triggered average

modS  = range(ota);

%% surrogates

nsurrogate = 500;             % Use 500 surrogates.
ota_surrogate = zeros(nsurrogate,winLen*2);

surrogates = randi([winLen,length(rawdata)-winLen],numel(oscEvents),nsurrogate);

for k=1:nsurrogate                       % For each surrogate,
    for sI = 1:length(oscEvents)
        chunk(sI,:) = rawdata(surrogates(sI,k)-winLen+1:surrogates(sI,k)+winLen);
    end
        ota_surrogate(k,:) = mean(chunk,1);
end
modS_surrogate  = range(ota_surrogate,2);

if fig == 'y'
    figure; hist(modS_surrogate); line([modS modS],get(gca,'ylim'));
    figure; plot(ota_surrogate','color',[0.5 0.5 0.5]); hold on;
    plot(ota,'LineWidth',2);
end

[mean_surrogate, std_surrogate] = normfit(modS_surrogate); % Fit surr. data

% Compute z-score.  Right sided because we only care if the actual data is
% significantly MORE modulated than the surrogate data.
[h,pval,~,zscore] = ztest(modS,mean_surrogate,std_surrogate,...
    'tail','right','alpha',alpha);

h = logical(h);

if h == 1 
    if zscore < 0
        error('Significant result for negative z-score');
    end
end

if isnan(h)
    error('h is NaN');
end

end
