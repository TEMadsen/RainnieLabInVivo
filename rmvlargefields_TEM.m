function [dataout] = rmvlargefields_TEM(datain, expected, pausebeforesave)
% RMVLARGEFIELDS_TEM identifies and removes the largest unexpected subfields of
%   a nested structure and from all cells, e.g., for use with FieldTrip dataout
%   with many nested cfg.previous's, including those with multiple inputs.  If
%   the input is a filename, it will be loaded, processed, and resaved using the
%   same filename and variable name.
%
% written 4/26/17 by Teresa E. Madsen, Ph.D.

%% check inputs

if ischar(datain)
  filename = datain;
  disp(['Loading datain from file ' filename])
  if ~existfile_TEM(filename)   % waits for server to reconnect first
    error('inputfile does not exist')
  end
  tic
  S = load(filename);
  toc   % displays time taken to load
  varname = fieldnames(S);
  datain = S.(varname{1});
  
  if nargin < 3 || isempty(pausebeforesave)
    pausebeforesave = true;   % default
  else
    pausebeforesave = istrue(pausebeforesave);
  end
end

S = whos('datain');
bytesin = S.bytes;
disp(['datain has ~' num2str(bytesin,'%.1g') ' bytes'])
if bytesin < 2e7
  dataout = datain;
  warning('No changes made because datain is smaller than 20 MB')
  return
end

if nargin < 2 || isempty(expected)
  expected = {'cfg','previous'};  % defaults
end

%% assign starting values for temporary variables

dataout = datain;     % start by assuming no changes
tmp = dataout;        % temp copy of the current level of analysis
fullstr = 'dataout';  % points to current tmp in full dataout structure
stop = false;   % flag when subfields get small enough or after removing a field
  
if isstruct(tmp) && isfield(tmp,'cfg')
  [bytes,fields] = fieldsizes(tmp);
  if bytes(strcmp(fields,'cfg')) < max(bytes)
    warning('No changes made because cfg is not the largest field of datain')
    return  % without evaluating the rest of the function
  end
  tmp = tmp.cfg;     % skip top level so we don't lose the actual data!
  fullstr = [fullstr '.cfg'];
end
if isstruct(tmp) && isfield(tmp,'previous')  
  [bytes,fields] = fieldsizes(tmp);
  if bytes(strcmp(fields,'previous')) < max(bytes)
    warning('No changes made because previous is not the largest field of datain(.cfg)')
    return  % without evaluating the rest of the function
  end
  tmp = tmp.previous;   % skip 1 more level to avoid most important metadata
  fullstr = [fullstr '.previous'];
end    

%% main loop

while ~stop
  if isstruct(tmp)
    [dataout, tmp, fullstr, stop] = rmvbiggestfield(dataout, tmp, fullstr, expected);
  elseif iscell(tmp)
    for c = 1:numel(tmp)
      cellstr = [fullstr '{' num2str(c) '}'];
      tmpfield = tmp{c};
      if isstruct(tmpfield)
        % Remove largest subfield on same level as an outputfile, even if
        % expected (strings in 'expected' are now preapproved for replacement).
        % These multi-file merge points are what cause the cfg history to expand
        % exponentially, and earlier steps will be preserved in those files
        % anyway.
        [dataout] = rmvbiggestfield(dataout, tmpfield, cellstr, {}, expected);
        % don't need other outputs because we can't dig through multiple layers
        % of cells.
      else
        [dataout] = rmvfield(dataout, tmpfield, cellstr);
      end
    end
    stop = true;
  else  % end of nested structure
    dataout = rmvfield(dataout, tmp, fullstr);
    stop = true;
  end   % if isstruct or iscell or neither
end   % while ~stop

%% check changes & save, if needed

S = whos('dataout');
  
if isequaln(datain, dataout)
  warning(['No changes made - stopped at ' fullstr])
elseif S.bytes >= bytesin   % data size did *not* decrease
  error('something weird happened')
else 	% success!
  disp(['dataout has ~' num2str(S.bytes,'%.1g') ' bytes:  ' ...
    num2str(100*(1-S.bytes/bytesin),'%.2g') '% less than datain!'])
  if exist('filename','var') && exist('varname','var')  % if loaded from file
    disp(['will save ' varname{1} ' to ' filename])      % save back to same file
    eval([varname{1} ' = dataout;'])                  % with same variable name
    if pausebeforesave
      disp('verify result first, then "dbcont"')
      keyboard
    end
    disp('saving...')
    tic
    save(filename, varname{1})
    toc   % displays time taken to save
  end
end

end   % main function

%% subfunction:  check field sizes

function [bytes,fields] = fieldsizes(tmp)
  fields = fieldnames(tmp);
  bytes = zeros(size(fields));
  for f = 1:numel(fields)
    tmpfield = tmp.(fields{f});   %#ok<NASGU> passed as string
    S = whos('tmpfield');
    bytes(f) = S.bytes;
  end
end

%% subfunction:  find largest field of the structure and remove it if unexpected
% if the fieldname matches one of the expected values, just reduce tmp to that
% lower substructure and append its name to the fullstr pointer

function [dataout, tmp, fullstr, stop] = rmvbiggestfield(dataout, tmp, fullstr, expected, preapproved)
% preapproved should be a cellstr, like expected, here
if nargin < 5 || isempty(preapproved);  preapproved = {};  end
stop = false;   % will indicate when a field has been removed
while ~stop && isstruct(tmp)
  [bytes,fields] = fieldsizes(tmp);
  [~,I] = max(bytes);
  fullstr = [fullstr '.' fields{I}];  %#ok<AGROW> % append largest field to full dataout pointer
  if ~ismember(fields{I}, expected)   % if unexpected fieldname
    if ~any(strcmp(fields,'outputfile'))
      warning('no outputfile field at this level')
      if ~isstruct(tmp.(fields{I}))   % won't be able to continue looping lower
        warning('...but this is the end of the nested structures, so last chance!')
        [dataout,stop] = rmvfield(dataout, tmp.(fields{I}), fullstr, false);  % require approval
      else
        % don't even ask to remove the field, just keep digging
      end
    else
      [dataout,stop] = rmvfield(dataout, tmp.(fields{I}), fullstr, ...
        ismember(fields{I}, preapproved));   % convert to boolean (but keep the cellstr in case removal is not approved)
    end
  end
  tmp = eval(fullstr);  % reduce current substructure to largest field (or replacement string)
end
end

%% subfunction:  replace field with short text description

function [dataout,stop] = rmvfield(dataout, tmpfield, fullstr, preapproved) %#ok<INUSL> called in string
% preapproved should be boolean here
if nargin < 4 || isempty(preapproved);  preapproved = false;  end
stop = false;   % will indicate when a field has been removed
S = whos('tmpfield');
desc = ['removed: ~' num2str(S.bytes,'%.1g') ' byte ' ...
  strjoin(strsplit(num2str(S.size)),'x') ' ' S.class];
if S.bytes > 64   % larger than the string to replace it!
  if preapproved 
    disp(['automatically replacing ' fullstr ' with ''' desc])
    eval([fullstr ' = ''' desc ''';']);  % replace field with small string
    stop = true;
  elseif input(['okay to replace ' fullstr ' with ''' desc '''?  (1/0)  '])  % yes/no
    eval([fullstr ' = ''' desc ''';']);  % replace field with small string
    stop = true;
  end
end
end