function [ cfg ] = get_cfg_TEM( analysis, expt )
% GET_CFG_TEM generates a cfg structure with appropriate defaults for a
%   particular analysis applied to a given expt.  See code for details.
%
% written March 9, 2017 by Teresa E. Madsen, Ph.D.

%% check inputs

if nargin < 2 || isempty(expt)
    expt = 'OrigFear';
elseif strcmp(analysis,'OrigFear')  % they were entered in the wrong order
    analysis = expt;
    expt = 'OrigFear';
end

%% select parameters of interest by experiment

switch expt
    case 'OrigFear'
        toilim      = [-60 60];   % time range of interest, in seconds, needs equal time before & after 0 for actvsbl
        twinlim     = [0.2 29.5]; % min/max length of time window to fit within a tone
        foilim      = [0.7 300];  % full freq range of preamp, adjust later so foi(1) > tapsmofrq(1)
        linef       = 60;         % line noise frequency, which we don't want included in narrow mid gamma band around 45-60 Hz
        wlim        = [0.7 7];    % Hz, range of preferred bandwidths for 0-60 Hz (f+/-w, for 3 tapers: 2/timwin)
        flogres     = 20;         % log freq resolution in # of bins per power of 2
        fsample     = 1000;       % sampling frequency for LFPs
    case 'food'
        toilim      = [-6 12];    % time range of interest, in seconds
        twinlim     = [0.2 29.5]; % min/max length of time windows of analysis
        foilim      = [1.5 150];  % adjusted later so foi(1) > tapsmofrq(1)
        linef       = 60;         % line noise frequency, which we don't want included in narrow mid gamma band around 45-60 Hz
        wlim        = [0.7 7];    % Hz, range of preferred bandwidths for 0-60 Hz (f+/-w, for 3 tapers: 2/timwin)
        flogres     = 20;         % log freq resolution in # of bins per power of 2
        fsample     = 1000;       % sampling frequency for LFPs
    case 'foodNonActive'
        toilim      = [-9 60];    % time range of interest, in seconds
        twinlim     = [0.2 40]; % min/max length of time windows of analysis
        foilim      = [1.5 20];  % adjusted later so foi(1) > tapsmofrq(1)
        linef       = 60;         % line noise frequency, which we don't want included in narrow mid gamma band around 45-60 Hz
        wlim        = [0.7 7];    % Hz, range of preferred bandwidths for 0-60 Hz (f+/-w, for 3 tapers: 2/timwin)
        flogres     = 20;         % log freq resolution in # of bins per power of 2
        fsample     = 1000;       % sampling frequency for LFPs
    case 'social'
        toilim      = [0 900];    % time range of interest, in seconds
        twinlim     = [0.2 29.5]; % min/max length of time windows of analysis
        foilim      = [1.5 150];  % adjusted later so foi(1) > tapsmofrq(1)
        linef       = 60;         % line noise frequency, which we don't want included in narrow mid gamma band around 45-60 Hz
        wlim        = [0.7 7];    % Hz, range of preferred bandwidths for 0-60 Hz (f+/-w, for 3 tapers: 2/timwin)
        flogres     = 20;         % log freq resolution in # of bins per power of 2
        fsample     = 1000;       % sampling frequency for LFPs
    otherwise
        error('experiment not yet defined')
end

%% create cfg for specific type of analysis

cfg = [];
switch analysis
    case 'gen'  % general parameters defined under "switch expt"
        fields = setdiff(who,{'analysis','expt'});  % exclude input arguments
        for f = 1:numel(fields)
            cfg.(fields{f}) = eval(fields{f});  % turn each variable into a cfg field
        end
        
    case 'bandify'  % block freqs into 12 fairly standard bands & toi into BDA
        cfg.foi = exp((ceil(log(foilim(1))*2):floor(log(foilim(2))*2))' ./ 2);
        cfg.w   = cfg.foi ./ exp(0.75);   % figure; errorbar(cfg.foi, cfg.w)
        cfg.bands = join([...
            cellstr(num2str(round((cfg.foi - cfg.w), 2, 'significant'))) ...
            cellstr(num2str(round((cfg.foi + cfg.w), 2, 'significant'))) ...
            {'(Slow)';'';'(Delta)';'';'(Theta)';'';'(Beta)';'';'(Gamma)';'';'(Fast)';''}], ...
            {' - ',' Hz '});
        cfg.toi = -15:30:45;
        cfg.winlen = 30;
        
        %% ft_freqanalysis
    case 'wav'  % input to ft_freqanalysis for wavelet-based time-freq analysis (original)
        cfg.width = round(linef/wlim(2));  % 9 smooths by +/-0.3@2.7Hz, +/-6.66@60Hz, +/-30@270Hz
        cfg.gwidth = pi;  % increases time smoothing w/out affecting freq smoothing,
        % pi here makes the wavelet length = width # of periods for each foi, must be >= 3
        cfg.pad = 'min';  % trim foi if necessary
        cfg = wavparams(cfg);
        
    case 'coarseWav'  % very coarse smoothing for averaging across trials & rats
        cfg.width = 3;  % smooths by +/-0.7 @ 2.1Hz, +/-20 @ 60Hz, +/-75 @ 225Hz
        cfg.gwidth = [];  % solve for largest integer that will fit within tone, increasing padding if necessary
        cfg.pad = 'ok2incr';  % maximize gwidth
        cfg = wavparams(cfg);
        
    case 'medWav'  % medium frequency smoothing (FT defaults)
        cfg.width = 7;  % smooths by +/-0.3 @ 2.1Hz, +/-8.57 @ 60Hz, +/-37 @ 259Hz
        cfg.gwidth = 3;   % minimum (per ft_freqanalysis)
        cfg = wavparams(cfg);
        
    case 'piWav'  % easier to explain:  sf = foi/7, st = 1/foi, wavlen = 7/foi
        cfg.width = 7;  % smooths by +/-0.3 @ 2.1Hz, +/-8.57 @ 60Hz, +/-37 @ 259Hz
        cfg.gwidth = pi;   % allows the easy explanation above
        cfg = wavparams(cfg);
        
    case 'sharpWav'   % sharpest possible freq smoothing, longer temporal smoothing
        cfg.width = 21;   % max with wavlen < 29.5, smooths by +/-0.1 @ 2.1Hz, +/-2.857 @ 60Hz, +/-13 @ 273Hz
        cfg.gwidth = 3;   % minimum (per ft_freqanalysis)
        cfg = wavparams(cfg);
        
    case 'mtmfft_movwin'  % input to custom function mtmfft_movwin_TEM.m for Chronux-like spectrograms & coherograms
        cfg.movwin = [2 0.2];   % window length & shift size (data is restructured into overlapping trials)
        cfg.method = 'mtmfft';  % this gives the whole spectrum with same freq resolution
        cfg.output = 'powandcsd';   % if multiple channels are passed together
        cfg.keeptrials = 'yes';     % refers to any unique rows of data.trialinfo
        cfg.pad = (2^nextpow2(cfg.movwin(1)*fsample))/fsample;
        cfg.foilim = [foilim(1) foilim(end)];   % full freq range of preamp filters
        cfg.tapsmofrq = max(wlim(1), 2/cfg.movwin(1));
        % don't care about bands narrower than +/-wlim(1), but remember that
        % in each artifact-free subtrial, ndatsample must be >= 2/cfg.tapsmofrq
        
    case 'mtmfft'  % for timeless spectra & coherence of whole subtrials
        cfg.method = 'mtmfft';  % this gives the whole spectrum with same freq resolution
        cfg.output = 'powandcsd';   % if multiple channels are passed together
        cfg.keeptrials = 'yes';     % refers to any unique rows of data.trialinfo
        cfg.pad = (2^nextpow2(twinlim(end)*fsample))/fsample;
        cfg.foilim = [foilim(1) foilim(end)];   % full freq range of preamp filters
        cfg.tapsmofrq = max(wlim(1), 2/twinlim(end));
        
    case 'mtmconvol'  % input to ft_freqanalysis for their version of multi-taper time-frequency analysis
        cfg.method  = 'mtmconvol';
        cfg.output  = 'fourier';
        cfg.taper   = 'dpss';
        cfg.pad = (2^nextpow2(range(toilim)*fsample))/fsample;
        if cfg.pad-range(toilim) > twinlim(1)
            twinlim(end) = min(twinlim(end),cfg.pad-range(toilim));
        end
        % to use a linear relationship between frequency & freq smoothing (w):
        % w = m*f + b;      b = w - m*f = w0 - m*0;       m = (w-b)/f = (w60-b)/60;
        b = wlim(1);            % 0.7 Hz smoothing at DC
        m = (wlim(2)-b)/linef;  % 0.105 Hz change in smoothing per Hz change in foi
        cfg.foi = 2.^(ceil(log2(foilim(1))*flogres)/flogres:(1/flogres):log2(foilim(end)));
        cfg.tapsmofrq = m.*cfg.foi + b;  % linear increase in smoothing between wlim(1) at DC & linefw(2) at 60 Hz
        omit = cfg.foi - cfg.tapsmofrq <= 0;   % requested smoothing would include DC
        omit = omit | cfg.foi + cfg.tapsmofrq > foilim(end);   % would be smoothed beyond range of interest
        cfg.t_ftimwin = 2./cfg.tapsmofrq;   % same # of tapers (3) per freq allows saving both trials & tapers
        omit = omit | cfg.t_ftimwin > twinlim(end);   % would require excessively long time windows
        %     omit = omit | cfg.t_ftimwin < twinlim(1);   % would require excessively short time windows
        if any(omit)
            cfg.foi(omit) = [];
            cfg.tapsmofrq(omit) = [];
            cfg.t_ftimwin(omit) = [];
        end
        tstep = floor(cfg.t_ftimwin(end)/2*fsample)/fsample;   % next smaller # of samples, from half the shortest wavelet length,
        cfg.toi = toilim(1):tstep:toilim(2);  % allowing time windows to overlap by >= 50%
        
        %% BLnorm
    case 'FTdB'  % based on FieldTrip's cfg.baselinetype = 'dB';
        % standard decibel transform of power ratio
        cfg.baseline      = 'BLfreq';
        cfg.baselinecent  = 'mean';
        cfg.baselinetype  = 'relative';
        cfg.transtype     = 'dB';
        cfg.transorder    = 'after';
        
    case 'Zraw'  % for cohspctrm (limited to 0-1 range, not positively skewed)
        cfg.baseline      = 'BLfreq';
        cfg.baselinecent  = 'mean';
        cfg.baselinetype  = 'z-score';
        cfg.transtype     = 'none';
        cfg.transorder    = 'before';
        
    case 'ZdB'  % for powspctrm
        cfg.baseline      = 'BLfreq';
        cfg.baselinecent  = 'mean';
        cfg.baselinetype  = 'z-score';
        cfg.transtype     = 'dB';
        cfg.transorder    = 'before';
        
        %% Cross-Frequency Coupling
    case 'KL-MI'
        cfg.freqlow    = foilim(1):wlim(1):20-wlim(1);      % phase frequencies
        cfg.freqhigh   = 20+2*wlim(2):2*wlim(2):foilim(2);  % amplitude frequencies
        cfg.method     = 'mi';    % KL Modulation Index, *NOT* z-score normalized
        cfg.keeptrials = 'yes';   % must be short enough to lose the time axis in
        % this calculation, i.e., whatever length of time we expect CFC to be stationary
        % as the output will have dimord = 'rpt_chan_freqlow_freqhigh'
        
    otherwise
        error('type not yet defined')
end

%% end main function - begin subfunction for wavelet parameters
% only requires:
%   cfg.width = scalar, freq smoothing factor such that:
%                             sf = cfg.foi ./ cfg.width; st = 1 ./ (2*pi .* sf);
%   cfg.gwidth = scalar or [] to solve for highest integer, given other limits
% 	cfg.pad = scalar or string:
%                 'min' to trim foi if necessary, or
%                 'ok2incr' to keep more foi or increase gwidth to twinlim(end)

    function cfg = wavparams(cfg)
        cfg.method = 'wavelet';   % need to compare speed & result with 'tfr'
        cfg.output = 'fourier';
        cfg.keeptrials = 'yes';
        cfg.foi = 2.^(ceil(log2(foilim(1))*flogres)/flogres:(1/flogres):log2(foilim(end)));
        cfg.foi(cfg.foi - cfg.foi ./ cfg.width < 0) = [];   % requested smoothing would include DC
        cfg.foi((3 .* cfg.width) ./ (pi .* cfg.foi) > twinlim(end)) = [];   % shortest possible wavelet would be longer than tone
        cfg.foi(cfg.foi + cfg.foi ./ cfg.width > foilim(end)) = [];   % would be smoothed beyond range of interest
        if isempty(cfg.gwidth) || ~isnumeric(cfg.gwidth) || cfg.gwidth < 3   % solve for largest possible gwidth based on twinlim(end) and/or cfg.pad-range(toilim)
            if ischar(cfg.gwidth) && strcmp(cfg.gwidth,'trimfoi')   % may trim foi to keep padding short
                cfg.pad = (2^nextpow2(range(toilim)*fsample))/fsample;  % start from toi only, no assumption of min wavlen
                maxwavlen = min([cfg.pad - range(toilim), twinlim(end)]);   % no longer than twinlim(end)
                cfg.foi(maxwavlen < (3 .* cfg.width) ./ (pi .* cfg.foi)) = [];   % shortest possible wavelet would be longer than available padding buffer
                maxwavlen = (3 .* cfg.width) ./ (pi .* cfg.foi(1));   % longest wavelength with minimal gwidth at lowest freq
            else
                maxwavlen = max([twinlim(end),(3 .* cfg.width) ./ (pi .* cfg.foi(1))]);   % or tone if longer, just for determining padding
            end
            cfg.pad = (2^nextpow2((range(toilim)+maxwavlen)*fsample))/fsample;
            maxwavlen = min([cfg.pad - range(toilim), twinlim(end)]);   % no longer than twinlim(end)
            cfg.gwidth = floor(maxwavlen .* pi .* cfg.foi(1) ./ cfg.width);  % round down for an easy to report integer
        end
        cfg.foi((cfg.gwidth .* cfg.width) ./ (pi .* cfg.foi) > twinlim(end)) = [];   % shortest wavelet would be longer than tone
        % standard deviations:
        sf = cfg.foi ./ cfg.width;   % st = 1 ./ (2*pi .* sf);  % this still seems to be the real temporal smoothing factor
        if any(sf(2:end) < diff(cfg.foi))
            error('higher resolution of cfg.foi values needed for this cfg.width')
        end
        cfg.wavlen = (cfg.gwidth .* cfg.width) ./ (pi .* cfg.foi);  % just FYI, not needed by ft_freqanalysis
        cfg.pad = (2^nextpow2((range(toilim)+cfg.wavlen(1))*fsample))/fsample;
        tstep = floor(cfg.wavlen(end)/2*fsample)/fsample;   % starting from half the
        while rem(range(toilim),tstep) ~= 0         % shortest wavelet length, tstep
            tstep = tstep - 1/fsample;      % should be the next smaller # of samples,
        end                     % that divides evenly into the time range, allowing
        cfg.toi = toilim(1):tstep:toilim(2);  % time windows to overlap by >= 50%
    end   % subfunction wavparams

end   % main function