function y = errorshade_TEM(matrix,dim,xaxis,face,alpha)

% ERRORSHADE_TEM(matrix,dim,xaxis,face) for 2 dimensional matrices
%   matrix: variable for plotting
%   dim: dimension of variability
%   xaxis: x-axis on plot
%   face: face color (ex. 'r','b',etc.)
%   alpha: face alpha (ex. 0.2)

if dim==1
    if ~isempty(find(isnan(nanmean(matrix,1)),1,'first'))
        xaxis=xaxis(1:(find(isnan(nanmean(matrix,1)),1,'first')-1));
        matrix=matrix(:,1:(find(isnan(nanmean(matrix,1)),1,'first')-1));
    end
    matste=nan(1,size(matrix,2));
    for k=1:size(matrix,2)
        matste(k)=nanstd(matrix(:,k))./sqrt(sum(~isnan(matrix(:,k))));
    end
    
    sempos=squeeze(nanmean(matrix(:,1:size(matrix,2)),1))+matste;
    semneg=squeeze(nanmean(matrix(:,1:size(matrix,2)),1))-matste;
    dum=xaxis;dum1=fliplr(-xaxis);dum1=dum1*-1;x=[dum dum1];
    dum=flipud(semneg(:));
    y=[sempos(:);dum]';
    
elseif dim==2
    if ~isempty(find(isnan(nanmean(matrix,2)),1,'first'))
        xaxis=xaxis(1:(find(isnan(nanmean(matrix,2)),1,'first')-1));
        matrix=matrix(1:(find(isnan(nanmean(matrix,2)),1,'first')-1),:);
    end
    matste=nan(size(matrix,1),1);
    for k=1:size(matrix,1)
        matste(k)=nanstd(matrix(k,:))./sqrt(sum(~isnan(matrix(k,:))));
    end

    sempos=squeeze(nanmean(matrix(1:size(matrix,1),:),2))+matste;
    semneg=squeeze(nanmean(matrix(1:size(matrix,1),:),2))-matste;
    dum=xaxis;dum1=fliplr(-xaxis);dum1=dum1*-1;x=[dum dum1];
    dum=flipud(semneg(:));
    y=[sempos(:);dum]';
    
end

hold on;
hobj = fill(x,y,face, 'FaceAlpha',alpha, 'EdgeColor',face,'EdgeAlpha',0);
hAnnotation = get(hobj,'Annotation');
hLegendEntry = get(hAnnotation,'LegendInformation');
set(hLegendEntry,'IconDisplayStyle','off')