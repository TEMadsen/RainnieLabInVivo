function [ stat ] = powridges_TEM( cfg,freq )
% POWRIDGES_TEM uses the Matlab function tfridge to identify the strongest
%   oscillation in each channel's raw power spectrum, even if it drifts across
%   frequencies over time (no penalty for changing frequencies).
%
% INPUTS:
%   cfg   = struct of configuration parameters, optional, may contain fields:
%     inputfile, outputfile, channel, trials, parameter,
%     keeptrials (default as in input), and wMultiplier (default 1)
%   freq  = struct of ft_datatype_freq, with required fields:
%     dimord, trialinfo, label, time, freq
%     powspctrm   = must be raw (unnormalized)
%     freq.cfg must contain historical data on the generating call to
%       ft_freqanalysis, as it will be searched for tapsmofrq or width to define
%       the bandwidth (w)
%
% OUTPUT:
%   stat  = struct of ft_datatype_timelock, with fields:
%     label, time, trialinfo from freq input
%     dimord    = 'rpt_chan_time' (or 'chan_time' if ~cfg.keeptrials) for all
%                 following:
%     rFreq     = peak frequency (Hz) by timebin
%     rpow      = sum of raw power within +/-w of the ridge frequency by timebin
%     rprom     = rpow ./ total raw spectral power per timebin (full prominence)
%     rplow     = rpow ./ sum of raw power below 16 Hz (low freq prominence),
%                 NaN if the ridge freq + w < 16 Hz
%     rtun      = rpow ./ sum of raw power in w-bands above & below ridge,
%                 NaN if the ridge freq is w/in 2w of the lowest available freq
%     cfg       = config structure for this analysis, will include these
%                 subfields that would otherwise be removed because FieldTrip
%                 can't automatically detect their dimord:
%         freq  = freq from freq input
%         w     = bandwidth per frequency, as used in calculation of r*,
%                 determined by cfg.wMultiplier times either of the following:
%                 - standard w for multitaper
%                 - std deviation in freq (cfg.foi/cfg.width) for wavelets
%
% written by Teresa E. Madsen, sometime before 8/30/17

%% load input variable, if necessary

if nargin < 2 || isempty(freq)
  if ~wait4server_TEM(cfg.inputfile)
    error('Server connection lost')
  end
      
  load(cfg.inputfile)
end

%% check config

if isempty(cfg) || ~isfield(cfg,'parameter') || isempty(cfg.parameter)
  if isfield(freq,'powspctrm')
    cfg.parameter = 'powspctrm';
  elseif isfield(freq,'fourierspctrm')
    cfg.parameter = 'fourierspctrm';
  else
    error('please provide cfg.parameter')
  end
end

if ~isfield(cfg,'wMultiplier') || isempty(cfg.wMultiplier)
  cfg.wMultiplier = 1;  % no scaling of the method-determined bandwidth
end

cfg.freq = freq.freq;   % these will be preserved in stat.cfg
cfg.w = cfg.wMultiplier .* ft_findcfg(freq.cfg,'tapsmofrq');   % finds field w/in nested previous cfgs
switch numel(cfg.w)
  case 0
    cfg.w = cfg.wMultiplier .* freq.freq ./ ft_findcfg(freq.cfg,'width');   % for wavelets
  case 1
    cfg.w = repmat(cfg.w,size(freq.freq));  % same w for all frequencies
  case numel(freq.freq)
    % good, do nothing
  otherwise
    error('numel(cfg.w) does not match numel(freq.freq)')
end
% FIXME:  expand this to work with other methods of ft_freqanalysis

dimord = strsplit(freq.dimord,'_');

if ~any(strcmpi(dimord,'chan'))
  assert(numel(freq.label) == 1 && (isempty(cfg) || ~isfield(cfg,'channel') ...
    || isempty(cfg.channel) || ischar(cfg.channel) || ...   % char can be 'all' or the name of one channel
    (iscell(cfg.channel) && numel(cfg.channel) == 1) || cfg.channel == 1), ...
    'multiple channels are named, but not specified in dimord')
  cfg.channel = 1;
  dimord = [{'chan'} dimord];
  freq.(cfg.parameter) = shiftdim(freq.(cfg.parameter),-1);   % adds singleton dimension
end

if ~isfield(cfg,'keeptrials') || isempty(cfg.keeptrials)
  cfg.keeptrials = any(strcmpi(dimord,'rpt'));  % if present in input, keep them in output
else
  cfg.keeptrials = istrue(cfg.keeptrials);  % convert from yes/no string to boolean
end

if ~any(strcmpi(dimord,'rpt')) % add trial dim just to keep calculations uniform
  cfg.trials = 1;
  dimord = [{'rpt'} dimord];
  freq.(cfg.parameter) = shiftdim(freq.(cfg.parameter),-1);   % adds singleton dimension
end

assert(strcmpi(strjoin(dimord,'_'),'rpt_chan_freq_time'),'dimord is not as expected')

if isempty(cfg) || ~isfield(cfg,'channel') || isempty(cfg.channel) ...
    || strcmpi(cfg.channel,'all')
  cfg.channel = 1:numel(freq.label);
elseif iscell(cfg.channel) || ischar(cfg.channel)
  cfg.channel = find(ismember(freq.label,cfg.channel));
elseif islogical(cfg.channel)
  cfg.channel = find(cfg.channel);
end   % otherwise, assume it already contains numeric indices

if isempty(cfg) || ~isfield(cfg,'trials') || isempty(cfg.trials) ...
    || strcmpi(cfg.trials,'all')
  cfg.trials = 1:size(freq.(cfg.parameter),1);
elseif islogical(cfg.trials) || ...
    (any(cfg.trials == 0) && all(cfg.trials == 0 || cfg.trials == 1))
  cfg.trials = find(cfg.trials);
end   % otherwise, assume it already contains numeric indices

%% preallocate output struct

dimsiz = [numel(cfg.trials) numel(cfg.channel) numel(freq.time)];

stat = struct('time',freq.time, 'trialinfo',freq.trialinfo(cfg.trials,:), ...
  'dimord','rpt_chan_time', 'rFreq',NaN(dimsiz), 'rpow',NaN(dimsiz), ...
  'rprom',NaN(dimsiz), 'rplow',NaN(dimsiz), 'rtun',NaN(dimsiz));
stat.label = freq.label(cfg.channel);  % cell array included above makes stat into a non-scalar structure

%% extract ridges

for tr = 1:numel(cfg.trials)
  for ch = 1:numel(cfg.channel)
    tfm = abs(squeeze(freq.(cfg.parameter)(cfg.trials(tr),cfg.channel(ch),:,:)));
    tnan = any(isnan(tfm),1);
    
    if sum(~tnan) <= 1  % input to tfridge won't be a vector
      warning(['insufficient valid timepoints for ' freq.label{cfg.channel(ch)} ...
        ', on trial with trialinfo = ' num2str(freq.trialinfo(cfg.trials(tr),:))]);
      continue  % to next channel
    end
    
    stat.rFreq(tr,ch,~tnan) = tfridge(tfm(:,~tnan),freq.freq);
    
    for t = find(~tnan)
      stat.rpow(tr,ch,t) = sum(tfm(freq.freq + cfg.w >= stat.rFreq(tr,ch,t) & ...
        freq.freq - cfg.w <= stat.rFreq(tr,ch,t), t), 1, 'includenan');
      if stat.rFreq(tr,ch,t) + cfg.w(freq.freq == stat.rFreq(tr,ch,t)) <= 16   % not "prominence" if the whole bandwidth isn't within the compared range
        stat.rplow(tr,ch,t) = stat.rpow(tr,ch,t) ./ ...
          sum(tfm(freq.freq <= 16, t), 1, 'includenan');
      end
      if stat.rFreq(tr,ch,t) > freq.freq(1) + 2*cfg.w(freq.freq == stat.rFreq(tr,ch,t))  % full range of frequencies must be available to sum
        stat.rtun(tr,ch,t) = stat.rpow(tr,ch,t) ./ ...
          (sum(tfm(freq.freq >= (stat.rFreq(tr,ch,t) - 2*cfg.w(freq.freq == stat.rFreq(tr,ch,t))) & ...
          freq.freq <= (stat.rFreq(tr,ch,t) + 2*cfg.w(freq.freq == stat.rFreq(tr,ch,t))), t), 1, 'includenan') ...
          - stat.rpow(tr,ch,t));
      end
    end
    stat.rprom(tr,ch,:) = stat.rpow(tr,ch,:) ./ shiftdim(sum(tfm,1,'includenan'),-1);
  end
end

%% remove trial dim if not specified to keep

if ~cfg.keeptrials
  fields = fieldnames(stat);
  for param = 1:numel(fields)
    if ndims(stat.(fields{param})) == 3 && ...  % field matches dimord
        size(stat.(fields{param}),2) == numel(stat.label) && ...
        size(stat.(fields{param}),3) == numel(stat.time) && ...
        (size(stat.(fields{param}),1) == 1 || ...   % check singleton first, since
        size(stat.(fields{param}),1) == size(stat.trialinfo,1))   % trialinfo may not be a field
      stat.(fields{param}) = shiftdim(stat.(fields{param}),1);   % moves 1st dim to end
      if size(stat.(fields{param}),3) > 1
        stat.(fields{param}) = mean(stat.(fields{param}),3,'omitnan');
      end
    end
  end
  stat.dimord = 'chan_time';
end

%% save cfg history

stat.cfg = cfg;
stat.cfg.previous = freq.cfg;

%% save output variable, if desired

if isfield(cfg,'outputfile')
  disp(['saving stat to ' cfg.outputfile])
  save(cfg.outputfile,'stat')
end

end
