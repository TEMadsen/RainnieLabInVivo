% this script changes the tilte of excel sheets from social interaction
% scoring into corresponding trials

%% creating cell vector of sheet names
shtname = cell (24,1,0)
i = 1
for k = 361:364
    for l = 1:2
        for m = 1:3
            shtname{i} = sprintf('%d_%d_%d',k,l,m);
            i = i+1;
        end
    end
end

%% assign new sheet names and save
xlswrite('Results_AreaOnly.xlsx',1) % # create test file
e = actxserver('Excel.Application'); % # open Activex server
ewb = e.Workbooks.Open(['smb://yrknas.yerkes.emory.edu/yerkes2/DRLab' ...
    '/Teresa/Assistants/Liz/social interaction video 2016/Results_Area' ...
    'Only.xlsx']);
 % # open file 

i = 1
for i = 1:length(shtname) 
ewb.Worksheets.Item(i).Name = 'shename{i}'; % # rename ith sheet
ewb.Save % # save to the same file
i = i+1
end

ewb.Close(false)
e.Quit