function [ filenames ] = SaveFigs_TEM( nameprefix )
% SAVEFIGS_TEM Saves all open figures
%   Saves all open figures, using the provided name prefix (can be full path to
%   a temporary figure folder, ending with filesep), the figure number, and if
%   applicable, the figure name.

if nargin < 1
  nameprefix = [fileparts(userpath) filesep 'MATLAB' filesep ...
    'Figure'];
end

fig = findall(0,'Type','Figure');
filenames = cell(size(fig));
for f = 1:numel(fig)
  filenames{f} = [nameprefix num2str(fig(f).Number) ...
    matlab.lang.makeValidName(fig(f).Name) '.fig'];
  savefig(fig(f),filenames{f},'compact');
end
end

