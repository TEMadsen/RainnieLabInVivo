function [output] = doGrp_SpecBSRRegrPhasesNonClust_OrigFear_TEM(spectype,postproctype)
% DOGRP_SPECBSRREGRPHASESNONCLUST_ORIGFEAR_TEM tests 4 corrs btwn spcgrm & BSRs
% over whole phases (i.e., recording sessions) rather than blocks of 7 tones,
% using non-cluster-based statistics (e.g., bonferroni correction for multiple
% comparisons), across all rats
%
%   This function is for group visualization & stats using
%   ft_statfun_depsamplesregrT on any spectrograms (specified by input vars)
%
% TO DO:
%   - select channels (as in doGrp_wavSpecRegr_raw_OrigFear_TEM ?) here
%   - re-select channels in doGrp_RidgeVbehav_OrigFear_TEM
%   - rename channels (as in doGrp_wavSpecRegr_raw_OrigFear_TEM ?)
%   - verify plotting code (no figures from ^ ?)
%   - after this function works, convert to doGrp_RidgeVbehav_OrigFear_TEM
%
%   Depends on global variables defined in info_origfear.
%
% written 9/14/17 by Teresa E. Madsen, Ph.D.

%% import global metadata structure

global exptMeta

if isempty(exptMeta);   info_origfear;   end

%% check inputs & assign defaults

if nargin == 0  % like when I just hit "Run and Time" in the Editor tab
  spectype      = 'piWav';        % close to FT default wavelet params, but using pi for integer # of cycles, etc.
  postproctype  = {'bandBDAmean'};  % bandBDAmean is mean dB power w/in 12 freq bands, 3x 30s BDA tone bins
  % in cell because some will require additional inputs (in a 2nd cell, e.g., {'smoothT',[6 3]})
end

%% create waitbar

wb = waitbar(0, 'Preparing to run group stats for SpecBSRRegrPhasesNonClust');

%% load operant count BDA tones and convert to BSR

BSRfile = [exptMeta.datafolder 'preprocBehav' filesep 'AllRatsBSR.mat'];

if existfile_TEM(BSRfile)
  load(BSRfile);   % BSR
else
  load(strrep(BSRfile,'BSR.mat','BDAtones.mat'));   % cntBDAtones

BSR = cellfun(@(x) (x(:,1) - x(:,2)) ./ (x(:,1) + x(:,2)), ...
  cntBDAtones(~exptMeta.ratT.excluded), 'UniformOutput',false);
BSR = [BSR{:}]';
% if count is NaN rather than 0, don't change BSR from NaN to 1
omit = cellfun(@(x) any(isnan(x),2), cntBDAtones(~exptMeta.ratT.excluded), ...
  'UniformOutput',false);
omit = [omit{:}]';
% if no operant behavior before or during tone, consider it complete suppression
BSR(isnan(BSR) & ~omit) = 1;
% if operant behavior increases in response to tone, consider it no suppression
BSR(BSR < 0) = 0;

save(BSRfile,'BSR')
end

%% choose best channels based on individual rat stats
          
for r = find(~exptMeta.ratT.excluded)'
  listing = dir([exptMeta.datafolder 'SpecBSRRegrPhasesNonClust' filesep ...
    num2str(exptMeta.ratT.ratnums(r)) filesep '*stat_indepsamplesregrT*.mat']);
  for p = 1:numel(listing)
      cfg = [];
      cfg.inputfile       = [listing(p).folder filesep listing(p).name];
      cfg.layout          = [exptMeta.configfolder 'layout_' ...
                        exptMeta.ratT.MWA{r} '_' exptMeta.ratT.side{r} '.mat'];
      cfg.parameter       = 'stat';
      cfg.maskparameter   = 'mask';
      cfg.maskstyle       = 'outline';
      cfg.showlabels      = 'yes';
      cfg.showoutline     = 'yes';  % necessary to view comment when only 1 array is present
      cfg.showscale       = 'no';   % this actually refers to an extra spectrogram that appears to be an average
      cfg.showcomment     = 'yes';
      cfg.comment         = [listing(p).name(1:23) newline ...
        'Previously selected channels:' newline ...
        exptMeta.regions{1} ' = ' exptMeta.ratT.(exptMeta.regions{1}){r} ', ' ...
        exptMeta.regions{2} ' = ' exptMeta.ratT.(exptMeta.regions{2}){r}];
      cfg.zlim            = 'maxabs';   % [-3 3];
      cfg.colormap        = jet;

      fig = figure(p); clf; fig.WindowStyle = 'docked';
      ft_multiplotTFR(cfg)
      
%       cfg.fticks = exp(1).^(0:1:5);
%       cfg.tticks = -30:30:60;
%       cfg.cticks = -4:2:4;
%       [cfg] = plotFTmatrix_TEM(cfg)
  end
  keyboard  % verify selected channels
  close all   % some rats lack 3rd phase
  % 200 - consider 15 for sig in both FC & Ext (none in Rcl)
  % 202 - consider 5 for only during tone & only delta sig (during FC)
  % 203 - no significance, no Rcl??
  % 204 - consider 12 to avoid reverse correlation with delta
  % 205 - consider 7 & 11 for greater FC significance with similar patterns to
  % previously selected channels
  % 210 - consider 4 & 11 for greater FC & Rcl sig
  % 212 - consider 2 for during FC tone sig
  % 338 - consider 7 because 4 looks weird on Rcl, similar sig
  % 342 - consider 7 & 14 for + corr. w/ gamma after FC tone/shocks, no Rcl or
  % Ext?
  % 339, 341, 343 - no significance anywhere
end

%% loop through each recording: prepping data, running stats, & plotting results
% then clear memory to avoid overload

comptime = nan(numel(exptMeta.phases)-1,3); % 3 steps: prep data, stats, & plot

if strcmp(postproctype{1},'bandBDAmean')
  bcfg = get_cfg_TEM('bandify','OrigFear');  % standard bands based on powers
  % of e + transitional bands between them (half-powers of e)
end

for p = 1:size(comptime,1)
  st = tic;   % start time for step 1: prep data
  waitbar((p-1)/(numel(exptMeta.phases)-1), wb, ['Preparing ' exptMeta.abbr{p} ...
    ' ' spectype ' ' postproctype{1} ' specs for regrT analysis']);
  
  % collect spectrograms of last 14 of each recording, except the very last, so
  % we can see how each neural response predicts the behavioral response to the
  % next tone
  tones = exptMeta.expected(p)-14:exptMeta.expected(p)-1;
  % collect next trial BSR values
  pBSR = BSR(:,sum(exptMeta.expected(1:p-1)) + tones + 1);
  omit = isnan(pBSR);   % check for missing values
  if numel(unique(pBSR(~omit))) < 3
    warning(['skipping ' exptMeta.abbr{p} ...
      ' because of < 3 unique behavioral values'])
    continue  % to next block
  end
  freq = cell(size(pBSR));   % # of rats by 14 tones per block
  rn = 0;   % start with previous rat at index 0

  for r = find(~exptMeta.ratT.excluded)'
    rn = rn + 1;  % next available row in freq, corresponding to same row in BSR
    
    for tr = 1:numel(tones)   % trial # within freq
      %% check whether trial will be omitted due to missing behavior
      
      if omit(rn,tr)  
        continue  % to next trial
      end
      
      %% define trial's neural data inputfile
    
    tn = tones(tr);   % trial # within phase
    
    inputfile = [exptMeta.datafolder postproctype{1} filesep ...
      num2str(exptMeta.ratT.ratnums(r)) filesep exptMeta.stimuli{1} num2str(tn) ...
      'of' exptMeta.abbr{p} '_raw' num2str(exptMeta.ratT.ratnums(r)) ...
      spectype 'TFAdBpow_' postproctype{1} '.mat'];
    
    if numel(postproctype) > 1
      switch postproctype{1}
        case 'smoothT'
          inputfile = insertBefore(inputfile, '.mat', ...
            [strrep(num2str(postproctype{2}(1)),'.','_') 'x' ...  % twin
            strrep(num2str(postproctype{2}(2)),'.','_')]);        % tshift
        case 'tfridge'
          inputfile = insertBefore(inputfile, '.mat', ...
            [strrep(num2str(postproctype{2}),'.','_') 'x']);      % wMult
        otherwise
          error('define modification to filename here')
      end
    end
    
    %% load inputfile, if it exists
    
    if ~existfile_TEM(inputfile)
      listing = dir(replaceBetween(inputfile, ... % get all .mat files in folder
        [num2str(exptMeta.ratT.ratnums(r)) filesep], '.mat', '*'));
      listing(~contains({listing.name},spectype)) = [];   % remove non-matches
      listing(~contains({listing.name}, ...  % check for both full & abbreviated
        [exptMeta.phases(p), exptMeta.abbr(p)])) = [];  % phase names
      if isempty(listing)
        warning(['Skipping all tones after ' num2str(tn) ' of ' ...
          exptMeta.phases{p} ' for rat #' num2str(exptMeta.ratT.ratnums(r)) ...
          ' because none of the files in ' newline ...
          inputfile(1:find(inputfile == filesep,1,'last')) ...
          ' were found to contain phase and ' spectype '.'])
        omit(rn,tr:end) = true;
        break  % out of tone loop (continue to next rat)
      else
        disp(char({listing.name}))
        warning('clarify neural inputfile')
        keyboard
      end
    end
    
    freq{rn,tr} = rmvlargefields_TEM(inputfile);
    
    %% add specs for bandBDAmean if applicable & missing
    
    if exist('bcfg','var') && ~isfield(freq{rn,tr}.cfg,'bands')
      bcfg.previous = freq{rn,tr}.cfg;
      freq{rn,tr}.cfg = bcfg;
    end
    
    %% check for excessive NaNs
    
    if sum(isnan(freq{rn,tr}.powspctrm(:)))/numel(freq{rn,tr}.powspctrm(:)) > 0.2
      if r == 1 && p == 1 && tn == 12  % known bad trial
        omit(rn,tr) = true;
      else
        warning(['powspctrm is ' num2str(round(...
          100*sum(isnan(freq{rn,tr}.powspctrm(:)))/numel(freq{rn,tr}.powspctrm(:)))) ...
          '% NaNs - omit?'])
        fig = figure('Name',[num2str(exptMeta.ratT.ratnums(r)) ...
          exptMeta.abbr{p} num2str(tn)]); clf; fig.WindowStyle = 'docked';
        fcfg = [];
        fcfg.zlim            = 'minmax';
        if exist('bcfg','var')
          fcfg.fticks        = bcfg.foi(contains(bcfg.bands,'('));
          fcfg.tticks        = bcfg.toi;
        end
        plotFTmatrix_TEM(fcfg,freq{rn,tr});
        keyboard  % omit(rn,tr) = true;
      end
    end
    
    %% select & rename channels
    
    
    
    end
  end
  comptime(p,1) = toc(st);
  
  %% make sure all rats & trials' data were prepared
  
  st = tic;   % start time for stats
  if ~omit(cellfun(@isempty,freq))  % if omit is not true where freq is empty
    warning('missing rat/trial not marked for omission');
    keyboard  % figure out why
  end
  badr = all(omit,2);   % find rat(s) with all trials omitted for this phase
  badtr = any(omit(~badr,:),1);   % find trial(s) missing data from any non-omitted rats
  if sum(badr) > 1 || sum(~badtr) < 13
    if sum(~badtr) == 0
      error('no trials present')
    end
    warning([exptMeta.abbr{p} ' only has ' ...
      num2str(sum(~badtr)) ' trials'])
    keyboard  % figure out why, skip if < 3 good trials
  end
  
  %% define parameters of stat test & name outputfile
  
  cfg = [];
  cfg.statistic         = 'ft_statfun_depsamplesregrT';
  cfg.method            = 'montecarlo';
  cfg.correctm          = 'max'; 
  cfg.tail              = 0;
  cfg.alpha             = 0.025;
  cfg.numrandomization  = 1000;
  cfg.parameter         = 'powspctrm';
  
  I = find(~omit);
  [cfg.design(2,:),~]   = ind2sub(size(omit),I);
  cfg.design(1,:)       = pBSR(I);
  
  cfg.ivar    = 1;  % independent variable = BSR to the *NEXT* tone
  cfg.uvar    = 2;  % dependent variable = subject #
  
  cfg.outputfile  = [exptMeta.datafolder 'SpecBSRRegrPhasesNonClust' filesep ...
    exptMeta.abbr{p} inputfile(strfind(inputfile,'_raw'):end-4) ...
    '_vs_NextTrlBSR_stat' erase(cfg.statistic,'ft_statfun') '.mat'];
  if isfield(cfg,'correctm')
    switch cfg.correctm
      case 'cluster'
        if isfield(cfg,'clusterthreshold') && ...
            strcmp(cfg.clusterthreshold,'nonparametric_individual')
          cfg.outputfile = insertBefore(cfg.outputfile,'.mat','_NPindiv');
        end
        if isfield(cfg,'orderedstats') && istrue(cfg.orderedstats)
          cfg.outputfile = insertBefore(cfg.outputfile,'.mat','_ordered');
        end
      otherwise
        cfg.outputfile = insertBefore(cfg.outputfile,'.mat',['_' cfg.correctm]);
    end
  end
  
  %% run stat test
  
  if existfile_TEM(cfg.outputfile)
    stat = rmvlargefields_TEM(cfg.outputfile);
  else
    waitbar((p-2/3)/(numel(exptMeta.phases)-1), wb, ...
      ['Running ' erase(cfg.statistic,'ft_statfun_') ' on ' exptMeta.abbr{p} ...
      ' ' spectype ' vs. next trial BSR']);
    
    stat = ft_freqstatistics(cfg, freq{I});
    comptime(p,2) = toc(st);  % record time only if stats were calculated, not just loaded
  end
  
  %% print & plot significant correlations of spectrograms & BSR
  
  st = tic;
  waitbar((p-1/3)/(numel(exptMeta.phases)-1), wb, ...
    ['Running ' erase(stat.cfg.statistic,'ft_statfun_') ' on ' exptMeta.abbr{p} ...
    ' ' spectype ' vs. next trial BSR for rat #' num2str(exptMeta.ratT.ratnums(r))]);
  
  disp([newline 'Found chan/freq/time bins with significant positive (' ...
    num2str(sum(stat.mask(:) & stat.stat(:) > 0)) ') and negative (' ...
    num2str(sum(stat.mask(:) & stat.stat(:) < 0)) ')' newline ...
    'correlations with next trial BSR within ' exptMeta.abbr{p} newline])
  
  cfg =[];
  cfg.parameter = 'stat';
  xfit = linspace(min(stat.cfg.design),max(stat.cfg.design),15);
  
  %% visualize significant correlations
  
  if any(stat.mask)  
    sigT = table(find(stat.mask),'VariableNames',{'I'});
  else % if no significant correlations, just find strongest/closest to significance
    sigT = table(find(stat.prob == min(stat.prob(:)) | ...
      stat.stat == min(stat.stat(:)) | stat.stat == max(stat.stat(:))), ...
      'VariableNames',{'I'});
  end
  [sigT.stat,sortOrd] = sort(stat.stat(sigT.I));  % sort in stat order so colors will reflect red(+) blue(-) correlations
  sigT.I = sigT.I(sortOrd);
  sigT.prob = stat.prob(sigT.I);
  if size(sigT,1) > 30  % only plot the strongest correlations
    sigT = sigT([1:15,end-14:end],:);
  end
    
  [sigT.ch,sigT.f,sigT.t] = ind2sub(size(stat.mask),sigT.I);
    sigT.chlabel = stat.label(sigT.ch);
    if exist('bcfg','var') && isequal(stat.freq(:),bcfg.foi(:))
      sigT.flabel = strip(bcfg.bands(sigT.f));
    else
      sigT.flabel = strip(cellstr(num2str(stat.freq(sigT.f)',3)));
    end
    if exist('bcfg','var') && isequal(stat.time(:),bcfg.toi(:))
      sigT.tlabel = exptMeta.intervals(sigT.t);
    else
      sigT.tlabel = strip(cellstr(num2str(stat.time(sigT.t)',3)));
    end
    sigT.powpertrl = nan(numel(sigT.I),numel(freq));   % aggregate cluster power over trials
    for tr = find(~omit)'
      sigT.powpertrl(:,tr) = freq{rn,tr}.powspctrm(sigT.I);
    end
    sigT.powpertrl(:,omit) = [];
    sigT.prob = stat.prob(sigT.I);
    CC = rowfun(@(x) corrcoef(stat.cfg.design, x), sigT, ...
      'InputVariables','powpertrl', 'OutputFormat','cell');
    sigT.CC = cellfun(@(x) x(2), CC); %, 'OutputFormat','uniform');
    sigT = sortrows(sigT,'CC','descend');
    sigT.color = mat2cell(flipud(jet(size(sigT,1))),ones(size(sigT,1),1));
    fitT = rowfun(@(x) polyfit(stat.cfg.design, x, 2), sigT, ...
      'InputVariables','powpertrl', 'OutputVariableNames',{'pc','ErrorEst'}); % fit square model
    sigT.pop_fit = cell2mat(rowfun(@(x1,x2) polyval(x1, xfit, x2), fitT, ...
      'InputVariables',{'pc','ErrorEst'}, 'OutputFormat','cell')); % evaluate model at various X values

    fig = figure('Name',[exptMeta.abbr{p} ' Correlations(s)']); clf; 
    fig.WindowStyle = 'docked'; fig.PaperPositionMode = 'auto';
      % plot next trl BSR vs. pow for each chan/freq/time bin
    h = plot(stat.cfg.design(:),sigT.powpertrl','o:', ...
      xfit(:),sigT.pop_fit','-');
    [h(1:size(sigT,1)).Color] = sigT.color{:};
    [h(size(sigT,1)+1:end).Color] = sigT.color{:};
    axis tight; xlabel('BSR in response to *NEXT* Tone'); ylabel('Power (dB)');
    legend(join([sigT.chlabel, sigT.flabel, sigT.tlabel, ...
      strip(cellstr(num2str(sigT.CC,3))), strip(cellstr(num2str(sigT.prob,3)))], ...
      {', ',', ',' Tones, coeff = ',', p = '}), 'Location','NorthEastOutside')
    
    title(['Strongest correlations with next trial BSR within ' ...
      exptMeta.abbr{p}])
    
    print([exptMeta.figurefolder 'StrongCorr_' ...
      stat.cfg.outputfile(find(stat.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
      '-dpng','-r0')
    %     close(fig)
  
  %% plot stat
  
  fig = figure('Name',exptMeta.abbr{p}); clf; fig.WindowStyle = 'docked';
  cfg=[];
  cfg.channel = cell(0,1);
  % get best channel for negative correlations...
  [~,I] = min(stat.stat(1:numel(exptMeta.lftchs),:,:));
  cfg.channel(end+1,1)        = stat.label(mode(I(:)));
  % ...in each region...
  if numel(stat.label) > numel(exptMeta.lftchs)
    [~,I] = min(stat.stat(numel(exptMeta.lftchs)+1:end,:,:));
    cfg.channel(end+1,1)      = stat.label(numel(exptMeta.lftchs) + mode(I(:)));
  end
  % ...and for positive correlations...
  [~,I] = max(stat.stat(1:numel(exptMeta.lftchs),:,:));
  cfg.channel(end+1,1)        = stat.label(mode(I(:)));
  % ...in each region
  if numel(stat.label) > numel(exptMeta.lftchs)
    [~,I] = max(stat.stat(numel(exptMeta.lftchs)+1:end,:,:));
    cfg.channel(end+1,1)      = stat.label(numel(exptMeta.lftchs) + mode(I(:)));
  end
  cfg.channel = unique(cfg.channel);  % also sorts them
  if isempty(cfg.channel)   % this shouldn't happen, but is an alternate chsel
    cfg.channel = [exptMeta.ratT.mPFC(r); exptMeta.ratT.BLA(r)];
  end
  cfg.zlim            = 'maxabs';
  cfg.parameter       = 'stat';
  cfg.maskparameter   = 'mask';
  cfg.maskstyle       = 'outline';
  cfg.feedback        = 'no';
  if exist('bcfg','var')
    cfg.fticks        = bcfg.foi(contains(bcfg.bands,'('));
    cfg.tticks        = bcfg.toi;
  end
  plotFTmatrix_TEM(cfg,stat);
  
  fig.PaperPositionMode = 'auto';
  print([exptMeta.figurefolder 'CorrSpectrogram_' ...
    stat.cfg.outputfile(find(stat.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
    '-dpng','-r0')
  %   close(fig)
  
  comptime(p,3) = toc(st);
end   % for each block

%% clean up

output = comptime;
close(wb);
% keyboard  % pause to review figures

end   % function
