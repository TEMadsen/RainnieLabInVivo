%% select channels based on nancnt in same timebin across trials within a block

CleanSlate  % provides a clean slate to start working with new data/scripts

wb = waitbar(0,'Initializing...');   % will display progress through loop

info_origfear   % load all background info & final parameters

gen = get_cfg_TEM('gen', 'OrigFear');  % collect general parameters of interest
mtmc = get_cfg_TEM('mtmconvol', 'OrigFear');  % and parameters for analysis

zthresh   = 15;     % use data cleaned with this artifact threshold
cthresh   = 3;      % and this clipping threshold
redo      = false;  % to ignore & overwrite old files
skiprats  = [];     % already processed or running on another computer

artT = table([],[],[],[],[],[],[], ...
  'VariableNames',{'Rat','Channel','BL_ArtifactFree','Tone_ArtifactFree', ...
  'SubtleArts','NoticeableArts','VisibleNaNs'});

%% full loop

for r = setdiff(find(~ratT.excluded)',skiprats)
  %% define inputfile
  
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllClean_z' num2str(zthresh) '_clip' num2str(cthresh) ...
    '_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];
  
  %% check for input data
  
  if ~existfile_TEM(inputfile)
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ...
      ' because ' inputfile ' was not found.'])
    continue  % to next rat
  end
  
  %% load input data (all chans, tone trials, artifacts replaced w/ nan)
  
  waitbar((r-1)/size(ratT,1), wb, ...
    ['Loading allch data for rat #' num2str(ratT.ratnums(r))]);

  data = rmvlargefields_TEM(inputfile);
  
  %% preallocate table
  
  RartT = table(repmat(ratT.ratnums(r),numel(data.label),1), data.label, ...
    nan(numel(data.label),1), nan(numel(data.label),1), ...
    nan(numel(data.label),1), nan(numel(data.label),1), ...
    nan(numel(data.label),1), 'VariableNames',artT.Properties.VariableNames);
  
  %% separate baseline
  
  cfg = [];
  cfg.trials = data.trialinfo(:,2) == 0;  % where no CS
  
  data1 = ft_selectdata(cfg,data);
  
  %% detect artifact free times
  
  nancnt = movmax(isnan([data1.trial{:}]),floor(mtmc.pad*fsample),2);
  
  for ch = 1:numel(data.label)
    RartT.BL_ArtifactFree(ch) = sum(~nancnt(ch,:))/numel(nancnt(ch,:));
  end
  
  %% select core period of tone trials
  
  cfg = [];
  cfg.trials = data.trialinfo(:,2) == 1;  % where CS+
  cfg.toilim = [-45 75];  % [median(cellfun(@min,data.time)), ...
%     median(cellfun(@min,data.time)) + mtmc.pad];
  
  data1 = ft_redefinetrial(cfg,data);
  
  %% make sure time axes are uniform
  
  tpts = cellfun(@numel,data1.time);
  while any(tpts ~= tpts(1))
    tr = find(tpts ~= tpts(1),1,'first');
    if tpts(tr) > tpts(1)
      ltr = tr;   % long trial #
      shtr = 1;   % short trial #
    elseif tpts(tr) < tpts(1)
      ltr = 1; shtr = tr;
    end
      [tf,I] = ismembertol(data1.time{shtr},data1.time{ltr}, ...
        1/(3*fsample), 'DataScale', 1);
      if ~all(tf)
        error('not all times in the shorter trial are found in the longer trial')
      end
      tmptime = data1.time{shtr};
      tmptrial = data1.trial{shtr};
      data1.time{shtr} = data1.time{ltr};
      data1.trial{shtr} = nan(size(data1.trial{ltr}));
      data1.time{shtr}(I) = tmptime;
      data1.trial{shtr}(:,I) = tmptrial;
      tpts = cellfun(@numel,data1.time);
  end
  trlmat = cat(3,data1.trial{:});
  
  %% mask times with NaNs on all chs
  
  shockt = repmat(movmax(all(isnan(trlmat),1), ...
    ceil(mtmc.t_ftimwin(1)*fsample),2),[numel(data.label), 1, 1]);
  trlmat(shockt) = zeros;
  
  %% count remaining NaNs
  
  nancnt = movmean(movmax(isnan(trlmat),ceil(mtmc.t_ftimwin(1)*fsample),2),3,3);
  for ch = 1:numel(data.label)
%     fig = figure(ch); clf; fig.WindowStyle = 'docked';
%     imagesc(data1.time{1},1:numel(data1.trial),squeeze(nancnt(ch,:,:))');
%     colormap(jet); colorbar;
    RartT.Tone_ArtifactFree(ch) = sum(sum(nancnt(ch,:,:) == 0,3),2) ...
      /numel(nancnt(ch,:,:));
    RartT.SubtleArts(ch) = sum(sum(nancnt(ch,:,:) > 0 & nancnt(ch,:,:) < 0.5,3),2) ...
      /numel(nancnt(ch,:,:));
    RartT.NoticeableArts(ch) = sum(sum(nancnt(ch,:,:) > 0.5 & nancnt(ch,:,:) < 1,3),2) ...
      /numel(nancnt(ch,:,:));
    RartT.VisibleNaNs(ch) = sum(sum(nancnt(ch,:,:) == 1,3),2) ...
      /numel(nancnt(ch,:,:));
  end
  fig = figure(r); clf; fig.WindowStyle = 'docked';
  plot(RartT.SubtleArts,'DisplayName','SubtleArts'); hold all;
  plot(RartT.NoticeableArts,'DisplayName','NoticeableArts');
  plot(RartT.VisibleNaNs,'DisplayName','VisibleNaNs');
  yyaxis right; plot(RartT.BL_ArtifactFree,'DisplayName','BL_ArtifactFree'); 
  hold all; plot(RartT.Tone_ArtifactFree,'DisplayName','Tone_ArtifactFree'); 

  artT = [artT; RartT]; %#ok<AGROW>
end

clearvars dat* -except datafolder

writetable(artT,[datafolder 'allrats_selectChsbyNaNs.csv'])