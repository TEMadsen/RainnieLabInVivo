%% Plots spectrograms in a topographic arrangement

% CleanSlate  % provides a clean slate to start working with new data/scripts

wb = waitbar(0,'Initializing...');   % will display progress through loop

info_origfear   % load all background info & final parameters

spectype      = 'mtmfft';   % w/ overlapping trials as moving windows
winlen        = 2;          % movwin(1) = length of moving windows
redo          = true;       % to ignore & overwrite old files
skiprats      = 1:8;         % already processed or running on another computer

errlog  = cell(size(ratT,1), sum(expected));   % MEs saved in rat-trl cells
rattime = NaN(size(ratT,1), 1);   % will save computation time per rat

outputagg     = [datafolder 'powridges' num2str(winlen) 's_agg.mat'];
outputstats   = [datafolder 'powridges' num2str(winlen) 's_stats.mat'];

chclrs = jet(16)*0.9;

%% load stats

if ~wait4server_TEM(datafolder)
  error('Server connection lost')
end
load(outputagg)     % ridgeT table
tallridge = tall(ridgeT);
clearvars ridgeT

load(outputstats)   % posthoc structure

%% get descriptive stats

% tic
% statarray = grpstats(tallridge,{'rat','channel','block','reltime'}, ...
%   {'all-stats'}, 'DataVars',{'fridge','rprom'})
% toc
% % statarray =
% %   8,132�34 tall table
% %           GroupLabel          rat    channel    block    reltime    GroupCount    numel_fridge    count_fridge    nnz_fridge    min_fridge    max_fridge    range_fridge    mean_fridge     meanci_fridge         predci_fridge        std_fridge    var_fridge    skewness_fridge    kurtosis_fridge    sem_fridge    numel_rprom    count_rprom    nnz_rprom    min_rprom    max_rprom    range_rprom    mean_rprom       meanci_rprom          predci_rprom       std_rprom    var_rprom    skewness_rprom    kurtosis_rprom    sem_rprom
% % Elapsed time is 310.403268 seconds.
% any(statarray.GroupCount ~= statarray.nnz_fridge)
% % ans =
% %   tall logical
% %    0

% waitbar(0,wb, 'calculating descriptive statistics - may take up to 5 minutes')
% tic
% statarray = grpstats(tallridge,{'rat','channel','block','reltime'}, ...
%   {'mean','std','kurtosis'}, 'DataVars',{'fridge','rprom'}) %#ok<NOPTS> output variable not created when terminated with ; !?
% toc   % Elapsed time is 253.040767 seconds.

%% view distribution of group means to decide which groups to focus on

fig = figure(size(ratT,1)*2+1); clf; fig.WindowStyle = 'docked';
boxplot(gather(statarray.mean_rprom), ...
  {gather(statarray.block),gather(statarray.reltime)}, ...
  'Colors','brg', 'Jitter',1, 'Notch','on', 'PlotStyle','compact', 'Whisker',0.1)
% best comparisons across all rats & channels:
% Hab3 vs. Acq2 During (grp #8 & 14, comp #293)
% Rcl1 Before vs. During (grp #16 & 17, comp #556)
grps = [8, 14, 16:17];  colors = {'g','m','b','r'};
comps = [293, 556];   cmpname = {'During_Hab3_vs_Acq2','Before_vs_During_Rcl1'};
blks = [3, 5, 6, 6];    % to visualize progression over time w/in whole trials
relts = [2, 2, 1, 2];   % to mark specific times of interest for each
labels = join([intervals(relts), blknames(blks)']);

%% full loop

for r = setdiff(find(~ratT.excluded)',skiprats)
  waitbar((r-1)/size(ratT,1) ,wb, 'comparing channels for each rat')
  
  ratstats = sortrows(gather(statarray(statarray.rat == ratT.ratnums(r),:)), ...
    {'channel','block','reltime'});
  
  %% plot summary for this rat
  
  fig = figure(r); clf; fig.WindowStyle = 'docked';
  
%   kedges = linspace(floor(min(ratstats.kurtosis_rprom)),...
%     ceil(max(ratstats.kurtosis_rprom)),10);
%   cedges = linspace(floor(min(ratstats.GroupCount(~ismember(ratstats.block,{'BL','Hab1','Acq1'})))),...
%     ceil(max(ratstats.GroupCount(~ismember(ratstats.block,{'BL','Hab1','Acq1'})))),10);
%   
  % find p-values for main & interaction effects and effect sizes for posthoc comparisons
  % effect size (ES) = (mean(high fear) - mean(low fear)) / std(low fear)
  
  % don't bother aggregating across rats, since the selection needs to be made
  % among each rat's channels
  
  rpromT = table(repmat(ratT.ratnums(r),size(allchs)), allchs, ...
    nan(size(allchs)), nan(size(allchs)), nan(size(allchs)), ...
    nan(size(allchs)), nan(size(allchs)), nan(size(allchs)), ...
    nan(size(allchs)), nan(size(allchs)), nan(size(allchs)), ...
    'VariableNames',{'rat','channel',posthoc.rprom.anovaT{1,1}{2:3,1}, ...
    'Interaction', 'p_During_Hab3_vs_Acq2', 'ES_During_Hab3_vs_Acq2', ...
    'p_Before_vs_During_Rcl1', 'ES_Before_vs_During_Rcl1', 'MaxKurtosis', ...
    'MeanCount'});
  
  chndx = [];   % will hold numeric indices for channel of each row of ratstats
  
  for ch = find(ismember(allchs,ratstats.channel))'
      chstats = ratstats(ratstats.channel == allchs{ch},:);   % so row #s == grps
      chndx = [chndx; repmat(ch,size(chstats,1),1)]; %#ok<AGROW>
      ES = [(chstats.mean_rprom(grps(2)) - chstats.mean_rprom(grps(1))) / ...
        chstats.std_rprom(grps(1)), ...
        (chstats.mean_rprom(grps(4)) - chstats.mean_rprom(grps(3))) / ...
        chstats.std_rprom(grps(3))];
      rpromT(ch,3:end) = {posthoc.rprom.anovaT{ch,r}{2:4,7}, ... % 3 ANOVA p-values
        posthoc.rprom.pvals(comps(1),ch,r), ES(1), ...
        posthoc.rprom.pvals(comps(2),ch,r), ES(2), max(chstats.kurtosis_rprom), ...
        mean(chstats.GroupCount(~ismember(chstats.block,{'BL','Hab1','Acq1'})))};
  
      subplot(2,4,1:4);
    errorbar(chstats.mean_rprom, chstats.std_rprom./sqrt(chstats.GroupCount), ...
      'Color',chclrs(ch,:))
    hold all
  end
  
  xticks(2:3:(numel(blknames)*numel(intervals))); xticklabels(blknames);
  xtickangle(45); ylabel('Mean +/- SEM'); title('Ridge Prominence')
  legend(cellstr(unique(ratstats.channel)),'Location','EastOutside')
  
  subplot(2,4,5);
  histogram2(ratstats.kurtosis_rprom, chndx, numel(allchs), ...
    'DisplayStyle','tile')
  xlabel('Kurtosis of Ridge Prom.'); ylabel('Channel #');
  
  subplot(2,4,6);
  histogram2(ratstats.GroupCount(~ismember(ratstats.block,{'BL','Hab1','Acq1'})), ...
    chndx(~ismember(ratstats.block,{'BL','Hab1','Acq1'})), numel(allchs), ...
    'DisplayStyle','tile')
  xlabel('Non-NaN Time Bins / 30s'); ylabel('Channel #');
  
  subplot(2,4,7);
  histogram2(ratstats.mean_rprom, chndx, numel(allchs), 'DisplayStyle','tile')
  xlabel('Ridge Prominence'); ylabel('Channel #');
   
  subplot(2,4,8);
  gscatter(rpromT.ES_During_Hab3_vs_Acq2, rpromT.ES_Before_vs_During_Rcl1, ...
    {rpromT.channel}, chclrs); 
  xlabel('ES During Hab3 vs Acq2'); ylabel('ES Before vs During Rcl1');
  axis tight; legend off; grid on;

%   subplot(2,3,6);
%   gscatter(ratstats.mean_fridge, ratstats.mean_rprom, ...
%     {ratstats.channel},chclrs); xlabel('Ridge Freq (Hz)'); ylabel('Ridge Prom');
%   legend off

  % %% visualize largest differences per channel
  %
  % for ch = 1:numel(allchs)
  %   fig = figure(size(ratT,1)+ch); clf; fig.WindowStyle = 'docked';
  %
  %   rchT = gather(tallridge(tallridge.rat == ratT.ratnums(r) & tallridge.channel == allchs{ch} & ...
  %     ismember(tallridge.block,blknames(blks)),:));
  %   for tr = unique(rchT.trial)'
  %     % plot whole trials as black lines
  %     trI = find(rchT.trial == tr);
  %     plot3(rchT.fridge(trI),rchT.rprom(trI),rchT.time(trI),'k:');
  %     hold all
  %   end
  %
  %   view(2); h = cell(size(grps));
  %
  %   for g = 1:numel(grps)
  %     % plot each grp as color-coded circles
  %     trI = find(rchT.block == blknames{blks(g)} & ...
  %       rchT.reltime == intervals{relts(g)});
  %     h{g} = scatter3(rchT.fridge(trI),rchT.rprom(trI),rchT.time(trI),36,colors{g}, ...
  %       'DisplayName',[intervals{relts(g)} ' ' blknames{blks(g)}]);
  %   end
  %
  %   zlabel('Time from Tone Onset (s)'); xlabel('Ridge Frequency (Hz)')
  %   ylabel('Ridge Prominence (ridge/total power)')
  %   title(['ANOVA interaction effect p < 10^{-' num2str(floor(-log10(rpromT.Interaction(ch)))) ...
  %     '}, posthoc p''s < 10^{-' num2str(floor(-log10(posthoc.rprom.pvals(comps,ch,r)'))) '}'])
  %   legend([h{:}],posthoc.gnames(grps),'Location','Best');
  %
  %   print([figurefolder num2str(ratT.ratnums(r)) allchs{ch} spectype ...
  %             num2str(winlen) 's_fridge_vs_rprom'],'-dpng')
  % end
  
  %% put back in FieldTrip format
  
  timelock = [];
  timelock.dimord           = 'chan_time';
  timelock.label            = cellstr(unique(ratstats.channel));
  timelock.time             = 1:(size(ratstats,1)/numel(timelock.label));   % groups + BL
  timelock.mean_rprom       = reshape(ratstats.mean_rprom,[],numel(timelock.label))';
  timelock.std_rprom        = reshape(ratstats.std_rprom,[],numel(timelock.label))';
  timelock.kurtosis_rprom   = reshape(ratstats.kurtosis_rprom,[],numel(timelock.label))';
  
  timelock = ft_datatype_timelock(timelock);  % as output from ft_timelockanalysis
  
  %% plot topographically
  
  fig = figure(size(ratT,1)+r); clf; fig.WindowStyle = 'docked';
  
  cfg = [];
  cfg.layout          = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  cfg.colormap        = jet;
  cfg.parameter       = 'mean_rprom';
  cfg.xlim            = grps(2)-0.5:0.5:grps(end)+0.5;  % can't get this to do what I want, so just look at every other subplot
  cfg.baseline        = [7.5 8.5];  % during habituation tones 8-10
  cfg.gridscale       = numel(allchs)+1;
  cfg.interpolatenan  = 'no';
  
  ft_topoplotER(cfg,timelock)
  
  %% sort rpromT by lowest p-values & MaxKurtosis, highest EffectSizes & Count
  
  disp(sortrows(rpromT, {'ES_During_Hab3_vs_Acq2','ES_Before_vs_During_Rcl1', ...
    'MaxKurtosis','MeanCount','Interaction','block','reltime', ...
    'p_During_Hab3_vs_Acq2','p_Before_vs_During_Rcl1'}, ...
    {'descend','descend','ascend','descend','ascend','ascend','ascend',...
    'ascend','ascend'}))
  
  %% pause to select channels & save in info_origfear
  
  keyboard
  
  % %% save figure
  %
  % print([figurefolder num2str(ratT.ratnums(r)) ...
  %   'topoplot_mean_rprom_grp14-17vs8_'],'-dpng');
  
end % rat

%% confirm with ridge frequency vs. tuning

for r = find(~ratT.excluded)'
  figure;
  %%% THIS CAUSES JAVA ERRORS - REDUCE TO DISTRIBUTION SOMEHOW %%%
%   gscatter(gather(tallridge.fridge(tallridge.rat == ratT.ratnums(r))), ...
%     gather(tallridge.rtun(tallridge.rat == ratT.ratnums(r))), ...
%     {gather(tallridge.channel(tallridge.rat == ratT.ratnums(r)))}, chclrs);
end