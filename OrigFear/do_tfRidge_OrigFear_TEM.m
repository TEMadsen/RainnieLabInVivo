function [output] = do_tfRidge_OrigFear_TEM( r, spectype, wMult, plt )
% DO_TFRIDGE_ORIGFEAR_TEM detects the dynamic peak frequency of any TFA,
% specified by spectype and rat # (r), smoothed by the freqanalysis method's
% default w times any wMult (1 if not provided), and plots results if desired
% (plt is an optional boolean flag)
%
%   Depends on global variables defined in info_origfear.
%
% written 8/17/17 by Teresa E. Madsen, Ph.D.

%% import global metadata structure

global exptMeta

if isempty(exptMeta);   info_origfear;   end

%% check for input

if nargin == 0  % like when I just hit "Run and Time" in the Editor tab
  r         = 1;        % poster rat
  spectype  = 'piWav';  % like FT default wav params, but using pi for integer # of cycles, etc.
  wMult     = 1.5;      % tfRidge bandwidth multiplier
elseif nargin == 2 && iscell(spectype)  % as input from fullFTpipeline
  plt = spectype{3};   
  wMult = spectype{2};   
  spectype = spectype{1};
end

%% create waitbar

wb = waitbar(0, ['Preparing to detect ' num2str(wMult) 'x frequency ridges in ' ...
  spectype ' for rat #' num2str(exptMeta.ratT.ratnums(r))]);

%% loop through all .mat files in spectype folder

listing = dir([exptMeta.datafolder spectype filesep num2str(exptMeta.ratT.ratnums(r)) filesep ...
  '*.mat']);
comptime  = nan(size(listing));     % for each file
esttime   = 130;                    % estimated comptime (s) per file
% esttime = nanmean(comptime);      % average of past calculations

for fn = 1:numel(listing)
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/(esttime.*numel(comptime)), wb, ...
    ['detecting ' num2str(wMult) 'x tfRidge in ' listing(fn).name]);
  
  %% load inputfile, skipping dB files
  
  inputfile = [listing(fn).folder filesep listing(fn).name];
  if contains(inputfile,'dB')
    continue  % to next file, tfRidge needs raw power
  end
  if contains(inputfile,'fourier') && ...
      existfile_TEM(strrep(inputfile,'fourier','powspctrm'))
    inputfile = strrep(inputfile,'fourier','powspctrm');
  end
  
  %% define & check for outputfile
  
  outputfile = strrep(inputfile,[filesep spectype filesep],...
    [filesep 'tfRidge' filesep]);   % change folder and filename
  outputfile = replace(outputfile,exptMeta.phases,exptMeta.abbr);
  outputfile = strrep(outputfile,'waveletTFA','TFA');
  outputfile = strrep(outputfile,'fourier','pow');
  outputfile = strrep(outputfile,'.mat', ...
    ['_' spectype '_' strrep(num2str(wMult),'.','_') 'x_tfRidge.mat']);
  
  if existfile_TEM(outputfile)
    warning(['outputfile already exists: ' outputfile(numel(exptMeta.datafolder):end)])
    continue  % skip to next file
  end
  
  freq = rmvlargefields_TEM(inputfile);
  
  %% convert to powspctrm if necessary
  
  if contains(inputfile,'fourier')
    freq = ft_checkdata(freq,'cmbrepresentation','sparsewithpow');
  end
  
  switch freq.dimord
    case {'rpt_chan_freq_time','rpttap_chan_freq_time'}
      freq.powspctrm = shiftdim(freq.powspctrm,1);  % wrap trial dim to end
      freq.dimord = 'chan_freq_time';
      if contains(listing(fn).name,'Baseline')
        freq.time = freq.time(1):mean(diff(freq.time)): ...
          (freq.time(end)*size(freq.powspctrm,4));  % extrapolate time axis out (probably longer than necessary)
        freq.powspctrm = reshape(freq.powspctrm, ...
          size(freq.powspctrm,1),size(freq.powspctrm,2),[]);  % concatenate all time into 1 trial
        freq.time = freq.time(1:size(freq.powspctrm,3));  % trim to correct number of samples
      else
        assert(size(freq.powspctrm,4) == 1, 'multiple trials not expected except during baseline')
      end
    case 'chan_freq_time'
      % okay, do nothing
    otherwise
      error('unexpected dimord')
  end
  
  if any(isnan(freq.powspctrm(:))) && ...             % when there are any NaNs,
      all(cellfun(@isempty,regexp(listing(fn).name, ...   % and the filename
      {'CS\+12ofConditioning\w*200\w*\.mat', ...      % doesn't match either of
      'CS\+12ofConditioning\w*338\w*\.mat'}, 'once')))  % these known exceptions
    warning('NaNs in converted powspctrm')
    figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
    keyboard  % pause to verify
  end
  % don't replace shock artifact with NaNs, as it will be easier to tell where
  % the artifact screws up the ridge detection without them (always drops to the
  % bottom few frequencies)
  
  %% detect frequency ridges
  
  cfg = [];
  cfg.wMultiplier   = wMult;
  
  stat = powridges_TEM(cfg,freq);
  
  %% visualize & save plots
  
  if plt
    t30 = find(stat.time - stat.time(1) > 30,1,'first');
    tnan = isnan(squeeze(stat.rFreq(1,1,:)));
    clim = [0 10.*log10(median(max(stat.rpow(1,:,...
      [1:find(stat.time > 15,1,'first') find(stat.time > 45,1,'first'):end]),[],3)))];
    parameter = {'rFreq','rpow','rprom','rplow','rtun'};
    figfiles = strrep(outputfile, [exptMeta.datafolder 'tfRidge' filesep ...
      num2str(exptMeta.ratT.ratnums(r)) filesep], exptMeta.figurefolder);
    figfiles = strrep(figfiles, '.mat', '_chname');   % will be replaced with actual channel name & have .png appended
    rf = unique(2.^fix(log2(freq.freq)));   % rounded to next power of 2 closer to 2^0
    rfi = zeros(size(rf));
    for f = 1:numel(rf)
      [~,rfi(f)] = min(abs(freq.freq - rf(f)));  % rounded frequency index
    end
    wndx = zeros(numel(freq.freq),2);  % upper & lower bounds of bandwidth, as indices into freq.freq
    for f = 1:numel(freq.freq)
      [~,wndx(f,1)] = min(abs(freq.freq - (freq.freq(f) - stat.cfg.w(f))));
      [~,wndx(f,2)] = min(abs(freq.freq - (freq.freq(f) + stat.cfg.w(f))));
    end
    for ch = find(ismember(stat.label',[exptMeta.ratT.mPFC(r) exptMeta.ratT.BLA(r)]))
      paramchmat = squeeze(stat.(parameter{1})(1,ch,~tnan));
      for param = 2:numel(parameter)
        paramchmat = [paramchmat, squeeze(stat.(parameter{param})(1,ch,~tnan))]; %#ok<AGROW>
      end
      [R,P] = corrcoef(paramchmat);
      fig = figure(ch); clf; fig.WindowStyle = 'docked';
      subplot(numel(parameter)+2,3,1:3:numel(parameter)*3);
      [S,AX,~,H,~] = plotmatrix(paramchmat);
      for param = 1:numel(parameter)
        H(param).NumBins = 15;
        AX(1,param).Title.String = parameter{param};
        AX(param,1).YLabel.String = parameter{param};
        for p2 = 1:numel(parameter)
          axis(AX(param,p2),'tight');
          AX(param,p2).FontSize = 8;
          if P(param,p2) < 0.025/(numel(stat.label)*numel(P)) % two-tailed w/ Bonferroni correction for multiple comparisons
            lgd = legend(S(param,p2),['R = ' num2str(R(param,p2),2)]);
            lgd.FontSize = 7;
            lgd.Location = 'northwest'; drawnow;
            lgd.Position = lgd.Position + [-0.033 0.015 0 0];
          end
        end
        tpax = subplot(numel(parameter)+2,3,param*3);
        plot(stat.time(~tnan),paramchmat(:,param)); axis tight
        tpax.FontSize = 8;
        if stat.time(end) < 70  % assume this is a tone-triggered trial
          tpax.XTick = -60:15:60;
          if param == numel(parameter)
            tpax.XLabel.String = 'Time from Tone Onset (s)';
          end
        end
        tpax.YLabel.String = parameter{param};
        
        % split at NaNs before calculating mean +/- std autocorr
        noNaNchunks = {paramchmat(:,param)}; chunkn = 1;
        while any(isnan(noNaNchunks{chunkn}))
          noNaNchunks{chunkn} = noNaNchunks{chunkn}(...
            find(~isnan(noNaNchunks{chunkn}),1,'first'):end);
          if any(isnan(noNaNchunks{chunkn}))
            noNaNchunks{chunkn+1} = noNaNchunks{chunkn}(...
              find(isnan(noNaNchunks{chunkn}),1,'first'):end);
            noNaNchunks{chunkn} = noNaNchunks{chunkn}(...
              1:find(isnan(noNaNchunks{chunkn}),1,'first')-1);
            chunkn = chunkn + 1;
          end
        end
        noNaNchunks = noNaNchunks(cellfun(@numel,noNaNchunks) > 2);
        acf = nan(t30 + 1, numel(noNaNchunks));
        bounds = nan(2, numel(noNaNchunks));
        for chunkn = 1:numel(noNaNchunks)
          nlags = min([t30, numel(noNaNchunks{chunkn})-1]);
          [acf(1:nlags+1,chunkn),~,bounds(:,chunkn)] = autocorr(...
              noNaNchunks{chunkn}, nlags);
        end
        acfstd = nanstd(acf,0,2);
        acf = nanmean(acf,2);
        boundstd = nanstd(bounds(1,:));
        bounds = nanmean(bounds(1,:));
        acax = subplot(numel(parameter)+2,3,param*3-1);
%         autocorr(paramchmat(:,param),t30); 
        errorbar(0:t30,acf,acfstd); hold all;
        errorbar([0 t30],[bounds bounds],[boundstd boundstd]);
        errorbar([0 t30],[-bounds -bounds],[boundstd boundstd]);
        axis tight
        acax.FontSize = 8;
        acax.XTick = linspace(0,t30,7);
        acax.XTickLabel = 0:5:30;
        if param == numel(parameter)
          acax.XLabel.String = 'Lag (s)';
        end
        acax.YLabel.String = 'AutoCorr';
        if param == 1
          acax.Title.String = strrep(figfiles(numel(exptMeta.figurefolder)+1:end-7),...
            '_',' ');
        end
      end
      spcax = subplot(numel(parameter)+2,3,numel(parameter)*3+1:(numel(parameter)+2)*3);  % figure;
      imagesc(freq.time,1:numel(freq.freq),10.*log10(squeeze(freq.powspctrm(ch,:,:))),clim);
      colormap(jet); colorbar; axis xy;
      spcax.FontSize = 10;
      spcax.YTick = rfi;
      spcax.YTickLabel = rf;  % converts to char, so this can't be used in place of rf
      spcax.YLabel.String = 'Frequency (Hz)';
      if stat.time(end) < 70  % assume this is a tone-triggered trial
        spcax.XLabel.String = 'Time from Tone Onset (s)';
        spcax.XTick = -60:15:60;
      end
      [~,fndx] = ismember(squeeze(stat.rFreq(1,ch,~tnan)),freq.freq);
      hold all; plot(stat.time(~tnan),wndx(fndx,:),'w')
      print(strrep(figfiles, 'chname', stat.label{ch}),'-dpng')
    end
  end
  
  %% save tfRidge file
  
  disp(['saving stat to ' outputfile(numel(exptMeta.datafolder):end)])
  save(outputfile,'stat');
  
  %% clear freq to conserve memory
  
  clearvars freq stat
  
  %% update estimate of time to complete analysis
  
  comptime(fn) = toc(starttime);
  esttime = nanmean([esttime; comptime(fn)]); % average into estimate of how long each file should take
end

%% clean-up

output = comptime;
close(wb);

end
