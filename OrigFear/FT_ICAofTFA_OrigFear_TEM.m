%% perform ICA decomposition of origfear spectrograms & assess results
% won't work directly on merged freq files because it automatically
% combines rpt-chan-freq, isolating only time, instead of making it
% chan-freq vs. rpt-time

CleanSlate

info_origfear                   % load all background info (filenames, etc)

%% which spectrograms to analyze

spectype  = 'wav';  % wavelet TFA
normtype  = 'z_dB'; % z-score based on mu & sigma of dB-transformed baseline spectrograms
redo      = false;  % to ignore pre-existing outputs and overwrite them

errlog = cell(size(ratT,1),1);  % preallocate cells to hold MEs

%% full loop

for r = find(~ratT.excluded)'
  %% define input & output filenames
  
  ratfolder = [datafolder num2str(ratT.ratnums(r)) filesep];
  inputfile = [ratfolder normtype 'Norm_' spectype 'TFA_' ...
    num2str(ratT.ratnums(r)) 'merged.mat'];
  outputfile = [ratfolder 'ICAof' normtype 'Norm_' spectype 'TFA' ...
    num2str(ratT.ratnums(r)) '.mat'];
  
  %% check if they exist
  
  if ~exist(outputfile,'file') && ~exist(inputfile,'file')
    errlog{r}.warning = ['Skipping rat #' num2str(ratT.ratnums(r)) ...
      ' because neither the inputfile nor the outputfile were found.'];
    warning(errlog{r}.warning)
    continue % to next rat
  end
  
  %% calculate or load ICA components
  
  if exist(outputfile,'file') && ~redo
    disp(['loading ICA components from file: ' outputfile])
    load(outputfile)
  else
    %% perform ica decomposition
    
    cfg = [];
    
    cfg.inputfile   = inputfile;
    cfg.outputfile  = outputfile;
    
    [comp] = ft_componentanalysis(cfg);
  end
  
  if all(any(isnan([comp.trial{:}])))
    errlog{r}.error = 'Something went wrong in ft_componentanalysis.';
    error(errlog{r}.error)
  end
  
  %% visualize results (spatial topography)
  
  fig1 = figure(1); clf;
  cfg = [];
  cfg.component = 1:numel(comp.label);
  cfg.layout    = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  cfg.colormap  = jet;
  cfg.zlim      = 'maxabs';
  cfg.comment   = 'no';
  
  ft_topoplotIC(cfg, comp)
  fig1.WindowStyle = 'docked';
  print(fig1,[figurefolder 'topoplotIC_' normtype 'Norm_' spectype 'TFA' ...
    num2str(ratT.ratnums(r))],'-dpng','-r300')
  
  %% visualize results (over time)
  
  cfg = [];
  cfg.viewmode    = 'component';
%   cfg.continuous  = 'yes';      % instead of commented cell above
%   cfg.blocksize   = 180;
  cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  cfg.colormap    = jet;        % ignored?
  cfg.zlim        = 'maxabs';   % ignored?
  
  %   cfg.event = struct('type',repmat('ToneOnset',47,1), ...
  %     'sample',unique(comp.sampleinfo(:,1)-round(comp.time{:}(1)*comp.fsample)), ...
  %     'value',unique(comp.trialinfo,'rows','stable'), ...
  %     'duration',ones(47,1), 'offset',zeros(47,1));
  %   This doesn't work, but someday I should go back and restore the events
  %   after merging files.
  
  cfg = ft_databrowser(cfg, comp);
end
