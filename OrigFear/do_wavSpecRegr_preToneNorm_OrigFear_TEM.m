function [output] = do_wavSpecRegr_preToneNorm_OrigFear_TEM( r )
% DO_WAVSPECREGR_PRETONENORM_ORIGFEAR_TEM tests regrT btwn spectrograms & trl#,
% one block of 7-15 trials at a time, normalizing raw data (no artifact
% rejection) by the pre-tone period
%
%   This function is for within-subject visualization & stats using
%   ft_statfun_indepsamplesregrT on the output of wavelet (power spectrograms).
%
%   Depends on global variables defined in info_origfear.
%
% written 7/27/17 by Teresa E. Madsen, Ph.D.

%% check for input

if nargin < 1
  info_origfear;  % metadata may not have been already loaded by master script
  r = 1;          % poster rat
end

%% declare global variables

global datafolder configfolder figurefolder ratT fsample stimuli phases  ...
  intervals blkedges blknames mainblocks blksof7names expected

%% create waitbar

wb = waitbar(0, ['Preparing to run wavSpecRegr preToneNorm on rat #' ...
  num2str(ratT.ratnums(r))]);

%% prepare_neighbours determines what sensors may form clusters

neighbfile = [configfolder 'neighb_' ratT.MWA{r} '_' ratT.side{r} '.mat'];

if existfile_TEM(neighbfile)
  load(neighbfile)
else
  cfg = [];
  cfg.method        = 'distance';
  cfg.neighbourdist = 0.5;  % mm
  cfg.elecfile      = [configfolder 'elec_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  neighbours        = ft_prepare_neighbours(cfg);
  save(neighbfile,'neighbours')
end
if any(cellfun(@numel,{neighbours(:).neighblabel}) < 3) || ...
    any(cellfun(@numel,{neighbours(:).neighblabel}) > 5)
  warning('unexpected # of neighbors')
  cfg = [];
  cfg.neighbours = neighbours;
  cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  ft_neighbourplot(cfg)
  keyboard
end

%% loop through main blocks, loading data

normfreq  = cell(6,10);   % 8-10 tones per block:  begin & end of each recording
comptime  = nan(size(normfreq,1),3);  % 3 steps:  prep data, run stats, plot results
esttime   = [342, 550, 16.3];     % each step's estimated comptime (s) per block
% esttime = nanmean(comptime,1);  % average of past calculations

for b = 1:numel(blksof7names)
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/sum(esttime.*size(normfreq,1)), wb, ...
    ['Normalizing ' blksof7names{b} ' rawWavSpecs before regrT analysis']);
  
  p = ceil(b/2);  % convert block to phase
  
  if rem(b,2) == 1  % odd #s
    tones = 1:expected(p)-7;            % 1st 8-10 of each recording
  else              % even #s
    tones = expected(p)-6:expected(p);  % last 7 of each recording
  end
  
  for tr = 1:numel(tones)   % trial # within block
    %% define trial inputfile
    
    tn = tones(tr);   % trial # within phase
    
    inputfile2 = [datafolder 'wavelet_raw' filesep num2str(ratT.ratnums(r)) ...
      filesep stimuli{1} num2str(tn) 'of' phases{p} '_raw_' ...
      num2str(ratT.ratnums(r)) 'waveletTFApreToneNorm_dB.mat'];
    
%     %% load pre-tone normalized inputfile, if it exists
%     
%     if existfile_TEM(inputfile2)
%       freq = rmvlargefields_TEM(inputfile2);
%     else
      %% load fourier inputfile & transform it
      
      inputfile1 = strrep(inputfile2,'preToneNorm_dB','fourier');
      
      if ~existfile_TEM(inputfile1)
        warning(['Skipping tone ' num2str(tn) ' of ' phases{p} ...
          ' because inputfile (' inputfile1(numel(datafolder):end) ...
          ') was not found.'])
        continue  % to next tone
      end
      
      freq = rmvlargefields_TEM(inputfile1);
      wavlen = freq.cfg.wavlen;
      
      %% convert to powspctrm
      
      freq = ft_checkdata(freq,'cmbrepresentation','sparsewithpow');
      
      assert(isequal(freq.dimord,'chan_freq_time'))
      if any(isnan(freq.powspctrm(:))) && ~(b == 2 && r == 1 && tr == 2)
        warning('NaNs in converted powspctrm')
        figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
        keyboard  % pause to verify
      end
      
    %% replace acq powspctrm time/freq bins including shock artifact w/ NaNs
    
    if b == 2
      for f = 1:numel(freq.freq)
        badt = freq.time > 29.5 - wavlen(f)/2 & ...
          freq.time < 30 + wavlen(f)/2;   % so no time bins will include shock time
        freq.powspctrm(:,f,badt) = NaN;
      end
    end
    % figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
    
    %% smooth & downsample time axis, dB normalize by median of pre-tone period
    
    twin = 10;
    tshift = 1;
    normfreq{b,tr} = freq;
    normfreq{b,tr}.time = -60:tshift:60;   % 121 10s windows shifting by 1s
    normfreq{b,tr}.powspctrm = nan(size(freq.powspctrm,1), ...
      numel(normfreq{b,tr}.freq), numel(normfreq{b,tr}.time));
    
    for t = 1:numel(normfreq{b,tr}.time)
      tndx = abs(freq.time-normfreq{b,tr}.time(t)) < twin/2;   % +/-5s
      if ~any(tndx)
        warning('no times selected')
        keyboard
      end
      for f = 1:numel(normfreq{b,tr}.freq)
        %         fndx = abs(freq.freq-normfreq{b,tr}.freq(f)) < ...
        %           normfreq{b,tr}.freq(f)/9;  % +/-w (from 0.09 to 29.4 Hz)
        % %           mtmc.tapsmofrq(f);  % +/-w (from 0.78 to 28.5 Hz)
        %         if ~any(fndx)
        %           warning('no frequencies selected')
        %           keyboard
        %         end
        baselineT = freq.time < -wavlen(f);   % only using data before t=0
        normfreq{b,tr}.powspctrm(:,f,t) = 10*log10(...  % convert power ratio to dB
          median(freq.powspctrm(:,f,tndx),3,'includenan') ./ ...  % to preserve sharp changes & avoid biasing by skew
          median(freq.powspctrm(:,f,baselineT),3,'includenan'));  % include nan so no time periods overlap shock
        %       reshape(freq.powspctrm(:,fndx,tndx), ...
        %           size(freq.powspctrm,1),[]), 2,'omitnan');
      end
    end
    if sum(isnan(normfreq{b,tr}.powspctrm(:)))/numel(normfreq{b,tr}.powspctrm) ...
        >= 0.2  % 24s - could be most of a before, during, or after tone period
      warning(['trial #' num2str(tn) ' of ' phases{p} ...
        ' skipped because more than 20% of the spectrogram is NaNs'])
      if b == 2 && r == 1 && tr == 2
        normfreq{b,tr} = [];
      else
        figure; plotFTmatrix_TEM(struct('zlim','max-max'),normfreq{b,tr})
        keyboard  % verify first
      end
    end
    %% clear freq to conserve memory
    
    clearvars freq
    
  end
  
  %% update estimate of time to complete analysis
  
  comptime(b,1) = toc(starttime);
  esttime(1) = nanmean([esttime(1); comptime(b,1)]); % average into estimate of how long each block should take
end

%% run within-subject stats on power spectrograms

stat = cell(size(normfreq,1),1);

for b = 1:size(normfreq,1)
  %% make sure all trials were done
  
  omit = cellfun(@isempty,normfreq(b,:));
  if sum(~omit) < 6
    warning([blksof7names{b} ' only has ' num2str(sum(~omit)) ' trials'])
    if sum(~omit) == 0
      warning(['skipping ' blksof7names{b} ' for rat # ' num2str(ratT.ratnums(r))])
      continue  % to next block
    end
    keyboard
  end
  
  %% define & check for outputfile
  
  cfg = [];
  cfg.outputfile  = [datafolder 'wavSpecRegr_preToneNorm' filesep num2str(ratT.ratnums(r)) ...
    filesep blksof7names{b} '_preToneNorm_raw_' num2str(ratT.ratnums(r)) ...
    'stat_indepsamplesregrT_NPindiv_ordered.mat'];
  
%   if existfile_TEM(cfg.outputfile)
%     stat{b} = rmvlargefields_TEM(cfg.outputfile);
%   else
    starttime = tic;  % only time computation, not just loading files
    
    cfg.statistic         = 'ft_statfun_indepsamplesregrT';
    
    waitbar(sum(comptime(:),'omitnan')/sum(esttime.*size(normfreq,1)), wb, ...
      ['Running ' cfg.statistic(12:end) ' on ' blksof7names{b} ...
      ' preToneNormWavSpecs for rat #' num2str(ratT.ratnums(r))]);
    
    cfg.method            = 'montecarlo';
    cfg.correctm          = 'cluster';
    cfg.clusteralpha      = 0.025;
    cfg.clusterstatistic  = 'maxsum';
    cfg.clusterthreshold  = 'nonparametric_individual';   % essential for reasonable thresholds!
    cfg.minnbchan         = 2;
    cfg.tail              = 0;
    cfg.clustertail       = 0;
    cfg.alpha             = 0.025;
    cfg.numrandomization  = 500;
    cfg.orderedstats      = 'yes';  % essential for reasonable thresholds!
    cfg.neighbours        = neighbours;
    cfg.parameter         = 'powspctrm';
    
    cfg.design  = find(~omit);  % skips missing trial #s as appropriate
    
    cfg.ivar    = 1;  % uvar = ivar (between-UO)
    
    stat{b} = ft_freqstatistics(cfg, normfreq{b,~omit});
    comptime(b,2) = toc(starttime);
    esttime(2) = nanmean([esttime(2); comptime(b,2)]); % average into estimate of how long each block should take
%   end
end

%% print & plot (top 10/nearly) significant correlations of spectrograms & trl#s

for b = find(~cellfun(@isempty,stat))'
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/sum(esttime.*size(normfreq,1)), wb, ...
    ['Plotting results of ' stat{b}.cfg.statistic ' on ' blksof7names{b} ...
    ' preToneNormWavSpecs for rat #' num2str(ratT.ratnums(r))]);
  
  pos = [];
  if isempty(stat{b}.posclusters)
    pos.n = 0;
    pos.I = [];  % no index because there are no clusters
  else
    [pos.p,pos.I] = sort([stat{b}.posclusters.prob]);
    pos.n = find(pos.p > 0.025,1,'first') - 1;
  end
  neg = [];
  if isempty(stat{b}.negclusters)
    neg.n = 0;
    neg.I = [];  % no index because there are no clusters
  else
    [neg.p,neg.I] = sort([stat{b}.negclusters.prob]);
    neg.n = find(neg.p > 0.025,1,'first') - 1;
  end
  bestch = zeros(pos.n+neg.n+2,1);
  disp([newline 'Found clusters with significant positive (' ...
    num2str(pos.n) ') and negative (' num2str(neg.n) ')' newline ...
    'correlations with trial #s within ' blksof7names{b} newline])
  
  cfg =[];
  cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  cfg.parameter = 'stat';
  cfg.feedback  = 'no';
  cfg.gridscale = 2*numel(stat{b}.label) + 1;
  %   ft_clusterplot(cfg,stat{b});  % doesn't work unless freq or time is singleton
  
  %% visualize positive correlation clusters
  
  for n = 1:(pos.n + 1)  % may plot one that's not considered significant
    if n > 10 || numel(pos.I) < n
      break  % out of for loop
    end
    cI = pos.I(n);  % cluster index
    I = find(stat{b}.posclusterslabelmat == cI);
    [ch,f,t] = ind2sub(size(stat{b}.posclusterslabelmat),I);
    bestch(n) = mode(ch);
    ch = unique(ch); f = unique(f); t = unique(t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat{b}.freq(min(f)) stat{b}.freq(max(f))];
    trange = [stat{b}.time(min(t)) stat{b}.time(max(t))];
    trlns = nan(1,size(normfreq,2));
    clustpowpertrl = nan(numel(I),size(normfreq,2));   % aggregate cluster power over trials
    for tr = find(~cellfun(@isempty,normfreq(b,:)))
      trlns(tr) = normfreq{b,tr}.trialinfo(1,1);
      clustpowpertrl(:,tr) = normfreq{b,tr}.powspctrm(I);
    end
    clustpowpertrl(:,isnan(trlns)) = [];
    trlns(isnan(trlns)) = [];
    
    fig = figure(b*100+n); clf; fig.WindowStyle = 'docked';
    fig.PaperPositionMode = 'auto';
    % plot mean +/- std across trials for each chan/freq/timebin w/in cluster
    subplot(1,2,1); hold all
    X = repmat(trlns,size(clustpowpertrl,1),1);
    CC = corrcoef(X(:), clustpowpertrl(:));
    [pc,ErrorEst] = polyfit(X(:), clustpowpertrl(:), 3);   % fit cubic model
    pop_fit = polyval(pc,trlns,ErrorEst);
    plot(X(:),clustpowpertrl(:),'+', trlns,mean(clustpowpertrl),'-', ...
      trlns,pop_fit,'-');
    axis tight
    legend({'chan/time/freq bins','mean per trial','cubic fit'}, ...
      'Location','Best')
    
    title([strjoin(stat{b}.label(ch)) newline ...
      'have significant (p = ' num2str(stat{b}.posclusters(cI).prob) ...
      ') positive correlations (overall coeff = ' num2str(CC(2)) ')' newline ...
      'at ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz, from ' ...
      num2str(trange(1)) ' - ' num2str(trange(2)) ' s' newline ...
      'with trial #s within ' blksof7names{b} ':'])
    
    % plot topographic extent of change in this time-freq cluster
    cfg.ylim = frange; cfg.xlim = trange;
    subplot(1,2,2);
    ft_topoplotER(cfg,stat{b});
    
    print([figurefolder 'PosWavPowerCorr' num2str(n) blksof7names{b} '_preToneNorm_raw_' ...
      num2str(ratT.ratnums(r)) 'stat_indepsamplesregrT_NPindiv_ordered'], ...
      '-dpng','-r0')
    %     close(fig)
  end
  
  %% visualize negative correlation clusters
  
  for n = 1:(neg.n + 1) % may plot one that's not considered significant
    if n > 10 || numel(neg.I) < n
      break  % out of for loop
    end
    cI = neg.I(n);  % cluster index
    I = find(stat{b}.negclusterslabelmat == cI);
    [ch,f,t] = ind2sub(size(stat{b}.negclusterslabelmat),I);
    bestch(n) = mode(ch);
    ch = unique(ch); f = unique(f); t = unique(t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat{b}.freq(min(f)) stat{b}.freq(max(f))];
    trange = [stat{b}.time(min(t)) stat{b}.time(max(t))];
    trlns = nan(1,size(normfreq,2));
    clustpowpertrl = nan(numel(I),size(normfreq,2));   % aggregate cluster power over trials
    for tr = find(~cellfun(@isempty,normfreq(b,:)))
      trlns(tr) = normfreq{b,tr}.trialinfo(1,1);
      clustpowpertrl(:,tr) = normfreq{b,tr}.powspctrm(I);
    end
    clustpowpertrl(:,isnan(trlns)) = [];
    trlns(isnan(trlns)) = [];
    
    fig = figure(b*100+n); clf; fig.WindowStyle = 'docked';
    fig.PaperPositionMode = 'auto';
    % plot mean +/- std across trials for each chan/freq/timebin w/in cluster
    subplot(1,2,1); hold all
    X = repmat(trlns,size(clustpowpertrl,1),1);
    CC = corrcoef(X(:), clustpowpertrl(:));
    [pc,ErrorEst] = polyfit(X(:), clustpowpertrl(:), 3);   % fit cubic model
    pop_fit = polyval(pc,trlns,ErrorEst);
    plot(X(:),clustpowpertrl(:),'+', trlns,mean(clustpowpertrl),'-', ...
      trlns,pop_fit,'-');
    axis tight
    legend({'chan/time/freq bins','mean per trial','cubic fit'}, ...
      'Location','Best')
    
    title([strjoin(stat{b}.label(ch)) newline ...
      'have significant (p = ' num2str(stat{b}.negclusters(cI).prob) ...
      ') negative correlations (overall coeff = ' num2str(CC(2)) ')' newline ...
      'at ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz, from ' ...
      num2str(trange(1)) ' - ' num2str(trange(2)) ' s' newline ...
      'with trial #s within ' blksof7names{b} ':'])
    
    % plot topographic extent of change in this time-freq cluster
    cfg.ylim = frange; cfg.xlim = trange;
    subplot(1,2,2);
    ft_topoplotER(cfg,stat{b});
    
    print([figurefolder 'NegWavPowerCorr' num2str(n) blksof7names{b} '_preToneNorm_raw_' ...
      num2str(ratT.ratnums(r)) 'stat_indepsamplesregrT_NPindiv_ordered'], ...
      '-dpng','-r0')
    %     close(fig)
  end
  
  %% plot stat
  
  fig = figure(b*100); clf; fig.WindowStyle = 'docked';
  cfg=[];
  cfg.channel         = mode(bestch(bestch ~= 0));
  if isempty(cfg.channel);    cfg.channel = ratT.mPFC{r};     end
  cfg.zlim            = quantile(stat{b}.stat(:),[0.025 0.975]);
  cfg.parameter       = 'stat';
  cfg.maskparameter   = 'mask';
  cfg.maskstyle       = 'outline';
  cfg.feedback        = 'no';
  plotFTmatrix_TEM(cfg,stat{b});
  title(['Correlations with ' blksof7names{b} ' Trial #s'])
  
  fig.PaperPositionMode = 'auto';
  print([figurefolder 'WavPowerCorrSpectrogram_' blksof7names{b} '_preToneNorm_raw_' ...
    num2str(ratT.ratnums(r)) 'stat_indepsamplesregrT_NPindiv_ordered'], ...
    '-dpng','-r0')
  %   close(fig)
  
  comptime(b,3) = toc(starttime);
  esttime(3) = nanmean([esttime(3); comptime(b,3)]); % average into estimate of how long each block should take
end

output = comptime;
close(wb);
end
