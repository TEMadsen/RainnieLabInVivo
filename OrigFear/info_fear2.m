%% info needed for analyzing 2nd batch of old fear rats' data
%#ok<*SNASGU> just provides background info for other analysis protocols

datafolder    = 'S:\Teresa\Analyses\TempData\';
configfolder  = 'S:\Teresa\Analyses\MWAconfigs\';

% put new figures in a folder named by date, in the format 'yyyy-mm-dd' 
figurefolder  = ['S:\Teresa\Figures\TempFigures\' datestr(now,29) '\']; 
if ~exist(figurefolder,'dir')
  mkdir(figurefolder)
end

%% define MWA configuration(s)
% comment out all but the next line after the elec & layout files are saved

MWA = 'NewBatchOldFear';  % append suffix _Left or _Right for filenames

% cfg = [];
% 
% % array & target info
% cfg.atlascoord = [2.7, 0.50, -6.00; ... 	% AP/ML/DV relative to bregma
%                  -3.0, 4.80, -8.60];      % dim 1/2/3 for below
% cfg.angle     = [-6; 6];  % + angle means approach from medial, in degrees
% cfg.angledim  = [2; 2];   % both probes angled along ML dim
% cfg.elecspac  = [0.25, 0.25; ...  % in AP dim, then ML dim, for each probe
%                 0.25, 0.25];      % in mm
% 
% lftchs = {'AD01','AD05'; ...  % farthest left array configuration, where
%           'AD02','AD06'; ...  % increasing rows indicate electrodes 
%           'AD03','AD07'; ...  % ordered A -> P, and increasing columns 
%           'AD04','AD08'};     % indicate those ordered L -> R
% 
% rtchs = {'AD09','AD13'; ...   % farthest right array configuration, where
%          'AD10','AD14'; ...   % increasing rows indicate electrodes 
%          'AD11','AD15'; ...   % ordered A -> P, and increasing columns 
%          'AD12','AD16'};      % indicate those ordered L -> R
% 
% %% for rats implanted on right side of brain
% 
% cfg.label = {lftchs; rtchs}; 	% 2 cell arrays nested in another cell array
% cfg.side = {'Right';'Right'}; % which side of the brain each probe was on
%        
% % calculate sensor positions & define layout
% MWAlayout(cfg,[MWA '_Right'],configfolder);
% 
% %% for rats implanted on left side of brain
% 
% cfg.label = {rtchs; lftchs}; 	% 2 cell arrays nested in another cell array
% cfg.side = {'Left';'Left'};   % which side of the brain each probe was on
%        
% % calculate sensor positions & define layout
% MWAlayout(cfg,[MWA '_Left'],configfolder);

%% experiment metadata

% each row # here corresponds to cell row (1st dim) in other variables
ratnums = [338; 339; 341; 342; 343];
excluded = logical([0; 0; 0; 0; 0]);   % see notes for why
side = {'Left';'Right';'Right';'Left';'Right'};
chmPFC = cell(numel(ratnums),1);    % best channel to use for CFC
chBLA = cell(numel(ratnums),1);     % best channel to use for CFC
finalch = false([numel(ratnums),1]);
ratT = table(ratnums, excluded, side, chmPFC, chBLA, finalch);
clearvars ratnums excluded side chmPFC chBLA finalch

% each column # here corresponds to cell column (2nd dim) in other variables
phases = {'Conditioning', 'Recall', 'Extinction'};  % add 'Baseline',

abbr = {'FC', 'Ext1', 'Ext2'};  % add 'BL', % for behavioral data

blocks = {'Habituation', 'Fear Recall', 'Extinction Recall'; ...
    'Acquisition', 'Early Extinction', 'Late Extinction'}; 

% each row # here corresponds to the numerical code that will be saved in
% trl and data.trialinfo to identify which subtrials correspond to which
% interval types
intervals = {'Before'; 'During'; 'After'};

% each row # here corresponds to the numerical code that will represent
% which region's data is being used for phase or amplitude source data
regions = {'mPFC'; 'BLA'};

% initialize variables
goodchs = cell(size(ratT,1),1);  % cell array of strings can't be contained within one cell of a table
notes = cell(size(ratT,1),numel(phases));
toneTS = cell(size(ratT,1),numel(phases));
nexfile = cell(size(ratT,1),numel(phases));

% any extra comments about each rat/session
notes{1,1} = 'only has array in the BLA';
notes{3,3} = 'video recording of ext2 is lost';
notes{4,3} = 'lost headcap before extinction';

%% channels to use for each rat, defaults to 'AD*' if empty
goodchs{1} = {'AD*','-AD01','-AD02','-AD03','-AD04','-AD05','-AD08',...  % all AD channels except...
  '-AD09','-AD10','-AD11','-AD12','-AD13','-AD14','-AD15','-AD16'};   % including these so test of correct # of channels will work
ratT.chmPFC(1) = {'none'};
ratT.chBLA(1) = {'AD07'};
ratT.finalch(1) = true;
goodchs{2} = {'AD*','-AD01','-AD02','-AD03',...  % all AD channels except...
  '-AD09','-AD10','-AD11','-AD13','-AD14','-AD15'};
ratT.chmPFC(2) = {'AD07'};
ratT.chBLA(2) = {'AD12'};
ratT.finalch(2) = true;
goodchs{3} = {'AD*','-AD07','-AD08',...  % all AD channels except...
  '-AD11','-AD13','-AD14','-AD16'};  
ratT.chmPFC(3) = {'AD06'};
ratT.chBLA(3) = {'AD10'};
ratT.finalch(3) = true;
goodchs{4} = {'AD*','-AD01','-AD02','-AD04','-AD06','-AD07',...  % all AD channels except...
  '-AD09','-AD10','-AD11'};
ratT.chmPFC(4) = {'AD05'};
ratT.chBLA(4) = {'AD12'};
ratT.finalch(4) = true;
goodchs{5} = {'AD*','-AD01','-AD08',...  % all AD channels except...
  '-AD14','-AD15','-AD16'};
ratT.chmPFC(5) = {'AD04'};
ratT.chBLA(5) = {'AD11'};
ratT.finalch(5) = true;

%% filenames and invalidated trials
% Habituation & Acquisition (fear conditioning)
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\338\338_150513_0000-1mrg-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\339\339_150513_0002-3mrg-aligned.nex';     
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\341\341_150624_0001-2mrg-aligned.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\342\342_150624_0003-4mrg-aligned.nex';
nexfile{5,1} = 'S:\Teresa\PlexonData Backup\343\343_150624_0005-6mrg-aligned.nex';

% Recall (extinction 1)
nexfile{1,2} = 'S:\Teresa\PlexonData Backup\338\338_150516_0001-aligned.nex';
nexfile{2,2} = 'S:\Teresa\PlexonData Backup\339\339_150516_0000-aligned.nex';
nexfile{3,2} = 'S:\Teresa\PlexonData Backup\341\341_150627_0000-aligned.nex';
nexfile{4,2} = 'S:\Teresa\PlexonData Backup\342\342_150627_0001-2mrg-aligned.nex';
nexfile{5,2} = 'S:\Teresa\PlexonData Backup\343\343_150627_0003-aligned.nex';

% Extinction (extinction 2)
nexfile{1,3} = 'S:\Teresa\PlexonData Backup\338\338_150517_0000-aligned.nex';
nexfile{2,3} = 'S:\Teresa\PlexonData Backup\339\339_150517_0002-aligned.nex';     
nexfile{3,3} = 'S:\Teresa\PlexonData Backup\341\341_150628_0000_VideoLost-aligned.nex';
nexfile{5,3} = 'S:\Teresa\PlexonData Backup\343\343_150628_0000-aligned.nex';
