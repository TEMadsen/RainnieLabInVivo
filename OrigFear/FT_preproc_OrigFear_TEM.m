%% Load all data from -aligned.nex files into FieldTrip format & merge
% into 1 dataset per rat, using new trialfun_TEM.m and supporting a cell
% array of strings for multiple filenames within one phase (to avoid merged
% NEX files, which are hard to read correctly).
%%% CURRENT AS OF 2/28/17 %%%

info_origfear  % load all metadata for both batches of original fear rats
%#ok<*SAGROW> many variables seem to grow when they're preallocated here
redo = false;    % set true to overwrite any old versions of outputfiles

errcnt = false(size(ratT,1),numel(phases),2);
errlog = cell(size(ratT,1),numel(phases),2);

%% find phase # & tone # in columns of data.trialinfo

pndx = find(contains(infotype,'phase', 'IgnoreCase',true));
tndx = find(contains(infotype,'trial', 'IgnoreCase',true));

%% full loop

for r = find(~ratT.excluded)'
  % Raw LFP data divided into long, non-overlapping tone-triggered
  % trials, merged across all recordings for each rat
  outputfile{2} = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'RawLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  if exist(outputfile{2},'file') && ~redo
    disp([outputfile{2} ' already exists.  Skipping rat.'])
    continue % skip to next rat
  end
  
  %% to skip continue, start here
  
  data2merge = cell(1);
  next = 1;   % put 1st recording's data in 1st cell
  for p = 1:numel(phases)   % includes baseline
    try
      if isempty(nexfile{r,p})   % in case of missing files
        continue  % skip to next phase
      end
      if ~iscell(nexfile{r,p})  % in case of multiple files per phase
        nexfile{r,p} = nexfile(r,p);  % convert to cellstr
      end
      
      %% for each file...
      
      for fn = 1:numel(nexfile{r,p})  % file number w/in a phase
        try
          %% define output filenames, etc.
          
          % Raw LFP data divided into long, non-overlapping tone-triggered
          % trials
          outputfile{1} = [datafolder num2str(ratT.ratnums(r)) filesep ...
            num2str(ratT.ratnums(r)) abbr{p} num2str(fn) 'RawLFPTrlData.mat'];
          
          %% load file if it exists
          
          if ~exist(outputfile{1},'file') || redo
            disp('starting from scratch');
            bestfile = 0;
            clearvars data
          else
            disp(['loading file ' outputfile{1}]);
            load(outputfile{1});
            bestfile = 1;
          end
          
          %% no processing done yet
          if bestfile == 0
            if ~exist(nexfile{r,p}{fn},'file')
              error(['Input file ' nexfile{r,p}{fn} ' does not exist.']);
            end
            %% define trials that cover whole recording
            
            cfg                         = [];
            cfg.dataset                 = nexfile{r,p}{fn};
            cfg.trialfun                = 'trialfun_TEM';
            
            if p == 4 % baseline has no tones
              %% import all data as 1 continuous trial
              cfg.continuous              = 'yes';
            else
              %% define tone-triggers
              
              if ~isempty(toneTS{r,p})
                cfg.trialdef.eventtype    = 'manual';
                cfg.trialdef.eventTS      = toneTS{r,p};
              else
                cfg.trialdef.eventtype    = ratT.CSplus{r};
              end
              
              %% import data as long tone-triggered trials
              
              cfg.trialdef.minsep         = 90;   % all in seconds
              cfg.trialdef.prestim        = 75;   % leave an extra 15s for padding/smoothing
              cfg.trialdef.poststim       = 75;   % half longest wavelet is enough
              cfg.trialdef.expandtrials   = true;
              
              cfg = ft_definetrial(cfg);
              cfg.trl = [cfg.trl ones(size(cfg.trl,1),1) ...  % add stimulus
                p*ones(size(cfg.trl,1),1)];   % type and phase # to trialinfo
              
            end
            
            %% load continuous LFP data, all channels
            
            cfg.channel     = 'AD*';
            
            data = ft_preprocessing(cfg);
            
            %% add elec positions & label trialinfo columns
            
            elecfile = [configfolder 'elec_' MWA '_' ratT.side{r} '.mat'];
            load(elecfile);
            data.elec = elec;
            
            data.infotype = infotype(1:3); % 'OrigTrialNum','StimulusType','PhaseNum'
            
            if ~isfield(data,'trialinfo') && numel(data.trial) == 1
              data.trialinfo = [1 0 p];   % place holders for non-existant trial # & stimulus type
            end
            
            %% remove NaNs from data & save 1st FT format data file
            
            cfg                         = [];
            cfg.artfctdef.nan.artifact  = artifact_nan(data);
            cfg.artfctdef.reject        = 'partial';
            cfg.outputfile              = outputfile{1};
            
            data = ft_rejectartifact(cfg,data);
            
            bestfile = 1;
          end
          
          %% tone-triggered trials saved
          if bestfile == 1
            if fn > 1   % if this isn't the first file in this phase
              if p == data2merge{end}.trialinfo(end,pndx) % and previous trial belongs to same phase
                data.trialinfo(:,tndx) = data.trialinfo(:,tndx) + ...
                  max(data2merge{end}.trialinfo(:,tndx)); % adjust trial #s
              end
            end
            data2merge{next} = data;
            next = next + 1;  % next recording will be saved in next cell
          end
        catch ME
          errcnt(r,p,fn) = true;
          errlog{r,p,fn} = ME;
          warning(['Error for rat #' num2str(ratT.ratnums(r)) ...
            ', ' phases{p} ' ' num2str(fn) ':'])
          getReport(ME)
          continue  % skips to next file
        end
      end
      
      %% check for correct # of trials per phase
      
      numtrls = 0;   % to start
      
      for dn = 1:numel(data2merge) % count trials in this phase across all files
        numtrls = numtrls + sum(data2merge{dn}.trialinfo(:,pndx) == p);
      end
      
      if numtrls ~= expected(p) % &&  ~isempty(data2merge{end}) % why would the last cell ever be empty?
        error('Incorrect # of trials for this phase')
      end
      
    catch ME
      errcnt(r,p,fn+1) = true;
      errlog{r,p,fn+1} = ME;
      warning(['Error for rat #' num2str(ratT.ratnums(r)) ...
        ', ' phases{p} ':'])
      getReport(ME)
      continue  % skips to next phase
    end
  end % phase
  
  %% check for errors
  
  if any(any(errcnt(r,:,:)))
    warning(['errors were found with rat #' ...
      num2str(ratT.ratnums(r)) ', data not merged'])
    continue  % skip to next rat
  end
  
  %% merge the data from all recordings
  
  cfg             = [];
  cfg.outputfile  = outputfile{2};
  
  if isempty(data2merge{1})
    warning(['No data present for rat #' num2str(ratT.ratnums(r)) ...
      '.  No merge performed.']);
  elseif numel(data2merge) == 1
    warning(['Only 1 phase of data present for rat #' ...
      num2str(ratT.ratnums(r)) '.  No merge performed.']);
  else
    merged = ft_appenddata(cfg, data2merge{:});
  end
  
  clearvars data data2merge merged
end

%% display errors again

if sum(errcnt(:)) == 0
  disp('No errors found!')
else
  disp('   *** Error Review ***   ')
  for r = 1:size(ratT,1)
    disp([num2str(sum(sum(errcnt(r,:,:)))) ' errors found for rat #' ...
      num2str(ratT.ratnums(r))])
    for p = 1:numel(phases)
      for fn = 1:size(errcnt,3)
        if errcnt(r,p,fn)
          warning(['Error for rat #' num2str(ratT.ratnums(r)) ', ' ...
            phases{p} ' ' num2str(fn) ':'])
          getReport(errlog{r,p,fn})
        end
      end
    end
  end
end