function output = do_preproc_OrigFear_TEM( r )
% DO_PREPROC_ORIGFEAR_TEM defines trials, loads data, and rejects artifacts
%
%   Currently skipping what was already done by FT_preproc_OrigFear_TEM and
%     using those outputfiles as input to this function, which will only perform
%     artifact rejection:  removing shock times (continuing as long as most
%     channels are still clipping on most trials) and then rejecting irregular
%     ICA components.
%
%   Output is a temporary variable to aggregate the maximum clipping times from
%     channels and shock trials across rats.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/7/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global exptMeta

%% Load inputfile

% Raw LFP data divided into long, non-overlapping tone-triggered trials, merged
% across all recordings for each rat
inputfile = [exptMeta.datafolder 'preproc' filesep ...
  num2str(exptMeta.ratT.ratnums(r)) filesep 'RawLFPTrialData_' ...
  num2str(exptMeta.ratT.ratnums(r)) 'Merged.mat'];

data = rmvlargefields_TEM(inputfile);   % strip excess historical metadata
data = ft_datatype_raw(data,'hassampleinfo','yes');

%% redefine trials to include at least 75s before & after each tone onset
% will allow equal data lengths for activation vs. baseline comparison

% establish current trial definition
if isfield(data.cfg,'trl')
  trl = data.cfg.trl;
else
  trl = [data.sampleinfo round(cellfun(@min,data.time)'*data.fsample) ...
    data.trialinfo];
end

cfg = [];
cfg.trl = trl;
for tr = find(cfg.trl(:,4) ~= 1)'  % skip 1st trial of any recording session
  if cfg.trl(tr,3) > -75*data.fsample
    adjust = cfg.trl(tr,3) + 75*data.fsample;   % # of samples to shift trials by
    cfg.trl(tr-1,2) = cfg.trl(tr-1,2) - adjust;
    cfg.trl(tr,[1 3]) = cfg.trl(tr,[1 3]) - adjust;
  end
end

if ~isequal(cfg.trl,trl)  % if different from current trial definition
  cfg.outputfile = inputfile;   % save back to original filename
  data = ft_redefinetrial(cfg,data);
end

%% Check whether rough artifact detection has been done

outputfile = [exptMeta.datafolder 'preproc' filesep ...
  num2str(exptMeta.ratT.ratnums(r)) filesep ...
  'AllClean_shockt_clip3_1500z_LFPTrialData_' ...
  num2str(exptMeta.ratT.ratnums(r)) '.mat'];

if existfile_TEM(outputfile)
  cleandata = rmvlargefields_TEM(outputfile);
else
  %% Define shock time artifacts
  
  shockt      = [29.25 31];   % 31 is perfect, leave it alone
  
  artifact = [];
  artifact.shock = NaN(7,2);
  
  for shk = 1:7
    assert(isequal(data.trialinfo(shk+10,:),[shk+10,1,1]),'mismatched trialinfo')
    samp = data.sampleinfo(shk+10,1):data.sampleinfo(shk+10,2);
    assert(isequal(size(samp),size(data.time{shk+10})),'samples don''t match timepoints')
    artifact.shock(shk,1) = samp(find(data.time{shk+10} < shockt(1),1,'last'));
    if any(data.time{shk+10} > shockt(2))
      artifact.shock(shk,2) = samp(find(data.time{shk+10} > shockt(2),1,'first'));
    else
      artifact.shock(shk,2) = samp(end);
    end
  end
  
  % eliminate shock trials that have already been cut off before the shock
  artifact.shock(artifact.shock(:,1) == artifact.shock(:,2),:) = [];
  
  %% detect clipping artifacts
  
  cfg = [];
  cfg.artfctdef.clip.channel          = 'all';
  cfg.artfctdef.clip.pretim           = 0.1;
  cfg.artfctdef.clip.psttim           = 0.1;
  cfg.artfctdef.clip.timethreshold    = 0.05;   % s
  cfg.artfctdef.clip.amplthreshold    = 3;      % uV of 2nd derivative
  
  cfg_preproc = [];    cfg_preproc.absdiff = 'yes';
  
  [~, artifact.clip] = ft_artifact_clip(cfg,ft_preprocessing(cfg_preproc,data));
  
  %% detect large artifacts (+/-1500 mV)
  
  databs = abs([data.trial{:}]);
  output.datavg = mean(databs,2); output.datstd = std(databs,1,2);
  clearvars databs;
  output.range = [min([data.trial{:}],[],2) max([data.trial{:}],[],2)];
  
  % convert 1500 mV to z for each channel, then use median as threshold
  output.zthresh = median((1500-output.datavg)./output.datstd);
  
  cfg.artfctdef.zvalue.cumulative = 'no'; % better at detecting artifacts with variable timing across channels, including shocks
  cfg.artfctdef.zvalue.channel        = 'all';
  cfg.artfctdef.zvalue.rectify        = 'yes';
  cfg.artfctdef.zvalue.cutoff         = output.zthresh;
  cfg.artfctdef.zvalue.trlpadding     = 0;
  cfg.artfctdef.zvalue.fltpadding     = 0;
  cfg.artfctdef.zvalue.artpadding     = 0.1;
  cfg.artfctdef.zvalue.interactive    = 'yes';
  
  [~, artifact.large] = ft_artifact_zvalue(cfg,data);
  
  %% preview & then remove shock time/large/clipping artifacts, & any NaNs 
  % (which break the ICA)
  
  cfg = [];
  cfg.artfctdef.shock.artifact  = artifact.shock;
  cfg.artfctdef.clip.artifact   = artifact.clip;
  cfg.artfctdef.large.artifact  = artifact.large;
  cfg.artfctdef.nan.artifact    = artifact_nan_TEM(data);
  
  cfg = ft_databrowser(cfg,data);  % preview if desired
  artifact = cfg.artfctdef;   % copy all, including any added visual artifacts
  
  cfg = [];
  cfg.artfctdef                 = artifact;
  cfg.artfctdef.reject          = 'partial';
  cfg.artfctdef.minaccepttim    = 4;
  cfg.outputfile                = outputfile;
  
  cleandata = ft_rejectartifact(cfg,data);
end

%% perform ICA on reduced-artifact data (or load if already done)

outputfile = [exptMeta.datafolder 'preproc' filesep ...
  num2str(exptMeta.ratT.ratnums(r)) filesep 'ICAofCleanLFPTrialData_' ...
  num2str(exptMeta.ratT.ratnums(r)) '.mat'];

if existfile_TEM(outputfile)
  ic_data = rmvlargefields_TEM(outputfile);
else
  cfg = [];
  cfg.outputfile = outputfile;
  
  ic_data = ft_componentanalysis(cfg,cleandata);
end

%% visualize results in the time domain

cfg = []; cfg.toilim = [-inf 0];
before_ic_data = ft_redefinetrial(cfg, ic_data);
before_ic_data.trialinfo = [before_ic_data.trialinfo ...
  ones(size(before_ic_data.trialinfo,1),1)];

cfg = []; cfg.toilim = [0 30];
during_ic_data = ft_redefinetrial(cfg, ic_data);
during_ic_data.trialinfo = [during_ic_data.trialinfo ...
  2*ones(size(during_ic_data.trialinfo,1),1)];

cfg = []; cfg.toilim = [30 inf];
after_ic_data = ft_redefinetrial(cfg, ic_data);
after_ic_data.trialinfo = [after_ic_data.trialinfo ...
  3*ones(size(after_ic_data.trialinfo,1),1)];

cfg = []; cfg.appenddim = 'rpt';
merged_ic_data = ft_appenddata(cfg,before_ic_data,during_ic_data,after_ic_data);
clearvars before_ic_data during_ic_data after_ic_data

[~,trlord] = sort(merged_ic_data.sampleinfo(:,1));
merged_ic_data.sampleinfo = merged_ic_data.sampleinfo(trlord,:);
merged_ic_data.trialinfo = merged_ic_data.trialinfo(trlord,:);
merged_ic_data.trial = merged_ic_data.trial(trlord);
merged_ic_data.time = merged_ic_data.time(trlord);

timax = cell(size(merged_ic_data.time));   % time axis
t0 = zeros(size(merged_ic_data.time));     % time zero per trial
cmeans = nan(numel(merged_ic_data.trial),numel(merged_ic_data.label));
cstds = nan(numel(merged_ic_data.trial),numel(merged_ic_data.label));
cskews = nan(numel(merged_ic_data.trial),numel(merged_ic_data.label));
timax{1} = 1:numel(merged_ic_data.time{1});            % index of each timestamp
t0(1) = 1; % find(merged_ic_data.time{1} > 0, 1, 'first');  % index of 1st timestamp after 0
for tr = 2:numel(merged_ic_data.trial)
  timax{tr} = timax{tr-1}(end) + (1:numel(merged_ic_data.time{tr}));
  t0(tr) = timax{tr-1}(end) + 1; % find(merged_ic_data.time{tr} > 0, 1, 'first');
end
duringtrls = merged_ic_data.trialinfo(:,end) == 2;
for c = 1:numel(merged_ic_data.label)
  fig = figure(c); clf; fig.WindowStyle = 'docked'; ax = gca; hold all
  for tr = 1:numel(merged_ic_data.trial)
    plot(timax{tr},merged_ic_data.trial{tr}(c,:));
    cmeans(tr,c) = mean(merged_ic_data.trial{tr}(c,:));
    cstds(tr,c) = std(merged_ic_data.trial{tr}(c,:));
    cskews(tr,c) = skewness(abs(merged_ic_data.trial{tr}(c,:)));
  end
  axis tight
  ax.XTick = t0(duringtrls);
  ax.XTickLabel = cellstr(num2str(merged_ic_data.trialinfo(duringtrls,1)));
  title(merged_ic_data.label{c})
end
fig = figure(c+1); clf; fig.WindowStyle = 'docked';
% subplot(3,1,1); imagesc(cmeans'); colormap(jet); colorbar;
ax = subplot(2,1,1); imagesc(cstds'); colormap(jet); colorbar; 
ax.CLim = quantile(cstds(:),[1/size(cstds,1) 1-1/size(cstds,1)]);
ax.XTick = find(duringtrls); title('Component Standard Deviations')
ax.XTickLabel = cellstr(num2str(merged_ic_data.trialinfo(duringtrls,1)));
ax = subplot(2,1,2); imagesc(cskews'); colormap(jet); colorbar;
ax.CLim = quantile(cskews(:),[1/size(cskews,1) 1-1/size(cskews,1)]);
ax.XTick = find(duringtrls); title('Component Skews')
ax.XTickLabel = cellstr(num2str(merged_ic_data.trialinfo(duringtrls,1)));
clearvars timax merged_ic_data

%% visualize results in the frequency domain

cfg = []; cfg.toilim = [-65 65]; ic_data = ft_redefinetrial(cfg,ic_data);
ic_data.trial = {[ic_data.trial{:}]};   % concatenate into 1 trial
ic_data.time = {(1:size(ic_data.trial{1},2))/1000};   % reconstruct time axis
cfg = get_cfg_TEM('wav','OrigFear');
cfg.output = 'pow'; cfg.keeptrials = 'no'; cfg.pad = 'nextpow2';
cfg.gwidth = 3*pi;  % increases time smoothing w/out affecting freq smoothing,
cfg.wavlen = (cfg.gwidth .* cfg.width) ./ (pi .* cfg.foi);  % just FYI, not needed by ft_freqanalysis
tstep = floor(cfg.wavlen(end)/2*fsample)/fsample;   % next smaller # of samples, from half the shortest wavelet length,
cfg.toi = ic_data.time{1}(1):tstep:ic_data.time{1}(end);  % allowing time windows to overlap by >= 50%
if numel(cfg.toi) > 1.4177e+06  % max size of time dim in complex array w/ 16 components & 167 frequencies
  error('will run out of memory')
end
cfg_math = []; cfg_math.parameter = 'powspctrm'; 
cfg_math.operation = '10*log10(x1)';
cfg_plot = []; cfg_plot.colormap = jet;
cfg_plot.showlabels = 'yes'; cfg_plot.layout = 'ordered'; 
% freqtime = nan(size(data.trial));
% for tr = 1:numel(data.trial)
%   tic
%   cfg.trials = find(ismember(ic_data.trialinfo,data.trialinfo(tr,:),'rows'));
  freq = ft_math(cfg_math,ft_freqanalysis(cfg,ic_data));
  cfg_plot.zlim = quantile(freq.powspctrm(:), ...
    [1/numel(freq.label) 1-1/numel(freq.label)]);
for c = 1:numel(ic_data.label)
  fig = figure(100+c); clf; fig.WindowStyle = 'docked';
  cfg_plot.channel = ic_data.label{c};
  ft_singleplotTFR(cfg_plot,freq); colorbar
  [~,sys] = memory;
%   freqtime(c) = toc  % 256s/trial for 1st 8 trials w/ mtmconvol, 85s/trial for 1st 3 trials w/ wavelet
  if sys.PhysicalMemory.Available < sys.PhysicalMemory.Total/2
    warning('high memory usage')
    keyboard % make some decisions & close some figures
  end
end

%% manually select which components to remove

rmvcn = input('Which component #s should be removed? (e.g., 1:3 or [6 8])  ');

%% remove selected components (from raw data) & review effect

if ~isempty(rmvcn)
  cfg = [];
  cfg.component = rmvcn;
  data_iccleaned = ft_rejectcomponent(cfg, ic_data, data);
  
  %% review cleaned data
  
  cfg = [];
  cfg.viewmode = 'vertical';
  cfg.continuous = 'no';
  
  cfg = ft_databrowser(cfg,data_iccleaned);
  data = data_iccleaned;  % rename variable before saving
end

%% save output

outputfile = [exptMeta.datafolder 'preproc' filesep ...
  num2str(exptMeta.ratT.ratnums(r)) filesep 'AllClean_ICA_LFPTrialData_' ...
  num2str(exptMeta.ratT.ratnums(r)) '.mat'];

disp(['saving data to ' outputfile(numel(exptMeta.datafolder):end)])
save(outputfile,'data');

end
