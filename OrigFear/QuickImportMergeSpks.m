%% import spikes from rat #200, channel 7, all units + unsorted

% INPUT:
%   filename - if empty string, will use File Open dialog
%   channel - 1-based channel number or channel name
%   unit  - unit number (0- unsorted, 1-4 units a-d)
%
% OUTPUT:
%   n - number of waveforms
%   npw - number of points in each waveform
%   ts - array of timestamps (in seconds) 
%   wave - array of waveforms [npw, n] converted to mV

filename = 'S:\Teresa\PlexonData Backup\200\200_091006_0001.plx';
channel = 7;
unit = 0:6;

alln = 0;       % total # of waveforms
allts = [];     % all timestamps
allwave = [];   % all waveforms

for u = unit
  [n, npw, ts, wave] = plx_waves_v(filename, channel, unit);
  alln = alln + n;
  allts = [allts; ts];
  allwave = [allwave; wave];
end