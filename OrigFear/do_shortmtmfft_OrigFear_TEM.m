function do_shortmtmfft_OrigFear_TEM( r )
% DO_SHORTMTMFFT_ORIGFEAR_TEM estimates spectra & coherence for short subtrials
%
%   Uses 4s periods immediately before & after tone onset, and before & after
%   tone offset, as well as 47*4 arbitrary 4s chunks of baseline.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/16/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder configfolder ratT fsample stimuli phases intervals

%% create waitbar

wb = waitbar(0,['Preparing for short mtmfft on rat #' num2str(ratT.ratnums(r))]);

%% define analysis parameters

mtmf = get_cfg_TEM('mtmfft', 'OrigFear');
% mtmf =
%   struct with fields:
%
%         method: 'mtmfft'
%         output: 'powandcsd'
%     keeptrials: 'yes'
%            pad: 32.7680   % changed to 4.0960 below
%         foilim: [0.7000 300]
%      tapsmofrq: 0.5000
timwin = 2/mtmf.tapsmofrq; % 4s segments right before/after tone onset/offset
mtmf.pad = (2^nextpow2(timwin*fsample))/fsample;

%% Load inputfile

inputfile = [datafolder 'preproc' filesep num2str(ratT.ratnums(r)) filesep ...
  'AllClean_ICA_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];

data = rmvlargefields_TEM(inputfile);     % strip excess historical metadata

assert(~any(any(isnan([data.trial{:}])))) % should have been removed in preproc

if ~isfield(data,'fsample') || isempty(data.fsample)
  data.fsample = fsample;
end

%% define baseline outputfile

outputfile = [datafolder 'shortmtmfft' filesep num2str(ratT.ratnums(r)) ...
  filesep 'Baseline_ICA_' num2str(ratT.ratnums(r)) 'shortmtmfft.mat'];

%% check if it exists

if ~existfile_TEM(outputfile)
  %% separate baseline
  
  cfg = [];
  cfg.trials  = data.trialinfo(:,2) == 0; 	% wherever there's no stimulus
  
  tmpdat = ft_selectdata(cfg, data);
  
  %% break baseline data into 4s sub-trials
  
  cfg = [];
  cfg.length    = timwin;
  cfg.overlap   = 0;
  
  tmpdat = ft_redefinetrial(cfg, tmpdat);
  
  cfg = [];
  cfg.trials  = 1:(47*4); 	% do all rats have at least 188?
  
  tmpdat = ft_selectdata(cfg, tmpdat);
  
  %% calculate baseline freqanalysis
  
  waitbar(0.1, wb, ...
    ['Calculating baseline shortmtmfft for rat #' num2str(ratT.ratnums(r))]);
  
  cfg = mtmf;   % default params defined in get_cfg_TEM, as editted above
  cfg.outputfile  = outputfile;
  
  freq = ft_freqanalysis(cfg, tmpdat);
  
  %   %% plot average if desired
  %
  %   cfg = [];
  %   cfg_math = [];
  %   cfg_math.parameter = 'powspctrm';
  %   cfg_math.operation = '10*log10(x1)';
  %   cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  %   ft_multiplotER(cfg,ft_math(cfg_math,freq))
  
  %% clear memory
  
  clearvars freq
end

%% trim tone-trials to 4s before/after tone onset

waitbar(0.5, wb, ...
  ['Preparing tone trials for short mtmfft, rat #' num2str(ratT.ratnums(r))]);

cfg = [];
cfg.trials  = data.trialinfo(:,2) == 1;   % whenever there's a tone

tmpdat = ft_selectdata(cfg, data);

BA_OnOff_dat = cell(2);   % 2 rows for before & after, 2 cols for on & offset

cfg = [];  cfg.toilim  = [-timwin 0];
BA_OnOff_dat{1,1} = ft_redefinetrial(cfg, tmpdat);
BA_OnOff_dat{1,1}.trialinfo = [BA_OnOff_dat{1,1}.trialinfo ...
  ones(size(BA_OnOff_dat{1,1}.trialinfo,1),1) ...   % indicates before tone
  2*ones(size(BA_OnOff_dat{1,1}.trialinfo,1),1)];   % indicates end of period

cfg.toilim  = [0 timwin];
BA_OnOff_dat{2,1} = ft_redefinetrial(cfg, tmpdat);
BA_OnOff_dat{2,1}.trialinfo = [BA_OnOff_dat{2,1}.trialinfo ...
  2*ones(size(BA_OnOff_dat{2,1}.trialinfo,1),1) ...   % indicates during tone
  ones(size(BA_OnOff_dat{2,1}.trialinfo,1),1)];   % indicates beginning of period

%% shocks need special treatment at tone offset

% Habituation
cfg.trials  = ismember(data.trialinfo,[(1:10)' ones(10,1) ones(10,1)],'rows');
cfg.toilim  = [30-timwin 30];
BA_OnOff_dat{1,2} = ft_redefinetrial(cfg, tmpdat);

% Recall & Extinction
cfg.trials  = ismember(data.trialinfo(:,3),2:3);
tmp2 = ft_redefinetrial(cfg, tmpdat);

% Acquisition
cfg.trials  = ismember(data.trialinfo,[(11:17)' ones(7,1) ones(7,1)],'rows');
cfg.toilim  = [29.2-timwin 29.2];
tmp1 = ft_redefinetrial(cfg, tmpdat);

BA_OnOff_dat{1,2} = ft_appenddata([],BA_OnOff_dat{1,2},tmp1,tmp2);

BA_OnOff_dat{1,2}.trialinfo = [BA_OnOff_dat{1,2}.trialinfo ...
  2*ones(size(BA_OnOff_dat{1,2}.trialinfo,1),1) ...   % indicates during tone
  2*ones(size(BA_OnOff_dat{1,2}.trialinfo,1),1)];     % indicates end of period

% Habituation
cfg.trials  = ismember(data.trialinfo,[(1:10)' ones(10,1) ones(10,1)],'rows');
cfg.toilim  = [30 30+timwin];
BA_OnOff_dat{2,2} = ft_redefinetrial(cfg, tmpdat);

% Recall & Extinction
cfg.trials  = ismember(data.trialinfo(:,3),2:3);
tmp2 = ft_redefinetrial(cfg, tmpdat);

% Acquisition
cfg.trials  = ismember(data.trialinfo,[(11:17)' ones(7,1) ones(7,1)],'rows');
cfg.toilim  = [31.5 31.5+timwin];
tmp1 = ft_redefinetrial(cfg, tmpdat);

BA_OnOff_dat{2,2} = ft_appenddata([],BA_OnOff_dat{2,2},tmp1,tmp2);

BA_OnOff_dat{2,2}.trialinfo = [BA_OnOff_dat{2,2}.trialinfo ...
  3*ones(size(BA_OnOff_dat{2,2}.trialinfo,1),1) ...   % indicates after tone
  ones(size(BA_OnOff_dat{2,2}.trialinfo,1),1)];   % indicates beginning of period

clearvars tmp*

%% calculate mtmfft for each interval

rownames = {'Before','After'};
colnames = {'Onset','Offset'};

for rn = 1:numel(rownames)
  for cn = 1:numel(colnames)
    %% define outputfile
    
    outputfile = [datafolder 'shortmtmfft' filesep num2str(ratT.ratnums(r)) ...
      filesep rownames{rn} 'Tone' colnames{cn} '_ICA_' ...
      num2str(ratT.ratnums(r)) 'shortmtmfft.mat'];
    
    %% check if it exists
    
    if ~existfile_TEM(outputfile)
      %% calculate subtrial spectra
      
      waitbar(0.5 + (rn-1+(cn-1)/numel(colnames))/(numel(rownames)), ...
        wb, ['Calculating shortmtmfft for ' rownames{rn} 'Tone' colnames{cn} ...
        ' of rat #' num2str(ratT.ratnums(r))]);
      
      cfg             = mtmf;
      cfg.outputfile  = outputfile;
      
      freq = ft_freqanalysis(cfg,BA_OnOff_dat{rn,cn});
      

%     %% plot average if desired
%     
%     figure;    cfg = [];    cfg_math = [];
%     cfg_math.parameter = 'powspctrm';
%     cfg_math.operation = '10*log10(x1)';
%     cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
%     ft_multiplotER(cfg,ft_math(cfg_math,freq))
        
      %% clear memory
      
      clearvars freq
    end
  end
end

close(wb)