function do_mtmfft_OrigFear_TEM( r )
% DO_MTMFFT_ORIGFEAR_TEM does spectral & coherence analysis on whole subtrials
%
%   Uses arbitrary 30s chunks throughout baseline, and the 30s periods before,
%   during, and after each tone trial.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/9/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder ratT fsample stimuli phases intervals

%% create waitbar

wb = waitbar(0,['Preparing for mtmfft on rat #' num2str(ratT.ratnums(r))]);

%% define analysis parameters

mtmf = get_cfg_TEM('mtmfft', 'OrigFear');
% mtmf =
%   struct with fields:
%
%         method: 'mtmfft'
%         output: 'powandcsd'
%     keeptrials: 'yes'
%            pad: 32.7680   % changed to 
%         foilim: [0.7000 300]
%      tapsmofrq: 0.5000
timwin = 2/mtmf.tapsmofrq; % short segments right before/after tone onset/offset
mtmf.pad = (2^nextpow2(timwin*fsample))/fsample;

%% Load inputfile

inputfile = [datafolder 'preproc' filesep num2str(ratT.ratnums(r)) filesep ...
  'AllClean_shockt_ICA_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];

data = rmvlargefields_TEM(inputfile);     % strip excess historical metadata

assert(~any(any(isnan([data.trial{:}])))) % should have been removed in preproc

if ~isfield(data,'fsample') || isempty(data.fsample)
  data.fsample = fsample;
end

%% define baseline outputfile

outputfile = [datafolder 'mtmfft' filesep num2str(ratT.ratnums(r)) ...
  filesep 'Baseline_shockt_ICA_' num2str(ratT.ratnums(r)) 'mtmfft.mat'];

%% check if it exists

% if ~existfile_TEM(outputfile)
  %% separate baseline
  
  cfg = [];
  cfg.trials  = data.trialinfo(:,2) == 0; 	% wherever there's no stimulus
  
  tmpdat = ft_selectdata(cfg, data);
  
  %% break baseline data into 30s sub-trials
  
  cfg = [];
  cfg.length    = 30;
  cfg.overlap   = 0;
  
  tmpdat = ft_redefinetrial(cfg, tmpdat);
  
  %   cfg = [];
  %   cfg.trials  = 1:20; 	% all rats have at least 87?
  %
  %   tmpdat = ft_selectdata(cfg, tmpdat);
  
  %% realign all trials to start at time 0 to avoid overburdening memory
  % otherwise, it pads each trial out to the length of time covered by all the
  % trials together
  
  cfg = [];
  cfg.offset  = [0; -cumsum(diff(tmpdat.sampleinfo(:,1)))];
  
  tmpdat = ft_redefinetrial(cfg, tmpdat);
  
  %% calculate baseline freqanalysis
  
  waitbar(0.1, wb, ...
    ['Calculating baseline mtmfft for rat #' num2str(ratT.ratnums(r))]);
  
  cfg = mtmf;   % default params defined in get_cfg_TEM (recorded above)
  cfg.outputfile  = outputfile;
  
  freq = ft_freqanalysis(cfg, tmpdat);
  
  %% clear memory
  
  clearvars freq
% end

%% trim tone-trials to 30s before/during/after tones

waitbar(0.5, wb, ...
  ['Preparing tone trials for mtmfft, rat #' num2str(ratT.ratnums(r))]);

cfg = [];
cfg.trials  = data.trialinfo(:,2) == 1;   % wherever there's a CS+

tmpdat = ft_selectdata(cfg, data);

cfg = [];  cfg.toilim  = [-30 0];
BDAdat{1} = ft_redefinetrial(cfg, tmpdat);
BDAdat{1}.trialinfo = [BDAdat{1}.trialinfo ...
  ones(size(BDAdat{1}.trialinfo,1),1)];

cfg.toilim  = [0 30];
BDAdat{2} = ft_redefinetrial(cfg, tmpdat);
BDAdat{2}.trialinfo = [BDAdat{2}.trialinfo ...
  2*ones(size(BDAdat{2}.trialinfo,1),1)];

cfg.toilim  = [30 60];
BDAdat{3} = ft_redefinetrial(cfg, tmpdat);
BDAdat{3}.trialinfo = [BDAdat{3}.trialinfo ...
  3*ones(size(BDAdat{3}.trialinfo,1),1)];

%% loop through all intervals

% trlinfo = unique(tmpdat.trialinfo,'rows','stable');
% for tr = 1:size(trlinfo,1)  % trial # across all phases
for ti = 1:numel(intervals)   % B/D/A
  %% define outputfile
  
  outputfile = [datafolder 'mtmfft' filesep num2str(ratT.ratnums(r)) filesep ...
    intervals{ti} 'Tones_shockt_ICA_' num2str(ratT.ratnums(r)) 'mtmfft.mat'];
  
  %% check if it exists
  
%   if ~existfile_TEM(outputfile)
    %% calculate subtrial spectra
    
    waitbar(0.5 + (ti-1)/(2*numel(intervals)), ...
      wb, ['Calculating mtmfft for ' intervals{ti} ' tone trials of rat #' ...
      num2str(ratT.ratnums(r))]);
    
    cfg             = mtmf;
    cfg.outputfile  = outputfile;
    
    freq = ft_freqanalysis(cfg,BDAdat{ti});
    
    %% clear memory
    
    clearvars freq
%   end
end

close(wb)