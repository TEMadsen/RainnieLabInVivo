function do_ToneOnOffsetPower_OrigFear_TEM( r )
% DO_TONEONOFFSETPOWER_ORIGFEAR_TEM compares power changes at tone on/offset
% across blocks
%
%   This function is for within-subject visualization & stats on the output of
%   shortmtmfft.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/19/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder configfolder ratT fsample stimuli phases intervals

%% create waitbar

wb = waitbar(0,['Preparing for tone on/offset power analysis on rat #' ...
  num2str(ratT.ratnums(r))]);

%% Load inputfile for each interval

freq = cell(2);
rownames = {'Before','After'};
colnames = {'Onset','Offset'};

for rn = 1:numel(rownames)
  for cn = 1:numel(colnames)
    %% load inputfile
    
    inputfile = [datafolder 'shortmtmfft' filesep num2str(ratT.ratnums(r)) ...
      filesep rownames{rn} 'Tone' colnames{cn} '_shockt_ICA_' ...
      num2str(ratT.ratnums(r)) 'shortmtmfft.mat'];
    
    freq{rn,cn} = rmvlargefields_TEM(inputfile);
  end
end

%% normalize after by before (dB transform of power ratio)

cfg = [];
cfg.parameter = 'powspctrm';
cfg.operation = '10*log10(x1./x2)';

freqchange = cell(1,2);

for cn = 1:numel(colnames)
  if ~isequal(freq{1,cn}.trialinfo(:,1:3),freq{2,cn}.trialinfo(:,1:3))
    [~,I12] = ismember(freq{1,cn}.trialinfo(:,1:3), ...
      freq{2,cn}.trialinfo(:,1:3),'rows');
    [~,I21] = ismember(freq{2,cn}.trialinfo(:,1:3), ...
      freq{1,cn}.trialinfo(:,1:3),'rows');
    if ~all(I21)
      error('after trials are expected to match before trials')
    end
    if ~all(I12)
      cfg_trl = []; cfg_trl.trials = I21;
      freq{1,cn} = ft_selectdata(cfg_trl,freq{1,cn});
    end
    if ~isequal(freq{1,cn}.trialinfo(:,1:3),freq{2,cn}.trialinfo(:,1:3))
      error('trial selection failed')
    end
  end
  
  freqchange{cn} = ft_math(cfg,freq{2,cn},freq{1,cn});
  freqchange{cn}.trialinfo = freq{1,cn}.trialinfo(:,1:3);
end

%% smooth the spectra similarly to mtmc.tapsmofrq

w0 = 0.5;                % 0.5 Hz smoothing at DC
mtmc.foi = 2.^(round(log2(0.7)*20)/20:(1/20):round(log2(300)*20)/20);
m = (mtmc.foi(end)-mtmc.foi(end-1)-w0)/300;  % 0.03 Hz change in smoothing per Hz change in foi
mtmc.tapsmofrq = m.*mtmc.foi + w0;  % linear increase in smoothing between wlim(1) at DC & wlim(2) for foilim(2)

smofrqchng = cell(1,2);

for cn = 1:numel(colnames)
  smofrqchng{cn} = freqchange{cn};
  smofrqchng{cn}.freq = mtmc.foi;
  smofrqchng{cn}.powspctrm = nan(size(freqchange{cn}.powspctrm,1), ...
    size(freqchange{cn}.powspctrm,2), numel(smofrqchng{cn}.freq));
  for f = 1:numel(smofrqchng{cn}.freq)
    fndx = abs(freqchange{cn}.freq-smofrqchng{cn}.freq(f)) < ...
      mtmc.tapsmofrq(f);
    smofrqchng{cn}.powspctrm(:,:,f) = median(...  % to preserve sharp changes
      freqchange{cn}.powspctrm(:,:,fndx),3,'omitnan');
  end
end

%% prepare_neighbours determines what sensors may form clusters

cfg = [];
cfg.method         = 'distance';
cfg.neighbourdist  = 1;  % mm
cfg.elec           = rmvlargefields_TEM([configfolder 'elec_' ...
  ratT.MWA{r} '_' ratT.side{r} '.mat']);
neighbours        = ft_prepare_neighbours(cfg);
if any(cellfun(@numel,{neighbours(:).neighblabel}) < 7)
  warning('unexpected # of neighbors')
  cfg = [];
  cfg.neighbours = neighbours;
  cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  ft_neighbourplot(cfg)
  keyboard
end

%% select data & plot before vs. after comparison

blks = {'Hab','Acq','Rcl','Ext'};
blktrlinfo = cat(3, [(4:10)' ones(7,1) ones(7,1)], ...
  [(11:17)' ones(7,1) ones(7,1)], [(1:7)' ones(7,1) 2*ones(7,1)], ...
  [(9:15)' ones(7,1) 3*ones(7,1)]);
blkfreq = cell(size(blktrlinfo,3),1);

for cn = 1:numel(colnames)  % tone on/offset
  for b = 1:size(blktrlinfo,3)
    cfg = [];
    cfg.trials = ismember(smofrqchng{cn}.trialinfo(:,1:3), blktrlinfo(:,:,b), ...
      'rows');
    
    blkfreq{b,cn} = ft_selectdata(cfg,smofrqchng{cn});
    
    cfg = [];
    cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
    cfg.comment = [blks{b} ' Tone ' colnames{cn}];
    cfg.feedback        = 'no';
    figure;
    ft_multiplotER(cfg,blkfreq{b,cn})
    %     figure;
    %     errorbar(repmat(blkfreq{b,cn}.freq(:),1,16), ...
    %       squeeze(mean(blkfreq{b,cn}.powspctrm,1))', ...
    %       squeeze(std(blkfreq{b,cn}.powspctrm,0,1))')
  end
end

%% run within-subject stats on power spectra

stat = cell(numel(blks),numel(colnames));

for b = 1:size(blktrlinfo,3)
  for cn = 1:numel(colnames)  % tone on/offset
    cfg = [];
    cfg.outputfile  = [datafolder 'ToneOnOffsetPower' filesep ...
      num2str(ratT.ratnums(r)) filesep blks{b} ' Tone ' colnames{cn} '_ICA_' ...
      num2str(ratT.ratnums(r)) 'stat_indepsamplesregrT_NPindiv_ordered.mat'];
    
    if existfile_TEM(cfg.outputfile)
      stat{p} = rmvlargefields_TEM(cfg.outputfile);
    else
      starttime = tic;  % only time computation, not just loading files
      
      cfg.statistic         = 'ft_statfun_indepsamplesregrT';
      
      waitbar(sum(comptime(:),'omitnan')/sum(esttime.*size(freq,1)), wb, ...
        ['Running ' cfg.statistic ' on ' blks{b} ' Tone ' colnames{cn} ...
        ' short mtmfft spectrograms for rat #' num2str(ratT.ratnums(r))]);
      
      cfg.method            = 'montecarlo';
      cfg.correctm          = 'cluster';
      cfg.clusteralpha      = 0.025;
      cfg.clusterstatistic  = 'maxsum';
      cfg.clusterthreshold  = 'nonparametric_individual';   % essential for reasonable thresholds!
      cfg.minnbchan         = 1;
      cfg.tail              = 0;
      cfg.clustertail       = 0;
      cfg.alpha             = 0.025;
      cfg.numrandomization  = 200;
      cfg.orderedstats      = 'yes';  % essential for reasonable thresholds!
      cfg.neighbours        = neighbours;
      cfg.parameter         = 'powspctrm';
      
      cfg.design  = 1:size(blkfreq{b,cn}.trialinfo,1);
      
      cfg.ivar    = 1;  % uvar = ivar (between-UO)
      
      stat{b,cn} = ft_freqstatistics(cfg, blkfreq{b,cn});
    end
  end
end

%% print & plot significant differences in power spectra

for b = 1:size(blktrlinfo,3)
  for cn = 1:numel(colnames)  % tone on/offset
    npos = sum([stat{b,cn}.posclusters.prob] < 0.05);
    nneg = sum([stat{b,cn}.negclusters.prob] < 0.05);
    bestch = zeros(npos+nneg+2,1);
    disp([newline 'Found clusters with significant positive (' ...
      num2str(npos) ') and negative (' num2str(nneg) ')' newline ...
      'correlations with trial #s within ' blks{b} ' Tone ' colnames{cn} ...
      ':' newline])
    cfg =[];
    cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
    cfg.parameter = 'stat';
    cfg.feedback  = 'no';
    for n = 1:(npos + 1) % plot at least one that's not considered significant
      [ch,f] = find(stat{b,cn}.posclusterslabelmat == n);
      bestch(n) = mode(ch);
      frange = [stat{b,cn}.freq(min(f)) stat{b,cn}.freq(max(f))];
      % plot mean +/- std across freqs for each chan w/in cluster
      figure; subplot(1,2,1); hold all
      for chI = unique(ch)'
        errorbar(blkfreq{b,cn}.trialinfo(:,1), ...
          mean(blkfreq{b,cn}.powspctrm(:,chI,f(ch == chI)),3), ...
          std(blkfreq{b,cn}.powspctrm(:,chI,f(ch == chI)),0,3))
      end
      title([strjoin(stat{b,cn}.label(unique(ch))) newline ...
        'have significant (p = ' num2str(stat{b,cn}.posclusters(n).prob) ...
        ') positive correlations' newline ...
        'at ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz' newline ...
        'with trial #s within ' blks{b} ' Tone ' colnames{cn} ':'])
      % plot topographic extent of correlation
      cfg.xlim = frange; subplot(1,2,2); ft_topoplotER(cfg,stat{b,cn});
    end
    for n = 1:(nneg + 1) % plot at least one that's not considered significant
      [ch,f] = find(stat{b,cn}.negclusterslabelmat == n);
      bestch(npos + 1 + n) = mode(ch);
      frange = [stat{b,cn}.freq(min(f)) stat{b,cn}.freq(max(f))];
      % plot mean +/- std across freqs for each chan w/in cluster
      figure; subplot(1,2,1); hold all
      for chI = unique(ch)'
        errorbar(blkfreq{b,cn}.trialinfo(:,1), ...
          mean(blkfreq{b,cn}.powspctrm(:,chI,f(ch == chI)),3), ...
          std(blkfreq{b,cn}.powspctrm(:,chI,f(ch == chI)),0,3))
      end
      title([strjoin(stat{b,cn}.label(unique(ch))) newline ...
        'have significant (p = ' num2str(stat{b,cn}.negclusters(n).prob) ...
        ') negative correlations' newline ...
        'at ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz' newline ...
        'with trial #s within ' blks{b} ' Tone ' colnames{cn} ':'])
      % plot topographic extent of correlation
      cfg.xlim = frange; subplot(1,2,2); ft_topoplotER(cfg,stat{b,cn});
    end
    cfg=[];
    cfg.channel         = mode(bestch); % ratT.mPFC{r};
    cfg.parameter       = 'stat';
    cfg.maskparameter   = 'mask';
    cfg.maskstyle       = 'box';
    cfg.feedback        = 'no';
    figure; subplot(2,2,1:2)
    ft_singleplotER(cfg,stat{b,cn});
    title([blks{b} ' Tone ' colnames{cn}])
    f = stat{b,cn}.freq > 1.5 & stat{b,cn}.freq < 4; subplot(2,2,3); hold all
    for ch = 1:numel(blkfreq{b,cn}.label)
      errorbar(blkfreq{b,cn}.trialinfo(:,1), ...
        mean(blkfreq{b,cn}.powspctrm(:,ch,f),3), ...
        std(blkfreq{b,cn}.powspctrm(:,ch,f),0,3))
    end
    title('Delta')
    f = stat{b,cn}.freq > 45 & stat{b,cn}.freq < 60; subplot(2,2,4); hold all
    for ch = 1:numel(blkfreq{b,cn}.label)
      errorbar(blkfreq{b,cn}.trialinfo(:,1), ...
        mean(blkfreq{b,cn}.powspctrm(:,ch,f),3), ...
        std(blkfreq{b,cn}.powspctrm(:,ch,f),0,3))
    end
    title('Mid-Gamma')
  end
end

%%
error('finish coding!')

