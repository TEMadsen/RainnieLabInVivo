function [output] = do_wavelet_raw_OrigFear_TEM( r )
% DO_WAVELET_RAW_ORIGFEAR_TEM conducts time-frequency analysis by trials on raw data
%
%   Default parameters documented below.
%
%   Depends on global variables defined in info_origfear.
%
% written 7/21/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder ratT fsample stimuli phases

%% create waitbar

wb = waitbar(0,['Preparing for wavelet decomposition on raw data from rat #' ...
  num2str(ratT.ratnums(r))]);
tic

%% define analysis parameters

wav = get_cfg_TEM('wav', 'OrigFear');
% wav = 
%   struct with fields:
% 
%         method: 'wavelet'
%         output: 'fourier'
%     keeptrials: 'yes'
%            pad: 131.0720
%            foi: [1�167 double] freq range 0.84 to 265 Hz
%          width: 9 means smooth freq by +/- 0.09 to +/- 29 Hz
%         gwidth: 3.1416
%         wavlen: [1�167 double] window range 10.7 to 0.034 s
%            toi: [1�7501 double] -60:0.016:60

%% Load inputfile

% Raw LFP data divided into long, non-overlapping tone-triggered trials, merged
% across all recordings for each rat
inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
  'RawLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];

data = rmvlargefields_TEM(inputfile);   % strip excess historical metadata

if ~isfield(data,'fsample') || isempty(data.fsample)
  data.fsample = fsample;
end

data = ft_datatype_raw(data,'hassampleinfo','yes');   % reconstruct if necessary

%% redefine trials to include at least 75s before & after each tone onset
% will allow equal data lengths for activation vs. baseline comparison, assumed
% for later wavSpecRegr stats

% establish current trial definition
if isfield(data.cfg,'trl')
  trl = data.cfg.trl;
else
  trl = [data.sampleinfo round(cellfun(@min,data.time)'*data.fsample) ...
    data.trialinfo];
end

cfg = [];
cfg.trl = trl;
for tr = find(cfg.trl(:,4) ~= 1)'  % skip 1st trial of any recording session
  if cfg.trl(tr,3) > -75*data.fsample
    adjust = cfg.trl(tr,3) + 75*data.fsample;   % # of samples to shift trials by
    cfg.trl(tr-1,2) = cfg.trl(tr-1,2) - adjust;
    cfg.trl(tr,[1 3]) = cfg.trl(tr,[1 3]) - adjust;
  end
end

if ~isequal(cfg.trl,trl)  % if different from current trial definition
  cfg.outputfile = inputfile;   % save back to original filename
  data = ft_redefinetrial(cfg,data);
end

%% define baseline outputfile

comptime = nan(numel(data.trial),1);
esttime = 1589;  % in s, total based on 1st run:  sum(comptime)

outputfile = [datafolder 'wavelet_raw' filesep num2str(ratT.ratnums(r)) ...
  filesep 'Baseline_raw_' num2str(ratT.ratnums(r)) 'waveletTFAfourier.mat'];

%% check if it exists

% if ~existfile_TEM(outputfile)
  %% separate baseline
  
  tic
  waitbar(0, wb, ['Calculating baseline wavelet TFA on raw data from rat #' ...
    num2str(ratT.ratnums(r))]);
  
  cfg = [];
  cfg.trials  = data.trialinfo(:,2) == 0; 	% wherever there's no stimulus
  
  tmpdat = ft_selectdata(cfg, data);
  
  assert(~any(any(isnan([tmpdat.trial{:}]))))
  
  %% break baseline data into 20 sub-trials of length wav.pad
  
  cfg = [];
  cfg.length    = wav.pad;
  cfg.overlap   = 0;
  
  tmpdat = ft_redefinetrial(cfg, tmpdat);
  
  cfg = [];
  cfg.trials  = 1:20; 	% all rats have at least 20
  
  tmpdat = ft_selectdata(cfg, tmpdat);
  
  %% realign all trials to start at time 0 to avoid overburdening memory
  % otherwise, it pads each trial out to the length of time covered by all the
  % trials together
  
  cfg = [];
  cfg.offset  = [0; -cumsum(diff(tmpdat.sampleinfo(:,1)))];
  
  tmpdat = ft_redefinetrial(cfg, tmpdat);
  
  %% calculate baseline freqanalysis
  
  cfg = wav;   % default params defined in get_cfg_TEM, adjust for baseline
  cfg.toi = (tmpdat.time{1}(1) + wav.wavlen(1)): ...
    (wav.toi(2)-wav.toi(1)):(tmpdat.time{1}(end) -  wav.wavlen(1));
  cfg.keeptrials  = 'yes';
  cfg.outputfile  = outputfile;
  
  freq = ft_freqanalysis(cfg, tmpdat);
  
  %% clear memory
  
  clearvars freq
  comptime(end) = toc;
% end

%% trim tone-trials to core wav.pad time

cfg = [];
cfg.trials  = data.trialinfo(:,2) == 1;   % wherever there's a CS+

tmpdat = ft_selectdata(cfg, data);

cfg = [];
cfg.toilim  = [median(wav.toi) - wav.pad/2 + 1/data.fsample, ...
  median(wav.toi) + wav.pad/2 - 1/data.fsample];   % cut 2 samples to avoid rounding errors

tmpdat = ft_redefinetrial(cfg, tmpdat);

%% loop through all tone trials

trlinfo = unique(tmpdat.trialinfo,'rows','stable');
for tr = 1:size(trlinfo,1)  % trial # across all phases
  %% define trial outputfile
  
  tn = trlinfo(tr,1);   % trial number within phase
  s = trlinfo(tr,2);    % stimulus type (1 for CS+ or 0 for none)
  p = trlinfo(tr,3);    % phase number
  
  outputfile = [datafolder 'wavelet_raw' filesep num2str(ratT.ratnums(r)) ...
    filesep stimuli{s} num2str(tn) 'of' phases{p} '_raw_' ...
    num2str(ratT.ratnums(r)) 'waveletTFAfourier.mat'];
  
%   %% check if it exists
%   
%   if existfile_TEM(outputfile)
%     continue  % to next trial
%   end

    %% calculate trial spectrogram
    
  tic
  waitbar(sum(comptime,'omitnan')/esttime, ...
    wb, ['Calculating wavelet TFA on raw data from tone trial #' num2str(tr) ...
      ', rat #' num2str(ratT.ratnums(r))]);
    
    cfg             = wav;
    cfg.trials      = ismember(tmpdat.trialinfo, trlinfo(tr,:), 'rows');
    cfg.outputfile  = outputfile;
    
    freq = ft_freqanalysis(cfg,tmpdat);
    
    %% clear memory
    
    clearvars freq
    comptime(tr) = toc;
    esttime = (numel(comptime)+19)*sum(comptime,'omitnan')/(20+tr);

end

%% clean up

output = comptime;
close(wb)