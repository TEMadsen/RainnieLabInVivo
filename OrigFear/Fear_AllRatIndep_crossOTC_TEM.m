%% calculates 2d cross-regional cross-frequency phase-amplitude modulation
% uses Dvorak & Fenton's Oscillation Triggered Comodulogram
% for all fear data

CleanSlate  % provides a clean slate to start working with new data/scripts

hx = waitbar(0,'Initializing...');   % will display progress through loop

info_origfear   % load all background info & final parameters

zthresh   = 15;     % use data cleaned with this artifact threshold
cthresh   = 3;      % and this clipping threshold
plt       = false;  % plot CFC
redo      = false;  % to ignore & overwrite old files
skiprats  = [];     % already processed or running on another computer

errlog = cell(size(ratT,1), sum(expected));   % preallocate for error messages
rattime = NaN(size(ratT,1), 1);   % preallocate for computation time per rat

%% load saved analysis params

paramfile = [datafolder 'crossOTCparams.mat'];

if exist(paramfile,'file')
  load(paramfile)
else
  
  %% set analysis parameters
  
  cfg = [];
  cfg.Fs        = 1000;         % Plexon's LFP sampling frequency
  cfg.foi_mod   = 20:300;       % central frequencies of modulated signals
  cfg.wFactor   = 8;            % scale factor
  cfg.powerTh   = 2;            % power threshold (S.D.)
  cfg.winLen    = 0.6*cfg.Fs;   % OTC window (+/- 0.6 s from oscillatory event)
  cfg.nfft      = 2^nextpow2(2*cfg.winLen);             % # of frequencies for fft
  cfg.f_fft     = cfg.Fs/2*linspace(0,1,cfg.nfft/2+1);  % actual freqs of fft
  cfg.f_plot    = cfg.f_fft<cfg.foi_mod(1);             % which freqs to plot
  
  % wavelet frequencies to find oscillations
  cfg.foi_osc = floor(cfg.foi_mod(1)-cfg.foi_mod(1)/cfg.wFactor):...
    ceil(cfg.foi_mod(end)+cfg.foi_mod(end)/cfg.wFactor);
  
  %% create set of Morlet wavelets - check this for accuracy!
  
  wavelets = cell(size(cfg.foi_osc));
  for bI = 1:length(cfg.foi_osc)
    f = cfg.foi_osc(bI);
    sigmaF = f/cfg.wFactor; % practical setting for spectral bandwidth (Tallon-Baudry)
    sigmaT = 1/(sigmaF*pi*2); % wavelet duration
    t = -4*sigmaT*cfg.Fs:4*sigmaT*cfg.Fs;
    t = t/cfg.Fs;
    if rem(length(t),2) == 0; t = t(1:end-1); end
    S1 = exp((-1*(t.^2)) / (2*sigmaT^2));
    S2 = exp(2*1i*pi*f*t);
    A = (sigmaT * sqrt(pi))^(-0.5); % normalization
    psi = A*S1.*S2;
    wavelets{bI} = psi;
  end
  
  save(paramfile,'cfg','wavelets');
end

%% make 2d Cross-Frequency Modulation plots for each rat/session/interval

for r = setdiff(find(~ratT.excluded)',skiprats)
  %   try
  tic   % will time computations per rat
  
  %% define inputfile
  
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllClean_z' num2str(zthresh) '_clip' num2str(cthresh) ...
    '_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];
  outputfile1 = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'WaveletTransform_' num2str(ratT.ratnums(r)) '.mat'];
  outputfile2 = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'crossOTCave_' num2str(ratT.ratnums(r)) '.mat'];
  
  if exist(outputfile1,'file') && exist(outputfile2,'file')
    disp(['Data for Rat # ' num2str(ratT.ratnums(r)) ' already analyzed!']);
    if plt
      disp('Loading previous analysis results from file.');
      load(outputfile2);
    end
  else
    %% preallocate memory for variables
    
    zscore = cell(sum(expected),2,2);
    pval = cell(sum(expected),2,2);
    h = cell(sum(expected),2,2);
    modS = cell(sum(expected),2,2);
    otc = cell(sum(expected),2,2);
    fft_otc = cell(sum(expected),2,2);
    pwrN = cell(sum(expected),2);
    
    fpeak = cell(sum(expected),2,2);
    otcpeak = cell(sum(expected),2,2);
    L = cell(sum(expected),1);
    Lsort = cell(sum(expected),1);
    otcmean = cell(sum(expected),2,2);
    fftmean = cell(sum(expected),2,2);
    modSmean = cell(sum(expected),2,2);
    cmax = 0;
    
    %% load data
    
    load(inputfile)
    
    %     S = load([datafolder num2str(ratT.ratnums(r)) filesep ...
    %       'Clean_z' num2str(zthresh) '_clip' num2str(cthresh) '_' ...
    %       ratT.mPFC{r} 'SubTrialData_' num2str(ratT.ratnums(r)) '.mat'],'data');
    %     datmPFC = S.data;
    %     S = load([datafolder num2str(ratT.ratnums(r)) filesep ...
    %       'Clean_z' num2str(zthresh) '_clip' num2str(cthresh) '_' ...
    %       ratT.BLA{r} 'SubTrialData_' num2str(ratT.ratnums(r)) '.mat'],'data');
    %     datBLA = S.data;
    
    %% reduce to channels of interest (latency keeps screwing up!)
    
    tmpcfg = [];
    tmpcfg.channel  = [ratT.mPFC(r); ratT.BLA(r)];
    
    data = ft_selectdata(tmpcfg,data);
    
    %% remove NaNs that replaced artifacts
    
    tmpcfg = [];
    tmpcfg.artfctdef.reject   = 'partial';
    tmpcfg.artfctdef.minaccepttim = (max(cellfun(@length,wavelets)) + ...
      cfg.winLen*2) / cfg.Fs;
    tmpcfg.artfctdef.nan.artifact    = artifact_nan(data);
    
    data = ft_rejectartifact(tmpcfg, data);
    
    %% analyze data
    
    trlinfo = unique(data.trialinfo,'rows','stable');
    for tr = 1:size(trlinfo,1)
      trials = find(ismember(data.trialinfo,trlinfo(tr,:),'rows'));
      for t = 1:numel(trials)
        waitbar(((t-1)/numel(trials)+(tr-1))/size(trlinfo,1),hx,...
          {['Calculating OTC for Rat # ' ...
          num2str(ratT.ratnums(r)) ',']; ...
          ['trialinfo = ' num2str(trlinfo(tr,:))]});
        toi = data.time{trials(t)} > -32 & data.time{trials(t)} < 62;
        trdata = data.trial{trials(t)}(:,toi);
        [zscoretemp,pvaltemp,htemp,modStemp,otctemp,...
          fft_otctemp,pwrNtemp] = ...
          crossOTCave_TEM(trdata,cfg,wavelets);
        for pr = 1:2 % each phase modulating region
          pwrN{tr,pr}{t}(:,:) = ...
            pwrNtemp{pr};
          for ar = 1:2 % each amp modulated region
            zscore{tr,pr,ar}(:,t) = ...
              zscoretemp{pr,ar};
            pval{tr,pr,ar}(:,t) = ...
              pvaltemp{pr,ar};
            h{tr,pr,ar}(:,t) = ...
              htemp{pr,ar};
            modS{tr,pr,ar}(:,t) = ...
              modStemp{pr,ar};
            otc{tr,pr,ar}(:,:,t) = ...
              otctemp{pr,ar};
            fft_otc{tr,pr,ar}(:,:,t) = ...
              fft_otctemp{pr,ar};
          end
        end
      end
      for pr = 1:2 % each phase modulating region
        for ar = 1:2 % each amp modulated region
          M = max(nanmean(zscore{tr,pr,ar},2),[],...
            'omitnan');
          cmax = max(M,cmax);
          % CONSIDER WEIGHTING TRIALS BY # OF EVENTS
          otcmean{tr,pr,ar} = nanmean(...
            otc{tr,pr,ar}(:,:,:),3);
          fftmean{tr,pr,ar} = fft(...
            otcmean{tr,pr,ar},cfg.nfft,2);
          modSmean{tr,pr,ar} = range(...
            otcmean{tr,pr,ar},2);
          [~,I] = max(modSmean{tr,pr,ar},[],...
            'omitnan');
          fpeak{tr,pr,ar} = cfg.foi_mod(I);
          otcpeak{tr,pr,ar} = ...
            otcmean{tr,pr,ar}(I,:);
        end
      end
    end
  end
  
  %% save analysis results
  %                 keyboard; % double-check result before saving!
  disp(['writing wavelet transform to file ' outputfile1]);
  save(outputfile1, 'pwrN', '-v7.3');
  
  disp(['writing OTC results to file ' outputfile2]);
  save(outputfile2, 'cfg', 'zscore', 'pval', 'h', ...
    'modS*', 'fft*', 'otc*', 'L*', 'fpeak', 'cmax', ...
    '-v7.3');
  
end
if plt == 'y'
  %% plot results - needs updating!!!
  for tr = 1:sum(expected)
    if isempty(zscore{tr,1,1})
      warning(['No ' join(string(data.trialinfo(tr,:)),'_') ...
        ' trials found for Rat # ' ...
        num2str(ratT.ratnums(r)) ', ' phases{p} ...
        '.  Skipping.']);
    else
      for pr = 1:2 % each phase modulating region
        for ar = 1:2 % each amp modulated region
          %% plot z-scores for each trial + mean
          figure(pr+(ar-1)*2); clf;
          imagesc([], cfg.foi_mod, ...
            [zscore{tr,pr,ar}(:,Lsort{tr}) ...
            nanmean(zscore{tr,pr,ar},2)],...
            [0 max(max(zscore{tr,pr,ar}))]);
          axis xy tight; hold on;
          plot([size(zscore{tr,pr,ar},2)+0.5 ...
            size(zscore{tr,pr,ar},2)+0.5],...
            [cfg.foi_mod(1) cfg.foi_mod(end)],...
            'w','LineWidth',2);
          set(gca, 'FontName', 'Arial', 'FontSize', 12);
          xlabel('Interaction Interval (sorted by duration) + Mean');
          ylabel('Modulated Frequency (Hz)');
          title({['Rat # ' num2str(ratT.ratnums(r)) ', ' ...
            char(phases(p)) ', ' join(string(data.trialinfo(tr,:)),'_')]; ...
            [regions{pr} ' phase modulating ' ...
            regions{ar} ' amplitude']; ...
            [num2str(size(zscore{tr,pr,ar},2)) ...
            ' Interactions']});
          colormap(jet); colorbar;
          set(gcf,'WindowStyle','docked');
          saveas(gcf,[figurefolder 'crossOTCavez_'...
            num2str(ratT.ratnums(r)) phases{p} '_' ...
            join(string(data.trialinfo(tr,:)),'_') '_' regions{pr} 'PhaseMod' ...
            regions{ar} 'Amp_fullint.png'])
          
          %% plot mean OTCG
          figure(pr+(ar-1)*2+4); clf;
          subplot(1,5,1:3);
          imagesc([-cfg.winLen cfg.winLen], ...
            cfg.foi_mod, otcmean{tr,pr,ar});
          axis xy tight; hold on;
          plot([0 0],[cfg.foi_mod(1) cfg.foi_mod(end)],...
            'w','LineWidth',2);
          plot(-cfg.winLen:cfg.winLen-1,otcpeak{tr,pr,ar}...
            +fpeak{tr,pr,ar},'k');
          set(gca, 'FontName', 'Arial', 'FontSize', 12);
          xlabel('Time from Oscillatory Peak (ms)');
          ylabel('Modulated Frequency (Hz)');
          title({['Rat # ' num2str(ratT.ratnums(r)) ', ' ...
            char(phases(p)) ', ' join(string(data.trialinfo(tr,:)),'_')]; ...
            [regions{pr} ' phase modulating ' ...
            regions{ar} ' amplitude']; ...
            ['Average of  ' ...
            num2str(size(zscore{tr,pr,ar},2)) ...
            ' Interactions']});
          colormap(jet); colorbar;
          set(gcf,'WindowStyle','docked');
          
          %% plot modulation strength of mean OTCG
          subplot(1,5,4);
          plot(modSmean{tr,pr,ar},cfg.foi_mod,'k');
          axis xy tight
          xlabel('Modulation Strength');
          
          %% plot FFT of mean OTCG
          subplot(1,5,5);
          imagesc(cfg.f_fft(cfg.f_plot), cfg.foi_mod, ...
            2*abs(fftmean{tr,pr,ar}(:,cfg.f_plot)));
          axis xy tight; hold on;
          set(gca, 'FontName', 'Arial', 'FontSize', 12);
          xlabel('Modulating Frequency (Hz)');
          title({['Rat # ' num2str(ratT.ratnums(r)) ', ' ...
            char(phases(p)) ', ' join(string(data.trialinfo(tr,:)),'_')]; ...
            [regions{pr} ' phase modulating ' ...
            regions{ar} ' amplitude']; ...
            ['Average of  ' ...
            num2str(size(zscore{tr,pr,ar},2)) ...
            ' Interactions']});
          colormap(jet); colorbar;
          set(gcf,'WindowStyle','docked');
          saveas(gcf,[figurefolder 'crossOTCGave_'...
            num2str(ratT.ratnums(r)) phases{p} '_' ...
            join(string(data.trialinfo(tr,:)),'_') '_' regions{pr} 'PhaseMod' ...
            regions{ar} 'Amp_fullint.png'])
        end
      end
    end
  end
end
close(hx)
