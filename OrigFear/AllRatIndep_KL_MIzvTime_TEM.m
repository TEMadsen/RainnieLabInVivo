%% uses saved FearData to calculate MI over Time for all rats, all trials
% top level script, depends on:
%   KL_MIvTime_TEM
%       sig_KL_MI_TEM
%           KL_MI_TEM and
%           shuffle_envelope_signal_TEM

clearvars all;
close all;
load('S:/Teresa/PlexonData Backup/FearData_LongTrials.mat')
clearvars -except MIvT* ZvT* PvT* HvT* Data good le* phases ratnums ...
    trials Pf Af times movingwin areas control
clc;

%% set parameters

hx = waitbar(0,'Getting started...');
Pf = [1 5]; Af = [45 59];
% Pf = [2 6]; Af = [65 100];   
movingwin = [15 5];
trials = 5;
legentry = [num2str(Pf(1)) '-' num2str(Pf(2)) ' Hz mod ' ...
    num2str(Af(1)) '-' num2str(Af(2)) ' Hz - Long Trials - win ' ...
    num2str(movingwin(1)) ' step ' num2str(movingwin(2))];
MIvT_mPFC = cell(numel(phases),numel(ratnums),trials);
MIvT_BLA = cell(numel(phases),numel(ratnums),trials);
ZvT_mPFC = cell(numel(phases),numel(ratnums),trials);
ZvT_BLA = cell(numel(phases),numel(ratnums),trials);
PvT_mPFC = cell(numel(phases),numel(ratnums),trials);
PvT_BLA = cell(numel(phases),numel(ratnums),trials);
HvT_mPFC = cell(numel(phases),numel(ratnums),trials);
HvT_BLA = cell(numel(phases),numel(ratnums),trials);
PPvT_mPFC = cell(numel(phases),numel(ratnums),trials);
PPvT_BLA = cell(numel(phases),numel(ratnums),trials);

%% make Cross-Frequency Modulation vs. Time plots for each rat and trial

for p=1:numel(phases) %#ok<FORPF> will try later
%     if p==2;    movingwin=[26 32];    else      movingwin=[30 30];    end
    for r=1:numel(ratnums)
        if isempty(Data(p).LFP(r).mPFC)
            warning(['No data available for Rat # ' num2str(ratnums(r)) ...
                ', ' char(phases(p))]);
        else
            for t=1:trials
                waitbar((t+((r-1)+(p-1)*trials)*numel(ratnums))...
                    /(numel(phases)*numel(ratnums)*trials),hx,...
                    ['Working on Rat # ' num2str(ratnums(r)) ...
                    ', ' char(phases(p)) ', Trial # ' num2str(t)]);
                [MIvT_mPFC{p,r,t},~,ZvT_mPFC{p,r,t},PvT_mPFC{p,r,t},...
                    HvT_mPFC{p,r,t},PPvT_mPFC{p,r,t}]=KL_MIvTime_TEM(...
                    Data(p).LFP(r).mPFC(:,t)',movingwin,Pf,Af,'n');
                [MIvT_BLA{p,r,t},times,ZvT_BLA{p,r,t},PvT_BLA{p,r,t},...
                    HvT_BLA{p,r,t},PPvT_BLA{p,r,t}]=KL_MIvTime_TEM(...
                    Data(p).LFP(r).BLA(:,t)',movingwin,Pf,Af,'n'); %#ok<SNASGU> saved to file
            end
        end
    end
end
close(hx)

%% good

good = false(numel(phases),numel(ratnums),trials);

for p = 1:numel(phases)
    for r = 1:numel(ratnums)
        good(p,r,:) = Data(p).LFP(r).goodtrials;
    end
end

%% save results

save(['S:\Teresa\PlexonData Backup\Fear_KLMIvTimewSig_' legentry '.mat'],...
    'MIvT*','ZvT*','P*','HvT*','Pf','Af','times','movingwin','phases',...
    'ratnums','legentry','good');
