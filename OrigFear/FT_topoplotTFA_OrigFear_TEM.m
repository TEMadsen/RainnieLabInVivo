%% Plots spectrograms in a topographic arrangement

CleanSlate  % provides a clean slate to start working with new data/scripts
% by clearing all variables, figures, and the command window

info_origfear            % load all background info & final parameters

errlog = cell(size(ratT,1), 1);   % MEs saved w/in nested cells:
% rat #, then tone x phase, then ch #

%% which spectrograms to plot

spectype      = 'mtmfft';   % w/ overlapping trials as moving windows
winlen        = '2s';       % movwin length as string, if applicable, '' if not
normtype.pow  = 'ZdB';      % pseudo-z-score of dB transformed spectrograms
normtype.coh  = 'Zraw';     % pseudo-z-score of coherograms (no transform)
redo      = false;  % to ignore & overwrite pre-existing output files
skiprats      = [];         % already processed or running on another computer

errlog  = cell(size(ratT,1), sum(expected));   % MEs saved in rat-trl cells
rattime = NaN(size(ratT,1), 1);   % will save computation time per rat

%% full loop

for r = setdiff(find(~ratT.excluded)',skiprats)
  ratfolder = [datafolder num2str(ratT.ratnums(r)) filesep];
  
  cfg = [];
  cfg.layout    = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  cfg.zlim      = 'maxabs';
  cfg.colormap  = jet;
          
  %% pre-select interesting examples
  
  ex(1).p = 1;  ex(1).tn = 10;  % habituation
  ex(2).p = 1;  ex(2).tn = 17;  % acquisition
  ex(3).p = 2;  ex(3).tn = 1;   % recall
  ex(4).p = 3;  ex(4).tn = 15;  % extinction
  
  %% loop through, plotting spectrograms
  
  for x = 1:numel(ex)
      %% define trial inputfile
      
      p = ex(x).p; tn = ex(x).tn;
      
      inputfile = [ratfolder normtype 'Norm_' spectype 'TFA_' ...
        stimuli{1} num2str(tn) 'of' phases{p} '_' ...
        num2str(ratT.ratnums(r)) 'allchs.mat'];
      
      %% check if it exists
      
      if ~exist(inputfile,'file')
        errlog{r}{tn,p}.warning = ['Skipping ' stimuli{1} ' ' ...
          num2str(tn) ' of ' phases{p} ' for rat #' ...
          num2str(ratT.ratnums(r)) ' because inputfile ' ...
          inputfile(numel(ratfolder)+1:end) ' does not exist.'];
        warning(errlog{r}{tn,p}.warning)
        continue  % to next example
      end
      
      %% load data
      
      load(inputfile)
      
      %% multiplot (full spectrograms)
      
      figure(10*r+x); clf;
%       subplot(1,3,1); 
      cfg.xlim = [-30 60]; cfg.ylim = [0 120];
      ft_multiplotTFR(cfg,freq)
      
%       % topoplot (delta power during/after tones)
%       
%       subplot(1,3,2);   cfg.xlim = [0 60];  cfg.ylim = [0 6];
%       ft_topoplotTFR(cfg,freq)
%       
%       % topoplot (mid-gamma power during/after tones)
%       
%       subplot(1,3,3);   cfg.xlim = [0 60];  cfg.ylim = [45 60];
%       ft_topoplotTFR(cfg,freq)
      
%% save figure

print([figurefolder normtype 'Norm_' spectype 'TFA_' ...
        stimuli{1} num2str(tn) 'of' phases{p} '_' ...
        num2str(ratT.ratnums(r)) 'topoSpec'],'-dpng');
      
  end % example trial
end % rat

%% save errlog

disp('Analysis complete!  Saving errlog.')
save([datafolder 'errlog' datestr(now,29) ...
  '_FT_topoplotTFA_OrigFear_TEM'], 'errlog')
