function [ output ] = doGrp_RidgeVbehav_OrigFear_TEM( spectype, wMult )
% DOGRP_RIDGEVBEHAV_ORIGFEAR_TEM will correlate tfRidge data with behavior
%   across all rats
%
% NEEDS CONVERSION FROM INDIVIDUAL RAT STATS
%
% written 2/13/18 by Teresa E. Madsen, Ph.D.

%% import global metadata structure

global exptMeta

if isempty(exptMeta);   info_origfear;   end

%% check inputs & assign defaults

if nargin == 0  % like when I just hit "Run and Time" in the Editor tab
  spectype  = 'piWav';  % like FT default wav params, but using pi for integer # of cycles, etc.
  wMult     = 1.5;      % tfRidge bandwidth multiplier
elseif nargin == 1 && iscell(spectype)  % as input from fullFTpipeline
  wMult = spectype{2};
  spectype = spectype{1};
end

%% create waitbar

wb = waitbar(0, 'Preparing to run group stats for RidgeVbehav');

%% load operant count BDA tones and convert to BSR

BSRfile = [exptMeta.datafolder 'preprocBehav' filesep 'AllRatsBSR.mat'];

if existfile_TEM(BSRfile)
  load(BSRfile);   % BSR
else
  load(strrep(BSRfile,'BSR.mat','BDAtones.mat'));   % cntBDAtones
  
  BSR = cellfun(@(x) (x(:,1) - x(:,2)) ./ (x(:,1) + x(:,2)), ...
    cntBDAtones(~exptMeta.ratT.excluded), 'UniformOutput',false); %#ok<USENS> loaded from file
  BSR = [BSR{:}]';
  % if count is NaN rather than 0, don't change BSR from NaN to 1
  omit = cellfun(@(x) any(isnan(x),2), cntBDAtones(~exptMeta.ratT.excluded), ...
    'UniformOutput',false);
  omit = [omit{:}]';
  % if no operant behavior before or during tone, consider it complete suppression
  BSR(isnan(BSR) & ~omit) = 1;
  % if operant behavior increases in response to tone, consider it no suppression
  BSR(BSR < 0) = 0;
  
  save(BSRfile,'BSR')
end

%% choose best channels based on individual rat stats

oldFolder = cd;
for r = find(~exptMeta.ratT.excluded)'
  listing = dir([exptMeta.datafolder 'RidgeVBehav' filesep ...
    num2str(exptMeta.ratT.ratnums(r)) filesep '*stat_indepsamplesregrT*.mat']);
  if isempty(listing)
    warning(['No matching files found for ' num2str(exptMeta.ratT.ratnums(r))]);
    continue  % to next rat
  end
  cd(listing(1).folder)   % to avoid full path in dataname
  parsedfilenames = split(string({listing.name}'),{'_tfRidge_','_vs_'});
  metrics = unique(parsedfilenames(:,2));
  for m = 1:numel(metrics)
    % aggregate results for this metric
    cfg = [];
    cfg.inputfile = {};
%     cfg.dataname = {};  % ignored by ft_multiplotER at line 238
    for p = 1:numel(exptMeta.phases)-1  % this ensures correct order
      matchingfile = find(contains({listing.name},metrics(m)) & ...
        contains({listing.name},exptMeta.abbr(p)));
      if numel(matchingfile) == 1
        cfg.inputfile = [cfg.inputfile listing(matchingfile).name];
%         cfg.dataname = [cfg.dataname exptMeta.abbr(p)];
      else
        warning([num2str(numel(matchingfile)) ' files contain both ' ...
          metrics(m) ' and ' exptMeta.abbr(p)])
        keyboard  % verify before continuing
      end
    end   % for each phase
    % plot stats for this metric
    cfg.layout          = [exptMeta.configfolder 'layout_' ...
      exptMeta.ratT.MWA{r} '_' exptMeta.ratT.side{r} '.mat'];
    cfg.parameter       = 'stat';
    cfg.maskparameter   = 'mask';   % only taken from 1st input >:(
    cfg.maskstyle       = 'thickness';
    cfg.showlabels      = 'yes';
    cfg.showoutline     = 'yes';  % necessary to view comment when only 1 array is present
    cfg.showscale       = 'no';   % this actually refers to an extra spectrogram that appears to be an average
    cfg.showcomment     = 'yes';
    cfg.comment         = [num2str(exptMeta.ratT.ratnums(r)) ' ' metrics{m} ...
      newline 'Previously selected channels:' newline ...
      exptMeta.regions{1} ' = ' exptMeta.ratT.(exptMeta.regions{1}){r} ', ' ...
      exptMeta.regions{2} ' = ' exptMeta.ratT.(exptMeta.regions{2}){r}];
    cfg.zlim            = 'maxabs';   % [-3 3];
    cfg.colormap        = jet;
    
    fig = figure(m); clf; fig.WindowStyle = 'docked';
    ft_multiplotER(cfg)   % might be easier to revise plotFTmatrix to work with 
    % this kind of data than to deal with all the bugs in this...  :(
    
    %       cfg.fticks = exp(1).^(0:1:5);
    %       cfg.tticks = -30:30:60;
    %       cfg.cticks = -4:2:4;
    %       [cfg] = plotFTmatrix_TEM(cfg)
  end   % for each metric (each gets its own figure)
  keyboard  % verify selected channels
  close all   % some rats lack 3rd phase
  % 200 - consider 15 for sig in both FC & Ext (none in Rcl)
  % 202 - consider 5 for only during tone & only delta sig (during FC)
  % 203 - no significance, no Rcl (because no behavioral variation - BSR == 1)
  % 204 - consider 12 to avoid reverse correlation with delta
  % 205 - consider 7 & 11 for greater FC significance with similar patterns to
  % previously selected channels
  % 210 - consider 4 & 11 for greater FC & Rcl sig
  % 212 - consider 2 for during FC tone sig
  % 338 - consider 7 because 4 looks weird on Rcl, similar sig
  % 342 - consider 7 & 14 for + corr. w/ gamma after FC tone/shocks, no Rcl (due
  % to lack of behavioral variation) or Ext (not recorded)
  % 339, 341, 343 - no significance anywhere
end   % for each rat
cd(oldFolder)   % return to git repo

%% loop through phases
% run each all the way through stat plots, then clear memory to avoid overload

comptime  = nan(numel(exptMeta.phases)-1,3);  % 3 steps: prep data, stats, & plot
output = cell(3,1);   % will be skew & kurtosis before & after dB conversion

for p = 1:numel(exptMeta.phases)-1  % all but baseline
  st = tic;
  waitbar((p-1)/(numel(exptMeta.phases)-1), wb, ['Preparing ' ...
    exptMeta.abbr{p} ' ' spectype ' ' ...
    num2str(wMult) 'x tfRidges for regression with behavioral measures']);
  
  merged_dB_file = [exptMeta.datafolder 'tfRidge' filesep ...
    num2str(exptMeta.ratT.ratnums(r)) filesep exptMeta.abbr{p} '_raw_' ...
    num2str(exptMeta.ratT.ratnums(r)) 'TFAdBpow_' spectype '_' ...
    strrep(num2str(wMult),'.','_') 'x_tfRidge.mat'];
  
  if existfile_TEM(merged_dB_file)
    load(merged_dB_file)  % timelock
  else
    %% loop through trials, prepping data
    
    timelock  = cell(exptMeta.expected(p),1);   % 15-17 tones per phase
    
    for tn = 1:exptMeta.expected(p)   % trial # within phase
      %% define trial's neural data inputfile
      
      inputfile = [exptMeta.datafolder 'tfRidge' filesep num2str(exptMeta.ratT.ratnums(r)) ...
        filesep exptMeta.stimuli{1} num2str(tn) 'of' exptMeta.abbr{p} '_raw_' ...
        num2str(exptMeta.ratT.ratnums(r)) 'TFApow_' spectype '_'...
        strrep(num2str(wMult),'.','_') 'x_tfRidge.mat'];
      
      %% load tfRidge inputfile, if it exists
      
      if ~existfile_TEM(inputfile)
        warning(['Inputfile not found:  ' inputfile(numel(exptMeta.datafolder):end)])
        listing = dir([exptMeta.datafolder 'tfRidge' filesep ...
          num2str(exptMeta.ratT.ratnums(r)) filesep exptMeta.stimuli{1} ...
          num2str(tn) 'of*' num2str(exptMeta.ratT.ratnums(r)) '*' spectype '*' ...
          strrep(num2str(wMult),'.','_') 'x_tfRidge.mat']);
        if isempty(listing)
          warning('skipping trial')
          continue  % to next tone
        else  % check for both abbreviation & full phase name
          fileNdx = contains({listing.name},[exptMeta.abbr(p), exptMeta.phases(p)]);
          switch sum(fileNdx)
            case 0
              warning('skipping trial')
              continue  % to next tone
            case 1
              warning(['using this file instead: ' listing(fileNdx).name])
              inputfile = [listing(fileNdx).folder listing(fileNdx).name];
            otherwise
              warning('more than 1 filename matches the requested data')
              disp(join([{listing(fileNdx).name}' {listing(fileNdx).date}' ...
                cellstr(num2str([listing(fileNdx).bytes]'))], ...
                {', date: ',', bytes: '}))
              keyboard  % figure out the problem & select the best file
          end
        end
      end
      
      timelock{tn} = rmvlargefields_TEM(inputfile);
      
      %% correct some fields that have changed in powridges since these were calculated
      
      if isfield(timelock{tn},'freq')
        timelock{tn}.cfg.freq = timelock{tn}.freq;  % preserves freq in spite of FT's inability to detect its dimord
        timelock{tn} = rmfield(timelock{tn},'freq'); 	% allows FT to recognize as timelock instead of freq
      end
      if isfield(timelock{tn},'w')
        timelock{tn}.cfg.w = timelock{tn}.w;  % preserves w in spite of FT's inability to detect its dimord
        timelock{tn} = rmfield(timelock{tn},'w');
      end
      
      switch timelock{tn}.dimord
        case 'rpt_chan_time'
          for tr = 1:size(timelock{tn}.trialinfo,1)
            for ch = 1:numel(timelock{tn}.label)
              for t = 1:numel(timelock{tn}.time)
                if timelock{tn}.rFreq(tr,ch,t) + ...
                    timelock{tn}.cfg.w(timelock{tn}.cfg.freq == timelock{tn}.rFreq(tr,ch,t)) > 16
                  timelock{tn}.rplow(tr,ch,t) = NaN;
                end
              end
            end
          end
        case 'chan_time'
          for ch = 1:numel(timelock{tn}.label)
            for t = 1:numel(timelock{tn}.time)
              if timelock{tn}.rFreq(ch,t) + ...
                  timelock{tn}.cfg.w(timelock{tn}.cfg.freq == timelock{tn}.rFreq(ch,t)) > 16
                timelock{tn}.rplow(ch,t) = NaN;
              end
            end
          end
        otherwise
          error('unexpected dimord')
      end   % switch dimord
      
      %% verify timelock structure
      
      timelock{tn} = ft_datatype_timelock(timelock{tn});
      
    end   % for each trial
    
    %% make sure all trials' data were prepared
    
    omit = cellfun(@isempty,timelock);
    if sum(omit) > 1 || sum(~omit) < 15   % only expected omissions are in acquisition
      warning([exptMeta.abbr{p} ' only has ' num2str(sum(~omit)) ' trials'])
      if sum(~omit) == 0
        warning(['skipping ' exptMeta.abbr{p} ' for rat # ' ...
          num2str(exptMeta.ratT.ratnums(r))])
        continue  % to next block
      end
      keyboard  % figure out why, skip if < 3 good trials
    end
    
    %% merge trials
    
    timelock = ft_appendtimelock([],timelock{~omit});
    
    %% identify parameters of interest (those that match dimord)
    
    [ fields, fldoi, ~, ~, dimord ] = datafields_TEM( timelock );
    fields = fields(fldoi);
    tdim = find(ismember(dimord,'time'));
    
    %% check skew & kurtosis before dB transform
    
    skewpre = struct;  skewpost = struct;  kurtpre = struct;  kurtpost = struct;
    for param = 1:numel(fields)
      skewpre.(fields{param}) = skewness(timelock.(fields{param}),[],tdim);
      kurtpre.(fields{param}) = kurtosis(timelock.(fields{param}),[],tdim);
    end
    
    %% use FT to perform dB transform so there's a record in the previous cfgs
    % faster to do this in two steps, otherwise it runs it one element at a time
    
    cfg1 = [];
    cfg1.parameter  = fields(~ismember(fields,'rFreq'));
    cfg1.operation  = 'log10';
    
    cfg2 = cfg1;
    cfg2.operation  = 'multiply';
    cfg2.scalar     = 10;
    
    timelock_dB = ft_math(cfg2,ft_math(cfg1,timelock));
    timelock_dB.rFreq = timelock.rFreq;   % no dB conversion, so it isn't copied automatically
    timelock = timelock_dB;   % no need to have so many copies of the data
    clearvars timelock_dB
    
    %% compare skew & kurtosis after vs. before dB transform & plot if worse
    
    for param = 1:numel(fields)
      skewpost.(fields{param}) = skewness(timelock.(fields{param}),[],tdim);
      kurtpost.(fields{param}) = kurtosis(timelock.(fields{param}),[],tdim);
      change = abs(skewpost.(fields{param})) - abs(skewpre.(fields{param}));
      if any(any(change > 0 & ...   % increased magnitude of skew
          abs(skewpost.(fields{param})) > 2))   % resulting in a value far from normal
        warning('dB transform made skew *worse*')
        figure('Name', [num2str(exptMeta.ratT.ratnums(r)) ...
          exptMeta.abbr{p} num2str(tn) fields{param} '_skew'])
        [~,~,ic] = unique(change);
        scatter(skewpre.(fields{param})(:), skewpost.(fields{param})(:), ...
          36.*(numel(change)+2-ic), ...   % bigger change, larger circle
          (1:numel(change))');  % channels color coded from blue(AD01) to red(AD16)
        colormap(jet);  hold all;
        plot([0 max(abs([skewpre.(fields{param})(:); skewpost.(fields{param})(:)]))], ...
          [0 -max(abs([skewpre.(fields{param})(:); skewpost.(fields{param})(:)]))])
        xlabel('skew before');  ylabel('skew after dB conversion');
        title([strjoin(timelock.label(any(change > 0.05 & ...   % increased magnitude of skew, resulting in a value far from normal)
          abs(skewpost.(fields{param})) > 0.5, 2))) ' got worse'])
        % keyboard
      end
      change = kurtpost.(fields{param}) - kurtpre.(fields{param});
      if any(any(change > 0 & ...  % increased kurtosis
          kurtpost.(fields{param}) > 5))  % resultant value much greater than normal distribution (3)
        warning('dB transform made kurt *worse*')
        figure('Name', [num2str(exptMeta.ratT.ratnums(r)) ...
          exptMeta.abbr{p} num2str(tn) fields{param} '_kurt'])
        [~,~,ic] = unique(change);
        scatter(kurtpre.(fields{param})(:), kurtpost.(fields{param})(:), ...
          3.6 .* (numel(change)+2-ic), ...   % bigger change, larger circle
          (1:numel(change))');  % channels color coded from blue(AD01) to red(AD16)
        colormap(jet);  hold all;
        plot([min([kurtpre.(fields{param})(:); kurtpost.(fields{param})(:)]) ...  % X(1)
          max([kurtpre.(fields{param})(:); kurtpost.(fields{param})(:)])], ...    % X(2)
          [min([kurtpre.(fields{param})(:); kurtpost.(fields{param})(:)]) ...     % Y(1)
          max([kurtpre.(fields{param})(:); kurtpost.(fields{param})(:)])], ...    % Y(2) -> no change from pre to post
          [min([kurtpre.(fields{param})(:); kurtpost.(fields{param})(:)]) ...     % X(1)
          max([kurtpre.(fields{param})(:); kurtpost.(fields{param})(:)])], [3 3], ...   % X(2), Y ->
          [3 3], [min([kurtpre.(fields{param})(:); kurtpost.(fields{param})(:)]) ...    % X, Y(1)
          max([kurtpre.(fields{param})(:); kurtpost.(fields{param})(:)])])   % Y(2) -> expected values for normal distribution
        xlabel('kurtosis before');  ylabel('kurtosis after dB conversion');
        title([strjoin(timelock.label(any(change > 0.05 & ...
          kurtpost.(fields{param}) > 5))) ' got worse'])
        % keyboard
      end   % if kurtosis increased beyond that of a normal distribution
    end   % for each parameter
    
    output{p} = struct('kurtpre',kurtpre, 'kurtpost',kurtpost, ...
      'skewpre',skewpre, 'skewpost',skewpost);
    
    %% save merged, dB-transformed file
    
    disp(['saving timelock to ' merged_dB_file])
    save(merged_dB_file,'timelock')
    
  end
  
  %% identify parameters of interest (those that match dimord)
  
  [ fields, fldoi ] = datafields_TEM( timelock );
  fields = fields(fldoi);
  
  %% downsample timelock to average +/-std of each measure in 30s BDA windows
  
  timelock_DS = rmfield(timelock,fields);   % copy all except fields of interest
  timelock_DS.time = [-15 15 45]; 	% before/during/after
  %   trlclrs = jet(size(timelock.trialinfo,1));
  for param = 1:numel(fields)
    timelock_DS.([fields{param} '_mean']) = nan(size(timelock.trialinfo,1),...
      numel(timelock.label),numel(timelock_DS.time));
    timelock_DS.([fields{param} '_std']) = timelock_DS.([fields{param} '_mean']);
    for tp = 1:numel(timelock_DS.time)
      timelock_DS.([fields{param} '_mean'])(:,:,tp) = ...
        mean(timelock.(fields{param})(:,:, ...
        abs(timelock.time - timelock_DS.time(tp)) < 15), 3, 'omitnan');
      timelock_DS.([fields{param} '_std'])(:,:,tp) = ...
        std(timelock.(fields{param})(:,:, ...
        abs(timelock.time - timelock_DS.time(tp)) < 15), 0, 3, 'omitnan');
    end
    %     fig = figure('Name', [num2str(exptMeta.ratT.ratnums(r)) ...
    %       exptMeta.abbr{p} ' ' replace(fields{param},'_',' ')]);
    %     clf;  fig.WindowStyle = 'docked'; sp = 1;
    %     for ch = 1:numel(timelock.label)
    %       if ismember(timelock.label{ch}, ...
    %           [exptMeta.ratT.mPFC(r) exptMeta.ratT.BLA(r)])
    %         subplot(2,1,sp);
    %         eh = errorbar(squeeze(timelock_DS.rFreq_mean(:,ch,:))', ...
    %           squeeze(timelock_DS.rFreq_std(:,ch,:))');
    %         for tr = 1:numel(eh)
    %           eh(tr).Color = trlclrs(tr,:);
    %         end
    %         axis tight
    %         sp = sp + 1;  % next subplot
    %       end
    %     end
  end
  comptime(p,1) = toc(st);
  
  %% reduce to subset of trials
  
  st = tic;
  
  cfg = [];
  cfg.trials = find(timelock_DS.trialinfo(:,1) < exptMeta.expected(p));   % not the last trial, as it won't have a next-trial-BSR
  cfg.trials(isnan(BSR(r-sum(exptMeta.ratT.excluded(1:r-1)), ...
    sum(exptMeta.expected(1:p-1)) + cfg.trials + 1))) = [];   % not any trial where next BSR is missing
  cfg.trials(any(any(isnan(timelock_DS.rFreq_mean(cfg.trials,:,:)),3),2)) = [];   % not any trial containing NaNs for ridge freq
  if numel(cfg.trials) > 14  % not more than 14 trials
    cfg.trials = cfg.trials(end-13:end);
  end
  
  timelock = ft_selectdata(cfg,timelock_DS);
  
  %% define parameters of stat test & name outputfile
  
  cfg = [];
  cfg.statistic         = 'ft_statfun_indepsamplesregrT';
  cfg.method            = 'montecarlo';
  cfg.correctm          = 'max';
  cfg.tail              = 0;
  cfg.alpha             = 0.025;
  cfg.numrandomization  = 1000;
  
  % regress tfRidge metrics for each tone with BSR to the *NEXT* tone
  cfg.design  = BSR(r-sum(exptMeta.ratT.excluded(1:r-1)), ...
    sum(exptMeta.expected(1:p-1)) + timelock.trialinfo(:,1) + 1);
  if numel(unique(cfg.design)) < 3
    warning(['skipping ' exptMeta.abbr{p} ...
      ' because of < 3 unique behavioral values'])
    continue  % to next block
  end
  
  cfg.ivar    = 1;  % uvar = ivar (between-UO)
  
  %% loop through each field, running stats separately
  
  [fields,fldio] = datafields_TEM(timelock);
  fields = fields(fldio);
  stat = cell(size(fields));
  
  for param = 1:numel(fields)
    cfg.parameter = fields{param};
    cfg.outputfile  = insertBefore(replace(merged_dB_file, ...
      [filesep 'tfRidge' filesep], [filesep 'RidgeVBehav' filesep]), '.mat', ...
      ['_' fields{param} '_vs_NextTrlBSR_stat' erase(cfg.statistic,'ft_statfun')]);
    if isfield(cfg,'clusterthreshold') && ...
        strcmp(cfg.clusterthreshold,'nonparametric_individual')
      cfg.outputfile = insertBefore(cfg.outputfile,'.mat','_NPindiv');
    end
    if isfield(cfg,'orderedstats') && istrue(cfg.orderedstats)
      cfg.outputfile = insertBefore(cfg.outputfile,'.mat','_ordered');
    end
    
    %% run stat test
    
    if existfile_TEM(cfg.outputfile)
      stat{param} = rmvlargefields_TEM(cfg.outputfile);
    else
      waitbar((p-2/3)/(numel(exptMeta.phases)-1), wb, ...
        ['Running ' erase(cfg.statistic,'ft_statfun_') ' on ' exptMeta.abbr{p} ...
        ' ' spectype ' vs. next trial BSR for rat #' num2str(exptMeta.ratT.ratnums(r))]);
      
      stat{param} = ft_timelockstatistics(cfg, timelock);
    end
  end
  comptime(p,2) = toc(st);  % record time only if stats were calculated, not just loaded
  
  %% print & plot (top 10/nearly) significant correlations of tfRidge metrics & next trial BSR
  
  st = tic;
  waitbar((p-1/3)/(numel(exptMeta.phases)-1), wb, ...
    ['visualizing results of ' erase(cfg.statistic,'ft_statfun_') ' on ' exptMeta.abbr{p} ...
    ' ' spectype ' vs. next trial BSR for rat #' num2str(exptMeta.ratT.ratnums(r))]);
  
  for param = 1:numel(stat)
    
    disp([newline 'Found chan/time bins with significant positive (' ...
      num2str(sum(stat{param}.mask(:) & stat{param}.stat(:) > 0)) ') and negative (' ...
      num2str(sum(stat{param}.mask(:) & stat{param}.stat(:) < 0)) ')' newline ...
      'correlations between ' replace(fields{param},'_',' ') ' & next trial BSR within ' exptMeta.abbr{p} newline])
    
    cfg =[];
    cfg.layout = [exptMeta.configfolder 'layout_' exptMeta.ratT.MWA{r} '_' ...
      exptMeta.ratT.side{r} '.mat'];
    cfg.parameter       = 'stat';
    cfg.maskparameter   = 'mask';
    cfg.zlim            = 'maxabs';
    cfg.gridscale       = 2*numel(stat{param}.label) + 1;
    cfg.colormap        = jet;
    
    xfit = linspace(min(stat{param}.cfg.design),max(stat{param}.cfg.design),15);
    
    %% visualize significant correlations
    
    if any(stat{param}.mask)
      sigT = table(find(stat{param}.mask),'VariableNames',{'I'});
    else % if no significant correlations, just find strongest/closest to significance
      sigT = table(find(stat{param}.prob == min(stat{param}.prob(:)) | ...  % find lowest probability of null hypothesis
        stat{param}.stat == min(stat{param}.stat(:)) | ...  % or strongest negative correlation
        stat{param}.stat == max(stat{param}.stat(:))), 'VariableNames',{'I'});  % or strongest positive correlation
    end
    [sigT.stat,sortOrd] = sort(stat{param}.stat(sigT.I));  % sort in stat order so colors will reflect red(+) blue(-) correlations
    sigT.I = sigT.I(sortOrd);
    sigT.prob = stat{param}.prob(sigT.I);
    if size(sigT,1) > 30  % only plot the strongest correlations
      lastneg = find(sigT.stat < 0,1,'last');
      if isempty(lastneg)
        lastneg = 1;
      elseif lastneg > 15
        lastneg = 15;
      end
      firstpos = find(sigT.stat > 0,1,'first');
      if isempty(firstpos)
        firstpos = size(sigT,1);
      elseif firstpos < size(sigT,1)-14
        firstpos = size(sigT,1)-14;
      end
      sigT = sigT([1:lastneg, firstpos:end],:);
    end
    
    [sigT.ch,sigT.t] = ind2sub(size(stat{param}.mask),sigT.I);
    sigT.chlabel = stat{param}.label(sigT.ch);
    if isequal(stat{param}.time(:),exptMeta.tCenter(:))
      sigT.tlabel = exptMeta.intervals(sigT.t);
    else
      sigT.tlabel = strip(cellstr(num2str(stat{param}.time(sigT.t)',3)));
    end
    sigT.valpertrl = nan(numel(sigT.I),size(timelock.trialinfo,1));   % aggregate metric value over trials
    for tr = 1:size(timelock.trialinfo,1)
      sigT.valpertrl(:,tr) = timelock.(fields{param})(tr,sigT.I);
    end
    CC = rowfun(@(x) corrcoef(stat{param}.cfg.design, x), sigT, ...
      'InputVariables','valpertrl', 'OutputFormat','cell');
    sigT.CC = cellfun(@(x) x(2), CC);
    sigT = sortrows(sigT,'CC','descend');
    sigT.color = mat2cell(flipud(jet(size(sigT,1))),ones(size(sigT,1),1));
    fitT = rowfun(@(x) polyfit(stat{param}.cfg.design, x, 2), sigT, ...
      'InputVariables','valpertrl', 'OutputVariableNames',{'pc','ErrorEst'}); % fit square model
    sigT.pop_fit = cell2mat(rowfun(@(x1,x2) polyval(x1, xfit, x2), fitT, ...
      'InputVariables',{'pc','ErrorEst'}, 'OutputFormat','cell')); % evaluate model at various X values
    
    fig = figure('Name',[exptMeta.abbr{p} ' ' replace(fields{param},'_',' ') ' Correlations(s)']);
    clf; fig.WindowStyle = 'docked'; fig.PaperPositionMode = 'auto';
    % plot next trl BSR vs. val for each chan/freq/time bin
    h = plot(stat{param}.cfg.design(:),sigT.valpertrl','o:', ...
      xfit(:),sigT.pop_fit','-');
    [h(1:size(sigT,1)).Color] = sigT.color{:};
    [h(size(sigT,1)+1:end).Color] = sigT.color{:};
    axis tight;
    xlabel('BSR in response to *NEXT* Tone');
    ylabel(replace(fields{param},'_',' '));
    legend(join([sigT.chlabel, sigT.tlabel, ...
      strip(cellstr(num2str(sigT.CC,3))), strip(cellstr(num2str(sigT.prob,3)))], ...
      {', ',' Tones, coeff = ',', p = '}), 'Location','NorthEastOutside')
    
    title(['Strongest correlations between ' replace(fields{param},'_',' ') ...
      ' & next trial BSR within ' exptMeta.abbr{p}])
    
    print([exptMeta.figurefolder 'StrongCorr_' ...
      stat{param}.cfg.outputfile(find(stat{param}.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
      '-dpng','-r0')
    %     close(fig)
    
    %% plot significant stat{param}
    
    for tp = 1:numel(stat{param}.time)
      if any(stat{param}.mask(:,tp))
        
        fig = figure('Name',[exptMeta.abbr{p} ' ' replace(fields{param},'_',' ') ...
          ' ' exptMeta.intervals{tp} ' Tones']);
        clf; fig.WindowStyle = 'docked';
        
        cfg.xlim  = [15 15];  % interpreted using nearest, so [0 30] becomes [-15 15]
        
        ft_topoplotER(cfg,stat{param});
        
        fig.PaperPositionMode = 'auto';
        print([exptMeta.figurefolder 'TopoCorr_' exptMeta.intervals{tp} '_' stat{param}.cfg.outputfile(...
          find(stat{param}.cfg.outputfile == filesep,1,'last')+1:end-4)], '-dpng','-r0')
        %   close(fig)
      end   % if any channel is significantly correlated
    end   % for each time period
  end   % for each param
  
  comptime(p,3) = toc(st);
end   % for each block

%% clean up

% output = comptime;
close(wb);

end   % function
