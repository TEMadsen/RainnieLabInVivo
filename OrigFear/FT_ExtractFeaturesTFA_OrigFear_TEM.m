%% extract features from spectrograms & coherograms for stats, correlation
% with behavior, and/or tracking over time
% took 14 hours (w/ redo, plt, & dostats all false) on 3/28/17
% 20 hours w/ java "exceptions" but no errors for winlen = 2s on 3/30/17

CleanSlate  % provides a clean slate to start working with new data/scripts

wb = waitbar(0,'Initializing...');   % will display progress through loop

info_origfear   % load all background info & final parameters

spectype      = 'mtmfft';   % w/ overlapping trials as moving windows
winlen        = 2;          % movwin(1) = length of moving windows
redo          = true;       % to ignore & overwrite old files
plt           = false;      % to show scatterplots of extracted features
dostats       = true;       % rough group stats for each channel
display       = 'off';      % on or off for visualization of posthoc results
skiprats      = [];         % already processed or running on another computer

errlog  = cell(size(ratT,1), sum(expected));   % MEs saved in rat-trl cells
rattime = NaN(size(ratT,1), 1);   % will save computation time per rat

outputagg     = [datafolder 'powridges' num2str(winlen) 's_agg.mat'];
outputstats   = [datafolder 'powridges' num2str(winlen) 's_stats.mat'];

metrics = {'fridge','rpow','rprom','rtun'};
ngrp  = (numel(blknames)-1)*3;  % # of groups (reltimes w/in blocks of trials)
ncomp = ngrp*(ngrp-1)/2;        % # of comparisons between groups (all possible)

%% preallocate aggregate variables

if exist(outputagg,'file') && ~redo
  load(outputagg)
  done = struct('rat',unique(ridgeT.rat), ...
    'trial',unique(ridgeT.trial), 'channel',unique(ridgeT.channel,'rows'), ...
    'count',crosstab(ridgeT.rat,ridgeT.trial,ridgeT.channel));
  [eRt,eTr,eCh] = ind2sub(size(done.count), find(done.count == 0));
  incRt = unique(eRt);  % incomplete rats
  % FIXME: write something that checks if it's all done & add that rat to
  % skiprats
else
  ridgeT = table([], [], [], [], [], [], [], [], 'VariableNames', ...
    [{'rat','channel','trial','time'} metrics]);
end

if exist(outputstats,'file') && ~redo
  load(outputstats)
  % FIXME: write something that checks if it's all done & add that rat to
  % skiprats
else
  posthoc = struct;
  for v = 1:numel(metrics)
    posthoc.(metrics{v}) = struct('pvals', NaN(ncomp,numel(allchs),size(ratT,1)), ...
      'means', NaN(ngrp,numel(allchs),size(ratT,1)));
    posthoc.(metrics{v}).anovaT = cell(numel(allchs), size(ratT,1));
  end
end

%% loop through all rats

for r = setdiff(find(~ratT.excluded)',skiprats)
  tic   % will time computations per rat
  
  waitbar((r-1)/size(ratT,1), wb, ...
    ['processing baseline for rat #' num2str(ratT.ratnums(r))])
  
  %% identify & quantify time-frequency ridges in baseline data
  
  cfg = [];
  cfg.inputfile = [datafolder num2str(ratT.ratnums(r)) filesep 'Baseline' ...
    num2str(ratT.ratnums(r)) spectype 'TFApow' num2str(winlen) 's.mat'];
  cfg.outputfile = [datafolder num2str(ratT.ratnums(r)) filesep 'Baseline' ...
    num2str(ratT.ratnums(r)) spectype 'TFApow' num2str(winlen) 's_powridges.mat'];
  
  if exist(cfg.outputfile,'file') && ~redo
    errlog{r,sum(expected)} = ['tfridges already extracted from Rat #' ...
      num2str(ratT.ratnums(r)) '''s Baseline. Skipping.'];
    warning(errlog{r,sum(expected)})
  else
    %% extract powridges
    
    stat = powridges_TEM(cfg);
    
    %% arrange data in a table, remove missing values, and append to aggregate
    
    for ch = 1:numel(stat.label)
      chT = table(repmat(ratT.ratnums(r),numel(stat.time),1), ...
        repmat(stat.label{ch},numel(stat.time),1), ...
        repmat(sum(expected),numel(stat.time),1), stat.time', ...
        squeeze(stat.fridge(1,ch,:)), squeeze(stat.rpow(1,ch,:)), ...
        squeeze(stat.rprom(1,ch,:)), squeeze(stat.rtun(1,ch,:)), ...
        'VariableNames',ridgeT.Properties.VariableNames);
      ridgeT = [ridgeT; rmmissing(chT)]; %#ok<AGROW>
    end
    
    %% plot visualizations if desired
    
    if plt
      fig = figure(sum(expected)); clf; fig.WindowStyle = 'docked';
      Xedges = [-inf linspace(floor(min(stat.rpow(:))),ceil(max(stat.rpow(:))),100) inf];
      Xvals = [Xedges(2) mean([Xedges(2:end-2); Xedges(3:end-1)],1) Xedges(end-1)];
      Yedges = [-inf linspace(floor(min(stat.fridge(:))),ceil(max(stat.fridge(:))),20) inf];
      Yvals = [Yedges(2) mean([Yedges(2:end-2); Yedges(3:end-1)],1) Yedges(end-1)];
      for ch = 1:numel(stat.label)
        N = histcounts2(stat.rpow(:,ch,:,:),stat.fridge(:,ch,:,:),Xedges,Yedges);
        subplot(4,4,ch); imagesc(Xvals,Yvals,N'); axis xy; colormap jet
        xlabel('Raw Power'); ylabel('Frequency (Hz)');
        title(stat.label{ch});
      end
    end
  end
  
  %% identify & quantify time-frequency ridges in trial data
  
  for p = 1:3    % phase number, excluding baseline
    if isempty(nexfile{r,p})
      warning(['Skipping rat ' num2str(ratT.ratnums(r)) '''s ' phases{p} ...
        ' due to missing data.'])
      continue  % to next phase, w/o saving warning in errlog, since it's expected
    end
    
    for tn = 1:expected(p)   % trial number within phase
      tr = sum([tn expected(1:p-1)]);   % trl # over whole expt. for errlog
      
      waitbar((r-1 + tr/sum(expected))/size(ratT,1), wb, ...   % no -1 for tr because baseline is done
        ['processing CS+ ' num2str(tn) ' of ' phases{p} ' for rat #' ...
        num2str(ratT.ratnums(r))])
      
      cfg = [];
      cfg.inputfile = [datafolder num2str(ratT.ratnums(r)) filesep 'CS+' ...
        num2str(tn) 'of' phases{p} '_' num2str(ratT.ratnums(r)) spectype ...
        'TFApow' num2str(winlen) 's.mat'];
      cfg.outputfile = [datafolder num2str(ratT.ratnums(r)) filesep 'CS+' ...
        num2str(tn) 'of' phases{p} '_' num2str(ratT.ratnums(r)) spectype ...
        'TFApow' num2str(winlen) 's_powridges.mat'];
      
      %% check whether files exist
      
      if exist(cfg.outputfile,'file') && ~redo
        errlog{r,tr} = ['tfridges already extracted from Rat #' num2str(ratT.ratnums(r)) ...
          '''s CS+ ' num2str(tn) ' of ' phases{p} '. Skipping.'];
        warning(errlog{r,tr})
        %         load(outputfile);   % get most recent ridgeT?
        continue  % to next tone
      end
      
      if ~exist(cfg.inputfile,'file')
        errlog{r,tr} = ['input file does not exist for Rat #' num2str(ratT.ratnums(r)) ...
          '''s CS+' num2str(tn) 'of' phases{p} '. Skipping.'];
        warning(errlog{r,tr})
        continue  % to next tone
      end
      
      %% extract powridges
      
      stat = powridges_TEM(cfg);
      
      %% arrange data in a table, remove missing values, and append to aggregate
      
      for ch = 1:numel(stat.label)
        chT = table(repmat(ratT.ratnums(r),numel(stat.time),1), ...
          repmat(stat.label{ch},numel(stat.time),1), ...
          repmat(tr,numel(stat.time),1), stat.time', ...
          squeeze(stat.fridge(1,ch,:)), squeeze(stat.rpow(1,ch,:)), ...
          squeeze(stat.rprom(1,ch,:)), squeeze(stat.rtun(1,ch,:)), ...
          'VariableNames',ridgeT.Properties.VariableNames);
        ridgeT = [ridgeT; rmmissing(chT)]; %#ok<AGROW>
      end
      
      %% plot visualizations if desired
      
      if plt
        fig = figure(tr); clf; fig.WindowStyle = 'docked';
        for ch = 1:numel(stat.label)
          subplot(4,4,ch);
          scatter(stat.rprom(1,ch,stat.time < 0), ...
            stat.fridge(1,ch,stat.time < 0), '.'); hold all;
          scatter(stat.rprom(1,ch,stat.time > 0 & stat.time < 30), ...
            stat.fridge(1,ch,stat.time > 0 & stat.time < 30), '.')
          scatter(stat.rprom(1,ch,stat.time > 30), ...
            stat.fridge(1,ch,stat.time > 30), '.')
          xlabel('Ridge Prominence'); ylabel('Frequency (Hz)');
          title(stat.label{ch}); axis tight;
        end
        legend({'Before','During','After'}, 'FontSize',6, 'Location','Best')
      end
      
      %% save progress on ridgeT
      
      save(outputagg,'ridgeT');
      
    end
    if plt
      keyboard % pause to examine figures
    end
  end
  %% statistically compare ridge measurements by trial & reltime for each ch
  
  if dostats
    for ch = 1:numel(allchs)
      %% simplify table
      partialT = ridgeT(ismember(ridgeT.rat,ratT.ratnums(r)) ...
        & ismember(ridgeT.channel,allchs{ch},'rows'),3:end);
      
      if size(partialT,1) == 0
        warning(['No data for rat ' num2str(ratT.ratnums(r)) ', ' allchs{ch}])
        continue  % to next channel
      end
      
      partialT.block = discretize(partialT.trial, blkedges, ...
        'categorical',blknames, 'IncludedEdge','right');
      partialT.reltime = discretize(partialT.time, [-inf 0 30 inf], ...
        'categorical', {'Before','During','After'}, 'IncludedEdge','right');
      partialT(ismember(partialT.block,'BL','rows'),:) = [];
      %     crosstab(partialT.reltime,partialT.block)
      
      %% run stats
      
      %     [d,p,stats] = manova1(partialT{ismember(partialT.reltime,'During','rows'), metrics}, ...
      %       partialT.block(ismember(partialT.reltime,'During','rows')));
      %     manovacluster(stats)
      for v = 1:numel(metrics)
        [p,tbl,stats,terms] = anovan(partialT.(metrics{v}), ...
          [{partialT.reltime} {partialT.block}], 'model','full', ...
          'varnames',{'reltime','block'}, 'display','off');
        posthoc.(metrics{v}).anovaT{ch,r} = tbl;  % copy ANOVA table into aggregate array
        
        [c,m,h,gnames] = multcompare(stats,'Dimension',[1 2], 'display',display);
        
        if isfield(posthoc,'gnames') && numel(gnames) < numel(posthoc.gnames)
          m_orig = m; m = NaN(numel(posthoc.gnames), size(m_orig,2));
          c_orig = c; c = NaN(size(posthoc.gcomp,1), size(c_orig,2));
          gcomp = c_orig(:,1:2);   % group comparisons as indices into gnames
          [~,gI] = ismember(gnames, posthoc.gnames);  % index correction
          for g = 1:numel(gI)
            m(gI(g),:) = m_orig(g,:); 	% copy row from m_orig into correct row of new m
            gcomp(gcomp == g) = gI(g);  % replace w/ indices into posthoc.gnames
          end
          [~,gcI] = ismember(gcomp, posthoc.gcomp, 'rows'); 	% index correction
          for gc = 1:numel(gcI)
            c(gcI(gc),:) = [gcomp(gc,:), c_orig(gc,3:end)];  % use corrected indices for group names as well as comparisons
          end
        end
        
        posthoc.(metrics{v}).pvals(:,ch,r) = c(:,6);  % copy posthoc p values into aggregate array
        posthoc.(metrics{v}).means(:,ch,r) = m(:,1);  % copy mean per block-time into agg
        
        if strcmp(display,'on')
          print([figurefolder num2str(ratT.ratnums(r)) allchs{ch} spectype ...
            num2str(winlen) 'sTFApowridges_' metrics{v} '_posthoc'],'-dpng')
        end
      end   % for all metrics
    end   % for all channels
    
    if ~isfield(posthoc,'gnames') || numel(gnames) > numel(posthoc.gnames)
      posthoc.gnames = gnames;    % group names
    end
    if ~isfield(posthoc,'gcomp') || numel(gcomp) > numel(posthoc.gcomp)
      posthoc.gcomp = c(:,1:2);   % order of group comparisons
    end
    
    %% save progress on stats
    
    save(outputstats,'posthoc');
    
  end
  
  %% save processing time
  rattime(r) = toc;
end

%% discretize ridgeT

ridgeT.block = discretize(ridgeT.trial, blkedges, ...
  'categorical',blknames, 'IncludedEdge','right');
reltime = discretize(ridgeT.time, [-inf 0 30 60 inf], ...
  'categorical', {'Before','During','After','NoTones'}, 'IncludedEdge','right');
reltime(ismember(ridgeT.block,'BL') & ...
  ismember(reltime,{'Before','During','After'})) = 'NoTones';   % doesn't work w/in table
ridgeT.reltime = reltime;   % crosstab(ridgeT.reltime,ridgeT.block)
ridgeT.channel = categorical(cellstr(ridgeT.channel), allchs);

save(outputagg,'ridgeT');

%% finish

if ~all(isempty(errlog(:)))
  warning('errlog is not empty - please review')
end

waitbar(1, wb, ['Done!  Total time: ' ...
  num2str(sum(rattime,'omitnan')/60) ' minutes']);
