function [output] = do_BDAmedian_OrigFear_TEM( spectype, r )
% DO_BDAMEDIAN_ORIGFEAR_TEM reduces TFA to median power per freq of 30s timebins
% before/during/after each tone
%
%   Depends on global variables defined in info_origfear.
%
% written 8/16/17 by Teresa E. Madsen, Ph.D.

%% check for input

if nargin == 0  % like when I just hit "Run and Time" in the Editor tab
  info_origfear;  % metadata may not have been already loaded by master script
  spectype  = 'medWav';  % freq output of do_[spectype]_origfear_TEM
  r         = 1;            % poster rat
end

%% declare global variables

global datafolder configfolder figurefolder ratT fsample stimuli phases  ...
  intervals blkedges blknames mainblocks blksof7names expected

%% create waitbar

wb = waitbar(0, ['Preparing to reduce time axis to 30s BDA tones, for rat #' ...
  num2str(ratT.ratnums(r)) '''s ' spectype]);

%% loop through all .mat files in spectype folder

listing = dir([datafolder spectype filesep num2str(ratT.ratnums(r)) filesep ...
  '*.mat']);
comptime  = nan(size(listing));     % for each file
esttime   = 50;                     % estimated comptime (s) per file
% esttime = nanmean(comptime);      % average of past calculations
output    = nan(numel(listing),2);  % skewness before/after dB conversion

for fn = 1:numel(listing)
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/(esttime.*numel(comptime)), wb, ...
    ['reducing ' listing(fn).name ' to 30s BDA tones']);
  
  %% load inputfile, skipping to dB version if available
  
  inputfile = [listing(fn).folder filesep listing(fn).name];
  if contains(inputfile,'Baseline')
    continue  % skip to next file - BDA doesn't make sense for baseline
  elseif contains(inputfile,'fourier') && ...
      existfile_TEM(strrep(inputfile,'fourier','powspctrm_dB'))
    inputfile = strrep(inputfile,'fourier','powspctrm_dB');
  elseif existfile_TEM(strrep(inputfile,'.mat','_dB.mat'))
    inputfile = strrep(inputfile,'.mat','_dB.mat');
  end
  outputfile = strrep(inputfile,[filesep spectype filesep],...
    [filesep 'BDAmedian' filesep]);   % change folder ^ and filename v
  outputfile = strrep(outputfile,'.mat',['_' spectype '_BDAmedian.mat']);
  if existfile_TEM(outputfile)
    warning(['outputfile already exists: ' outputfile(numel(datafolder):end)])
    continue  % skip to next file
  end
  
  freq = rmvlargefields_TEM(inputfile);
  tmpcfg = freq.cfg;
  while ~isfield(tmpcfg,'wavlen')
    if isfield(tmpcfg,'previous')
      if isstruct(tmpcfg.previous)
        tmpcfg = tmpcfg.previous;
      elseif iscell(tmpcfg.previous)
        tmpcfg = tmpcfg.previous{1};
      else
        error('unexpected datatype of tmpcfg.previous')
      end
    else
      error('wavlen not found in nested cfgs')
    end
  end
  wavlen = tmpcfg.wavlen;
  
  %% convert to powspctrm if necessary
  
  if contains(listing(fn).name,'fourier')
    inputfile = strrep(inputfile,'fourier','powspctrm');
    freq = ft_checkdata(freq,'cmbrepresentation','sparsewithpow');
  end
  
  switch freq.dimord
    case 'rpt_chan_freq_time'
      freq.powspctrm = shiftdim(freq.powspctrm,1);  % wrap trial dim to end
      freq.dimord = 'chan_freq_time';
      assert(size(freq.powspctrm,4) == 1, 'multiple trials per file not expected')
    case 'chan_freq_time'
      % okay, do nothing
    otherwise
      error('unexpected dimord')
  end
  
  %% convert to dB if necessary
  
  if ~any(freq.powspctrm(:) <= 0) 
    output(fn,1) = skewness(freq.powspctrm(:));
    if output(fn,1) < 3
      warning('relatively low skew')
      figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
      keyboard  % verify need to convert to dB first
    end
    inputfile = strrep(inputfile,'.mat','_dB.mat');
    
    % faster to do this in two steps, otherwise it runs it one element at a time
    cfg = [];
    cfg.parameter   = 'powspctrm';
    cfg.operation   = 'log10';  
    freq = ft_math(cfg,freq);
    
    cfg.operation   = 'multiply';
    cfg.scalar      = 10;
    cfg.outputfile  = inputfile;
    freq = ft_math(cfg,freq);
    
    output(fn,2) = skewness(freq.powspctrm(:));
    if abs(output(fn,2)) > output(fn)
      warning('dB conversion may have made distribution *less* normal')
    end
  end
  
  if any(isnan(freq.powspctrm(:))) && ...             % when there are any NaNs,
      all(cellfun(@isempty,regexp(listing(fn).name, ...   % and the filename
      {'CS\+12ofConditioning\w*200\w*\.mat', ...      % doesn't match either of
      'CS\+12ofConditioning\w*338\w*\.mat'}, 'once')))  % these known exceptions
    warning('NaNs in converted powspctrm')
    figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
    keyboard  % pause to verify
  end
  
  %% replace acq powspctrm time/freq bins including shock artifact w/ NaNs
  
  if ~isempty(regexp(listing(fn).name,'CS\+1[1-7]ofConditioning\w*\.mat','once'))
    maxbadt = freq.time > 29 & freq.time < 34;   % so random noise far from shock won't be included in artifact
    minbadt = freq.time > 29.5 & freq.time < 30;   % actual shock time
    % take median across channels of median across frequencies, omitting NaNs so
    % we can see what's going on (i.e., clipping) in higher frequencies while
    % lower are still masked, base threshold on outliers outside of any masking
    medianPow = squeeze(median(median(freq.powspctrm,1,'omitnan'),2,'omitnan'));   
%     NonShkPowRange = quantile(medianPow(~maxbadt),normcdf([-4 4]));
    [muhat,sigmahat] = normfit(medianPow(~maxbadt));
    NonShkPowRange = [muhat - 4*sigmahat, muhat + 4*sigmahat];
    badt = minbadt(:) | (maxbadt(:) & (medianPow(:) < NonShkPowRange(1) | ...
      medianPow(:) > NonShkPowRange(2)));
%     fig = figure; fig.WindowStyle = 'docked'; hold all; 
%     plot(freq.time(~badt),medianPow(~badt),'b.',freq.time(badt),medianPow(badt),'r+')
    SSbadt = [find(badt,1,'first') find(badt,1,'last')];  % start stop samples
    if (diff(SSbadt)+1)/sum(badt) >= 2  % more than 1/2 of this time is not really artifact
      warning('discontinuity in detected clipping artifact')
      keyboard  % decide how to deal with this
    end
    for f = 1:numel(freq.freq)   % lower freqs set maxbadt for higher
      artStart = nearest(freq.time,freq.time(SSbadt(1)) - wavlen(f)/2);
      artEnd = nearest(freq.time,freq.time(SSbadt(2)) + wavlen(f)/2);
      freq.powspctrm(:,f,artStart:artEnd) = NaN;
    end
%     figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
%     keyboard  % review
  end
  
  %% reduce time axis to 30s BDA tones
  
  BDAfreq = freq;
  BDAfreq.time = -15:30:45;   % center times of 3 30s windows shifting by 30s
  BDAfreq.powspctrm = nan(size(freq.powspctrm,1), ...
    numel(BDAfreq.freq), numel(BDAfreq.time));
  
  for f = 1:numel(BDAfreq.freq)  % use median to preserve sharp changes & avoid biasing by skew,
    for t = 1:numel(BDAfreq.time)
      tndx = abs(freq.time-BDAfreq.time(t)) < 15;   % +/-15s
      if ~any(tndx)
        warning('no times selected')
        keyboard
      end
      BDAfreq.powspctrm(:,f,t) = median(freq.powspctrm(:,f,tndx),3,'omitnan');
    end  % omit nans so shock artifact doesn't invalidate during & after time bins
    % for ch = 1:numel(smthfreq.label)
    % fig = figure; fig.WindowStyle = 'docked';
    % subplot(1,2,1); histfit(squeeze(freq.powspctrm(ch,f,:)));
    % title(['f = ' num2str(round(freq.freq(f),3,'significant')) ' Hz, skew = ' num2str(skewness(squeeze(freq.powspctrm(ch,f,:)))) ', before smoothing'])
    % subplot(1,2,2); autocorr(squeeze(freq.powspctrm(ch,f,:)),find(freq.time - freq.time(1) > 30,1,'first'))
    % fig = figure; fig.WindowStyle = 'docked';
    % subplot(1,2,1); histfit(squeeze(smthfreq.powspctrm(ch,f,:)));
    % title(['f = ' num2str(round(smthfreq.freq(f),3,'significant')) ' Hz, skew = ' num2str(skewness(squeeze(smthfreq.powspctrm(ch,f,:)))) ', after smoothing'])
    % subplot(1,2,2); autocorr(squeeze(smthfreq.powspctrm(ch,f,:)),30)
    % end
  end
  % figure; plotFTmatrix_TEM(struct('zlim','zeromax'),smthfreq)
  outputfile = strrep(inputfile,[filesep spectype filesep],...
    [filesep 'BDAmedian' filesep]);   % change folder and filename
  outputfile = strrep(outputfile,'.mat',['_' spectype '_BDAmedian.mat']);
  if existfile_TEM(outputfile)
    warning(['outputfile already exists: ' outputfile(numel(datafolder):end)])
    keyboard  % check before overwriting
  end
  
  %% save smoothed file
  
  freq = BDAfreq;
%     figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
%     keyboard  % review
  disp(['saving freq to ' outputfile(numel(datafolder):end)])
  save(outputfile,'freq');
  
  %% clear freq to conserve memory
  
  clearvars freq smthfreq
  
  %% update estimate of time to complete analysis
  
  comptime(fn) = toc(starttime);
  esttime = nanmean([esttime; comptime(fn)]); % average into estimate of how long each file should take
end

%% clean up

if all(isnan(output(:)));   output = comptime;  end
close(wb);

end   % function