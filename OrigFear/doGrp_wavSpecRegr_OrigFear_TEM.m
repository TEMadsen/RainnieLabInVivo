function [output] = doGrp_wavSpecRegr_OrigFear_TEM
% DOGRP_WAVSPECREGR_ORIGFEAR_TEM tests for linear trends btwn specgrams & trl#,
% across all rats, one block of 7 trials at a time
%
%   This function is for group-wide visualization & stats using
%   ft_statfun_depsamplesregrT on the output of wavelet (power spectrograms).
%
%   Depends on global variables defined in info_origfear.
%
% written 7/17/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder configfolder figurefolder ratT fsample stimuli phases ...
  intervals blkedges blknames mainblocks blksof7names expected regions

if isempty(ratT)
  info_origfear
end

%% create waitbar

wb = waitbar(0, 'Preparing for group wavelet spectrogram analysis');

%% loop through main blocks, loading data

smthfreq  = cell(6,10,3); % size(ratT,1));   % 7-10 tones per block:  begin & end of each recording
comptime  = nan(size(smthfreq,1),3);  % 3 steps:  prep data, run stats, plot results
esttime   = [573, 160, 15];   % each step's estimated comptime (s) per block

datfile = [datafolder 'wavSpecRegr' filesep 'ICA_pre-stat-data.mat'];
if existfile_TEM(datfile)
  load(datfile)
else
  for b = 1:numel(blksof7names)
    %% set up block
    
    starttime = tic;
    waitbar(sum(comptime(:),'omitnan')/sum(esttime.*size(smthfreq,1)), wb, ...
      ['Preparing ' blksof7names{b} ' wavelet spectrograms for regression analysis']);
    
    p = ceil(b/2);  % convert block to phase
    
    if rem(b,2) == 1  % odd #s
      tones = 1:10;                       % 1st 10 of each recording
    else              % even #s
      tones = expected(p)-6:expected(p);  % last 7 of each recording
    end
    
    freq = cell(numel(tones),size(smthfreq,3));  % preallocate by block - gets too big otherwise
    
    %% loop through rats & trials to collect data, smooth, & downsample
    
    for r = 1:3 % find(~ratT.excluded)'
      for tr = 1:numel(tones)   % trial # within block
        %% define trial inputfile
        
        tn = tones(tr);   % trial # within phase
        
        inputfile = [datafolder 'wavelet' filesep num2str(ratT.ratnums(r)) ...
          filesep stimuli{1} num2str(tn) 'of' phases{p} '_ICA_' ...
          num2str(ratT.ratnums(r)) 'waveletTFAdB.mat'];
        
        %% load inputfile, if it exists
        
        if ~existfile_TEM(inputfile)
          warning(['Skipping tone ' num2str(tn) ' of ' phases{p} ...
            ' because inputfile (' inputfile(numel(datafolder):end) ') was not found.'])
          continue  % to next tone
        end
        
        freq{tr,r} = rmvlargefields_TEM(inputfile);
        
        %% smooth & downsample time axis, use linear change in freq smoothing
        
        mtmc = get_cfg_TEM('mtmconvol','OrigFear');
        
        smthfreq{b,tr,r} = freq{tr,r};
        smthfreq{b,tr,r}.time = -60:0.5:60;   % 5s windows shifting by 0.5s
        smthfreq{b,tr,r}.freq = mtmc.foi(mtmc.foi-mtmc.tapsmofrq > freq{tr,r}.freq(1) ...
          & mtmc.foi+mtmc.tapsmofrq < freq{tr,r}.freq(end));  % where full range is available to average
        smthfreq{b,tr,r}.powspctrm = nan(size(freq{tr,r}.powspctrm,1), ...
          numel(smthfreq{b,tr,r}.freq), numel(smthfreq{b,tr,r}.time));
        for t = 1:numel(smthfreq{b,tr,r}.time)
          tndx = abs(freq{tr,r}.time-smthfreq{b,tr,r}.time(t)) < 2.5;   % +/-2.5s
          if ~any(tndx)
            warning('no times selected')
            keyboard
          end
          for f = 1:numel(smthfreq{b,tr,r}.freq)
            fndx = abs(freq{tr,r}.freq-smthfreq{b,tr,r}.freq(f)) < ...
              mtmc.tapsmofrq(f);  % +/-w
            if ~any(fndx)
              warning('no frequencies selected')
              keyboard
            end
            smthfreq{b,tr,r}.powspctrm(:,f,t) = median(...  % to preserve sharp changes & avoid biasing by skew
              reshape(freq{tr,r}.powspctrm(:,fndx,tndx), ...
              size(freq{tr,r}.powspctrm,1),[]), 2,'omitnan');
          end
        end
        
        %% select best 2 region-representative channels
        
        cfg = [];
        cfg.channel = [ratT.mPFC(r) ratT.BLA(r)];  % keep in their cells!
        
        smthfreq{b,tr,r} = ft_selectdata(cfg,smthfreq{b,tr,r});
        
        %% rename each channel to the region it represents
        
        for rrc = 1:numel(regions)
          if ~strcmp(ratT.(regions{rrc}){r},'none')   % don't pad w/ NaNs, as this will cause the stats to come out NaNs
            smthfreq{b,tr,r}.label{ ...
              ismember(smthfreq{b,tr,r}.label, ratT.(regions{rrc}){r})} = ...
              regions{rrc};
          end
        end
        
        %% check for too many NaNs
        
        if sum(isnan(smthfreq{b,tr,r}.powspctrm(:)))/numel(smthfreq{b,tr,r}.powspctrm) ...
            >= 0.2  % 24s - could be most of a before, during, or after tone period
          warning(['trial #' num2str(tn) ' of ' phases{p} ...
            ' skipped because more than 20% of the spectrogram is NaNs'])
          smthfreq{b,tr,r} = [];
        end
      end
    end
    
    %% clear freq to free up memory
    
    clearvars freq
    
    %% update estimate of time to complete analysis
    
    comptime(b,1) = toc(starttime);
    esttime(1) = nanmean([esttime(1); comptime(b,1)]); % average into estimate of how long each block should take
  end
  
  disp(['saving temp data to ' datfile])
  save(datfile,'smthfreq','comptime','esttime')
  
end

%% run group stats on power spectrograms

stat = cell(size(smthfreq,1),1);

for b = 1:size(smthfreq,1)
  %% determine which trials & rats to omit from stats due to missing data
  
  omitrats = all(cellfun(@isempty,smthfreq(b,:,:)),2);   % rats for which all trials are missing
  omittrls = any(cellfun(@isempty,smthfreq(b,:,~omitrats)),3);  % trials missing for any non-omitted rats
  if any(omitrats) || any(omittrls)
    warning([num2str(sum(omitrats)) ' rats'' and ' num2str(sum(omittrls)) ...
      ' trials'' data are missing from ' blksof7names{b}])
    if sum(omitrats) > 2 || ...   % 1 rat is excluded completely & 1 rat will be missing all data from last 2 blocks
        sum(omittrls) > 4   % 3 trials will be missing from every even#ed block, plus one missing from acquisition
      keyboard  % if more than that, pause and check
    end
  end
  
  %% define & check for outputfile
  
  cfg = [];
  cfg.outputfile  = [datafolder 'wavSpecRegr' filesep blksof7names{b} ...
    '_ICA_stat_depsamplesregrT_NPindiv_ordered.mat'];
  
  %   if existfile_TEM(cfg.outputfile)
  %     stat{b} = rmvlargefields_TEM(cfg.outputfile);
  %   else
  starttime = tic;  % only time computation, not just loading files
  
  cfg.statistic         = 'ft_statfun_depsamplesregrT';
  
  waitbar(sum(comptime(:),'omitnan')/sum(esttime.*size(smthfreq,1)), wb, ...
    ['Running ' cfg.statistic(12:end) ' on ' blksof7names{b} ...
    ' wavelet spectrograms for all rats']);
  
  cfg.method            = 'montecarlo';
  cfg.correctm          = 'cluster';
  cfg.clusteralpha      = 0.025;
  cfg.clusterstatistic  = 'maxsum';
  cfg.clusterthreshold  = 'nonparametric_individual';   % essential for reasonable thresholds!
  cfg.minnbchan         = 0;  % required when there are no neighbors given
  cfg.tail              = 0;
  cfg.clustertail       = 0;
  cfg.alpha             = 0.025;
  cfg.numrandomization  = 500;
  cfg.orderedstats      = 'yes';  % essential for reasonable thresholds!
  cfg.neighbours        = [];   % use time/freq info only in determining clusters
  cfg.parameter         = 'powspctrm';
  
  trlns = find(~omittrls);    % skips missing trial #s as appropriate
  ratns = 1:sum(~omitrats);   % must be 1 to the # of subjects
  
  cfg.design  = [repmat(trlns(:)',1,numel(ratns)); ...  % 1 2 3 1 2 3 1 2 3
    reshape(repmat(ratns,numel(trlns),1),1,[])];    % 1 1 1 2 2 2 3 3 3
  cfg.ivar    = 1;  % indep var = trial #
  cfg.uvar    = 2;  % unit of obs = rat
  
  stat{b} = ft_freqstatistics(cfg, smthfreq{b,~omittrls,~omitrats});
  comptime(b,2) = toc(starttime);
  esttime(2) = nanmean([esttime(2); comptime(b,2)]); % average into estimate of how long each block should take
  %   end
end

%% print & plot (top 10/nearly) significant correlations of spectrograms & trl#s

for b = find(~cellfun(@isempty,stat))'
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/sum(esttime.*size(smthfreq,1)), wb, ...
    ['Plotting results of ' stat{b}.cfg.statistic(12:end) ' on ' blksof7names{b} ...
    ' wavelet spectrograms for rat #' num2str(ratT.ratnums(r))]);
  
  pos = [];
  if ~isfield(stat{b},'posclusters') || isempty(stat{b}.posclusters)
    pos.n = 0;
    pos.I = [];  % no index because there are no clusters
  else
    [pos.p,pos.I] = sort([stat{b}.posclusters.prob]);
    pos.n = find(pos.p > 0.025,1,'first') - 1;
  end
  neg = [];
  if ~isfield(stat{b},'negclusters') || isempty(stat{b}.negclusters)
    neg.n = 0;
    neg.I = [];  % no index because there are no clusters
  else
    [neg.p,neg.I] = sort([stat{b}.negclusters.prob]);
    neg.n = find(neg.p > 0.025,1,'first') - 1;
  end
  bestch = zeros(pos.n+neg.n+2,1);
  disp([newline 'Found clusters with significant positive (' ...
    num2str(pos.n) ') and negative (' num2str(neg.n) ')' newline ...
    'correlations with trial #s within ' blksof7names{b} newline])
  trlns = nan(1,size(smthfreq,2));
  
  %% visualize positive correlation clusters
  
  for n = 1:(pos.n + 1)  % may plot one that's not considered significant
    if n > 10 || numel(pos.I) < n
      break  % out of for loop
    end
    cI = pos.I(n);  % cluster index
    I = find(stat{b}.posclusterslabelmat == cI);
    [ch,f,t] = ind2sub(size(stat{b}.posclusterslabelmat),I);
    bestch(n) = mode(ch);
    ch = unique(ch); f = unique(f); t = unique(t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat{b}.freq(min(f)) stat{b}.freq(max(f))];
    trange = [stat{b}.time(min(t)) stat{b}.time(max(t))];
    clustpowpertrl = nan(numel(I),size(smthfreq,2));   % aggregate cluster power over trials
    for tr = 1:numel(trlns)
      if ~any(cellfun(@isempty,smthfreq(b,tr,:)),3)
        trlns(tr) = smthfreq{b,tr,1}.trialinfo(1,1);  % same for all rats
        for r = 1:size(smthfreq,3)
          if ~isempty(smthfreq(b,tr,r))   % in case of omitted rats
            clustpowpertrl(:,tr,r) = smthfreq{b,tr,r}.powspctrm(I);
          end
        end
      end
    end
    
    fig = figure(b*100+n); clf; fig.WindowStyle = 'docked';
    fig.PaperPositionMode = 'auto';
    % plot mean per rat across trials for each chan/freq/timebin w/in cluster
    hold all
    for r = 1:size(smthfreq,3)
      if ~isempty(smthfreq(b,tr,r))   % in case of omitted rats
        X = repmat(trlns,size(clustpowpertrl,1),1);
        Y = clustpowpertrl(:,:,r);
        CC = corrcoef(X(:), Y(:));
        [pc,ErrorEst] = polyfit(X(:), Y(:), 3);   % fit cubic model
        pop_fit = polyval(pc,trlns,ErrorEst);
        plot(X(:),Y(:),'+', trlns,mean(Y),'-', ...
          trlns,pop_fit,'--');
      end
    end
    axis tight
    legend({'chan/time/freq bins','mean per trial','cubic fit'}, ...
      'Location','Best')
    
    title([strjoin(stat{b}.label(ch)) newline ...
      'have significant (p = ' num2str(stat{b}.posclusters(cI).prob) ...
      ') positive correlations (overall coeff = ' num2str(CC(2)) ')' newline ...
      'at ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz, from ' ...
      num2str(trange(1)) ' - ' num2str(trange(2)) ' s' newline ...
      'with trial #s within ' blksof7names{b} ':'])
    
    print([figurefolder 'PosWavPowerCorr' num2str(n) blksof7names{b} ...
      '_ICA_stat_depsamplesregrT_NPindiv_ordered'], '-dpng','-r0')
    %     close(fig)
  end
  
  %% visualize negative correlation clusters
  
  for n = 1:(neg.n + 1)  % may plot one that's not considered significant
    if n > 10 || numel(neg.I) < n
      break  % out of for loop
    end
    cI = neg.I(n);  % cluster index
    I = find(stat{b}.negclusterslabelmat == cI);
    [ch,f,t] = ind2sub(size(stat{b}.negclusterslabelmat),I);
    bestch(n) = mode(ch);
    ch = unique(ch); f = unique(f); t = unique(t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat{b}.freq(min(f)) stat{b}.freq(max(f))];
    trange = [stat{b}.time(min(t)) stat{b}.time(max(t))];
    clustpowpertrl = nan(numel(I),size(smthfreq,2));   % aggregate cluster power over trials
    for tr = 1:numel(trlns)
      if ~any(cellfun(@isempty,smthfreq(b,tr,:)),3)
        trlns(tr) = smthfreq{b,tr,1}.trialinfo(1,1);  % same for all rats
        for r = 1:size(smthfreq,3)
          if ~isempty(smthfreq(b,tr,r))   % in case of omitted rats
            clustpowpertrl(:,tr,r) = smthfreq{b,tr,r}.powspctrm(I);
          end
        end
      end
    end
    
    fig = figure(b*100+n); clf; fig.WindowStyle = 'docked';
    fig.PaperPositionMode = 'auto';
    % plot mean per rat across trials for each chan/freq/timebin w/in cluster
    hold all
    for r = 1:size(smthfreq,3)
      if ~isempty(smthfreq(b,tr,r))   % in case of omitted rats
        X = repmat(trlns,size(clustpowpertrl,1),1);
        Y = clustpowpertrl(:,:,r);
        CC = corrcoef(X(:), Y(:));
        [pc,ErrorEst] = polyfit(X(:), Y(:), 3);   % fit cubic model
        pop_fit = polyval(pc,trlns,ErrorEst);
        plot(X(:),Y(:),'+', trlns,mean(Y),'-', ...
          trlns,pop_fit,'--');
      end
    end
    axis tight
    legend({'chan/time/freq bins','mean per trial','cubic fit'}, ...
      'Location','Best')
    
    title([strjoin(stat{b}.label(ch)) newline ...
      'have significant (p = ' num2str(stat{b}.negclusters(cI).prob) ...
      ') negative correlations (overall coeff = ' num2str(CC(2)) ')' newline ...
      'at ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz, from ' ...
      num2str(trange(1)) ' - ' num2str(trange(2)) ' s' newline ...
      'with trial #s within ' blksof7names{b} ':'])
    
    print([figurefolder 'NegWavPowerCorr' num2str(n) blksof7names{b} ...
      '_ICA_stat_depsamplesregrT_NPindiv_ordered'], '-dpng','-r0')
    %     close(fig)
  end
  
  %% plot stat
  
  for rrc = 1:numel(regions)
    fig = figure(b*100+rrc*30); clf; fig.WindowStyle = 'docked';
    cfg=[];
    cfg.channel         = regions{rrc};
    cfg.zlim            = quantile(stat{b}.stat(:),[0.025 0.975]);
    cfg.parameter       = 'stat';
    cfg.maskparameter   = 'mask';
    cfg.maskstyle       = 'outline';
    cfg.feedback        = 'no';
    plotFTmatrix_TEM(cfg,stat{b});
    title([regions{rrc} ' Correlations with ' blksof7names{b} ' Trial #s'])
    
  fig.PaperPositionMode = 'auto';
  print([figurefolder 'WavPowerCorrSpectrogram_' blksof7names{b} '_ICA_' ...
    regions{rrc} '_stat_depsamplesregrT_NPindiv_ordered'], '-dpng','-r0')
  %   close(fig)
  end
  
  comptime(b,3) = toc(starttime);
  esttime(3) = nanmean([esttime(3); comptime(b,3)]); % average into estimate of how long each block should take
end

%% clean up

output = comptime;
close(wb);
end
