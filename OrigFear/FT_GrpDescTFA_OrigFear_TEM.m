%% Accumulates all rats' freq into one structure, calculates descriptive
% statistics & averages for plotting

CleanSlate  % provides a clean slate to start working with new data/scripts,
% but does not clear functions, breakpoints, force unresponsive figures, etc.

wb = waitbar(0,'Initializing...');   % will display progress through loop
h = findall(wb,'Type','text');
h(1).Interpreter = 'none';

info_origfear             % load all metadata

artstr    = 'raw';   % string describing artifact rejection method used
spectype  = 'mtmconvol';  % assuming default parameters from get_cfg_TEM
winlen    = '';           % movwin length as string, if applicable, '' if not
normtype.pow  = 'ZdB';    % pseudo-z-score of dB transformed spectrograms
% normtype.coh  = 'Zraw';   % FIXME:  none of FT's aggregating/averaging
% functions work for coherence!!
redoavg   = false;        % to ignore & overwrite old group average files
redoplt   = false;        % to ignore & overwrite old spectrogram plots

%% loop through powspctrm & cohspctrm

fields = fieldnames(normtype);

for sf = 1:numel(fields)  % spectral field
  if strcmp(fields{sf},'coh')
    chnames = 'labelcmb';
    chcfg = 'channelcmb';
  else
    chnames = 'label';
    chcfg = 'channel';
  end
  
  outputfig = cell(numel(blknames)-1, 1);
  makefig = true(numel(blknames)-1, 1);   % assume we want all figures
  
  %% loop through blocks of 3-4 tones, averaging across rats
  
  for b = 1:numel(blknames)-1
    waitbar((sf-1+(b-1)/(numel(blknames)-1))/numel(fields), wb, ...
      ['Working on averaging ' fields{sf} 'spectra for ' blknames{b} '...'])
    
    %% define and check for output files
    
    outputfig{b} = [figurefolder 'GrpAve_' normtype.(fields{sf}) '_Norm_' ...
      artstr '_' spectype winlen 'TFA' fields{sf} '_' blknames{b}];  % .png
    
    if existfile_TEM([outputfig{b} '.png'])
      warning(['figure already printed: ' outputfig{b}]);
      makefig(b) = redoplt;   % only make it if it's marked for redo
    end
    
    outputfile = [datafolder 'GrpAve_' normtype.(fields{sf}) '_Norm_' ...
      artstr '_' spectype winlen 'TFA' fields{sf} '_' blknames{b} '.mat'];
    
    if existfile_TEM(outputfile)
      warning(['grpavg output file already computed: ' outputfile])
      calcavg = redoavg;                  % only calc if marked for redo
      loadavg = makefig(b) && ~redoavg; 	% only load if needed to make fig
    else
      loadavg = false;        % there's nothing to load
      calcavg = true;         % so it needs to be calculated
    end
    
    %% load or calculate group average for this block
    
    if loadavg
      avgfreq = rmvlargefields_TEM(outputfile);
    elseif calcavg
      makefig(b) = true;  % always remake fig if recalculating avg
      
      %% loop through rats
      
      rat2chfiles = cell(1,size(ratT,1));
      
      for r = find(~ratT.excluded)'
        %% check for file with only 2, renamed channels for input to groupavg
        
        rat2chfiles{r} = [datafolder num2str(ratT.ratnums(r)) filesep ...
          normtype.(fields{sf}) '_Norm_' artstr '_' spectype winlen 'TFA' ...
          fields{sf} '_' num2str(ratT.ratnums(r)) blknames{b} '_2ch.mat'];
        ratallchfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
          normtype.(fields{sf}) '_Norm_' artstr '_' spectype winlen 'TFA' ...
          fields{sf} '_' num2str(ratT.ratnums(r)) blknames{b} '.mat'];
        
        %% stop here to avoid continue
        
        if existfile_TEM(rat2chfiles{r})
          warning(['outputfile already exists: ' rat2chfiles{r}]);
          continue  % to next rat
        end
        
        if ~existfile_TEM(ratallchfile)
          warning(['inputfile does not exist: ' ratallchfile]);
          continue  % to next rat
        end
        
        %% load block, reduce to the 2 region-representative channels
        
        disp(['Loading freq for rat ' num2str(ratT.ratnums(r)) ' ' blknames{b}])
        %       load(ratallchfile)
        freq = rmvlargefields_TEM(ratallchfile);
        
        cfg = [];
        cfg.(chcfg) = [ratT.mPFC(r) ratT.BLA(r)];  % keep in their cells!
        
        freq = ft_selectdata(cfg,freq);
        
        %% rename each channel to the region it represents
        
        for rrc = 1:numel(regions)
          if strcmp(ratT.(regions{rrc}){r},'none')
            % pad with NaNs so this region isn't dropped from group average
            chdim = find(startsWith(split(freq.dimord,'_'),'chan'));
            assert(chdim == 1, 'unexpected dimord')
            freq.([fields{sf} 'spctrm'])(end+1,:,:) = NaN;
            freq.(chnames){end+1,:} = regions{rrc};
          else
            freq.(chnames){ismember(freq.(chnames), ratT.(regions{rrc}){r})} = ...
              regions{rrc};
          end
        end
        
        %% save renamed 2ch freq file
        
        disp(['Saving freq to ' rat2chfiles{r}])
        save(rat2chfiles{r},'freq');
        
      end   % for rat
      
      %% use ft_freqgrandaverage to collect all rats into 1 freq file
      % don't save - it's huge!
      
      goodfiles = cellfun(@existfile_TEM, rat2chfiles);
      
      if ~any(goodfiles)
        error('There are no good files for this block of tones/stimuli.')
      end
      
      if sum(goodfiles) < sum(~ratT.excluded)
        warning('Some rats are missing data.')
      end
      
      rat2chfiles = rat2chfiles(goodfiles);
      freq = cell(size(rat2chfiles));
      
      for rt = 1:numel(freq)
        freq{rt} = rmvlargefields_TEM(rat2chfiles{rt});
        if numel(freq{rt}.label) < numel(regions)
          r = find(cumsum(goodfiles) == rt,1,'first');
          for rrc = find(~ismember(regions,freq{rt}.label)')
            assert(strcmp(ratT.(regions{rrc}){r},'none'), ...   % otherwise...
              'missing channel should be present')
            % pad with NaNs so this region isn't dropped from group average
            chdim = find(startsWith(split(freq{rt}.dimord,'_'),'chan'));
            assert(chdim == 1, 'unexpected dimord')
            freq{rt}.([fields{sf} 'spctrm'])(end+1,:,:) = NaN;
            freq{rt}.(chnames){end+1,:} = regions{rrc};
          end
        end
      end
      
      cfg.keepindividual = 'yes';  % needed to get variance in next section
      
      grpfreq = ft_freqgrandaverage(cfg, freq{:});
      
      %% use ft_freqdescriptives to get variance & average across all rats
      
      cfg = [];
      
      cfg.variance     = 'yes';
      cfg.jackknife    = 'no';   % at least temporarily, as it returns all NaNs
      cfg.keeptrials   = 'no';
      cfg.outputfile    = outputfile;
      
      avgfreq = ft_freqdescriptives(cfg, grpfreq);
    end   % if loadavg or calcavg
    
    %% save max & min for later clims
    
    for ch = 1:size(avgfreq.([fields{sf} 'spctrm']),1)
      clims.([avgfreq.(chnames){ch,:}])(1,b) = min(min( ...
        avgfreq.([fields{sf} 'spctrm'])(ch,:,:), [], 'omitnan'), [], 'omitnan');
      clims.([avgfreq.(chnames){ch,:}])(2,b) = max(max( ...
        avgfreq.([fields{sf} 'spctrm'])(ch,:,:), [], 'omitnan'), [], 'omitnan');
    end
    
    %% plot group average spectrograms (1 subplot / region)
    
    fig = figure(b); clf; fig.WindowStyle = 'docked';
    fig.Name = blknames{b};
    plotFTmatrix_TEM([],avgfreq);
    
  end   % for blocks - finish computing & plotting all first
  
  %% refine figures
  
  waitbar(sf/numel(fields), wb, ...
      ['Refining figures for ' fields{sf} 'spectra...'])
    
  for b = 1:numel(blknames)-1  % then change all axes to +/- max for all blocks
    if makefig(b)
      fig = figure(b);
      sph = findall(fig,'Type','Axes');  % subplot handles
      cbh = findall(fig,'Type','Colorbar');  % colorbar handles
      
      for sp = 1:numel(sph)
        ax = sph(sp);
        ax.CLim = [-1.2 1.2]; 
        c = cbh(sp);
        c.Ticks = -1.2:0.4:1.2;
        c.Label.String = normtype.(fields{sf});
      end   % for subplots (channels)
      
      %% save figure
      
      print(fig, outputfig{b}, '-dpng', '-r300');
    end   % if makefig(b)
  end   % for block
end   % for spectral field

waitbar(1, wb, ['Done plotting group averaged ' strjoin(fields,'/') 'spectra!'])
