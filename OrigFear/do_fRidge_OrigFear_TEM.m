function [output] = do_fRidge_OrigFear_TEM( spectype, r )
% DO_FRIDGE_ORIGFEAR_TEM detects the dynamic peak frequency of any TFA,
% specified by spectype.
%
%   Depends on global variables defined in info_origfear.
%
% written 8/9/17 by Teresa E. Madsen, Ph.D.

%% check for input

if nargin == 0  % like when I just hit "Run and Time" in the Editor tab
  info_origfear;  % metadata may not have been already loaded by master script
  spectype  = 'sharpWav';   % freq output of do_[spectype]_origfear_TEM
  r         = 1;            % poster rat
end

%% declare global variables

global datafolder configfolder figurefolder ratT fsample stimuli phases  ...
  intervals blkedges blknames mainblocks blksof7names expected

%% create waitbar

wb = waitbar(0, ['Preparing to detect frequency ridges in ' spectype ...
  ' for rat #' num2str(ratT.ratnums(r))]);

%% loop through all .mat files in spectype folder

listing = dir([datafolder spectype filesep num2str(ratT.ratnums(r)) filesep ...
  '*.mat']);
comptime  = nan(size(listing));     % for each file
esttime   = 50;                     % estimated comptime (s) per file
% esttime = nanmean(comptime);      % average of past calculations

for fn = 1:numel(listing)
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/(esttime.*numel(comptime)), wb, ...
    ['detecting fRidge in ' listing(fn).name]);
  
  %% load inputfile, skipping dB files
  
  inputfile = [listing(fn).folder filesep listing(fn).name];
  if contains(inputfile,'dB')
    continue  % to next file, fridge needs raw power
  end
  if contains(inputfile,'fourier') && ...
      existfile_TEM(strrep(inputfile,'fourier','powspctrm'))
    inputfile = strrep(inputfile,'fourier','powspctrm');
  end
  outputfile = strrep(inputfile,[filesep spectype filesep],...
    [filesep 'fRidge' filesep]);   % change folder and filename
  outputfile = strrep(outputfile,'.mat',['_' spectype '_fRidge.mat']);
  outputfile = strrep(outputfile,'fourier','powspctrm');
  if existfile_TEM(outputfile)
    warning(['outputfile already exists: ' outputfile(numel(datafolder):end)])
    continue  % skip to next file
  end
  
  freq = rmvlargefields_TEM(inputfile);
  
  %% convert to powspctrm if necessary
  
  if contains(inputfile,'fourier')
    inputfile = strrep(inputfile,'fourier','powspctrm');
    freq = ft_checkdata(freq,'cmbrepresentation','sparsewithpow');
  end
  
  switch freq.dimord
    case {'rpt_chan_freq_time','rpttap_chan_freq_time'}
      freq.powspctrm = shiftdim(freq.powspctrm,1);  % wrap trial dim to end
      freq.dimord = 'chan_freq_time';
      if contains(listing(fn).name,'Baseline')
        freq.time = freq.time(1):mean(diff(freq.time)): ...
          (freq.time(end)*size(freq.powspctrm,4));  % extrapolate time axis out (probably longer than necessary)
        freq.powspctrm = reshape(freq.powspctrm, ...
          size(freq.powspctrm,1),size(freq.powspctrm,2),[]);  % concatenate all time into 1 trial
        freq.time = freq.time(1:size(freq.powspctrm,3));  % trim to correct number of samples
      else
        assert(size(freq.powspctrm,4) == 1, 'multiple trials not expected except during baseline')
      end
    case 'chan_freq_time'
      % okay, do nothing
    otherwise
      error('unexpected dimord')
  end
  
  if any(isnan(freq.powspctrm(:))) && ...             % when there are any NaNs,
      all(cellfun(@isempty,regexp(listing(fn).name, ...   % and the filename
      {'CS\+12ofConditioning\w*200\w*\.mat', ...      % doesn't match either of
      'CS\+12ofConditioning\w*338\w*\.mat'}, 'once')))  % these known exceptions
    warning('NaNs in converted powspctrm')
    figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
    keyboard  % pause to verify
  end
  % don't replace shock artifact with NaNs, as it will be easier to tell where
  % the artifact screws up the ridge detection without them (always drops to the
  % bottom few frequencies)
  
  %% detect frequency ridges
  
  stat = powridges_TEM([],freq);
  
  %% visualize & save plots
  
  t30 = find(stat.time - stat.time(1) > 30,1,'first');
  tnan = isnan(squeeze(stat.fridge(1,1,:)));
  clim = [0 10.*log10(median(max(stat.rpow(1,:,...
    [1:find(stat.time > 15,1,'first') find(stat.time > 45,1,'first'):end]),[],3)))];
  parameter = {'fridge','rpow','rprom','rtun'};
  figfiles = strrep(inputfile, ...
    [datafolder spectype filesep num2str(ratT.ratnums(r)) filesep], ...
    figurefolder);
  figfiles = strrep(figfiles, '.mat', ['_' spectype '_fRidge_chname']);
  rf = unique(2.^fix(log2(freq.freq)));   % rounded to next power of 2 closer to 2^0
  rfi = zeros(size(rf));
  for f = 1:numel(rf)
    [~,rfi(f)] = min(abs(freq.freq - rf(f)));  % rounded frequency index
  end
  wndx = zeros(numel(freq.freq),2);  % upper & lower bounds of bandwidth, as indices into freq.freq
  for f = 1:numel(freq.freq)
    [~,wndx(f,1)] = min(abs(freq.freq - (freq.freq(f) - stat.w(f))));
    [~,wndx(f,2)] = min(abs(freq.freq - (freq.freq(f) + stat.w(f))));
  end
  for ch = 1:numel(stat.label)
    paramchmat = [squeeze(stat.(parameter{1})(1,ch,~tnan)), squeeze(stat.(parameter{2})(1,ch,~tnan)), squeeze(stat.(parameter{3})(1,ch,~tnan)), squeeze(stat.(parameter{4})(1,ch,~tnan))];
    [R,P] = corrcoef(paramchmat);
    fig = figure(ch); clf; fig.WindowStyle = 'docked';
    subplot(numel(parameter)+2,3,1:3:numel(parameter)*3);
    [S,AX,BigAx,H,HAx] = plotmatrix(paramchmat);
    for param = 1:numel(parameter)
      H(param).NumBins = 15;
      AX(1,param).Title.String = parameter{param};
      AX(param,1).YLabel.String = parameter{param};
      for p2 = 1:numel(parameter)
        axis(AX(param,p2),'tight');
        AX(param,p2).FontSize = 8;
        if P(param,p2) < 0.025/(numel(stat.label)*numel(P)) % two-tailed w/ Bonferroni correction for multiple comparisons
          lgd = legend(S(param,p2),['R = ' num2str(R(param,p2),2)]);
          lgd.FontSize = 7;
          lgd.Location = 'northwest'; drawnow;
          lgd.Position = lgd.Position + [-0.026 0.0222 0 0];
        end
      end
      acax = subplot(numel(parameter)+2,3,param*3-1);
      autocorr(paramchmat(:,param),t30); axis tight
      acax.FontSize = 8;
      acax.XTick = linspace(0,t30,7);
      acax.XTickLabel = 0:5:30;
      acax.XLabel.String = 'Lag (s)';
      acax.YLabel.String = 'AutoCorr';
      acax.Title = [];
      tpax = subplot(numel(parameter)+2,3,param*3);
      plot(stat.time(~tnan),paramchmat(:,param)); axis tight
      tpax.FontSize = 8;
      if stat.time(end) < 70  % assume this is a tone-triggered trial
        tpax.XLabel.String = 'Time from Tone Onset (s)';
        tpax.XTick = -60:15:60;
      end
      tpax.YLabel.String = parameter{param};
    end
    spcax = subplot(numel(parameter)+2,3,numel(parameter)*3+1:(numel(parameter)+2)*3);  % figure;
    imagesc(freq.time,1:numel(freq.freq),10.*log10(squeeze(freq.powspctrm(ch,:,:))),clim);
    colormap(jet); colorbar; axis xy;
    spcax.FontSize = 10;
    spcax.YTick = rfi;
    spcax.YTickLabel = rf;  % converts to char, so this can't be used in place of rf
    spcax.YLabel.String = 'Frequency (Hz)';
    if stat.time(end) < 70  % assume this is a tone-triggered trial
      spcax.XLabel.String = 'Time from Tone Onset (s)';
      spcax.XTick = -60:15:60;
    end
    [~,fndx] = ismember(squeeze(stat.fridge(1,ch,~tnan)),freq.freq);
    hold all; plot(stat.time(~tnan),wndx(fndx,:),'w')
    print(strrep(figfiles, 'chname', stat.label{ch}),'-dpng')
  end
  
  %% save tfRidge file
  
  outputfile = strrep(inputfile,[filesep spectype filesep],...
    [filesep 'fRidge' filesep]);   % change folder and filename
  outputfile = strrep(outputfile,'.mat',['_' spectype '_fRidge.mat']);
  if existfile_TEM(outputfile)
    warning(['outputfile already exists: ' outputfile(numel(datafolder):end)])
    keyboard  % check before overwriting
  end
  
  disp(['saving stat to ' outputfile(numel(datafolder):end)])
  save(outputfile,'stat');
  
  %% clear freq to conserve memory
  
  clearvars freq stat
  
  %% update estimate of time to complete analysis
  
  comptime(fn) = toc(starttime);
  esttime = nanmean([esttime; comptime(fn)]); % average into estimate of how long each file should take
end

%% clean-up

output = comptime;
close(wb);

end
