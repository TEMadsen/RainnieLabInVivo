function do_behavior_OrigFear_TEM( r )
% DO_BEHAVIOR_ORIGFEAR_TEM compares freezing and barpressing with neural data
%
%   Loads freezing and barpressing data from .csv files and tests their
%   correlations with each neural metric in 30s BDA tone subtrials.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/9/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder ratT fsample stimuli phases 

%% create waitbar

wb = waitbar(0,['Preparing for behavior analysis on rat #' num2str(ratT.ratnums(r))]);

%% loop through behavioral measures

for b = {'freezing','barpress'}
  %% load behavior data into a timelock format & save

  %% run timelock stats on behavior data alone

  %% loop through neural measures
  
  for n = {'power','coherence','granger','CFC'}
    %% correlate behavioral data with neural metric


  end
end
