%% Visualize normalized time-frequency analysis results

close all; clearvars; clc;    % clean slate

info_fear                     % load all background info (filenames, etc)

foi = 1.05.^(0:95);         % freqs from 1-103 Hz progressing up by 5% each
toi = (1:(17+2*15)*3)/3+1/3;  % all tones + before & after times
plt = false;                  % if plots are desired for each tone and rat
errcnt = 0;                   % error count
errloc = [];                  % error location (r,p,t)

%% preallocate space to save data over rats/times (BDA each tone)

BLnormPow_mPFC = NaN(numel(foi),numel(toi),size(ratT,1));
BLnormPow_BLA = NaN(numel(foi),numel(toi),size(ratT,1));
BLnormCoh = NaN(numel(foi),numel(toi),size(ratT,1));

PTnormPow_mPFC = NaN(numel(foi),numel(toi)/3,size(ratT,1));
PTnormPow_BLA = NaN(numel(foi),numel(toi)/3,size(ratT,1));
PTnormCoh = NaN(numel(foi),numel(toi)/3,size(ratT,1));

%% use try-catch when running unsupervised batches

for r = 1:size(ratT,1)
  try
    t1 = 1;  % start before 1st Habituation tone for new rat (BLnorm)
    t2 = 1;  % start during 1st Habituation tone for new rat (PTnorm)
    
    %% gather baseline data
    baselinefile = [datafolder 'SpecCoherograms_' int2str(ratT.ratnums(r)) ...
      'Baseline.mat'];
    
    if ~exist(baselinefile,'file')
      error([baselinefile ' does not exist!']);
    end
    
    %% load data
    
    BL = load(baselinefile);
    
    %% check for accuracy
    
    if BL.freq.time(1) >= 0
      error('no pre-tone baseline found');
    else
      disp(['first timestamp is ' num2str(BL.freq.time(1))]);
    end
    
    if strcmp(goodchs{r}{1},'AD*')
      if numel(BL.freq.label) ~= (17 - numel(goodchs{r}))
        error('different channels used than planned');
      else
        for ch = 1:numel(BL.freq.label)
          k = strfind(goodchs{r}, BL.freq.label{ch});
          if any([k{:}])
            error([BL.freq.label{ch} ' should be excluded']);
          else
            disp([BL.freq.label{ch} ' is not excluded']);
          end
        end
      end
    elseif numel(BL.freq.label) ~= numel(goodchs{r}) || ...
        any(any(~strcmp(BL.freq.label,goodchs{r})))
      error('different channels used than planned');
    else
      disp([BL.freq.label{:} ' are correct']);   % still true
    end
    
    maxdiff = max(abs(BL.freq.freq-foi));
    if maxdiff > 0.02
      error([baselinefile ' does not have the same ' ...
        'frequency bins as foi']);
    else
      disp(['max difference from foi = ' num2str(maxdiff)]);
    end
    
    %% translate channel names into indices
    
    indx = NaN(size(BL.freq.labelcmb));
    
    for i = 1:size(BL.freq.labelcmb,1)
      % find 1st ch index
      indx(i,1) = find(strcmp(BL.freq.labelcmb(i,1),BL.freq.label));
      
      % find 2nd ch index
      indx(i,2) = find(strcmp(BL.freq.labelcmb(i,2),BL.freq.label));
    end
    
    %% calculate coherence
    
    % preallocate space
    BL.freq.coherence   = NaN(size(BL.freq.crsspctrm));
    
    for i = 1:size(indx,1)
      BL.freq.coherence(i,:,:) = abs(BL.freq.crsspctrm(i,:,:)./ ...
        sqrt(BL.freq.powspctrm(indx(i,1),:,:).* ...
        BL.freq.powspctrm(indx(i,2),:,:)));
    end
    
    %% average baseline data
    
    meanBLpow = mean(BL.freq.powspctrm,3,'omitnan');
    meanBLcoh = mean(BL.freq.coherence,3,'omitnan');
    
    %% normalize all other spec/coherograms by baseline
    
    for p = 1:numel(phases)
      if ~isempty(nexfile{r,p}) % no need for an error if there was no recording
        for tone = 1:17
          if tone > 15 && p > 1
            break % jumps past end of for tone loop
          end
          try
            inputfile  = [datafolder 'SpecCoherograms_' ...
              int2str(ratT.ratnums(r)) phases{p} int2str(tone) '.mat'];
            
            if ~exist(inputfile,'file')
              error([inputfile ' does not exist!']);
            end
            %% load data
            
            load(inputfile);
            
            %% check for accuracy
            
            if freq.time(1) >= 0
              error('no pre-tone baseline found');
            else
              disp(['first timestamp is ' num2str(freq.time(1))]);
            end
            
            if strcmp(goodchs{r}{1},'AD*')
              if numel(freq.label) ~= (17 - numel(goodchs{r}))
                error('different channels used than planned');
              else
                for ch = 1:numel(freq.label)
                  k = strfind(goodchs{r}, freq.label{ch});
                  if any([k{:}])
                    error([freq.label{ch} ' should be excluded']);
                  else
                    disp([freq.label{ch} ' is not excluded']);
                  end
                end
              end
            elseif numel(freq.label) ~= numel(goodchs{r}) || ...
                any(any(~strcmp(freq.label,goodchs{r})))
              error('different channels used than planned');
            else
              disp([freq.label{:} ' are correct']);   % still true
            end
            
            maxdiff = max(abs(freq.freq-foi));
            if maxdiff > 0.01
              error([inputfile ' does not have the same ' ...
                'frequency bins as foi']);
            else
              disp(['max difference from foi = ' num2str(maxdiff)]);
            end
            
            %% check input compatibility
            
            if numel(freq.labelcmb) ~= numel(BL.freq.labelcmb) || ...
                any(any(~strcmp(freq.labelcmb,BL.freq.labelcmb)))
              error([inputfile ' does not have the same ' ...
                'channel combinations as ' baselinefile]);
            end
            
            %% calculate coherence
            
            % preallocate space
            freq.coherence      = NaN(size(freq.crsspctrm));
            
            for i = 1:size(indx,1)
              freq.coherence(i,:,:) = abs(freq.crsspctrm(i,:,:)./ ...
                sqrt(freq.powspctrm(indx(i,1),:,:).* ...
                freq.powspctrm(indx(i,2),:,:)));
            end
            
            %% normalize by baseline period
            
            normfreq = freq;   % duplicate variable
            
            % normalization method 'db'
            normfreq.powspctrm = 10*log10(freq.powspctrm ./ ...
              repmat(meanBLpow,[1 1 numel(freq.time)]));
            
            % normalization method 'vssum'
            normfreq.coherence = (freq.coherence - repmat(meanBLcoh,...
              [1 1 numel(freq.time)])) ./ ...
              (freq.coherence + repmat(meanBLcoh,[1 1 numel(freq.time)]));
            
            %% plot raw, BLnorm, & PTnorm spectrograms & coherograms
            
            if plt
              %% display raw spectrograms & coherograms
              
              for target = 1:3 %#ok<UNRCH> can be reached if plt = true is set in top cell
                cfg                 = [];
                
                if target == 1
                  cfg.parameter       = 'powspctrm';
                  cfg.channel         = ratT.chmPFC{r};
                elseif target == 2
                  cfg.parameter       = 'powspctrm';
                  cfg.channel         = ratT.chBLA{r};
                else
                  cfg.parameter       = 'coherence';
                  cfg.zlim            = 'zeromax';
                  cfg.channel         = ratT.chmPFC{r};
                  cfg.refchannel      = ratT.chBLA{r};
                end
                
                if ~strcmp('none',cfg.channel) && ...
                    (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
                  figure(target);  clf;
                  ft_singleplotTFR(cfg, freq);
                  colormap(jet);
                  xlabel('Time from Tone Onset (seconds)');
                  ylabel('Frequency (Hz)');
                  
                  if target < 3
                    title(['Rat ' int2str(ratT.ratnums(r)) ', Tone #' ...
                      int2str(tone) ' of ' phases{p} ', ' ...
                      regions{target} ' Power']);
                    print([figurefolder int2str(ratT.ratnums(r)) ...
                      phases{p} int2str(tone) '_Spectrogram_' ...
                      regions{target} ],'-dpng');
                  else
                    title(['Rat ' int2str(ratT.ratnums(r)) ', Tone #' ...
                      int2str(tone) ' of ' phases{p} ', Coherence']);
                    print([figurefolder int2str(ratT.ratnums(r)) ...
                      phases{p} int2str(tone) '_Coherogram'],'-dpng');
                  end
                end
              end
              
              %% display baseline normalized spectrograms & coherograms
              
              for target = 1:3
                cfg                 = [];
                cfg.zlim            = 'maxabs';
                
                if target == 1
                  cfg.parameter       = 'powspctrm';
                  cfg.channel         = ratT.chmPFC{r};
                elseif target == 2
                  cfg.parameter       = 'powspctrm';
                  cfg.channel         = ratT.chBLA{r};
                else
                  cfg.parameter       = 'coherence';
                  cfg.channel         = ratT.chmPFC{r};
                  cfg.refchannel      = ratT.chBLA{r};
                end
                
                if ~strcmp('none',cfg.channel) && ...
                    (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
                  figure(target+3);  clf;
                  ft_singleplotTFR(cfg, normfreq);
                  colormap(jet);
                  xlabel('Time from Tone Onset (seconds)');
                  ylabel('Frequency (Hz)');
                  
                  if target < 3
                    title(['Rat ' int2str(ratT.ratnums(r)) ', Tone #' ...
                      int2str(tone) ' of ' phases{p} ', ' ...
                      regions{target} ' Power (dB change from baseline)']);
                    print([figurefolder int2str(ratT.ratnums(r)) ...
                      phases{p} int2str(tone) '_BLnormSpectrogram_' ...
                      regions{target} ],'-dpng');
                  else
                    title(['Rat ' int2str(ratT.ratnums(r)) ', Tone #' ...
                      int2str(tone) ' of ' phases{p} ...
                      ', Coherence (change from baseline)']);
                    print([figurefolder int2str(ratT.ratnums(r)) phases{p} ...
                      int2str(tone) '_BLnormCoherogram'],'-dpng');
                  end
                end
              end
              
              %% display pre-tone normalized spectrograms & coherograms
              
              for target = 1:3
                cfg                 = [];
                cfg.baseline        = [-30 0];
                cfg.zlim            = 'maxabs';
                
                if target == 1
                  cfg.baselinetype    = 'db';
                  cfg.parameter       = 'powspctrm';
                  cfg.channel         = ratT.chmPFC{r};
                elseif target == 2
                  cfg.baselinetype    = 'db';
                  cfg.parameter       = 'powspctrm';
                  cfg.channel         = ratT.chBLA{r};
                else
                  cfg.baselinetype    = 'vssum';
                  cfg.parameter       = 'coherence';
                  cfg.channel         = ratT.chmPFC{r};
                  cfg.refchannel      = ratT.chBLA{r};
                end
                
                if ~strcmp('none',cfg.channel) && ...
                    (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
                  figure(target+6);  clf;
                  ft_singleplotTFR(cfg, freq);
                  colormap(jet);
                  xlabel('Time from Tone Onset (seconds)');
                  ylabel('Frequency (Hz)');
                  
                  if target < 3
                    title(['Rat ' int2str(ratT.ratnums(r)) ', Tone #' ...
                      int2str(tone) ' of ' phases{p} ', ' ...
                      regions{target} ' Power (dB change from pre-tone)']);
                    print([figurefolder int2str(ratT.ratnums(r)) ...
                      phases{p} int2str(tone) '_PTnormSpectrogram_' ...
                      regions{target} ],'-dpng');
                  else
                    title(['Rat ' int2str(ratT.ratnums(r)) ', Tone #' ...
                      int2str(tone) ' of ' phases{p} ...
                      ', Coherence (change from pre-tone)']);
                    print([figurefolder int2str(ratT.ratnums(r)) phases{p} ...
                      int2str(tone) '_PTnormCoherogram'],'-dpng');
                  end
                end
              end
            end
            
            %% save BLnorm BDA tone data to display over trials
            
            imPFC = strcmp(ratT.chmPFC(r),freq.label);
            iBLA = strcmp(ratT.chBLA(r),freq.label);
            iC = sum([strcmp(ratT.chmPFC(r),freq.labelcmb) ...
              strcmp(ratT.chBLA(r),freq.labelcmb)],2) == 2;
            tB = freq.time<=0;
            tD = freq.time>0 & freq.time<=30;
            tA = freq.time>30;
            
            % before
            BLnormPow_mPFC(:,t1,r) = mean(normfreq.powspctrm(...
              imPFC,:,tB),3,'omitnan');
            BLnormPow_BLA(:,t1,r) = mean(normfreq.powspctrm(...
              iBLA,:,tB),3,'omitnan');
            BLnormCoh(:,t1,r) = mean(normfreq.coherence(...
              iC,:,tB),3,'omitnan');
            t1 = t1 + 1;
            
            % during
            BLnormPow_mPFC(:,t1,r) = mean(normfreq.powspctrm(...
              imPFC,:,tD),3,'omitnan');
            BLnormPow_BLA(:,t1,r) = mean(normfreq.powspctrm(...
              iBLA,:,tD),3,'omitnan');
            BLnormCoh(:,t1,r) = mean(normfreq.coherence(...
              iC,:,tD),3,'omitnan');
            t1 = t1 + 1;
            
            % after
            BLnormPow_mPFC(:,t1,r) = mean(normfreq.powspctrm(...
              imPFC,:,tA),3,'omitnan');
            BLnormPow_BLA(:,t1,r) = mean(normfreq.powspctrm(...
              iBLA,:,tA),3,'omitnan');
            BLnormCoh(:,t1,r) = mean(normfreq.coherence(...
              iC,:,tA),3,'omitnan');
            t1 = t1 + 1;  % move to next tone
            
            %% average pre-tone data
            
            meanPTpow = mean(freq.powspctrm(:,:,tB),3,'omitnan');
            meanPTcoh = mean(freq.coherence(:,:,tB),3,'omitnan');
            
            %% normalize by pre-tone period
            
            % normalization method 'db'
            normPTpowspctrm = 10*log10(freq.powspctrm ./ ...
              repmat(meanPTpow,[1 1 numel(freq.time)]));
            
            % normalization method 'vssum'
            normPTcoherence = (freq.coherence - repmat(meanPTcoh,...
              [1 1 numel(freq.time)])) ./ (freq.coherence ...
              + repmat(meanPTcoh,[1 1 numel(freq.time)]));
            
            %% save PTnorm during tone data to display over trials
            
            % during
            PTnormPow_mPFC(:,t2,r) = mean(normPTpowspctrm(...
              imPFC,:,tD),3,'omitnan');
            PTnormPow_BLA(:,t2,r) = mean(normPTpowspctrm(...
              iBLA,:,tD),3,'omitnan');
            PTnormCoh(:,t2,r) = mean(normPTcoherence(...
              iC,:,tD),3,'omitnan');
            t2 = t2 + 1;
            
          catch ME1
            errcnt = errcnt + 1;
            errloc(errcnt,:) = [r p tone];
            warning(['Error (' ME1.message ') while processing Rat # ' ...
              int2str(ratT.ratnums(r)) ', ' phases{p} ', Tone # ' ...
              int2str(tone) '! Continuing with next in line.']);
          end
          %% clear data for next file
          clearvars cfg freq normfreq normPT*
        end
      end
    end
  catch ME2
    errcnt = errcnt + 1;
    errloc(errcnt,:) = [r 1 0];   % phase 1, before tone baseline
    warning(['Error (' ME2.message ') while processing Rat # ' ...
      int2str(ratT.ratnums(r)) '! Continuing with next in line.']);
  end
  %% clear data for next rat
  clearvars BL
end

%% save aggregated data

save([datafolder 'meanBLnormSpecCoh.mat'], 'BLnorm*', '-v6');
save([datafolder 'meanPTnormSpecCoh.mat'], 'PTnorm*', '-v6');

%% use singleplotTFR to visualize mean BLnorm data

meanBLnorm.powspctrm(1,:,:) = mean(BLnormPow_mPFC,3,'omitnan');
meanBLnorm.powspctrm(2,:,:) = mean(BLnormPow_BLA,3,'omitnan');
meanBLnorm.powspctrm(3,:,:) = mean(BLnormCoh,3,'omitnan');
meanBLnorm.dimord = 'chan_freq_time';
meanBLnorm.freq = foi;
meanBLnorm.time = toi;
meanBLnorm.label = {'mPFC Power'; 'BLA Power'; 'Coherence'};

cfg                 = [];
cfg.zlim            = 'maxabs';
cfg.parameter       = 'powspctrm';

for c = 1:numel(meanBLnorm.label)
  cfg.channel = meanBLnorm.label{c};
  figure;
  ft_singleplotTFR(cfg, meanBLnorm);
  colormap(jet);
  xlabel('Tone #');
  ylabel('Frequency (Hz)');
%   title(['BLnorm ' meanBLnorm.label{c} ' for Rat #' int2str(ratT.ratnums(r)) ' Over Time']);
  title(['Mean BLnorm ' meanBLnorm.label{c} ' for All Rats Over Time']);
%   print([figurefolder int2str(ratT.ratnums(r)) 'BLnorm_' meanBLnorm.label{c}],'-dpng');
    print([figurefolder 'meanBLnorm_' meanBLnorm.label{c} '_all'],'-dpng');
end

%% use singleplotTFR to visualize mean PTnorm data

meanPTnorm.powspctrm(1,:,:) = mean(PTnormPow_mPFC,3,'omitnan');
meanPTnorm.powspctrm(2,:,:) = mean(PTnormPow_BLA,3,'omitnan');
meanPTnorm.powspctrm(3,:,:) = mean(PTnormCoh,3,'omitnan');
meanPTnorm.dimord = 'chan_freq_time';
meanPTnorm.freq = foi;
meanPTnorm.time = 1:(17+2*15);
meanPTnorm.label = {'mPFC Power'; 'BLA Power'; 'Coherence'};

cfg                 = [];
cfg.zlim            = 'maxabs';
cfg.parameter       = 'powspctrm';

for c = 1:numel(meanPTnorm.label)
  cfg.channel = meanPTnorm.label{c};
  figure;
  ft_singleplotTFR(cfg, meanPTnorm);
  colormap(jet);
  xlabel('Tone #');
  ylabel('Frequency (Hz)');
%   title(['PTnorm ' meanPTnorm.label{c} ' for Rat #' int2str(ratT.ratnums(r)) ' Over Time']);
  title(['Mean PTnorm ' meanPTnorm.label{c} ' for All Rats Over Time']);
%   print([figurefolder int2str(ratT.ratnums(r)) 'PTnorm_' meanPTnorm.label{c}],'-dpng');
  print([figurefolder 'meanPTnorm_' meanPTnorm.label{c} '_all'],'-dpng');
end

%% exclude "bad" rats from mean BLnorm data

meanBLnorm.powspctrm(1,:,:) = mean(BLnormPow_mPFC(:,:,~ratT.excluded),3,'omitnan');
meanBLnorm.powspctrm(2,:,:) = mean(BLnormPow_BLA(:,:,~ratT.excluded),3,'omitnan');
meanBLnorm.powspctrm(3,:,:) = mean(BLnormCoh(:,:,~ratT.excluded),3,'omitnan');
meanBLnorm.dimord = 'chan_freq_time';
meanBLnorm.freq = foi;
meanBLnorm.time = toi;
meanBLnorm.label = {'mPFC Power'; 'BLA Power'; 'Coherence'};

cfg                 = [];
cfg.zlim            = 'maxabs';
cfg.parameter       = 'powspctrm';

for c = 1:numel(meanBLnorm.label)
  cfg.channel = meanBLnorm.label{c};
  figure;
  ft_singleplotTFR(cfg, meanBLnorm);
  colormap(jet);
  xlabel('Tone #');
  ylabel('Frequency (Hz)');
  title(['Mean BLnorm ' meanBLnorm.label{c} ' for 5 Rats Over Time']);
  print([figurefolder 'meanBLnorm_' meanBLnorm.label{c} '_good'],'-dpng');
end

%% exclude "bad" rats from mean PTnorm data

meanPTnorm.powspctrm(1,:,:) = mean(PTnormPow_mPFC(:,:,~ratT.excluded),3,'omitnan');
meanPTnorm.powspctrm(2,:,:) = mean(PTnormPow_BLA(:,:,~ratT.excluded),3,'omitnan');
meanPTnorm.powspctrm(3,:,:) = mean(PTnormCoh(:,:,~ratT.excluded),3,'omitnan');
meanPTnorm.dimord = 'chan_freq_time';
meanPTnorm.freq = foi;
meanPTnorm.time = 1:(17+2*15);
meanPTnorm.label = {'mPFC Power'; 'BLA Power'; 'Coherence'};

cfg                 = [];
cfg.zlim            = 'maxabs';
cfg.parameter       = 'powspctrm';

for c = 1:numel(meanPTnorm.label)
  cfg.channel = meanPTnorm.label{c};
  figure;
  ft_singleplotTFR(cfg, meanPTnorm);
  colormap(jet);
  xlabel('Tone #');
  ylabel('Frequency (Hz)');
  title(['Mean PTnorm ' meanPTnorm.label{c} ' for 5 Rats Over Time']);
  print([figurefolder 'meanPTnorm_' meanPTnorm.label{c} '_good'],'-dpng');
end
