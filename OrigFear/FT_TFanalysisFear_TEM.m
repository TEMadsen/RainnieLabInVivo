%% Calculate raw spectrograms and coherograms for each trial

close all; clearvars; clc;  % clean slate

info_fear               % load all background info (filenames, etc)

foi = 1.05.^(-6:103);   % freqs from 0.75-150 Hz progressing up by 5% each
tapsmofrq = foi/20;             % smooth each band by +/- 5%
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
  tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;         % for 3 tapers (K=3), T=2/W
toi = -30:min(t_ftimwin)/2:60;    % time windows overlap by at least 50%
errcnt = 0;                       % error count
errloc = [];                      % error location (r,p,t)

%% use try-catch when running unsupervised batches

for r = 1:size(ratT,1)
  for p = 1:numel(phases)
    try
      if ~isempty(nexfile{r,p})
        % Tone-triggered trials with artifacts removed (except for fake
        % tone on/off artifacts).  Best for spectrograms & coherograms.
        inputfile = [datafolder 'CleanLFPTrialData_' ...
          int2str(ratT.ratnums(r)) phases{p} '.mat'];
        % Each tone will have its own output file.  Checking here for one
        % of the last tones.
        outputfile9 = [datafolder 'SpecCoherograms_' ...
          int2str(ratT.ratnums(r)) phases{p} '9.mat'];
        
        %% check existing data
        
        filecorrect = false; % start false in case file doesn't exist
        
        if exist(outputfile9,'file')
          filecorrect = true; % true unless demonstrated to be false
          disp(['Data for Rat # ' int2str(ratT.ratnums(r)) ', ' ...
            phases{p} ' already analyzed! Check for accuracy.']);
          load(outputfile9);
          
          if ~any(any(any(~isnan(freq.powspctrm))))
            warning('Only NaNs output from freqanalysis');
            filecorrect = false;
          end
          
          if freq.time(1) >= 0
            warning('no pre-tone baseline found');
            filecorrect = false;
          else
            disp(['first timestamp is ' num2str(freq.time(1))]);
          end
          
          if strcmp(goodchs{r}{1},'AD*')
            if numel(freq.label) ~= (numel(lftchs) + numel(rtchs) + 1 ...
                - numel(goodchs{r}))
              warning('different channels used than planned');
              filecorrect = false;  % reprocess this file
            else
              for ch = 1:numel(freq.label)
                k = strfind(goodchs{r}, freq.label{ch});
                if any([k{:}])
                  warning([freq.label{ch} ' should be excluded']);
                  filecorrect = false;  % reprocess this file
                  break  % jumps to end of for ch loop
                else
                  disp([freq.label{ch} ' is not excluded']);
                end
              end   % still true if no excluded channels
            end
          elseif numel(freq.label) ~= numel(goodchs{r}) || ...
              any(any(~strcmp(freq.label,goodchs{r})))
            warning('different channels used than planned');
            filecorrect = false;  % reprocess this file
          else
            disp([freq.label{:} ' are correct']);   % still true
          end
        end
        
        %% reprocess if necessary
        
        if filecorrect
          disp('checked file seems correct');
        else
          %% load data
          
          load(inputfile);
          
          %% loop through tones
          
          for t = unique(data.trialinfo(:,1))'
            try
              trials = find(data.trialinfo(:,1) == t);
              
              if isempty(trials)
                warning(['No trial #' int2str(t) ' found for ' ...
                  num2str(ratT.ratnums(r)) '''s ' phases{p}]);
              else
                disp(['Calculating spectrograms & coherograms for trial #' ...
                  int2str(t) ' of ' num2str(ratT.ratnums(r)) '''s ' ...
                  phases{p} ' (' int2str(numel(trials)) ' trials)']);
                
                %% calculate spectrograms & coherograms
                
                cfg             = [];
                
                cfg.method      = 'mtmconvol';
                cfg.output      = 'powandcsd';
                cfg.keeptrials  = 'no';
                cfg.trials      = trials;
                
                cfg.foi         = foi;
                cfg.tapsmofrq   = tapsmofrq;
                cfg.t_ftimwin   = t_ftimwin;
                cfg.toi         = toi;
                cfg.pad = 2^nextpow2(96000)/1000; % assumes longest trial is 96s
                
                cfg.outputfile  = [datafolder 'SpecCoherograms_' ...
                  int2str(ratT.ratnums(r)) phases{p} int2str(t) '.mat'];
                
                freq = ft_freqanalysis(cfg,data);
                
                if ~any(any(any(~isnan(freq.powspctrm))))
                  error('Only NaNs output from freqanalysis');
                end
              end
            catch ME1
              errcnt = errcnt + 1;
              errloc(errcnt,:) = [r p t]; %#ok<SAGROW>
              warning(['Error (' ME1.message ') while processing Rat # ' ...
                int2str(ratT.ratnums(r)) ', ' phases{p} ', Trial #' ...
                int2str(t) '! Continuing with next in line.']);
            end
          end
        end
        %% clear variables
        
        clearvars data cfg freq;
        
      end
    catch ME2
      errcnt = errcnt + 1;
      errloc(errcnt,:) = [r p 0]; %#ok<SAGROW> % not during a specific tone
      warning(['Error (' ME2.message ') while processing Rat # ' ...
        int2str(ratT.ratnums(r)) ', ' phases{p} ...
        '! Continuing with next in line.']);
    end
  end
end
