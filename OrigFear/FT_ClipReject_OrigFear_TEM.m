%% Removes clipping artifacts ONLY

info_origfear  % load all background info (filenames, etc)

cthresh = 3;  % clipping threshold in uV of the 2nd derivative of the data
redo = true;  % to ignore & overwrite old files
interactrat = true;   % to select visual artifacts per rat & review all channels after automated detection
interactch = true;   % to select visual artifacts per channel

%% preallocate variables that aggregate info across all rats

nancnt = cell(size(ratT,1), 1);  % rat cells will contain chndx x trl matrices

excludech = false(size(ratT,1), numel(allchs)); % initialize false, will be
% set to true if channel is missing from data or excluded based on artifacts

%% full loop

for r = find(~ratT.excluded)'
  % Raw LFP data divided into long, non-overlapping tone-triggered
  % trials, merged across all recordings for each rat
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'RawLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  % Clean LFP data with individual channel artifacts replaced with NaNs,
  % remerged to include all channels
  outputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllClean_clip' num2str(cthresh) ...
    '_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];
  
  %% check for output file
  if exist(outputfile,'file') && ~redo
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ' because ' ...
      outputfile ' already exists.'])
    continue
  end
  
  %% check for input data
  
  if ~exist(inputfile,'file')
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ' because ' ...
      inputfile ' was not found. Run FT_preproc_OrigFear_TEM.m first.'])
    continue
  end
  
  %% load input data
  
  disp(['Loading ' inputfile])
  data = rmvlargefields_TEM(inputfile);
  
  chfile = cell(size(allchs));
  subfile = cell(size(allchs));
  
%     d2dat = abs(diff(diff([data.trial{:}],1,2),1,2));
%     for chndx = 1:numel(data.label)
%       fig = figure(chndx); clf; fig.WindowStyle = 'docked';
%       histogram(movmax(d2dat(chndx,:),50),0:0.1:7)   % 3 uV is safest clipping threshold
%     end
  
  %% mark disconnection events
  
  if interactrat
    cfg = [];
    
    cfg = ft_databrowser(cfg,data);
    
    visual = cfg.artfctdef.visual.artifact;
  else
    visual = []; %#ok<UNRCH> due to setting in 1st section that may be altered
  end
  
  %% perform artifact detection on each channel separately
  
  for ch = 1:numel(allchs)
    %% find the channel name in this data, exclude if missing
    
    chndx = ismember(data.label, allchs{ch});
    if ~any(chndx)
      excludech(r, ch) = true;
      warning([allchs{ch} ' missing from file'])
      continue  % skip to next channel
    end

    %% check for output file
    
    subfile{ch} = [datafolder num2str(ratT.ratnums(r)) filesep ...
      'Clean_clip' num2str(cthresh) '_' ...
      allchs{ch} 'SubTrialData_' num2str(ratT.ratnums(r)) '.mat'];
    
    if exist(subfile{ch},'file') && ~redo
      warning(['Skipping rat #' num2str(ratT.ratnums(r)) ', channel ' ...
        allchs{ch} ' because ' subfile{ch} ' already exists.'])
      continue
    end
    
    %% divide data into channels
    
    artifact = [];
    cfg           = [];
    cfg.channel   = allchs{ch};
    
    data1ch = ft_selectdata(cfg,data);
    
    %% take 1st derivative of signal
    
    cfg = [];
    cfg.absdiff = 'yes';
    
    d1dat = ft_preprocessing(cfg,data1ch);
    % figure; histfit(movmax(abs(diff([d1dat.trial{:}])),50),2000,'gamma')
    
    %% define clipping artifacts
    
    cfg                                 = [];
    
    cfg.artfctdef.clip.channel          = 'AD*';
    cfg.artfctdef.clip.pretim           = 0.1;
    cfg.artfctdef.clip.psttim           = 0.1;
    cfg.artfctdef.clip.timethreshold    = 0.05;     % s
    cfg.artfctdef.clip.amplthreshold    = cthresh;  % uV
    
    [~, artifact.clip] = ft_artifact_clip(cfg,d1dat);
    
    % bugs in ft_artifact_clip add time before & after end of file, breaks
    % ft_rejectartifact or triggers bug in convert_event that breaks
    % ft_databrowser as noted below
    if artifact.clip(1,1) < 1
      artifact.clip(1,1) = 1;
    end
    endsamp = numel([d1dat.time{:}]);
    if artifact.clip(end,end) > endsamp
      artifact.clip(end,end) = endsamp;
    end
    
    %% review artifacts if needed
    
    cfg                               = [];
    cfg.artfctdef.clip.artifact       = artifact.clip;
    cfg.artfctdef.visual.artifact     = visual;
    
    if interactch
      % this conserves memory & makes it easier to jump to next artifact
      cfg.continuous = 'yes'; %#ok<UNRCH> due to setting in 1st section that may be altered   
      cfg.blocksize = 30;
      try
        cfg.event = data.cfg.previous{1, 1}.previous.previous.event;
        % note that events in remaining files may not be exact
        for fn = 2:numel(data.cfg.previous)
          if isfield(data.cfg.previous{fn}.previous.previous, 'event')
            tmpevt = struct2table(data.cfg.previous{fn}.previous.previous.event);
            tmpevt.sample = tmpevt.sample + cfg.event(end).sample + 1;
            tmpevt.timestamp = tmpevt.timestamp + cfg.event(end).timestamp ...
              + round(cfg.event(end).timestamp / cfg.event(end).sample);  % # of timestamps per sample
            cfg.event = [cfg.event; table2struct(tmpevt)];
          end
        end
      catch
        % events may not be present
      end
      
      % bug in convert_event, lines 180 & 181 should be swapped - prevents
      % plotting of all but the first large & jump artifacts - but it's easier
      % to just prevent any artifacts from including samples after the end!
      cfg = ft_databrowser(cfg,data1ch);
      
      artifact.visual   = cfg.artfctdef.visual.artifact;
      keyboard  % dbcont when satisfied
      % excludech(r, ch) = true;   % exclude this channel if desired
    end
    
    clearvars d1dat
    
    %% replace artifactual data with NaNs (for cross channel analyses)
    
    %     cfg.artfctdef.visual.artifact = artifact.visual;
    cfg.artfctdef.reject          = 'nan';
    cfg.artfctdef.minaccepttim    = 0.25;
    
    chfile{ch} = [datafolder num2str(ratT.ratnums(r)) filesep ...
      'Clean_clip' num2str(cthresh) '_' ...
      allchs{ch} 'TrialData_' num2str(ratT.ratnums(r)) '.mat'];
    
    cfg.outputfile                = chfile{ch};
    
    data1ch = ft_rejectartifact(cfg,data1ch);
    
    if numel(data1ch.trial) ~= numel(data.trial)  % if any trials were rejected completely
      excludech(r, ch) = true;  % exclude this channel
    end
    
    for tr = 1:numel(data.trial)
      nancnt{r}(ch,tr) = sum(isnan(data1ch.trial{tr}));
    end
    
    %% parse trials to remove NaNs (for individual channel analyses)
    
    cfg.artfctdef.reject          = 'partial';
    cfg.outputfile                = subfile{ch};
    
    data1ch = ft_rejectartifact(cfg,data1ch);
    
    %     keyboard  % dbcont when satisfied
    %     clearvars data1ch
  end   % for ch = 1:numel(data.label)
  
  %% remerge each channel file into one cleaned data file
  
  cfg = [];
  cfg.inputfile   = chfile(~excludech(r, :));
  cfg.outputfile  = outputfile;
  
  data = ft_appenddata(cfg);
  
  %% visualize result
  
  if interactrat
    cfg                               = [];
    
    cfg = ft_databrowser(cfg,data);
  end
end   % for r = find(~ratT.excluded)'

%% plot & save count of NaNs by rats & channels

for r = find(~ratT.excluded)'
  fig = figure(r); clf; fig.WindowStyle = 'docked';
  imagesc(nancnt{r})
end

save([datafolder 'Cleaned_clip' ...
  num2str(cthresh) '_nancnt' datestr(now,30) '.mat'],'nancnt');
