% merge channels

[sig007ts,I] = sort([sig007a;sig007b;sig007c;sig007d;sig007i]);
sig007wf = [sig007a_wf, sig007b_wf, sig007c_wf, sig007d_wf, sig007i_wf];
sig007wf = sig007wf(:,I);

save('S:\Teresa\PlexonData Backup\200\200_091006_0001_sig007.mat', ...
  'sig007wf', 'sig007ts');