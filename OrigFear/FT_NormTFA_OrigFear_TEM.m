%% Normalizes trial spectrograms & plots if desired
%%% old version that lists all filenames for no good reason %%%

CleanSlate  % provides a clean slate to start working with new data/scripts
% by clearing all variables, figures, and the command window

info_origfear            % load all background info & final parameters
errlog = cell(size(ratT,1), numel(allchs)); 	% MEs saved w/in nested cells
% rat x channel, then phase x tone #

%% to normalize only specific file(s), in a specific way

zthresh   = 15;       % which artifact threshold to use the data from
cthresh   = 3;        % and which clipping threshold
spectype  = 'wav';
% normtype = 'standard';  % based on FieldTrip's cfg.baselinetype = 'dB';
% normtype = 'z-score'; 	% my original proposal
normtype = 'z_dB'; % same as 'z-score', but using the familiar dB transform
redo      = false;    % to ignore pre-existing outputs and overwrite them
plt       = false;    % to plot & pause execution to check each step

%% full loop

for r = find(~ratT.excluded)'
%   try
    %% find available spectrogram files
    
    ratfolder = [datafolder num2str(ratT.ratnums(r)) filesep];
    filelist = what(ratfolder);
    
    %% preallocate variables
    
    pndx = cell(numel(filelist.mat),numel(phases));
    chndx = cell(numel(filelist.mat),numel(allchs));
    sndx = cell(numel(filelist.mat),numel(stimuli));
    
    %% find indices of relevant info in all filenames
    
    for p = 1:numel(phases)
      pndx(:,p) = strfind(filelist.mat,phases{p});
    end
    for ch = 1:numel(allchs)
      chndx(:,ch) = strfind(filelist.mat,allchs{ch});
    end
    for s = 1:numel(stimuli)            % need to keep this to find tone #
      sndx(:,s) = strfind(filelist.mat,stimuli{s});
    end
    tndx = strfind(filelist.mat,'of');  % to find tone #
    zndx = strfind(filelist.mat,['_z' num2str(zthresh) ...
      '_clip' num2str(cthresh) '_']);   % only finds desired files
    stndx = strfind(filelist.mat, spectype);  % only finds desired files
    ntndx = strfind(filelist.mat, normtype);  % only finds desired files
    
    %% put file info in a more legible format
    
    fileS = cell2struct(...
      [filelist.mat, pndx, chndx, sndx, tndx, zndx, stndx, ntndx], ...
      [{'filename'}; phases(:); allchs; ...
      {'CSplus'; 'tndx'; 'zndx'; 'stndx'; 'ntndx'}], 2);
    
    %% loop through channels
    
    for ch = 1:numel(allchs)
%       try
        %% preallocate cells for error log
        
        errlog{r,ch} = cell(numel(phases),max(expected));
        
        %% identify baseline file (where none of these are empty)
        
        filenums = find(~arrayfun(@(x) isempty(x.(allchs{ch})),fileS) & ...
          ~arrayfun(@(x) isempty(x.Baseline),fileS) & ...
          ~arrayfun(@(x) isempty(x.zndx),fileS) & ...
          ~arrayfun(@(x) isempty(x.stndx),fileS));
        
        %% stop here to avoid continue
        
        if numel(filenums) < 1
          errlog{r,ch}{4,1}.warning = ...   % phase = baseline (4), trial = 1
            ['No baseline freq file found for rat #' ...
            num2str(ratT.ratnums(r)) ', ' allchs{ch}];
          warning(errlog{r,ch}{4,1}.warning)
          continue % to next ch
        elseif numel(filenums) > 1
          errlog{r,ch}{4,1}.error = ...
            ['More than one baseline freq file found for rat #' ...
            num2str(ratT.ratnums(r)) ', ' allchs{ch}];
          error(errlog{r,ch}{4,1}.error)
        end   % else (numel(filenums) == 1), move on to next section
        
        %% load baseline freq file - hopefully faster to load once per rat
        % and channel than to use cfg.baselinefile with BLnorm
        
        baselinefile = [ratfolder fileS(filenums).filename];
        disp(['Loading baseline freq data for rat #' ...
          num2str(ratT.ratnums(r)) ', ' allchs{ch}])
        S = load(baselinefile,'freq');
        BLfreq = S.freq;
        S = [];
        
        if plt
          pow = squeeze(10*log10(BLfreq.powspctrm));
          meanpow = mean(pow,2,'omitnan');
          stdpow = std(pow,0,2,'omitnan');
          
          fig = figure(ch); clf; fig.WindowStyle = 'docked';
          ax1 = subplot(2,1,1);
          imagesc(BLfreq.time, 1:numel(BLfreq.freq), pow)
          axis xy; colormap(jet); colorbar;
          ax1.CLim = [min(meanpow-stdpow) max(meanpow+stdpow)];
          yticks(1:wav.res:numel(BLfreq.freq))
          yticklabels(cellstr(num2str(round(BLfreq.freq(1:wav.res:end)'))));
          title('raw baseline freq input (10*log10(uV^2/Hz))')
          
          ax2 = subplot(2,1,2);
          edges = linspace(floor(min(pow(:))), ceil(max(pow(:))), 100);
          N = zeros(numel(BLfreq.freq), numel(edges)-1);
          for f = 1:numel(BLfreq.freq)
            N(f,:) = histcounts(pow(f,:), edges, 'Normalization', 'probability');
          end
          imagesc(1:numel(BLfreq.freq), edges(2:end), N');
          axis xy; colormap(jet); colorbar;
          xticks(1:wav.res:numel(BLfreq.freq))
          xticklabels(cellstr(num2str(round(BLfreq.freq(1:wav.res:end)'))));
          hold all; errorbar(meanpow, stdpow, 'k');
          
          keyboard
        end
        
        %% check for all NaNs in any frequency
        
        if any(all(isnan(BLfreq.powspctrm),3))
          errlog{r,ch}{4,1}.warning = ...
            'At least one foi is all NaNs in baseline file.';
          warning(errlog{r,ch}{4,1}.warning)
          continue % to next ch
        end
        
        %% loop through all phases of fear conditioning & extinction
        
        for p = 1:3 % 1:numel(phases) w/o baseline
%           try
            %% find filenames including all identifying info desired
            filenums = find(~arrayfun(@(x) isempty(x.(allchs{ch})),fileS) & ...
              ~arrayfun(@(x) isempty(x.(phases{p})),fileS) & ...
              ~arrayfun(@(x) isempty(x.zndx),fileS) & ...
              ~arrayfun(@(x) isempty(x.stndx),fileS));  % spec type
            
            %% stop here to avoid continue
            
            if numel(filenums) < 1   % spectrograms don't exist for any tones in this phase
              errlog{r,ch}{p,max(expected)+1}.warning = ...
                ['No ' stimuli{s} ' freq files found during ' phases{p} ...
                ' for rat #' num2str(ratT.ratnums(r)) ', ' allchs{ch}];
              warning(errlog{r,ch}{p,max(expected)+1}.warning)
              continue % next stimtype
            end
            
            %% loop through all freq files, IDing tone #
            
            tonenum = NaN(size(filenums));
            for k = 1:numel(filenums)
              tonenum(k) = str2double(fileS(filenums(k)).filename(numel(stimuli{s})+1:...
                fileS(filenums(k)).tndx-1)); % number is between 'CS+' and 'of'
            end
            
            %% check for duplicates or too many trials
            
            if numel(tonenum) > numel(unique(tonenum))
              errlog{r,ch}{p,max(expected)+1}.warning = ['Duplicate ' ...
                stimuli{s} ' tone #s found during ' phases{p} ' for rat #' ...
                num2str(ratT.ratnums(r)) ', ' allchs{ch}];
              error(errlog{r,ch}{p,max(expected)+1}.warning)
              %                             continue
              %           keyboard  % select one? narrow criteria?
            end
            
            if max(tonenum) > expected(p)
              errlog{r,ch}{p,max(expected)+1}.warning = ...
                [stimuli{s} ' tone #s >' num2str(expected(p)) ...
                ' found during ' phases{p} ' for rat #' ...
                num2str(ratT.ratnums(r)) ', ' allchs{ch}];
              warning(errlog{r,ch}{p,max(expected)+1}.warning)
              %           continue
              %% alternate approach (examine data!)
              for tn = unique(tonenum)'
                %% load the raw freq file
                
                rawfile = [ratfolder ...
                  fileS(filenums(tonenum == tn)).filename];
                disp(['Loading trial freq data for ' stimuli{s} ' ' ...
                  num2str(tn) ' of ' phases{p} ' for rat #' ...
                  num2str(ratT.ratnums(r)) ', ' allchs{ch}])
                S = load(rawfile,'freq');
                freq = S.freq;
                S = [];
                
                %% plot spectrogram
                
                fig = figure(100+tn); clf; fig.WindowStyle = 'docked';
                imagesc(freq.time, 1:numel(freq.freq), squeeze(10*log10(freq.powspctrm)))
                axis xy
                title('rawfile freq input (10*log10(uv^2/Hz))')
                
              end
              keyboard  % select one? narrow criteria?
              %           %% this step automatically selects 9 tones/phase, verified okay for all diff4fear rats
              %           if p == 1 % for conditioning, last 9 tones are most important
              %             filenums(tonenum < max(tonenum)-8) = [];
              %             tonenum(tonenum < max(tonenum)-8) = [];
              %             tonenum = tonenum-min(tonenum)+1;
              %           else % otherwise, first 9 tones are most important
              %             filenums(tonenum > 9) = [];
              %             tonenum(tonenum > 9) = [];
              %           end
            end
            
            %% loop through tones, normalizing to baseline
            
            for tn = unique(tonenum)'
%               try
                %% define configuration params & check for normalized freq file
                
                cfg = [];
                cfg.parameter   = 'powspctrm';
                cfg.baseline    = 'BLfreq';
                cfg.tol         = tol;   % Hz diff btwn foi, set in info script
                
                switch normtype
                  case 'standard'  % based on FieldTrip's cfg.baselinetype = 'dB';
                    % decibel transform of power ratio
                    cfg.baselinecent  = 'mean';
                    cfg.baselinetype  = 'relative';
                    cfg.transtype     = 'dB';
                    cfg.transorder    = 'after';
                    
                  case 'z-score'  % my original proposal
                    cfg.baselinecent  = 'mean';
                    cfg.baselinetype  = 'z-score';
                    cfg.transtype     = 'ln';   % arbitrarily avoiding confusion with familiar dB
                    cfg.transorder    = 'before';
                    
                  case 'z_dB'  % my revised proposal
                    cfg.baselinecent  = 'mean';
                    cfg.baselinetype  = 'z-score';
                    cfg.transtype     = 'dB';   % capitalizes on familiarity of dB transform
                    cfg.transorder    = 'before';
                    
                  otherwise
                    error('You need to define this normtype here.')
                    
                end
                
                cfg.outputfile = [ratfolder normtype 'Norm' ...
                  stimuli{s} num2str(tn) 'of' phases{p} '_' ...
                  num2str(ratT.ratnums(r)) allchs{ch} '.mat'];
                
                %% stop here to avoid continue
                
                if exist(cfg.outputfile,'file')
                  errlog{r,ch}{p,tn}.warning = ...
                    ['Already calculated ' normtype ' normalized data for ' ...
                    stimuli{s} ' ' num2str(tn) ' of ' phases{p} ' for rat #' ...
                    num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                  warning(errlog{r,ch}{p,tn}.warning)
                  if ~redo
                    continue % to next tone
                  end
                end
                
                %% check for raw freq file
                
                k = filenums(tonenum == tn);
                
                if isempty(k)
                  errlog{r,ch}{p,tn}.warning = ...
                    ['No inputfile found for ' stimuli{s} ' #' tn ' of ' ...
                    phases{p} ' for rat #' num2str(ratT.ratnums(r)) ', ' ...
                    allchs{ch}];
                  warning(errlog{r,ch}{p,tn}.warning)
                  continue  % to next tone
                end
                
                %% load the raw freq file
                
                rawfile = [ratfolder ...
                  fileS(filenums(tonenum == tn)).filename];
                disp(['Loading trial freq data for ' stimuli{s} ' ' ...
                  num2str(tn) ' of ' phases{p} ' for rat #' ...
                  num2str(ratT.ratnums(r)) ', ' allchs{ch}])
                S = load(rawfile,'freq');
                freq = S.freq;
                S = [];
                
                %% stop here to avoid continue
                
                if all(isnan(freq.powspctrm(:)))
                  errlog{r,ch}{p,tn}.warning = ...
                    ['Spectrogram is all NaNs for ' stimuli{s} ' ' ...
                    num2str(tn) ' of ' phases{p} ' for rat #' ...
                    num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                  warning(errlog{r,ch}{p,tn}.warning)
                  continue % to next tone
                elseif any(all(isnan(freq.powspctrm),3),2) % keeping this separate so I can comment it out if necessary
                  errlog{r,ch}{p,tn}.warning = ...
                    ['Spectrogram has all NaNs for at least one frequency during ' ...
                    stimuli{s} ' ' num2str(tn) ' of ' phases{p} ...
                    ' for rat #' num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                  warning(errlog{r,ch}{p,tn}.warning)
                  continue % to next tone
                end
                
                %% verify that shocks occur on CS+ trials during Acquisition and only then
                
                if all(all(isnan(freq.powspctrm(1,:,abs(freq.time - 29.75) < 0.3)))) % 29.45-30.05 because there should be 50ms of artifact padding
                  if ~(p == 1 && tn > 10) % if there are artifacts at shock time, verify they're not shocks
                    errlog{r,ch}{p,tn}.warning = ...
                      ['Shock time is all NaNs for ' stimuli{s} ' ' ...
                      num2str(tn) ' of ' phases{p} ' for rat #' ...
                      num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                    warning(errlog{r,ch}{p,tn}.warning)
                    
                    fig = figure(100+tn); clf; fig.WindowStyle = 'docked';
                    imagesc(freq.time, 1:numel(freq.freq), squeeze(10*log10(freq.powspctrm)))
                    axis xy
                    title('dB transformed rawfile freq input (10*log10(uv^2/Hz))')
                    keyboard % visual inspection may be the easiest way to verify this (raw data would be more obvious)
                  end
                elseif p == 1 && tn > 10 % if there are no shock artifacts when there should be
                  errlog{r,ch}{p,tn}.warning = ...
                    ['Shock time is NOT all NaNs for ' stimuli{s} ' ' ...
                    num2str(tn) ' of ' phases{p} ' for rat #' ...
                    num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                  error(errlog{r,ch}{p,tn}.warning)
                  
                  %%% Shock time is NOT all NaNs for CS+ 11, 13-17 of
                  %%% Conditioning for rat #200, AD01 !!!
                  
                  %               keyboard  % reverse the stimuli
                end
                
                %% verify sufficiently similar foi to baseline
                
                if numel(freq.freq) ~= numel(BLfreq.freq)
                  errlog{r,ch}{p,tn}.warning = ...
                    ['Trial freq file contains ' numel(freq.freq) ...
                    ' of baseline freq''s ' numel(BLfreq.freq) ' foi'];
                  warning(errlog{r,ch}{p,tn}.warning)
                  continue % to next tone
                end
                
                if max(diff([freq.freq; BLfreq.freq],1,1)) >= ...
                    min(min(diff([freq.freq; BLfreq.freq],1,2)))/2 || ...
                    max(diff([freq.freq; BLfreq.freq],1,1)) >= cfg.tol
                  errlog{r,ch}{p,tn}.warning = ...
                    ['Trial freq foi differs from baseline freq foi by up to ' ...
                    num2str(max(diff([freq.freq; BLfreq.freq],1,1)))];
                  warning(errlog{r,ch}{p,tn}.warning)
                  continue % to next tone
                end
                
                %% normalize the raw freq file by baseline
                
                normfreq = BLnorm(cfg,freq,BLfreq);
                
                if plt
                  fig = figure(200 + tn); clf; fig.WindowStyle = 'docked';
                  ax1 = subplot(2,1,1);
                  imagesc(normfreq.time, 1:numel(normfreq.freq), squeeze(normfreq.powspctrm))
                  axis xy; colormap(jet); colorbar;
                  yticks(1:wav.res:numel(normfreq.freq))
                  yticklabels(cellstr(num2str(round(normfreq.freq(1:wav.res:end)'))));
                  title(['baseline normalized trial freq: ' normtype])
                  
                  ax2 = subplot(2,1,2);
                  edges = linspace(floor(min(normfreq.powspctrm(:))), ceil(max(normfreq.powspctrm(:))), 100);
                  N = zeros(numel(normfreq.freq), numel(edges)-1);
                  for f = 1:numel(normfreq.freq)
                    N(f,:) = histcounts(normfreq.powspctrm(1,f,:), edges, 'Normalization', 'probability');
                  end
                  imagesc(1:numel(normfreq.freq), edges(2:end), N');
                  axis xy; colormap(jet); colorbar;
                  xticks(1:wav.res:numel(normfreq.freq))
                  xticklabels(cellstr(num2str(round(normfreq.freq(1:wav.res:end)'))));
                  
                  keyboard % pause to check output
                end
                
%               catch ME
%                 errlog{r,ch}{p,tn}.error = ME;
%                 warning(errlog{r,ch}{p,tn}.error.message)
%               end
            end % tone
%           catch ME
%             errlog{r,ch}{p,max(expected)+1}.error = ME;
%             warning(errlog{r,ch}{p,max(expected)+1}.error.message)
%           end
        end % phase
%       catch ME
%         errlog{r,ch}.error = ME;
%         warning(errlog{r,ch}.error.message)
%       end % try
    end % channels
    %   for ch = 1:numel(allchs)
    %     fig = figure(ch);  fig.WindowStyle = 'docked';
    %     ax = gca; ax.CLim = [0 1e6];
    %   end
    %   keyboard % pause to review baseline freq inputs
    
%   catch ME
%     errlog{r,numel(allchs)+1}.error = ME;
%     warning(errlog{r,numel(allchs)+1}.error.message)
%   end % try
end % rat

%% save errlog

disp('Analysis complete!  Saving errlog.')
save([datafolder 'errlog' datestr(now,29) ...
  '_FT_NormTFA_OrigFear_TEM'],'errlog')
