function do_spikes_OrigFear_TEM( r )
% DO_SPIKES_ORIGFEAR_TEM analyses spiking activity alone & in relation to LFPs
%
%   Preprocesses spikes first.  Final sorted versions here:
% S:\Teresa\PlexonData Backup\spike sorting-Orig Fear\final\
%
%   Depends on global variables defined in info_origfear.
%
% written 6/9/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder ratT fsample stimuli phases 

%% create waitbar

wb = waitbar(0,['Preparing for spike analysis on rat #' num2str(ratT.ratnums(r))]);

%% Spikes alone		http://www.fieldtriptoolbox.org/tutorial/spike

% Read the spike data into MATLAB using 
ft_read_spike		
% Create a trial structure for the spike trains using using 
ft_read_event
ft_spike_maketrials
% Converting spike structure into continuous raw structure, and back, using 
ft_checkdata		
% Computation of inter spike interval distribution, and its visualization, using 
ft_spike_isi
ft_spike_plot_isireturn
% Computation of the mean and variance of the action potential waveform, using 
ft_spike_waveform
% Computation of peri stimulus time histogram, using 
ft_spike_psth
% Computation of spike-densities, using 
ft_spikedensity
% Visualization of spike trains, using 
ft_spike_plot_raster
% Computing average firing rates and correlations between neuronal firing rates, using 
ft_spike_rate
% Compute cross-correlation function between neurons using 
ft_spike_xcorr		
% Compute joint peri-stimulus time histograms, and their visualization, using 
ft_spike_jpsth
ft_spike_plot_jpsth

%% Spike-Field		http://www.fieldtriptoolbox.org/tutorial/spikefield

% Read the LFP and event data into MATLAB using 
ft_preprocessing
ft_definetrial		
% Read the spike data into MATLAB using 
ft_read_spike		
% Ensure that the spike representation contains trials, using either
ft_appendspike        % raw format or
ft_spike_maketrials   % spike format 
% For comparing LFPs and spikes recorded from the same electrode, run 
ft_spiketriggeredinterpolation
% Compute the spike triggered average on the raw and bandpass filtered LFP using 
ft_spiketriggeredaverage
% Compute the phase and power of the LFP at each time of spiking using 
ft_spiketriggeredspectrum
% Compute statistics on these instantaneous spike-LFP phases using 
ft_spiketriggeredspectrum_stat
