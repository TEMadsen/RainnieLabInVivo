function [output] = do_Spectrograms_OrigFear_TEM( r )
% DO_SPECTROGRAMS_ORIGFEAR_TEM quantifies spectrogram changes across t>0
% compared to t<0, one block of 3 trials at a time
%
%   This function is for within-subject visualization & stats using actvsbl
%   on the output of mtmconvol.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/20/17 by Teresa E. Madsen, Ph.D.

%% check for input

if nargin < 1
  info_origfear;  % metadata may not have been already loaded by master script
  r = 1;          % poster rat
end

%% declare global variables

global datafolder configfolder figurefolder ratT fsample stimuli phases  ...
  intervals blkedges blknames mainblocks expected

%% create waitbar

wb = waitbar(0, ['Preparing for spectrogram analysis on rat #' ...
  num2str(ratT.ratnums(r))]);

%% prepare_neighbours determines what sensors may form clusters

neighbfile = [configfolder 'neighb_' ratT.MWA{r} '_' ratT.side{r} '.mat'];

if existfile_TEM(neighbfile)
  load(neighbfile)
else
  cfg = [];
  cfg.method        = 'distance';
  cfg.neighbourdist = 1;  % mm
  cfg.elec          = [configfolder 'elec_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  neighbours        = ft_prepare_neighbours(cfg);
  if any(cellfun(@numel,{neighbours(:).neighblabel}) < 7)
    warning('unexpected # of neighbors')
    cfg = [];
    cfg.neighbours = neighbours;
    cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
    ft_neighbourplot(cfg)
    keyboard
  end
end

%% loop through main blocks, splitting at t=0 & merging trials

freq = cell(numel(mainblocks),3,2);   % 3 tones per block, baseline (t<0) vs. activation (t>0)
comptime = nan(numel(mainblocks),3);  % 3 steps:  prep data, run stats, plot results
esttime = [51.7    3539.3    24.2];   % each step's estimated comptime (in s) per block
    
for b = 1:numel(mainblocks)
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/sum(esttime.*numel(mainblocks)), wb, ...
    ['Preparing ' blknames{mainblocks(b)} ...
    ' data for spectrogram analysis on rat #' num2str(ratT.ratnums(r))]);
  
  tones = ((blkedges(mainblocks(b))+1):blkedges(mainblocks(b)+1));  % overall trial #s
  
  if b < 3
    p = 1;  % no adjustment for tone #s in 1st recording
  else
    p = b - 1;
    tones = tones - sum(expected(1:(p-1)));   % adjust for previous recordings
  end
  
  for tr = 1:numel(tones)
    %% define trial inputfile
    
    tn = tones(tr);   % trial number within phase
    
    inputfile = [datafolder 'mtmconvol' filesep num2str(ratT.ratnums(r)) ...
      filesep stimuli{1} num2str(tn) 'of' phases{p} '_ICA_' ...
      num2str(ratT.ratnums(r)) 'mtmconvolTFAfourier.mat'];
    
    %% load inputfile, if it exists
    
    if ~existfile_TEM(inputfile)
      warning(['Skipping tone ' num2str(tn) ' of ' phases{p} ...
        ' because inputfile (' inputfile(numel(datafolder):end) ') was not found.'])
      continue  % to next tone
    end
    
    freq{b,tr,1} = rmvlargefields_TEM(inputfile);
    t_ftimwin = freq{b,tr,1}.cfg.t_ftimwin;
    
    %% convert to powspctrm
   
    freq{b,tr,1} = ft_checkdata(freq{b,tr,1},'cmbrepresentation','sparsewithpow');
    
%     %% plot spectrograms
%     
%     cfg = [];
%     cfg.layout        = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
%     cfg.comment       = [stimuli{1} num2str(tn) 'of' phases{p}];
%     cfg.showscale     = 'no';
%     cfg.feedback      = 'no';
%     cfg.parameter     = 'powspctrm';
%     cfg.baseline      = [-inf -freq{b,tr,1}.cfg.t_ftimwin(1)];
%     cfg.baselinetype  = 'db';
%     fig = figure(sum(expected(1:(p-1)))+tn); clf; fig.WindowStyle = 'docked';
%     ft_multiplotTFR(cfg,freq{b,tr,1})
     
    %% db transform
    
    cfg = [];
    cfg.parameter = 'powspctrm';
    cfg.operation = '10*log10(x1)';
    
    freq{b,tr,1} = ft_math(cfg,freq{b,tr,1});
    
    %% split at t=0
    
    cfg = [];
    cfg.latency   = [0 inf];
    freq{b,tr,2} = ft_selectdata(cfg,freq{b,tr,1});
    freq{b,tr,2}.trialinfo = [tn 1 p 2];
    
    cfg.latency   = [-inf 0];
    freq{b,tr,1} = ft_selectdata(cfg,freq{b,tr,1});
    freq{b,tr,1}.trialinfo = [tn 1 p 1];
    
    assert(isequal(freq{b,tr,1}.dimord,freq{b,tr,2}.dimord,'chan_freq_time'))
    
    %% replace baseline powspctrm time/freq bins including data after t=0 w/ NaNs
    
    for f = 1:numel(freq{b,tr,1}.freq)
      badt = freq{b,tr,1}.time > -t_ftimwin(f);
      freq{b,tr,1}.powspctrm(:,f,badt) = NaN;
    end
    
    badt = squeeze(all(all(isnan(freq{b,tr,1}.powspctrm),1),2));
    freq{b,tr,1}.time = freq{b,tr,1}.time(~badt);
    freq{b,tr,1}.powspctrm = freq{b,tr,1}.powspctrm(:,:,~badt);
    
    %% replace acquisition powspctrm time/freq bins including shock time w/ NaNs
    
    if b == 2
      for f = 1:numel(freq{b,tr,2}.freq)
        badt = freq{b,tr,2}.time > 29.5 - t_ftimwin(f) & ...
          freq{b,tr,2}.time < 30 + t_ftimwin(f);
        freq{b,tr,2}.powspctrm(:,f,badt) = NaN;
      end
    end
    
    %%% try smoothing trial data??
    
    %% adjust to same time windows (need to match for stat test)
    
    assert(isequal(freq{b,tr,1}.trialinfo(:,1:3),freq{b,tr,2}.trialinfo(:,1:3)))
    if ~isequal(numel(freq{b,tr,1}.time),numel(freq{b,tr,2}.time))
      [ntime,shorter] = min([numel(freq{b,tr,1}.time),numel(freq{b,tr,2}.time)]);
      assert(shorter == 1);
      freq{b,tr,2}.time = freq{b,tr,2}.time(1:ntime);
      freq{b,tr,2}.powspctrm = freq{b,tr,2}.powspctrm(:,:,1:ntime);
      freq{b,tr,1}.time = freq{b,tr,2}.time;
    end
  end
  
  %% make sure all trials were done
  
  if any(cellfun(@isempty,freq(b,:)))
    warning(['Skipping ' blknames{mainblocks(b)} ' because not all inputfiles were found.'])
    continue  % to next blk
  end
  
  comptime(b,1) = toc(starttime);
  esttime(1) = nanmean([esttime(1); comptime(b,1)]); % average into estimate of how long each block should take
end

%% run within-subject stats on power spectrograms
% compares each time bin of active period to MEAN of all baseline time bins; so
% nonparametric stats won't help with the invalid measure of central tendency
% for the skewed baseline power data

stat = cell(numel(mainblocks),1);

for b = 1:numel(mainblocks)
  cfg = [];
  cfg.outputfile  = [datafolder 'Spectrograms' filesep num2str(ratT.ratnums(r)) ...
    filesep blknames{mainblocks(b)} '_ICA_' num2str(ratT.ratnums(r)) ...
    'stat_actvsblT_NPindiv_ordered.mat'];
  
  if existfile_TEM(cfg.outputfile)
    stat{b} = rmvlargefields_TEM(cfg.outputfile);
  else
    starttime = tic;  % only time computation, not just loading files
    
    cfg.statistic         = 'ft_statfun_actvsblT';
    
    waitbar(sum(comptime(:),'omitnan')/sum(esttime.*numel(mainblocks)), wb, ...
      ['Running ' cfg.statistic ' on ' blknames{mainblocks(b)} ...
      ' spectrograms for rat #' num2str(ratT.ratnums(r))]);
    
    cfg.method            = 'montecarlo';
    cfg.correctm          = 'cluster';
    cfg.clusteralpha      = 0.025;
    cfg.clusterstatistic  = 'maxsum';
    cfg.clusterthreshold  = 'nonparametric_individual';   % essential for reasonable thresholds!
    cfg.minnbchan         = 1;
    cfg.tail              = 0;
    cfg.clustertail       = 0;
    cfg.alpha             = 0.025;
    cfg.numrandomization  = 200;
    cfg.orderedstats      = 'yes';  % essential for reasonable thresholds!
    cfg.neighbours        = neighbours;
    cfg.parameter         = 'powspctrm';
    
    ntrls = size(freq,2);
    cfg.design  = [2*ones(1,ntrls), ones(1,ntrls); ... % 1 = active, 2 = baseline
      1:ntrls, 1:ntrls];
    
    cfg.ivar    = 1;
    cfg.uvar    = 2;
    
    stat{b} = ft_freqstatistics(cfg, freq{b,:});
    comptime(b,2) = toc(starttime);
    esttime(2) = nanmean([esttime(2); comptime(b,2)]); % average into estimate of how long each block should take
  end
end

%% print & plot (top 10 / nearly) significant differences in power spectrograms

for b = 1:numel(stat)
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/sum(esttime.*numel(mainblocks)), wb, ...
    ['Plotting results of ' stat{b}.cfg.statistic ' on ' blknames{mainblocks(b)} ...
    ' spectrograms for rat #' num2str(ratT.ratnums(r))]);
  
  pos = [];
  if isempty(stat{b}.posclusters)
    pos.n = 0;
    pos.I = [];  % no index because there are no clusters
  else
    [pos.p,pos.I] = sort([stat{b}.posclusters.prob]);
    pos.n = find(pos.p > 0.025,1,'first') - 1;
  end
  neg = [];
  if isempty(stat{b}.negclusters)
    neg.n = 0;
    neg.I = [];  % no index because there are no clusters
  else
    [neg.p,neg.I] = sort([stat{b}.negclusters.prob]);
    neg.n = find(neg.p > 0.025,1,'first') - 1;
  end
  bestch = zeros(pos.n+neg.n+2,1);
  disp([newline 'Found clusters with significant positive (' ...
    num2str(pos.n) ') and negative (' num2str(neg.n) ')' newline ...
    'changes from baseline during ' blknames{mainblocks(b)} newline])
  cfg =[];
  cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  cfg.parameter = 'stat';
  cfg.feedback  = 'no';
  cfg.gridscale = 2*numel(stat{b}.label) + 1;
  %%% NEED TO FIX THIS WITH A LOOP OR WHATEVER TO WORK ACROSS CELLS %%%
%   timedecBLfreq = quantile(freq{b,:,1}.powspctrm,9,4);   % individual timebins take too much memory to use in histograms below
  for n = 1:(pos.n + 1) % plot at least one that's not considered significant, if possible
    if n > 10 || numel(pos.I) < n
      break  % out of for loop
    end
    cI = pos.I(n);  % cluster index
    I = find(stat{b}.posclusterslabelmat == cI);
    [ch,f,t] = ind2sub(size(stat{b}.posclusterslabelmat),I);
    bestch(n) = mode(ch);
    ch = unique(ch); f = unique(f); t = unique(t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat{b}.freq(min(f)) stat{b}.freq(max(f))];
    trange = [stat{b}.time(min(t)) stat{b}.time(max(t))];
    %     voxpercActfreq = quantile(freq{b,2}.powspctrm(:,I),99,2);   % individual chan/freq/time bins take too much memory to use in histograms below
    fig = figure(b*100+n); clf; fig.WindowStyle = 'docked';
    % plot power value distributions within the cluster
%     subplot(1,2,1); hold all;
%     histogram(timedecBLfreq(:,ch,f,:), 50, 'Normalization','probability');
%     histogram(freq{b,:,2}.powspctrm(:,I), 50, 'Normalization','probability');
%     legend('Baseline','Activation')
    title(['Power Increase (p = ' num2str(stat{b}.posclusters(cI).prob) ...
      ') during ' blknames{mainblocks(b)} newline ...
      'in ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz, ' ...
      'from ' num2str(trange(1)) ' - ' num2str(trange(2)) ' s' newline ...
      'for ' strjoin(stat{b}.label(unique(ch)))]);
    
    % plot topographic extent of change in this time-freq cluster
    cfg.ylim = frange; cfg.xlim = trange;
%     subplot(1,2,2);
    ft_topoplotTFR(cfg,stat{b});
    
    fig.PaperPositionMode = 'auto';
    print([figurefolder 'PowerIncr' num2str(n) blknames{mainblocks(b)} ...
      '_ICA_' num2str(ratT.ratnums(r)) 'stat_actvsblT_NPindiv_ordered'],'-dpng','-r0')
    close(fig)
  end
  for n = 1:(neg.n + 1) % plot at least one that's not considered significant
    if n > 10 || numel(neg.I) < n
      break  % out of for loop
    end
    cI = neg.I(n);  % cluster index
    I = find(stat{b}.negclusterslabelmat == cI);
    [ch,f,t] = ind2sub(size(stat{b}.negclusterslabelmat),I);
    bestch(n) = mode(ch);
    ch = unique(ch); f = unique(f); t = unique(t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat{b}.freq(min(f)) stat{b}.freq(max(f))];
    trange = [stat{b}.time(min(t)) stat{b}.time(max(t))];
    %     voxpercActfreq = quantile(freq{b,2}.powspctrm(:,I),99,2);   % individual chan/freq/time bins take too much memory to use in histograms below
    fig = figure(b*100+n); clf; fig.WindowStyle = 'docked';
    % plot power value distributions within the cluster
%     subplot(1,2,1); hold all;
%     histogram(timedecBLfreq(:,ch,f,:), 50, 'Normalization','probability');
%     histogram(freq{b,:,2}.powspctrm(:,I), 50, 'Normalization','probability');
%     legend('Baseline','Activation')
    title(['Power Decrease (p = ' num2str(stat{b}.negclusters(cI).prob) ...
      ')' newline ...
      'in ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz, ' ...
      'from ' num2str(trange(1)) ' - ' num2str(trange(2)) ' s' newline ...
      'for ' strjoin(stat{b}.label(unique(ch)))])
    
    % plot topographic extent of change in this time-freq cluster
    cfg.ylim = frange; cfg.xlim = trange;
%     subplot(1,2,2);
    ft_topoplotTFR(cfg,stat{b});
    
    fig.PaperPositionMode = 'auto';
    print([figurefolder 'PowerDecr' num2str(n) blknames{mainblocks(b)} ...
      '_ICA_' num2str(ratT.ratnums(r)) 'stat_actvsblT_NPindiv_ordered'],'-dpng','-r0')
    close(fig)
  end
  % plot stat
  fig = figure(b*100); clf; fig.WindowStyle = 'docked';
  cfg=[];
  cfg.channel         = mode(bestch(bestch ~= 0));
  if isempty(cfg.channel);    cfg.channel = ratT.mPFC{r};     end
  cfg.zlim            = quantile(stat{b}.stat(:),[0.025 0.975]);
  cfg.parameter       = 'stat';
  cfg.maskparameter   = 'mask';
  cfg.maskstyle       = 'outline';
  cfg.feedback        = 'no';
  plotFTmatrix_TEM(cfg,stat{b});
  title([blknames{mainblocks(b)} ' During Tone vs. Before'])
  
  fig.PaperPositionMode = 'auto';
  print([figurefolder 'SigChangeSpectrogram_' blknames{mainblocks(b)} ...
    '_ICA_' num2str(ratT.ratnums(r)) 'stat_actvsblT_NPindiv_ordered'],'-dpng','-r0')
  close(fig)
  
  comptime(b,3) = toc(starttime);
  esttime(3) = nanmean([esttime(3); comptime(b,3)]); % average into estimate of how long each block should take
end
output = comptime;
close(wb);
end
