function [output] = do_bandBDAsum_OrigFear_TEM( spectype, r )
% DO_BANDBDASUM_ORIGFEAR_TEM reduces TFA to sum of raw power per freq band BDA
% each tone
%
%   Depends on global variables defined in info_origfear & bands defined by
%   get_cfg_TEM('OrigFear','bandify') 
%
% written 9/5/17 by Teresa E. Madsen, Ph.D.

%% import global metadata structure

global exptMeta

if isempty(exptMeta);   info_origfear;   end

%% check inputs & assign defaults

if nargin == 0  % like when I just hit "Run and Time" in the Editor tab
  spectype  = 'piWav';  % close to FT default wavelet params, but using pi for integer # of cycles, etc.
  r         = 1;        % poster rat
end

%% create waitbar

wb = waitbar(0, ['Preparing to reduce powspctrm to 12 freq bands BDA tones on rat #' ...
  num2str(exptMeta.ratT.ratnums(r))]);

%% loop through all .mat files in spectype folder

listing = dir([exptMeta.datafolder spectype filesep ...
  num2str(exptMeta.ratT.ratnums(r)) filesep '*fourier.mat']);
if numel(listing) > sum(exptMeta.expected)
  warning('more files than expected - check for duplicates')
  keyboard
end
comptime  = nan(size(listing));     % for each file
esttime   = 50;                     % estimated comptime (s) per file
% esttime = nanmean(comptime);      % average of past calculations
output    = nan(numel(listing),2);  % skewness before/after dB conversion

for fn = 1:numel(listing)
  if contains(listing(fn).name,'Baseline')
    continue  % skip to next file
  end
  
  starttime = tic;
  waitbar(fn/numel(listing), wb, ...
    ['blockifying freq & time axes of ' listing(fn).name]);
  
  %% define input & output files, skipping trial if already complete
  
  inputfile = [listing(fn).folder filesep listing(fn).name];
  
  % pow converted version:
  outputfile1 = replace(inputfile, exptMeta.phases, exptMeta.abbr);
  outputfile1 = strrep(outputfile1,'_raw_','_raw');
  outputfile1 = strrep(outputfile1,'waveletTFAfourier',[spectype 'TFApow']);
  
  % blockified version:
  outputfile2 = strrep(outputfile1,[filesep spectype filesep],...
    [filesep 'bandBDAsum' filesep]);   % change folder and filename
  outputfile2 = strrep(outputfile2, '.mat', '_bandBDAsum.mat');
  
  if existfile_TEM(outputfile2)
    warning(['outputfile already exists: ' ...
      outputfile2(numel(exptMeta.datafolder):end)])
    continue  % skip to next file
  end
  
  %% skip to pow version if already saved, otherwise, load fourier version
  
  if existfile_TEM(outputfile1)
    freq = rmvlargefields_TEM(outputfile1);
  else
    freq = rmvlargefields_TEM(inputfile);
    
    %% convert to powspctrm & check dimord
    
    freq = ft_checkdata(freq,'cmbrepresentation','sparsewithpow');
    
    switch freq.dimord
      case 'rpt_chan_freq_time'
        freq.powspctrm = shiftdim(freq.powspctrm,1);  % wrap trial dim to end
        freq.dimord = 'chan_freq_time';
        if contains(listing(fn).name,'Baseline')
          freq.time = freq.time(1):mean(diff(freq.time)): ...
            (freq.time(end)*size(freq.powspctrm,4));  % extrapolate time axis out (probably longer than necessary)
          freq.powspctrm = reshape(freq.powspctrm, ...
            size(freq.powspctrm,1),size(freq.powspctrm,2),[]);  % concatenate all time into 1 trial
          freq.time = freq.time(1:size(freq.powspctrm,3));  % trim to correct number of samples
        else
          assert(size(freq.powspctrm,4) == 1, 'multiple trials not expected except during baseline')
        end
      case 'chan_freq_time'
        % okay, do nothing
      otherwise
        error('unexpected dimord')
    end
  end
  
  %% check for NaNs
  
  if any(isnan(freq.powspctrm(:))) && ...             % when there are any NaNs,
      all(cellfun(@isempty,regexp(listing(fn).name, ...   % and the filename
      {'CS\+12of\w*C\w*200\w*\.mat', ...              % doesn't match either of
      'CS\+12of\w*C\w*338\w*\.mat'}, 'once')))        % these known exceptions
    warning('NaNs in converted powspctrm')
    figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
    keyboard  % pause to verify
  end
  
  %% replace acq powspctrm time/freq bins including shock artifact w/ NaNs
  
  wavlen = ft_findcfg(freq.cfg, 'wavlen');
  if ~isempty(regexp(listing(fn).name,'CS\+1[1-7]of\w*C\w*\.mat','once'))   % Conditioning or FC are the only phase names or abbreviations with capital C
    for f = 1:numel(freq.freq)
      badt = freq.time > 29.5 - wavlen(f)/2 & ...
        freq.time < 30 + wavlen(f)/2;   % so no time bins will include shock time
      freq.powspctrm(:,f,badt) = NaN;
    end
  end
  % figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
  
  %% blockify time & freq axes
  
  bcfg = get_cfg_TEM('bandify','OrigFear');  % standard bands based on powers 
  % of e + transitional bands between them (half-powers of e)
  
  blkfreq = freq;
  blkfreq.time = [-15 15 45];   % center times of before/during/after tone
  blkfreq.freq = bcfg.foi;
  blkfreq.powspctrm = nan(size(freq.powspctrm,1), ...
    numel(blkfreq.freq), numel(blkfreq.time));
  
  for f = 1:numel(blkfreq.freq)  % use median to preserve sharp changes & avoid biasing by skew,
    fndx = freq.freq > blkfreq.freq(f) - bcfg.w(f) & ...
      freq.freq < blkfreq.freq(f) + bcfg.w(f);
    disp([bcfg.bands{f} ' includes ' num2str(sum(fndx)) ' frequency bins'])
    for t = 1:numel(blkfreq.time)
      tndx = abs(freq.time-blkfreq.time(t)) < bcfg.winlen/2;   % +/-15s
      if ~any(tndx)
        warning('no times selected')
        keyboard
      end
      blkfreq.powspctrm(:,f,t) = sum(...
        reshape(freq.powspctrm(:,fndx,tndx), size(freq.powspctrm,1),1,[]), ...
        3, 'omitnan');
    end  
    % for ch = 1:numel(smthfreq.label)
    % fig = figure; fig.WindowStyle = 'docked';
    % subplot(1,2,1); histfit(squeeze(freq.powspctrm(ch,f,:)));
    % title(['f = ' num2str(round(freq.freq(f),3,'significant')) ' Hz, skew = ' num2str(skewness(squeeze(freq.powspctrm(ch,f,:)))) ', before smoothing'])
    % subplot(1,2,2); autocorr(squeeze(freq.powspctrm(ch,f,:)),find(freq.time - freq.time(1) > 30,1,'first'))
    % fig = figure; fig.WindowStyle = 'docked';
    % subplot(1,2,1); histfit(squeeze(smthfreq.powspctrm(ch,f,:)));
    % title(['f = ' num2str(round(smthfreq.freq(f),3,'significant')) ' Hz, skew = ' num2str(skewness(squeeze(smthfreq.powspctrm(ch,f,:)))) ', after smoothing'])
    % subplot(1,2,2); autocorr(squeeze(smthfreq.powspctrm(ch,f,:)),30)
    % end
  end
  % figure; plotFTmatrix_TEM(struct('zlim','zeromax'),smthfreq)
   
  %% save smoothed file
  
  freq = blkfreq; %#ok<NASGU> saved to file
  disp(['saving freq to ' outputfile2(numel(exptMeta.datafolder):end)])
  save(outputfile2,'freq');
  
  %% clear freq to conserve memory
  
  clearvars freq blkfreq
  
  %% update estimate of time to complete analysis
  
  comptime(fn) = toc(starttime);
  esttime = nanmean([esttime; comptime(fn)]); % average into estimate of how long each file should take
end

%% clean up

if all(isnan(output(:)))  % no skew data was collected
  output = comptime;  % return comptime instead
end
close(wb);

end   % function
