function [output] = do_coherograms_OrigFear_TEM( r )
% DO_COHEROGRAMS_ORIGFEAR_TEM quantifies coherogram differences across blocks
%
%   This function is for within-subject visualization & stats using
%   ft_statfun_indepsamplesZcoh on the output of mtmconvol.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/22/17 by Teresa E. Madsen, Ph.D.

%% check for input

if nargin < 1
  info_origfear;  % metadata may not have been already loaded by master script
  r = 1;          % poster rat
end

%% declare global variables

global datafolder configfolder ratT fsample stimuli phases intervals ...
  blkedges blknames mainblocks expected

%% create waitbar

wb = waitbar(0,['Preparing for coherogram analysis on rat #' ...
  num2str(ratT.ratnums(r))]);

%% loop through main blocks, loading fourier data & masking shock times

freq = cell(numel(mainblocks),4);   % trials will stay in separate blocks
freqcoh = cell(numel(mainblocks),4);  % coherograms for plotting
comptime = nan(numel(mainblocks),3);  % prep data, run stats, plot results
exptime = [32.1,1675.7,4.4];   % expected computation time per block, in seconds
% guess based on Spectrograms's time, will be updated by real comptime

for b = 1:numel(mainblocks)
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/...
    sum([exptime(1)*numel(mainblocks) exptime(2:3)]), wb, ...
    ['Preparing ' blknames{mainblocks(b)} ...
    ' data for coherogram analysis on rat #' num2str(ratT.ratnums(r))]);
  
  tones = ((blkedges(mainblocks(b))):blkedges(mainblocks(b)+1));  % overall trial #s
  % including one trial earlier than original definition of these blocks
  
  if b < 3
    p = 1;  % no adjustment for tone #s in 1st recording
  else
    p = b - 1;
    tones = tones - sum(expected(1:(p-1)));   % adjust for previous recordings
    if b == 3
      tones = tones + 1;  % want original block & one AFTER rather than before
    end
  end
  
  for tr = 1:numel(tones)
    %% define trial inputfile
    
    tn = tones(tr);   % trial number within phase
    
    %% load raw mtmconvol output
    
    inputfile = [datafolder 'mtmconvol' filesep num2str(ratT.ratnums(r)) ...
      filesep stimuli{1} num2str(tn) 'of' phases{p} '_ICA_' ...
      num2str(ratT.ratnums(r)) 'mtmconvolTFAfourier.mat'];
    
    freq{b,tr} = rmvlargefields_TEM(inputfile);
    assert(strcmp(freq{b,tr}.dimord,'rpttap_chan_freq_time'),'unexpected dimord')
    t_ftimwin = freq{b,tr}.cfg.t_ftimwin;
    
    %% replace acquisition cohspctrm time/freq bins including shock time w/ NaNs
    
    if b == 2
      for f = 1:numel(freq{b,tr}.freq)
        badt = freq{b,tr}.time > 29.5 - t_ftimwin(f) & ...
          freq{b,tr}.time < 30 + t_ftimwin(f);
        freq{b,tr}.fourierspctrm(:,:,f,badt) = NaN;
      end
    end
    
    %% convert fourier to cohspctrm (abs) & save
    
    cfg = [];
    cfg.method  = 'coh';
    cfg.complex = 'abs';
    cfg.outputfile = [datafolder 'coherograms' filesep num2str(ratT.ratnums(r)) ...
      filesep stimuli{1} num2str(tn) 'of' phases{p} '_ICA_' ...
      num2str(ratT.ratnums(r)) 'mtmconvolTFAcoh.mat'];
    
    freqcoh{b,tr} = ft_connectivityanalysis(cfg,freq{b,tr});
    
%     %% plot coherogram
%     
%     cfg = [];
%     cfg.parameter     = 'cohspctrm';
%     cfg.comment       = [stimuli{1} num2str(tn) 'of' phases{p}];
%     cfg.colormap      = jet;
%     cfg.feedback      = 'no';
%     
%     fig = figure(sum(expected(1:(p-1)))+tn); clf; fig.WindowStyle = 'docked';
%     ft_connectivityplot(cfg, freqcoh{b,tr})
    
    %%% try smoothing trial data??
    
  end
  comptime(b,1) = toc(starttime);
  exptime(1) = nanmean([exptime(1); comptime(b,1)]); % average into estimate of how long each block should take
end

%% run within-subject stats on fourier representations

ntrls = size(freq,2);

cfg = [];
cfg.outputfile  = [datafolder 'coherograms' filesep num2str(ratT.ratnums(r)) ...
  filesep 'Compare' num2str(ntrls) 'perBlock_ICA_' ...
  num2str(ratT.ratnums(r)) 'stat_indepsZcoh_NPindiv_ordered.mat'];

if existfile_TEM(cfg.outputfile)
  stat = rmvlargefields_TEM(cfg.outputfile);
else
  starttime = tic;  % only time computation, not just loading files
  
  cfg.statistic         = 'ft_statfun_indepsamplesZcoh';
  
  waitbar(sum(comptime(:),'omitnan')/...
    sum([exptime(1)*numel(mainblocks) exptime(2:3)]), wb, ...
    ['Running ' cfg.statistic ' on ' blknames{mainblocks(b)} ...
    ' coherograms for rat #' num2str(ratT.ratnums(r))]);
  
  cfg.parameter         = 'fourierspctrm';
  cfg.method            = 'montecarlo';
  cfg.correctm          = 'cluster';
  cfg.neighbours        = [];   % will cluster based on time and freq only
  cfg.clusteralpha      = 0.025;
  cfg.clusterstatistic  = 'maxsum';
  cfg.clusterthreshold  = 'nonparametric_individual';   % essential for reasonable thresholds!
  cfg.minnbchan         = 1;
  cfg.tail              = 0;
  cfg.clustertail       = 0;
  cfg.alpha             = 0.025;
  cfg.numrandomization  = 200;
  cfg.orderedstats      = 'yes';  % essential for reasonable thresholds!
  
  design  = [ones(ntrls,1), 2*ones(ntrls,1), 3*ones(ntrls,1), 4*ones(ntrls,1)];   % block #s
  cfg.design = design(:);
  cfg.ivar    = 1;  % UO is trials, not matched across blocks (i.e., no uvar)
  
  stat = ft_freqstatistics(cfg, freq{:});
  %%% ERROR/
%   Assignment has more non-singleton rhs dimensions than non-singleton
% subscripts
% Error in ft_statistics_montecarlo (line 318)
%       statrand(:,i) = dum.stat;
% Error in ft_freqstatistics (line 193)
%     [stat, cfg] = statmethod(cfg, dat, design);
% Error in do_coherograms_OrigFear_TEM (line 152)
%   stat = ft_freqstatistics(cfg, freq{:});
% Error in fullFTpipeline_OrigFear_TEM (line 67)
%         outputs{r,s} = fun(r); 
% 318       statrand(:,i) = dum.stat;
% size(statrand) =>   21      200
% size(dum.stat) =>   21  3382016
% dum is output of ft_statfun_indepsamplesZcoh(cfg, tmpdat, tmpdesign);
% uses dim(1) = 7 as nchan, when it's really rpttap, maybe needs wav rather than
% multitaper
  %%% /ERROR
  comptime(1,2) = toc(starttime);
  exptime(2) = nanmean([exptime(2); comptime(1,2)]); % average into estimate of how long each block should take
end

%% print & plot (nearly) significant differences in coherograms

starttime = tic;
waitbar(sum(comptime(:),'omitnan')/...
  sum([exptime(1)*numel(mainblocks) exptime(2:3)]), wb, ...
  ['Plotting results of ' stat.cfg.statistic ' on coherograms for rat #' ...
  num2str(ratT.ratnums(r))]);

npos = sum([stat.posclusters.prob] < 0.05);
nneg = sum([stat.negclusters.prob] < 0.05);
bestch = zeros(npos+nneg+2,1);
disp([newline 'Found clusters with significant positive (' ...
  num2str(npos) ') and negative (' num2str(nneg) ')' newline ...
  'differences in coherograms by block' newline])
cfg =[];
cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
cfg.parameter = 'stat';
cfg.feedback  = 'no';
timeavgBLfreq = nanmean(freqcoh{b,:}.cohspctrm,4);   % individual timebins take too much memory to use in histograms below
for n = 1:(npos + 1) % plot at least one that's not considered significant
  I = find(stat.posclusterslabelmat == n);
  [ch,f,t] = ind2sub(size(stat.posclusterslabelmat),I);
  bestch(n) = mode(ch);
  frange = [stat.freq(min(f)) stat.freq(max(f))];
  trange = [stat.time(min(t)) stat.time(max(t))];
  
  fig = figure(b*100+n); clf; fig.WindowStyle = 'docked';
  % plot value distributions within the cluster
  subplot(1,2,1); hold all;
  histogram(timeavgBLfreq(:,ch,f), 100, 'Normalization','probability');
  histogram(freq{b}.fourierspctrm(:,I), 100, 'Normalization','probability');
  legend('Baseline','Activation')
  
  % plot topographic extent of change in this time-freq cluster
  cfg.ylim = frange; cfg.xlim = trange; subplot(1,2,2);
  ft_topoplotTFR(cfg,stat);
  title(['Significant increase (p = ' num2str(stat.posclusters(n).prob) ...
    ')' newline ...
    'in ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz Power ' ...
    'from ' num2str(trange(1)) ' - ' num2str(trange(2)) ' s' newline ...
    'for ' strjoin(stat.label(unique(ch)))])
end
for n = 1:(nneg + 1) % plot at least one that's not considered significant
  I = find(stat.negclusterslabelmat == n);
  [ch,f,t] = ind2sub(size(stat.negclusterslabelmat),I);
  bestch(npos+1+n) = mode(ch);
  frange = [stat.freq(min(f)) stat.freq(max(f))];
  trange = [stat.time(min(t)) stat.time(max(t))];
  
  fig = figure(b*100+1+npos+n); clf; fig.WindowStyle = 'docked';
  % plot value distributions within the cluster
  subplot(1,2,1); hold all;
  histogram(timeavgBLfreq(:,ch,f), 100, 'Normalization','probability');
  histogram(freq{b}.fourierspctrm(:,I), 100, 'Normalization','probability');
  legend('Baseline','Activation')
  
  % plot topographic extent of change in this time-freq cluster
  cfg.ylim = frange; cfg.xlim = trange; subplot(1,2,2);
  ft_topoplotTFR(cfg,stat);
  title(['Significant decrease (p = ' num2str(stat.negclusters(n).prob) ...
    ')' newline ...
    'in ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz Power ' ...
    'from ' num2str(trange(1)) ' - ' num2str(trange(2)) ' s' newline ...
    'for ' strjoin(stat.label(unique(ch)))])
end
% plot stat
fig = figure(b*100); clf; fig.WindowStyle = 'docked';
cfg=[];
cfg.channel         = mode(bestch); % ratT.mPFC{r};
cfg.zlim            = stat.cfg.clustercritval;
cfg.parameter       = 'stat';
cfg.maskparameter   = 'mask';
cfg.maskstyle       = 'outline';
cfg.feedback        = 'no';
plotFTmatrix_TEM(cfg,stat);
title([blknames{mainblocks(b)} ' During Tone vs. Before'])

comptime(1,3) = toc(starttime);
output = comptime;
close(wb);
end
