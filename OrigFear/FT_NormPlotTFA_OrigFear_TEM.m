%% Normalize spectrograms & coherograms to baseline, average blocks and plot
% took 30.6 hours for power & coherence normalization of mtmfft2s spectra for
% 8.5/12 rats (no averaging blocks of tones together), 11.646 hrs for power
% normalization & averaging of mtmconvol spectra for 10/11.5 rats (no coherence)

% Results (mtmfft) still seem to have a bit of the 1/f dropoff, even though they
% should all be centered on zero with similar variance now, so this needs some
% troubleshooting.  Will do as I normalize mtmconvol data.

% ^that's less obvious in mtmconvol data, so I think it's probably due to a
% highly skewed distribution of baseline power in the highest frequencies
% because of motion artifacts.

CleanSlate  % provides a clean slate to start working with new data/scripts

wb = waitbar(0,'Initializing...');   % will display progress through loop
h = findall(wb,'Type','text');
h(1).Interpreter = 'none';

info_origfear             % load all metadata

artstr    = 'raw';        % string describing artifact rejection method used
spectype  = 'mtmconvol';  % assuming default parameters from get_cfg_TEM
winlen    = '';           % movwin length as string, if applicable, '' if not
normtype.pow  = 'ZdB';    % pseudo-z-score of dB transformed spectrograms
% normtype.coh  = 'Zraw';   % FIXME:  none of FT's aggregating/averaging functions
                                  % work for coherence!!
redonorm  = false;        % to ignore & overwrite old tone files
redoavg   = false;        % to ignore & overwrite old block files
compare   = false;  % to plot distributions before vs. after normalization, pause after block
plttone   = false;  % to plot allch spectrograms for each tone, pause after block
pltblk    = true;   % to plot 2ch spectrograms for each block of 3-4 tones, pause after rat
skiprats  = 1;           % already processed or input data not ready

errlog  = cell(size(ratT,1), sum(expected));   % MEs saved in rat-trl cells
rattime = NaN(size(ratT,1), 1);   % will save computation time per rat

%% full loop

for r = setdiff(find(~ratT.excluded)',skiprats)
%   try
    tic   % will time computations per rat
    
    %% loop through powspctrm & cohspctrm
    
    fields = fieldnames(normtype);
    
    for sf = 1:numel(fields)  % spectral field
      if strcmp(fields{sf},'coh')
        chnames = 'labelcmb';
        chcfg = 'channelcmb';
      else
        chnames = 'label';
        chcfg = 'channel';
      end
      
      %% define baseline freq inputfile
      
      inputfile = [datafolder num2str(ratT.ratnums(r)) filesep 'Baseline_' ...
        artstr '_' num2str(ratT.ratnums(r)) spectype 'TFA' fields{sf} winlen '.mat'];
      
      %% verify existence of baseline file
      
      if ~existfile_TEM(inputfile)
        errlog{r,sum(expected)} = ...
          ['Skipping rat #' num2str(ratT.ratnums(r)) '''s ' fields{sf} ...
          'spctrm because ' inputfile(numel(datafolder):end) ' was not found.'];
        warning(errlog{r,sum(expected)})
        continue % to next field
      end
      
      %% load baseline freq
      
      waitbar((r-1+(sf-1)/numel(fields))/size(ratT,1), wb, ...
        ['Loading baseline freq file: ' newline inputfile(numel(datafolder):end)])
      BLfreq = rmvlargefields_TEM(inputfile);
      
      %% check baseline format
      
      BLdimord = strsplit(BLfreq.dimord,'_');
      assert(isequal(numel(BLdimord),ndims(BLfreq.([fields{sf} 'spctrm']))));
      % make sure baseline is formatted as 1 long trial
      if any(strcmp(BLdimord,'rpt'))
        nBLt = size(BLfreq.([fields{sf} 'spctrm']),find(strcmp(BLdimord,'rpt')));
        if nBLt > 1
          assert(find(strcmp(BLdimord,'rpt')) == 1 && ...
            find(strcmp(BLdimord,'time')) == numel(BLdimord), ... % otherwise...
            'unexpected dimord')
          BLfreq.([fields{sf} 'spctrm']) = shiftdim(BLfreq.([fields{sf} 'spctrm']),1);
          dims = size(BLfreq.([fields{sf} 'spctrm']));
          BLfreq.([fields{sf} 'spctrm']) = reshape(BLfreq.([fields{sf} 'spctrm']), ...
            [dims(1:end-2), dims(end-1)*dims(end)]);
          BLfreq.time = BLfreq.time(1):mean(diff(BLfreq.time)):...
            dims(end-1)*dims(end)*mean(diff(BLfreq.time));
          BLdimord = BLdimord(2:end);
          BLfreq.dimord = strjoin(BLdimord,'_');
        end
      end
      
      %% check for outliers - NO!  STOP SCREWING AROUND WITH THIS!  
      %%% IT PROBABLY DOESN'T MATTER ANYWAY!
      
%       switch BLfreq.dimord
%       case 'chan_freq_time'
%         outliermat = false(size(BLfreq.([fields{sf} 'spctrm'])));
%         outlierrange = NaN(numel(BLfreq.label),numel(BLfreq.freq),3);
%         outliereffect = NaN(numel(BLfreq.label),numel(BLfreq.freq),6);  % mean, std, & skew, before & after outlier removal
%         for ch = 1:numel(BLfreq.label)
%           for f = 1:numel(BLfreq.freq)
%             x = BLfreq.time;
%             A = 10.*log10(squeeze(BLfreq.([fields{sf} 'spctrm'])(ch,f,:)));
%             [TF,lower,upper,center] = isoutlier(A,'gesd');
% %             figure;
% %             plot(x,A, x(TF),A(TF),'x', x([1 end]),[lower lower], ...
% %               x([1 end]),[upper upper], x([1 end]),[center center])
% %             legend('Original Data','Outlier','Lower Threshold',...
% %               'Upper Threshold','Center Value')
%             outliermat(ch,f,:) = TF;
%             outlierrange(ch,f,:) = [lower center upper];
%             outliereffect(ch,f,:) = [mean(A,'omitnan') std(A,'omitnan') skewness(A) ...
%               mean(A(~TF),'omitnan') std(A(~TF),'omitnan') skewness(A(~TF))];
%           end
%         end
%       otherwise
%         error('not yet implemented')
%       end
         
      %% loop through blocks of 3-4 tones, normalizing each tone, then averaging
      % and reduce to 2 region-representative channels
      
      for b = 1:numel(blknames)-1
        %% define & check for outputfile
        
        trlsinblk = blkedges(b)+1:blkedges(b+1);
        
        blkoutput = [datafolder num2str(ratT.ratnums(r)) filesep ...
          normtype.(fields{sf}) '_Norm_' artstr '_' spectype winlen 'TFA' ...
          fields{sf} '_' num2str(ratT.ratnums(r)) blknames{b} '_2ch.mat'];
        
        %% stop here to avoid continue
        
        if existfile_TEM(blkoutput)
          errlog{r,trlsinblk(1)} = ...
            ['Already averaged ' normtype.(fields{sf}) ' normalized data for ' ...
            blknames{b} ' of rat #' num2str(ratT.ratnums(r))];
          warning(errlog{r,trlsinblk(1)})
          if ~redoavg
            continue % to next block
          end
        end
        
        %% normalize each tone by baseline
        
        outputfiles = cell(size(trlsinblk));  % save filenames in case any are already calculated
        freq = cell(size(trlsinblk));   % but faster to avoid reloading into memory repeatedly
        
        for t = 1:numel(trlsinblk)
          %% identify phase & tone, skip if no data exists
          
          tr = trlsinblk(t);                          % trial # across all phases
          p = find(tr <= cumsum(expected),1,'first'); % phase
          tn = tr - sum(expected(1:p-1));             % tone # within phase
          
          %% stop here to avoid continue
          
          if isempty(nexfile{r,p})
            continue  % no need for a warning message when original files don't exist
          end
          
          %% define configuration params, input & output files
          
          cfg             = get_cfg_TEM(normtype.(fields{sf}), 'OrigFear');
          cfg.parameter   = [fields{sf} 'spctrm'];
          cfg.inputfile   = [datafolder num2str(ratT.ratnums(r)) filesep ...
            stimuli{1} num2str(tn) 'of' phases{p} '_' artstr '_' ...
            num2str(ratT.ratnums(r)) spectype 'TFA' fields{sf} winlen '.mat'];
          cfg.outputfile  = [datafolder num2str(ratT.ratnums(r)) filesep ...
            normtype.(fields{sf}) '_Norm_' artstr '_' spectype winlen 'TFA' fields{sf} '_' ...
            num2str(ratT.ratnums(r)) phases{p} '_' stimuli{1} num2str(tn) '.mat'];
          
          outputfiles{t} = cfg.outputfile;  % save filename to average w/ block
          
          %% check for in/output files
          
          if existfile_TEM(cfg.outputfile) && ~redonorm
            errlog{r,tr} = ...
              ['Already calculated ' normtype.(fields{sf}) ' normalized ' ...
              spectype winlen 'TFA ' fields{sf} 'spctrm for ' ...
              stimuli{1} ' ' num2str(tn) ' of ' phases{p} ' for rat #' ...
              num2str(ratT.ratnums(r))];
            warning(errlog{r,tr})
            continue % to next tone
          end
          
          if ~existfile_TEM(cfg.inputfile)
            errlog{r,tr} = ['Inputfile not found: ' ...
              cfg.inputfile(numel(datafolder):end)];
            warning(errlog{r,tr})
            continue  % to next tone
          end
          
          %% normalize the raw freq file by baseline (all channels)
          % if compare, plot comparison of raw vs. normalized value distributions
          
          waitbar((r-1+(sf-1+tr/sum(expected))/numel(fields))/size(ratT,1), ... % no -1 for tr because baseline is "done"
            wb, ['Normalizing ' fields{sf} 'spctrm for trial #' num2str(tr) ...
            ', rat #' num2str(ratT.ratnums(r)) ',' newline 'mean(rattime) = ' ...
            num2str(round(mean(rattime,'omitnan')/60)) ' minutes']);
          
          if compare
            fig = figure(t); clf; fig.WindowStyle = 'docked';
          end
          
          freq{t} = BLnorm_TEM(cfg,[],BLfreq,compare);
          
          %% plot spectrograms
          
          if plttone
            fig = figure(numel(trlsinblk)+t); clf; fig.WindowStyle = 'docked';
            fig.Name = [abbr{p} ' ' stimuli{1} num2str(tn)];
            plotFTmatrix_TEM([],freq{t});
          end
          
          %% reduce data in memory to 2 best channels
          
          if strcmp(fields{sf},'coh') && ismember('none',[ratT.mPFC(r) ratT.BLA(r)])
            errlog{r,tr} = ['Coherence data cannot be reduced to one ' ...
              'cross-regional pair if either region has no channels. - ' ...
              errlog{r,tr}]';   % append in case of other errors/warnings
            warning(errlog{r,tr})
            continue  % to next tone
          end
            
          cfg = [];
          cfg.(chcfg) = [ratT.mPFC(r) ratT.BLA(r); ... 	% keep in their cells!
            ratT.BLA(r) ratT.mPFC(r)];  % either order should be kept
          
          freq{t} = ft_selectdata(cfg, freq{t});
          
        end   % for trial
        
        %% pause to examine figures
        
        if (compare || plttone) && any(~cellfun(@isempty, freq))
          keyboard
        end
        
        %% verify that all tones in block were normalized (stop here to avoid continue)
        
        if strcmp(fields{sf},'coh')
          errlog{r,tr} = ['coherence cannot be averaged yet - skipping for ' ... % last tr of block
            blknames{b} '. - ' errlog{r,tr}];  % append so prior messages aren't lost
          warning(errlog{r,tr})
          continue  % to next block
        end
        
        if ~all(cellfun(@existfile_TEM,outputfiles))
          errlog{r,tr} = ['Not all tone trials were normalized for ' ... % last tr of block
            blknames{b} '. - ' errlog{r,tr}];  % append so explanation isn't lost
          warning(errlog{r,tr})
          continue % to next block
        end
        
        %% reduce to 2 best channels, average & save the block of 3 tones
        
        waitbar((r-1+(sf-1+b/numel(blknames))/numel(fields))/size(ratT,1), ... % no -1 for b because baseline is "done"
          wb, ['Averaging normalized ' fields{sf} 'spectra for ' blknames{b} ...
          ', rat #' num2str(ratT.ratnums(r)) ',' newline 'mean(rattime) = ' ...
          num2str(round(mean(rattime,'omitnan')/60)) ' minutes']);
        
        %% combine block of trials using ft_appendfreq
        
        cfg = [];
        cfg.parameter = [fields{sf} 'spctrm'];
        cfg.appenddim = 'rpt';
        cfg.tolerance = tol;
        
        if any(cellfun(@isempty, freq))
          cfg.(chcfg) = [ratT.mPFC(r) ratT.BLA(r); ... 	% keep in their cells!
            ratT.BLA(r) ratT.mPFC(r)];  % either order should be kept
          for t = find(cellfun(@isempty, freq))
            % load from file & clear historical metadata
            freq{t} = rmvlargefields_TEM(outputfiles{t});  
            if strcmp(fields{sf},'coh')   % change from chancmb to chan_chan
              freq{t} = ft_checkdata(freq{t}, 'cmbrepresentation','full');
            end
          end
        end
        
        freq = ft_appendfreq(cfg, freq{:});  % use data in memory
        
        if strcmp(fields{sf},'coh')
          %% restore sparse representation & cumtapcnt
          if ~isfield(freq,'cumtapcnt')
            dimord = split(freq.dimord,'_');
            nrpt = size(freq.([fields{sf} 'spctrm']),find(strcmp(dimord,'rpt')));
            freq.cumtapcnt = 3*ones(nrpt,1);  % assumes default params, but doesn't really matter anyway - just used for trial count
          end
          % change back from chan_chan to chancmb (ends up duplicating
          % symmetrical channel pairs)
          freq = ft_checkdata(freq, 'cmbrepresentation', 'sparse');
        end
        
        %% average block
        
        cfg = [];
        %       cfg.variance      = 'yes';  % does not omit nans with either of these, but I
        %       cfg.jackknife     = 'yes';  % could fix that by adding 'omitnan' to sums (e.g., line 141)
        %                                   % and changing nrpt to sum(~isnan()) (e.g., line 140)
        
        freq = ft_freqdescriptives(cfg, freq);
        %%% DOESN'T WORK FOR COHERENCE!!!
        % Descriptive statistics of bivariate metrics is not computed by this
        % function anymore. To this end you should use FT_CONNECTIVITYANALYSIS.

        freq = rmvlargefields_TEM(freq);  % clear historical metadata
        
        %% rename each channel to the region it represents
        % original labels are preserved in freq.cfg.channel
        
        for rrc = 1:numel(regions)
          if strcmp(ratT.(regions{rrc}){r},'none')
            % pad with NaNs so this region isn't dropped from group average
            chdim = find(startsWith(split(freq.dimord,'_'),'chan'));
            assert(chdim == 1, 'unexpected dimord')
            freq.([fields{sf} 'spctrm'])(end+1,:,:) = NaN;
            freq.(chnames){end+1,:} = regions{rrc};
          else
            freq.(chnames){ismember(freq.(chnames), ratT.(regions{rrc}){r})} = ...
              regions{rrc};
          end
        end
        
        %% plot if desired
        
        if pltblk
          fig = figure(2*max(diff(blkedges))+b); clf; fig.WindowStyle = 'docked';
          fig.Name = blknames{b};
          
          plotFTmatrix_TEM([],freq);
        end   % if plt
        
        %% save renamed 2ch freq file
        
        disp(['Saving freq to ' blkoutput])
        save(blkoutput,'freq');
        
      end   % for block
      
      %% pause to examine figures
      
      if pltblk
        keyboard
      end
      
    end   % for spectral field
    rattime(r) = toc;
    
%   catch ME
%     warning(ME.message);
%     errlog{r,tr} = ME;
%   end
end   % for rat

%% save errlog

save([datafolder 'errlog' datestr(now,29) ...
  '_FT_NormPlotTFA_OrigFear_TEM'], 'errlog')

waitbar(1, wb, ['Done!  Total time: ' ...
  num2str(sum(rattime,'omitnan')/60) ' minutes']);
