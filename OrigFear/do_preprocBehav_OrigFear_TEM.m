function [output] = do_preprocBehav_OrigFear_TEM()
% DO_PREPROCBEHAV_ORIGFEAR_TEM loads freezing and operant behavioral data
%
%   Loads freezing and barpressing/nosepoking data from .xls or .csv files and
%   parses them into FieldTrip structures (ft_datatype_raw and event,
%   respectively)
%
%   Depends on global variables defined in info_origfear.
%
% written 8/8/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global exptMeta

if isempty(exptMeta);   info_origfear;   end

%% create waitbar & predefine trialinfo

wb = waitbar(0,'Importing freezing data for 1st batch of rats');
comptime = [];  % will add multiple steps along the way

trialinfo = [(1:exptMeta.expected(1))' ones(exptMeta.expected(1),1) ones(exptMeta.expected(1),1); ...
  (1:exptMeta.expected(2))' ones(exptMeta.expected(2),1) 2*ones(exptMeta.expected(2),1); ...
  (1:exptMeta.expected(3))' ones(exptMeta.expected(3),1) 3*ones(exptMeta.expected(3),1); ...
  (1:exptMeta.expected(4))' zeros(exptMeta.expected(4),1) 4*ones(exptMeta.expected(4),1)];

%% import 1st batch's freezing data from Excel files in 30s tone-triggered bins

for r = find(~exptMeta.ratT.excluded)'
  if r > 8
    break   % out of for loop - these are processed differently below
  end
  %% update waitbar
  
  st = tic;
  
  waitbar(r/8, wb, ...
    ['Importing freezing data for rat ' num2str(exptMeta.ratT.ratnums(r))])
  
  %% define & check for outputfile
  
  outputfile = [exptMeta.datafolder 'preprocBehav' filesep ...
    'PctFreezing_30sBDAtones_' num2str(exptMeta.ratT.ratnums(r)) '.mat'];
  
  if existfile_TEM(outputfile)
    warning(['Skipping - outputfile already exists: ' ...
      outputfile(numel(exptMeta.datafolder):end)])
    continue  % to next rat
  end
  
  %% preallocate raw data structure (ft_datatype_raw)
  
  data = [];
  data.label      = {'PctFreezing'};
  data.time       = repmat({-15:30:45},1,size(trialinfo,1));
  data.trial      = cell(1,size(trialinfo,1));
  data.trialinfo  = trialinfo;
  data.sampleinfo = zeros(size(trialinfo,1),2);
  
  %% identify 30s export files for this rat
  
  for p = 1:3   % only phases w/ tones
    if r == 1 && p == 3
      inputfile = {[exptMeta.freezefolder '200\200_091007_0000.xlsx']};
      binsPerTone = 5;
      data.time(data.trialinfo(:,3) == p) = repmat({-45:30:75},1,exptMeta.expected(p));
    else
      binsPerTone = 3;
      inputfile = cell(1);
      switch class(exptMeta.nexfile{r,p,1})
        case 'double'   % empty set
          continue  % to next phase
        case 'char'   % one filename
          inputfile{1} = [exptMeta.freezefolder ...
            exptMeta.nexfile{r,p,1}(numel(exptMeta.LFPfolder) + (1:19)) ...
            '_30s.xlsx'];
        case 'cell'   % multiple filenames
          for fn = 1:numel(exptMeta.nexfile{r,p,1})
            inputfile{fn} = [exptMeta.freezefolder ...
              exptMeta.nexfile{r,p,1}{fn}(numel(exptMeta.LFPfolder) + (1:19)) ...
              '_30s.xlsx'];
          end
        otherwise
          error('unexpected nexfile type')
      end
    end
    tr = 1;   % start w/ 1st tone per phase
    for fn = 1:numel(inputfile)
      if ~existfile_TEM(inputfile{fn})
        error(['freezing data file not found: ' ...
            inputfile{fn}(numel(exptMeta.freezefolder):end)])
      end
      OldBatchOldFear_30sBinPercent = importFS30s_TEM(inputfile{fn});
      rn = 1;   % start w/ 1st row of table
      while tr <= exptMeta.expected(p) && rn < size(OldBatchOldFear_30sBinPercent,1)
        trr = ismember(data.trialinfo,[tr 1 p],'rows');
        if sum(trr) ~= 1 || ~isempty(data.trial{trr})
          error('problem with trial assignment')
        end
        data.trial{trr} = ...
          OldBatchOldFear_30sBinPercent.Percent(rn:rn+binsPerTone-1);
        data.sampleinfo(trr,:) = [OldBatchOldFear_30sBinPercent.FromSecond(rn) ...
          OldBatchOldFear_30sBinPercent.ToSecond(rn+binsPerTone-1)] .* exptMeta.fsLFP;
        tr = tr + 1; rn = rn + binsPerTone;   % move to next tone
      end   % for each tone
    end   % for each file
  end   % for each phase
  
  %% check result & save
  
  if sum(cellfun(@isempty,data.trial)) > 1
    warning(['more than one empty trial for rat ' num2str(exptMeta.ratT.ratnums(r))])
    keyboard
  end
  
  data = ft_datatype_raw(data);
  
  disp(['saving freezing data to ' outputfile])
  save(outputfile,'data')
  
  comptime(end+1) = toc(st);
end   % for each rat

%% import 2nd batch's freezing data from Excel file in 0.3s bins

waitbar(0, wb, 'Importing freezing data for 2nd batch of rats');
  
FSexportfolder = [exptMeta.freezefolder 'RawDataExports' filesep];
[NewBatchOldFear_03sBinPercent,header] = importFSfile_TEM(...
  [FSexportfolder '2015_oldfear_0_3sBinPercent_edit170825TEM.xlsx']);
binCenters = 0.15:0.3:0.3*(size(NewBatchOldFear_03sBinPercent,2)-9);

%% loop through each row

for rn = find(~isnan(NewBatchOldFear_03sBinPercent.TrialID))'
  %% update waitbar
  st = tic;
  waitbar(rn/size(NewBatchOldFear_03sBinPercent,1), wb, ...
    ['reformating freezing data from row ' num2str(rn) ' of ' ...
    num2str(size(NewBatchOldFear_03sBinPercent,1))])
  
  %% reformat as raw data (ft_datatype_raw)
  
  data = [];
  data.label  = {'PctFreezing'};
  data.time   = {binCenters};
  data.trial  = {NewBatchOldFear_03sBinPercent{rn,10:end}};
  data.hdr    = table2struct(NewBatchOldFear_03sBinPercent(rn,1:9));
  data.hdr.FSexportHdr  = header(~all(cellfun(@isempty,header),2),:);
  
  % remove padding after end of video
  enddat = find(~isnan(data.trial{1}),1,'last');
  if enddat < numel(data.trial{1})
    data.time{1} = data.time{1}(1:enddat);
    data.trial{1} = data.trial{1}(1:enddat);
  end
  
  %% match FreezeScan export row metadata to neural data files
  
  data.hdr.r = find(exptMeta.ratT.ratnums == data.hdr.Rat);
  
  if isempty(data.hdr.r)
    disp(data.hdr)
    data.hdr.r = find(exptMeta.ratT.ratnums == ...
      input('Which rat ID does this header match?  (#, or 0 for none)  '));
    if isempty(data.hdr.r)
      warning('skipping row - rat not identified')
      continue  % to next row
    end
  end
  
  data.hdr.p = find(strncmp(join([exptMeta.LFPfolder num2str(exptMeta.ratT.ratnums(data.hdr.r)) ...
    filesep data.hdr.Plexonname],''), exptMeta.nexfile(data.hdr.r,:,1), ...
    numel(exptMeta.LFPfolder)+14));  % match filenames (just through date, since many have the wrong video #)
  if numel(data.hdr.p) > 1  % just in case, match filename through video #
    data.hdr.p = find(strncmp(join([exptMeta.LFPfolder ...
      num2str(exptMeta.ratT.ratnums(data.hdr.r)) filesep data.hdr.Plexonname], ...
      ''), exptMeta.nexfile(data.hdr.r,:,1), numel(exptMeta.LFPfolder)+19));
  end
  data.hdr.order = [];
  
  if isempty(data.hdr.p)
    for p = 1:size(exptMeta.nexfile,2)
      if iscell(exptMeta.nexfile{data.hdr.r,p,1})
        data.hdr.order = find(strncmp(join([exptMeta.LFPfolder ...
          num2str(exptMeta.ratT.ratnums(data.hdr.r)) filesep ...
          data.hdr.Plexonname],''), exptMeta.nexfile{data.hdr.r,p,1}, ...
          numel(exptMeta.LFPfolder)+19));  % must use video file # here, or both will match
        if isempty(data.hdr.order)
          disp(['for p = ' num2str(p) ', exptMeta.nexfile = ' ...
            strjoin(exptMeta.nexfile{data.hdr.r,p,1}, [newline blanks(17) 'and '])])
        else
          data.hdr.p = p;
          break   % out of phase loop (to line 98) - no need to check or display the rest
        end
      else
        disp(['for p = ' num2str(p) ', nexfile = ' ...
          exptMeta.nexfile{data.hdr.r,p,1}(numel(exptMeta.LFPfolder):end)])
      end
    end   % for p = 1:size(exptMeta.nexfile,2)
    if isempty(data.hdr.p)
      disp(data.hdr)   % nexfile names will have been displayed above
      data.hdr.p = input('Which phase does this header match?  (#, or [] for none)  ');
    end
  end   % if p is empty, but r is not
  if isempty(data.hdr.p)
    disp(data.hdr)
    warning('unable to identify file automatically')
    keyboard
  end
  
  %% verify format & save
  
  outputfile = [exptMeta.datafolder 'preprocBehav' filesep 'PctFreezing_' ...
    num2str(exptMeta.ratT.ratnums(data.hdr.r)) exptMeta.phases{data.hdr.p} ...
    num2str(data.hdr.order) 'tr' num2str(data.hdr.TrialID) '.mat'];
  
  if existfile_TEM(outputfile)
    warning(['Skipping - outputfile already exists: ' outputfile(numel(exptMeta.datafolder):end)])
    continue  % to next file
  end
    
  data = ft_datatype_raw(data);
  
  disp(['saving freezing data to ' outputfile(numel(exptMeta.datafolder):end)])
  save(outputfile, 'data')
  
  comptime(end+1) = toc(st);
end   % for rn = 1:size(NewBatchOldFear_03sBinPercent,1)

%% import operant behavioral data (barpressing or nosepoking)
% open each raw ABET export file, extract relevant meta-data, & restructure
% operant data into FieldTrip-usable format

outputfile = [exptMeta.datafolder 'preprocBehav' filesep 'AllRatsOperantEvents.mat'];

if existfile_TEM(outputfile)
  warning(['Loading operant behavior from file: ' outputfile(numel(exptMeta.datafolder):end)])
  load(outputfile)  % to parse out into individual rats' event files
else
  listing = dir([exptMeta.operantfolder '*.csv']);
  headers = struct('hdr',cell(size(listing)), ...   % original header
    'r',cell(size(listing)),'p',cell(size(listing)));   % matched to
  events = cell(size(exptMeta.ratT,1),numel(exptMeta.phases));  % plural for aggregate cells
  
  %% loop through each CSV file
  
  for fn = 1:numel(listing)
    %% update waitbar
    st = tic;
    waitbar(fn/numel(listing),wb,['reading operant data from file ' ...
      num2str(fn) ' of ' num2str(numel(listing))])
    
    %% read CSV file
    
    filename = [listing(fn).folder filesep listing(fn).name];
    % numerical columns read as double (%f),	text columns as categorical (%C)
    formatSpec = {'%f','%f','%C','%C','%C','%f','%f', ...   % event info
      '%C','%f','%C','%f','%C','%f','%C','%f','%C','%f'};   % Arg Name/Value pairs
    ds = datastore(filename, 'TextscanFormats',formatSpec);
    tt = tall(ds);
    headers(fn).hdr = importCSVfilehdr_TEM(filename, ds.NumHeaderLines-1);  % last 1 is variable names, included in ds
    
    %% match ABET export headers to neural data files
    
    headers(fn).r = find(strncmp(erase(headers(fn).hdr.AnimalID,{'TEM','#',' ',','}), ...
      cellstr(num2str(exptMeta.ratT.ratnums)),3));
    
    if isempty(headers(fn).r)
      disp(headers(fn).hdr)
      headers(fn).r = find(exptMeta.ratT.ratnums == ...
        input('Which rat ID does this header match?  (#, or 0 for none)  '));
    end
    
    if isempty(headers(fn).r)
      warning('skipping file - rat not identified')
      continue  % to next file
    end
          
    headers(fn).p = find(strncmp([exptMeta.LFPfolder num2str(exptMeta.ratT.ratnums(headers(fn).r)) ...
      filesep headers(fn).hdr.Filename], exptMeta.nexfile(headers(fn).r,:,1), ...
      numel(exptMeta.LFPfolder)+19));   % match filenames
    
    if isempty(headers(fn).p)
      switch headers(fn).hdr.Filename
        case '091003_0000.plx, , '  % rat # left off
          headers(fn).r = 1; headers(fn).p = 1;
        case '339_150517_0001, , '  % wrong video file code
          headers(fn).r = 10; headers(fn).p = 3;
        case '342_150627_0000, , '  % wrong video file code
          headers(fn).r = 12; headers(fn).p = 2;
        case '343_150627_0002, , '  % wrong video file code
          headers(fn).r = 13; headers(fn).p = 2;
        case '203_091209_0001.plx, , '  % extra session only done for one rat
          warning('skipping 3rd extinction session for 203')
          headers(fn).r = NaN;
          headers(fn).p = NaN;
          continue
        otherwise
          for p = 1:size(exptMeta.nexfile,2)
            if iscell(exptMeta.nexfile{headers(fn).r,p,1})
              headers(fn).order = find(strncmp([exptMeta.LFPfolder ...
                num2str(exptMeta.ratT.ratnums(headers(fn).r)) filesep ...
                headers(fn).hdr.Filename], exptMeta.nexfile{headers(fn).r,p,1}, ...
                numel(exptMeta.LFPfolder)+19));
              if isempty(headers(fn).order)
                disp(['for p = ' num2str(p) ', nexfile = ' ...
                  strjoin(exptMeta.nexfile{headers(fn).r,p,1}, [newline blanks(17) 'and '])])
              else
                headers(fn).p = p;
                break   % out of phase loop - no need to check or display the rest
              end
            else
              disp(['for p = ' num2str(p) ', nexfile = ' ...
                exptMeta.nexfile{headers(fn).r,p,1}(numel(exptMeta.LFPfolder):end)])
            end
          end
          if isempty(headers(fn).p)
            disp(headers(fn).hdr)   % nexfile names will have been displayed above
            headers(fn).p = input('Which phase does this header match?  (#, or [] for none)  ');
          end
      end
    end
    if isempty(headers(fn).p)
      disp(headers(fn).hdr)
      warning('unable to identify file automatically')
      keyboard
    end
    
    %% organize events by FieldTrip convention (see ft_read_event)
    
    evttypes = gather(categories(tt.Item_Name));
    evtcnts = gather(countcats(tt.Item_Name));
    evttypes = table(evttypes(contains(evttypes,'#')), ...  % all in/output devices (lever or nosepoke, tone, pellet dispenser, Plexon bits & strobe, etc.)
      evtcnts(contains(evttypes,'#')), 'VariableNames',{'Name','Count'});
    event = struct('type',cell(sum(evttypes.Count),1), ...   % singular for individual log files
      'sample',cell(sum(evttypes.Count),1), 'value',cell(sum(evttypes.Count),1), ...
      'offset',cell(sum(evttypes.Count),1), 'duration',cell(sum(evttypes.Count),1), ...
      'timestamp',cell(sum(evttypes.Count),1));
    ei = 0;   % starting event index
    for et = 1:size(evttypes,1)
      tmpevt = gather(tt(tt.Item_Name == evttypes.Name{et},:));   % gather upfront - these tables aren't big enough to worry about memory issues
      [event(ei+(1:evttypes.Count(et))).type] = deal(evttypes.Name{et});
      tmp = mat2cell(round(tmpevt.Evnt_Time*fsLFP) + 1, ones(evttypes.Count(et),1));
      [event(ei+(1:evttypes.Count(et))).sample] = deal(tmp{:});
      tmp = cellstr(tmpevt.Evnt_Name);
      nargs = max(tmpevt.Num_Args);
      for n = 1:nargs
        tmp = join([tmp cellstr(tmpevt.(['Arg' num2str(n) '_Name'])) ...
          cellstr(num2str(tmpevt.(['Arg' num2str(n) '_Value'])))], ' | ');
      end
      [event(ei+(1:evttypes.Count(et))).value] = deal(tmp{:});
      tmp = mat2cell(zeros(evttypes.Count(et),1),ones(evttypes.Count(et),1));
      [event(ei+(1:evttypes.Count(et))).offset] = deal(tmp{:});
      tmp = mat2cell(ones(evttypes.Count(et),1),ones(evttypes.Count(et),1));
      [event(ei+(1:evttypes.Count(et))).duration] = deal(tmp{:});
      tmp = mat2cell(round(tmpevt.Evnt_Time*fsSpk) + 1, ones(evttypes.Count(et),1));
      [event(ei+(1:evttypes.Count(et))).timestamp] = deal(tmp{:});
      % update last row before starting next event type
      ei = ei + evttypes.Count(et);
    end   % for each event type
    
    %% aggregate with other files
    
    if isempty(events{headers(fn).r,headers(fn).p})   % no other files have shared the same metadata
      events{headers(fn).r,headers(fn).p} = event;
    else  % one or more previous files shared this metadata
      ord = headers(fn).order;
      other = headers([headers.r] == headers(fn).r & [headers.p] == headers(fn).p).order;
      if ~isempty(ord)
        if ~isempty(other) && sum(other == ord) > 1
          error('more than one file has the same r, p, & order')
        end
      elseif ~isempty(other)
        ord = find(~ismember([1 2],other));
      else
        warning('multiple files match same r & p with no order specified')
        keyboard  % figure out how to merge them
      end
      switch ord
        case 1  % current events come first
          events{headers(fn).r,headers(fn).p} = {event, events{headers(fn).r,headers(fn).p}};
        case 2  % current events come 2nd
          events{headers(fn).r,headers(fn).p} = {events{headers(fn).r,headers(fn).p}, event};
        otherwise
          error('unexpected order specification')
      end
    end
    
    comptime(end+1) = toc(st);
  end   % for each file in directory
  
  %% verify that all tones are accounted for
  
  for r = find(~exptMeta.ratT.excluded)'
    for p = 1:3
      if isempty(events{r,p})
        warning(['No events found for rat ' num2str(exptMeta.ratT.ratnums(r)) ', ' exptMeta.phases{p}])
        continue
      end
      if iscell(events{r,p})
        count = zeros(numel(events{r,p}),1);
        for c = 1:numel(events{r,p})
          count(c) = sum(strcmp({events{r,p}{c}.type},'Tone #1'));
        end
        disp(['rat ' num2str(exptMeta.ratT.ratnums(r)) ' has ' num2str(c) ' cells for ' ...
          exptMeta.phases{p} ', containing ' strjoin(cellstr(num2str(count)),' and ') ...
          ' tones, respectively'])
        count = sum(count);
      elseif isstruct(events{r,p})
        count = sum(strcmp({events{r,p}.type},'Tone #1'));
      else
        error('unexpected event format')
      end
      if count ~= exptMeta.expected(p)
        error('tone count is different than expected for phase')
      end
    end
  end
  
  %% save behavioral data
  
  disp(['saving events & headers to ' outputfile(numel(exptMeta.datafolder):end)])
  save(outputfile, 'events','headers')
  
end

%% parse aggregate event files into one event structure per rat
% with each operant behavior timestamp given relative to nearest tone

cntBDAtones = cell(size(exptMeta.ratT,1),1);

for r = find(~exptMeta.ratT.excluded)'
  cntBDAtones{r} = nan(sum(exptMeta.expected(1:3)),3);  % count behavior in 30s BDA tones
  for p = 1:3
    if isempty(events{r,p})
      warning(['no events found for rat ' num2str(exptMeta.ratT.ratnums(r)) ...
        ', ' exptMeta.phases{p}])
      continue  % to next phase
    end
    
    %% define & check for outputfile 
    
    outputfile = [exptMeta.datafolder 'preprocBehav' filesep ...
      'ABETEvents' num2str(exptMeta.ratT.ratnums(r)) exptMeta.abbr{p} '.mat'];
    
    if existfile_TEM(outputfile)
      warning(['Loading operant behavior from file: ' outputfile(numel(exptMeta.datafolder):end)])
      load(outputfile)  % to count operant responses BDA tones
    else
    
    %% reformat event data
    
    if isstruct(events{r,p})
      events{r,p} = events(r,p);  % put inside a cell to allow loop through cells
    end
    tmp = events{r,p};
    priortones = 0;   % no prior tones in this phase
    for c = 1:numel(tmp)
      [tmp{c}.rat] = deal(exptMeta.ratT.ratnums(r));
      [tmp{c}.phase] = deal(exptMeta.phases{p});
      [tmp{c}.cell] = deal(c);
      [tmp{c}.tonetrial] = deal(priortones + 1);  % beginning of each file is allocated to the trial of the next tone
      tmp{c} = sortrows(struct2table(tmp{c}),'timestamp');
      toneEvts = find(contains(tmp{c}.type,'tone','IgnoreCase',true) & ...
        ~contains(tmp{c}.type,'report','IgnoreCase',true));
      for tn = 1:numel(toneEvts)
        if priortones + tn > exptMeta.expected(p)
          error('tone # is higher than expected for phase')
        end
        tmp{c}.tonetrial(tmp{c}.timestamp - tmp{c}.timestamp(toneEvts(tn)) > ...
          -75*exptMeta.fsSpk) = priortones + tn;
      end
      priortones = priortones + tn;   % in case of another cell
    end
    event = vertcat(tmp{:});
    
    %% make type categorical
    
    event.type = categorical(event.type);
    
    %% save event to file
    
    disp(['saving event to ' outputfile(numel(exptMeta.datafolder):end)])
    save(outputfile, 'event')
    
    end   % if file already exists, load it
    
    %% count operant responses BDA tones
    
    evtTypes = categories(event.type);
    operantType = find(contains(evtTypes,'response','IgnoreCase',true));
    toneType = find(contains(evtTypes,'tone','IgnoreCase',true) & ...
      ~contains(evtTypes,'report','IgnoreCase',true));
    if numel(operantType) ~= 1 || numel(toneType) ~= 1
      error('ambiguous event types')
    end
    toneNdx = find(event.type == evtTypes{toneType});
    
    for tn = 1:exptMeta.expected(p)
      trlNdx = event.tonetrial == tn;
      if ~trlNdx(toneNdx)
        error('the tone event is not included within its own trial')
      end
      tdiff = (event.timestamp - event.timestamp(toneNdx(tn))) ./ exptMeta.fsSpk;
      cntBDAtones{r}(ismember(trialinfo,[tn 1 p],'rows'),:) = [...
        sum(trlNdx & event.type == evtTypes{operantType} & tdiff > -30 & tdiff <= 0) ...
        sum(trlNdx & event.type == evtTypes{operantType} & tdiff > 0 & tdiff <= 30) ...
        sum(trlNdx & event.type == evtTypes{operantType} & tdiff > 30 & tdiff <= 60)];
    end
  end
end

%% save cntBDAtones
  
aggoutputfile = [exptMeta.datafolder 'preprocBehav' filesep 'AllRatsBDAtones.mat'];
disp(['saving cntBDAtones to ' aggoutputfile(numel(exptMeta.datafolder):end)])
save(aggoutputfile,'cntBDAtones')

%% clean up

output = comptime;
close(wb)

end   % function