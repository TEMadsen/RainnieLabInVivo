%% Completely automated artifact rejection!
% process each channel independently, removing clipping & large (e.g., shock)
% artifacts based on automated thresholds, so no human intervention is required!

CleanSlate

info_origfear  % load all background info (filenames, etc)

zthresh       = 'Shock';  % z-score threshold for large artifacts based on min(max(z-score during shock trials))
cthresh       = 3;        % clipping threshold in uV of the 2nd derivative of the data
redo          = true;    % to ignore & overwrite old files
skiprats      = [];       % already processed or running on another computer
interactrat   = false;     % to select visual artifacts per rat & review all channels after automated detection
interactch    = false;     % to preview z-thresholds & select visual artifacts per channel
mtmc          = get_cfg_TEM('mtmconvol','OrigFear');  % defaults set in that function

%% preallocate variables that aggregate info across all rats

meanch = nan(numel(allchs),size(ratT,1));
stdch = nan(numel(allchs),size(ratT,1));  

nanprop = cell(size(ratT,1), 1);  % rat cells will contain chndx x trl matrices

excludech = false(size(ratT,1), numel(allchs)); % initialize false, will be
% set to true if channel is missing from data or excluded based on artifacts

%% full loop

for r = setdiff(find(~ratT.excluded)',skiprats)
  cfg = [];
  
  % Raw LFP data divided into long, non-overlapping tone-triggered
  % trials, merged across all recordings for each rat
  cfg.inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'RawLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  % Clean LFP data with individual channel artifacts replaced with NaNs,
  % remerged to include all channels
  cfg.outputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllClean_z' zthresh '_clip' num2str(cthresh) ...
    '_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];
  
  %% check for output file
  if exist(cfg.outputfile,'file') && ~redo
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ' because ' ...
      cfg.outputfile(numel(datafolder):end) ' already exists.'])
    continue
  end
  
  %% check for input data
  
  if ~exist(cfg.inputfile,'file')
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ' because ' ...
      cfg.inputfile(numel(datafolder):end) ...
      ' was not found. Run FT_preproc_OrigFear_TEM.m first.'])
    continue
  end
  
  %% load inputfile
  
  data = rmvlargefields_TEM(cfg.inputfile);
  
  %% collect appropriate z-score thresholds for each channel
  
  tmpdat = [data.trial{:}];
  
  for ch = 1:numel(allchs)
    % find the channel name in this data, exclude if missing
    chndx = ismember(data.label, allchs{ch});
    if ~any(chndx)
      excludech(r, ch) = true;
      warning([allchs{ch} ' missing from file'])
      continue  % skip to next channel
    end
    meanch(ch,r) = mean(abs(tmpdat(chndx,:)));
    stdch(ch,r) = std(abs(tmpdat(chndx,:)));
  end
  
  zscores = nan(numel(data.label),numel(data.trial));
  
  for tr = 1:numel(data.trial)
    zscores(:,tr) = max((abs(data.trial{tr})-meanch(~isnan(meanch(:,r)),r)) ...
      ./stdch(~isnan(stdch(:,r)),r),[],2);
  end   % max z-score for each channel in each trial
  
  fig = figure(r); clf; fig.WindowStyle = 'docked';
  imagesc(zscores); colormap(jet); colorbar;
  xticks(blkedges(2:end)); xticklabels(blknames); xtickangle(15);
  yticks(1:numel(data.label)); yticklabels(data.label)
end

fig = figure; clf; fig.WindowStyle = 'docked';
errorbar(meanch,stdch)

%% temp stop
  zthreshn = floor(median(min(zscores(:, ...   % min across shock trials, then median across channels
    ismember(data.trialinfo, [(11:17)' ones(7,2)], 'rows')),[],2)));
  % this works okay (catching some shocks on each channel)
  
  %% define parameters of artifact rejection
  
  cfg.interactsubj                    = interactrat;
  cfg.interactch                      = interactch;
  
  cfg.artfctdef.clip.channel          = 'all';
  cfg.artfctdef.clip.pretim           = 0.1;
  cfg.artfctdef.clip.psttim           = 0.1;
  cfg.artfctdef.clip.timethreshold    = 0.05;     % s
  cfg.artfctdef.clip.amplthreshold    = cthresh;  % uV
  
  cfg.artfctdef.zvalue.channel        = 'all';
  cfg.artfctdef.zvalue.cutoff         = zthreshn;
  cfg.artfctdef.zvalue.trlpadding     = 0;
  cfg.artfctdef.zvalue.fltpadding     = 0;
  cfg.artfctdef.zvalue.artpadding     = 0.1;
  cfg.artfctdef.zvalue.rectify        = 'yes';
  
  cfg.artfctdef.minaccepttim          = 0.25;
  
  %% reject artifacts
  
  [cleandata] = AutoArtReject_TEM(cfg, data);
  
  %% count NaNs per channel & trial

  for ch = 1:numel(allchs)
    % find the channel name in this data, exclude if missing
    chndx = ismember(cleandata.label, allchs{ch});
    if ~any(chndx)
      excludech(r, ch) = true;
      warning([allchs{ch} ' missing from file'])
      continue  % skip to next channel
    end
    for tr = 1:numel(cleandata.trial)
      nanprop{r}(ch,tr) = sum(isnan(cleandata.trial{tr}(chndx,:))) ...
        / size(cleandata.time{tr},2);
    end  
  end
end

%% plot & save proportion of NaNs by rats & channels

for r = find(~ratT.excluded)'
  fig = figure(r); clf; fig.WindowStyle = 'docked';
  imagesc(nanprop{r}); colormap(jet); colorbar;
  xticks(blkedges(2:end)); xticklabels(blknames); xtickangle(15);
  yticks(1:numel(allchs));
end

save([datafolder 'Cleaned_z' zthresh '_clip' num2str(cthresh) '_nanprop' ...
  datestr(now,30) '.mat'],'nanprop');
