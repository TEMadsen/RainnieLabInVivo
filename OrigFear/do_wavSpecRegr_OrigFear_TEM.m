function [output] = do_wavSpecRegr_OrigFear_TEM( r )
% DO_WAVSPECREGR_ORIGFEAR_TEM tests for linear trends btwn spectrograms & trl#,
% one block of 7-15 trials at a time
%
%   This function is for within-subject visualization & stats using
%   ft_statfun_indepsamplesregrT on the output of wavelet (power spectrograms).
%
%   Depends on global variables defined in info_origfear.
%
% written 6/26/17 by Teresa E. Madsen, Ph.D.

%% check for input

if nargin < 1
  info_origfear;  % metadata may not have been already loaded by master script
  r = 1;          % poster rat
end

%% declare global variables

global datafolder configfolder figurefolder ratT fsample stimuli phases  ...
  intervals blkedges blknames mainblocks blksof7names expected

%% create waitbar

wb = waitbar(0, ['Preparing for wavelet spectrogram analysis on rat #' ...
  num2str(ratT.ratnums(r))]);

%% prepare_neighbours determines what sensors may form clusters

neighbfile = [configfolder 'neighb_' ratT.MWA{r} '_' ratT.side{r} '.mat'];

if existfile_TEM(neighbfile)
  load(neighbfile)
else
  cfg = [];
  cfg.method        = 'distance';
  cfg.neighbourdist = 0.5;  % mm
  cfg.elecfile      = [configfolder 'elec_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  neighbours        = ft_prepare_neighbours(cfg);
  save(neighbfile,'neighbours')
end
if any(cellfun(@numel,{neighbours(:).neighblabel}) < 3) || ...
    any(cellfun(@numel,{neighbours(:).neighblabel}) > 5)
  warning('unexpected # of neighbors')
  cfg = [];
  cfg.neighbours = neighbours;
  cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  ft_neighbourplot(cfg)
  keyboard
end

%% loop through main blocks, loading data

freq      = cell(6,10);   % 7-10 tones per block:  begin & end of each recording
smthfreq  = cell(size(freq));     % smoothed and downsampled time axis
comptime  = nan(size(freq,1),3);  % 3 steps:  prep data, run stats, plot results
esttime   = [802, 454, 11.6];     % each step's estimated comptime (s) per block
% esttime = nanmean(comptime,1);  % average of past calculations

for b = 1:numel(blksof7names)
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/sum(esttime.*size(freq,1)), wb, ...
    ['Preparing ' blksof7names{b} ' wavelet spectrograms for regression analysis']);
  
  p = ceil(b/2);  % convert block to phase
  
  if rem(b,2) == 1  % odd #s
    tones = 1:10;                       % 1st 10 of each recording
  else              % even #s
    tones = expected(p)-6:expected(p);  % last 7 of each recording
  end
  
  for tr = 1:numel(tones)   % trial # within block
    %% define trial inputfile
    
    tn = tones(tr);   % trial # within phase
    
    inputfile2 = [datafolder 'wavelet' filesep num2str(ratT.ratnums(r)) ...
      filesep stimuli{1} num2str(tn) 'of' phases{p} '_ICA_' ...
      num2str(ratT.ratnums(r)) 'waveletTFAdB.mat'];
    
    %% load dB transformed inputfile, if it exists
    
    if existfile_TEM(inputfile2)
      freq{b,tr} = rmvlargefields_TEM(inputfile2);
    else
      %% load fourier inputfile & transform it
      
      inputfile1 = strrep(inputfile2,'dB','fourier');
      
      if ~existfile_TEM(inputfile1)
        warning(['Skipping tone ' num2str(tn) ' of ' phases{p} ...
          ' because inputfile (' inputfile1(numel(datafolder):end) ') was not found.'])
        continue  % to next tone
      end
      
      freq{b,tr} = rmvlargefields_TEM(inputfile1);
      wavlen = freq{b,tr}.cfg.wavlen;
      
      %% convert to powspctrm
      
      freq{b,tr} = ft_checkdata(freq{b,tr},'cmbrepresentation','sparsewithpow');
      
      assert(isequal(freq{b,tr}.dimord,'chan_freq_time'))
      
      %% replace acquisition powspctrm time/freq bins including shock time w/ NaNs
      
      if b == 2
        for f = 1:numel(freq{b,tr}.freq)
          badt = freq{b,tr}.time > 29 - wavlen(f) & ...
            freq{b,tr}.time < 32 + wavlen(f);
          freq{b,tr}.powspctrm(:,f,badt) = NaN;
        end
      end
      
      %     %% plot wavelet spectrograms
      %
      %     cfg = [];
      %     cfg.layout        = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
      %     cfg.comment       = [stimuli{1} num2str(tn) 'of' phases{p}];
      %     cfg.showscale     = 'no';
      %     cfg.feedback      = 'no';
      %     cfg.parameter     = 'powspctrm';
      %     cfg.baseline      = [-inf -freq{b,tr}.cfg.wavlen(1)];
      %     cfg.baselinetype  = 'db';
      %     cfg.zlim          = [-10 10];
      %     cfg.colormap      = jet;
      %     fig = figure(sum(expected(1:(p-1)))+tn); clf; fig.WindowStyle = 'docked';
      %     ft_multiplotTFR(cfg,freq{b,tr})
      
      %% db transform & save
      
      cfg = [];
      cfg.parameter   = 'powspctrm';
      cfg.operation   = '10*log10(x1)';
      cfg.outputfile  = inputfile2;
      
      freq{b,tr} = ft_math(cfg,freq{b,tr});
      
    end
    
    %% smooth & downsample time axis, use linear change in freq smoothing
    
    mtmc = get_cfg_TEM('mtmconvol','OrigFear');
    
    smthfreq{b,tr} = freq{b,tr};
    smthfreq{b,tr}.time = -60:0.5:60;   % 5s windows shifting by 0.5s
    smthfreq{b,tr}.freq = mtmc.foi(mtmc.foi-mtmc.tapsmofrq > freq{b,tr}.freq(1) ...
      & mtmc.foi+mtmc.tapsmofrq < freq{b,tr}.freq(end));  % where full range is available to average
    smthfreq{b,tr}.powspctrm = nan(size(freq{b,tr}.powspctrm,1), ...
      numel(smthfreq{b,tr}.freq), numel(smthfreq{b,tr}.time));
    for t = 1:numel(smthfreq{b,tr}.time)
      tndx = abs(freq{b,tr}.time-smthfreq{b,tr}.time(t)) < 2.5;   % +/-2.5s
      if ~any(tndx)
        warning('no times selected')
        keyboard
      end
      for f = 1:numel(smthfreq{b,tr}.freq)
        fndx = abs(freq{b,tr}.freq-smthfreq{b,tr}.freq(f)) < ...
          mtmc.tapsmofrq(f);  % +/-w
        if ~any(fndx)
          warning('no frequencies selected')
          keyboard
        end
        smthfreq{b,tr}.powspctrm(:,f,t) = median(...  % to preserve sharp changes & avoid biasing by skew
          reshape(freq{b,tr}.powspctrm(:,fndx,tndx), ...
          size(freq{b,tr}.powspctrm,1),[]), 2,'omitnan');
      end
    end
    if sum(isnan(smthfreq{b,tr}.powspctrm(:)))/numel(smthfreq{b,tr}.powspctrm) ...
        >= 0.2  % 24s - could be most of a before, during, or after tone period
      warning(['trial #' num2str(tn) ' of ' phases{p} ...
        ' skipped because more than 20% of the spectrogram is NaNs'])
      smthfreq{b,tr} = [];
    end
  end
  
  %% update estimate of time to complete analysis
  
  comptime(b,1) = toc(starttime);
  esttime(1) = nanmean([esttime(1); comptime(b,1)]); % average into estimate of how long each block should take
end

%% run within-subject stats on power spectrograms

stat = cell(size(smthfreq,1),1);

for b = 1:size(smthfreq,1)
  %% make sure all trials were done
  
  omit = cellfun(@isempty,smthfreq(b,:));
  if sum(~omit) < 6
    warning([blksof7names{b} ' only has ' num2str(sum(~omit)) ' trials'])
    keyboard
  end
  
  %% define & check for outputfile
  
  cfg = [];
  cfg.outputfile  = [datafolder 'wavSpecRegr' filesep num2str(ratT.ratnums(r)) ...
    filesep blksof7names{b} '_ICA_' num2str(ratT.ratnums(r)) ...
    'stat_indepsamplesregrT_NPindiv_ordered.mat'];
  
%   if existfile_TEM(cfg.outputfile)
%     stat{b} = rmvlargefields_TEM(cfg.outputfile);
%   else
    starttime = tic;  % only time computation, not just loading files
    
    cfg.statistic         = 'ft_statfun_indepsamplesregrT';
    
    waitbar(sum(comptime(:),'omitnan')/sum(esttime.*size(smthfreq,1)), wb, ...
      ['Running ' cfg.statistic(12:end) ' on ' blksof7names{b} ...
      ' wavelet spectrograms for rat #' num2str(ratT.ratnums(r))]);
    
    cfg.method            = 'montecarlo';
    cfg.correctm          = 'cluster';
    cfg.clusteralpha      = 0.025;
    cfg.clusterstatistic  = 'maxsum';
    cfg.clusterthreshold  = 'nonparametric_individual';   % essential for reasonable thresholds!
    cfg.minnbchan         = 2;
    cfg.tail              = 0;
    cfg.clustertail       = 0;
    cfg.alpha             = 0.025;
    cfg.numrandomization  = 500;
    cfg.orderedstats      = 'yes';  % essential for reasonable thresholds!
    cfg.neighbours        = neighbours;
    cfg.parameter         = 'powspctrm';
    
    cfg.design  = find(~omit);  % skips missing trial #s as appropriate
    
    cfg.ivar    = 1;  % uvar = ivar (between-UO)
    
    stat{b} = ft_freqstatistics(cfg, smthfreq{b,~omit});
    comptime(b,2) = toc(starttime);
    esttime(2) = nanmean([esttime(2); comptime(b,2)]); % average into estimate of how long each block should take
%   end
end

%% print & plot (top 10/nearly) significant correlations of spectrograms & trl#s

for b = find(~cellfun(@isempty,stat))'
  starttime = tic;
  waitbar(sum(comptime(:),'omitnan')/sum(esttime.*size(smthfreq,1)), wb, ...
    ['Plotting results of ' stat{b}.cfg.statistic ' on ' blksof7names{b} ...
    ' wavelet spectrograms for rat #' num2str(ratT.ratnums(r))]);
  
  pos = [];
  if isempty(stat{b}.posclusters)
    pos.n = 0;
    pos.I = [];  % no index because there are no clusters
  else
    [pos.p,pos.I] = sort([stat{b}.posclusters.prob]);
    pos.n = find(pos.p > 0.025,1,'first') - 1;
  end
  neg = [];
  if isempty(stat{b}.negclusters)
    neg.n = 0;
    neg.I = [];  % no index because there are no clusters
  else
    [neg.p,neg.I] = sort([stat{b}.negclusters.prob]);
    neg.n = find(neg.p > 0.025,1,'first') - 1;
  end
  bestch = zeros(pos.n+neg.n+2,1);
  disp([newline 'Found clusters with significant positive (' ...
    num2str(pos.n) ') and negative (' num2str(neg.n) ')' newline ...
    'correlations with trial #s within ' blksof7names{b} newline])
  
  cfg =[];
  cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  cfg.parameter = 'stat';
  cfg.feedback  = 'no';
  cfg.gridscale = 2*numel(stat{b}.label) + 1;
  %   ft_clusterplot(cfg,stat{b});  % doesn't work unless freq or time is singleton
  
  %% visualize positive correlation clusters
  
  for n = 1:(pos.n + 1)  % may plot one that's not considered significant
    if n > 10 || numel(pos.I) < n
      break  % out of for loop
    end
    cI = pos.I(n);  % cluster index
    I = find(stat{b}.posclusterslabelmat == cI);
    [ch,f,t] = ind2sub(size(stat{b}.posclusterslabelmat),I);
    bestch(n) = mode(ch);
    ch = unique(ch); f = unique(f); t = unique(t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat{b}.freq(min(f)) stat{b}.freq(max(f))];
    trange = [stat{b}.time(min(t)) stat{b}.time(max(t))];
    trlns = nan(1,size(smthfreq,2));
    clustpowpertrl = nan(numel(I),size(smthfreq,2));   % aggregate cluster power over trials
    for tr = find(~cellfun(@isempty,smthfreq(b,:)))
      trlns(tr) = smthfreq{b,tr}.trialinfo(1,1);
      clustpowpertrl(:,tr) = smthfreq{b,tr}.powspctrm(I);
    end
    clustpowpertrl(:,isnan(trlns)) = [];
    trlns(isnan(trlns)) = [];
    
    fig = figure(b*100+n); clf; fig.WindowStyle = 'docked';
    fig.PaperPositionMode = 'auto';
    % plot mean +/- std across trials for each chan/freq/timebin w/in cluster
    subplot(1,2,1); hold all
    X = repmat(trlns,size(clustpowpertrl,1),1);
    CC = corrcoef(X(:), clustpowpertrl(:));
    [pc,ErrorEst] = polyfit(X(:), clustpowpertrl(:), 3);   % fit cubic model
    pop_fit = polyval(pc,trlns,ErrorEst);
    plot(X(:),clustpowpertrl(:),'+', trlns,mean(clustpowpertrl),'-', ...
      trlns,pop_fit,'-');
    axis tight
    legend({'chan/time/freq bins','mean per trial','cubic fit'}, ...
      'Location','Best')
    
    title([strjoin(stat{b}.label(ch)) newline ...
      'have significant (p = ' num2str(stat{b}.posclusters(cI).prob) ...
      ') positive correlations (overall coeff = ' num2str(CC(2)) ')' newline ...
      'at ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz, from ' ...
      num2str(trange(1)) ' - ' num2str(trange(2)) ' s' newline ...
      'with trial #s within ' blksof7names{b} ':'])
    
    % plot topographic extent of change in this time-freq cluster
    cfg.ylim = frange; cfg.xlim = trange;
    subplot(1,2,2);
    ft_topoplotER(cfg,stat{b});
    
    print([figurefolder 'PosWavPowerCorr' num2str(n) blksof7names{b} '_ICA_' ...
      num2str(ratT.ratnums(r)) 'stat_indepsamplesregrT_NPindiv_ordered'], ...
      '-dpng','-r0')
%     close(fig)
  end
  
  %% visualize negative correlation clusters
  
  for n = 1:(neg.n + 1) % may plot one that's not considered significant
    if n > 10 || numel(neg.I) < n
      break  % out of for loop
    end
    cI = neg.I(n);  % cluster index
    I = find(stat{b}.negclusterslabelmat == cI);
    [ch,f,t] = ind2sub(size(stat{b}.negclusterslabelmat),I);
    bestch(n) = mode(ch);
    ch = unique(ch); f = unique(f); t = unique(t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat{b}.freq(min(f)) stat{b}.freq(max(f))];
    trange = [stat{b}.time(min(t)) stat{b}.time(max(t))];
    trlns = nan(1,size(smthfreq,2));
    clustpowpertrl = nan(numel(I),size(smthfreq,2));   % aggregate cluster power over trials
    for tr = find(~cellfun(@isempty,smthfreq(b,:)))
      trlns(tr) = smthfreq{b,tr}.trialinfo(1,1);
      clustpowpertrl(:,tr) = smthfreq{b,tr}.powspctrm(I);
    end
    clustpowpertrl(:,isnan(trlns)) = [];
    trlns(isnan(trlns)) = [];
    
    fig = figure(b*100+n); clf; fig.WindowStyle = 'docked';
    fig.PaperPositionMode = 'auto';
    % plot mean +/- std across trials for each chan/freq/timebin w/in cluster
    subplot(1,2,1); hold all
    X = repmat(trlns,size(clustpowpertrl,1),1);
    CC = corrcoef(X(:), clustpowpertrl(:));
    [pc,ErrorEst] = polyfit(X(:), clustpowpertrl(:), 3);   % fit cubic model
    pop_fit = polyval(pc,trlns,ErrorEst);
    plot(X(:),clustpowpertrl(:),'+', trlns,mean(clustpowpertrl),'-', ...
      trlns,pop_fit,'-');
    axis tight
    legend({'chan/time/freq bins','mean per trial','cubic fit'}, ...
      'Location','Best')
    
    title([strjoin(stat{b}.label(ch)) newline ...
      'have significant (p = ' num2str(stat{b}.negclusters(cI).prob) ...
      ') negative correlations (overall coeff = ' num2str(CC(2)) ')' newline ...
      'at ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz, from ' ...
      num2str(trange(1)) ' - ' num2str(trange(2)) ' s' newline ...
      'with trial #s within ' blksof7names{b} ':'])
    
    % plot topographic extent of change in this time-freq cluster
    cfg.ylim = frange; cfg.xlim = trange;
    subplot(1,2,2);
    ft_topoplotER(cfg,stat{b});
    
    print([figurefolder 'NegWavPowerCorr' num2str(n) blksof7names{b} '_ICA_' ...
      num2str(ratT.ratnums(r)) 'stat_indepsamplesregrT_NPindiv_ordered'], ...
      '-dpng','-r0')
%     close(fig)
  end
  
  %% plot stat
  
  fig = figure(b*100); clf; fig.WindowStyle = 'docked';
  cfg=[];
  cfg.channel         = mode(bestch(bestch ~= 0));
  if isempty(cfg.channel);    cfg.channel = ratT.mPFC{r};     end
  cfg.zlim            = quantile(stat{b}.stat(:),[0.025 0.975]);
  cfg.parameter       = 'stat';
  cfg.maskparameter   = 'mask';
  cfg.maskstyle       = 'outline';
  cfg.feedback        = 'no';
  plotFTmatrix_TEM(cfg,stat{b});
  title(['Correlations with ' blksof7names{b} ' Trial #s'])
  
  fig.PaperPositionMode = 'auto';
  print([figurefolder 'WavPowerCorrSpectrogram_' blksof7names{b} '_ICA_' ...
    num2str(ratT.ratnums(r)) 'stat_indepsamplesregrT_NPindiv_ordered'], ...
    '-dpng','-r0')
%   close(fig)
  
  comptime(b,3) = toc(starttime);
  esttime(3) = nanmean([esttime(3); comptime(b,3)]); % average into estimate of how long each block should take
end
output = comptime;
close(wb);
end
