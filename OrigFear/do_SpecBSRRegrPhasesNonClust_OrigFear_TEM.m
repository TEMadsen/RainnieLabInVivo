function [output] = do_SpecBSRRegrPhasesNonClust_OrigFear_TEM(spectype,r,postproctype)
% DO_SPECBSRREGRPHASESNONCLUST_ORIGFEAR_TEM tests 4 corrs btwn spcgrm & BSRs
% over whole phases (i.e., recording sessions) rather than blocks of 7 tones,
% using non-cluster-based statistics (e.g., bonferroni correction for multiple
% comparisons)
%
%   This function is for within-subject visualization & stats using
%   ft_statfun_indepsamplesregrT on any spectrograms (specified by input vars)
%
%   Depends on global variables defined in info_origfear.
%
% written 9/7/17 by Teresa E. Madsen, Ph.D.

%% import global metadata structure

global exptMeta

if isempty(exptMeta);   info_origfear;   end

%% check inputs & assign defaults

if nargin == 0  % like when I just hit "Run and Time" in the Editor tab
  spectype      = 'piWav';        % close to FT default wavelet params, but using pi for integer # of cycles, etc.
  r             = 1;              % poster rat
  postproctype  = {'bandBDAmean'};  % bandBDAmean is mean dB power w/in 12 freq bands, 3x 30s BDA tone bins
  % in cell because some will require additional inputs (a 2nd cell, e.g., {'smoothT',[6 3]})
end

%% create waitbar

wb = waitbar(0, ['Preparing to run SpecBSRRegrPhasesNonClust on rat #' ...
  num2str(exptMeta.ratT.ratnums(r))]);

%% prepare_neighbours determines what sensors may form clusters

neighbfile = [exptMeta.configfolder 'neighb_' exptMeta.ratT.MWA{r} '_' ...
  exptMeta.ratT.side{r} '.mat'];

if existfile_TEM(neighbfile)
  load(neighbfile)
else
  cfg = [];
  cfg.method        = 'distance';
  cfg.neighbourdist = 0.5;  % mm
  cfg.elecfile      = [exptMeta.configfolder 'elec_' exptMeta.ratT.MWA{r} ...
    '_' exptMeta.ratT.side{r} '.mat'];
  neighbours        = ft_prepare_neighbours(cfg);
  save(neighbfile,'neighbours')
end
if any(cellfun(@numel,{neighbours(:).neighblabel}) < 3) || ...
    any(cellfun(@numel,{neighbours(:).neighblabel}) > 5)
  warning('unexpected # of neighbors')
  cfg = [];
  cfg.neighbours = neighbours;
  cfg.layout = [exptMeta.configfolder 'layout_' exptMeta.ratT.MWA{r} '_' ...
    exptMeta.ratT.side{r} '.mat'];
  ft_neighbourplot(cfg)
  keyboard
end

%% load operant count BDA tones and convert to BSR

load([exptMeta.datafolder 'preprocBehav' filesep 'AllRatsBDAtones.mat']);

BSR = (cntBDAtones{r}(:,1) - cntBDAtones{r}(:,2)) ...
  ./ (cntBDAtones{r}(:,1) + cntBDAtones{r}(:,2)); %#ok<USENS> loaded from file
% if no operant behavior before or during tone, consider it complete suppression
BSR(isnan(BSR)) = 1;
% if operant behavior increases in response to tone, consider it no suppression
BSR(BSR < 0) = 0;

%% loop through each recording: prepping data, running stats, & plotting results
% then clear memory to avoid overload

comptime = nan(numel(exptMeta.phases)-1,3); % 3 steps: prep data, stats, & plot

if strcmp(postproctype{1},'bandBDAmean')
  bcfg = get_cfg_TEM('bandify','OrigFear');  % standard bands based on powers
  % of e + transitional bands between them (half-powers of e)
end

for p = 1:size(comptime,1)
  st = tic;   % start time for step 1: prep data
  waitbar((p-1)/(numel(exptMeta.phases)-1), wb, ['Preparing ' exptMeta.abbr{p} ...
    ' ' spectype ' ' postproctype{1} ' specs for regrT analysis']);
  
  % collect spectrograms of last 14 of each recording, except the very last, so
  % we can see how each neural response predicts the behavioral response to the
  % next tone
  tones = exptMeta.expected(p)-14:exptMeta.expected(p)-1;
  
  freq  = cell(numel(tones),1);   % 14 tones per block
  
  for tr = 1:numel(tones)   % trial # within freq
    %% define trial's neural data inputfile
    
    tn = tones(tr);   % trial # within phase
    
    inputfile = [exptMeta.datafolder postproctype{1} filesep ...
      num2str(exptMeta.ratT.ratnums(r)) filesep exptMeta.stimuli{1} num2str(tn) ...
      'of' exptMeta.abbr{p} '_raw' num2str(exptMeta.ratT.ratnums(r)) ...
      spectype 'TFAdBpow_' postproctype{1} '.mat'];
    
    if numel(postproctype) > 1
      switch postproctype{1}
        case 'smoothT'
          inputfile = insertBefore(inputfile, '.mat', ...
            [strrep(num2str(postproctype{2}(1)),'.','_') 'x' ...  % twin
            strrep(num2str(postproctype{2}(2)),'.','_')]);        % tshift
        case 'tfridge'
          inputfile = insertBefore(inputfile, '.mat', ...
            [strrep(num2str(postproctype{2}),'.','_') 'x']);      % wMult
        otherwise
          error('define modification to filename here')
      end
    end
    
    %% load inputfile, if it exists
    
    if ~existfile_TEM(inputfile)
      listing = dir(replaceBetween(inputfile, ... % get all .mat files in folder
        [num2str(exptMeta.ratT.ratnums(r)) filesep], '.mat', '*'));
      listing(~contains({listing.name},spectype)) = [];   % remove non-matches
      listing(~contains({listing.name}, ...  % check for both full & abbreviated
        [exptMeta.phases(p), exptMeta.abbr(p)])) = [];  % phase names
      if isempty(listing)
        warning(['Skipping all tones after ' num2str(tn) ' of ' ...
          exptMeta.phases{p} ' because none of the files in ' newline ...
          inputfile(1:find(inputfile == filesep,1,'last')) ...
          ' were found to contain phase and ' spectype '.'])
        break  % out of tone loop
      else
        disp(char({listing.name}))
        warning('clarify neural inputfile')
        keyboard
      end
    end
    
    freq{tr} = rmvlargefields_TEM(inputfile);
    
    %% add specs for bandBDAmean if applicable & missing
    
    if exist('bcfg','var') && ~isfield(freq{tr}.cfg,'bands')
      bcfg.previous = freq{tr}.cfg;
      freq{tr}.cfg = bcfg;
    end
    
    %% check for excessive NaNs
    
    if sum(isnan(freq{tr}.powspctrm(:)))/numel(freq{tr}.powspctrm(:)) > 0.2
      if r == 1 && p == 1 && tn == 12  % known bad trial
        freq{tr} = [];
      else
        warning(['powspctrm is ' num2str(round(...
          100*sum(isnan(freq{tr}.powspctrm(:)))/numel(freq{tr}.powspctrm(:)))) ...
          '% NaNs - omit?'])
        fig = figure('Name',[num2str(exptMeta.ratT.ratnums(r)) ...
          exptMeta.abbr{p} num2str(tn)]); clf; fig.WindowStyle = 'docked';
        fcfg = [];
        fcfg.zlim            = 'minmax';
        if exist('bcfg','var')
          fcfg.fticks        = bcfg.foi(contains(bcfg.bands,'('));
          fcfg.tticks        = bcfg.toi;
        end
        plotFTmatrix_TEM(fcfg,freq{tr});
        keyboard  % freq{tr} = [];
      end
    end
  end
  comptime(p,1) = toc(st);
  
  %% make sure all trials' data were prepared
  
  st = tic;   % start time for stats
  omit = cellfun(@isempty,freq);
  if sum(omit) > 1 || sum(~omit) < 13
    warning([exptMeta.abbr{p} ' only has ' num2str(sum(~omit)) ' trials'])
    if sum(~omit) == 0
      warning(['skipping ' exptMeta.abbr{p} ' for rat # ' ...
        num2str(exptMeta.ratT.ratnums(r))])
      continue  % to next block
    end
    keyboard  % figure out why, skip if < 3 good trials
  end
  
  %% define parameters of stat test & name outputfile
  
  cfg = [];
  cfg.statistic         = 'ft_statfun_indepsamplesregrT';
  cfg.method            = 'montecarlo';
  cfg.correctm          = 'max'; 
  cfg.tail              = 0;
  cfg.alpha             = 0.025;
  cfg.numrandomization  = 1000;
  cfg.parameter         = 'powspctrm';
  
  % regress spectrogram of each tone with BSR to the *NEXT* tone
  cfg.design  = BSR(sum(exptMeta.expected(1:p-1)) + tones(~omit) + 1);
  if numel(unique(cfg.design)) < 3
    warning(['skipping ' exptMeta.abbr{p} ...
      ' because of < 3 unique behavioral values'])
    continue  % to next block
  end
  
  cfg.ivar    = 1;  % uvar = ivar (between-UO)
  
  cfg.outputfile  = [exptMeta.datafolder 'SpecBSRRegrPhasesNonClust' filesep ...
    num2str(exptMeta.ratT.ratnums(r)) filesep exptMeta.abbr{p} ...
    inputfile(strfind(inputfile,'_raw'):end-4) '_vs_NextTrlBSR_stat' ...
    erase(cfg.statistic,'ft_statfun') '.mat'];
  if isfield(cfg,'correctm')
    switch cfg.correctm
      case 'cluster'
        if isfield(cfg,'clusterthreshold') && ...
            strcmp(cfg.clusterthreshold,'nonparametric_individual')
          cfg.outputfile = insertBefore(cfg.outputfile,'.mat','_NPindiv');
        end
        if isfield(cfg,'orderedstats') && istrue(cfg.orderedstats)
          cfg.outputfile = insertBefore(cfg.outputfile,'.mat','_ordered');
        end
      otherwise
        cfg.outputfile = insertBefore(cfg.outputfile,'.mat',['_' cfg.correctm]);
    end
  end
  
  %% run stat test
  
  if existfile_TEM(cfg.outputfile)
    stat = rmvlargefields_TEM(cfg.outputfile);
  else
    waitbar((p-2/3)/(numel(exptMeta.phases)-1), wb, ...
      ['Running ' erase(cfg.statistic,'ft_statfun_') ' on ' exptMeta.abbr{p} ...
      ' ' spectype ' vs. next trial BSR for rat #' num2str(exptMeta.ratT.ratnums(r))]);
    
    stat = ft_freqstatistics(cfg, freq{~omit});
    comptime(p,2) = toc(st);  % record time only if stats were calculated, not just loaded
  end
  
  %% print & plot significant correlations of spectrograms & BSR
  
  st = tic;
  waitbar((p-1/3)/(numel(exptMeta.phases)-1), wb, ...
    ['Running ' erase(stat.cfg.statistic,'ft_statfun_') ' on ' exptMeta.abbr{p} ...
    ' ' spectype ' vs. next trial BSR for rat #' num2str(exptMeta.ratT.ratnums(r))]);
  
  disp([newline 'Found chan/freq/time bins with significant positive (' ...
    num2str(sum(stat.mask(:) & stat.stat(:) > 0)) ') and negative (' ...
    num2str(sum(stat.mask(:) & stat.stat(:) < 0)) ')' newline ...
    'correlations with next trial BSR within ' exptMeta.abbr{p} newline])
  
  cfg =[];
  cfg.layout = [exptMeta.configfolder 'layout_' exptMeta.ratT.MWA{r} '_' ...
    exptMeta.ratT.side{r} '.mat'];
  cfg.parameter = 'stat';
  cfg.feedback  = 'no';
  cfg.gridscale = 2*numel(stat.label) + 1;
  xfit = linspace(min(stat.cfg.design),max(stat.cfg.design),15);
  
  %% visualize significant correlations
  
  if any(stat.mask)  
    sigT = table(find(stat.mask),'VariableNames',{'I'});
  else % if no significant correlations, just find strongest/closest to significance
    sigT = table(find(stat.prob == min(stat.prob(:)) | ...
      stat.stat == min(stat.stat(:)) | stat.stat == max(stat.stat(:))), ...
      'VariableNames',{'I'});
  end
  [sigT.stat,sortOrd] = sort(stat.stat(sigT.I));  % sort in stat order so colors will reflect red(+) blue(-) correlations
  sigT.I = sigT.I(sortOrd);
  sigT.prob = stat.prob(sigT.I);
  if size(sigT,1) > 30  % only plot the strongest correlations
    sigT = sigT([1:15,end-14:end],:);
  end
    
  [sigT.ch,sigT.f,sigT.t] = ind2sub(size(stat.mask),sigT.I);
    sigT.chlabel = stat.label(sigT.ch);
    if exist('bcfg','var') && isequal(stat.freq(:),bcfg.foi(:))
      sigT.flabel = strip(bcfg.bands(sigT.f));
    else
      sigT.flabel = strip(cellstr(num2str(stat.freq(sigT.f)',3)));
    end
    if exist('bcfg','var') && isequal(stat.time(:),bcfg.toi(:))
      sigT.tlabel = exptMeta.intervals(sigT.t);
    else
      sigT.tlabel = strip(cellstr(num2str(stat.time(sigT.t)',3)));
    end
    sigT.powpertrl = nan(numel(sigT.I),numel(freq));   % aggregate cluster power over trials
    for tr = find(~omit)'
      sigT.powpertrl(:,tr) = freq{tr}.powspctrm(sigT.I);
    end
    sigT.powpertrl(:,omit) = [];
    sigT.prob = stat.prob(sigT.I);
    CC = rowfun(@(x) corrcoef(stat.cfg.design, x), sigT, ...
      'InputVariables','powpertrl', 'OutputFormat','cell');
    sigT.CC = cellfun(@(x) x(2), CC); %, 'OutputFormat','uniform');
    sigT = sortrows(sigT,'CC','descend');
    sigT.color = mat2cell(flipud(jet(size(sigT,1))),ones(size(sigT,1),1));
    fitT = rowfun(@(x) polyfit(stat.cfg.design, x, 2), sigT, ...
      'InputVariables','powpertrl', 'OutputVariableNames',{'pc','ErrorEst'}); % fit square model
    sigT.pop_fit = cell2mat(rowfun(@(x1,x2) polyval(x1, xfit, x2), fitT, ...
      'InputVariables',{'pc','ErrorEst'}, 'OutputFormat','cell')); % evaluate model at various X values

    fig = figure('Name',[exptMeta.abbr{p} ' Correlations(s)']); clf; 
    fig.WindowStyle = 'docked'; fig.PaperPositionMode = 'auto';
      % plot next trl BSR vs. pow for each chan/freq/time bin
    h = plot(stat.cfg.design(:),sigT.powpertrl','o:', ...
      xfit(:),sigT.pop_fit','-');
    [h(1:size(sigT,1)).Color] = sigT.color{:};
    [h(size(sigT,1)+1:end).Color] = sigT.color{:};
    axis tight; xlabel('BSR in response to *NEXT* Tone'); ylabel('Power (dB)');
    legend(join([sigT.chlabel, sigT.flabel, sigT.tlabel, ...
      strip(cellstr(num2str(sigT.CC,3))), strip(cellstr(num2str(sigT.prob,3)))], ...
      {', ',', ',' Tones, coeff = ',', p = '}), 'Location','NorthEastOutside')
    
    title(['Strongest correlations with next trial BSR within ' ...
      exptMeta.abbr{p}])
    
    print([exptMeta.figurefolder 'StrongCorr_' ...
      stat.cfg.outputfile(find(stat.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
      '-dpng','-r0')
    %     close(fig)
  
  %% plot stat
  
  fig = figure('Name',exptMeta.abbr{p}); clf; fig.WindowStyle = 'docked';
  cfg=[];
  cfg.channel = cell(0,1);
  % get best channel for negative correlations...
  [~,I] = min(stat.stat(1:numel(exptMeta.lftchs),:,:));
  cfg.channel(end+1,1)        = stat.label(mode(I(:)));
  % ...in each region...
  if numel(stat.label) > numel(exptMeta.lftchs)
    [~,I] = min(stat.stat(numel(exptMeta.lftchs)+1:end,:,:));
    cfg.channel(end+1,1)      = stat.label(numel(exptMeta.lftchs) + mode(I(:)));
  end
  % ...and for positive correlations...
  [~,I] = max(stat.stat(1:numel(exptMeta.lftchs),:,:));
  cfg.channel(end+1,1)        = stat.label(mode(I(:)));
  % ...in each region
  if numel(stat.label) > numel(exptMeta.lftchs)
    [~,I] = max(stat.stat(numel(exptMeta.lftchs)+1:end,:,:));
    cfg.channel(end+1,1)      = stat.label(numel(exptMeta.lftchs) + mode(I(:)));
  end
  cfg.channel = unique(cfg.channel);  % also sorts them
  if isempty(cfg.channel)   % this shouldn't happen, but is an alternate chsel
    cfg.channel = [exptMeta.ratT.mPFC(r); exptMeta.ratT.BLA(r)];
  end
  cfg.zlim            = 'maxabs';
  cfg.parameter       = 'stat';
  cfg.maskparameter   = 'mask';
  cfg.maskstyle       = 'outline';
  cfg.feedback        = 'no';
  if exist('bcfg','var')
    cfg.fticks        = bcfg.foi(contains(bcfg.bands,'('));
    cfg.tticks        = bcfg.toi;
  end
  plotFTmatrix_TEM(cfg,stat);
  
  fig.PaperPositionMode = 'auto';
  print([exptMeta.figurefolder 'CorrSpectrogram_' ...
    stat.cfg.outputfile(find(stat.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
    '-dpng','-r0')
  %   close(fig)
  
  comptime(p,3) = toc(st);
end   % for each block

%% clean up

output = comptime;
close(wb);
% keyboard  % pause to review figures

end   % function
