%% extract features from spectrograms & coherograms for stats, correlation
% with behavior, and/or tracking over time

% CleanSlate  % provides a clean slate to start working with new data/scripts

% wb = waitbar(0,'Initializing...');   % will display progress through loop
% h = findall(wb,'Type','text');
% h(1).Interpreter = 'none';

info_origfear   % load all background info & final parameters

zthresh       = 15;         % analyze data cleaned using this artifact threshold
cthresh       = 3;          % and this clipping threshold
spectype      = 'mtmfft';   % w/ overlapping trials as moving windows
winlen        = 2;          % movwin(1) = window length (s)
normtype.pow  = 'ZdB';      % pseudo-z-score of dB transformed spectrograms
normtype.coh  = 'Zraw';     % pseudo-z-score of coherograms (no transform)
redo          = false;      % to ignore & overwrite old files
plt           = true;       % to plot visualization of extracted features
skiprats      = [];

% errlog  = cell(size(ratT,1), sum(expected));   % MEs saved in rat-trl cells
% rattime = NaN(size(ratT,1), 1);   % will save computation time per rat
fields = fieldnames(normtype);

% %% experiment with poster rat's best data
% 
% r = 1;    % 200
% p = 2;    % recall
% tn = 1;   % 1st tone
% s = 1;    % CS+
% sf = 1;   % pow
% chnames = 'label';  % for pow
% % sf = 2;   % coh
% % chnames = 'labelcmb';  % for coh
% % winlen = 2^nextpow2(4*fsample);   % 4s windows
% % tlim = [-10 10];  % just for a sample of how it does at tone onset
% %
% % %% load cleaned data
% %
% % datainputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
% %     'AllClean_z' num2str(zthresh) '_clip' num2str(cthresh) ...
% %     '_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];
% %
% % % load(datainputfile)
% %
% % ch = ismember(data.(chnames),ratT.mPFC{r});
% % tr = ismember(data.trialinfo,[tn s p],'rows');
% % td = data.time{tr};
% % tndx = td >= tlim(1) & td <= tlim(2);
% % dat = data.trial{tr}(ch,tndx);
% % td = td(tndx);
% %
% % % clearvars data
% %
% % %% experiment with "synchrosqueezed" Fourier transform for noise reduction
% % % may not be appropriate for neural signals because of the pink noise background
% %
% % [sst,f] = fsst(dat,fsample,winlen);   % window doesn't seem to matter much
% %
% % sst = abs(sst);
% % figure; imagesc(td,f,sst); colormap(jet); colorbar; axis xy;
% % % plotting log scale power here looks really strange because of so many 0s?
% %
% % % nr = 3;
% % % nfb = find(foi >= 6,1,'first') - find(foi <= 4,1,'last');
% % % nfb = find(f == max(fridge(:,1)));
% % fridge = tfridge(sst,f,1); %,'NumRidges',nr,'NumFrequencyBins',nfb);
% % % 1st goes from 8-11 Hz before tone to 3-4 Hz during
% % % 2nd swaps from 1-6 Hz before tone to 7-13 Hz during
% %
% % ylim([min(f) max(fridge(:))]); hold all;
% % for n = 1:nr
% %   plot(td,fridge(:,n));
% % end
% 
% %% or spectro/coherograms
% 
% freqinputfile = [datafolder num2str(ratT.ratnums(r)) filesep stimuli{s} ...
%   num2str(tn) 'of' phases{p} '_' num2str(ratT.ratnums(r)) spectype 'TFA' ...
%   fields{sf} num2str(winlen) 's.mat'];
% 
% S = load(freqinputfile);
% varname = fieldnames(S);  % may be freq (for pow) or stat (for coh)
% freq = S.(varname{1});  % there should only be 1
% S = [];
% 
% % goodchs{r} = freq.(chnames);
% ch = ismember(freq.(chnames),ratT.mPFC{r});
% % ch = sum(ismember(freq.(chnames),ratT.mPFC{r}),2) + ...
% %   sum(ismember(freq.(chnames),ratT.BLA{r}),2) == 2;
% tfm = abs(squeeze(freq.([fields{sf} 'spctrm'])(1,ch,:,:)));
% foi = freq.freq;
% toi = freq.time;
% w = ft_findcfg(freq.cfg,'tapsmofrq');   % finds field w/in nested previous cfgs
% 
% clearvars freq
% 
% %% experiment w/ ridge extraction to identify delta frequency & strength
% % have to start with unnormalized (& untransformed) spectrogram (expects energy)
% 
% % single ridge with no penalty for switching frequencies is simple & works fine
% % for this strong delta signal - ONLY FOR RAW POWSPCTRM, not cohspctrm, & not
% % normalized (those may work with some penalty, but I can't think of an
% % objective way to set it)
% 
% % tfmsc = tfm.*repmat(foi',1,size(tfm,2));  % scale by 1/f (this is only going
% % to work for delta anyway, so don't bother)
% mx = max(abs(tfm(:)))*ones(size(toi));
% % nr = 3;
% % nfb = find(foi >= 6,1,'first') - find(foi <= 4,1,'last');
% % nfb = find(foi == max(fridge(:,1)));
% fridge = tfridge(tfm,foi); %,0.01,'NumRidges',nr,'NumFrequencyBins',nfb);
% % fridge = tfridge(tfm,foi,1);
% % 1st ranges 1.8-10.1 Hz before tone, drifts from 3.5 to 2.8 Hz during tone,
% % ranges 2.3-5 Hz after tone
% mf = find(foi > (max(fridge(:)) + w),1,'first');
% if isempty(mf)
%   mf = numel(foi);
% end
% figure; imagesc(toi, foi(1:mf), tfm(1:mf,:));
% colormap(jet); colorbar; axis xy;
% hold all;
% % for n = 1:nr
% plot(toi,fridge,'w');   % (:,n)
% plot(toi,fridge - w,'--w');
% plot(toi,fridge + w,'--w');
% % end
% 
% %% quantify ridge prominence
% 
% rpow = NaN(size(toi));
% bandpow = NaN(size(toi));
% % lfpow = NaN(size(toi));
% for t = 1:numel(toi)
%   rpow(t) = sum(tfm(foi >= (fridge(t) - w) & foi <= (fridge(t) + w), t), ...
%     1, 'includenan');
%   bandpow(t) = sum(tfm(foi >= (fridge(t) - 3*w) & foi <= (fridge(t) + 3*w), t), ...
%     1, 'includenan');
%   %   lfpow(t) = sum(tfm(foi <= 30, t), 1, 'includenan');
% end
% rtun = rpow./(bandpow-rpow);  % tuning of peak band compared to same size bands above & below
% % rprom = rpow./lfpow;  % prominence within low freq band (don't know for sure that all peaks will be <30 Hz
% rprom = rpow./sum(tfm,1,'includenan');  % prominence w/in total spectral power
% figure; plot(toi,rpow); hold all;
% yyaxis right; plot(toi,rprom,toi,rtun);
% 
% %% correlate freq & tuning
% 
% figure; scatter(fridge(toi <= 0), rprom(toi <= 0)); hold all;
% scatter(fridge(toi > 0 & toi <= 30), rprom(toi > 0 & toi <= 30))
% scatter(fridge(toi > 30), rprom(toi > 30))
% legend({'Before','During','After'})
% 
% figure; scatter(fridge(toi <= 0), rpow(toi <= 0)); hold all;
% scatter(fridge(toi > 0 & toi <= 30), rpow(toi > 0 & toi <= 30))
% scatter(fridge(toi > 30), rpow(toi > 30))
% legend({'Before','During','After'})
% 
% figure; scatter(fridge(toi <= 0), rtun(toi <= 0)); hold all;
% scatter(fridge(toi > 0 & toi <= 30), rtun(toi > 0 & toi <= 30))
% scatter(fridge(toi > 30), rtun(toi > 30))
% legend({'Before','During','After'})
% 
% %% explore similar FT functions to identify a reasonable data structure
% 
% % maybe something like
% stat = struct('peakf',fridge', 'peakpow',rpow, 'peakprom',rprom, 'time',toi);
% % but with dimord subj_rpt_chan_time? do any functions use both subj & rpt?
% % I guess I could just add rat # as a column in trialinfo
% 
% % FT_FREQDESCRIPTIVES computes descriptive univariate statistics of the
% % frequency or time-frequency decomposition of the EEG/MEG signal, thus the
% % power spectrum and its standard error - only across trials or subjects (1st
% % dim, which is removed from output).
% 
% % FT_FREQSTATISTICS computes significance probabilities and/or critical values
% % of a parametric statistical test or a non-parametric permutation test - across
% % trials, subjects, or separate inputs, grouped according to a custom design
% % matrix.
% 
% % http://www.fieldtriptoolbox.org/faq/how_can_i_test_for_correlations_between_neuronal_data_and_quantitative_stimulus_and_behavioural_variables?s[]=design
% % http://www.fieldtriptoolbox.org/walkthrough#statistics
% 
% cfg.design = freq.trialinfo(:,[1 3])';  % tone #s & phases, could use freezing instead
% cfg.ivar = 1:2;   % both independent variables
% cfg.uvar = 3;   % units of observation, if I add rat # as a 3rd row of design after concatenating trials
% 
% % for stats, it might be easier to just do mtmfft on before, during, and after,
% % but that would require way too much tapering (or averaging or whatever other
% % kind of smoothing across frequencies)
% 
% % output from freqstats has same dimord as input, minus rpt/subj dim
% %

%% explore statistical output of FT_ExtractFeaturesTFA_OrigFear_TEM

load([datafolder 'powridges' num2str(winlen) 's_agg.mat'])

metrics = ridgeT.Properties.VariableNames(end-3:end);

display = 'on';

%% loop

for r = setdiff(find(~ratT.excluded)',skiprats)
  for ch = 1:numel(allchs)
    %% simplify table
    partialT = ridgeT(ismember(ridgeT.rat,num2str(ratT.ratnums(r)),'rows') ...
      & ismember(ridgeT.channel,allchs{ch},'rows'),3:end);
    if size(partialT,1) == 0
      disp('No data for this rat and channel')
      continue
    end
    partialT.block = discretize(partialT.trial, blkedges, ...
      'categorical',blknames, 'IncludedEdge','right');
    partialT.reltime = discretize(partialT.time, [-inf 0 30 inf], ...
      'categorical', {'Before','During','After'}, 'IncludedEdge','right');
    partialT(ismember(partialT.block,'BL','rows'),:) = [];
    %     crosstab(partialT.reltime,partialT.block)
    
    %% run stats
    
    %     [d,p,stats] = manova1(partialT{ismember(partialT.reltime,'During','rows'), metrics}, ...
    %       partialT.block(ismember(partialT.reltime,'During','rows')));
    %     manovacluster(stats)
    for v = 1:numel(metrics)
      [p,tbl,stats,terms] = anovan(partialT.(metrics{v}), ...
        [{partialT.reltime} {partialT.block}], 'model','full', ...
        'varnames',{'reltime','block'}, 'display','off');
      posthoc.(metrics{v}).anovaT{ch,r} = tbl;  % copy ANOVA table into aggregate array
      
      [c,m,h,gnames] = multcompare(stats,'Dimension',[1 2], 'display',display);
      posthoc.(metrics{v}).p(:,ch,r) = c(:,6);  % copy posthoc p values into aggregate array
      posthoc.(metrics{v}).m(:,ch,r) = m(:,1);  % copy mean per block-time into agg
      
      if strcmp(display,'on')
        print([figurefolder num2str(ratT.ratnums(r)) allchs{ch} spectype ...
          num2str(winlen) 'sTFApowridges_' metrics{v} '_posthoc'],'-dpng')
      end
    end   % for all metrics
    if ~isfield(posthoc,'gnames')
      posthoc.gnames = gnames;    % group names
    end
    if ~isfield(posthoc,'gcomp')
      posthoc.gcomp = c(:,1:2);   % order of group comparisons
    end
  end   % for all chs
end   % for all rats

% save progress on stats   
save(['C:\Users\Teresa\Documents\Work\OrigFear\powridges' ...
  num2str(winlen) 's_stats.mat'], 'posthoc');
    