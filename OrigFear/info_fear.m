%% info needed for analyzing fear data, including all rats filenames
%#ok<*SNASGU> just provides background info for other analysis protocols

datafolder    = [tserver 'Analyses' filesep 'TempData' filesep];
configfolder  = [tserver 'Analyses' filesep 'MWAconfigs' filesep];

% put new figures in a folder named by date, in the format 'yyyy-mm-dd'
figurefolder  = [tserver 'Figures' filesep 'TempFigures' filesep ...
  datestr(now,29) filesep];
if ~exist(figurefolder,'dir')
  mkdir(figurefolder)
end

%% define MWA configuration(s)

MWA = 'OldBatchOldFear';      % name for this microwire array configuration

lftchs = {'AD01','AD05'; ...  % mPFC array configuration, where increasing
  'AD02','AD06'; ...  % rows indicate electrodes ordered A -> P,
  'AD03','AD07'; ...  % and increasing columns indicate those
  'AD04','AD08'};     % ordered L -> R

rtchs = {'AD09','AD13'; ...   % BLA array configuration, where increasing
  'AD10','AD14'; ...   % rows indicate electrodes ordered A -> P,
  'AD11','AD15'; ...   % and increasing columns indicate those
  'AD12','AD16'};      % ordered L -> R

if exist([configfolder 'elec_' MWA '_Right.mat'],'file') && ...
    exist([configfolder 'layout_' MWA '_Right.mat'],'file')
  disp('using existing elec & layout files')
else
  cfg = [];   % all arrays were configured & implanted the same for this expt
  
  % array & target info
  cfg.atlascoord = [2.7, 0.50, -5.00; ... 	% AP/ML/DV relative to bregma
    -3.0, 5.20, -8.50];      % dim 1/2/3 for below
  cfg.angle     = [-6; 6];  % + angle means approach from medial, in degrees
  cfg.angledim  = [2; 2];   % both probes angled along ML dim
  cfg.elecspac  = [0.25, 0.25; ...  % in AP dim, then ML dim, for each probe
    0.25, 0.25];      % in mm
  
  cfg.label = {lftchs; rtchs}; 	% 2 cell arrays nested in another cell array
  cfg.side = {'Right';'Right'}; % which side of the brain each probe was on
  
  % calculate sensor positions & define layout
  MWAlayout(cfg,[MWA '_Right'],configfolder);
end

%% experiment metadata

% each row # here corresponds to cell row (1st dim) in other variables
ratnums = [200; 202; 203; 204; 205; 210; 211; 212];
excluded = logical([0; 1; 0; 0; 0; 0; 1; 1]);   % see notes for why
side = {'Right';'Right';'Right';'Right';'Right';'Right';'Right';'Right'};
chmPFC = cell(numel(ratnums),1);    % best channel to use for CFC
chBLA = cell(numel(ratnums),1);     % best channel to use for CFC
finalch = false([numel(ratnums),1]);
ratT = table(ratnums, excluded, side, chmPFC, chBLA, finalch);
clearvars ratnums excluded side chmPFC chBLA finalch

% each column # here corresponds to cell column (2nd dim) in other variables
phases = {'Conditioning', 'Recall', 'Extinction'};  % add 'Baseline',

abbr = {'FC', 'Ext1', 'Ext2'};  % add 'BL', % for behavioral data

blocks = {'Habituation', 'Fear Recall', 'Extinction Recall'; ...
  'Acquisition', 'Early Extinction', 'Late Extinction'};

% each row # here corresponds to the numerical code that will be saved in
% trl and data.trialinfo to identify which subtrials correspond to which
% interval types
intervals = {'Before'; 'During'; 'After'};

% each row # here corresponds to the numerical code that will represent
% which region's data is being used for phase or amplitude source data
regions = {'mPFC'; 'BLA'};

% initialize variables
goodchs   = cell(size(ratT,1),1);  % cellstr can't fit in 1 cell of a table
notdeadchs = cell(size(ratT,1),1);
notes     = cell(size(ratT,1),numel(phases));
toneTS    = cell(size(ratT,1),numel(phases));
nexfile   = cell(size(ratT,1),numel(phases));

% any extra comments about each rat
notes{1,3} = 'tone timestamps for 200''s Extinction are in "toneTS{1,3}"';
toneTS{1,3} = [486759 863759 1204759 1420759 1628759 1976759 2142759 ...
  2350759 2673759 2906759 3088759 3459759 3731760 3981760 4294760];
notes{2,3} = 'failed extinction';
notes{7,2} = 'lost headcap before recall';
notes{8,2} = 'failed to consolidate & express fear learning';

%% channels to use (or exclude if prefaced by '-') for each rat
% defaults to 'AD*' if empty
goodchs{1} = {'AD*','-AD01',...
  '-AD09','-AD13','-AD14','-AD15','-AD16'};
notdeadchs{1} = {'AD*','-AD16'};   % ,'-AD13' has 3.3s (vs 5.8) of clipping
ratT.chmPFC(1) = {'AD05'};
ratT.chBLA(1) = {'AD11'};
ratT.finalch(1) = false;
goodchs{2} = {'AD*','-AD04','-AD07','-AD08',...
  '-AD09','-AD15','-AD16'};
notdeadchs{2} = {'AD*'}; % was ,'-AD04','-AD07','-AD08','-AD15'
% but NONE of these cause more than 152 ms of clipping within core trials
ratT.chmPFC(2) = {'AD05'};
ratT.chBLA(2) = {'AD14'};
ratT.finalch(2) = false;
goodchs{3} = {'AD*','-AD04','-AD05','-AD06','-AD07','-AD08',...
  '-AD09','-AD10','-AD11','-AD12','-AD13','-AD16'};
% 6 & 8 were disabled during recording
notdeadchs{3} = {'AD*','-AD06','-AD08','-AD13','-AD16'}; % maybe ,'-AD07','-AD09'
% but these clip the same amount as others
ratT.chmPFC(3) = {'AD03'};
ratT.chBLA(3) = {'AD14'};
ratT.finalch(3) = false;
goodchs{4} = {'AD*','-AD01','-AD02','-AD04','-AD05','-AD08',...
  '-AD09','-AD16'};
notdeadchs{4} = {'AD*','-AD16'};   % others clip similar amounts (medium)
ratT.chmPFC(4) = {'AD07'};
ratT.chBLA(4) = {'AD10'};
ratT.finalch(4) = false;
goodchs{5} = {'AD*','-AD04','-AD05','-AD07','-AD08',...
  '-AD09','-AD10','-AD11','-AD12','-AD15','-AD16'};
notdeadchs{5} = {'AD*','-AD16'};   % others clip similar amounts (not much)
ratT.chmPFC(5) = {'AD06'};
ratT.chBLA(5) = {'AD13'};
ratT.finalch(5) = false;
goodchs{6} = {'AD*','-AD01','-AD02','-AD04','-AD05','-AD08',...
  '-AD09','-AD11','-AD13','-AD15','-AD16'};
notdeadchs{6} = {'AD*'};  % 12-14 clip >9s, 11-15 >7s, 9-10 >5s, 1-4&16 >3s
ratT.chmPFC(6) = {'AD03'};
ratT.chBLA(6) = {'AD14'};
ratT.finalch(6) = false;
goodchs{7} = {'AD*','-AD01','-AD02','-AD03','-AD04','-AD08',...
  '-AD09','-AD13','-AD15','-AD16'};
ratT.chmPFC(7) = {'AD05'};
ratT.chBLA(7) = {'AD14'};
ratT.finalch(7) = false;
goodchs{8} = {'AD*','-AD01','-AD02','-AD03','-AD05','-AD08',...
  '-AD12','-AD13','-AD15','-AD16'};
notdeadchs{8} = {'AD*','-AD01','-AD02','-AD13','-AD15','-AD16'};
% these clip >17s, compared to <4s for the rest
ratT.chmPFC(8) = {'AD06'};
ratT.chBLA(8) = {'AD11'};
ratT.finalch(8) = true;

%% filenames
% Habituation & Acquisition (fear conditioning)
nexfile{1,1} = {[tserver 'PlexonData Backup' filesep '200' filesep ...
  '200_091003_0000-alignedLFPs.nex']; [tserver 'PlexonData Backup' ...
  filesep '200' filesep '200_091003_0001-aligned.nex']};
% Can't use PlexUtil or NeuroExplorer merged files because either FieldTrip
% crashes or the timestamps are off after the merge.
% FT_preproc_Fear_TEM.m can handle this cellstr.
nexfile{2,1} = [tserver 'PlexonData Backup' filesep '202' filesep ...
  '202_091005_0002-aligned.nex'];
nexfile{3,1} = [tserver 'PlexonData Backup' filesep '203' filesep ...
  '203_091204_0001-alignedLFPs.nex'];
nexfile{4,1} = [tserver 'PlexonData Backup' filesep '204' filesep ...
  '204_100313_0119-aligned.nex'];
nexfile{5,1} = [tserver 'PlexonData Backup' filesep '205' filesep ...
  '205_100311_0110-alignedLFPs.nex'];
nexfile{6,1} = [tserver 'PlexonData Backup' filesep '210' filesep ...
  '210_100611_0060-alignedLFPs.nex'];
nexfile{7,1} = [tserver 'PlexonData Backup' filesep '211' filesep ...
  '211_100514_0000-aligned.nex'];
nexfile{8,1} = [tserver 'PlexonData Backup' filesep '212' filesep ...
  '212_100604_0044-alignedLFPs.nex'];

% Recall (extinction 1)
nexfile{1,2} = [tserver 'PlexonData Backup' filesep '200' filesep ...
  '200_091006_0001-aligned.nex'];
nexfile{2,2} = [tserver 'PlexonData Backup' filesep '202' filesep ...
  '202_091008_0000-aligned.nex'];
nexfile{3,2} = [tserver 'PlexonData Backup' filesep '203' filesep ...
  '203_091207_0000-aligned.nex'];
nexfile{4,2} = [tserver 'PlexonData Backup' filesep '204' filesep ...
  '204_100316_0127-aligned.nex'];
nexfile{5,2} = [tserver 'PlexonData Backup' filesep '205' filesep ...
  '205_100314_0121-aligned.nex'];
nexfile{6,2} = [tserver 'PlexonData Backup' filesep '210' filesep ...
  '210_100614_0066-aligned.nex'];
nexfile{8,2} = [tserver 'PlexonData Backup' filesep '212' filesep ...
  '212_100607_0049-aligned.nex'];

% Extinction (extinction 2)
nexfile{1,3} = [tserver 'PlexonData Backup' filesep '200' filesep ...
  '200_091007_0000-aligned.nex'];
nexfile{2,3} = [tserver 'PlexonData Backup' filesep '202' filesep ...
  '202_091009_0000-aligned.nex'];
nexfile{3,3} = [tserver 'PlexonData Backup' filesep '203' filesep ...
  '203_091208_0000-aligned.nex'];
nexfile{4,3} = [tserver 'PlexonData Backup' filesep '204' filesep ...
  '204_100317_0131-aligned.nex'];
nexfile{5,3} = [tserver 'PlexonData Backup' filesep '205' filesep ...
  '205_100315_0124-aligned.nex'];
nexfile{6,3} = [tserver 'PlexonData Backup' filesep '210' filesep ...
  '210_100615_0068-aligned.nex'];
nexfile{8,3} = [tserver 'PlexonData Backup' filesep '212' filesep ...
  '212_100608_0053-aligned.nex'];

%% complete workflow:
% 0) align original .plx files, save as .nex
% 1) FT_preproc_Fear_TEM - Load all data from -aligned.nex files into
% FieldTrip format & merge into 1 dataset per rat, using new trialfun_TEM.m
% and supporting a cell array of strings for multiple filenames within one
% phase (to avoid merged NEX files, which are hard to read correctly).
% 2) FT_rejectArt_Fear_TEM
% 3) FT_ica_Fear_TEM
% 4) spectra/coher
% 5) cross-frequency
% 6) compare to behavior
% 7) granger causality?