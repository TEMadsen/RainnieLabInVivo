function [output] = do_mtmconvol_OrigFear_TEM( r )
% DO_MTMCONVOL_ORIGFEAR_TEM conducts the time-frequency analysis by trials
%
%   Default parameters documented below.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/8/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder ratT fsample stimuli phases

%% create waitbar

wb = waitbar(0,['Preparing for mtmconvol on rat #' num2str(ratT.ratnums(r))]);
tic

%% define analysis parameters

mtmc = get_cfg_TEM('mtmconvol', 'OrigFear');
% mtmc = 
%   struct with fields:
% 
%        method: 'mtmconvol'
%        output: 'fourier'
%         taper: 'dpss'
%           foi: [1�176 double] 0.7071 - 304.4370 Hz
%     tapsmofrq: [1�176 double] 0.6401 - 17.8514 Hz
%     t_ftimwin: [1�176 double] 0.2241 - 6.2493 s
%           toi: [1�1201 double]
%           pad: 131.0720

%% Load inputfile

inputfile = [datafolder 'preproc' filesep num2str(ratT.ratnums(r)) filesep ...
  'AllClean_ICA_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];

data = rmvlargefields_TEM(inputfile);   % strip excess historical metadata

comptime = nan(numel(data.trial),1);
esttime = 7644;  % in s, based on 1st run:  sum(comptime)

if ~isfield(data,'fsample') || isempty(data.fsample)
  data.fsample = fsample;
end

%% define baseline outputfile

outputfile = [datafolder 'mtmconvol' filesep num2str(ratT.ratnums(r)) ...
  filesep 'Baseline_ICA_' num2str(ratT.ratnums(r)) 'mtmconvolTFAfourier.mat'];

%% check if it exists

% if ~existfile_TEM(outputfile)
  %% separate baseline
  
  tic
  waitbar(0, wb, ['Calculating baseline mtmconvol for rat #' ...
    num2str(ratT.ratnums(r))]);
  
  cfg = [];
  cfg.trials  = data.trialinfo(:,2) == 0; 	% wherever there's no stimulus
  
  tmpdat = ft_selectdata(cfg, data);
  
  assert(~any(any(isnan([tmpdat.trial{:}]))))
  
  %% break baseline data into 20 sub-trials of length mtmc.pad
  
  cfg = [];
  cfg.length    = mtmc.pad;
  cfg.overlap   = 0;
  
  tmpdat = ft_redefinetrial(cfg, tmpdat);
  
  cfg = [];
  cfg.trials  = 1:20; 	% all rats have at least 20
  
  tmpdat = ft_selectdata(cfg, tmpdat);
  
  %% realign all trials to start at time 0 to avoid overburdening memory
  % otherwise, it pads each trial out to the length of time covered by all the
  % trials together
  
  cfg = [];
  cfg.offset  = [0; -cumsum(diff(tmpdat.sampleinfo(:,1)))];
  
  tmpdat = ft_redefinetrial(cfg, tmpdat);
  
  %% calculate baseline freqanalysis
  
  cfg = mtmc;   % default params defined in get_cfg_TEM, adjust for baseline
  cfg.toi = (tmpdat.time{1}(1) + mtmc.t_ftimwin(1)): ...
    (mtmc.toi(2)-mtmc.toi(1)):(tmpdat.time{1}(end) -  mtmc.t_ftimwin(1));
  cfg.keeptrials  = 'yes';
  cfg.outputfile  = outputfile;
  
  freq = ft_freqanalysis(cfg, tmpdat);
  
  %% clear memory
  
  clearvars freq
  comptime(end) = toc;
% end

%% trim tone-trials to core mtmc.pad time

cfg = [];
cfg.trials  = data.trialinfo(:,2) == 1;   % wherever there's a CS+

tmpdat = ft_selectdata(cfg, data);

cfg = [];
cfg.toilim  = [median(mtmc.toi) - mtmc.pad/2 + 1/data.fsample, ...
  median(mtmc.toi) + mtmc.pad/2 - 1/data.fsample];   % cut 2 samples to avoid rounding errors

tmpdat = ft_redefinetrial(cfg, tmpdat);

%% loop through all tone trials

trlinfo = unique(tmpdat.trialinfo,'rows','stable');
for tr = 1:size(trlinfo,1)  % trial # across all phases
  %% define trial outputfile
  
  tn = trlinfo(tr,1);   % trial number within phase
  s = trlinfo(tr,2);    % stimulus type (1 for CS+ or 0 for none)
  p = trlinfo(tr,3);    % phase number
  
  outputfile = [datafolder 'mtmconvol' filesep num2str(ratT.ratnums(r)) ...
    filesep stimuli{s} num2str(tn) 'of' phases{p} '_ICA_' ...
    num2str(ratT.ratnums(r)) 'mtmconvolTFAfourier.mat'];
  
  %% check if it exists
  
%   if ~existfile_TEM(outputfile)
    %% calculate trial spectrogram
    
  tic
  waitbar(sum(comptime,'omitnan')/esttime, ...
    wb, ['Calculating mtmconvol for tone trial #' num2str(tr) ', rat #' ...
      num2str(ratT.ratnums(r))]);
    
    cfg             = mtmc;
    cfg.trials      = ismember(tmpdat.trialinfo, trlinfo(tr,:), 'rows');
    cfg.outputfile  = outputfile;
    
    freq = ft_freqanalysis(cfg,tmpdat);
    
    %% clear memory
    
    clearvars freq
    comptime(tr) = toc;
    esttime = (numel(comptime)+19)*sum(comptime,'omitnan')/(20+tr);

%   end
end

output = comptime;
close(wb)