function [output] = do_SpecBehavRegrPhases_OrigFear_TEM(spectype,r,postproctype)
% DO_SPECBEHAVREGRPHASES_ORIGFEAR_TEM tests 4 linear trends btwn spcgrm & behavs
% over whole phases (i.e., recording sessions) rather than blocks of 7 tones
%
%   This function is for within-subject visualization & stats using
%   ft_statfun_indepsamplesregrT on any spectrograms (specified by input vars)
%
%   Depends on global variables defined in info_origfear.
%
% written 9/5/17 by Teresa E. Madsen, Ph.D.

%% import global metadata structure

global exptMeta

if isempty(exptMeta);   info_origfear;   end

%% check inputs & assign defaults

if nargin == 0  % like when I just hit "Run and Time" in the Editor tab
  spectype      = 'piWav';        % close to FT default wavelet params, but using pi for integer # of cycles, etc.
  r             = 1;              % poster rat
  postproctype  = {'bandBDAmean'};  % bandBDAmean is mean dB power w/in 12 freq bands, 3x 30s BDA tone bins
  % in cell because some will require additional inputs (a 2nd cell, e.g., {'smoothT',[6 3]})
end

%% create waitbar

wb = waitbar(0, ['Preparing to run SpecBehavRegrPhases on rat #' ...
  num2str(exptMeta.ratT.ratnums(r))]);

%% prepare_neighbours determines what sensors may form clusters

neighbfile = [exptMeta.configfolder 'neighb_' exptMeta.ratT.MWA{r} '_' ...
  exptMeta.ratT.side{r} '.mat'];

if existfile_TEM(neighbfile)
  load(neighbfile)
else
  cfg = [];
  cfg.method        = 'distance';
  cfg.neighbourdist = 0.5;  % mm
  cfg.elecfile      = [exptMeta.configfolder 'elec_' exptMeta.ratT.MWA{r} ...
    '_' exptMeta.ratT.side{r} '.mat'];
  neighbours        = ft_prepare_neighbours(cfg);
  save(neighbfile,'neighbours')
end
if any(cellfun(@numel,{neighbours(:).neighblabel}) < 3) || ...
    any(cellfun(@numel,{neighbours(:).neighblabel}) > 5)
  warning('unexpected # of neighbors')
  cfg = [];
  cfg.neighbours = neighbours;
  cfg.layout = [exptMeta.configfolder 'layout_' exptMeta.ratT.MWA{r} '_' ...
    exptMeta.ratT.side{r} '.mat'];
  ft_neighbourplot(cfg)
  keyboard
end

%% load operant count BDA tones and convert to BSR

load([exptMeta.datafolder 'preprocBehav' filesep 'AllRatsBDAtones.mat']);

BSR = (cntBDAtones{r}(:,1) - cntBDAtones{r}(:,2)) ...
  ./ (cntBDAtones{r}(:,1) + cntBDAtones{r}(:,2)); %#ok<USENS> loaded from file
% if no operant behavior before or during tone, consider it complete suppression
BSR(isnan(BSR)) = 1;
% if operant behavior increases in response to tone, consider it no suppression
BSR(BSR < 0) = 0;

%% loop through each recording: prepping data, running stats, & plotting results
% then clear memory to avoid overload

comptime = nan(numel(exptMeta.phases)-1,3); % 3 steps: prep data, stats, & plot

if strcmp(postproctype{1},'bandBDAmean')
  bcfg = get_cfg_TEM('bandify','OrigFear');  % standard bands based on powers 
  % of e + transitional bands between them (half-powers of e)
end

for p = 1:size(comptime,1)
  st = tic;   % start time for step 1: prep data
  waitbar((p-1)/(numel(exptMeta.phases)-1), wb, ['Preparing ' exptMeta.abbr{p} ...
    ' ' spectype ' ' postproctype{1} ' specs for regrT analysis']);
  
  % collect spectrograms of last 14 of each recording, except the very last, so
  % we can see how each neural response predicts the behavioral response to the
  % next tone
  tones = exptMeta.expected(p)-14:exptMeta.expected(p)-1;  
  
  freq  = cell(numel(tones),1);   % 14 tones per block
  
  for tr = 1:numel(tones)   % trial # within freq
    %% define trial's neural data inputfile
    
    tn = tones(tr);   % trial # within phase
        
    inputfile = [exptMeta.datafolder postproctype{1} filesep ...
      num2str(exptMeta.ratT.ratnums(r)) filesep exptMeta.stimuli{1} num2str(tn) ...
      'of' exptMeta.abbr{p} '_raw' num2str(exptMeta.ratT.ratnums(r)) ...
      spectype 'TFAdBpow_' postproctype{1} '.mat'];   
    
    if numel(postproctype) > 1
      switch postproctype{1}
        case 'smoothT'
          inputfile = insertBefore(inputfile, '.mat', ...
            [strrep(num2str(postproctype{2}(1)),'.','_') 'x' ...  % twin
            strrep(num2str(postproctype{2}(2)),'.','_')]);        % tshift
        case 'tfridge'
          inputfile = insertBefore(inputfile, '.mat', ...
            [strrep(num2str(postproctype{2}),'.','_') 'x']);      % wMult
        otherwise
          error('define modification to filename here')
      end
    end
    
    %% load inputfile, if it exists
    
    if ~existfile_TEM(inputfile)
      listing = dir(replaceBetween(inputfile, ... % get all .mat files in folder 
        [num2str(exptMeta.ratT.ratnums(r)) filesep], '.mat', '*'));  
      listing(~contains(listing.name,spectype)) = [];   % remove non-matches
      if isempty(listing)
        warning(['Skipping tone ' num2str(tn) ' of ' exptMeta.phases{p} ...
          ' because inputfile (' inputfile(numel(exptMeta.datafolder):end) ...
          ') was not found.'])
        continue  % to next tone
      else
        disp(listing)
        warning('clarify neural inputfile')
        keyboard
      end
    end
      
    freq{tr} = rmvlargefields_TEM(inputfile);
    
    %% add specs for bandBDAmean if applicable & missing
    
    if exist('bcfg','var') && ~isfield(freq{tr}.cfg,'bands')
      bcfg.previous = freq{tr}.cfg;
      freq{tr}.cfg = bcfg;
    end
    
    %% check for excessive NaNs
    
    if sum(isnan(freq{tr}.powspctrm(:)))/numel(freq{tr}.powspctrm(:)) > 0.2
      if r == 1 && p == 1 && tn == 12  % known bad trial
        freq{tr} = [];
      else
      warning(['powspctrm is ' num2str(round(...
        100*sum(isnan(freq{tr}.powspctrm(:)))/numel(freq{tr}.powspctrm(:)))) ...
        '% NaNs - omit?'])
      fig = figure('Name',[num2str(exptMeta.ratT.ratnums(r)) ...
        exptMeta.abbr{p} num2str(tn)]); clf; fig.WindowStyle = 'docked';
      fcfg = [];
      fcfg.zlim            = 'minmax';
      if exist('bcfg','var')
        fcfg.fticks        = bcfg.foi(contains(bcfg.bands,'('));
        fcfg.tticks        = bcfg.toi;
      end
      plotFTmatrix_TEM(fcfg,freq{tr}); 
      keyboard  % freq{tr} = [];
      end
    end
  end
  comptime(p,1) = toc(st);

  %% make sure all trials' data were prepared
  
  st = tic;   % start time for stats
  omit = cellfun(@isempty,freq);
  if sum(omit) > 1 || sum(~omit) < 13
    warning([exptMeta.abbr{p} ' only has ' num2str(sum(~omit)) ' trials'])
    if sum(~omit) == 0
      warning(['skipping ' exptMeta.abbr{p} ' for rat # ' ...
        num2str(exptMeta.ratT.ratnums(r))])
      continue  % to next block
    end
    keyboard  % figure out why, skip if < 3 good trials
  end
  
  %% define parameters of stat test & name outputfile
  
  cfg = [];
  cfg.statistic         = 'ft_statfun_indepsamplesregrT';
  cfg.method            = 'montecarlo';
  cfg.correctm          = 'cluster';
  cfg.clusteralpha      = 0.025;
  cfg.clusterstatistic  = 'maxsum';
  cfg.clusterthreshold  = 'nonparametric_individual';   % essential for reasonable thresholds!
  cfg.orderedstats      = 'yes';  % essential for reasonable thresholds!
  cfg.minnbchan         = 2;
  cfg.tail              = 0;
  cfg.clustertail       = 0;
  cfg.alpha             = 0.025;
  cfg.numrandomization  = 1000;
  cfg.neighbours        = neighbours;
  cfg.parameter         = 'powspctrm';
  
  % regress spectrogram of each tone with BSR to the *NEXT* tone
  cfg.design  = BSR(sum(exptMeta.expected(1:p-1)) + tones(~omit) + 1);
  if numel(unique(cfg.design)) < 3
    warning(['skipping ' exptMeta.abbr{p} ...
      ' because of < 3 unique behavioral values'])
    continue  % to next block
  end
  
  cfg.ivar    = 1;  % uvar = ivar (between-UO)
  
  cfg.outputfile  = [exptMeta.datafolder 'SpecBehavRegrPhases' filesep ...
    num2str(exptMeta.ratT.ratnums(r)) filesep exptMeta.abbr{p} ...
    inputfile(strfind(inputfile,'_raw'):end-4) '_vs_NextTrlBSR_stat' ...
    erase(cfg.statistic,'ft_statfun') '.mat'];
  if isfield(cfg,'correctm')
    switch cfg.correctm
      case 'cluster'
        if isfield(cfg,'clusterthreshold') && ...
          strcmp(cfg.clusterthreshold,'nonparametric_individual')
          cfg.outputfile = insertBefore(cfg.outputfile,'.mat','_NPindiv');
        end
        if isfield(cfg,'orderedstats') && istrue(cfg.orderedstats)
          cfg.outputfile = insertBefore(cfg.outputfile,'.mat','_ordered');
        end
      otherwise
        cfg.outputfile = insertBefore(cfg.outputfile,'.mat',['_' cfg.correctm]);
    end
  end
    
  %% run stat test

  if existfile_TEM(cfg.outputfile)
    stat = rmvlargefields_TEM(cfg.outputfile);
  else
    waitbar((p-2/3)/(numel(exptMeta.phases)-1), wb, ...
      ['Running ' erase(cfg.statistic,'ft_statfun_') ' on ' exptMeta.abbr{p} ...
      ' ' spectype ' vs. next trial BSR for rat #' num2str(exptMeta.ratT.ratnums(r))]);
    
    stat = ft_freqstatistics(cfg, freq{~omit});
    comptime(p,2) = toc(st);  % record time only if stats were calculated, not just loaded
  end
  
  %% print & plot (top 10/nearly) significant correlations of spectrograms & BSR
  
  st = tic;
  waitbar((p-1/3)/(numel(exptMeta.phases)-1), wb, ...
      ['Running ' erase(stat.cfg.statistic,'ft_statfun_') ' on ' exptMeta.abbr{p} ...
      ' ' spectype ' vs. next trial BSR for rat #' num2str(exptMeta.ratT.ratnums(r))]);
    
  pos = [];
  if isempty(stat.posclusters)
    pos.n = 0;
    pos.I = [];  % no index because there are no clusters
  else
    [pos.p,pos.I] = sort([stat.posclusters.prob]);
    if any(pos.p > 0.025)
      pos.n = find(pos.p > 0.025,1,'first') - 1;
    else
      pos.n = numel(pos.p);
    end
  end
  neg = [];
  if isempty(stat.negclusters)
    neg.n = 0;
    neg.I = [];  % no index because there are no clusters
  else
    [neg.p,neg.I] = sort([stat.negclusters.prob]);
    if any(neg.p > 0.025)
      neg.n = find(neg.p > 0.025,1,'first') - 1;
    else
      neg.n = numel(neg.p);
    end
  end
  disp([newline 'Found clusters with significant positive (' ...
    num2str(pos.n) ') and negative (' num2str(neg.n) ')' newline ...
    'correlations with next trial BSR within ' exptMeta.abbr{p} newline])
  
  cfg =[];
  cfg.layout = [exptMeta.configfolder 'layout_' exptMeta.ratT.MWA{r} '_' exptMeta.ratT.side{r} '.mat'];
  cfg.parameter = 'stat';
  cfg.feedback  = 'no';
  cfg.gridscale = 2*numel(stat.label) + 1;
  %   ft_clusterplot(cfg,stat);  % doesn't work unless freq or time is singleton
  xfit = linspace(min(stat.cfg.design),max(stat.cfg.design),15);
    
  %% visualize positive correlation clusters
  
  for n = 1:(pos.n + 1) % may plot one that's not considered significant
    if n > 10 || numel(pos.I) < n
      break  % out of for loop
    end
    cI = pos.I(n);  % cluster index
    binT = table;
    binT.I = find(stat.posclusterslabelmat == cI);
    [binT.ch,binT.f,binT.t] = ind2sub(size(stat.posclusterslabelmat),binT.I);
    binT.chlabel = stat.label(binT.ch);
    if exist('bcfg','var') && isequal(stat.freq(:),bcfg.foi(:))
      binT.flabel = strip(bcfg.bands(binT.f));
    else
      binT.flabel = strip(cellstr(num2str(stat.freq(binT.f)',3)));
    end      
    if exist('bcfg','var') && isequal(stat.time(:),bcfg.toi(:))
      binT.tlabel = exptMeta.intervals(binT.t);
    else
      binT.tlabel = strip(cellstr(num2str(stat.time(binT.t)',3)));
    end      
    % ch = unique(binT.ch); f = unique(binT.f); t = unique(binT.t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat.freq(min(binT.f)) stat.freq(max(binT.f))];
    trange = [stat.time(min(binT.t)) stat.time(max(binT.t))];
    binT.clustpowpertrl = nan(numel(binT.I),numel(freq));   % aggregate cluster power over trials
    for tr = find(~omit)'
      binT.clustpowpertrl(:,tr) = freq{tr}.powspctrm(binT.I);
    end
    binT.clustpowpertrl(:,omit) = [];
    binT.CC = rowfun(@(x) corrcoef(stat.cfg.design, x), binT, ...
      'InputVariables','clustpowpertrl', 'OutputFormat','cell');
    binT.CC = rowfun(@(x) x{1}(2), binT, 'InputVariables','CC', ...
      'OutputFormat','uniform');
    binT = sortrows(binT,'CC','descend');
    binT.color = flipud(jet(size(binT,1)));
    X = repmat(stat.cfg.design,size(binT,1),1);
    [pc,ErrorEst] = polyfit(X(:), binT.clustpowpertrl(:), 3); % fit cubic model
    pop_fit = polyval(pc, xfit, ErrorEst);

    fig = figure(p*100+n); clf; fig.WindowStyle = 'docked';
    fig.PaperPositionMode = 'auto';
    % plot next trl BSR vs. mean +/- std for each chan/freq/timebin w/in cluster
    sp = subplot(1,3,1:2); hold all
    sp.ColorOrder = binT.color;
    plot(stat.cfg.design(:),binT.clustpowpertrl','o:', ...
      xfit(:),pop_fit','-k');
    axis tight; xlabel('BSR in response to *NEXT* Tone'); ylabel('Power (dB)');
    legend([join([binT.chlabel, binT.flabel, binT.tlabel, ...
      strip(cellstr(num2str(binT.CC)))], {', ',', ',' Tones, coeff = '}); ...
      {'whole cluster cubic fit'}], 'Location','NorthEastOutside')
    
    title(['Positive correlations with next trial BSR within ' exptMeta.abbr{p} ...
      newline '(p = ' num2str(stat.negclusters(cI).prob) ', mean coeff = ' ...
      num2str(mean(binT.CC),3) ')'])
    
    % plot topographic extent of change in this time-freq cluster
    cfg.ylim = frange; cfg.xlim = trange;
    subplot(1,3,3);
    ft_topoplotER(cfg,stat);
    
    print([exptMeta.figurefolder 'PosCorr' num2str(n) ...
      stat.cfg.outputfile(find(stat.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
      '-dpng','-r0')
    %     close(fig)
  end
  
  %% visualize negative correlation clusters
  
  for n = 1:(neg.n + 1) % may plot one that's not considered significant
    if n > 10 || numel(neg.I) < n
      break  % out of for loop
    end
    cI = neg.I(n);  % cluster index
    binT = table;
    binT.I = find(stat.negclusterslabelmat == cI);
    [binT.ch,binT.f,binT.t] = ind2sub(size(stat.negclusterslabelmat),binT.I);
    binT.chlabel = stat.label(binT.ch);
    if exist('bcfg','var') && isequal(stat.freq(:),bcfg.foi(:))
      binT.flabel = strip(bcfg.bands(binT.f));
    else
      binT.flabel = strip(cellstr(num2str(stat.freq(binT.f)',3)));
    end      
    if exist('bcfg','var') && isequal(stat.time(:),bcfg.toi(:))
      binT.tlabel = exptMeta.intervals(binT.t);
    else
      binT.tlabel = strip(cellstr(num2str(stat.time(binT.t)',3)));
    end      
    % ch = unique(binT.ch); f = unique(binT.f); t = unique(binT.t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat.freq(min(binT.f)) stat.freq(max(binT.f))];
    trange = [stat.time(min(binT.t)) stat.time(max(binT.t))];
    binT.clustpowpertrl = nan(numel(binT.I),numel(freq));   % aggregate cluster power over trials
    for tr = find(~omit)'
      binT.clustpowpertrl(:,tr) = freq{tr}.powspctrm(binT.I);
    end
    binT.clustpowpertrl(:,omit) = [];
    CC = rowfun(@(x) corrcoef(stat.cfg.design, x), binT, ...
      'InputVariables','clustpowpertrl', 'OutputFormat','cell');
    binT.CC = cellfun(@(x) x(2), CC);
    binT = sortrows(binT,'CC','ascend');
    binT.color = flipud(jet(size(binT,1)));
    X = repmat(stat.cfg.design,size(binT,1),1);
    if numel(unique(stat.cfg.design)) > 3
      [pc,ErrorEst] = polyfit(X(:), binT.clustpowpertrl(:), 3); % fit cubic model
      mlabel = {'whole cluster cubic fit'};
    else
      [pc,ErrorEst] = polyfit(X(:), binT.clustpowpertrl(:), 2); % fit square model
      mlabel = {'whole cluster square fit'};
    end
    pop_fit = polyval(pc, xfit, ErrorEst);

    fig = figure(p*110+n); clf; fig.WindowStyle = 'docked';
    fig.PaperPositionMode = 'auto';
    % plot next trl BSR vs. mean +/- std for each chan/freq/timebin w/in cluster
    sp = subplot(1,3,1:2); hold all
    sp.ColorOrder = binT.color;
    plot(stat.cfg.design(:),binT.clustpowpertrl','o:', ...
      xfit(:),pop_fit','-k');
    axis tight; xlabel('BSR in response to *NEXT* Tone'); ylabel('Power (dB)');
    legend([join([binT.chlabel, binT.flabel, binT.tlabel, ...
      strip(cellstr(num2str(binT.CC)))], {', ',', ',' Tones, coeff = '}); ...
      mlabel], 'Location','NorthEastOutside')
    
    title(['Negative correlations with next trial BSR within ' exptMeta.abbr{p} newline ...
      '(p = ' num2str(stat.negclusters(cI).prob) ', mean coeff = ' ...
      num2str(mean(binT.CC),3) ')'])
    
    % plot topographic extent of change in this time-freq cluster
    cfg.ylim = frange; cfg.xlim = trange;
    subplot(1,3,3);
    ft_topoplotER(cfg,stat);
    
    print([exptMeta.figurefolder 'NegCorr' num2str(n) ...
      stat.cfg.outputfile(find(stat.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
      '-dpng','-r0')
    %     close(fig)
  end
  
  %% plot stat
  
  fig = figure('Name',exptMeta.abbr{p}); clf; fig.WindowStyle = 'docked';
  cfg=[];
  cfg.channel = cell(0,1);
  % get best channel for negative correlations...
  [~,I] = min(stat.stat(1:numel(exptMeta.lftchs),:,:));
  cfg.channel(end+1,1)        = stat.label(mode(I(:)));
  % ...in each region...
  if numel(stat.label) > numel(exptMeta.lftchs)
    [~,I] = min(stat.stat(numel(exptMeta.lftchs)+1:end,:,:));
    cfg.channel(end+1,1)      = stat.label(numel(exptMeta.lftchs) + mode(I(:)));
  end
  % ...and for positive correlations...
  [~,I] = max(stat.stat(1:numel(exptMeta.lftchs),:,:));
  cfg.channel(end+1,1)        = stat.label(mode(I(:)));
  % ...in each region
  if numel(stat.label) > numel(exptMeta.lftchs)
    [~,I] = max(stat.stat(numel(exptMeta.lftchs)+1:end,:,:));
    cfg.channel(end+1,1)      = stat.label(numel(exptMeta.lftchs) + mode(I(:)));
  end
  cfg.channel = unique(cfg.channel);  % also sorts them
  if isempty(cfg.channel)   % this shouldn't happen, but is an alternate chsel
    cfg.channel = [exptMeta.ratT.mPFC(r); exptMeta.ratT.BLA(r)];
  end
  cfg.zlim            = 'maxabs';
  cfg.parameter       = 'stat';
  cfg.maskparameter   = 'mask';
  cfg.maskstyle       = 'outline';
  cfg.feedback        = 'no';
  if exist('bcfg','var')
    cfg.fticks        = bcfg.foi(contains(bcfg.bands,'('));
    cfg.tticks        = bcfg.toi;
  end
  plotFTmatrix_TEM(cfg,stat);
  
  fig.PaperPositionMode = 'auto';
  print([exptMeta.figurefolder 'CorrSpectrogram_' ...
    stat.cfg.outputfile(find(stat.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
    '-dpng','-r0')
  %   close(fig)
  
  comptime(p,3) = toc(st);
end   % for each block

%% clean up

output = comptime;
close(wb);

end   % function
