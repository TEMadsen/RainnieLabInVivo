function do_CFC_OrigFear_TEM( r )
% DO_CFC_ORIGFEAR_TEM conducts cross-frequency analysis by 30s subtrials
%
%
%
%   Depends on global variables defined in info_origfear.
%
% written 6/9/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder ratT fsample stimuli phases 

%% create waitbar

wb = waitbar(0,['Preparing for CFC on rat #' num2str(ratT.ratnums(r))]);

%% define analysis parameters

cfc = get_cfg_TEM('KL-MI', 'OrigFear');
% cfc = 
%   struct with fields:
% 
%        freqlow: [1�32 double]
%       freqhigh: [1�35 double]
%         method: 'mi'
%     keeptrials: 'yes'

%% Load inputfile (coarsely calculated wavelet freq with fourier output)

inputfile = [datafolder 'wavelet' filesep whatever '.mat'];

freq = rmvlargefields_TEM(inputfile);     % strip excess historical metadata

%% calculate MI (consider implementing shuffling procedure to convert to z-score)

crossfreq = ft_crossfrequencyanalysis(cfg, freq);
