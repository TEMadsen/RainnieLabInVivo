%% full FT pipeline

CleanSlate  % provides a clean slate to start working with new data/scripts

starttime = tic;  % overall time
wb = waitbar(0,'Initializing...');   % will display progress through loop

info_origfear %#ok<*SAGROW> multiple metadata variables preallocated here

PreProcSteps = {};  %'preproc','preprocBehav','preprocSpikes'   % preproc alone is for LFPs, skip to use raw data (no artifact rejection)
inputArgs = [];   % empty set for no steps

SigProcSteps = {'piWav'};   %, 'CFC' = cross-frequency coupling, 'SFC' = spike-field coherence
inputArgs = [inputArgs, {{}}];  % empty cell for step with no inputs (e.g., piWav)

PostProcSteps = {'tfRidge'};   %'bandBDAmean','tfRidge','BLnorm','smoothT'
inputArgs = [inputArgs, {{'piWav',1.5,true}}];  % cell within cell for multiple inputs to same function
% twin & tshift for smoothT (note:  {30 30} centers the timebins at -60:30:60
% instead of -45:30:45, so use BDA functions instead), {spectype,wMult,plt} for
% tfRidge, e.g., {'piWav',1.5,true}

StatTestSteps = {'RidgeVbehav'};   %'SpecBSRRegrPhasesNonClust','SpecFreezeRegrPhasesNonClust','BlkCompare',
inputArgs = [inputArgs, {{'piWav',1.5}}];  % simplified subset of prior steps/inputs

AllSteps = [PreProcSteps, SigProcSteps, PostProcSteps, StatTestSteps];

% preallocate for error messages, computation time, and completion flags
% per rat (or group) and step
errlog = cell(size(exptMeta.ratT,1)+1, numel(AllSteps));
comptime = NaN(size(errlog));
% esttime = nanmean(comptime);   % average time/rat for each step
esttime = [1732, 263, 275];  % coarse: 1892, sharp: 2021
done = false(size(errlog));   
done(exptMeta.ratT.excluded,:) = true;   % count excluded rats as done
done(end,1:end-numel(StatTestSteps)) = true;   % only stats need to be run on group
done(:,1:2) = true;   % steps completed earlier
done(1:end-1,3) = true;   % individual rat stats done earlier
% note: r = 3 & 12, p = 2 RidgeVbehav stats are missing because BSR == 1
% throughout Rcl, so no behavioral variation to correlate with neural

outputs = cell(size(exptMeta.ratT,1)+1, numel(AllSteps));

%% loop through each step for each rat

for r = find(any(~done(1:end-1,:),2))'  % rats with any steps not completed
  for s = find(~done(r,:))  % steps not completed for that rat
    waitbar(sum(sum(done).*esttime)/(sum(esttime)*size(done,1)), wb, ...
      ['Performing ' AllSteps{s} ' for rat #' num2str(exptMeta.ratT.ratnums(r)) ...
      newline 'Est. minutes remaining:  ' num2str(sum(sum(~done).*esttime)/60)])
    
    outputfolder = [exptMeta.datafolder AllSteps{s} filesep num2str(exptMeta.ratT.ratnums(r)) filesep];
    
    %% check whether step was already completed
    
    if exist(outputfolder,'dir')
      listing = dir([outputfolder '*.mat']);
      if numel(listing) > 0
        msg = [num2str(numel(listing)) ' file(s) found in ' outputfolder];
        disp([newline msg])
        [~,I] = sort([listing.datenum],'descend');
        if numel(listing) > 10
          I = I(1:10);
          disp('The 10 most recently modified are:')
        end
        disp([{listing(I).name}', {listing(I).date}'])
        done(r,s) = strcmp('Yes',questdlg('Is this step complete?'));
        if done(r,s)
          errlog{r,s} = ['user marked complete:  ' msg];
          continue  % to next step
        end
      end   % if numel(listing) > 0
    else  % i.e., if ~exist(outputfolder,'dir')
      mkdir(outputfolder)
    end
    
    %% perform this step for this rat
    
%     try
      fun = eval(['@do_' AllSteps{s} '_OrigFear_TEM']);
      steptime = tic;
% Just do everything once!  The loops were unnecessarily complicated.
      outputs{r,s} = fun(r,inputArgs{s}{:});
      comptime(r,s) = toc(steptime);
      esttime(s) = nanmean([esttime(s) comptime(r,s)]);
      done(r,s) = true;   % mark complete if no errors
%     catch ME
%       warning(['error during ' steps{s} ' for rat ' ...
%         num2str(exptMeta.ratT.ratnums(r)) ':' newline ME.message])
%       errlog{r,s} = ME;
%     end
  end   % for s = 1:numel(steps)
  close all   % close any figures produced by the function
end   % for r = find(~exptMeta.ratT.excluded)'

%% aggregate results across rats, run stats, & plot

for s = find(~done(end,:))  % group stat steps not completed
  waitbar(sum(sum(done).*esttime)/(sum(esttime)*size(done,1)), wb, ...
    ['Performing group analysis of ' AllSteps{s} ...
    newline 'Est. minutes remaining:  ' num2str(sum(sum(~done).*esttime)/60)])
    
  %% check whether step was completed for each rat
  
  if ~all(done(find(~exptMeta.ratT.excluded),s)) %#ok<FNDSB> different size
    errlog{end,s} = ['not all rats marked complete for ' AllSteps{s}];
    warning(errlog{end,s})
    continue  % to next step
  end
  
  %% perform the step
  
% try
    fun = eval(['@doGrp_' AllSteps{s} '_OrigFear_TEM']);
    steptime = tic;
    outputs{end,s} = fun(inputArgs{s}{:});
    comptime(end,s) = toc(steptime);
    esttime = esttime * (comptime(end,s)/esttime(s)); % scale remaining group steps according to this one's ratio of group to rat steptime
    done(end,s) = true;   % mark complete if no errors
% catch ME
%   warning(['error during group analysis of ' steps{s} ':' newline ME.message])
%   errlog{end,s} = ME;
% end
end   % for s = each step requiring aggregate stats and/or plots

%% save outputs & errlog with time in filenames, to avoid overwriting

save([exptMeta.datafolder 'outputs' datestr(now,30) '_fullFTpipeline_new_OrigFear_TEM'], 'outputs')
save([exptMeta.datafolder 'errlog' datestr(now,30) '_fullFTpipeline_new_OrigFear_TEM'], 'errlog')

waitbar(1, wb, ['Done!  Total time: ' ...
  num2str(toc(starttime)/60) ' minutes']);
