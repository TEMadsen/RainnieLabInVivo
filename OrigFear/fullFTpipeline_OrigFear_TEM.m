%% full FT pipeline

CleanSlate  % provides a clean slate to start working with new data/scripts

starttime = tic;  % overall time
wb = waitbar(0,'Initializing...');   % will display progress through loop

info_origfear %#ok<*SAGROW> multiple metadata variables preallocated here

PreProcSteps = {};  %'preproc','preprocBehav','preprocSpikes'   % preproc alone is for LFPs, skip to use raw data (no artifact rejection
SigProcSteps = {'wavelet_coarse'};   %,'BLnorm','smoothT','4s_mtmfft','CFC','SFC'   % cross-frequency coupling & spike-field coherence
StatTestSteps = {'wavSpecBlockComp'};  %,'regrT_neuralVbehav','T_neuralVbehav'
AllSteps = [PreProcSteps, SigProcSteps, StatTestSteps];

% preallocate for error messages, computation time, and completion flags
% per rat (or group) and step
errlog = cell(size(ratT,1)+1, numel(AllSteps));
comptime = NaN(size(errlog));
esttime = NaN(1,numel(AllSteps));   % average time/rat for each step
done = false(size(errlog));   done(ratT.excluded,:) = true;   % count as done
done(size(ratT,1)+1,1:numel([PreProcSteps,SigProcSteps])) = true;   % not needed for group
done(1:13,:) = true;   % completed earlier

outputs = cell(size(ratT,1)+1, numel(AllSteps));

%% loop through each step for each rat

for r = find(~ratT.excluded)'
  for s = 1:numel(AllSteps)
    waitbar(sum(sum(done).*esttime)/(sum(esttime)*size(done,1)), wb, ...
      ['Performing ' AllSteps{s} ' analysis for rat #' num2str(ratT.ratnums(r)) ...
      newline 'Est. minutes remaining:  ' num2str(sum(sum(~done).*esttime)/60)])
    
    outputfolder = [datafolder AllSteps{s} filesep num2str(ratT.ratnums(r)) filesep];
    
    %% check whether step was already completed
    
    if done(r,s)
      errlog{r,s} = 'already marked complete';
      continue  % to next step
    end
    
    if exist(outputfolder,'dir')
      listing = dir([outputfolder '*.mat']);
      if numel(listing) > 0
        msg = [num2str(numel(listing)) ' file(s) found in ' outputfolder];
        disp([newline msg])
        [~,I] = sort([listing.datenum],'descend');
        if numel(listing) > 10
          I = I(1:10);
          disp('The 10 most recently modified are:')
        end
        disp([{listing(I).name}', {listing(I).date}'])
        done(r,s) = strcmp('Yes',questdlg('Is this step complete?'));
        if done(r,s)
          errlog{r,s} = ['user marked complete:  ' msg];
          continue  % to next step
        end
      end   % if numel(listing) > 0
    else  % i.e., if ~exist(outputfolder,'dir')
      mkdir(outputfolder)
    end
    
    %% perform this step for this rat
    
%     try
      fun = eval(['@do_' AllSteps{s} '_OrigFear_TEM']);
      steptime = tic;
      if nargout(fun) ~= 0
        outputs{r,s} = fun(r);
      else
        fun(r);
      end
      comptime(r,s) = toc(steptime);
      esttime(s) = nanmean([esttime(s) comptime(r,s)]);
      done(r,s) = true;   % mark complete if no errors
%     catch ME
%       warning(['error during ' steps{s} ' for rat ' ...
%         num2str(ratT.ratnums(r)) ':' newline ME.message])
%       errlog{r,s} = ME;
%     end
  end   % for s = 1:numel(steps)
end   % for r = find(~ratT.excluded)'

%% aggregate results across rats, run stats, & plot

for s = 1:numel(StatTestSteps)
  waitbar(sum(sum(done).*esttime)/(sum(esttime)*size(done,1)), wb, ...
    ['Performing group analysis of ' StatTestSteps{s} ...
    newline 'Est. minutes remaining:  ' num2str(sum(sum(~done).*esttime)/60)])
    
  %% check whether step was already completed for group or each rat
  
  sI = ismember(AllSteps,StatTestSteps{s});
  if done(end,sI)
    errlog{end,sI} = 'already marked complete';
    continue  % to next step
  end
  
  if ~all(done(find(~ratT.excluded),sI)) %#ok<FNDSB> different size
    errlog{end,sI} = ['not all rats marked complete for ' StatTestSteps{s}];
    warning(errlog{end,sI})
    continue  % to next step
  end
  
  %% perform the step
  
% try
    fun = eval(['@doGrp_' StatTestSteps{s} '_OrigFear_TEM']);
    steptime = tic;
    if nargout(fun) ~= 0
      outputs{end,sI} = fun();
    else
      fun(r);
    end
    comptime(end,sI) = toc(steptime);
    esttime = esttime * (comptime(end,sI)/esttime(sI));   % scale remaining group steps according to this one's ratio of group to rat steptime
    done(end,sI) = true;   % mark complete if no errors
% catch ME
%   warning(['error during group analysis of ' steps{s} ':' newline ME.message])
%   errlog{end,s} = ME;
% end
end   % for s = each step requiring aggregate stats and/or plots

%% save errlog

save([datafolder 'errlog' datestr(now,30) ... % with time, to avoid overwriting
  '_fullFTpipeline_OrigFear_TEM'], 'errlog')

waitbar(1, wb, ['Done!  Total time: ' ...
  num2str(toc(starttime)/60) ' minutes']);
