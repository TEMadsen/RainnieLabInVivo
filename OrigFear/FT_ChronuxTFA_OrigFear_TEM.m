%% Calculate raw spectrograms & coherograms using Chronux calculations 
% with FieldTrip structure

CleanSlate  % provides a clean slate to start working with new data/scripts

info_origfear   % load all background info & final parameters

zthresh = 15;   % analyze data cleaned using this artifact threshold
cthresh = 3;    % and this clipping threshold
redo = false;   % to ignore & overwrite old files

errlog = cell(size(ratT,1),numel(allchs));   % MEs saved w/in tr# cells

%% full loop

for r = find(~ratT.excluded)'   % may be better to run a smaller batch of rats on multiple computers
  %% define inputfile
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllClean_z' num2str(zthresh) '_clip' num2str(cthresh) ...
    '_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];
  
  %% check for input data
  
  if ~exist(inputfile,'file')
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ...
      ' because ' inputfile ' was not found.'])
    continue
  end
  
  %% load input data (all channels, tone trials, artifacts replaced with nans)
  
  disp(['Loading allch data for rat #' num2str(ratT.ratnums(r))])
  S = load(inputfile,'data');
  data = S.data;
  S = [];   % use empty set instead of clearvars inside parfor loop
  
  %% set common parameters
  
  cfg             = mtm;          % see info_origfear
  
  %% calculate baseline separately
  
  cfg.toilim = [wav.BLtoi(1) wav.BLtoi(end)];   % full duration of analysis
  cfg.trials = data.trialinfo(:,2) == 0; % wherever there's no stimulus type
  cfg.outputfile  = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'Baseline_z' num2str(zthresh) '_clip' num2str(cthresh) '_' ...
    num2str(ratT.ratnums(r)) 'allch_ChronuxTFA.mat'];
  
  freq = ft_freqanalysis(cfg,data); %#ok<NASGU> saved to file
  
  %% calculate all tone trials together
  
  cfg.toilim = [wav.toi(1) wav.toi(end)]; % 1st & last windows will center on these times
  cfg.trials      = data.trialinfo(:,2) == 1;   % wherever there's a CS+
  cfg.outputfile  = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllTones_z' num2str(zthresh) '_clip' num2str(cthresh) '_' ...
    num2str(ratT.ratnums(r)) 'allch_ChronuxTFA.mat'];
  
  freq = ft_freqanalysis(cfg,data);
  %       catch ME
  %         warning(ME.message);
  %         errlog{r,ch}{tr} = ME;
  %       end
end