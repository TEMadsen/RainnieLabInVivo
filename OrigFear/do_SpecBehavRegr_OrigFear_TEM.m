function [output] = do_SpecBehavRegr_OrigFear_TEM( spectype, r, twin, tshift )
% DO_SPECBEHAVREGR_ORIGFEAR_TEM tests 4 linear trends btwn spcgrm & behaviors
%
%   This function is for within-subject visualization & stats using
%   ft_statfun_indepsamplesregrT on any spectrograms (specified by input vars).
%
%   Depends on global variables defined in info_origfear.
%
% written 8/28/17 by Teresa E. Madsen, Ph.D.

%% import global metadata structure

global exptMeta

if isempty(exptMeta);   info_origfear;   end

%% check inputs & assign defaults

if nargin == 0  % like when I just hit "Run and Time" in the Editor tab
  spectype  = 'piWav';  % close to FT default wavelet params, but using pi for integer # of cycles, etc.
  r         = 1;        % poster rat
  twin      = 6;        % length of time windows (s)
  tshift    = 3;        % shifting by this size step (s)
end

%% create waitbar

wb = waitbar(0, ['Preparing to run SpecBehavRegr on rat #' ...
  num2str(exptMeta.ratT.ratnums(r))]);

%% prepare_neighbours determines what sensors may form clusters

neighbfile = [exptMeta.configfolder 'neighb_' exptMeta.ratT.MWA{r} '_' ...
  exptMeta.ratT.side{r} '.mat'];

if existfile_TEM(neighbfile)
  load(neighbfile)
else
  cfg = [];
  cfg.method        = 'distance';
  cfg.neighbourdist = 0.5;  % mm
  cfg.elecfile      = [exptMeta.configfolder 'elec_' exptMeta.ratT.MWA{r} ...
    '_' exptMeta.ratT.side{r} '.mat'];
  neighbours        = ft_prepare_neighbours(cfg);
  save(neighbfile,'neighbours')
end
if any(cellfun(@numel,{neighbours(:).neighblabel}) < 3) || ...
    any(cellfun(@numel,{neighbours(:).neighblabel}) > 5)
  warning('unexpected # of neighbors')
  cfg = [];
  cfg.neighbours = neighbours;
  cfg.layout = [exptMeta.configfolder 'layout_' exptMeta.ratT.MWA{r} '_' ...
    exptMeta.ratT.side{r} '.mat'];
  ft_neighbourplot(cfg)
  keyboard
end

%% load operant count BDA tones and convert to BSR

load([exptMeta.datafolder 'preprocBehav' filesep 'AllRatsBDAtones.mat']);

BSR = (cntBDAtones{r}(:,1) - cntBDAtones{r}(:,2)) ...
  ./ (cntBDAtones{r}(:,1) + cntBDAtones{r}(:,2)); %#ok<USENS> loaded from file
% if no operant behavior before or during tone, consider it complete suppression
BSR(isnan(BSR)) = 1;
% if operant behavior increases in response to tone, consider it no suppression
BSR(BSR < 0) = 0;

%% loop through blocks:  begin & end of each recording (minus last tone of each)
% run each block all the way through, then clear memory to avoid overload

comptime  = nan(numel(exptMeta.blksof7names),3);  % 3 steps: prep data, stats, & plot

for b = 1:numel(exptMeta.blksof7names)
  st = tic;
  waitbar((b-1)/numel(exptMeta.blksof7names), wb, ['Preparing ' ...
    exptMeta.blksof7names{b} ' ' spectype ' specs for regrT analysis']);
  
  p = ceil(b/2);  % convert block to phase
  
  if rem(b,2) == 1  % odd #s = beginning of each recording
    tones = 1:exptMeta.expected(p)-8;            % 1st 7-9 of each recording
  else              % even #s = end of each recording
    tones = exptMeta.expected(p)-6:exptMeta.expected(p)-1;  % last 6 of each recording, except the very last
  end
  
  freq  = cell(numel(tones),1);   % 7-10 tones per block
  
  for tr = 1:numel(tones)   % trial # within block
    %% define trial's neural data inputfile
    
    tn = tones(tr);   % trial # within phase
    
    inputfile = [exptMeta.datafolder 'smoothT' filesep num2str(exptMeta.ratT.ratnums(r)) ...
      filesep exptMeta.stimuli{1} num2str(tn) 'of' exptMeta.abbr{p} '_raw' ...
      num2str(exptMeta.ratT.ratnums(r)) spectype 'TFAdBpow_smoothT' ...
      strrep(num2str(twin),'.','_') 'x' strrep(num2str(tshift),'.','_') '.mat'];   
    
    %% load dB transformed & time-smoothed inputfile, if it exists
    
    if ~existfile_TEM(inputfile)
      warning(['Skipping tone ' num2str(tn) ' of ' exptMeta.phases{p} ...
        ' because inputfile (' inputfile(numel(exptMeta.datafolder):end) ...
        ') was not found.'])
      continue  % to next tone
    end
      
    freq{tr} = rmvlargefields_TEM(inputfile);
    
    %% check for excessive NaNs
    
    if sum(isnan(freq{tr}.powspctrm(:)))/numel(freq{tr}.powspctrm(:)) > 0.2
      warning(['powspctrm is ' num2str(round(...
        100*sum(isnan(freq{tr}.powspctrm(:)))/numel(freq{tr}.powspctrm(:)))) ...
        '% NaNs - omit?'])
      figure('Name',[num2str(exptMeta.ratT.ratnums(r)) ...
        exptMeta.blksof7names{b} num2str(tr)]); 
      plotFTmatrix_TEM([],freq{tr}); 
      keyboard  % freq{tr} = [];
    end
    
  end
  comptime(b,1) = toc(st);

  %% make sure all trials' data were prepared
  
  st = tic;
  omit = cellfun(@isempty,freq);
  if sum(omit) > 1 || sum(~omit) < 5
    warning([exptMeta.blksof7names{b} ' only has ' num2str(sum(~omit)) ' trials'])
    if sum(~omit) == 0
      warning(['skipping ' exptMeta.blksof7names{b} ' for rat # ' ...
        num2str(exptMeta.ratT.ratnums(r))])
      continue  % to next block
    end
    keyboard  % figure out why, skip if < 3 good trials
  end
  
  %% define parameters of stat test & name outputfile
  
  cfg = [];
  cfg.statistic         = 'ft_statfun_indepsamplesregrT';
  cfg.method            = 'montecarlo';
  cfg.correctm          = 'cluster';
  cfg.clusteralpha      = 0.025;
  cfg.clusterstatistic  = 'maxsum';
  cfg.clusterthreshold  = 'nonparametric_individual';   % essential for reasonable thresholds!
  cfg.orderedstats      = 'yes';  % essential for reasonable thresholds!
  cfg.minnbchan         = 2;
  cfg.tail              = 0;
  cfg.clustertail       = 0;
  cfg.alpha             = 0.025;
  cfg.numrandomization  = 1000;
  cfg.neighbours        = neighbours;
  cfg.parameter         = 'powspctrm';
  
  % regress spectrogram of each tone with BSR to the *NEXT* tone
  cfg.design  = BSR(sum(exptMeta.expected(1:p-1)) + tones(~omit) + 1);
  if numel(unique(cfg.design)) < 3
    warning(['skipping ' exptMeta.blksof7names{b} ...
      ' because of < 3 unique behavioral values'])
    continue  % to next block
  end
  
  cfg.ivar    = 1;  % uvar = ivar (between-UO)
  
  cfg.outputfile  = [exptMeta.datafolder 'SpecBehavRegr' filesep ...
    num2str(exptMeta.ratT.ratnums(r)) filesep exptMeta.blksof7names{b} ...
    inputfile(strfind(inputfile,'_raw'):end-4) '_vs_NextTrlBSR_stat' ...
    erase(cfg.statistic,'ft_statfun') '.mat'];
  if isfield(cfg,'clusterthreshold') && ...
      strcmp(cfg.clusterthreshold,'nonparametric_individual')
    cfg.outputfile = strrep(cfg.outputfile,'.mat','_NPindiv.mat');
  end
  if isfield(cfg,'orderedstats') && istrue(cfg.orderedstats)
    cfg.outputfile = strrep(cfg.outputfile,'.mat','_ordered.mat');
  end
  
  %% run stat test

  if existfile_TEM(cfg.outputfile)
    stat = rmvlargefields_TEM(cfg.outputfile);
  else
    waitbar((b-2/3)/numel(exptMeta.blksof7names), wb, ...
      ['Running ' erase(cfg.statistic,'ft_statfun_') ' on ' exptMeta.blksof7names{b} ...
      ' ' spectype ' vs. next trial BSR for rat #' num2str(exptMeta.ratT.ratnums(r))]);
    
    stat = ft_freqstatistics(cfg, freq{~omit});
    comptime(b,2) = toc(st);  % record time only if stats were calculated, not just loaded
  end
  
  %% print & plot (top 10/nearly) significant correlations of spectrograms & BSR
  
  st = tic;
  waitbar((b-1/3)/numel(exptMeta.blksof7names), wb, ...
      ['Running ' erase(cfg.statistic,'ft_statfun_') ' on ' exptMeta.blksof7names{b} ...
      ' ' spectype ' vs. next trial BSR for rat #' num2str(exptMeta.ratT.ratnums(r))]);
    
  pos = [];
  if isempty(stat.posclusters)
    pos.n = 0;
    pos.I = [];  % no index because there are no clusters
  else
    [pos.p,pos.I] = sort([stat.posclusters.prob]);
    pos.n = find(pos.p > 0.025,1,'first') - 1;
  end
  neg = [];
  if isempty(stat.negclusters)
    neg.n = 0;
    neg.I = [];  % no index because there are no clusters
  else
    [neg.p,neg.I] = sort([stat.negclusters.prob]);
    neg.n = find(neg.p > 0.025,1,'first') - 1;
  end
  bestch = zeros(pos.n+neg.n+2,1);
  disp([newline 'Found clusters with significant positive (' ...
    num2str(pos.n) ') and negative (' num2str(neg.n) ')' newline ...
    'correlations with next trial BSR within ' exptMeta.blksof7names{b} newline])
  
  cfg =[];
  cfg.layout = [exptMeta.configfolder 'layout_' exptMeta.ratT.MWA{r} '_' exptMeta.ratT.side{r} '.mat'];
  cfg.parameter = 'stat';
  cfg.feedback  = 'no';
  cfg.gridscale = 2*numel(stat.label) + 1;
  %   ft_clusterplot(cfg,stat);  % doesn't work unless freq or time is singleton
  
  %% visualize positive correlation clusters
  
  for n = 1:(pos.n + 1) % may plot one that's not considered significant
    if n > 10 || numel(pos.I) < n
      break  % out of for loop
    end
    cI = pos.I(n);  % cluster index
    I = find(stat.posclusterslabelmat == cI);
    [ch,f,t] = ind2sub(size(stat.posclusterslabelmat),I);
    bestch(n) = mode(ch);
    ch = unique(ch); f = unique(f); t = unique(t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat.freq(min(f)) stat.freq(max(f))];
    trange = [stat.time(min(t)) stat.time(max(t))];
    clustpowpertrl = nan(numel(I),numel(freq));   % aggregate cluster power over trials
    for tr = find(~omit)'
      clustpowpertrl(:,tr) = freq{tr}.powspctrm(I);
    end
    clustpowpertrl(:,omit) = [];
    
    fig = figure(b*100+n); clf; fig.WindowStyle = 'docked';
    fig.PaperPositionMode = 'auto';
    % plot next trl BSR vs. mean +/- std for each chan/freq/timebin w/in cluster
    subplot(1,2,1); hold all
    X = repmat(stat.cfg.design,size(clustpowpertrl,1),1);
    CC = corrcoef(X(:), clustpowpertrl(:));
    [pc,ErrorEst] = polyfit(X(:), clustpowpertrl(:), 3);   % fit cubic model
    pop_fit = polyval(pc, stat.cfg.design, ErrorEst);
    plot(X(:),clustpowpertrl(:),'+', stat.cfg.design,mean(clustpowpertrl),'-', ...
      stat.cfg.design,pop_fit,'-');
    axis tight; xlabel('BSR in response to *NEXT* Tone'); ylabel('Power (dB)');
    legend({'chan/time/freq bins','mean per trial','cubic fit'}, ...
      'Location','Best')
    
    title([strjoin(stat.label(ch)) newline ...
      'powspctrms are positively correlated with next trial BSR within ' ...
      exptMeta.blksof7names{b} newline ...
      'at ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz, from ' ...
      num2str(trange(1)) ' - ' num2str(trange(2)) ' s, (p = ' ...
      num2str(stat.posclusters(cI).prob) ', overall coeff = ' num2str(CC(2)) ')'])
    
    % plot topographic extent of change in this time-freq cluster
    cfg.ylim = frange; cfg.xlim = trange;
    subplot(1,2,2);
    ft_topoplotER(cfg,stat);
    
    print([exptMeta.figurefolder 'PosCorr' num2str(n) ...
      stat.cfg.outputfile(find(stat.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
      '-dpng','-r0')
    %     close(fig)
  end
  
  %% visualize negative correlation clusters
  
  for n = 1:(neg.n + 1) % may plot one that's not considered significant
    if n > 10 || numel(neg.I) < n
      break  % out of for loop
    end
    cI = neg.I(n);  % cluster index
    I = find(stat.negclusterslabelmat == cI);
    [ch,f,t] = ind2sub(size(stat.negclusterslabelmat),I);
    bestch(n) = mode(ch);
    ch = unique(ch); f = unique(f); t = unique(t);  % have to use a block around these, rather than individual pixels, due to memory issues
    frange = [stat.freq(min(f)) stat.freq(max(f))];
    trange = [stat.time(min(t)) stat.time(max(t))];
    trlns = nan(1,numel(freq));
    clustpowpertrl = nan(numel(I),numel(freq));   % aggregate cluster power over trials
    for tr = find(~cellfun(@isempty,freq))'
      trlns(tr) = freq{tr}.trialinfo(1,1);
      clustpowpertrl(:,tr) = freq{tr}.powspctrm(I);
    end
    clustpowpertrl(:,isnan(trlns)) = [];
    
    fig = figure(b*110+n); clf; fig.WindowStyle = 'docked';
    fig.PaperPositionMode = 'auto';
    % plot next trl BSR vs. mean +/- std for each chan/freq/timebin w/in cluster
    subplot(1,2,1); hold all
    X = repmat(stat.cfg.design,size(clustpowpertrl,1),1);
    CC = corrcoef(X(:), clustpowpertrl(:));
    [pc,ErrorEst] = polyfit(X(:), clustpowpertrl(:), 3);   % fit cubic model
    pop_fit = polyval(pc, stat.cfg.design, ErrorEst);
    plot(X(:),clustpowpertrl(:),'+', stat.cfg.design,mean(clustpowpertrl),'-', ...
      stat.cfg.design,pop_fit,'-');
    axis tight; xlabel('BSR in response to *NEXT* Tone'); ylabel('Power (dB)');
    legend({'chan/time/freq bins','mean per trial','cubic fit'}, ...
      'Location','Best')
    
    title([strjoin(stat.label(ch)) newline ...
      'powspctrms are negatively correlated with next trial BSR within ' ...
      exptMeta.blksof7names{b} newline ...
      'at ' num2str(frange(1)) ' - ' num2str(frange(2)) ' Hz, from ' ...
      num2str(trange(1)) ' - ' num2str(trange(2)) ' s, (p = ' ...
      num2str(stat.negclusters(cI).prob) ', overall coeff = ' num2str(CC(2)) ')'])
    
    % plot topographic extent of change in this time-freq cluster
    cfg.ylim = frange; cfg.xlim = trange;
    subplot(1,2,2);
    ft_topoplotER(cfg,stat);
    
    print([exptMeta.figurefolder 'NegCorr' num2str(n) ...
      stat.cfg.outputfile(find(stat.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
      '-dpng','-r0')
    %     close(fig)
  end
  
  %% plot stat
  
  fig = figure(b*100); clf; fig.WindowStyle = 'docked';
  cfg=[];
  cfg.channel         = mode(bestch(bestch ~= 0));
  if isempty(cfg.channel);    cfg.channel = exptMeta.ratT.mPFC{r};     end
  cfg.zlim            = quantile(stat.stat(:),[0.025 0.975]);
  cfg.parameter       = 'stat';
  cfg.maskparameter   = 'mask';
  cfg.maskstyle       = 'outline';
  cfg.feedback        = 'no';
  plotFTmatrix_TEM(cfg,stat);
  title(['Correlations with ' exptMeta.blksof7names{b} ' next trial BSR'])
  
  fig.PaperPositionMode = 'auto';
  print([exptMeta.figurefolder 'CorrSpectrogram_' ...
    stat.cfg.outputfile(find(stat.cfg.outputfile == filesep,1,'last')+1:end-4)], ...
    '-dpng','-r0')
  %   close(fig)
  
  comptime(b,3) = toc(st);
end   % for each block

%% clean up

output = comptime;
close(wb);

end   % function
