%% info needed for analyzing original fear data from both batches
%#ok<*SNASGU> just provides background info for other analysis protocols

global exptMeta

%% experiment metadata

% each row # here corresponds to cell row (1st dim) in other variables
ratnums = [200; 202; 203; 204; 205; 210; 211; 212; ...
  338; 339; 341; 342; 343];   % 340 lost headcap during 6th shock

excluded = logical([0; 0; 0; 0; 0; 0; 1; 0; ...
  0; 0; 0; 0; 0]);
MWA = [repmat({'OldBatchOldFear'},[8,1]); ...
  repmat({'NewBatchOldFear'},[5,1])];
side = {'Right';'Right';'Right';'Right';'Right';'Right';'Right';'Right'; ...
  'Left';'Right';'Right';'Left';'Right'};
finalch = false(size(ratnums));

% Event006 for 4 kHz, Event007 for 2 kHz, Event008 for 6 kHz
CSplus = repmat({'Event006'},size(ratnums));

exptMeta.ratT = table(ratnums, excluded, MWA, side, CSplus, finalch);

% column labels for data.trialinfo
% (4th column of cfg.trl = 1st column of data.trialinfo)
exptMeta.infotype = {'OrigTrialNum','StimulusType','PhaseNum','Intervals'};

% each row # here corresponds to the numerical code that will be saved in
% cfg.trl and data.trialinfo to identify which subtrials correspond to
% which interval types (0 means no stimulus)
exptMeta.stimuli = {'CS+'};       % 2nd column of data.trialinfo
exptMeta.stimstr = {'CSplus'};    % alternate strings for variable names, fieldnames, etc.
exptMeta.intervals = {'Before'; 'During'; 'After'};  % when divided into subtrials relative to tone
exptMeta.tCenter = [-15 15 45];   % center times for above intervals

% each column # here corresponds to cell column (2nd dim) in other vars.
% also 3rd column of data.trialinfo
exptMeta.phases = {'Conditioning', 'Recall', 'Extinction', 'Baseline'};
% added Baseline last to avoid re-numbering files, etc.
exptMeta.abbr = {'FC', 'Rcl', 'Ext', 'BL'};  % for filenames

% expected # of trials per phase, for error checking
exptMeta.expected = [17 15 15 1];

% break into 3-4 tone blocks to use as grouping for rough stats
exptMeta.blkedges = unique(ceil([linspace(0,10,4) linspace(10,exptMeta.expected(1),3) ...
  linspace(exptMeta.expected(1),sum(exptMeta.expected(1:2)),6) ...
  linspace(sum(exptMeta.expected(1:2)),sum(exptMeta.expected(1:3)),6) sum(exptMeta.expected)]));
exptMeta.blknames = {'Hab1','Hab2','Hab3','Acq1','Acq2','Rcl1', ...
  'Rcl2','Rcl3','Rcl4','Rcl5','Ext1','Ext2','Ext3','Ext4','Ext5','BL'};
% verify lineup with [exptMeta.blknames'  num2cell(cumsum(diff(exptMeta.blkedges')))]
exptMeta.mainblocks = [3 5 6 15];  % blocks to plot:  
% late habituation, late acquisition, early recall, and late extinction
exptMeta.blksof7names = {'Hab','Acq','Rcl','EarlyExt','ExtRcl','LateExt'};

% each row # here corresponds to the numerical code that will represent
% which region's data is being used for phase or amplitude source data
exptMeta.regions = {'mPFC'; 'BLA'};

% add table columns for single best channel in each region to use for
% cross-regional measures
exptMeta.ratT = [exptMeta.ratT cell2table(cell(size(exptMeta.ratT,1),numel(exptMeta.regions)), ...
  'VariableNames',exptMeta.regions)];

% initialize variables
exptMeta.goodchs = cell(size(exptMeta.ratT,1),1);  % cell array of strings can't be contained within one cell of a table
exptMeta.notes = cell(size(exptMeta.ratT,1),numel(exptMeta.phases));
exptMeta.toneTS = cell(size(exptMeta.ratT,1),numel(exptMeta.phases));  % needs to be preallocated so index doesn't exceed dimensions
exptMeta.nexfile = cell(size(exptMeta.ratT,1),numel(exptMeta.phases),2);   % 3rd dim:  aligned LFPs (1) & sorted spikes (2)

% any extra comments about each rat/session
exptMeta.notes{1,3} = 'tone timestamps for 200''s Ext are in "exptMeta.toneTS{1,3}"';
% I can't remember what happened here or how I retrieved them, so I should
% probably look back at these files at some point.
exptMeta.toneTS{1,3} = [486759 863759 1204759 1420759 1628759 1976759 2142759 ...
  2350759 2673759 2906759 3088759 3459759 3731760 3981760 4294760];
exptMeta.notes{2,3} = 'failed extinction';   % or really delayed (3rd extinction session available)
exptMeta.notes{3,2} = 'seizure-like activity in response to tones?'; % at least, lots of negatively skewed, spikey stuff
exptMeta.notes{7,2} = 'lost headcap before recall';  % excluded
exptMeta.notes{8,2} = 'failed to consolidate & express fear learning';   % not the latent inhibition rat (215)
exptMeta.notes{9,1} = 'Headstage unplugged after 2nd shock (possible seizure? review video)';
exptMeta.notes{10,1} = 'possible seizure after 4th shock? review video';
exptMeta.notes{11,3} = 'video recording of Ext is lost';   % probably saved over by 343's 0000.avi (verified match by recording duration)
exptMeta.notes{12,3} = 'lost headcap before Ext';  % Rcl is fine!

exptMeta.fsLFP = 1000;   % LFP sampling frequency
exptMeta.fsSpk = 40000;  % spike sampling frequency

exptMeta.tol = 0.01;   % tolerance for appending comparable bins of freq (Hz) & time (s),
% bin values must round to same multiple of tol to be included in any
% aggregate, but that's dumb because the difference can be way less than
% tol and cause bins to be dropped, so I'll probably end up submitting a
% bug report & suggesting they change ft_appendfreq to use ismembertol() or
% max(abs(diff()))

% other analysis parameters moved to function:  get_cfg_TEM

%% temp storage locations

if ~wait4server_TEM(tserver)  % provides opportunity to connect before following this backup plan
  disp('You do not appear to be connected to the S drive.')
  warning('Inputs may not be accessible, and outputs will be stored locally.')
  
  exptMeta.datafolder = [fileparts(userpath) filesep 'TempData' filesep];
  
  if ~exist(exptMeta.datafolder,'dir')
    mkdir(exptMeta.datafolder)
  end
  
  exptMeta.figurefolder  = [fileparts(userpath) filesep 'TempFigures' filesep];
else
  exptMeta.datafolder    = [tserver 'Analyses' filesep 'TempData' filesep ...
    'OrigFear' filesep];
  
  if ~exist(exptMeta.datafolder,'dir')
    mkdir(exptMeta.datafolder)
  end
  
  exptMeta.configfolder  = [tserver 'Analyses' filesep 'MWAconfigs' filesep];
  
  % put new figures in a folder named by date, in the format 'yyyy-mm-dd'
  exptMeta.figurefolder  = [tserver 'Figures' filesep 'TempFigures' filesep ...
    datestr(now,29) filesep];
end

if ~exist(exptMeta.figurefolder,'dir')
  mkdir(exptMeta.figurefolder)
end

%% define MWA configuration(s)

exptMeta.lftchs = {'AD01','AD05'; ...  % farthest left array configuration, where
  'AD02','AD06'; ...  % increasing rows indicate electrodes ordered A -> P,
  'AD03','AD07'; ...  % and increasing columns indicate those ordered L -> R.
  'AD04','AD08'};   % mPFC if implanted on right, BLA if implanted on left.

exptMeta.rtchs  = {'AD09','AD13'; ...  % farthest left array configuration, where
  'AD10','AD14'; ...  % increasing rows indicate electrodes ordered A -> P,
  'AD11','AD15'; ...  % and increasing columns indicate those ordered L -> R.
  'AD12','AD16'};   % BLA if implanted on right, mPFC if implanted on left.

exptMeta.allchs = sort([exptMeta.lftchs(:); exptMeta.rtchs(:)]);

if ~wait4server_TEM(tserver)
  warning('MWA config files are not currently accessible.  They can be recreated locally if necessary.')
else
  MWA = 'OldBatchOldFear';      % name for this microwire array configuration
  
  if exist([exptMeta.configfolder 'elec_' MWA '_Right.mat'],'file') && ...
      exist([exptMeta.configfolder 'layout_' MWA '_Right.mat'],'file')
    disp('using existing elec & layout files')
  else
    cfg = [];
    
    % array & target info
    cfg.atlascoord  = [2.7, 0.50, -5.00; ...  % AP/ML/DV relative to bregma
                      -3.0, 5.20, -8.50];     % dim 1/2/3 for below
    cfg.angle       = [-6; 6];  % + angle means approach from medial, in degrees
    cfg.angledim    = [2; 2];   % both probes angled along ML dim
    cfg.elecspac    = [0.25, 0.25; ...  % in AP dim, then ML dim, for each probe
                      0.25, 0.25];      % in mm
    
    cfg.label = {exptMeta.lftchs; exptMeta.rtchs}; 	% 2 cell arrays nested in another cell array
    cfg.side = {'Right';'Right'}; % which side of the brain each probe was on
    
    % calculate sensor positions & define layout
    MWAlayout_TEM(cfg,[MWA '_Right'],exptMeta.configfolder);
  end
  
  MWA = 'NewBatchOldFear';  % append suffix _Left or _Right for filenames
  
  if exist([exptMeta.configfolder 'elec_' MWA '_Right.mat'],'file') && ...
      exist([exptMeta.configfolder 'layout_' MWA '_Left.mat'],'file')
    disp('using existing elec & layout files')
  else
    cfg = [];
    
    % array & target info
    cfg.atlascoord  = [2.7, 0.50, -6.00; ... 	% AP/ML/DV relative to bregma
                      -3.0, 4.80, -8.60];     % dim 1/2/3 for below
    cfg.angle       = [-6; 6];  % + angle means approach from medial, in degrees
    cfg.angledim    = [2; 2];   % both probes angled along ML dim
    cfg.elecspac    = [0.25, 0.25; ...  % in AP dim, then ML dim, for each probe
                      0.25, 0.25];      % in mm
    
    %% for rats implanted on right side of brain
    
    cfg.label = {exptMeta.lftchs; exptMeta.rtchs}; 	% 2 cell arrays nested in another cell array
    cfg.side = {'Right';'Right'}; % which side of the brain each probe was on
    
    % calculate sensor positions & define layout
    MWAlayout_TEM(cfg,[MWA '_Right'],exptMeta.configfolder);
    
    %% for rats implanted on left side of brain
    
    cfg.label = {exptMeta.rtchs; exptMeta.lftchs}; 	% 2 cell arrays nested in another cell array
    cfg.side = {'Left';'Left'};   % which side of the brain each probe was on
    
    % calculate sensor positions & define layout
    MWAlayout_TEM(cfg,[MWA '_Left'],exptMeta.configfolder);
  end
end

%% clear extra variables

clearvars ratn* excluded MWA side finalch CS*

%% channels to use (or exclude if prefaced by '-') for each rat
% defaults to 'AD*' if empty
exptMeta.goodchs{1} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';'AD08';...
  'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15';'AD16'};  % all, from mtmfft/mtmconvol, verified by selectChsbyNaNs
% {'AD*','-AD16'};   before mtmfft
exptMeta.ratT.mPFC{1} = 'AD05';  % neither have any artifacts in core trials
exptMeta.ratT.BLA{1} = 'AD09';   % was 'AD10';   % selected by ridge prominence, but it has strangely reduced background power during recall
exptMeta.ratT.finalch(1) = true;

exptMeta.goodchs{2} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD08';...
  'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD16'};  % all but 'AD07';'AD15'; from mtmconvol, verified worst by selectChsbyNaNs
% before mtmfft:  {'AD*'}; % was ,'-AD04','-AD07','-AD08','-AD15'
% but NONE of these cause more than 152 ms of clipping within core trials
exptMeta.ratT.mPFC{2} = 'AD01'; % was 'AD04';  % selected by ridge prominence, 1 or 6 would have only subtle artifacts
exptMeta.ratT.BLA{2} = 'AD09'; % was 'AD12';   % selected by ridge prominence, 9, 10, or 14 would have only subtle artifacts, 13 seems to have focal seizure activity after shocks & during tones
exptMeta.ratT.finalch(2) = true;

exptMeta.goodchs{3} = {'AD01';'AD02';'AD03';'AD04';'AD05'; ...  % 6 & 8 were disabled during recording
  'AD10';'AD11';'AD12';'AD13';'AD14';'AD15'};   % all but 'AD07';'AD09'; ;'AD16', from mtmconvol, verified worst by selectChsbyNaNs
% before mtmfft:  {'AD*','-AD06','-AD08','-AD13','-AD16'}; % maybe ,'-AD07','-AD09'
% but these clip the same amount as others.
exptMeta.ratT.mPFC{3} = 'AD03'; % was 'AD05';  % selected by ridge prominence, 3 has fewer noticeable artifacts
exptMeta.ratT.BLA{3} = 'AD10';   % selected by ridge prominence, least artifacts in region, 1, 4, & 14 seem like they could be shorted together
exptMeta.ratT.finalch(3) = true;

exptMeta.goodchs{4} = {'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';'AD08';...
  'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15';'AD16'};  % all but 'AD01'; from mtmconvol, verified worst by selectChsbyNaNs
% before mtmfft:  {'AD*','-AD16'};   % others clip similar amounts (medium)
exptMeta.ratT.mPFC{4} = 'AD07'; % was 'AD03';  % selected by ridge prominence, 6 or 7 would have only subtle artifacts
exptMeta.ratT.BLA{4} = 'AD09'; % was 'AD12';   % selected by ridge prominence, 9 or 15 would have only subtle artifacts
exptMeta.ratT.finalch(4) = true;

exptMeta.goodchs{5} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';'AD08';...
  'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15'};  % all but ;'AD16', from mtmconvol, verified worst by selectChsbyNaNs
% before mtmfft2s:  {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';'AD08';...
%   'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15'};  % all but 16, from mtmfft8s?
exptMeta.ratT.mPFC{5} = 'AD05';  % selected by ridge prominence (and least missing time)
exptMeta.ratT.BLA{5} = 'AD10';   % selected by ridge prominence (and least missing time)
exptMeta.ratT.finalch(5) = true;

exptMeta.goodchs{6} = {'AD01';'AD02';'AD03';...
  'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15'};  % all but 'AD04';'AD05';'AD06';'AD07';'AD08';;'AD16', from mtmconvol, verified worst by selectChsbyNaNs
exptMeta.ratT.mPFC{6} = 'AD02';  % selected by fewest artifacts, was 'AD07';  % selected by ridge prominence (incl. interesting increase in Ext session)
exptMeta.ratT.BLA{6} = 'AD09';   % selected by ridge prominence, 10 or 15 would have slightly fewer artifacts, but all subtle anyway
exptMeta.ratT.finalch(6) = true;

% exptMeta.goodchs{7} = {'AD*','-AD01','-AD02','-AD03','-AD04','-AD08',...
%   '-AD09','-AD13','-AD15','-AD16'};  % doesn't matter - excluded
% exptMeta.ratT.mPFC{7} = 'AD05';
% exptMeta.ratT.BLA{7} = 'AD14';
% exptMeta.ratT.finalch(7) = true; % just to get something done but really false;

exptMeta.goodchs{8} = {'AD03';'AD04';'AD05';'AD06';'AD07';...
  'AD09';'AD10';'AD11';'AD12';'AD13';'AD14'};  % all but 'AD01';'AD02';'AD08';;'AD15';'AD16', from mtmconvol, verified worst during baseline by selectChsbyNaNs
% {'AD*','-AD01','-AD02','-AD13','-AD15','-AD16'};   % before mtmfft
% these clip >17s, compared to <4s for the rest
exptMeta.ratT.mPFC{8} = 'AD07';  % selected by ridge prominence (and least kurtosis), 4-6 would have only subtle artifacts
exptMeta.ratT.BLA{8} = 'AD12';   % selected by ridge prominence (and least kurtosis), 14 would have fewer artifacts, but all subtle anyway
exptMeta.ratT.finalch(8) = true;

% above (before mtmfft commented selection) based on clipping (notdeadchs), below not

exptMeta.goodchs{9} = {'AD03';'AD04';'AD05';'AD06';'AD07';'AD08'};  % only 1 array present, otherwise all but 'AD01';'AD02'; from mtmconvol, verified worst by selectChsbyNaNs
% {'AD*','-AD01','-AD02','-AD03','-AD04','-AD05','-AD08',...  % before mtmfft
exptMeta.ratT.mPFC{9} = 'none';
exptMeta.ratT.BLA{9} = 'AD04';   % mostly due to least kurtosis in ridge prominence, 7 or 8 would have fewer artifacts, but all subtle anyway
exptMeta.ratT.finalch(9) = true;

exptMeta.goodchs{10} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';'AD08';...
  'AD09';'AD10';'AD12'};  % all but 'AD11';'AD13';'AD14';'AD15';;'AD16' from mtmconvol, verified worst by selectChsbyNaNs
% {'AD*','-AD01','-AD02','-AD03',...  % before mtmfft
%   '-AD09','-AD10','-AD11','-AD13','-AD14','-AD15'};
exptMeta.ratT.mPFC{10} = 'AD08';   % selected by ridge prominence (also fewest artifacts), but fear effects are not apparent in any channel for this rat
exptMeta.ratT.BLA{10} = 'AD12';  % selected by fewest artifacts, was 'AD16'; % mostly due to least kurtosis in ridge prominence
exptMeta.ratT.finalch(10) = true;

exptMeta.goodchs{11} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';'AD08';...
  'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15';'AD16'};  % all, from mtmfft/mtmconvol, verified by selectChsbyNaNs
% {'AD*','-AD07','-AD08',...  % before mtmfft
%   '-AD11','-AD13','-AD14','-AD16'};
exptMeta.ratT.mPFC{11} = 'AD03';  % selected by ridge prominence, 5 or 6 would have fewer artifacts, but all subtle anyway
exptMeta.ratT.BLA{11} = 'AD12';   % selected by ridge prominence (and least kurtosis), 10 would have only subtle artifacts
exptMeta.ratT.finalch(11) = true;

exptMeta.goodchs{12} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD08';...
  'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15';'AD16'};  % all but 'AD07'; from mtmconvol, verified worst during baseline by selectChsbyNaNs
% {'AD*','-AD01','-AD02','-AD04','-AD06','-AD07',...  % before mtmfft
%   '-AD09','-AD10','-AD11'};
exptMeta.ratT.mPFC{12} = 'AD05';   % no artifacts in core tone trials
exptMeta.ratT.BLA{12} = 'AD12';  % 9, 10, or 13 would have no artifacts
exptMeta.ratT.finalch(12) = true;

exptMeta.goodchs{13} = {'AD01';'AD02';'AD03';'AD04';'AD05';'AD06';'AD07';'AD08';...
  'AD09';'AD10';'AD11';'AD12';'AD13';'AD14';'AD15';'AD16'};  % all, from mtmfft, verified by selectChsbyNaNs
% {'AD*','-AD01','-AD08',...  % before mtmfft
%   '-AD14','-AD15','-AD16'};
exptMeta.ratT.mPFC{13} = 'AD07';  % selected by ridge prominence (and least kurtosis), only a few subtle artifacts
exptMeta.ratT.BLA{13} = 'AD10';   % selected by ridge prominence, no artifacts in core trials
exptMeta.ratT.finalch(13) = true;

%% filenames

exptMeta.LFPfolder = [tserver 'PlexonData Raw' filesep];
exptMeta.spkfolder = [tserver 'SpikeSorting' filesep 'spike sorting-Orig Fear' filesep 'final' filesep];

% Habituation & Acquisition (fear conditioning)
exptMeta.nexfile{1,1,1} = {[exptMeta.LFPfolder '200' filesep '200_091003_0000-alignedLFPs.nex']; ...
  [exptMeta.LFPfolder '200' filesep '200_091003_0001-aligned.nex']};
% Can't use PlexUtil or NeuroExplorer merged files because either FieldTrip
% crashes or the timestamps are off after the merge.
% FT_preproc_Fear_TEM.m can handle this cellstr.
exptMeta.nexfile{1,1,2} = {[exptMeta.spkfolder 'Acq' filesep '200_091003_0000_1unit-03.nex']; ...   % missing???
  [exptMeta.spkfolder '200_091003_0001_1unit-03.nex']};
exptMeta.nexfile{2,1,1} = [exptMeta.LFPfolder '202' filesep '202_091005_0002-aligned.nex'];
exptMeta.nexfile{2,1,2} = [exptMeta.spkfolder 'Acq' filesep '202_091005_0002_1unit-02.nex'];
exptMeta.nexfile{3,1,1} = [exptMeta.LFPfolder '203' filesep '203_091204_0001-alignedLFPs.nex'];
exptMeta.nexfile{3,1,2} = [exptMeta.spkfolder 'Acq' filesep '203_091204_0001_1unit-02.nex'];
exptMeta.nexfile{4,1,1} = [exptMeta.LFPfolder '204' filesep '204_100313_0119-aligned.nex'];
exptMeta.nexfile{4,1,2} = [exptMeta.spkfolder 'Acq' filesep '204_100313_0119_1unit-02.nex'];
exptMeta.nexfile{5,1,1} = [exptMeta.LFPfolder '205' filesep '205_100311_0110-alignedLFPs.nex'];
exptMeta.nexfile{5,1,2} = [exptMeta.spkfolder 'Acq' filesep '205_100311_0110_1unit-03.nex'];
exptMeta.nexfile{6,1,1} = [exptMeta.LFPfolder '210' filesep '210_100611_0060-alignedLFPs.nex'];
exptMeta.nexfile{6,1,2} = [exptMeta.spkfolder 'Acq' filesep '210_100611_0060_1unit-03.nex'];
exptMeta.nexfile{7,1,1} = [exptMeta.LFPfolder '211' filesep '211_100514_0000-aligned.nex'];   % no spikes sorted because this rat is excluded anyway
exptMeta.nexfile{8,1,1} = [exptMeta.LFPfolder '212' filesep '212_100604_0044-alignedLFPs.nex'];
exptMeta.nexfile{8,1,2} = [exptMeta.spkfolder 'Acq' filesep '212_100604_0044_1unit-03.nex'];
exptMeta.nexfile{9,1,1} = {[exptMeta.LFPfolder '338' filesep '338_150513_0000-aligned.nex']; ...
  [exptMeta.LFPfolder '338' filesep '338_150513_0001-aligned.nex']};
exptMeta.nexfile{9,1,2} = {[exptMeta.spkfolder 'Acq' filesep '338_150513_0000_1unit-03.nex']; ...   % missing???
  [exptMeta.spkfolder 'Acq' filesep '338_150513_0001_1unit-03.nex']};
exptMeta.nexfile{10,1,1} = {[exptMeta.LFPfolder '339' filesep '339_150513_0002-aligned.nex']; ...
  [exptMeta.LFPfolder '339' filesep '339_150513_0003-aligned.nex']};   % no spikes sorted for this rat because there were no units found
exptMeta.nexfile{11,1,1} = [exptMeta.LFPfolder '341' filesep '341_150624_0001-aligned.nex'];
exptMeta.nexfile{11,1,2} = [exptMeta.spkfolder 'Acq' filesep '341_150624_0001_1unit-03.nex'];
exptMeta.nexfile{12,1,1} = [exptMeta.LFPfolder '342' filesep '342_150624_0003-aligned.nex'];
exptMeta.nexfile{12,1,2} = [exptMeta.spkfolder 'Acq' filesep '342_150624_0003_1unit-03.nex'];
exptMeta.nexfile{13,1,1} = {[exptMeta.LFPfolder '343' filesep '343_150624_0005-aligned.nex']; ...
  [exptMeta.LFPfolder '343' filesep '343_150624_0006_continue-aligned.nex']};
exptMeta.nexfile{13,1,2} = {[exptMeta.spkfolder 'Acq' filesep '343_150624_0005_1unit-03.nex']; ...
  [exptMeta.spkfolder 'Acq' filesep '343_150624_0006_1unit-03.nex']};  % missing???

% Recall (extinction 1)
exptMeta.nexfile{1,2,1} = [exptMeta.LFPfolder '200' filesep '200_091006_0001-aligned.nex'];
exptMeta.nexfile{1,2,2} = [exptMeta.spkfolder 'Ext1' filesep '200_091006_0001_1unit-08.nex'];
exptMeta.nexfile{2,2,1} = [exptMeta.LFPfolder '202' filesep '202_091008_0000-aligned.nex'];
exptMeta.nexfile{2,2,2} = [exptMeta.spkfolder 'Ext1' filesep '202_091008_0000_1unit-07.nex'];
exptMeta.nexfile{3,2,1} = [exptMeta.LFPfolder '203' filesep '203_091207_0000-aligned.nex'];
exptMeta.nexfile{3,2,2} = [exptMeta.spkfolder 'Ext1' filesep '203_091207_0000_1unit-06.nex'];
exptMeta.nexfile{4,2,1} = [exptMeta.LFPfolder '204' filesep '204_100316_0127-aligned.nex'];
exptMeta.nexfile{4,2,2} = [exptMeta.spkfolder 'Ext1' filesep '204_100316_0127_1unit-07.nex'];
exptMeta.nexfile{5,2,1} = [exptMeta.LFPfolder '205' filesep '205_100314_0121-aligned.nex'];
exptMeta.nexfile{5,2,2} = [exptMeta.spkfolder 'Ext1' filesep '205_100314_0121_1unit-06.nex'];
exptMeta.nexfile{6,2,1} = [exptMeta.LFPfolder '210' filesep '210_100614_0066-aligned.nex'];
exptMeta.nexfile{6,2,2} = [exptMeta.spkfolder 'Ext1' filesep '210_100614_0066_1unit-05.nex'];
exptMeta.nexfile{8,2,1} = [exptMeta.LFPfolder '212' filesep '212_100607_0049-aligned.nex'];
exptMeta.nexfile{8,2,2} = [exptMeta.spkfolder 'Ext1' filesep '212_100607_0049_1unit-06.nex'];
exptMeta.nexfile{9,2,1} = [exptMeta.LFPfolder '338' filesep '338_150516_0001-aligned.nex'];
exptMeta.nexfile{9,2,2} = [exptMeta.spkfolder 'Ext1' filesep '338_150516_0001_1unit-05.nex'];
exptMeta.nexfile{10,2,1} = [exptMeta.LFPfolder '339' filesep '339_150516_0000-aligned.nex'];  % no units found, so no spike sorting done
exptMeta.nexfile{11,2,1} = [exptMeta.LFPfolder '341' filesep '341_150627_0000-aligned.nex'];
exptMeta.nexfile{11,2,2} = [exptMeta.spkfolder 'Ext1' filesep '341_150627_0000_1unit-04.nex'];
exptMeta.nexfile{12,2,1} = [exptMeta.LFPfolder '342' filesep '342_150627_0001-aligned.nex'];
exptMeta.nexfile{12,2,2} = [exptMeta.spkfolder 'Ext1' filesep '342_150627_0001_1unit-04.nex'];
exptMeta.nexfile{13,2,1} = [exptMeta.LFPfolder '343' filesep '343_150627_0003-aligned.nex'];
exptMeta.nexfile{13,2,2} = [exptMeta.spkfolder 'Ext1' filesep '343_150627_0003_1unit-05.nex'];

% Extinction (extinction 2)
exptMeta.nexfile{1,3,1} = [exptMeta.LFPfolder '200' filesep '200_091007_0000-aligned.nex'];
exptMeta.nexfile{1,3,2} = [exptMeta.spkfolder 'EXT2' filesep '200_091007_0000_1unit-03.nex'];
exptMeta.nexfile{2,3,1} = [exptMeta.LFPfolder '202' filesep '202_091009_0000-aligned.nex'];
exptMeta.nexfile{2,3,2} = [exptMeta.spkfolder 'EXT2' filesep '202_091009_0000_1unit-03.nex'];
exptMeta.nexfile{3,3,1} = [exptMeta.LFPfolder '203' filesep '203_091208_0000-aligned.nex'];
exptMeta.nexfile{3,3,2} = [exptMeta.spkfolder 'EXT2' filesep '203_091208_0000_1unit-03.nex'];
exptMeta.nexfile{4,3,1} = [exptMeta.LFPfolder '204' filesep '204_100317_0131-aligned.nex'];
exptMeta.nexfile{4,3,2} = [exptMeta.spkfolder 'EXT2' filesep '204_100317_0131_1unit-03.nex'];
exptMeta.nexfile{5,3,1} = [exptMeta.LFPfolder '205' filesep '205_100315_0124-aligned.nex'];
exptMeta.nexfile{5,3,2} = [exptMeta.spkfolder 'EXT2' filesep '205_100315_0124_1unit-03.nex'];
exptMeta.nexfile{6,3,1} = [exptMeta.LFPfolder '210' filesep '210_100615_0068-aligned.nex'];
exptMeta.nexfile{6,3,2} = [exptMeta.spkfolder 'EXT2' filesep '210_100615_0068_1unit-03.nex'];
exptMeta.nexfile{8,3,1} = [exptMeta.LFPfolder '212' filesep '212_100608_0053-aligned.nex'];
exptMeta.nexfile{8,3,2} = [exptMeta.spkfolder 'EXT2' filesep '212_100608_0053_1unit-03.nex'];
exptMeta.nexfile{9,3,1} = [exptMeta.LFPfolder '338' filesep '338_150517_0000-aligned.nex'];
exptMeta.nexfile{9,3,2} = [exptMeta.spkfolder 'EXT2' filesep '338_150517_0000_1unit-03.nex'];
exptMeta.nexfile{10,3,1} = [exptMeta.LFPfolder '339' filesep '339_150517_0002-aligned.nex'];  % no units worth sorting
exptMeta.nexfile{11,3,1} = [exptMeta.LFPfolder '341' filesep '341_150628_0000_VideoLost-aligned.nex'];
exptMeta.nexfile{11,3,2} = [exptMeta.spkfolder 'EXT2' filesep '341_150628_0000_1unit-03.nex'];
exptMeta.nexfile{13,3,1} = [exptMeta.LFPfolder '343' filesep '343_150628_0000-aligned.nex'];
exptMeta.nexfile{13,3,2} = [exptMeta.spkfolder 'EXT2' filesep '343_150628_0000_1unit-04.nex'];

% Baseline (no spikes sorted)
exptMeta.nexfile{1,4,1} = [exptMeta.LFPfolder '200' filesep '200_091002_0000-aligned.nex'];
exptMeta.nexfile{2,4,1} = [exptMeta.LFPfolder '202' filesep '202_091004_0001-aligned.nex'];
exptMeta.nexfile{3,4,1} = [exptMeta.LFPfolder '203' filesep '203_091203_0000-aligned.nex'];
exptMeta.nexfile{4,4,1} = [exptMeta.LFPfolder '204' filesep '204_100312_0116-aligned.nex'];
exptMeta.nexfile{5,4,1} = [exptMeta.LFPfolder '205' filesep '205_100310_0103-aligned.nex'];
exptMeta.nexfile{6,4,1} = [exptMeta.LFPfolder '210' filesep '210_100610_0058-aligned.nex'];
exptMeta.nexfile{7,4,1} = [exptMeta.LFPfolder '211' filesep '211_100513_0000-aligned.nex'];
exptMeta.nexfile{8,4,1} = [exptMeta.LFPfolder '212' filesep '212_100603_0037-aligned.nex'];
exptMeta.nexfile{9,4,1} = [exptMeta.LFPfolder '338' filesep '338_150512_0002-aligned.nex'];
exptMeta.nexfile{10,4,1} = [exptMeta.LFPfolder '339' filesep '339_150512_0000-aligned.nex'];
exptMeta.nexfile{11,4,1} = [exptMeta.LFPfolder '341' filesep '341_150623_0000_baseline-aligned.nex'];
exptMeta.nexfile{12,4,1} = [exptMeta.LFPfolder '342' filesep '342_150623_0001-aligned.nex'];
exptMeta.nexfile{13,4,1} = [exptMeta.LFPfolder '343' filesep '343_150623_0002-aligned.nex'];

%% behavior files

exptMeta.operantfolder = [tserver 'Behavior' filesep 'ABETdata' filesep ...
  'RawDataExports' filesep];  % one file per ABET session n this group folder
exptMeta.freezefolder = [tserver 'Behavior' filesep 'FreezeScan' filesep];  
% 2nd batch of rats together in one Excel file in subfolder 'RawDataExports', 
% older rats each have their own separate subfolders

%% complete workflow - MOST REPLACED BY fullFTpipeline_OrigFear.m script
% 0) X align original .plx files, save as -aligned.nex
% 1) X FT_preproc_OrigFear_TEM - Load all data from -aligned.nex files into
%     FieldTrip format & merge into 1 dataset per rat, using new
%     trialfun_TEM.m and supporting a cell array of strings for multiple
%     filenames within one phase (to avoid merged NEX files, which are hard
%     to read correctly).
% 2) X FT_AutoArtReject_OrigFear_TEM -> now trying
%     FT_ClipShockReject_OrigFear_TEM to avoid mistaking real data for artifacts
% 3) X FT_mtmconvolTFA_OrigFear_TEM.m - calculate spectrograms & coherograms 
%     using mtmconvol
% 5)  FT_NormPlotTFA_OrigFear_TEM.m - normalize spectrograms & coherograms
%     to baseline & plot if desired
% 6)  FT_GrpDescTFA_OrigFear_TEM.m - calculate group statistics on
%     normalized spectrograms
% 7)  FT_timelessfreq_Fear_TEM.m - calculate multitaper spectrum &
%     coherence for specific chunks of time (e.g., during tones)
% 8)  statistical testing between phases or time points
% 9)  compare to behavior
% 10) cross-frequency
% 11) granger causality?
% 12) spikes??
