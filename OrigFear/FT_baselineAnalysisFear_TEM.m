%% Calculate baseline spectrograms and coherograms for each rat

close all; clearvars; clc;  % clean slate

ft_defaults                 % set FieldTrip defaults
info_fear                   % load all background info (filenames, etc)

foi       = 1.05.^(0:95);   % freqs from 1-103 Hz progressing up by 5% each
tapsmofrq = foi/20;         % smooth each band by +/- 5%

for f = find(tapsmofrq < 0.25)  % don't care about narrower bands than this
  tapsmofrq(f) = 0.25;          % keeps time windows under 8 seconds
end

t_ftimwin = 2./tapsmofrq;   % for 3 tapers (K=3), T=2/W

plt       = false;           % whether or not to plot spec/coherograms

%% use try-catch when running unsupervised batches

for r = 6 % 1:size(ratT,1)
  %   try
  outputfile = [datafolder 'SpecCoherograms_' ...
    int2str(ratT.ratnums(r)) 'Baseline.mat'];
  
  filecorrect = false; % start false in case file doesn't exist
  
  if exist(outputfile,'file')
    filecorrect = true; % true unless demonstrated to be false
    disp(['Baseline data for Rat # ' int2str(ratT.ratnums(r)) ...
      ' already analyzed! Checking for accuracy.']);
    load(outputfile);
    
    if freq.time(1) >= 0
      warning('no pre-tone baseline found');
      filecorrect = false;
    else
      disp(['first timestamp is ' num2str(freq.time(1))]);
    end
    
    if strcmp(goodchs{r}{1},'AD*')
      if numel(freq.label) ~= (17 - numel(goodchs{r}))
        warning('different channels used than planned');
        filecorrect = false;  % reprocess this file
      else
        for ch = 1:numel(freq.label)
          k = strfind(goodchs{r}, freq.label{ch});
          if any([k{:}])
            warning([freq.label{ch} ' should be excluded']);
            filecorrect = false;  % reprocess this file
            break  % jumps to end of for ch loop
          else
            disp([freq.label{ch} ' is not excluded']);
          end
        end   % still true if no excluded channels
      end
    elseif numel(freq.label) ~= numel(goodchs{r}) || ...
        any(any(~strcmp(freq.label,goodchs{r})))
      warning('different channels used than planned');
      filecorrect = false;  % reprocess this file
    else
      disp([freq.label{:} ' are correct']);   % still true
    end
  end
  
  if filecorrect
    disp('checked file seems correct.  skipping.');
  else
    %% gather baseline data
    p = 1;            % from 5 minute baseline before habituation tones
    
    % Clean LFP data, broken into arbitrary "trials" to remove
    % chunks of time containing artifacts (NaNs, low freq, theta
    % freq, and large), and filtered to remove high frequency
    % chewing artifacts
    inputfile = [datafolder 'CleanLFPContData_' ...
      int2str(ratT.ratnums(r)) phases{p} '.mat'];
    
    if exist(inputfile,'file')
      %% load data
      
      load(inputfile);
      
      %% identify baseline period
      
      if data.time{1}(1) >= 0
        error('no pre-tone baseline found')
      end
      
      baseS = [];     % baseline trial structure
      
      for tr = 1:numel(data.time)   % for each arbitrary trial
        if data.time{tr}(end) < 0   % all samples are before the 1st tone
          baseS = [baseS; data.sampleinfo(tr,1:2)]; %#ok<AGROW>
        elseif data.time{tr}(1) < 0 % some samples are before the 1st tone
          baseS = [baseS; data.sampleinfo(tr,1), ...
            data.sampleinfo(tr,1)+find(data.time{tr}(:)<0,1,'last')-1]; %#ok<AGROW>
        else                % no samples are before the 1st tone
          trials = 1:tr-1;  % all trials with any samples before 1st tone
          break             % skip the remaining trials
        end
      end
      
      if sum(diff(baseS,1,2)) < 300*data.fsample
        warning('less than 5 minutes of pre-tone baseline found')
      end
      
      %% calculate baseline spectra & coherence
      
      disp(['Calculating baseline spectra & coherence for Rat # ' ...
        int2str(ratT.ratnums(r)) ' (' int2str(trials(end)) ...
        ' trials)']);
      
      cfg             = [];
      
      cfg.method      = 'mtmconvol';
      cfg.output      = 'powandcsd';
      cfg.keeptrials  = 'no';
      cfg.trials      = trials;
      
      cfg.foi         = foi;
      cfg.tapsmofrq   = tapsmofrq;
      cfg.t_ftimwin   = t_ftimwin;
      
      % time windows overlap by at least 50%
      cfg.toi         = data.time{1}(1):min(t_ftimwin)/2:0;
      
      cfg.pad = 2^nextpow2(max(diff(data.sampleinfo(trials,:),1,2)))...
        /data.fsample;
      cfg.outputfile  = outputfile;
      
      freq = ft_freqanalysis(cfg,data);
      
      %% plot, if desired
      
      if plt
        %% calculate coherence
        
        for i = 1:size(freq.labelcmb,1) %#ok<UNRCH> set in first cell
          % find 1st ch index
          indx1 = strcmp(freq.labelcmb(i,1),freq.label);
          
          % find 2nd ch index
          indx2 = strcmp(freq.labelcmb(i,2),freq.label);
          
          freq.coherence(i,:,:) = abs(freq.crsspctrm(i,:,:)./ ...
            sqrt(freq.powspctrm(indx1,:,:).* ...
            freq.powspctrm(indx2,:,:)));
        end
        
        %% display raw spectrograms & coherograms
        
        for target = 1:3
          cfg                 = [];
          
          if target == 1
            cfg.parameter       = 'powspctrm';
            cfg.channel         = ratT.chmPFC{r};
          elseif target == 2
            cfg.parameter       = 'powspctrm';
            cfg.channel         = ratT.chBLA{r};
          else
            cfg.parameter       = 'coherence';
            cfg.zlim            = 'zeromax';
            cfg.channel         = ratT.chmPFC{r};
            cfg.refchannel      = ratT.chBLA{r};
          end
          
          if ~strcmp('none',cfg.channel) && ...
              (~isfield(cfg,'refchannel') || ~strcmp('none',cfg.refchannel))
            figure(target);  clf;
            ft_singleplotTFR(cfg, freq);
            colormap(jet);
            xlabel('Time from 1st Tone Onset (seconds)');
            ylabel('Frequency (Hz)');
            
            if target < 3
              title(['Rat ' int2str(ratT.ratnums(r)) ' Baseline ' ...
                regions{target} ' Power']);
              print([figurefolder int2str(ratT.ratnums(r)) ...
                'BaselineSpectrogram_' regions{target}],'-dpng');
            else
              title(['Rat ' int2str(ratT.ratnums(r)) ...
                ' Baseline Coherence']);
              print([figurefolder int2str(ratT.ratnums(r)) ...
                'BaselineCoherogram'],'-dpng');
            end
          end
        end
      end
    end
  end
  %% clear variables
  
  clearvars data cfg freq;
  
  %   catch ME
  %     warning(['Error (' ME.message ') while processing Rat # ' ...
  %       int2str(ratT.ratnums(r)) ', ' phases{p} ...
  %       '! Continuing with next in line.']);
  %   end
end
