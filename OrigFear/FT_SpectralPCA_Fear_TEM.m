% identify spectral patterns (principal components of the complete
% spectrogram) that differentiate between fear states and define
% acquisition and extinction related state space trajectories

close all; clearvars; clc;  % clean slate

ft_defaults                 % set FieldTrip defaults
info_fear                   % load all background info (filenames, etc)

errcnt = 0;                 % error count
errloc = [];                % error location (r,p)

%% full loop

for r = 1:size(ratT,1)
  for p = 3:numel(phases)
    try
      if ~isempty(nexfile{r,p})
        %% try it on one file before looping
        
        inputfile = [datafolder 'RawLFPTrialData_'...
          int2str(ratT.ratnums(r)) phases{p} '.mat'];
        
        outputfile1 = [datafolder 'RawTrialSpectrograms_'...
          int2str(ratT.ratnums(r)) phases{p} '.mat'];
        
        outputfile2 = [datafolder 'SpectralPCA_'...
          int2str(ratT.ratnums(r)) phases{p} '.mat'];
        
        %% load appropriate file
        
%         if exist(outputfile2,'file')
%           disp(['outputfile2 already exists.  Loading ' outputfile2]);
%           load(outputfile2);
%         end
        
%         if ~exist('mfreq','var')
          if exist(outputfile1,'file')
            disp(['outputfile1 already exists.  Loading ' outputfile1]);
            load(outputfile1);
          elseif ~exist(inputfile,'file')
            error('inputfile missing');
          else
            % warning(['outputfile1 does not exist.  Skipping ' outputfile1]);
            % break
            %% calculate spectrograms & coherograms
            
            cfg                     = [];
            
            cfg.method              = 'mtmconvol';
            cfg.output              = 'pow';
            cfg.keeptrials          = 'yes';
            cfg.foi                 = foi;
            cfg.tapsmofrq           = tapsmofrq;
            cfg.t_ftimwin           = t_ftimwin;
            cfg.toi                 = 'all';
            cfg.pad                 = 'nextpow2';
            
            cfg.inputfile           = inputfile;
            cfg.outputfile          = outputfile1;
            
            freq = ft_freqanalysis(cfg);
            % MODIFIED 4/25/16 by TEM -
            % (1) line 264 - improve "educated guess" when cfg.toi = 'all'
            % and cfg.t_ftimwin exists:  now uses minimum window length
            % instead of minimum time between samples
            % (2) line 420 - add "nextpow2" option for cfg.pad:  rounds
            % maximum trial length (# of samples) up to next power of 2
          end
          
          disp('Data loaded.  Reformating.');
          
          %% store analysis details
          
          foi = freq.freq;
          P = unique(nextpow2(foi));
          fticks = 2.^P(1:end-1);   % for labeling log scale frequency axes
          
          chs = freq.label;  tapsmofrq = freq.cfg.tapsmofrq;
          trials = freq.trialinfo; time = freq.time;
          
          %% merge all trials into one long time axis & all channels into
          % one long "frequency" axis
          
          temp = mat2cell(freq.powspctrm, ones(1,size(trials,1)), ...
            ones(1,numel(chs)), numel(foi), numel(time));
          temp2 = cell(size(trials,1),1);
          for tr = 1:size(trials,1)
            for ch = 1:numel(chs)
              % remove singleton dimensions & convert to dB
              temp{tr,ch} = 10*log10(squeeze(temp{tr,ch}));
            end
            temp2{tr} = vertcat(temp{tr,:});
          end
          mfreq = struct('label',{freq.label}, ...
            'dimord',freq.dimord(10:end), ...
            'freq',repmat(foi,1,numel(chs)), ...
            'time',repmat(time,1,size(trials,1)), 'powspctrm',[temp2{:}]);
          
          %% clear temp variables
          
          clearvars tem*
          
          %% remove time points with any NaNs
          
          tnan = isnan(mfreq.powspctrm(1,:));
          
          mfreq.time = mfreq.time(~tnan);
          mfreq.powspctrm = mfreq.powspctrm(:,~tnan);
          
          % get tone # corresponding to each time point
          tones = sort(repmat(trials(:,1),length(time),1));
          tones = tones(~tnan);
          
%         end
        
        %% get some indices & labels for plotting
        
        tsamp = 1:floor(numel(mfreq.time)/1000):numel(mfreq.time);
        tticks = find(abs(mfreq.time) < 0.1);   % tone onset samples
        tlabels = cellstr(num2str(trials(:,1)));
        
        tonezone = abs(mfreq.time) < 60;  % +/-60s from each tone onset
        tonezoneticks = sort([ ...
          find(abs(abs(mfreq.time(tonezone))-60) < 0.095) ...
          find(abs(abs(mfreq.time(tonezone))-30) < 0.095) ...
          find(abs(mfreq.time(tonezone)) < 0.095)]);
        tonezonelabels = cell(numel(tonezoneticks),1);
        next = 1;
        for tr = 1:numel(tlabels)
          tonezonelabels{next} = '';
          tonezonelabels{next+1} = ['T' tlabels{tr}];   % tone onset
          tonezonelabels{next+2} = '';
          tonezonelabels{next+3} = '/';   % transition between trials
          next = next+4;
        end
        
        % find index of lowest & highest frequency per channel
        chmin = find(mfreq.freq == min(mfreq.freq));
        chmax = find(mfreq.freq == max(mfreq.freq));
        
        %% verify correct arrangement
        
        figure(1); clf;
        imagesc(tsamp, 1:numel(mfreq.freq), mfreq.powspctrm(:,tsamp));
        ax = gca; ax.YTick = chmin; ax.YTickLabel = chs; axis xy;
        colormap(jet);
        ylabel('1-150 Hz (log2 scale) per Channel');
        xlabel('Tone #'); ax.XTick = tticks; ax.XTickLabel = tlabels;
        title('Sampling of Spectrogram by Channels');
        figname{1} = '_multispec';
        
        %% calculate spectral PCA
        
        if ~exist('sPCA','var')
          disp('Calculating PCA');
          [coeff,score,latent,tsquared,explained,mu] = pca( ...
            mfreq.powspctrm', 'Rows','complete', ...
            'VariableWeights','variance');
          sPCA = struct('coeff',coeff, 'score',score, ...
            'latent',latent, 'tsquared',tsquared, ...
            'explained',explained, 'mu',mu);
          clearvars coeff score latent tsquared explained mu
        end
        
        %% plots
        
        disp('Plotting');
        
        figure(2); clf; hold all;
        for ch = 1:numel(chs)
          for pc = 1:7   % 1st 7 PCs (# of colors in default rotation)
            plot(foi,sPCA.coeff(chmin(ch):chmax(ch),pc), ...
              'DisplayName',['PC' num2str(pc) ' on ' chs{ch}])
          end
        end
        ax = gca; ax.XScale = 'log'; ax.XTick = fticks; axis tight;
        legend('Location','bestoutside');
        xlabel('Frequency (Hz)'); ylabel('Arbitrary Units');
        title('1st 7 Principal Components');
        figname{2} = '_PCcoeff';
        
        figure(3); clf;
        pareto(sPCA.explained); ylim([0 sum(sPCA.explained(1:11))]);
        xlabel('Principal Component'); ylabel('Variance Explained (%)');
        title('Cumulative Variance Explained by Each PC');
        figname{3} = '_PCexplained';
        
        figure(4); clf;
        scatter3(sPCA.score(tonezone,1), sPCA.score(tonezone,2), ...
          sPCA.score(tonezone,3), tones(tonezone), mfreq.time(tonezone));
        xlabel('PC1'); ylabel('PC2'); zlabel('PC3');
        colormap(jet); colorbar; axis tight;
        title({'Spectral PCA State Space Trajectory '; ...
          'Colorbar indicates Time from Tone Onset (seconds)'; ...
          'Circle size increases with Tone #'});
        figname{4} = '_PCA_SST';
        
        figure(5); clf; hold all;
        for pc = 1:7   % 1st 7 principal components
          plot(1:sum(tonezone), sPCA.score(tonezone,pc), ...
            'DisplayName',['PC' num2str(pc)])
        end
        ax = gca; legend('show'); ylabel('Arbitrary Units');
        xlabel('Tone # (+/-60s around each tone onset');
        ax.XTick = tonezoneticks; ax.XTickLabel = tonezonelabels;
        title('Strength of 1st 7 Principal Components over Time');
        figname{5} = '_PCscoreVtime';
        
        figure(6); clf;
        h = cell(6,3);  % to hold histogram handles for each PC, BDA tones
        for pc = 1:6    % 1st 6 principal components
          subplot(2,3,pc); hold all;
          h{pc,1} = histogram(sPCA.score(mfreq.time <= 0, pc), ...
            'Normalization','pdf', 'DisplayStyle','stairs', ...
            'DisplayName','Before');
          h{pc,2} = histogram(sPCA.score(mfreq.time > 0 & ...
            mfreq.time <= 30, pc), 'Normalization','pdf', ...
            'DisplayStyle','stairs', 'DisplayName','During');
          h{pc,3} = histogram(sPCA.score(mfreq.time > 30, pc), ...
            'Normalization','pdf', 'DisplayStyle','stairs', ...
            'DisplayName','After');
          title(['PC' num2str(pc)]);
          legend('Location','best');
        end
        figname{6} = '_PCscoreHist';
        
        % scan for different distributions by tone number and BDA, then
        % redo scatter plot based on most differentiated PCs
        
        %% save figures
        
        for fig = 1:6
          figure(fig);
          print([figurefolder int2str(ratT.ratnums(r)) phases{p} ...
            figname{fig}],'-dpng');
        end
        
        %% save & clear data
        
        save(outputfile2,'sPCA','mfreq','foi','chs','tapsmofrq', ...
          'trials','time');
        clearvars('sPCA','*req');
        
      end
    catch ME
      errcnt = errcnt + 1;
      errloc(errcnt,:) = [r p]; %#ok<SAGROW>
      warning(['Error (' ME.message ') while processing Rat # ' ...
        int2str(ratT.ratnums(r)) ', ' phases{p} ...
        '! Continuing with next in line.']);
    end
  end
end
