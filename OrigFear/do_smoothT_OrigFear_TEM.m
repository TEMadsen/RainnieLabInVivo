function [output] = do_smoothT_OrigFear_TEM( spectype, r, twin, tshift )
% DO_SMOOTHT_ORIGFEAR_TEM smooths and downsamples the time axis of any TFA,
% specified by spectype & r, to the median over twin sized time windows,
% shifting by tshift (both in s).  Also converts fourier to powspctrm and
% performs dB transform if needed.
%
%   Depends on global metadata variable defined in info_origfear.
%
% written 8/6/17 by Teresa E. Madsen, Ph.D.

%% import global metadata structure

global exptMeta

if isempty(exptMeta);   info_origfear;   end

%% check inputs & assign defaults

if nargin == 0  % like when I just hit "Run and Time" in the Editor tab
  spectype  = 'piWav';  % close to FT default wavelet params, but using pi for integer # of cycles, etc.
  r         = 1;        % poster rat
  twin      = 3;        % length of time windows (s)
  tshift    = 0.3;      % shifting by this size step (s)
end

%% create waitbar

wb = waitbar(0, ['Preparing to smooth and downsample time axes of ' spectype ...
  ' for rat #' num2str(exptMeta.ratT.ratnums(r))]);

%% loop through all .mat files in spectype folder

listing = dir([exptMeta.datafolder spectype filesep ...
  num2str(exptMeta.ratT.ratnums(r)) filesep '*fourier.mat']);
if numel(listing) > sum(exptMeta.expected)
  warning('more files than expected - check for duplicates')
  keyboard
end
comptime  = nan(size(listing));     % for each file
esttime   = 50;                     % estimated comptime (s) per file
% esttime = nanmean(comptime);      % average of past calculations
output    = nan(numel(listing),2);  % skewness before/after dB conversion

for fn = 1:numel(listing)
  starttime = tic;
  waitbar(fn/numel(listing), wb, ...
    ['smoothing & downsampling time axis of ' listing(fn).name]);
  
  %% define input & output files, skipping trial if already complete
  
  inputfile = [listing(fn).folder filesep listing(fn).name];
  
  % dB converted version:
  outputfile1 = replace(inputfile, exptMeta.phases, exptMeta.abbr);
  outputfile1 = strrep(outputfile1,'_raw_','_raw');
  outputfile1 = strrep(outputfile1,'waveletTFAfourier',[spectype 'TFAdBpow']);
  
  % time-smoothed version:
  outputfile2 = strrep(outputfile1,[filesep spectype filesep],...
    [filesep 'smoothT' filesep]);   % change folder and filename
  outputfile2 = strrep(outputfile2, '.mat', ['_smoothT' ...
    strrep(num2str(twin),'.','_') 'x' strrep(num2str(tshift),'.','_') '.mat']);
  
  if existfile_TEM(outputfile2)
    warning(['outputfile already exists: ' ...
      outputfile2(numel(exptMeta.datafolder):end)])
    continue  % skip to next file
  end
  
  %% skip to dB version if already saved, otherwise, load fourier version
  
  if existfile_TEM(outputfile1)
    freq = rmvlargefields_TEM(outputfile1);
  else
    freq = rmvlargefields_TEM(inputfile);
    
    %% convert to powspctrm & check dimord
    
    freq = ft_checkdata(freq,'cmbrepresentation','sparsewithpow');
    
    switch freq.dimord
      case 'rpt_chan_freq_time'
        freq.powspctrm = shiftdim(freq.powspctrm,1);  % wrap trial dim to end
        freq.dimord = 'chan_freq_time';
        if contains(listing(fn).name,'Baseline')
          freq.time = freq.time(1):mean(diff(freq.time)): ...
            (freq.time(end)*size(freq.powspctrm,4));  % extrapolate time axis out (probably longer than necessary)
          freq.powspctrm = reshape(freq.powspctrm, ...
            size(freq.powspctrm,1),size(freq.powspctrm,2),[]);  % concatenate all time into 1 trial
          freq.time = freq.time(1:size(freq.powspctrm,3));  % trim to correct number of samples
        else
          assert(size(freq.powspctrm,4) == 1, 'multiple trials not expected except during baseline')
        end
      case 'chan_freq_time'
        % okay, do nothing
      otherwise
        error('unexpected dimord')
    end
    
    %% convert to dB if necessary
    
    if ~any(freq.powspctrm(:) <= 0) % && skewness(freq.powspctrm(:)) > 3
      output(fn,1) = skewness(freq.powspctrm(:));
      
      % faster to do this in two steps, otherwise it runs it one element at a time
      cfg = [];
      cfg.parameter   = 'powspctrm';
      cfg.operation   = 'log10';
      freq = ft_math(cfg,freq);
      
      cfg.operation   = 'multiply';
      cfg.scalar      = 10;
      cfg.outputfile  = outputfile1;
      freq = ft_math(cfg,freq);
      
      output(fn,2) = skewness(freq.powspctrm(:));
      if abs(output(fn,2)) > output(fn)
        warning('dB conversion may have made distribution *less* normal')
      end
    end
  end
  
  %% check for NaNs
  
  if any(isnan(freq.powspctrm(:))) && ...             % when there are any NaNs,
      all(cellfun(@isempty,regexp(listing(fn).name, ...   % and the filename
      {'CS\+12ofConditioning\w*200\w*\.mat', ...      % doesn't match either of
      'CS\+12ofConditioning\w*338\w*\.mat'}, 'once')))  % these known exceptions
    warning('NaNs in converted powspctrm')
    figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
    keyboard  % pause to verify
  end
  
  %% replace acq powspctrm time/freq bins including shock artifact w/ NaNs
  
  wavlen = ft_findcfg(freq.cfg, 'wavlen');
  if ~isempty(regexp(listing(fn).name,'CS\+1[1-7]ofConditioning\w*\.mat','once'))
    for f = 1:numel(freq.freq)
      badt = freq.time > 29.5 - wavlen(f)/2 & ...
        freq.time < 30 + wavlen(f)/2;   % so no time bins will include shock time
      freq.powspctrm(:,f,badt) = NaN;
    end
  end
  % figure; plotFTmatrix_TEM(struct('zlim','zeromax'),freq)
  
  %% smooth & downsample time axis
  
  smthfreq = freq;
  smthfreq.time = (round(freq.time(1)/tshift):round(freq.time(end)/tshift)) ...
    *tshift;
  smthfreq.powspctrm = nan(size(freq.powspctrm,1), ...
    numel(smthfreq.freq), numel(smthfreq.time));
  
  for f = 1:numel(smthfreq.freq)  % use median to preserve sharp changes & avoid biasing by skew,
    for t = 1:numel(smthfreq.time)
      tndx = abs(freq.time-smthfreq.time(t)) < twin/2;   % +/-5s
      if ~any(tndx)
        warning('no times selected')
        keyboard
      end
      if twin + tshift < 29.5 - wavlen(1)/2 || ...  % window length is short enough to fit in tone before shock artifact
          any(freq.time(tndx) < 29.5) && any(freq.time(tndx) > 30)  % or this time point includes time both before and after shock
        % include nan so no time periods overlap shock
        smthfreq.powspctrm(:,f,t) = median(freq.powspctrm(:,f,tndx),3,'includenan');
      else  % omit nan to avoid completely eliminating during & after tone periods in acquisition
        smthfreq.powspctrm(:,f,t) = median(freq.powspctrm(:,f,tndx),3,'omitnan');
      end
    end  
    % for ch = 1:numel(smthfreq.label)
    % fig = figure; fig.WindowStyle = 'docked';
    % subplot(1,2,1); histfit(squeeze(freq.powspctrm(ch,f,:)));
    % title(['f = ' num2str(round(freq.freq(f),3,'significant')) ' Hz, skew = ' num2str(skewness(squeeze(freq.powspctrm(ch,f,:)))) ', before smoothing'])
    % subplot(1,2,2); autocorr(squeeze(freq.powspctrm(ch,f,:)),find(freq.time - freq.time(1) > 30,1,'first'))
    % fig = figure; fig.WindowStyle = 'docked';
    % subplot(1,2,1); histfit(squeeze(smthfreq.powspctrm(ch,f,:)));
    % title(['f = ' num2str(round(smthfreq.freq(f),3,'significant')) ' Hz, skew = ' num2str(skewness(squeeze(smthfreq.powspctrm(ch,f,:)))) ', after smoothing'])
    % subplot(1,2,2); autocorr(squeeze(smthfreq.powspctrm(ch,f,:)),30)
    % end
  end
  % figure; plotFTmatrix_TEM(struct('zlim','zeromax'),smthfreq)
   
  %% save smoothed file
  
  freq = smthfreq; %#ok<NASGU> saved to file
  disp(['saving freq to ' outputfile2(numel(exptMeta.datafolder):end)])
  save(outputfile2,'freq');
  
  %% clear freq to conserve memory
  
  clearvars freq smthfreq
  
  %% update estimate of time to complete analysis
  
  comptime(fn) = toc(starttime);
  esttime = nanmean([esttime; comptime(fn)]); % average into estimate of how long each file should take
end

%% clean up

% output = comptime;
close(wb);

end   % function
