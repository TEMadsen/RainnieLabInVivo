function [output] = do_wavelet_OrigFear_TEM( r )
% DO_WAVELET_ORIGFEAR_TEM conducts the time-frequency analysis by trials
%
%   Default parameters documented below.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/25/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder ratT fsample stimuli phases

%% create waitbar

wb = waitbar(0,['Preparing for wavelet decomposition on rat #' ...
  num2str(ratT.ratnums(r))]);
tic

%% define analysis parameters

wav = get_cfg_TEM('wav', 'OrigFear');
% wav = 
%   struct with fields:
% 
%         method: 'wavelet'
%         output: 'fourier'
%     keeptrials: 'yes'
%            foi: [1�175 double]
%          width: 6
%         gwidth: 3.1416
%         wavlen: [1�175 double]
%            toi: [1�11763 double]
%            pad: 131.0720

%% Load inputfile

inputfile = [datafolder 'preproc' filesep num2str(ratT.ratnums(r)) filesep ...
  'AllClean_ICA_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];

data = rmvlargefields_TEM(inputfile);   % strip excess historical metadata

comptime = nan(numel(data.trial),1);
esttime = 7644;  % in s, based on 1st run:  sum(comptime)

if ~isfield(data,'fsample') || isempty(data.fsample)
  data.fsample = fsample;
end

%% define baseline outputfile

outputfile = [datafolder 'wavelet' filesep num2str(ratT.ratnums(r)) ...
  filesep 'Baseline_ICA_' num2str(ratT.ratnums(r)) 'waveletTFAfourier.mat'];

%% check if it exists

if ~existfile_TEM(outputfile)
  %% separate baseline
  
  tic
  waitbar(0, wb, ['Calculating baseline wavelet TFA for rat #' ...
    num2str(ratT.ratnums(r))]);
  
  cfg = [];
  cfg.trials  = data.trialinfo(:,2) == 0; 	% wherever there's no stimulus
  
  tmpdat = ft_selectdata(cfg, data);
  
  assert(~any(any(isnan([tmpdat.trial{:}]))))
  
  %% break baseline data into 20 sub-trials of length wav.pad
  
  cfg = [];
  cfg.length    = wav.pad;
  cfg.overlap   = 0;
  
  tmpdat = ft_redefinetrial(cfg, tmpdat);
  
  cfg = [];
  cfg.trials  = 1:20; 	% all rats have at least 20
  
  tmpdat = ft_selectdata(cfg, tmpdat);
  
  %% realign all trials to start at time 0 to avoid overburdening memory
  % otherwise, it pads each trial out to the length of time covered by all the
  % trials together
  
  cfg = [];
  cfg.offset  = [0; -cumsum(diff(tmpdat.sampleinfo(:,1)))];
  
  tmpdat = ft_redefinetrial(cfg, tmpdat);
  
  %% calculate baseline freqanalysis
  
  cfg = wav;   % default params defined in get_cfg_TEM, adjust for baseline
  cfg.toi = (tmpdat.time{1}(1) + wav.wavlen(1)): ...
    (wav.toi(2)-wav.toi(1)):(tmpdat.time{1}(end) -  wav.wavlen(1));
  cfg.keeptrials  = 'yes';
  cfg.outputfile  = outputfile;
  
  freq = ft_freqanalysis(cfg, tmpdat);
  
  %% clear memory
  
  clearvars freq
  comptime(end) = toc;
end

%% trim tone-trials to core wav.pad time

cfg = [];
cfg.trials  = data.trialinfo(:,2) == 1;   % wherever there's a CS+

tmpdat = ft_selectdata(cfg, data);

cfg = [];
cfg.toilim  = [median(wav.toi) - wav.pad/2 + 1/data.fsample, ...
  median(wav.toi) + wav.pad/2 - 1/data.fsample];   % cut 2 samples to avoid rounding errors

tmpdat = ft_redefinetrial(cfg, tmpdat);

%% loop through all tone trials

trlinfo = unique(tmpdat.trialinfo,'rows','stable');
for tr = 1:size(trlinfo,1)  % trial # across all phases
  %% define trial outputfile
  
  tn = trlinfo(tr,1);   % trial number within phase
  s = trlinfo(tr,2);    % stimulus type (1 for CS+ or 0 for none)
  p = trlinfo(tr,3);    % phase number
  
  outputfile = [datafolder 'wavelet' filesep num2str(ratT.ratnums(r)) ...
    filesep stimuli{s} num2str(tn) 'of' phases{p} '_ICA_' ...
    num2str(ratT.ratnums(r)) 'waveletTFAfourier.mat'];
  
  %% check if it exists
  
  if ~existfile_TEM(outputfile)
    %% calculate trial spectrogram
    
  tic
  waitbar(sum(comptime,'omitnan')/esttime, ...
    wb, ['Calculating wavelet TFA for tone trial #' num2str(tr) ', rat #' ...
      num2str(ratT.ratnums(r))]);
    
    cfg             = wav;
    cfg.trials      = ismember(tmpdat.trialinfo, trlinfo(tr,:), 'rows');
    cfg.outputfile  = outputfile;
    
    freq = ft_freqanalysis(cfg,tmpdat);
    
    %% clear memory
    
    clearvars freq
    comptime(tr) = toc;
    esttime = (numel(comptime)+19)*sum(comptime,'omitnan')/(20+tr);

  end
end

output = comptime;
close(wb)