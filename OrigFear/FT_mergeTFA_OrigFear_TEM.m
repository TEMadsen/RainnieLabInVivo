%% Merges spectrograms across channels and trials (excluding baseline)

CleanSlate  % provides a clean slate to start working with new data/scripts
% by clearing all variables, figures, and the command window

info_origfear            % load all background info & final parameters

errlog = cell(size(ratT,1), 1);   % MEs saved w/in nested cells:
% rat #, then tone x phase, then ch #

%% which spectrograms to merge

zthresh   = 15;     % which artifact threshold to use the data from
cthresh   = 3;      % and which clipping threshold
spectype  = 'wav';  % wavelet TFA
normtype  = 'z_dB'; % z-score based on mu & sigma of dB-transformed baseline spectrograms
redoch    = true;  % to ignore & overwrite pre-existing channel-merge outputs
redotr    = true;   % to ignore & overwrite pre-existing trial-merge outputs

%% full loop

for r = find(~ratT.excluded)'
  %% preallocate errlog
  
  errlog{r} = cell(max(expected), numel(phases));
  
  %% define outputfile
  
  ratfolder = [datafolder num2str(ratT.ratnums(r)) filesep];
  outputfile = [ratfolder normtype 'Norm_' spectype 'TFA_' ...
    num2str(ratT.ratnums(r)) 'merged.mat'];
  
  %% check if it exists
  
  if exist(outputfile,'file') && ~redotr
    errlog{r}{1,4}.warning = ['Skipping rat #' num2str(ratT.ratnums(r)) ...
      ' because outputfile ' outputfile(numel(ratfolder)+1:end) ...
      ' already exists.'];  % tone 1 of phase 4 here just means no specific trial or channel
    warning(errlog{r}{1,4}.warning)
    continue  % to next rat
  end
  
  %% initialize list of trial filenames
  
  trfiles2merge = {};
  nexttr = 1; 	% next cell to fill
  
  %% loop through all phases of fear conditioning & extinction
  
  for p = 1:3   % 1:numel(phases) w/o baseline
    %% loop through tones
    
    for tn = 1:expected(p)
      %% define trial outputfile
      
      troutputfile = [ratfolder normtype 'Norm_' spectype 'TFA_' ...
        stimuli{1} num2str(tn) 'of' phases{p} '_' ...
        num2str(ratT.ratnums(r)) 'allchs.mat'];
      
      %% check if it exists
      
      if exist(troutputfile,'file') && ~redoch
        trfiles2merge{nexttr} = troutputfile;   %#ok<SAGROW> can't leave empty cells
        nexttr = nexttr + 1;  % still needs to be merged with other trials
        errlog{r}{tn,p}.warning = ['Skipping ' stimuli{1} ' ' ...
          num2str(tn) ' of ' phases{p} ' for rat #' ...
          num2str(ratT.ratnums(r)) ' because outputfile ' ...
          troutputfile(numel(ratfolder)+1:end) ' already exists.'];
        warning(errlog{r}{tn,p}.warning)
        continue  % to next tone
      end
      
      %% initialize list of channel filenames
      
      chfiles2merge = {};
      nextch = 1; 	% next cell to fill
      
      errlog{r}{tn,p} = cell(numel(allchs),1);  % preallocate errlog
      
      %% loop through channels
      % this needs to be innermost so bad channels will be skipped, rather than
      % any trials containing a bad channel
      
      for ch = 1:numel(allchs)
        %% name input file
        
        inputfile = [ratfolder normtype 'Norm' ...
          stimuli{1} num2str(tn) 'of' phases{p} '_' ...
          num2str(ratT.ratnums(r)) allchs{ch} '.mat'];
        
        %% check if it exists
        
        if ~exist(inputfile,'file')
          errlog{r}{tn,p}{ch}.warning = ['Skipping ' stimuli{1} ' ' ...
            num2str(tn) ' of ' phases{p} ' for rat #' ...
            num2str(ratT.ratnums(r)) ', ' allchs{ch} ...
            ' because inputfile ' inputfile(numel(ratfolder)+1:end) ...
            ' not found.'];
          warning(errlog{r}{tn,p}{ch}.warning)
          continue % to next tone
        end
        
        %% add filename to list
        
        chfiles2merge{nextch} = inputfile;  %#ok<SAGROW> can't leave empty cells
        nextch = nextch + 1;
        
      end   % channels
      
      %% check whether any files were found
      
      if all(cellfun(@isempty, chfiles2merge))
        errlog{r}{tn,p}{numel(allchs)+1}.warning = ['Skipping ' ...
          stimuli{1} ' ' num2str(tn) ' of ' phases{p} ' for rat #' ...
          num2str(ratT.ratnums(r)) ' because no matching spectrograms were found.'];
        warning(errlog{r}{tn,p}{numel(allchs)+1}.warning)
        continue % to next channel
      end
      
      %% (try to) merge all channels for this trial
      
      cfg = [];
      cfg.parameter   = 'powspctrm';
      cfg.appenddim   = 'chan';
      cfg.tolerance   = tol;   
      cfg.inputfile   = chfiles2merge;
      cfg.outputfile  = troutputfile;
      
      try
        freq = ft_appendfreq(cfg); % with breakpoint at line 445 of ft_appendfreq,
        % if boolval == 0 when it stops, run the commented lines under
        % catch, otherwise just continue
        
        trfiles2merge{nexttr} = troutputfile;   %#ok<SAGROW> can't leave empty cells
        nexttr = nexttr + 1;
      catch ME
        errlog{r}{tn,p}{numel(allchs)+1}.error = ME;
        warning(errlog{r}{tn,p}{numel(allchs)+1}.error.message)
        
        keyboard  % put breakpoint at line 445 of ft_appendfreq & re-run:
        % ft_appendfreq(cfg)  % and then when it stops at the breakpoint:
        % [~,col] = find(abs(faxis - repmat(median(faxis,2), 1, Ndata))>tol);
        % disp(['bad channels: [' num2str(unique(col)') ']']) % copy & paste this output to respond to input question
        % dbcont  % until back in this function & once more
        
        badch = input('Which channels (by cell #) should be excluded?');
        chfiles2merge(badch) = [];  %#ok<SAGROW> can't leave empty cells
        cfg.inputfile = chfiles2merge;
        
        try
          freq = ft_appendfreq(cfg);
          
          trfiles2merge{nexttr} = troutputfile;   %#ok<SAGROW> can't leave empty cells
          nexttr = nexttr + 1;
        catch ME  % this time we'll leave it alone
          errlog{r}{tn,p}{numel(allchs)+2}.error = ME;
          warning(errlog{r}{tn,p}{numel(allchs)+2}.error.message)
        end
      end % try
      
%       %% check for errors
%       
%       assert(numel(wav.foi) == numel(freq.freq))
%       assert(numel(wav.toi) == numel(freq.time))
%       assert(numel(chfiles2merge) == numel(freq.label))
%       
    end % tone
  end % phase
  
  %% check whether any files were found
  
  if all(cellfun(@isempty, trfiles2merge))
    errlog{r}{1,4}.warning = ['Skipping rat #' ...
      num2str(ratT.ratnums(r)) ' because no matching spectrograms were found.'];
    warning(errlog{r}{1,4}.warning)  % tone 1 of phase 4 just used to indicate no specific trial
    continue % to next rat
  end
  
  %% (try to) merge all trials for this rat
  
  cfg = [];
  cfg.parameter   = 'powspctrm';
  cfg.appenddim   = 'rpt';    % trials (repeats?)
  cfg.tolerance   = tol;
  cfg.inputfile   = trfiles2merge;
  cfg.outputfile  = outputfile;
  
  try
    freq = ft_appendfreq(cfg);
  catch ME
    errlog{r}{1,4}.error = ME;
    warning(errlog{r}{1,4}.error.message)
    
    keyboard  % re-run ft_appendfreq(cfg) to debug and ID bad chs
  end % try
  
%       %% check for errors - will never work unless I redo TFA w/ uniform
%       padding
%       
%       assert(numel(wav.foi) == numel(freq.freq))
%       assert(numel(wav.toi) == numel(freq.time))
%       assert(numel(trfiles2merge) == size(freq.powspctrm,1))
%       disp([num2str(numel(freq.label)) ' channels included in merge.'])
%       
end % rat

%% save errlog

disp('Analysis complete!  Saving errlog.')
save([datafolder 'errlog' datestr(now,29) ...
  '_FT_mergeTFA_OrigFear_TEM'], 'errlog')
