function do_power_OrigFear_TEM( r )
% DO_POWER_ORIGFEAR_TEM analyzes spectral power changes within & across trials
%
%   Currently skipping everything done by NormPlotTFA, since it's not able to be
%   done within FieldTrip.  Instead, this function is for within-subject stats
%   on the output of mtmfft.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/9/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder configfolder ratT fsample stimuli phases intervals

%% create waitbar

wb = waitbar(0,['Preparing for power analysis on rat #' num2str(ratT.ratnums(r))]);

%% Load inputfile for each interval

BDAfreq = cell(numel(intervals),1);
% trlinfo = unique(tmpdat.trialinfo,'rows','stable');
% for tr = 1:size(trlinfo,1)  % trial # across all phases
for ti = 1:numel(intervals)   % B/D/A
  %% load inputfile
  
  inputfile = [datafolder 'mtmfft' filesep num2str(ratT.ratnums(r)) filesep ...
    intervals{ti} 'Tones_shockt_ICA_' num2str(ratT.ratnums(r)) 'mtmfft.mat'];
  
  BDAfreq{ti} = rmvlargefields_TEM(inputfile);
end

%% normalize during by before (dB transform of power ratio)

if ~isequal(BDAfreq{2}.trialinfo(:,1:3),BDAfreq{1}.trialinfo(:,1:3))
  error('match trials first')
end

cfg = [];
cfg.parameter = 'powspctrm';
cfg.operation = '10*log10(x1./x2)';

normDfreq = ft_math(cfg,BDAfreq{2},BDAfreq{1});
normDfreq.trialinfo = BDAfreq{1}.trialinfo(:,1:3);

%% downsample the spectra

DSnormDfreq = normDfreq;
DSnormDfreq.freq = unique(round(normDfreq.freq/0.5)*0.5);
DSnormDfreq.powspctrm = nan(size(normDfreq.powspctrm,1), ...
    size(normDfreq.powspctrm,2), numel(DSnormDfreq.freq));
for f = 1:numel(DSnormDfreq.freq)
  fndx = abs(normDfreq.freq-DSnormDfreq.freq(f)) < 0.25;
  DSnormDfreq.powspctrm(:,:,f) = mean(normDfreq.powspctrm(:,:,fndx),3,'omitnan');
end

%% run within-subject stats on power spectra

%% start with simple ANOVA during 5 tones (norm by before) per each of 4 blocks

%% select data

blktrlinfo = cat(3, [(6:10)' ones(5,1) ones(5,1)], ...
  [(13:17)' ones(5,1) ones(5,1)], [(1:5)' ones(5,1) 2*ones(5,1)], ...
  [(11:15)' ones(5,1) 3*ones(5,1)]);
blkDfreq = cell(size(blktrlinfo,3),1);

for b = 1:size(blktrlinfo,3)
  cfg = [];
  cfg.trials = ismember(DSnormDfreq.trialinfo(:,1:3),blktrlinfo(:,:,b),'rows');
  
  blkDfreq{b} = ft_selectdata(cfg,DSnormDfreq);
  
  cfg = [];
  cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  figure;
  ft_multiplotER(cfg,blkDfreq{b})
end

%% prepare_neighbours determines what sensors may form clusters

cfg = [];
cfg.method         = 'distance';
cfg.neighbourdist  = 0.5;  % mm
cfg.elec           = rmvlargefields_TEM([configfolder 'elec_' ...
  ratT.MWA{r} '_' ratT.side{r} '.mat']);
neighbours        = ft_prepare_neighbours(cfg);
if any(cellfun(@numel,{neighbours(:).neighblabel}) < 3) || ...
    any(cellfun(@numel,{neighbours(:).neighblabel}) > 5)
  warning('unexpected # of neighbors')
  cfg = [];
  cfg.neighbours = neighbours;
  cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
  ft_neighbourplot(cfg)
  keyboard
end

%% run stat test

cfg = [];
cfg.method            = 'montecarlo';
cfg.statistic         = 'ft_statfun_indepsamplesF';
cfg.correctm          = 'cluster';
cfg.clusteralpha      = 0.1;
cfg.clusterstatistic  = 'maxsum';
cfg.minnbchan         = 2;
cfg.tail              = 1;  % required for F-stats
cfg.clustertail       = 1;  % required for F-stats
cfg.alpha             = 0.1;
cfg.numrandomization  = 500;
cfg.neighbours        = neighbours;

cfg.design  = [ones(1,5) 2*ones(1,5) 3*ones(1,5) 4*ones(1,5)];

cfg.ivar    = 1;  % uvar is trials, not matched across blocks (between-UO)

[stat] = ft_freqstatistics(cfg, blkDfreq{:});

%% plot significant differences in power spectra

cfg=[];
cfg.channel         = ratT.mPFC{r};
cfg.parameter       = 'stat';
cfg.maskparameter   = 'mask';
cfg.maskstyle       = 'box';
% cfg.zlim            = [-5 5];
% cfg.xlim            = [0 .7];
figure; subplot(2,2,1:2);
ft_singleplotER(cfg,stat);
cfg =[];
cfg.layout = [configfolder 'layout_' ratT.MWA{r} '_' ratT.side{r} '.mat'];
cfg.parameter = 'stat';
cfg.xlim = [2.6 4.6];
% cfg.ylim = [5 8];
% cfg.zlim = [-5 5];
subplot(2,2,3); ft_topoplotER(cfg,stat);
title('Delta')
cfg.xlim = [53 55.2];
subplot(2,2,4); ft_topoplotTFR(cfg,stat);
title('Mid-Gamma')

%% 
error('finish coding!')

