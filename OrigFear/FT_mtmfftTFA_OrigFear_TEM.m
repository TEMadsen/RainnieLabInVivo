%% Calculate raw spectrograms & coherograms using mtmfft_movwin_TEM.m
% took approx 29 hours for 4 or 8s windows (3/24 & 3/10/17, respectively),
% produced warnings about trialinfo dimord and java errors, but nothing blocking
% progress.  <24 hours for 2s windows (3/27/17) w/ no java errors.

CleanSlate  % provides a clean slate to start working with new data/scripts

h = waitbar(0,'Initializing...');   % will display progress through loop

info_origfear %#ok<*SAGROW> multiple metadata variables preallocated here
gen = get_cfg_TEM('gen', 'OrigFear');  % collect general parameters of interest
mtmf = get_cfg_TEM('mtmfft_movwin', 'OrigFear');  % and parameters for analysis

zthresh   = 15;     % use data cleaned with this artifact threshold
cthresh   = 3;      % and this clipping threshold
redo      = false;  % to ignore & overwrite old files
skiprats  = [];     % already processed or running on another computer

errlog = cell(size(ratT,1), sum(expected));   % preallocate for error messages
rattime = NaN(size(ratT,1), 1);   % preallocate for computation time per rat

%% full loop

for r = setdiff(find(~ratT.excluded)',skiprats)
  %   try
  tic   % will time computations per rat
  
  %% define inputfile
  
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllClean_z' num2str(zthresh) '_clip' num2str(cthresh) ...
    '_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];
  
  %% check for input data
  
  if ~exist(inputfile,'file')
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ...
      ' because ' inputfile ' was not found.'])
    continue
  end
  
  %% load input data (all chans, tone trials, artifacts replaced w/ nans)
  
  waitbar((r-1)/size(ratT,1), h, ['Loading allch data for rat #' ...
    num2str(ratT.ratnums(r)) newline 'mean(rattime) = ' ...
    num2str(round(mean(rattime,'omitnan')/60)) ' minutes']);
  
  load(inputfile);
  
  %% define baseline outputfiles
  
  outputpow = [datafolder num2str(ratT.ratnums(r)) filesep 'Baseline' ...
    num2str(ratT.ratnums(r)) 'mtmfftTFApow' num2str(mtmf.movwin(1)) 's.mat'];
  outputcoh = [datafolder num2str(ratT.ratnums(r)) filesep 'Baseline' ...
    num2str(ratT.ratnums(r)) 'mtmfftTFAcoh' num2str(mtmf.movwin(1)) 's.mat'];
  
  %% check if it exists
  
  if exist(outputpow,'file') && exist(outputcoh,'file') && ~redo
    errlog{r,sum(expected)} = ...
      ['mtmfftTFA already calculated and converted for Rat #' ...
      num2str(ratT.ratnums(r)) '''s Baseline. Skipping.'];
    warning(errlog{r,sum(expected)})
    goodchs{r} = allchs;
  else
    waitbar((r-0.9)/size(ratT,1), h, ...
      ['Calculating baseline freq for rat #' ...
      num2str(ratT.ratnums(r)) newline 'mean(rattime) = ' ...
      num2str(round(mean(rattime,'omitnan')/60)) ' minutes']);
    
    %% restructure into moving windows as trials
    % doing this outside of function only for baseline to enable next
    % step of selecting good channels & trials
    
    cfg = [];
    cfg.trials  = data.trialinfo(:,2) == 0;  % wherever there's no tone
    cfg.length  = mtmf.movwin(1);
    cfg.overlap = (mtmf.movwin(1)-mtmf.movwin(2))/mtmf.movwin(1);
    
    tmpdat = ft_redefinetrial(cfg, data);
    
    %% for baseline only, reject trials with any artifacts, keep 1st 1000
    
    goodchtrls = cell2mat(cellfun(@(x) all(~isnan(x),2), ...
      tmpdat.trial, 'UniformOutput', false));
    ngoodchspertrl = sort(sum(goodchtrls,1),'descend');
    ngoodchs = ngoodchspertrl(1000);  % >= 1000 trials have >= n good chs
    
    if ngoodchs < 2   % coherence calculation won't work
      error('fewer than 2 good channels found in baseline data')
    end
    
    [~, chnum] = sort(sum(goodchtrls,2),'descend');  % sort best to worst
    bestchs = sort(chnum(1:ngoodchs));  % keep best, re-sort numerically
    goodchs{r} = tmpdat.label(bestchs);   % convert #s back to strings
    goodtrls = find(all(goodchtrls(bestchs,:),1),1000,'first');
    
    if numel(goodtrls) < 1000
      error('problem finding 1000 artifact-free windows in baseline data')
    end
    
    cfg = [];
    
    cfg.channel = goodchs{r};
    cfg.trials = goodtrls;
    
    tmpdat = ft_selectdata(cfg, tmpdat);
    
    %% calculate baseline separately
    
    cfg = mtmf;
    
    freq = mtmfft_movwin_TEM(cfg,tmpdat);
    
    %% convert crsspctrm to cohspctrm & save
    
    cfg = [];
    cfg.method      = 'coh';
    cfg.complex     = 'complex';
    cfg.channel     = goodchs{r};
    cfg.outputfile  = outputcoh;
    
    stat = ft_connectivityanalysis(cfg,freq);
    
    %% remove crsspctrm & save powspctrm
    
    freq = rmfield(freq,{'crsspctrm','labelcmb'});
    
    waitbar((r-0.6)/size(ratT,1), h, ['Saving baseline freq for rat #' ...
      num2str(ratT.ratnums(r)) newline 'mean(rattime) = ' ...
      num2str(round(mean(rattime,'omitnan')/60)) ' minutes']);
    
    save(outputpow, 'freq', '-v7.3');
  end
  
  waitbar((r-0.5)/size(ratT,1), h, ['Preparing tone trials for rat #' ...
    num2str(ratT.ratnums(r)) newline 'mean(rattime) = ' ...
    num2str(round(mean(rattime,'omitnan')/60)) ' minutes']);
  
  %% trim tone-trials to core 90s + 1 window length
  
  cfg = [];
  cfg.trials = data.trialinfo(:,2) == 1;  % wherever there's a CS+
  cfg.toilim = [gen.toilim(1) - mtmf.movwin(1)/2, ...   % so 1st & last windows are
    gen.toilim(end) + mtmf.movwin(1)/2];  % centered on (not limited by) 1st & last toi
  
  data = ft_redefinetrial(cfg, data);
  
  %% process 1 trial at a time to minimize memory use
  
  for tr = 1:numel(data.trial)
    %% define trial outputfile
    
    tn = data.trialinfo(tr,1);   % trial number within phase
    s = data.trialinfo(tr,2);    % stimulus type (1 for CS+ or 0 for none)
    p = data.trialinfo(tr,3);    % phase number
    
    outputpow = [datafolder num2str(ratT.ratnums(r)) filesep ...
      stimuli{s} num2str(tn) 'of' phases{p} '_' ...
      num2str(ratT.ratnums(r)) 'mtmfftTFApow' num2str(mtmf.movwin(1)) 's.mat'];
    outputcoh = [datafolder num2str(ratT.ratnums(r)) filesep ...
      stimuli{s} num2str(tn) 'of' phases{p} '_' ...
      num2str(ratT.ratnums(r)) 'mtmfftTFAcoh' num2str(mtmf.movwin(1)) 's.mat'];
    
    %% check for outputfiles
    
    if exist(outputpow,'file') && exist(outputcoh,'file') && ~redo
      errlog{r,tr} = ['mtmfftTFA already calculated and converted for ' ...
        stimuli{s} ' #' num2str(tn) ' of ' phases{p} ', Rat #' ...
        num2str(ratT.ratnums(r)) '. Skipping.'];
      warning(errlog{r,tr})
      continue  % to next trial
    end
    
    waitbar((r-0.4+0.4*((tr-1)/numel(data.trial)))/size(ratT,1), h, ...
      ['Calculating freq for tone trial #' num2str(tr) ', rat #' ...
      num2str(ratT.ratnums(r)) newline 'mean(rattime) = ' ...
      num2str(round(mean(rattime,'omitnan')/60)) ' minutes']);
    
    %% calculate spectrograms & coherograms
    
    cfg = mtmf;
    cfg.trials    = tr;
    cfg.channel   = goodchs{r};
    
    freq = mtmfft_movwin_TEM(cfg,data);
    
    %% reduce to what is best across all trials
    
    goodchs{r} = intersect(goodchs{r}, freq.label);
    
    %% convert crsspctrm to cohspctrm & save
    
    cfg = [];
    cfg.method      = 'coh';
    cfg.complex     = 'complex';
    cfg.channel     = goodchs{r};
    cfg.outputfile  = outputcoh;
    
    stat = ft_connectivityanalysis(cfg,freq);
    
    %% remove crsspctrm & save powspctrm
    
    freq = rmfield(freq,{'crsspctrm','labelcmb'});
    
    waitbar((r-0.4+0.4*((tr-0.5)/numel(data.trial)))/size(ratT,1), h, ...
      ['Saving freq for tone trial #' num2str(tr) ', rat #' ...
      num2str(ratT.ratnums(r)) newline 'mean(rattime) = ' ...
      num2str(round(mean(rattime,'omitnan')/60)) ' minutes']);
    
    save(outputpow, 'freq', '-v7.3');
    
  end
  rattime(r) = toc;
  
  %       catch ME
  %         warning(ME.message);
  %         errlog{r,tr} = ME;
  %       end
end

waitbar(1, h, ['Done!  Total time: ' ...
  num2str(sum(rattime,'omitnan')/60) ' minutes']);

%% save goodchs & errlog

save([datafolder 'goodchs' datestr(now,29) ...
  '_FT_mtmfftTFA_OrigFear_TEM'],'goodchs')

save([datafolder 'errlog' datestr(now,29) ...
  '_FT_mtmfftTFA_OrigFear_TEM'],'errlog')
