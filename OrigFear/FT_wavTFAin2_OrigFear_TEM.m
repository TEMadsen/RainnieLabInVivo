%% Calculate raw wavelet spectrograms & coherograms in 2 steps (baseline & all tones)

CleanSlate  % provides a clean slate to start working with new data/scripts

info_origfear   % load all background info & final parameters

zthresh = 15;   % analyze data cleaned using this artifact threshold
cthresh = 3;    % and this clipping threshold
redo = true;    % to ignore & overwrite old files

errlog = cell(size(ratT,1),numel(allchs));   % MEs saved w/in tr# cells

%% full loop

for r = find(~ratT.excluded)'   % may be better to run a smaller batch of rats on multiple computers
  %% define inputfile
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllClean_z' num2str(zthresh) '_clip' num2str(cthresh) ...
    '_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];
  
  %% check for input data
  
  if ~exist(inputfile,'file')
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ...
      ' because ' inputfile ' was not found.'])
    continue
  end
  
  %% load input data (all channels, tone trials, artifacts replaced with nans)
  
  disp(['Loading allch data for rat #' num2str(ratT.ratnums(r))])
  S = load(inputfile,'data');
  data = S.data;
  S = [];   % use empty set instead of clearvars inside parfor loop
  
  %% set common parameters
  
  cfg             = wav;          % see info_origfear
  cfg.output      = 'powandcsd';  % replaces default in info script
  cfg.keeptrials  = 'yes';        % now refers to tone trials
  
  %% calculate baseline separately
  
  cfg.toi         = wav.BLtoi;
  cfg.trials      = data.trialinfo(:,2) == 0;   % wherever there's no stimulus type
  cfg.outputfile  = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'Baseline_z' num2str(zthresh) '_clip' num2str(cthresh) '_' ...
    num2str(ratT.ratnums(r)) 'allch_wavTFA.mat'];
  cfg.polyremoval = -1;   % this didn't fix it
  % not sure why, but compounded with memory errors, it's not worth fixing
  
  freq = ft_freqanalysis(cfg,data); %#ok<NASGU> saved to file
  %%% currently all NaNs!!! %%%
  % turns all NaN in ft_preproc_polyremoval @ lines 88-91
  % called by ft_specest_wavelet @ line 75
  % called by ft_freqanalysis @ line 520
  % any NaNs in data make beta all NaNs during poly fit step
  
  %%% SPLIT BACK INTO CHANNELS %%%
  % for coherence, remove NaNs by channel pairs
  
  if all(isnan(freq.powspctrm(:)))
    error('freq.powspctrm is all NaNs')
  end
  
  for ch = 1:numel(freq.label)
    fig = figure(ch); clf; fig.WindowStyle = 'docked';
    imagesc(freq.time, 1:numel(freq.freq), squeeze(freq.powspctrm(:,ch,:,:)))
    title('raw baseline freq input')
  end
  
  %% calculate all tone trials together
  
  cfg.toi         = wav.toi;
  cfg.trials      = data.trialinfo(:,2) == 1;   % wherever there's a CS+
  cfg.outputfile  = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllTones_z' num2str(zthresh) '_clip' num2str(cthresh) '_' ...
    num2str(ratT.ratnums(r)) 'allch_wavTFA.mat'];
  
  freq = ft_freqanalysis(cfg,data);
  %       catch ME
  %         warning(ME.message);
  %         errlog{r,ch}{tr} = ME;
  %       end
end