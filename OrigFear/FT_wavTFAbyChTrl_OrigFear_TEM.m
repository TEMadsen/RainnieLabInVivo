%% Calculate raw wavelet spectrograms for each channel & trial

CleanSlate  % provides a clean slate to start working with new data/scripts

info_origfear   % load all background info & final parameters

zthresh = 15;   % analyze data cleaned using this artifact threshold
cthresh = 3;    % and this clipping threshold
redo = false;   % to ignore & overwrite old files

errlog = cell(size(ratT,1), numel(allchs));   % MEs saved w/in tr# cells

%% full loop

for r = find(~ratT.excluded)'   % may be better to run a smaller batch of rats on multiple computers
  %% loop through all channels
  for ch = 1:numel(allchs) % defined in info script
    %% define inputfile
    inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
      'Clean_z' num2str(zthresh) '_clip' num2str(cthresh) '_' ...
      allchs{ch} 'SubTrialData_' num2str(ratT.ratnums(r)) '.mat'];
    
  %% check for input data
  
  if ~exist(inputfile,'file')
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ', ' allchs{ch} ...
      ' because ' inputfile ' was not found.'])
    continue
  end
  
    %% load input data
    
    disp(['Loading sub-trial data for rat #' num2str(ratT.ratnums(r))])
      S = load(inputfile,'data');
      data1ch = S.data;
      S = [];   % use empty set instead of clearvars inside parfor loop
    
    %% loop through all tone trials
    
    trlinfo = unique(data1ch.trialinfo,'rows','stable');
    for tr = 1:size(trlinfo,1)  % trial # across all phases
%       try
      %% define analysis parameters & check for outputfile
      
      tn = trlinfo(tr,1);   % trial number within phase
      s = trlinfo(tr,2);    % stimulus type (1 for CS+ or 0 for none)
      p = trlinfo(tr,3);    % phase number
      
      cfg         = wav;  % see info_origfear
      cfg.trials  = ismember(data1ch.trialinfo, trlinfo(tr,:), 'rows');
      
      if s == 0  % no stimulus, i.e., baseline VI-60
        cfg.outputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
          phases{p} '_z' num2str(zthresh) '_clip' num2str(cthresh) '_' ...
          num2str(ratT.ratnums(r)) allchs{ch} 'wavTFA.mat'];
        cfg.toi   = wav.BLtoi;
      else
        cfg.outputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
          stimuli{s} num2str(tn) 'of' phases{p} '_z' num2str(zthresh) ...
          '_clip' num2str(cthresh) '_' num2str(ratT.ratnums(r)) ...
          allchs{ch} 'wavTFA.mat'];
        cfg.toi   = wav.toi;
      end
      
      %% stop here to avoid continue
      
      if exist(cfg.outputfile,'file') && ~redo
        errlog{r,ch}{tr} = ['wavTFA already calculated for ' stimuli{s} ...
          ' #' num2str(tn) ' of ' phases{p} ', Rat #' ...
          num2str(ratT.ratnums(r)) ', ' allchs{ch} '. Skipping.'];
        warning(errlog{r,ch}{tr})
        continue  % to next trial
      end
      
      %% calculate spectrogram
      
      freq = ft_freqanalysis(cfg,data1ch);
      
      %% check for errors
      
      assert(numel(cfg.foi) == numel(freq.freq))
      assert(numel(cfg.toi) == numel(freq.time))
      assert(all(ismembertol(freq.freq, cfg.foi, tol, 'DataScale', 1)))
      assert(all(ismembertol(freq.time, cfg.toi, tol, 'DataScale', 1)))
      
%       catch ME
%         warning(ME.message);
%         errlog{r,ch}{tr} = ME;
%       end
    end
  end
end
