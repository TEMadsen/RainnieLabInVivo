%% Calculate raw spectrograms & coherograms using mtmconvol

CleanSlate  % provides a clean slate to start working with new data/scripts

wb = waitbar(0,'Initializing...');   % will display progress through loop

info_origfear %#ok<*SAGROW> multiple metadata variables preallocated here
gen = get_cfg_TEM('gen', 'OrigFear');  % collect general parameters of interest
mtmc = get_cfg_TEM('mtmconvol', 'OrigFear');  % and parameters for analysis

artstr    = 'raw';  % string describing artifact rejection method used
nBLt      = 20;     % # of pad-length "trials" from baseline to analyze (max that works for all rats)
redo      = false;  % to ignore & overwrite old files
skiprats  = [];     % already processed or running on another computer

errlog = cell(size(ratT,1), sum(expected));   % preallocate for error messages
rattime = NaN(size(ratT,1), 1);   % preallocate for computation time per rat

%% full loop

for r = setdiff(find(~ratT.excluded)',skiprats)
  %   try
  tic   % will time computations per rat
  %% define inputfile
  
  if strcmp(artstr,'raw')
    inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
      'RawLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  else
    inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
      'AllClean_' artstr '_LFPTrialData_' num2str(ratT.ratnums(r)) '.mat'];
  end
  
  %% check for input data
  
  if ~existfile_TEM(inputfile)
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ...
      ' because ' inputfile ' was not found.'])
    continue  % to next rat
  end
  
  %% load input data (all chans, tone trials, artifacts replaced w/ nans)
  
  waitbar((r-1)/size(ratT,1), wb, ...
    ['Loading allch data for rat #' num2str(ratT.ratnums(r)) ...
    newline 'mean(rattime) = ' num2str(mean(rattime,'omitnan')/60) ' minutes']);
  data = rmvlargefields_TEM(inputfile);
  
  if ~isfield(data,'fsample') || isempty(data.fsample)
    data.fsample = fsample;
  end
  goodchs{r} = data.label;  % use all channels, overwriting what's in info_origfear
  
  %% define baseline outputfiles
  
  outputpow = [datafolder num2str(ratT.ratnums(r)) filesep 'Baseline_' ...
    artstr '_' num2str(ratT.ratnums(r)) 'mtmconvolTFApow.mat'];
  outputcoh = [datafolder num2str(ratT.ratnums(r)) filesep 'Baseline_' ...
    artstr '_' num2str(ratT.ratnums(r)) 'mtmconvolTFAcoh.mat'];
  
  %% check if it exists
  
  if exist(outputpow,'file') && exist(outputcoh,'file') && ~redo
    errlog{r,sum(expected)} = ...
      ['mtmconvolTFA already calculated and converted for Rat #' ...
      num2str(ratT.ratnums(r)) '''s Baseline. Skipping.'];
    warning(errlog{r,sum(expected)})
  else  % not continue, as that would skip all tone trials for this rat
    waitbar((r-0.9)/size(ratT,1), wb, ...
      ['Preparing baseline data for rat #' num2str(ratT.ratnums(r)) ...
      newline 'mean(rattime) = ' num2str(mean(rattime,'omitnan')/60) ' minutes']);
    
    %% separate baseline
    
    cfg = [];
    cfg.trials  = data.trialinfo(:,2) == 0; 	% wherever there's no stimulus
    cfg.channel = goodchs{r};
    
    tmpdat = ft_selectdata(cfg, data);
    
    %% mark & remove artifacts from baseline data
    % invalidate times with NaNs on any channel, unless distribution is highly
    % skewed across channels, in which case channels can be removed
    
    [artifact,count] = artifact_nan_TEM(tmpdat);
    
    cfg = [];
    cfg.artfctdef.nan.artifact      = artifact;
    cfg.artfctdef.reject            = 'partial';
    cfg.artfctdef.minaccepttim      = mtmc.pad;   % min(mtmc.t_ftimwin);
    
    try
      tmpdat = ft_rejectartifact(cfg,tmpdat);
    catch ME
      if contains(ME.message,'No trials left')
        warning('No pad-length trials are available with no NaNs on the current channels')
        % go back to raw data for baseline trial
        cfg = []; cfg.trials = data.trialinfo(:,2) == 0; cfg.channel = goodchs{r};
        tmpdat = ft_selectdata(cfg, data);
        % locate non-uniform NaNs
        [uI, prop] = findnans_TEM(tmpdat.trial,true);
        [artifact,tmpdat,times] = artifact_nan2zero_TEM(tmpdat);
        % remove only uniform NaNs
        cfg = []; cfg.artfctdef.allnan.artifact = artifact;
        cfg.artfctdef.reject = 'partial'; cfg.artfctdef.minaccepttim = min(mtmc.t_ftimwin);
        tmpdat = ft_rejectartifact(cfg,tmpdat);
        % see how many pad-length trials we can get this way
        cfg = []; cfg.length = mtmc.pad; cfg.overlap   = 0;
        tmpdat = ft_redefinetrial(cfg, tmpdat);
        figure; bar(categorical(tmpdat.label),cellfun(@numel,times));
        title([num2str(numel(tmpdat.trial)) ' pad-length trials when only uniform NaNs are removed'])
        ylabel('# of non-uniform NaNs per ch')
        keyboard;   % reconsider channel selection, approach, etc.
      else
        rethrow(ME)
      end
    end
    
    assert(~any(any(isnan([tmpdat.trial{:}]))))
    
    %% break artifact-free baseline data into sub-trials no longer than mtmc.pad
    
    cfg = [];
    cfg.length    = mtmc.pad;
    cfg.overlap   = 0;
    
    tmpdat = ft_redefinetrial(cfg, tmpdat);
    
    if isinf(nBLt)
      nBLt = min([nBLt numel(tmpdat.trial)])
    end
    
    if numel(tmpdat.trial) < nBLt   % if fewer than this, try another approach
      warning(['only ' num2str(numel(tmpdat.trial)) ' pad-length trials are available with no NaNs on the current channels'])
      % go back to raw data for baseline trial
      cfg = []; cfg.trials = data.trialinfo(:,2) == 0; cfg.channel = goodchs{r};
      tmpdat = ft_selectdata(cfg, data);
      % locate non-uniform NaNs
      [uI, prop] = findnans_TEM(tmpdat.trial,true);
      % remove only uniform NaNs
      [artifact,tmpdat,times] = artifact_nan2zero_TEM(tmpdat);
      cfg = []; cfg.artfctdef.allnan.artifact = artifact;
      cfg.artfctdef.reject = 'partial'; cfg.artfctdef.minaccepttim = min(mtmc.t_ftimwin);
      tmpdat = ft_rejectartifact(cfg,tmpdat);
      % see how many pad-length trials we can get this way
      cfg = []; cfg.length = mtmc.pad; cfg.overlap   = 0;
      tmpdat = ft_redefinetrial(cfg, tmpdat);
      figure; bar(categorical(tmpdat.label),cellfun(@numel,times));
      title([num2str(numel(tmpdat.trial)) ' pad-length trials when only uniform NaNs are removed'])
      ylabel('# of non-uniform NaNs per ch')
      keyboard;   % reconsider channel selection, approach, etc.
    else
      begtim  = cellfun(@min,tmpdat.time);
      endtim  = cellfun(@max,tmpdat.time);
      timrng = NaN(numel(tmpdat.trial)-(nBLt-1), 1);
      for tr = 1:(numel(tmpdat.trial)-(nBLt-1))
        timrng(tr) = endtim(tr+(nBLt-1))-begtim(tr);
      end
      [~,tr1] = min(timrng);  % find shortest time range (least NaNs interrupting trials)
    end
    
    %% realign all trials to start at time 0 to avoid overburdening memory
    % otherwise, it pads each trial out to the length of time covered by all the
    % trials together
    
    cfg = [];
    cfg.offset  = [0; -cumsum(diff(tmpdat.sampleinfo(:,1)))];
    
    tmpdat = ft_redefinetrial(cfg, tmpdat);
    
    %% calculate baseline freqanalysis
    
    waitbar((r-0.8)/size(ratT,1), wb, ...
      ['Calculating baseline freq for rat #' num2str(ratT.ratnums(r)) ...
      newline 'mean(rattime) = ' num2str(mean(rattime,'omitnan')/60) ' minutes']);
    
    cfg = mtmc;             % parameters defined in get_cfg_TEM
    cfg.toi = begtim(1):(mtmc.toi(2)-mtmc.toi(1)):endtim(1);  % adjusted for baseline only
    cfg.trials      = tr1:(tr1+(nBLt-1));
    cfg.keeptrials  = 'yes'; 	% conserves memory during calculation
    
    freq = ft_freqanalysis(cfg, tmpdat);
    
%     %% turn trials back into time - nevermind, it doesn't matter
%     
%     tmpfrq = rmfield(freq,{'dimord','time','powspctrm','crsspctrm', ...
%       'cumtapcnt','trialinfo'});  % remove fields that will change
%     for tr = 1:nBLt
%       
%     end

    %% check baseline freq for errors
    
    assert(all(ismembertol(freq.freq, cfg.foi, tol, 'DataScale', 1)))
    assert(all(ismembertol(freq.time, cfg.toi, tol, 'DataScale', 1)))
    assert(~all(isnan(freq.powspctrm(:))) && ~all(isnan(freq.crsspctrm(:))))
    if any(isnan(freq.powspctrm(:)))
      [uI, prop1] = findnans_TEM(freq.powspctrm);
      warning(['freq.powspctrm is ' num2str(prop1(end)*100) '% NaNs'])
      if prop1(end) > 0.25  % more than 25% NaNs
        figure; cfg = []; cfg.parameter = 'powspctrm'; cfg.plt = true;
        findnans_TEM(freq, cfg);
        keyboard;
      end
    else
      prop1 = 0;
    end
    if any(isnan(freq.crsspctrm(:)))
      [uI, prop2] = findnans_TEM(freq.crsspctrm);
      if prop2(end) > prop1(end)
        warning(['freq.crsspctrm is ' num2str(prop2(end)*100) '% NaNs'])
        if prop2(end)/prop1(end) > 1.25  % more than 25% increase from powspctrm
          figure; cfg = []; cfg.plt = true; findnans_TEM(freq, cfg);
          keyboard; % findnans_TEM(freq.crsspctrm,true);
        end
      end
    else
      prop2 = 0;
    end
    
    %% convert baseline crsspctrm to cohspctrm & save
    
    waitbar((r-0.8)/size(ratT,1), wb, ...
      ['Converting & saving baseline coherence for rat #' num2str(ratT.ratnums(r)) ...
      ',' newline 'mean(rattime) = ' num2str(mean(rattime,'omitnan')/60) ' minutes']);
    
    cfg = [];
    cfg.method      = 'coh';
    cfg.complex     = 'complex';
    cfg.outputfile  = outputcoh;
    
    stat = ft_connectivityanalysis(cfg,freq);
    
    if any(isnan(stat.cohspctrm(:)))
      [uI, prop3] = findnans_TEM(stat.cohspctrm);
      if prop3(end) > prop2(end)
        warning(['stat.cohspctrm is ' num2str(prop3(end)*100) '% NaNs'])
        if prop3(end)/prop2(end) > 1.25  % more than 25% increase from crsspctrm
          figure; cfg = []; cfg.plt = true; findnans_TEM(stat, cfg);
          keyboard;
        end
      end
    else
      prop3 = 0;
    end
    
    %% remove baseline crsspctrm & save powspctrm
    
    freq = rmfield(freq,{'crsspctrm','labelcmb'});
    
    waitbar((r-0.6)/size(ratT,1), wb, ['Saving baseline freq for rat #' ...
      num2str(ratT.ratnums(r)) newline 'mean(rattime) = ' ...
      num2str(round(mean(rattime,'omitnan')/60)) ' minutes']);
    
    save(outputpow, 'freq', '-v7.3');
  end   % if baseline output files exist && ~redo
  
  %% trim tone-trials to core mtmc.pad time
  
  waitbar((r-0.5)/size(ratT,1), wb, ...
    ['Preparing tone trials for rat #' num2str(ratT.ratnums(r)) ...
    newline 'mean(rattime) = ' num2str(mean(rattime,'omitnan')/60) ' minutes']);
  
  cfg = [];
  cfg.trials  = data.trialinfo(:,2) == 1;   % wherever there's a CS+
  cfg.channel = goodchs{r};
  
  tmpdat = ft_selectdata(cfg, data);
  
  cfg = [];
  cfg.toilim  = [median(mtmc.toi)-mtmc.pad/2+1/fsample ...
    median(mtmc.toi)+mtmc.pad/2-1/fsample];   % cut 2 samples to avoid rounding errors
  
  tmpdat = ft_redefinetrial(cfg, tmpdat);
  
  %% mark & remove artifacts
  
  [artifact,tmpdat,times] = artifact_nan2zero_TEM(tmpdat);
  
  cfg = [];
  % naming this artifact "allnan" will leave a permanent record in the
  % data.cfg.previous structure to remind me that non-uniform NaNs were
  % replaced w/ 0s
  cfg.artfctdef.allnan.artifact 	= artifact;
  cfg.artfctdef.reject            = 'partial';
  cfg.artfctdef.minaccepttim      = min(mtmc.t_ftimwin);
  
  tmpdat = ft_rejectartifact(cfg,tmpdat);
  
  %% loop through all tone trials
  
  trlinfo = unique(tmpdat.trialinfo,'rows','stable');
  for tr = 1:size(trlinfo,1)  % trial # across all phases
    %% define trial outputfile
    
    tn = trlinfo(tr,1);   % trial number within phase
    s = trlinfo(tr,2);    % stimulus type (1 for CS+ or 0 for none)
    p = trlinfo(tr,3);    % phase number
    
    outputpow = [datafolder num2str(ratT.ratnums(r)) filesep ...
      stimuli{s} num2str(tn) 'of' phases{p} '_' ...
      artstr '_' num2str(ratT.ratnums(r)) 'mtmconvolTFApow.mat'];
    outputcoh = [datafolder num2str(ratT.ratnums(r)) filesep ...
      stimuli{s} num2str(tn) 'of' phases{p} '_' ...
      artstr '_' num2str(ratT.ratnums(r)) 'mtmconvolTFAcoh.mat'];
    
    %% check if they exist
    
    if existfile_TEM(outputpow) && existfile_TEM(outputcoh) && ~redo
      errlog{r,tr} = ['mtmconvolTFA already calculated for ' ...
        stimuli{s} ' #' num2str(tn) ' of ' phases{p} ', Rat #' ...
        num2str(ratT.ratnums(r)) '. Skipping.'];
      warning(errlog{r,tr})
      %         disp('loading to check for coherence values > 1')
      %         freq = rmvlargefields_TEM(outputpow);
      %         stat = rmvlargefields_TEM(outputcoh);
    else
      
      %% calculate trial spectrogram
      
      waitbar((r-0.4+0.4*((tr-1)/size(trlinfo,1)))/size(ratT,1), wb, ...
        ['Calculating freq for tone trial #' num2str(tr) ', rat #' ...
        num2str(ratT.ratnums(r)) newline 'mean(rattime) = ' ...
        num2str(mean(rattime,'omitnan')/60) ' minutes']);
      
      cfg         = mtmc;
      cfg.trials  = ismember(tmpdat.trialinfo, trlinfo(tr,:), 'rows');
      
      freq = ft_freqanalysis(cfg,tmpdat);
      
      %% check for errors
      
      assert(all(ismembertol(freq.freq, cfg.foi, tol, 'DataScale', 1)))
      assert(all(ismembertol(freq.time, cfg.toi, tol, 'DataScale', 1)))
      assert(~all(isnan(freq.powspctrm(:))) && ~all(isnan(freq.crsspctrm(:))))
      if any(isnan(freq.powspctrm(:)))
        [uI, prop1] = findnans_TEM(freq.powspctrm);
        warning(['freq.powspctrm is ' num2str(prop1(end)*100) '% NaNs'])
        if prop1(end) > 0.25  % more than 25% NaNs
          figure; cfg = []; cfg.parameter = 'powspctrm'; cfg.plt = true;
          findnans_TEM(freq, cfg);
          keyboard;
        end
      else
        prop1 = 0;
      end
      if any(isnan(freq.crsspctrm(:)))
        [uI, prop2] = findnans_TEM(freq.crsspctrm);
        if prop2(end) > prop1(end)
          warning(['freq.crsspctrm is ' num2str(prop2(end)*100) '% NaNs'])
          if prop2(end)/prop1(end) > 1.25  % more than 25% increase from powspctrm
            figure; cfg = []; cfg.plt = true; findnans_TEM(freq, cfg);
            keyboard;
          end
        end
      else
        prop2 = 0;
      end
      
      %% convert crsspctrm to cohspctrm
      
      cfg = [];
      cfg.method      = 'coh';
      cfg.complex     = 'complex';
      
      stat = ft_connectivityanalysis(cfg,freq);
      
      %       end
      
      %% check for errors
      
      assert(size(stat.labelcmb,1) == numel(goodchs{r})*(numel(goodchs{r})-1)/2)
      assert(all(ismembertol(stat.freq, mtmc.foi, tol, 'DataScale', 1)))
      assert(all(ismembertol(stat.time, mtmc.toi, tol, 'DataScale', 1)))
      assert(~any(any(all(isnan(stat.cohspctrm),3))))
      % no channel-pair or freq is NaNs across all time
      
      if any(isnan(stat.cohspctrm(:)))
        [uI, prop3] = findnans_TEM(stat.cohspctrm);
        if ~(existfile_TEM(outputpow) && existfile_TEM(outputcoh) && ~redo) % prop2 wasn't calculated
          if prop3(end) > prop2(end)
            warning(['stat.cohspctrm is ' num2str(prop3(end)*100) '% NaNs'])
            if prop3(end)/prop2(end) > 1.25  % more than 25% increase from crsspctrm
              figure; cfg = []; cfg.plt = true; findnans_TEM(stat, cfg);
              keyboard;
            end
          end
        end
      else
        prop3 = 0;
      end
      
      maxabove0 = max(abs(stat.cohspctrm(:)),[],'omitnan')-1;
      if maxabove0 > 10^-15
        [chgt1,fgt1,tgt1] = ind2sub(size(stat.cohspctrm), find(maxabove0 > 10^-15)); % used for debugging
        error('coherence values should not be >1')
      elseif existfile_TEM(outputpow) && existfile_TEM(outputcoh) && ~redo
        continue  % to next trial
      end
      
      %% remove crsspctrm from freq
      
      freq = rmfield(freq,{'crsspctrm','labelcmb'});
      
      %% re-replace artifacts w/ NaNs in freq.pow & stat.coh
      
      for ch = 1:numel(freq.label)
        badt = [times{tr,ch}];
        if ~isempty(badt) && any(...
            badt > (min(freq.time) - max(freq.cfg.t_ftimwin)) & ...
            badt < (max(freq.time) + max(freq.cfg.t_ftimwin)))
          ci = find(any(strcmp(freq.label{ch}, stat.labelcmb)));
          %         fig = figure(ch); clf; fig.WindowStyle = 'docked';
          %         imagesc(10*log10(squeeze(freq.powspctrm(ch,:,:)))); axis xy;
          %         fig = figure(50+ch); clf; fig.WindowStyle = 'docked';
          %         m = floor(sqrt(numel(ci))); n = ceil(numel(ci)/m);
          %         for c = 1:numel(ci)
          %           subplot(m,n,c);
          %           imagesc(10*log10(abs(squeeze(freq.crsspctrm(ci(c),:,:)))));
          %           axis xy
          %         end
          for t = 1:numel(freq.time)
            for f = 1:numel(freq.freq)
              mint = freq.time(t) - freq.cfg.t_ftimwin(f);
              maxt = freq.time(t) + freq.cfg.t_ftimwin(f);
              if any(badt > mint & badt < maxt)
                freq.powspctrm(ch,f,t) = NaN;
                stat.cohspctrm(ci,f,t) = NaN;
              end
            end
          end
          %         fig = figure(100+ch); clf; fig.WindowStyle = 'docked';
          %         imagesc(10*log10(squeeze(freq.powspctrm(ch,:,:)))); axis xy
          %         fig = figure(150+ch); clf; fig.WindowStyle = 'docked';
          %         for c = 1:numel(ci)
          %           subplot(m,n,c);
          %           imagesc(10*log10(abs(squeeze(freq.crsspctrm(ci(c),:,:)))));
          %           axis xy
          %         end
          %         keyboard % inspect figures
          %         close all
        end
      end
      
      if any(isnan(freq.powspctrm(:)))
        [uI, prop4] = findnans_TEM(freq.powspctrm);
        warning(['freq.powspctrm is ' num2str(prop4(end)*100) '% NaNs'])
        if ~(existfile_TEM(outputpow) && existfile_TEM(outputcoh) && ~redo) % prop1 wasn't calculated
          if prop4(end)-prop1(end) > 0.25  % 25% more than orig powspctrm
            figure; cfg = []; cfg.plt = true; findnans_TEM(freq, cfg);
            keyboard; % findnans_TEM(freq.powspctrm,true);
          end
        end
      else
        prop4 = 0;
      end
      
      if any(isnan(stat.cohspctrm(:)))
        [uI, prop5] = findnans_TEM(stat.cohspctrm);
        if prop5(end) > prop3(end)
          warning(['stat.cohspctrm is ' num2str(prop5(end)*100) '% NaNs'])
          if prop5(end)-prop3(end) > 0.25  % 25% more than orig cohspctrm
            figure; cfg = []; cfg.plt = true; findnans_TEM(stat, cfg);
            keyboard;
          end
        end
      else
        prop5 = 0;
      end
      
      %% save freq & stat
      
      waitbar((r-0.4+0.4*((tr-0.5)/size(trlinfo,1)))/size(ratT,1), wb, ...
        ['Saving freq & stat for tone trial #' num2str(tr) ', rat #' num2str(ratT.ratnums(r)) ...
        newline 'mean(rattime) = ' num2str(mean(rattime,'omitnan')/60) ' minutes']);
      
      save(outputpow, 'freq', '-v7.3');
      save(outputcoh, 'stat', '-v7.3');
    end
  end
  rattime(r) = toc;
  %   catch ME
  %     warning(ME.message);
  %     errlog{r,sum(expected)+1} = ME;
  %   end
end

%% save errlog

save([datafolder 'errlog' datestr(now,30) ...
  '_FT_mtmconvolTFA_OrigFear_TEM'], 'errlog')

waitbar(1, wb, ['Done!  Total time: ' ...
  num2str(sum(rattime,'omitnan')/60) ' minutes']);