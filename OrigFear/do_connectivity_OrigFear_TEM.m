function do_connectivity_OrigFear_TEM( r )
% DO_CONNECTIVITY_ORIGFEAR_TEM calculates & compares connectivity patterns
%   within & across trials, including coherence and granger causality
%
%   This function is for within-subject stats on the output of mtmfft.
%
%   Depends on global variables defined in info_origfear.
%
% written 6/9/17 by Teresa E. Madsen, Ph.D.

%% declare global variables

global datafolder ratT fsample stimuli phases 

%% create waitbar

wb = waitbar(0,['Preparing for connectivity analysis on rat #' ...
  num2str(ratT.ratnums(r))]);

%% Define input & outputfiles for baseline

inputfile = [datafolder 'mtmfft' filesep num2str(ratT.ratnums(r)) ...
  filesep 'Baseline_shockt_ICA_' num2str(ratT.ratnums(r)) 'mtmfft.mat'];

outputfile = [datafolder 'connectivity' filesep num2str(ratT.ratnums(r)) ...
  filesep 'Baseline_shockt_ICA_' num2str(ratT.ratnums(r)) 'coherence.mat'];

%% convert baseline crsspctrm to cohspctrm & save
    
if ~existfile_TEM(outputfile)
    waitbar(0.1, wb, ...
      ['Converting & saving baseline coherence for rat #' num2str(ratT.ratnums(r))]);
    
    cfg = [];
    cfg.method      = 'coh';
    cfg.complex     = 'complex';
    cfg.inputfile   = inputfile;
    cfg.outputfile  = outputfile;
    
    stat = ft_connectivityanalysis(cfg);
    
    %% clear memory
    
    clearvars stat
end

%% Define input & outputfiles for tone trials

inputfile = [datafolder 'mtmfft' filesep num2str(ratT.ratnums(r)) ...
  filesep 'BDATones_shockt_ICA_' num2str(ratT.ratnums(r)) 'mtmfft.mat'];

outputfile = [datafolder 'connectivity' filesep num2str(ratT.ratnums(r)) ...
  filesep 'BDATones_shockt_ICA_' num2str(ratT.ratnums(r)) 'coherence.mat'];

%% convert BDA-tone subtrials crsspctrm to cohspctrm & save
    
if ~existfile_TEM(outputfile)
    waitbar(0.1, wb, ['Converting & saving tone trial coherence for rat #' ...
      num2str(ratT.ratnums(r))]);
    
    cfg = [];
    cfg.method      = 'coh';
    cfg.complex     = 'complex';
    cfg.inputfile   = inputfile;
    cfg.outputfile  = outputfile;
    
    stat = ft_connectivityanalysis(cfg);
    
    %% clear memory
    
    clearvars stat
end

%% run within-subject stats on coherence

error('finish coding!')

%% plot significant differences in coherence



%% calculate & save granger causality



%% run within-subject stats on granger causality



%% plot significant differences in granger causality


