%% load data and compare recording sources for quality & fidelity
% in vivo data is from stim electrode in GP
% in vitro data is from audio file played into beef

clc; close all; clearvars

datafolder = 'S:\Teresa\Maysam\EnerCage\TestRecordings\';

outputfile{1} = [datafolder 'allRawData.mat'];
redo = false;

%% load raw data

if exist(outputfile{1},'file') && ~redo
  load(outputfile{1})
else
  
  %% recorded on Plexon rig, via Byunghun's adapter
  
  cfg                 = [];
  cfg.dataset         = [datafolder 'Plexon-adapter\test recording.nex'];
  cfg.continuous      = 'yes';
  cfg.channel         = 'AD*';
  cfg.checkmaxfilter  = true; % don't know what this does, but this is the default that's not being properly applied
  
  data.invivo.Plexon1 = ft_preprocessing(cfg);  % already in uV, others need to be converted
  
  cfg.dataset         = [datafolder 'Plexon-adapter\test recording1.nex'];
  
  data.invivo.Plexon2 = ft_preprocessing(cfg);
  
  %% recorded via Byunghun's headstage powered inductively by EnerCage
  
  temp = load([datafolder 'Byunghun_headstage\In_Vivo_Emory_12152016_ch14_wireless.mat']);
  
  data.invivo.ByunghunEC.label{1}     = 'AD08';   % same electrode -> same name
  data.invivo.ByunghunEC.time{1}      = temp.time';
  data.invivo.ByunghunEC.trial{1}     = 1000000/330*(temp.ch14-median(temp.ch14))';
  data.invivo.ByunghunEC.fsample      = 25000;
  data.invivo.ByunghunEC.sampleinfo   = [1 numel(data.invivo.ByunghunEC.time{1})];
  
  temp = load([datafolder 'Byunghun_headstage\In_vitro_test_wireless.mat']);
  
  data.invitro.ByunghunEC.label{1}    = 'ch14';   % diff electrode -> diff name
  data.invitro.ByunghunEC.time{1}     = temp.time';
  data.invitro.ByunghunEC.trial{1}    = 1000000/330*(temp.ch14-median(temp.ch14))';
  data.invitro.ByunghunEC.fsample     = 25000;
  data.invitro.ByunghunEC.sampleinfo  = [1 numel(data.invitro.ByunghunEC.time{1})];
  
  %% recorded via Byunghun's headstage powered by battery
  
  temp = load([datafolder 'Byunghun_headstage\In_Vivo_Emory_12192016_ch14_battery1.mat']);
  
  data.invivo.Battery1.label{1}   = 'AD08';   % same electrode -> same name
  data.invivo.Battery1.time{1}    = temp.time';
  data.invivo.Battery1.trial{1}   = 1000000/330*(temp.ch14-median(temp.ch14))';
  data.invivo.Battery1.fsample    = 25000;
  data.invivo.Battery1.sampleinfo = [1 numel(data.invivo.Battery1.time{1})];
  
  temp = load([datafolder 'Byunghun_headstage\In_Vivo_Emory_12192016_ch14_battery2.mat']);
  
  data.invivo.Battery2.label{1}   = 'AD08';   % same electrode -> same name
  data.invivo.Battery2.time{1}    = temp.time';
  data.invivo.Battery2.trial{1}   = 1000000/330*(temp.ch14-median(temp.ch14))';
  data.invivo.Battery2.fsample    = 25000;
  data.invivo.Battery2.sampleinfo = [1 numel(data.invivo.Battery2.time{1})];
  
  temp = load([datafolder 'Byunghun_headstage\In_Vivo_Emory_12192016_ch14_battery3.mat']);
  
  data.invivo.Battery3.label{1}   = 'AD08';   % same electrode -> same name
  data.invivo.Battery3.time{1}    = temp.time';
  data.invivo.Battery3.trial{1}   = 1000000/330*(temp.ch14-median(temp.ch14))';
  data.invivo.Battery3.fsample    = 25000;
  data.invivo.Battery3.sampleinfo = [1 numel(data.invivo.Battery3.time{1})];
  
  temp = load([datafolder 'Byunghun_headstage\In_vitro_test_DC.mat']);
  
  data.invitro.Battery.label{1}   = 'ch14';   % diff electrode -> diff name
  data.invitro.Battery.time{1}    = temp.time';
  data.invitro.Battery.trial{1}   = 1000000/330*(temp.ch14-median(temp.ch14))';
  data.invitro.Battery.fsample    = 25000;
  data.invitro.Battery.sampleinfo = [1 numel(data.invitro.Battery.time{1})];
  
  %% Yaoyao's data (see README for explanations of each trial & section)
  
  filelist = dir([datafolder 'recording_data_yaoyao_jia\**\*.mat']);
  
  for s = 1:numel(filelist)
    temp = load([filelist(s).folder filesep filelist(s).name]);
    fn = fieldnames(temp);
    if numel(fn) ~= 1 || size(temp.(fn{1}),2) ~= 1
      disp(fn)
      keyboard  % pause to view contents and correct following
    end
    trlname = ['Yaoyao_t' filelist(s).folder(end) 's' filelist(s).name(end-4)];
    data.invivo.(trlname).label{1}   = 'AD08'; 	% same electrode -> same name
    data.invivo.(trlname).trial{1}   = 1000000/1000*(temp.(fn{1})-median(temp.(fn{1})))';
    data.invivo.(trlname).fsample    = 666;
    data.invivo.(trlname).time{1} = (1:numel(temp.(fn{1})))/data.invivo.(trlname).fsample;
    data.invivo.(trlname).sampleinfo = [1 numel(temp.(fn{1}))];
  end
  
  temp = [];
  
  %% save data to make this faster in the future
  
  save(outputfile{1},'data');
end

%% preprocess all in vivo recordings

datafields = fieldnames(data.invivo);

outputfile{2} = [datafolder 'preprocDataStats.mat'];
redo = false;

%% load preprocessed data & stats

if exist(outputfile{2},'file') && ~redo
  load(outputfile{2})
else
  
  for d = 1:numel(datafields)
    %% evaluate some basic descriptive stats
    
    stats.invivo.(datafields{d}).min = min(data.invivo.(datafields{d}).trial{1}, [], 'omitnan');
    stats.invivo.(datafields{d}).max = max(data.invivo.(datafields{d}).trial{1}, [], 'omitnan');
    stats.invivo.(datafields{d}).mean = mean(data.invivo.(datafields{d}).trial{1}, 'omitnan');
    stats.invivo.(datafields{d}).median = median(data.invivo.(datafields{d}).trial{1}, 'omitnan');
    stats.invivo.(datafields{d}).mode = mode(data.invivo.(datafields{d}).trial{1});
    stats.invivo.(datafields{d}).std = std(data.invivo.(datafields{d}).trial{1}, 'omitnan');
    stats.invivo.(datafields{d}).range = range(data.invivo.(datafields{d}).trial{1});
    stats.invivo.(datafields{d}).numel = numel(data.invivo.(datafields{d}).trial{1});
    stats.invivo.(datafields{d}).iqr = iqr(data.invivo.(datafields{d}).trial{1});
    stats.invivo.(datafields{d}).tmean = trimmean(data.invivo.(datafields{d}).trial{1}, 30);
    
    % z value corresponding to 1 sample in normally distributed data
    stats.invivo.(datafields{d}).z1sample = norminv(1-1/stats.invivo.(datafields{d}).numel,0,1);
    % z value corresponding to +/- 800 uV
    stats.invivo.(datafields{d}).z800uV = 800/stats.invivo.(datafields{d}).std;
    
    %% save min diff & bit resolution
    
    temp = abs(diff(data.invivo.(datafields{d}).trial{1}));
    stats.invivo.(datafields{d}).mindiff = min(temp(temp > 0));
    stats.invivo.(datafields{d}).bitres = nextpow2(...
      stats.invivo.(datafields{d}).range / ...
      stats.invivo.(datafields{d}).mindiff);
    
    %% load clean data (artifacts removed) if available
    
    outputfile{3} = [datafolder datafields{d} '_clean.mat'];
    
    if exist(outputfile{3},'file') && ~redo
      S = load(outputfile{3},'data');
      data.invivo.(datafields{d}) = S.data;
    else  % skips artifact rejection but still does 2nd set of stats
      %% mark NaNs as artifacts
      
      artifact.nan = [];
      
      for tr = 1:numel(data.invivo.(datafields{d}).trial)
        % identify whether there are any NaNs at each timepoint -
        % specifying dim = 1 is important in single channel case!
        tnan = any(isnan(data.invivo.(datafields{d}).trial{tr}),1);
        
        if any(tnan)
          % determine the file sample #s for this trial
          trsamp = data.invivo.(datafields{d}).sampleinfo(tr,1):...
            data.invivo.(datafields{d}).sampleinfo(tr,2);
          
          while any(tnan)
            % start from the end so sample #s don't shift
            endnan = find(tnan,1,'last');
            tnan = tnan(1:endnan);  % remove any non-NaNs after this
            
            % find last non-NaN before the NaNs
            beforenan = find(~tnan,1,'last');
            
            if isempty(beforenan)   % if no more non-NaNs
              begnan = 1;
              tnan = false;   % while loop ends
            else  % still more to remove - while loop continues
              begnan = beforenan + 1;
              tnan = tnan(1:beforenan);  % remove the identified NaNs
            end
            
            % identify file sample #s that correspond to beginning and end of
            % this chunk of NaNs and append to artifact.nan
            artifact.nan = [artifact.nan; ...
              trsamp(begnan) trsamp(endnan)];
          end   % while any(tnan)
        end   % if any(tnan)
      end   % for tr = 1:numel(data.invivo.(datafields{d}).trial)
      
      %% remove NaNs from data
      
      cfg                         = [];
      cfg.artfctdef.nan.artifact  = artifact.nan;
      cfg.artfctdef.reject        = 'partial';
      
      data.invivo.(datafields{d}) = ft_rejectartifact(cfg,data.invivo.(datafields{d}));
      
      %% filter & downsample Byunghun's data
      
      if data.invivo.(datafields{d}).fsample == 25000
        %% low pass filter under 200 Hz
        
        cfg = [];
        cfg.lpfilter      = 'yes';
        cfg.lpfreq        = 200;
        
        data.invivo.(datafields{d}) = ft_preprocessing(cfg,data.invivo.(datafields{d}));
        
        %% downsample by a factor of 25
        
        data.invivo.(datafields{d}).trial{1} = data.invivo.(datafields{d}).trial{1}(1:25:end);
        data.invivo.(datafields{d}).time{1} = data.invivo.(datafields{d}).time{1}(1:25:end);
        data.invivo.(datafields{d}).fsample = data.invivo.(datafields{d}).fsample/25;
        data.invivo.(datafields{d}).sampleinfo(2) = numel(data.invivo.(datafields{d}).time{1});
        
      end
      
      %% spectrogram of raw data
      
      % load old TFA result if available
      
      outputfile{5} = [datafolder datafields{d} '_TFA.mat'];
      
      if exist(outputfile{5},'file') && ~redo
        S = load(outputfile{5},'freq');
        TFA.invivo.(datafields{d}) = S.freq;
      else
        %% set parameters & run
        
        cfg = [];
        cfg.method      = 'mtmconvol';
        cfg.output      = 'pow';
        cfg.pad         = 'nextpow2';
        cfg.foi         = logspace(log10(0.5),log10(200),200);
        cfg.tapsmofrq   = cfg.foi/30;
        cfg.tapsmofrq(cfg.tapsmofrq < 0.25) = 0.25;   % keeps timwin < 8s
        cfg.t_ftimwin   = 2./cfg.tapsmofrq;
        cfg.toi         = (data.invivo.(datafields{d}).time{1}(1)+4):...
          min(cfg.t_ftimwin)/2:(data.invivo.(datafields{d}).time{end}(end)-4);
        cfg.outputfile  = outputfile{5};
        
        TFA.invivo.(datafields{d}) = ft_freqanalysis(cfg,data.invivo.(datafields{d}));
      end
      
      %% plot TFA
      
      TFA.invivo.(datafields{d}).powspctrm = 10*log10(TFA.invivo.(datafields{d}).powspctrm);
      
      cfg = [];
      cfg.colormap  = jet;
      cfg.title     = [datafields{d} ' TFA in 10*log10(uV^2)'];
      
      ft_singleplotTFR(cfg, TFA.invivo.(datafields{d}));
      
      xlabel('Time (seconds)'); ylabel('Frequency (Hz)');
      
      print(outputfile{5}(1:(end-4)),'-dpng')
      
      %       %% to stop after TFA without repeating artifact rejection
      %
      %       continue
      
      %% identify large artifacts
      
      cfg                                 = [];
      cfg.artfctdef.zvalue.channel        = 'all';
      cfg.artfctdef.zvalue.cutoff         = stats.invivo.(datafields{d}).z800uV;
      cfg.artfctdef.zvalue.trlpadding     = 0;
      cfg.artfctdef.zvalue.fltpadding     = 0;
      cfg.artfctdef.zvalue.artpadding     = 0.1;
      cfg.artfctdef.zvalue.rectify        = 'yes';
      cfg.artfctdef.zvalue.interactive    = 'yes';
      
      [~, artifact.large] = ft_artifact_zvalue(cfg,data.invivo.(datafields{d}));
      
      %% take 1st derivative of signal
      
      cfg = [];
      cfg.absdiff = 'yes';
      
      d1dat = ft_preprocessing(cfg,data.invivo.(datafields{d}));
      
      %% define clipping artifacts
      
      cfg                                 = [];
      
      cfg.artfctdef.clip.channel          = 'all';
      cfg.artfctdef.clip.pretim           = 0.1;
      cfg.artfctdef.clip.psttim           = 0.1;
      cfg.artfctdef.clip.timethreshold    = 0.05;   % s
      cfg.artfctdef.clip.amplthreshold    = 5;    % uV
      
      [~, artifact.clip] = ft_artifact_clip(cfg,d1dat);
      
      if ~isempty(artifact.clip)
        % bugs in ft_artifact_clip may indicate non-integer sample numbers,
        % and/or add time before & after end of file, either of which
        % breaks ft_rejectartifact or triggers bug in convert_event that
        % breaks ft_databrowser
        artifact.clip = round(artifact.clip);
        if any(artifact.clip(:,1) < 1)
          artifact.clip(artifact.clip(:,1) < 1, 1) = 1;
        end
        endsamp = data.invivo.(datafields{d}).sampleinfo(2);
        if any(artifact.clip(:,end) > endsamp)
          artifact.clip(artifact.clip(:,end) > endsamp, end) = endsamp;
        end
      end
      
      %% review artifacts
      
      cfg                               = [];
      cfg.artfctdef.clip.artifact       = artifact.clip;
      cfg.artfctdef.large.artifact      = artifact.large;
      
      cfg = ft_databrowser(cfg,data.invivo.(datafields{d}));
      
      %% remove artifacts
      
      cfg.artfctdef.reject          = 'partial';
      cfg.artfctdef.minaccepttim    = 3;
      cfg.outputfile                = outputfile{3};
      
      data.invivo.(datafields{d}) = ft_rejectartifact(cfg,data.invivo.(datafields{d}));
      
      d1dat = []; temp = [];
    end
    
    %% re-evaluate basic stats on cleaned data
    
    stats.invivo.(datafields{d}).cleanmin = min([data.invivo.(datafields{d}).trial{:}], [], 'omitnan');
    stats.invivo.(datafields{d}).cleanmax = max([data.invivo.(datafields{d}).trial{:}], [], 'omitnan');
    stats.invivo.(datafields{d}).cleanmean = mean([data.invivo.(datafields{d}).trial{:}], 'omitnan');
    stats.invivo.(datafields{d}).cleanmedian = median([data.invivo.(datafields{d}).trial{:}], 'omitnan');
    stats.invivo.(datafields{d}).cleanmode = mode([data.invivo.(datafields{d}).trial{:}]);
    stats.invivo.(datafields{d}).cleanstd = std([data.invivo.(datafields{d}).trial{:}], 'omitnan');
    stats.invivo.(datafields{d}).cleanrange = range([data.invivo.(datafields{d}).trial{:}]);
    stats.invivo.(datafields{d}).cleannumel = numel([data.invivo.(datafields{d}).trial{:}]);
    stats.invivo.(datafields{d}).cleaniqr = iqr([data.invivo.(datafields{d}).trial{:}]);
    stats.invivo.(datafields{d}).cleantmean = trimmean([data.invivo.(datafields{d}).trial{:}], 30);
    stats.invivo.(datafields{d}).cleanpct = 100*...
      stats.invivo.(datafields{d}).cleannumel/stats.invivo.(datafields{d}).numel;
    
  end
  
  %% save stats & preprocessed data (uncomment after "continue" @255 is removed)
  
  save(outputfile{2},'data','stats');
end

%% merge related (cleaned!) data & rename to more descriptive titles

cfg = [];

data.invivo.Plexon = ft_appenddata(cfg, data.invivo.Plexon1, ...
  data.invivo.Plexon2);
data.invivo = rmfield(data.invivo,{'Plexon1','Plexon2'});
data.invivo.ByunghunBatt = ft_appenddata(cfg, data.invivo.Battery1, ...
  data.invivo.Battery2, data.invivo.Battery3);
data.invivo = rmfield(data.invivo,{'Battery1','Battery2','Battery3'});
data.invivo.YaoyaoEC1H1RPi = ft_appenddata(cfg, data.invivo.Yaoyao_t1s1, ...
  data.invivo.Yaoyao_t1s2, data.invivo.Yaoyao_t1s3, ...
  data.invivo.Yaoyao_t1s4, data.invivo.Yaoyao_t1s5, ...
  data.invivo.Yaoyao_t1s6);
data.invivo = rmfield(data.invivo,{'Yaoyao_t1s1','Yaoyao_t1s2',...
  'Yaoyao_t1s3','Yaoyao_t1s4','Yaoyao_t1s5','Yaoyao_t1s6'});
data.invivo.YaoyaoEC1H2RPi = ft_appenddata(cfg, data.invivo.Yaoyao_t2s1, ...
  data.invivo.Yaoyao_t2s2, data.invivo.Yaoyao_t2s3);
data.invivo = rmfield(data.invivo,{'Yaoyao_t2s1','Yaoyao_t2s2',...
  'Yaoyao_t2s3'});
data.invivo.YaoyaoEC2H2RPi = data.invivo.Yaoyao_t3s1;
data.invivo = rmfield(data.invivo,'Yaoyao_t3s1');
data.invivo.YaoyaoEC2H2sCOM = data.invivo.Yaoyao_t4s1;
data.invivo = rmfield(data.invivo,'Yaoyao_t4s1');
data.invivo.YaoyaoBattH2sCOM = data.invivo.Yaoyao_t5s1;
data.invivo = rmfield(data.invivo,'Yaoyao_t5s1');

datafields = fieldnames(data.invivo);

%% re-evaluate basic stats on merged data

for d = 1:numel(datafields)
    cleanmin{d,1} = min([data.invivo.(datafields{d}).trial{:}], [], 'omitnan');
    cleanmax{d,1} = max([data.invivo.(datafields{d}).trial{:}], [], 'omitnan');
    cleanmean{d,1} = mean([data.invivo.(datafields{d}).trial{:}], 'omitnan');
    cleanmedian{d,1} = median([data.invivo.(datafields{d}).trial{:}], 'omitnan');
    cleanmode{d,1} = mode([data.invivo.(datafields{d}).trial{:}]);
    cleanstd{d,1} = std([data.invivo.(datafields{d}).trial{:}], 'omitnan');
    cleanrange{d,1} = range([data.invivo.(datafields{d}).trial{:}]);
    cleannumel{d,1} = numel([data.invivo.(datafields{d}).trial{:}]);
    cleaniqr{d,1} = iqr([data.invivo.(datafields{d}).trial{:}]);
    cleantmean{d,1} = trimmean([data.invivo.(datafields{d}).trial{:}], 30);
end

%% make a better stats table

statT = table(datafields,cleanmin,cleanmax,cleanmean,cleanmedian, ...
  cleanmode,cleanstd,cleanrange,cleannumel,cleaniqr,cleantmean);

%% spectral analyses

redo = false;

for d = 1:numel(datafields)
  %% load freq analysis if available
  
  outputfile{4} = [datafolder datafields{d} '_freq.mat'];
  
  if exist(outputfile{4},'file') && ~redo
    S = load(outputfile{4},'freq');
    freq.invivo.(datafields{d}) = S.freq;
  else
    %% fix time axes so all trials are 1s apart
    
    startsmp = 1;
    
    for tr = 1:numel(data.invivo.(datafields{d}).trial)
      data.invivo.(datafields{d}).sampleinfo(tr,:) = [startsmp, ...
        startsmp - 1 + numel(data.invivo.(datafields{d}).time{tr})];
      data.invivo.(datafields{d}).time{tr} = (startsmp:...
        data.invivo.(datafields{d}).sampleinfo(tr,2)) / ...
        data.invivo.(datafields{d}).fsample;
      startsmp = data.invivo.(datafields{d}).sampleinfo(tr,2) + ...
        data.invivo.(datafields{d}).fsample;
    end
    
    %% set parameters & run
    
    cfg = [];
    cfg.method      = 'mtmconvol';
    cfg.output      = 'pow';
    cfg.pad         = 'nextpow2';
    cfg.foi         = logspace(log10(0.5),log10(200),200);
    cfg.tapsmofrq   = cfg.foi/30;
    cfg.tapsmofrq(cfg.tapsmofrq < 0.25) = 0.25;   % keeps timwin < 8s
    cfg.t_ftimwin   = 2./cfg.tapsmofrq;
    cfg.toi         = (data.invivo.(datafields{d}).time{1}(1)+4):...
      min(cfg.t_ftimwin)/2:(data.invivo.(datafields{d}).time{end}(end)-4);
    cfg.outputfile  = outputfile{4};
    
    freq.invivo.(datafields{d}) = ft_freqanalysis(cfg,data.invivo.(datafields{d}));
  end
  
%   %% plot freq
%   
%   freq.invivo.(datafields{d}).powspctrm = 10*log10(freq.invivo.(datafields{d}).powspctrm);
%   
%   cfg = [];
%   cfg.colormap  = jet;
%   cfg.title     = [datafields{d} ' TFA in 10*log10(uV^2)'];
%   
%   ft_singleplotTFR(cfg, freq.invivo.(datafields{d}));
%   
%   xlabel('Time (seconds)'); ylabel('Frequency (Hz)');
%   
%   print(outputfile{4}(1:(end-4)),'-dpng')
end

%% save freq & statT

save([datafolder 'cleanStatT_freq.mat'],'statT','freq');

%% plot mean(dB) of all in 1 figure

figure; hold all;

for d = 1:numel(datafields)
  plot(freq.invivo.(datafields{d}).freq, ...
    mean( 10*log10(freq.invivo.(datafields{d}).powspctrm), 3, 'omitnan'));
end

legend(datafields)
axis tight