%% load data and compare recording sources for quality & fidelity
% in vivo data is from stim electrode in GP
% in vitro data is from audio file played into beef or saline

clc; close all; clearvars

datafolder = 'S:\Teresa\Maysam\EnerCage\TestRecordings\';

load([datafolder 'allRawData.mat']); % see CompareRecordingSources.m for details

%% preprocess all in vitro recordings

datafields = fieldnames(data.invitro);

outputfile{2} = [datafolder 'preprocInVitroDataStats.mat'];
redo = false;

%% load preprocessed data & stats

if exist(outputfile{2},'file') && ~redo
  load(outputfile{2})
else
  
  for d = 1:numel(datafields)
    %% evaluate some basic descriptive stats
    
    stats.invitro.(datafields{d}).min = min(data.invitro.(datafields{d}).trial{1}, [], 'omitnan');
    stats.invitro.(datafields{d}).max = max(data.invitro.(datafields{d}).trial{1}, [], 'omitnan');
    stats.invitro.(datafields{d}).mean = mean(data.invitro.(datafields{d}).trial{1}, 'omitnan');
    stats.invitro.(datafields{d}).median = median(data.invitro.(datafields{d}).trial{1}, 'omitnan');
    stats.invitro.(datafields{d}).mode = mode(data.invitro.(datafields{d}).trial{1});
    stats.invitro.(datafields{d}).std = std(data.invitro.(datafields{d}).trial{1}, 'omitnan');
    stats.invitro.(datafields{d}).range = range(data.invitro.(datafields{d}).trial{1});
    stats.invitro.(datafields{d}).numel = numel(data.invitro.(datafields{d}).trial{1});
    stats.invitro.(datafields{d}).iqr = iqr(data.invitro.(datafields{d}).trial{1});
    stats.invitro.(datafields{d}).tmean = trimmean(data.invitro.(datafields{d}).trial{1}, 30);
    
    % z value corresponding to 1 sample in normally distributed data
    stats.invitro.(datafields{d}).z1sample = norminv(1-1/stats.invitro.(datafields{d}).numel,0,1);
    % z value corresponding to +/- 800 uV
    stats.invitro.(datafields{d}).z800uV = 800/stats.invitro.(datafields{d}).std;
    
    %% save min diff & bit resolution
    
    temp = abs(diff(data.invitro.(datafields{d}).trial{1}));
    stats.invitro.(datafields{d}).mindiff = min(temp(temp > 0));
    stats.invitro.(datafields{d}).bitres = nextpow2(...
      stats.invitro.(datafields{d}).range / ...
      stats.invitro.(datafields{d}).mindiff);
    
    %% load clean data (artifacts removed) if available
    
    outputfile{3} = [datafolder datafields{d} '_clean.mat'];
    
    if exist(outputfile{3},'file') && ~redo
      S = load(outputfile{3},'data');
      data.invitro.(datafields{d}) = S.data;
    else  % skips artifact rejection but still does 2nd set of stats
      %% mark NaNs as artifacts
      
      artifact.nan = [];
      
      for tr = 1:numel(data.invitro.(datafields{d}).trial)
        % identify whether there are any NaNs at each timepoint -
        % specifying dim = 1 is important in single channel case!
        tnan = any(isnan(data.invitro.(datafields{d}).trial{tr}),1);
        
        if any(tnan)
          % determine the file sample #s for this trial
          trsamp = data.invitro.(datafields{d}).sampleinfo(tr,1):...
            data.invitro.(datafields{d}).sampleinfo(tr,2);
          
          while any(tnan)
            % start from the end so sample #s don't shift
            endnan = find(tnan,1,'last');
            tnan = tnan(1:endnan);  % remove any non-NaNs after this
            
            % find last non-NaN before the NaNs
            beforenan = find(~tnan,1,'last');
            
            if isempty(beforenan)   % if no more non-NaNs
              begnan = 1;
              tnan = false;   % while loop ends
            else  % still more to remove - while loop continues
              begnan = beforenan + 1;
              tnan = tnan(1:beforenan);  % remove the identified NaNs
            end
            
            % identify file sample #s that correspond to beginning and end of
            % this chunk of NaNs and append to artifact.nan
            artifact.nan = [artifact.nan; ...
              trsamp(begnan) trsamp(endnan)];
          end   % while any(tnan)
        end   % if any(tnan)
      end   % for tr = 1:numel(data.invitro.(datafields{d}).trial)
      
      %% remove NaNs from data
      
      cfg                         = [];
      cfg.artfctdef.nan.artifact  = artifact.nan;
      cfg.artfctdef.reject        = 'partial';
      
      data.invitro.(datafields{d}) = ft_rejectartifact(cfg,data.invitro.(datafields{d}));
      
      %% filter & downsample Byunghun's data
      
      if data.invitro.(datafields{d}).fsample == 25000
        %% low pass filter under 200 Hz
        
        cfg = [];
        cfg.lpfilter      = 'yes';
        cfg.lpfreq        = 200;
        
        data.invitro.(datafields{d}) = ft_preprocessing(cfg,data.invitro.(datafields{d}));
        
        %% downsample by a factor of 25
        
        data.invitro.(datafields{d}).trial{1} = data.invitro.(datafields{d}).trial{1}(1:25:end);
        data.invitro.(datafields{d}).time{1} = data.invitro.(datafields{d}).time{1}(1:25:end);
        data.invitro.(datafields{d}).fsample = data.invitro.(datafields{d}).fsample/25;
        data.invitro.(datafields{d}).sampleinfo(2) = numel(data.invitro.(datafields{d}).time{1});
        
      end
      
      %% spectrogram of raw data
      
      % load old TFA result if available
      
      outputfile{5} = [datafolder datafields{d} '_TFA.mat'];
      
      if exist(outputfile{5},'file') && ~redo
        S = load(outputfile{5},'freq');
        TFA.invitro.(datafields{d}) = S.freq;
      else
        %% set parameters & run
        
        cfg = [];
        cfg.method      = 'mtmconvol';
        cfg.output      = 'pow';
        cfg.pad         = 'nextpow2';
        cfg.foi         = logspace(log10(0.5),log10(200),200);
        cfg.tapsmofrq   = cfg.foi/30;
        cfg.tapsmofrq(cfg.tapsmofrq < 0.25) = 0.25;   % keeps timwin < 8s
        cfg.t_ftimwin   = 2./cfg.tapsmofrq;
        cfg.toi         = (data.invitro.(datafields{d}).time{1}(1)+4):...
          min(cfg.t_ftimwin)/2:(data.invitro.(datafields{d}).time{end}(end)-4);
        cfg.outputfile  = outputfile{5};
        
        TFA.invitro.(datafields{d}) = ft_freqanalysis(cfg,data.invitro.(datafields{d}));
      end
      
      %% plot TFA
      
      TFA.invitro.(datafields{d}).powspctrm = 10*log10(TFA.invitro.(datafields{d}).powspctrm);
      
      cfg = [];
      cfg.colormap  = jet;
      cfg.title     = [datafields{d} ' TFA in 10*log10(uV^2)'];
      
      ft_singleplotTFR(cfg, TFA.invitro.(datafields{d}));
      
      xlabel('Time (seconds)'); ylabel('Frequency (Hz)');
      
      print(outputfile{5}(1:(end-4)),'-dpng')
      
      %       %% to stop after TFA without repeating artifact rejection
      %
      %       continue
      
      %% identify large artifacts
      
      cfg                                 = [];
      cfg.artfctdef.zvalue.channel        = 'all';
      cfg.artfctdef.zvalue.cutoff         = stats.invitro.(datafields{d}).z800uV;
      cfg.artfctdef.zvalue.trlpadding     = 0;
      cfg.artfctdef.zvalue.fltpadding     = 0;
      cfg.artfctdef.zvalue.artpadding     = 0.1;
      cfg.artfctdef.zvalue.rectify        = 'yes';
      cfg.artfctdef.zvalue.interactive    = 'yes';
      
      [~, artifact.large] = ft_artifact_zvalue(cfg,data.invitro.(datafields{d}));
      
      %% take 1st derivative of signal
      
      cfg = [];
      cfg.absdiff = 'yes';
      
      d1dat = ft_preprocessing(cfg,data.invitro.(datafields{d}));
      
      %% define clipping artifacts
      
      cfg                                 = [];
      
      cfg.artfctdef.clip.channel          = 'all';
      cfg.artfctdef.clip.pretim           = 0.1;
      cfg.artfctdef.clip.psttim           = 0.1;
      cfg.artfctdef.clip.timethreshold    = 0.05;   % s
      cfg.artfctdef.clip.amplthreshold    = 5;    % uV
      
      [~, artifact.clip] = ft_artifact_clip(cfg,d1dat);
      
      if ~isempty(artifact.clip)
        % bugs in ft_artifact_clip may indicate non-integer sample numbers,
        % and/or add time before & after end of file, either of which
        % breaks ft_rejectartifact or triggers bug in convert_event that
        % breaks ft_databrowser
        artifact.clip = round(artifact.clip);
        if any(artifact.clip(:,1) < 1)
          artifact.clip(artifact.clip(:,1) < 1, 1) = 1;
        end
        endsamp = data.invitro.(datafields{d}).sampleinfo(2);
        if any(artifact.clip(:,end) > endsamp)
          artifact.clip(artifact.clip(:,end) > endsamp, end) = endsamp;
        end
      end
      
      %% review artifacts
      
      cfg                               = [];
      cfg.artfctdef.clip.artifact       = artifact.clip;
      cfg.artfctdef.large.artifact      = artifact.large;
      
      cfg = ft_databrowser(cfg,data.invitro.(datafields{d}));
      
      %% remove artifacts
      
      cfg.artfctdef.reject          = 'partial';
      cfg.artfctdef.minaccepttim    = 3;
      cfg.outputfile                = outputfile{3};
      
      data.invitro.(datafields{d}) = ft_rejectartifact(cfg,data.invitro.(datafields{d}));
      
      d1dat = []; temp = [];
    end
    
    %% re-evaluate basic stats on cleaned data
    
    stats.invitro.(datafields{d}).cleanmin = min([data.invitro.(datafields{d}).trial{:}], [], 'omitnan');
    stats.invitro.(datafields{d}).cleanmax = max([data.invitro.(datafields{d}).trial{:}], [], 'omitnan');
    stats.invitro.(datafields{d}).cleanmean = mean([data.invitro.(datafields{d}).trial{:}], 'omitnan');
    stats.invitro.(datafields{d}).cleanmedian = median([data.invitro.(datafields{d}).trial{:}], 'omitnan');
    stats.invitro.(datafields{d}).cleanmode = mode([data.invitro.(datafields{d}).trial{:}]);
    stats.invitro.(datafields{d}).cleanstd = std([data.invitro.(datafields{d}).trial{:}], 'omitnan');
    stats.invitro.(datafields{d}).cleanrange = range([data.invitro.(datafields{d}).trial{:}]);
    stats.invitro.(datafields{d}).cleannumel = numel([data.invitro.(datafields{d}).trial{:}]);
    stats.invitro.(datafields{d}).cleaniqr = iqr([data.invitro.(datafields{d}).trial{:}]);
    stats.invitro.(datafields{d}).cleantmean = trimmean([data.invitro.(datafields{d}).trial{:}], 30);
    stats.invitro.(datafields{d}).cleanpct = 100*...
      stats.invitro.(datafields{d}).cleannumel/stats.invitro.(datafields{d}).numel;
    
  end
  
  %% save stats & preprocessed data (uncomment after "continue" @255 is removed)
  
  save(outputfile{2},'data','stats');
end

%% merge related (cleaned!) data & rename to more descriptive titles

cfg = [];

data.invitro.Plexon = ft_appenddata(cfg, data.invitro.Plexon1, ...
  data.invitro.Plexon2);
data.invitro = rmfield(data.invitro,{'Plexon1','Plexon2'});
data.invitro.ByunghunBatt = ft_appenddata(cfg, data.invitro.Battery1, ...
  data.invitro.Battery2, data.invitro.Battery3);
data.invitro = rmfield(data.invitro,{'Battery1','Battery2','Battery3'});
data.invitro.YaoyaoEC1H1RPi = ft_appenddata(cfg, data.invitro.Yaoyao_t1s1, ...
  data.invitro.Yaoyao_t1s2, data.invitro.Yaoyao_t1s3, ...
  data.invitro.Yaoyao_t1s4, data.invitro.Yaoyao_t1s5, ...
  data.invitro.Yaoyao_t1s6);
data.invitro = rmfield(data.invitro,{'Yaoyao_t1s1','Yaoyao_t1s2',...
  'Yaoyao_t1s3','Yaoyao_t1s4','Yaoyao_t1s5','Yaoyao_t1s6'});
data.invitro.YaoyaoEC1H2RPi = ft_appenddata(cfg, data.invitro.Yaoyao_t2s1, ...
  data.invitro.Yaoyao_t2s2, data.invitro.Yaoyao_t2s3);
data.invitro = rmfield(data.invitro,{'Yaoyao_t2s1','Yaoyao_t2s2',...
  'Yaoyao_t2s3'});
data.invitro.YaoyaoEC2H2RPi = data.invitro.Yaoyao_t3s1;
data.invitro = rmfield(data.invitro,'Yaoyao_t3s1');
data.invitro.YaoyaoEC2H2sCOM = data.invitro.Yaoyao_t4s1;
data.invitro = rmfield(data.invitro,'Yaoyao_t4s1');
data.invitro.YaoyaoBattH2sCOM = data.invitro.Yaoyao_t5s1;
data.invitro = rmfield(data.invitro,'Yaoyao_t5s1');

datafields = fieldnames(data.invitro);

%% spectral analyses

redo = true;

for d = 1:numel(datafields)
  %% load freq analysis if available
  
  outputfile{4} = [datafolder datafields{d} '_freq.mat'];
  
  if exist(outputfile{4},'file') && ~redo
    S = load(outputfile{4},'freq');
    freq.invitro.(datafields{d}) = S.freq;
  else
    %% fix time axes so all trials are 1s apart
    
    startsmp = 1;
    
    for tr = 1:numel(data.invitro.(datafields{d}).trial)
      data.invitro.(datafields{d}).sampleinfo(tr,:) = [startsmp, ...
        startsmp - 1 + numel(data.invitro.(datafields{d}).time{tr})];
      data.invitro.(datafields{d}).time{tr} = (startsmp:...
        data.invitro.(datafields{d}).sampleinfo(tr,2)) / ...
        data.invitro.(datafields{d}).fsample;
      startsmp = data.invitro.(datafields{d}).sampleinfo(tr,2) + ...
        data.invitro.(datafields{d}).fsample;
    end
    
    %% set parameters & run
    
    cfg = [];
    cfg.method      = 'mtmconvol';
    cfg.output      = 'pow';
    cfg.pad         = 'nextpow2';
    cfg.foi         = logspace(log10(0.5),log10(200),200);
    cfg.tapsmofrq   = cfg.foi/30;
    cfg.tapsmofrq(cfg.tapsmofrq < 0.25) = 0.25;   % keeps timwin < 8s
    cfg.t_ftimwin   = 2./cfg.tapsmofrq;
    cfg.toi         = (data.invitro.(datafields{d}).time{1}(1)+4):...
      min(cfg.t_ftimwin)/2:(data.invitro.(datafields{d}).time{end}(end)-4);
    cfg.outputfile  = outputfile{4};
    
    freq.invitro.(datafields{d}) = ft_freqanalysis(cfg,data.invitro.(datafields{d}));
  end
  
  %% plot freq
  
  freq.invitro.(datafields{d}).powspctrm = 10*log10(freq.invitro.(datafields{d}).powspctrm);
  
  cfg = [];
  cfg.colormap  = jet;
  cfg.title     = [datafields{d} ' TFA in 10*log10(uV^2)'];
  
  ft_singleplotTFR(cfg, freq.invitro.(datafields{d}));
  
  xlabel('Time (seconds)'); ylabel('Frequency (Hz)');
  
  print(outputfile{4}(1:(end-4)),'-dpng')
end

%% make a better stats table

statT = table(datafields);
