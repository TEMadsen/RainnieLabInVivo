function [elec,layout] = MWAlayout_TEM(cfg,MWA,outputloc)
%MWALAYOUT_TEM Create Sensor Data and Layout for MicroWire Array Configurations
% Defines 3D stereotaxic coordinates for each electrode (in the format of
% FT_DATATYPE_SENS), a landscape-oriented 2D approximation for plotting (created
% by FT_PREPARE_LAYOUT), and a neighbour array to define related clusters of
% electrodes (created by FT_PREPARE_NEIGHBOURS, using distance < 0.5 mm).
%
%   INPUTS
% cfg         = configuration structure describing N probes implanted in a
%               given rat, including the following fields:
%     cfg.atlascoord  = Nx3 array containing the target atlas coordinates
%                       (AP/ML/DV) of the most ventral electrode pair (or
%                       center) of each probe, with which bregma is
%                       measured.
%     cfg.angle       = Nx1 array containing the angles of approach, in
%                       degrees, for each probe. Positive angles mean the
%                       approach is from medial or posterior.
%     cfg.angledim    = Nx1 array containing 1 or 2 for each probe,
%                       indicating that the angle is applied to the AP or
%                       ML dimension, Nx2 character array also acceptable.
%     cfg.elecspac    = Nx2 array of the spacing (in mm) between electrodes
%                       in the AP & ML dimensions, for each probe.
%     cfg.label       = Nx1 cell array where the cell corresponding to each
%                       probe contains a cell array of strings representing
%                       the names of each channel, organized in the same
%                       way as the electrodes in the probe, assuming you're
%                       looking down at the rat's head from behind, as
%                       during surgery (i.e., rows are A -> P, columns are
%                       L -> R).
%     cfg.side        = Nx1 cell array of strings indicating which side of
%                       the brain each probe was on.
%     cfg.shorter     = optional Nx1 cell array where the cell
%                       corresponding to each probe contains a numerical
%                       array of values representing how much shorter (in
%                       mm, negative if longer) each wire is compared to
%                       the longest one used for measuring bregma.  These
%                       should be arranged in the same way as cfg.label.
%                       If this field is missing or empty, the default
%                       assumption is that all wires within each probe are
%                       the same length.
%     cfg.bregmaPt      = optional Nx2 array containing the row & column
%                       indices (.5 if between 2 electrodes) corresponding
%                       to the cell(s) in cfg.label that represent the
%                       longest electrode pair which was used to measure
%                       bregma and targets cfg.atlascoord.  If this field
%                       is missing or empty, the default assumption is that
%                       bregma is approximated to the center of the probe,
%                       so the probe is centered around the target
%                       coordinates, as is generally done when all wires
%                       are the same length.
% MWA         = optional string that names this microwire array
%               configuration, to be used for output files elec_[MWA].mat,
%               layout_[MWA].mat, and neighb_[MWA].mat
% outputloc   = optional string containing the full path to a folder where
%               the output files should be stored
%
%   OUTPUTS
% elec    = FieldTrip structure that represents a sensor array, customized
%           for M intracranial electrodes, contains these fields:
%    elec.label     = Mx1 cell-array with channel labels
%    elec.elecpos   = Mx3 matrix with electrode positions
%    elec.chanpos   = identical to cfg.elecpos
%    elec.tra       = MxM matrix to equate electrodes and channels (array
%                     of 0s with 1s along the diagonal)
% layout  = FieldTrip structure required for plotting the topographical
%           distribution of the potential or field distribution, or for
%           plotting timecourses in a topographical arrangement, containing
%           the following fields:
%    layout.label   = Mx1 cell-array with channel labels
%    layout.pos     = Mx2 matrix with channel positions
%    layout.width   = Mx1 vector with the width of each box for
%                     multiplotting
%    layout.height  = Mx1 matrix with the height of each box for
%                     multiplotting
%    layout.mask    = optional cell-array with line segments that determine
%                     the area for topographic interpolation
%    layout.outline = optional cell-array with line segments that represent
%                     the head, nose, ears, sulci or other anatomical
%                     features
% neighbours = array of structures with the "neighbours" of each channel, as in:
%    neighbours(1).label = 'Fz';
%    neighbours(1).neighblabel = {'Cz', 'F3', 'F3A', 'FzA', 'F4A', 'F4'};
%    neighbours(2).label = 'Cz';
%    neighbours(2).neighblabel = {'Fz', 'F4', 'RT', 'RTP', 'P4', 'Pz', 'P3', 'LTP', 'LT', 'F3'};
%    neighbours(3).label = 'Pz';
%    neighbours(3).neighblabel = {'Cz', 'P4', 'P4P', 'Oz', 'P3P', 'P3'};
%    ...etc. Note that a channel is not considered to be a neighbour of itself.
%
% written by Teresa E. Madsen, Ph.D.
% added neighbours 6/20/17

%% check inputs

if ~isstruct(cfg) || any(~isfield(cfg, ...
    {'atlascoord','angle','angledim','elecspac','label','side'}))
  error('See Help for required input structure')
end

[N, ~] = structfun(@size,cfg);

if all(N == N(1))
  N = N(1);
else
  error('All fields of cfg should have one row per probe')
end

if ~isfield(cfg,'shorter') || isempty(cfg.shorter) || isempty(cfg.shorter{1})
  cfg.shorter = cell(size(cfg.label));
  for n = 1:N
    cfg.shorter{n} = zeros(size(cfg.label{n}));
  end
end

if ~isfield(cfg,'bregmaPt') || isempty(cfg.bregmaPt)
  cfg.bregmaPt = zeros(N,2);
  for n = 1:N
    cfg.bregmaPt(n,:) = size(cfg.label{n})/2 + 0.5;
    if cfg.shorter{n}(floor(cfg.bregmaPt(n,1)),floor(cfg.bregmaPt(n,2))) ~= 0 ...
        || cfg.shorter{n}(floor(cfg.bregmaPt(n,1)),ceil(cfg.bregmaPt(n,2))) ~= 0 ...
        || cfg.shorter{n}(ceil(cfg.bregmaPt(n,1)),floor(cfg.bregmaPt(n,2))) ~= 0 ...
        || cfg.shorter{n}(ceil(cfg.bregmaPt(n,1)),ceil(cfg.bregmaPt(n,2))) ~= 0
      warning(['default assumption that probe was centered on bregma ' ...
        'does not make sense when wires in this area are not the longest'])
    end
  end
end

if nargin < 2 || ~ischar(MWA)
  warning('outputs will not be saved')
else
  if nargin < 3 || ~ischar(outputloc)
    outputloc = userpath;
  end
  if strcmp(outputloc(end),';')
    outputloc = outputloc(1:end-1);
  end
  if ~strcmp(outputloc(end),filesep)
    outputloc = [outputloc filesep];
  end
  elecfile    = [outputloc 'elec_' MWA '.mat'];
  layoutfile 	= [outputloc 'layout_' MWA '.mat'];
  neighbfile  = [outputloc 'neighb_' MWA '.mat'];
end

%% check for existing elecfile

if exist('elecfile','var') && exist(elecfile,'file')
  load(elecfile)
else
  %% begin creating elec output
  
  elec.dims = {'AP','ML','DV'};  % not part of FieldTrip's expected structure
  
  % Mx1 cell-array with channel labels
  elec.label = cfg.label{1}(:);
  
  for n = 2:N
    elec.label = [elec.label; cfg.label{n}(:)];
  end
  
  % Mx3 matrix with electrode positions
  elec.elecpos  = zeros(numel(elec.label),3);
  
  %% estimate stereotaxic coordinates of each electrode in each probe
  
  medmult = ones(N,1);    % medial coordinate multiplier (-1 for left side)
  
  for n = 1:N
    [~,chndx] = ismember(elec.label,cfg.label{n});
    [R,C] = ind2sub(size(cfg.label{n}),chndx);
    
    % each AP row's distance from bregmaPt (1st row is most anterior/+)
    Rdist = (cfg.bregmaPt(n,1) - R)*cfg.elecspac(n,1);
    
    % each ML column's distance from bregmaPt
    switch lower(cfg.side{n}(1))
      case 'r'  % right (1st column is most medial/-)
        Cdist = (C - cfg.bregmaPt(n,2))*cfg.elecspac(n,2);
      case 'l'  % left (1st column is most lateral/+)
        Cdist = (cfg.bregmaPt(n,2) - C)*cfg.elecspac(n,2);
        medmult(n) = -1;
      otherwise
        error('cfg.side must indicate right or left for each probe')
    end
    
    switch cfg.angledim(n,:)
      case {1, 'AP'}
        for ch = find(chndx ~= 0)'
          % AP
          elec.elecpos(ch,1) = cfg.atlascoord(n,1) + ...
            Rdist(ch)*cosd(cfg.angle(n)) - ...
            cfg.shorter{n}(R(ch),C(ch))*sind(cfg.angle(n));
          % ML (not angled)
          elec.elecpos(ch,2) = medmult(n)*(cfg.atlascoord(n,2) + Cdist(ch));
          % DV
          elec.elecpos(ch,3) = cfg.atlascoord(n,3) + ...
            cfg.shorter{n}(R(ch),C(ch))*cosd(cfg.angle(n)) + ...
            Rdist(ch)*sind(cfg.angle(n));
        end
      case {2, 'ML'}
        for ch = find(chndx ~= 0)'
          % AP (not angled)
          elec.elecpos(ch,1) = cfg.atlascoord(n,1) + Rdist(ch);
          % ML
          elec.elecpos(ch,2) = medmult(n)*(cfg.atlascoord(n,2) + ...
            Cdist(ch)*cosd(cfg.angle(n)) - ...
            cfg.shorter{n}(R(ch),C(ch))*sind(cfg.angle(n)));
          % DV
          elec.elecpos(ch,3) = cfg.atlascoord(n,3) + ...
            cfg.shorter{n}(R(ch),C(ch))*cosd(cfg.angle(n)) + ...
            Cdist(ch)*sind(cfg.angle(n));
        end
      otherwise
        error('angledim must be either 1 (AP) or 2 (ML)')
    end
  end
  
  %% complete the metadata and check that it matches FieldTrip's expectation
  
  % Mx3 matrix with channel positions (the same as electrode positions)
  elec.chanpos  = elec.elecpos;
  % MxM matrix to combine electrodes into channels
  elec.tra      = diag(ones(1,numel(elec.label)));
  
  elec.type       = 'plexon';
  elec.unit       = 'mm';
  elec.chantype 	= cellstr(repmat('LFP',size(elec.label)));
  elec.chanunit 	= cellstr(repmat('uV',size(elec.label)));
  
  clear ft_datatype_sens  % clears persistent variables so it will recheck
  elec = ft_datatype_sens(elec);  % error check
  
  %% save elec
  
  if exist('elecfile','var')
    disp(['Saving "elec" to ' elecfile]);
    save(elecfile,'elec');
  end
end

%% remember min/max of each dimension for each probe

realmin = zeros(N,3);   % actual minimum value for each dimension
realmax = zeros(N,3);   % actual maximum value for each dimension

for n = 1:N
  [~,chndx] = ismember(elec.label,cfg.label{n});
  realmin(n,:) = min(elec.elecpos(chndx ~= 0,:));
  realmax(n,:) = max(elec.elecpos(chndx ~= 0,:));
end

%% create simplified coordinates & choose dimensions for plotting

elecpos = elec.elecpos;           % start with the real position values

for d = 1:3  % simplify to 1 arbitrary unit per unique value along each dim
  [~, ~, elecpos(:,d)] = unique(elec.elecpos(:,d));
end

arrayconfig = zeros(N,3);   % # of wire positions & depths for each array
wiredist = zeros(N,3);    % distance btwn wire pos & depths for each array

for n = 1:N
  lengths = unique(cfg.shorter{n});
  arrayconfig(n,:) = [size(cfg.label{n}) numel(lengths)];
  if numel(lengths) == 1
    wiredist(n,:) = [cfg.elecspac(n,:) 0];
  else
    wiredist(n,:) = [cfg.elecspac(n,:) min(diff(lengths))];
  end
end

[~, ~, rnw] = unique(mean(arrayconfig)); 	% rank dims by mean # of wires
if ismember(rnw',perms([1 2 3]),'rows')
  dimord = fliplr(rnw');  % keep it simple!  X is biggest, Y is smaller, Z is omitted
else
  worstnw = rnw == 1;                       % worst by mean # of wires
  if sum(worstnw) == 1    % only 1 dim is worst by nw & should be discarded
    dimord = [find(~worstnw)' find(worstnw)];   % order of 1st 2 may change later
  else                    % there's more than 1 dim that could be discarded
    [~, ~, ras] = unique(mean(realmax - realmin));  % rank by mean array size
    worstas = (ras == min(ras(worstnw))) & worstnw; % worst size w/in worstnw
    if sum(worstas) == 1  % only 1 dim is worst by both nw & mean array size
      dimord = [find(~worstas)' find(worstas)];
    else  % not possible to differentiate between worstnw by mean array size
      [~, ~, rwd] = unique(mean(wiredist));   % rank by mean wire distance
      worstwd = (rwd == min(rwd(worstas))) & worstas; % worst range w/in worstas
      if sum(worstwd) == 1   % only 1 dim is worst by all 3 measures
        dimord = [find(~worstwd)' find(worstwd)];
      else % not possible to differentiate between worsts by full range
        button = questdlg('Which dimension should be ignored for 2D plots?', ...
          'Input Required', 'AP','ML','DV', 'AP');
        switch button
          case 'AP'
            dimord = [2 3 1];   % X = ML, Y = DV
          case 'ML'
            dimord = [1 3 2];   % X = AP, Y = DV
          case 'DV'
            dimord = [1 2 3];   % X = AP, Y = ML
          otherwise
            error('unable to determine which dimension to discard for 2D plots')
        end
      end
    end
  end
end

if numel(dimord) ~= 3 || ~ismember(dimord,perms([1 2 3]),'rows')
  error('something went wrong with the automatic selection of dimensions')
end

dimord = dimord(1:2); 	% keep only the 1st 2 for plotting

if dimord(1) == 3       % if X is DV, trade with Y
  dimord(1) = dimord(2);
  dimord(2) = 3;
elseif dimord(2) == 2 	% if Y is ML, flip axis so perspective is from top instead of bottom
  [~, ~, elecpos(:,2)] = unique(-elec.elecpos(:,2));
end

%% verify true coordinates by plotting in three 2D axes

% normalize each dim to 0-1 scale of proximity from viewpoint in standard
% brain slice views (for conversion to point sizes)
APdepth = (max(elecpos(:,1)) - elecpos(:,1))./max(elecpos(:,1));
MLdepth = (elecpos(:,2)-1)./max(elecpos(:,2));
DVdepth = (elecpos(:,3)-1)./max(elecpos(:,3));

colors = jet(numel(elec.label));
[~, co] = sort(elec.label);

maxrange = max(max(realmax) - min(realmin));
midrange = mean([min(realmin); max(realmax)]);
ranges = [midrange - maxrange/2; midrange + maxrange/2];

figure;
subplot(1,3,1);
scatter(elec.elecpos(:,2), elec.elecpos(:,3), ...   % real X & Y values
  (APdepth.*12 + 3).^2, ...  % simplified Z used for size
  colors(co,:));  % a unique color for every electrode (ordered by ch #)
xlabel('ML'); xlim(ranges(:,2));
ylabel('DV'); ylim(ranges(:,3));
axis square;
title('Coronal (from behind the rat)');

subplot(1,3,2);
scatter(elec.elecpos(:,2), elec.elecpos(:,1), ...   % real X & Y values
  (DVdepth.*12 + 3).^2, ...  % simplified Z used for size
  colors(co,:));  % a unique color for every electrode (ordered by ch #)
xlabel('ML'); xlim(ranges(:,2));
ylabel('AP'); ylim(ranges(:,1));
axis square;
title('Horizontal (from above the rat)');

subplot(1,3,3);
scatter(elec.elecpos(:,1), elec.elecpos(:,3), ...   % real X & Y values
  (MLdepth.*12 + 3).^2, ...  % simplified Z used for size
  colors(co,:));  % a unique color for every electrode (ordered by ch #)
xlabel('AP'); xlim(ranges(:,1));
ylabel('DV'); ylim(ranges(:,3));
axis square;
title('Sagittal (from the rat''s side)'); % left if dimord(2)=2 (X=ML), right otherwise

if all(dimord == [2 3])
  subplot(1,3,1);
elseif all(dimord == [2 1])
  subplot(1,3,2);
elseif all(dimord == [1 3])
  subplot(1,3,3);
end
legend('selected layout')

%% manually create layout for plotting

manlay.label = [elec.label; {'SCALE'}; {'COMNT'}];
manlay.width = repmat(0.8,size(manlay.label));
manlay.height = repmat(0.6,size(manlay.label));
manlay.pos = elecpos(:,dimord);   % reorder dimensions in layout
tophalf = mean(manlay.pos(1:floor(size(manlay.pos,1)/2),:));
bottomhalf = mean(manlay.pos(ceil(size(manlay.pos,1)/2)+1:end,:));
manlay.pos = [manlay.pos; tophalf(1), bottomhalf(2); bottomhalf(1), tophalf(2)];
manlay.dims = elec.dims(dimord); 	% save dimension labels

% draw outline and mask at +/- 0.5 around each array
for n = 1:N
  [~,chndx] = ismember(cfg.label{n},elec.label);
  [~,imin] = min(manlay.pos(chndx(:),:));
  [~,imax] = max(manlay.pos(chndx(:),:));
  manlay.mask{n} = [manlay.pos(chndx(imin(1)),1)-1/2 manlay.pos(chndx(imin(2)),2)-1/2; ...
    manlay.pos(chndx(imin(1)),1)-1/2 manlay.pos(chndx(imax(2)),2)+1/2; ...
    manlay.pos(chndx(imax(1)),1)+1/2 manlay.pos(chndx(imax(2)),2)+1/2; ...
    manlay.pos(chndx(imax(1)),1)+1/2 manlay.pos(chndx(imin(2)),2)-1/2; ...
    manlay.pos(chndx(imin(1)),1)-1/2 manlay.pos(chndx(imin(2)),2)-1/2];
end

manlay.outline = manlay.mask;

%% check and save layout

cfg = [];
cfg.skipscale   = 'no';
cfg.skipcomnt   = 'no';
cfg.feedback    = 'yes';
cfg.style       = '2d';
% manlay.cfg.box      = 'no';  % these don't seem to work regardless of whether
% manlay.cfg.labeloffset = 10;    % I make them fields of cfg or cfg.layout.cfg
cfg.layout      = manlay;

if exist('layoutfile','var')
  cfg.output = layoutfile;
end

layout = ft_prepare_layout(cfg);

%% prepare neighbours

cfg = [];
cfg.method          = 'distance';
cfg.neighbourdist   = 0.5;  % mm
cfg.elec            = elec;
neighbours          = ft_prepare_neighbours(cfg);

%% plot to verify

cfg = [];
cfg.neighbours = neighbours;
cfg.layout = layout;
ft_neighbourplot(cfg)

%% save elec

if exist('neighbfile','var')
  disp(['Saving "neighbours" to ' neighbfile]);
  save(neighbfile,'neighbours');
end

end

