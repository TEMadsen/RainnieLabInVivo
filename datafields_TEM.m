function [ fields, fldoi, fldsiz, dimsiz, dimord ] = datafields_TEM( data )
%DATAFIELDS_TEM indentifies the data fields that match the specified dimord
% and describes the overall structure of the data
%
%   INPUTS:
%     data  = any structure in a FieldTrip-like configuration with a dimord
%           string and variables that index each dimension of any N-dimensional
%           matrices
%
%   OUTPUTS:
%     fields  = all fieldnames in data structure
%     fldoi   = which fields' sizes correspond to the dimord
%     fldsiz  = size of each field
%     dimsiz  = expected size of each dimension in dimord
%     dimord  = cell array of strings split from data.dimord string
%
% written 9/19/2017 by Teresa E. Madsen, Ph.D.

%% check inputs

assert(isstruct(data) && isfield(data,'dimord') && ischar(data.dimord), ...  % otherwise, throw error:
  'input data must be a structure with a dimord string')

%% determine dimord & expected sizes of each dimension

dimord = strsplit(data.dimord,'_');
dimsiz = zeros(size(dimord));
for d = 1:numel(dimord)
  switch dimord{d}
    case 'rpt'
      dimsiz(d) = size(data.trialinfo,1);
    case 'chan'
      dimsiz(d) = numel(data.label);
    case 'freq'
      dimsiz(d) = numel(data.freq);
    case 'time'
      dimsiz(d) = numel(data.time);
    otherwise
      error('unexpected dimord')
  end
end

%% identify fields of interest

fields = fieldnames(data);
fldsiz = ones(numel(fields), numel(dimsiz));
for param = 1:numel(fields)
  fldsiz(param,1:ndims(data.(fields{param}))) = size(data.(fields{param}));
end
fldoi = ismember(fldsiz,dimsiz,'rows');

%% check result

if ~any(fldoi)
  warning('no fields match the expected dimord')
  disp(data)
end

end
