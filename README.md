To use the RainnieLabInVivo repository:

1) Follow the steps in SetupGit.m one section at a time.

2) Check out the resources in S:\Teresa\Software\Git and online tutorials to learn how to use git.

3) Run MaintainGit.m daily, at Matlab startup and shutdown, or as often as you have new commits to push to the servers or want to check for updates from the servers.