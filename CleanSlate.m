%% provides a clean slate to start working with new data/scripts
% This is a basic version to include at beginning of all scripts. The
% harsher, more complete version commented below should be copied into the
% "CleanSlate" shortcut (QuickAccess icon "C").

clc         % clear command window
close all   % close all figures, use "close all force" if not responding
clearvars   % use "clear all" to include all variables and functions

% % provides a clean slate to start working
% % with new data/scripts: clearing the
% % command window, variables, functions, and
% % figures (even if not responding)
% clc; close all force; clear all