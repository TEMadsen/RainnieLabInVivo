function disperrlog( errlog )
% DISPERRLOG displays all contents of errlog nested structure
%
% unfold(errlog) does the same thing, but both are too much to read

for r = 1:numel(errlog)
  fields{1} = fieldnames(errlog(r));
  for ch = 1:numel(fields{1})
    if ~isstruct(errlog(r).(fields{1}{ch}))
      disp(['errlog(' num2str(r) ').' fields{1}{ch} ' = '])
      disp(errlog(r).(fields{1}{ch}))
      continue
    end
    fields{2} = fieldnames(errlog(r).(fields{1}{ch}));
    for p = 1:numel(fields{2})
      if ~isstruct(errlog(r).(fields{1}{ch}).(fields{2}{p}))
        disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} ' = '])
        disp(errlog(r).(fields{1}{ch}).(fields{2}{p}))
        continue
      end
      fields{3} = fieldnames(errlog(r).(fields{1}{ch}).(fields{2}{p}));
      for s = 1:numel(fields{3})
        if ~isstruct(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}))
          disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} ' = '])
          disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}))
          continue
        end
        fields{4} = fieldnames(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}));
        if ischar(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4}))
          disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} '.' fields{4}{f4} ' = '])
          disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4}))
          continue
        end
        % indexing changes from multiple fields of a scalar structure to one field of a non-scalar structure
        for b = 1:numel(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4}))
          if ~isstruct(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b))
            disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} '.' fields{4}{f4} '(' num2str(b) ') = '])
            disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b))
            continue
          end
          fields{5} = fieldnames(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b));
          for f5 = 1:numel(fields{5})
            if ischar(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5}))
              disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} '.' fields{4}{f4} '(' num2str(b) ').' fields{5}{f5} ' = '])
              disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5}))
              continue
            end
            for t = 1:numel(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5}))
              if ~isstruct(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5})(t))
                disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} '.' fields{4}{f4} '(' num2str(b) ').' fields{5}{f5} '(' num2str(t) ') = '])
                disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5})(t))
                continue
              end
              fields{6} = fieldnames(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5})(t));
              for f6 = 1:numel(fields{6})
                if ~isstruct(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5})(t).(fields{6}{f6}))
                  disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} '.' fields{4}{f4} '(' num2str(b) ').' fields{5}{f5} '(' num2str(t) ').' fields{6}{f6} ' = '])
                  disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5})(t).(fields{6}{f6}))
                  continue
                end
                keyboard % dbcont if okay to continue without drilling down further into structure
              end
            end
          end
        end
      end
    end
  end
end

end

