function [zscore2d,pval2d,h2d,MI2d,phasepref2d] = sig_KL_MI2d_TEM(data,foi_phase,w_phase,foi_amp,w_amp,plt)
%%   Compute the two-dimensional Kullback�Leibler MI and plot the result.
%       From Tort et al., J Neurophysiol, 2010
%
%   USAGE:
%       [zscore2d,pval2d,h2d,MI2d,phasepref2d] = sig_KL_MI2d_TEM(...
%           data,foi_phase,w_phase,foi_amp,w_amp,plt);
%
%   INPUTS:
%       data        = The unfiltered data, assumes 1000 Hz sampling rate
%       foi_phase   = central frequencies for phase modulation
%       w_phase     = width of phase band (foi +/- w)
%       foi_amp     = central frequencies for modulated amplitude
%       w_amp       = width of amplitude band (foi +/- w)
%       plt         = 'y' or 'n' to plot data
%
%   OUTPUTS:
%       A plot of the two-dimensional z-score transformed modulation index.
%       zscore2d        = the z-score of the MI of raw data vs surrogates
%       pval2d          = the corresponding p-value.
%       h2d             = the result of the z-test: 1 if the null 
%                           hypothesis is rejected, 0 if it is not
%       MI2d            = modulation index from Tort et al., J Neurophysiol, 2010
%       phasepref2d     = the "preferred" phase (center of bin with highest
%                           gamma amplitude) expressed in degrees relative 
%                           to delta/theta peak
%
%   DEPENDENCIES:
%    sig_KL_MI_TEM.m
%    eegfilt.m (From EEGLAB Toolbox, http://www.sccn.ucsd.edu/eeglab/)
%
%   original function programmed by MAK.  Nov 12, 2007.
%   modified by TEM 2015

if nargin < 6 || isempty(plt);  plt = 'n';  end

zscore2d = zeros(length(foi_phase), length(foi_amp));
pval2d = zeros(length(foi_phase), length(foi_amp));
h2d = zeros(length(foi_phase), length(foi_amp));
MI2d = zeros(length(foi_phase), length(foi_amp));
phasepref2d = zeros(length(foi_phase), length(foi_amp));

epochframes = length(data);
h = waitbar(0,'Filtering data...');

for i=1:length(foi_phase)
    filtorder = 3*fix(1000/(foi_phase(i)-w_phase));
    if filtorder*3 >= epochframes
        filtorder = fix(epochframes/3)-1;
    end
    % Filter the low freq signal & extract its phase.
    theta = eegfilt(data,1000,foi_phase(i)-w_phase,foi_phase(i)+w_phase,...
        epochframes,filtorder); 
    phase = angle(hilbert(theta));
    waitbar(i/length(foi_phase))
    for j=1:length(foi_amp)
        % Filter the high freq signal & extract its amplitude envelope.
        gamma = eegfilt(data,1000,foi_amp(j)-w_amp,foi_amp(j)+w_amp);     
        amp = abs(hilbert(gamma));
        % Compute & test significance of the modulation index.
        [zscore2d(i,j),pval2d(i,j),h2d(i,j),MI2d(i,j),phasepref2d(i,j)] = ...
            sig_KL_MI_TEM(amp, phase);   
    end
end
close(h)

if plt == 'y'
    % Plot the two-dimensional modulation index.
    figure(gcf); imagesc(foi_phase, foi_amp, zscore2d');  colorbar; axis xy
    set(gca, 'FontName', 'Arial', 'FontSize', 14); title('KL-MI 2d');
    xlabel('Phase Frequency (Hz)');  ylabel('Envelope Frequency (Hz)');
end
end
