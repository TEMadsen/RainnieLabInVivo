function [ artifact,count ] = artifact_nan_TEM( data )
% ARTIFACT_NAN marks NaNs on any channel as an artifact, taking FT format
%   data structure & returning the same format as ft_artifact_xxx, for
%   input to ft_rejectartifact.  Can also output count of non-uniform NaNs
%   (those not present on all channels) by trial & channel.
%
% written 2/28/17 by Teresa E. Madsen, Ph.D.

artifact = [];
count = zeros(numel(data.trial), numel(data.label));

for tr = 1:numel(data.trial)
  % identify whether there are any NaNs at each timepoint
  trlnan = isnan(data.trial{tr});
  count(tr,:) = sum(trlnan(:,~all(trlnan,1)),2);  % only count NaNs not on all channels
  tnan = any(trlnan,1);  % need dim for single channel case
  
  if any(tnan)
    % determine the file sample #s for this trial
    trsamp = data.sampleinfo(tr,1):data.sampleinfo(tr,2);
    
    while any(tnan)
      % start from the end so sample #s don't shift
      endnan = find(tnan,1,'last');
      tnan = tnan(1:endnan);  % remove any non-NaNs after this
      
      % find last non-NaN before the NaNs
      beforenan = find(~tnan,1,'last');
      
      if isempty(beforenan)   % if no more non-NaNs
        begnan = 1;
        tnan = false;   % while loop ends
      else  % still more to remove - while loop continues
        begnan = beforenan + 1;
        tnan = tnan(1:beforenan);  % remove the identified NaNs
      end
      
      % identify file sample #s that correspond to beginning and end of
      % this chunk of NaNs and append to artifact
      artifact = [artifact; trsamp(begnan) trsamp(endnan)]; %#ok<AGROW>
    end   % while any(tnan)
  end   % if any(tnan)
end   % for tr = 1:numel(data.trial)

end
