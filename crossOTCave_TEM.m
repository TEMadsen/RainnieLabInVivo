function [zscore,pval,h,modS,otc,fft_otc,pwrN] = crossOTCave_TEM(trdata,cfg,wavelets,plt)
%   cross-region cross-frequency phase-amplitude modulation,
%       using OTC modulation strength converted to z-score.
%           From Dvorak & Fenton, J. Neurosci. Methods 2014
%   by Teresa E. Madsen 2015
%
%   Usage:
%       [zscore,pval,h,modS,otc,fft_otc,pwrN] = crossOTC_TEM(...
%                       trdata,cfg,wavelets,plt);
%
%   Inputs:
%       trdata      = a 2xN matrix of unfiltered time series - each will be used
%           as both the low frequency, phase-modulating signal and the high
%           frequency, amplitude-modulated signal
%       cfg         = configuration structure including fields:
%           foi_mod     = central frequencies for modulated amplitude
%           foi_osc     = wavelet frequencies to find oscillations
%           Fs          = sampling frequency
%           wFactor     = scale factor (each band +/- itself/wfactor)
%           powerTh     = power threshold (S.D.)
%           winLen      = OTC window (+/- samples from oscillatory event)
%           nfft        = # of frequencies for fft
%           f_fft       = actual frequencies of fft
%           f_plot      = which frequencies to plot
%       wavelets    = cell containing morlet wavelets for each frequency of cfg.foi_osc
%       plt         = 'y' or 'n' to plot data
%
%   Outputs (each organized within cells representing source of
%           LF phase-modulating data in row index &
%           HF amplitude-modulated data in column index):
%       zscore        = the z-score of the MS of raw data vs surrogates
%       pval          = the corresponding p-value.
%       h             = the result of the z-test: 1 if the null
%                           hypothesis is rejected, 0 if it is not
%       MS            = modulation strength from Dvorak & Fenton, J. Neurosci. Methods 2014
%       phasepref     = the "preferred" phase expressed in degrees
%                           relative to delta/theta peak
%       pwrN          = 2 cells containing normalized wavelet-based
%                           spectrograms of data from each region
%       and 4 plots of the 2d z-score transformed modulation strength.
%
%   DEPENDENCIES:
%       sig_OTC_TEM

%% check inputs and set up other variables

if nargin < 4 || isempty(plt);  plt = 'n';  end

[m,n] = size(trdata);
if m~=2
    error('Data must be a row vector (1xN)');
end

if any(~isfield(cfg,{'foi_mod','foi_osc','Fs','wFactor','powerTh',...
        'winLen','nfft','f_fft','f_plot'}))
    error('Check help for list of required cfg fields');
end

zscore      = cell(2);
pval        = cell(2);
h           = cell(2);
modS        = cell(2);
otc         = cell(2);
fft_otc     = cell(2);
pwrN        = cell(2,1);    % z-score power for each region
osc         = cell(2,1);    % samples and frequencies for each peak
samples = cell(2,numel(cfg.foi_mod)); % samples for each peak by frequency
alpha = 0.05/length(cfg.foi_mod); % corrected for multiple comparisons

%% normalize data

for a = 1:2
    trdata(a,:) = trdata(a,:)/std(trdata(a,:));
end

%% store samples and frequencies of high power oscillations

% compute nomalized power across bands
for bI = 1:numel(cfg.foi_osc)
    psi = wavelets{bI};
    N = round((length(psi)-1)/2);
    
    for a = 1:2
        c = conv(trdata(a,:),psi); % convolution
        c = c(N+1:N+n);
        power = (abs(c)).^2;
        pwrN{a}(bI,1:n) = power/std(power); % normalized power
    end
end

% figure;
% imagesc([],cfg.foi_osc,pwrN{2});
% axis xy tight
% colormap(jet); colorbar;

%% find local maxima in normalized power profile

for a = 1:2
    bw = imregionalmax(pwrN{a});
    [y,x] = find(bw==1);
    pN = pwrN{a}(bw);
    
    % extract powers higher than threshold not on the border
    k = pN > cfg.powerTh & x > 1 & x < n & y > 1 & y < length(cfg.foi_osc);
    x = x(k);
    y = y(k);
    
    % convert to samples and frequencies
    freq = cfg.foi_osc(y);
    
    osc{a} = cat(2,x,freq');
end

%% for each band, extract relevant samples from each area

for bI = 1:length(cfg.foi_mod)
    f = cfg.foi_mod(bI);
    stB = cfg.foi_mod(bI)-f/cfg.wFactor;    % bandwidth proportional to frequency
    edB = cfg.foi_mod(bI)+f/cfg.wFactor;
    
    for a = 1:2
        %% find samples at specific frequency
        k = osc{a}(:,2) >= stB & osc{a}(:,2) < edB;
        samp = osc{a}(k,1);
        samples{a,bI} = samp(samp > cfg.winLen & samp < n-cfg.winLen);
    end
end

%% calculate modulation strength across regions

ho = waitbar(0,'Calculating OTC across regions...');
for bI=1:length(cfg.foi_mod)
    waitbar(bI/length(cfg.foi_mod),ho,['Calculating OTC across regions for '...
        num2str(cfg.foi_mod(bI)) ' Hz,']);
    for LFPM = 1:2     % low frequency phase-modulating data source
        for HFAM = 1:2 % high frequency amplitude-modulated data source
            if isempty(samples{HFAM,bI})
                zscore{LFPM,HFAM}(bI) = NaN;
                pval{LFPM,HFAM}(bI) = NaN;
                h{LFPM,HFAM}(bI) = false;
                modS{LFPM,HFAM}(bI) = NaN;
                otc{LFPM,HFAM}(bI,1:1200) = NaN;
            else
            % Compute & test significance of the modulation strength.
            [zscore{LFPM,HFAM}(bI),pval{LFPM,HFAM}(bI),...
                h{LFPM,HFAM}(bI),modS{LFPM,HFAM}(bI),...
                otc{LFPM,HFAM}(bI,:)] = sig_OTCave_TEM(...
                samples{HFAM,bI},trdata(LFPM,:),cfg.winLen,alpha);
            end
        end
    end
end
close(ho)

%% fft of comodulogram

for LFPM = 1:2      % low frequency phase-modulating data source
    for HFAM = 1:2  % high frequency amplitude-modulated data source
        fft_otc{LFPM,HFAM} = fft(otc{LFPM,HFAM},cfg.nfft,2);
    end
end

%% plots

if plt == 'y'
    for LFPM = 1:2      % low frequency phase-modulating data source
        for HFAM = 1:2  % high frequency amplitude-modulated data source
            % Plot the oscillation-triggered comodulogram
            figure(LFPM+2*(HFAM-1));
            h1 = subplot(1,5,1:3); hold on;
            imagesc([],cfg.foi_mod,otc{LFPM,HFAM});
            plot([cfg.winLen,cfg.winLen],[cfg.foi_mod(1),...
                cfg.foi_mod(end)],'w','LineWidth',2);
            axis xy tight
            xlabel('Time (ms)');
            ylabel('Frequency (Hz)');
            title('Single Trial OTC');
            colormap(jet); colorbar;
            
            h2 = subplot(1,5,4); hold on;
            plot(zscore{LFPM,HFAM},cfg.foi_mod,'k');
            plot(zscore{LFPM,HFAM}(h{LFPM,HFAM}),...
                cfg.foi_mod(h{LFPM,HFAM}),'ro');
            axis xy tight
            xlabel('Normalized Modulation Strength');
            
            h3 = subplot(1,5,5);
            imagesc(cfg.f_fft(cfg.f_plot),cfg.foi_mod,2*abs(fft_otc{LFPM,HFAM}(:,cfg.f_plot)));
            axis xy tight
            colormap(jet); colorbar;
            title('FFT of OTCG');
            xlabel('Frequency (Hz)');
        end
    end
end
end
