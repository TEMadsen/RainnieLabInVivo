function [MI,t,Z,P,H,PP]=KL_MIvTime_TEM(data,movingwin,Pf,Af,fig)
%%   Cross-frequency phase-amplitude modulation over time, 
%       using Kullback�Leibler modulation index from Tort et al., 2010,
%       and computing its significance using the shuffling procedue from 
%           Hentschke et al, 2007.
%   by Teresa E. Madsen 10/7/12
%
%   Usage:
%       [MI,t,Z,P,H,PP]=KL_MIvTime_TEM(data,movingwin,Pf,Af,fig);
% 
%   Input:
%       data    = unfiltered time series data as a row vector (1xN)
%       movingwin   = in seconds, form [window winstep] - window must be at 
%                   least as long as the period of the lowest frequency 
%                   used (i.e., movingwin(1) > 1/Pf(1))
%       Pf  = frequency range for phase info in the form [min max] (Hz)
%       Af  = frequency range for amplitude info in the form [min max]
%       fig = 'y'/'n' to creating a figure display of the result
%
%   Output:
%       MI  = Kullback�Leibler modulation index from Tort et al., 2010
%       t   = times
%       Z   = z-scores vs. 100 shuffled surrogates
%       P   = p-values vs. 100 shuffled surrogates
%       H   = significant (1) or not (0) vs. 100 shuffled surrogates
%       PP  = preferred phase (highest gamma amplitude)

[M,N] = size(data);
if M ~= 1
    error('Data must be a row vector (1xN)');
end

if movingwin(1) < 1/Pf(1);
    error('Data window must be longer than period of lowest frequency');
end

Fs = 1000;          % hardcoded to assume Plexon's LFP sampling frequency
pad = [0 Fs];       % add 1 second of 0s to each end of signal
data = padarray(detrend(data),pad);

PhaseFreq = eegfilt(data,Fs,Pf(1),Pf(2));
Phase = angle(hilbert(PhaseFreq));      % phase time series
Phase = Phase(1+pad(2):end-pad(2));     % Remove padding.
AmpFreq = eegfilt(data,Fs,Af(1),Af(2));
Amp = abs(hilbert(AmpFreq));            % amplitude envelope time series
Amp = Amp(1+pad(2):end-pad(2));         % Remove padding.

Nwin = round(Fs*movingwin(1));    % number of samples in window
Nstep = round(movingwin(2)*Fs);   % number of samples to step through
winstart = 1:Nstep:N-Nwin+1;
nw = length(winstart);
MI = zeros(nw,1);
Z = zeros(nw,1);
P = zeros(nw,1);
H = zeros(nw,1);
PP = zeros(nw,1);
winmid = winstart+round(Nwin/2);
t = winmid/Fs;
wb = waitbar(0,'CFC calculation progress:');

for n = 1:nw    % parallelized in sig_KL_MI_TEM.m
    waitbar(n/nw,wb);
    indx = winstart(n):(winstart(n)+Nwin-1);
    Phasewin = Phase(indx);
    Ampwin = Amp(indx);
    [zscore,pval,h,mi,phasepref] = sig_KL_MI_TEM(Ampwin,Phasewin);
    if isnan(mi)
        error('Modulation Index = NaN ???')
    end
    MI(n) = mi;
    Z(n) = zscore;
    P(n) = pval;
    H(n) = h;
    PP(n) = phasepref;
end
delete(wb);

if fig == 'y'
    gcf; AX = plotyy(t,MI,t,Z); title('KL-MI over Time');
    xlabel('Time (seconds)'); 
    set(get(AX(1),'Ylabel'),'String','Modulation Index');
    set(get(AX(2),'Ylabel'),'String','Z-score');
    axes(AX(2)); hold on; 
    line([min(t) max(t)],[min(abs(Z(H==1))),min(abs(Z(H==1)))],...
        'Color','r');
end
end
