function maintainGit_repoOwner
% MAINTAINGIT_REPOOWNER is only for the repo owner to run!
%   offers to merge main development (i.e., current) branch into master, then
%   merges master into all other branches (as long as they are present both
%   locally & on the repo server)

%% suppress warning caused by editing this function while it's running

w = warning;  % save current warnings to reinstate at end of function
warning('off','MATLAB:lang:cannotClearExecutingFunction')

%% fetch all & merge remote changes into current branch
% On a Windows machine, "error: cannot stat 'filename': Permission denied"
% may indicate that you have made local changes to the folder structure
% that can't be updated because the folder is open in Windows Explorer or
% Matlab.  Try closing both, then run the needed commands (push/pull, etc.)
% in Git Bash before reopening Matlab and rerunning this script.

% change directory to general code folder
fprintf('\n          UPDATING RAINNIELABINVIVO\n')
cd([fileparts(userpath) filesep 'GitHub' filesep 'RainnieLabInVivo'])

% check status of current branch
disp('   running "git status"')
[cmdout, statout] = git('status');
disp([cmdout newline])
if statout ~= 0
  keyboard
end
if ~(contains(cmdout,'nothing to commit, working tree clean') || ...
    contains(cmdout,'nothing to commit, working directory clean'))
  error('all changes to current branch must be committed first')
end
assert(startsWith(cmdout,'On branch '))
% name of current branch, assumed to be repo owner's primary development branch
devbranch = cmdout(11:strfind(cmdout,newline)-1);

% fetch all remote changes, merging only into the current branch (assumed to be
% the primary development branch, so take time to look closely at conflicts)
disp('   running "git pull --all -v -s recursive -Xpatience"')
[cmdout, statout] = git('pull --all -v -s recursive -Xpatience');
disp([cmdout newline])
if statout ~= 0
  keyboard
end

%% figure out which branches need updating

% list all local branches with their remote counterparts, and all remote
% branches, even if not matched locally
disp('   running "git branch -avv"')
[cmdout, statout] = git('branch -avv');
disp([cmdout newline])
if statout ~= 0
  keyboard
end

% parse cmdout into table of branches (local & remote names), marked with
% current (*), needs pull (behind), needs push (ahead)
cmdoutchar = char(splitlines(cmdout));
colI = [0 find(all(isspace(cmdoutchar),1),2,'first') size(cmdoutchar,2)];
cmdoutstr = strip(string(mat2cell(cmdoutchar, ...   % splits character matrix into 3 columns of cells
  ones(1,size(cmdoutchar,1)), diff(colI))));  % then converts to string array, removing leading & trailing spaces
assert(strcmp(devbranch,cmdoutstr(startsWith(cmdoutstr(:,1),'*'),2))) % verify current branch name
branchname = cmdoutstr(:,2);
remotename = string(zeros(size(branchname)));
needspull = false(size(branchname));
needspush = false(size(branchname));
omit = false(size(branchname));   % omit matched remotes from table
for lbn = 1:numel(branchname)
  if ~contains(branchname(lbn),'remotes/')  % if not a remote branch
    [message,delim] = split(cmdoutstr(lbn,3),{'[',': ',']'});
    if isempty(delim)
      remotename(lbn) = "";
      needspush(lbn) = true;
    else
      assert(strcmp(delim(1),'['))
      remotename(lbn) = branchname(endsWith(branchname,message(2)));
      if strcmp(delim(2),': ')
        if contains(message(3),'behind')
          needspull(lbn) = true;
        end
        if contains(message(3),'ahead')
          needspush(lbn) = true;
        end
      end
    end
  else
    if ~any(strcmp(remotename,branchname(lbn)))
      remotename(lbn) = branchname(lbn);
      branchname(lbn) = "";
      needspull(lbn) = true;
    else
      omit(lbn) = true;
    end
  end
end
branchT = table(branchname(~omit),remotename(~omit),needspull(~omit),needspush(~omit), ...
  'VariableNames',{'LocalBranch','RemoteBranch','NeedsPull','NeedsPush'});

%% if master branch needs pull, do so, then update devbranch

devI = strcmp(branchT.LocalBranch,devbranch);
masterI = strcmp(branchT.LocalBranch,'master');

if branchT.NeedsPull(masterI)
  % more robust to conflicts if it's checked out
  disp('   running "git checkout master"')
  [cmdout, statout] = git('checkout master');
  disp([cmdout newline])
  if statout ~= 0
    keyboard
  end
  % update local master branch with changes made on another computer
  disp('   running "git pull -v -s recursive -Xpatience"')
  [cmdout, statout] = git('pull -v -s recursive -Xpatience');
  disp([cmdout newline])
  if statout ~= 0
    keyboard
  end
  branchT.NeedsPull(masterI) = false;
  % if master branch requires push, do so
  if branchT.NeedsPush(masterI)
    disp('   running "git push -v"')
    [cmdout, statout] = git('push -v');
    disp([cmdout newline])
    if statout ~= 0
      keyboard
    end
    branchT.NeedsPush(masterI) = false;
  end
  
  % then return to devbranch
  disp(['   running "git checkout ' devbranch '"'])
  [cmdout, statout] = git(['checkout ' devbranch]);
  disp([cmdout newline])
  if statout ~= 0
    keyboard
  end
end

%% update current WIP branch with any changes made to the master branch

disp('   running "git merge master -v -s recursive -Xpatience"')
[cmdout, statout] = git('merge master -v -s recursive -Xpatience');
disp([cmdout newline])
if statout ~= 0
  keyboard
end
disp('   running "git push -v"')
[cmdout, statout] = git('push -v');
disp([cmdout newline])
if statout ~= 0
  keyboard
end
branchT.NeedsPush(devI) = false;

%% if master branch requires push, do so before merging devbranch

if branchT.NeedsPush(masterI)
  disp('   running "git push origin master -v"')
  [cmdout, statout] = git('push origin master -v');
  disp([cmdout newline])
  if statout ~= 0
    keyboard
  end
  branchT.NeedsPush(masterI) = false;
end

%% optionally merge primary development branch into master

if ~strcmp(devbranch,'master')
  button = questdlg(['Would you like to merge the changes from ' devbranch ...
    ' into master before merging master into all other branches?'], ...
    'Is the current branch ready to share?','Yes','No','No');
  if strcmp(button,'Yes')
    disp('   running "git checkout master"')
    [cmdout, statout] = git('checkout master');
    disp([cmdout newline])
    if statout ~= 0
      keyboard
    end
    % take time to look closely at conflicts
    disp(['   running "git merge ' devbranch ' -v -s recursive -Xpatience"'])
    [cmdout, statout] = git(['merge ' devbranch ' -v -s recursive -Xpatience']);
    disp([cmdout newline])
    if statout ~= 0
      keyboard
    end
    disp('   running "git push -v"')
    [cmdout, statout] = git('push -v');
    disp([cmdout newline])
    if statout ~= 0
      keyboard
    end
    branchT.NeedsPush(masterI) = false;
  end
end

%% loop through remaining branches, pulling if needed & merging master into each

for lbn = find(~devI & ~masterI)'
  if strcmp(branchT.LocalBranch(lbn),'')  % isempty doesn't work with string
    warning(['Skipping unmatched branch: ' char(branchT.RemoteBranch(lbn))])
  elseif strcmp(branchT.RemoteBranch(lbn),'')
    warning(['Skipping unmatched branch: ' char(branchT.LocalBranch(lbn))])
  else
    disp(['   running "git checkout ' char(branchT.LocalBranch(lbn)) '"'])
    [cmdout, statout] = git(['checkout ' char(branchT.LocalBranch(lbn))]);
    disp([cmdout newline])
    if statout ~= 0
      keyboard
    end
    if branchT.NeedsPull(lbn)
      disp('   running "git pull -v"')
      [cmdout, statout] = git('pull -v');
      disp([cmdout newline])
      if statout ~= 0
        keyboard
      end
    end
    % prefer changes made in master when they conflict with this branch
    disp('   running "git merge master -v -s recursive -Xtheirs"')
    [cmdout, statout] = git('merge master -v -s recursive -Xtheirs');
    disp([cmdout newline])
    if statout ~= 0
      keyboard  % may need to "git merge --abort" before dbcont
    end
  end
end

%% go back to devbranch & push all changes

disp(['   running "git checkout ' devbranch '"'])
[cmdout, statout] = git(['checkout ' devbranch]);
disp([cmdout newline])
if statout ~= 0
  keyboard
end

% send changes on all branches to GitLab
disp('   running "git push origin --all -v"')
[cmdout, statout] = git('push origin --all -v');
disp([cmdout newline])
if statout ~= 0
  keyboard
end

% check status of current branch
disp('   running "git status"')
[cmdout, statout] = git('status');
disp([cmdout newline])
if statout ~= 0
  keyboard
end

%% Make sure latest versions of maintainGit & setupGit are on the server

serverdir = [tserver 'Software' filesep 'Git' filesep 'userdir' filesep];

if ~isfolder(serverdir)
  warning('You may not be connected to the S drive.  Skipping file upload.')
else
  [status,message] = copyfile('setupGit.m', serverdir(1:end-8));
  if status
    disp(['Successfully copied ''setupGit.m'' to ' serverdir(1:end-8)])
  else
    error(message)
  end
  
  [status,message] = copyfile('maintainGit.m', serverdir(1:end-8));
  if status
    disp(['Successfully copied ''maintainGit.m'' to ' serverdir(1:end-8)])
  else
    error(message)
  end
end

%% reinstate original warnings

warning(w)

end   % function