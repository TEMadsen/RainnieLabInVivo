%% info needed for analyzing fear data, including all rats filenames
%#ok<*SNASGU> just provides background info for other analysis protocols

datafolder = 'S:\Teresa\Analyses\TempData\';

% put new figures in a folder named by date, in the format 'yyyy-mm-dd' 
figurefolder = ['S:\Teresa\Figures\TempFigures\' ...
  datestr(now,29) '\']; 
if ~exist(figurefolder,'dir')
  mkdir(figurefolder)
end

% each row # here corresponds to cell row (1st dim) in other variables
ratnums = [361; 362; 363; 364];
excluded = logical([0; 0; 0; 0]);   % see notes for why
side = {'Right';'left';'left';'Right'};
chNAc = cell(numel(ratnums),1);    % best channel to use for CFC
chBLA = cell(numel(ratnums),1);     % best channel to use for CFC
finalch = false([numel(ratnums),1]);
group = {'VPA';'saline'}
ratT = table(ratnums, excluded, side, group, chNAc, chBLA, finalch);
clearvars ratnums excluded side chNAc chBLA finalch group

% each column # here corresponds to cell column (2nd dim) in other variables


phases = {'Conditioning', 'Recall', 'Extinction'};  % add 'Baseline',

abbr = {'RC1', 'RC2', 'RC3', 'RC4', 'RC5', 'RC6', 'RC7', ...
    'Ext1', 'Ext2'};  % add 'BL', % for behavioral data

blocks = {'Early Conditioning', 'Intermediate Conditioning', ...
    'Late Conditioning', 'Early Extinction', 'Late Extinction'};

% each row # here corresponds to the numerical code that will be saved in
% trl and data.trialinfo to identify which subtrials correspond to which
% interval types
intervals = {'Before'; 'During'; 'After'};

% each row # here corresponds to the numerical code that will represent
% which region's data is being used for phase or amplitude source data
regions = {'NAc'; 'BLA'};

% NAc if implanted on right, BLA if implanted on left
lftchs = {'AD01','AD02','AD03','AD04','AD05','AD06','AD07','AD08'};

% BLA if implanted on right, NAc if implanted on left
rtchs = {'AD09','AD10','AD11','AD12','AD13','AD14','AD15','AD16'};

% initialize variables
goodchs = cell(size(ratT,1),1);  % cell array of strings can't be contained within one cell of a table
notes = cell(size(ratT,1),numel(phases));
toneTS = cell(size(ratT,1),numel(phases));
nexfile = cell(size(ratT,1),numel(phases));

% any extra comments about each rat
notes{1,3} = 'lost tone timestamp from original recordings but retrieved';

% channels to use for each rat, defaults to 'AD*' if empty
goodchs{1} = {'AD*'};  % all AD channels except...
% ratT.chNAc(1) = {'AD05'};
% ratT.chBLA(1) = {'AD11'};
% ratT.finalch(1) = true;
goodchs{2} = {'AD*'};  % all AD channels except...
% ratT.chNAc(2) = {'AD05'};
% ratT.chBLA(2) = {'AD14'};
% ratT.finalch(2) = true;
goodchs{3} = {'AD*'};  % all AD channels except...
% ratT.chNAc(3) = {'AD03'};
% ratT.chBLA(3) = {'AD14'};
% ratT.finalch(3) = true;
goodchs{4} = {'AD*'};  % all AD channels except...
% ratT.chNAc(4) = {'AD07'};
% ratT.chBLA(4) = {'AD10'};
% ratT.finalch(4) = true;


%% filenames and invalidated trials
% RC1
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\200\200_091003_0000-1mrg-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\202\202_091005_0002-aligned.nex';
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\203\203_091204_0001-alignedLFPs.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\204\204_100313_0119-aligned.nex';

% RC2
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\200\200_091003_0000-1mrg-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\202\202_091005_0002-aligned.nex';
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\203\203_091204_0001-alignedLFPs.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\204\204_100313_0119-aligned.nex';

% RC1
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\200\200_091003_0000-1mrg-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\202\202_091005_0002-aligned.nex';
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\203\203_091204_0001-alignedLFPs.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\204\204_100313_0119-aligned.nex';

% RC1
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\200\200_091003_0000-1mrg-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\202\202_091005_0002-aligned.nex';
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\203\203_091204_0001-alignedLFPs.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\204\204_100313_0119-aligned.nex';

% RC1
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\200\200_091003_0000-1mrg-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\202\202_091005_0002-aligned.nex';
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\203\203_091204_0001-alignedLFPs.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\204\204_100313_0119-aligned.nex';

% RC1
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\200\200_091003_0000-1mrg-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\202\202_091005_0002-aligned.nex';
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\203\203_091204_0001-alignedLFPs.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\204\204_100313_0119-aligned.nex';

% RC1
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\200\200_091003_0000-1mrg-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\202\202_091005_0002-aligned.nex';
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\203\203_091204_0001-alignedLFPs.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\204\204_100313_0119-aligned.nex';

% RC1
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\200\200_091003_0000-1mrg-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\202\202_091005_0002-aligned.nex';
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\203\203_091204_0001-alignedLFPs.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\204\204_100313_0119-aligned.nex';

%% update FieldTrip

% change directory to local copy of FieldTrip
cd 'C:\Users\tmadsen\gitSandbox\fieldtrip'

% Switch to master branch and update the working directory
!git checkout master

% update local master branch to the latest official version of FieldTrip
!git pull upstream master  

% update personal forked copy on GitHub (aka remote repository)
!git push origin master  

% remind yourself if you had any works in progress
disp('Available branches (* indicates currently active branch):');
!git branch

% change directory back to general code folder
cd 'C:\Users\tmadsen\gitSandbox\RainnieLabInVivo'

% Switch to master branch and update the working directory
!git checkout master

% update personal repo on GitLab (aka remote repository)
!git push origin master  

% remind yourself if you had any works in progress
disp('Available branches (* indicates currently active branch):');
!git branch
