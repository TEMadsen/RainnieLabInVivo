function h = plot_vector_TEM(X,f,plt,Xerr,c,w,smW)
% Function to plot a frequency dependent vector X. If error bars are
% specified in Xerr, it also plots them. Xerr can either contain upper and
% lower confidence intervals on X (plotted as shaded area), or simply a
% theoretical confidence level (for the coherence). Used to plot the
% spectrum and coherency.  Can smooth over bandwidth if desired.
%
%   ~Modified from Chronux by TEM~
%
% Usage: plot_vector_TEM(X,f,plt,Xerr,c,w,smW)
%
% INPUT
% X: input vector as a function of frequency (f) or matrix with frequency
%     bins as rows and channels/trials as columns
% f: f axis grid for plot. Default [1:size(X,1)]
% plt: 'l' for log, 'n' for no log.
% Xerr: lower and upper confidence intervals for X1 - lower/upper by
%       frequencies (by channels/trials). Or simply a single number
%       specifying an f-independent confidence level, which will be drawn
%       as a black dashed line.
% c: controls the color of the plot - input 'b','g','r' etc. or [] to
%                                     rotate through default colors
% w: controls the width of the lines - input 1, 1.5, 2 etc
% smW: if smoothing is desired, input bandwidth (W = params.tapers(1)/T;)
%
% OUTPUT
% h: handle for figure axis containing plot
%

if nargin < 1;                          error('Need data');     end;

[N,C] = size(X);
if N == 1 && C ~= 1;                    X=X';   N=C;    C=1;    end;
if nargin < 2 || isempty(f);            f=1:N;                  end;
if length(f)~=N;
  error('frequencies and data have incompatible lengths');
end;

if nargin < 3 || isempty(plt);          plt='l';                end;
if nargin < 4 || isempty(Xerr);         Xerr=[];                end;
if strcmp(plt,'l');                     X=10*log10(X);
  if nargin >=4 && ~isempty(Xerr);      Xerr=10*log10(Xerr);    end;
end;

h = gca;  % creates a figure axis if none exists
if nargin < 5 || isempty(c);
  defcolor = h.ColorOrder;              c = defcolor(1,:);
end;

if nargin < 6 || isempty(w);            w=2;                    end;
if nargin == 7
  smf = ceil(smW/diff(f(1:2))/2)*2-1;   % odd # of freq bins to smooth over
  for ch = 1:C
    X(:,ch) = smooth(X(:,ch), smf);
    if ~isempty(Xerr)
      Xerr(1,:,ch) = smooth(Xerr(1,:,ch), smf);
      Xerr(2,:,ch) = smooth(Xerr(2,:,ch), smf);
    end
  end
end

for ch = 1:C
  if exist('defcolor','var');     c = defcolor(ch,:);     end;
  plot(f, X(:,ch), 'Color',c, 'Linewidth',w);
  hold all;
end

if ~isempty(Xerr);
  if length(Xerr) == 1;
    line(get(h,'xlim'), [Xerr,Xerr], 'Color','k', 'LineStyle','--');
  else  % uses shaded area to denote error range for each channel/trial
    for ch=1:C
      if exist('defcolor','var');     c = defcolor(ch,:);     end;
      ShErr(1:length(f)) = Xerr(1,:,ch);  % lower error bound forwards
      ShErr((length(f)+1):(2*length(f))) = flip(Xerr(2,:,ch));  % upper error bound backwards
      ShF(1:length(f)) = f;   % forwards frequency axis for lower error
      ShF((length(f)+1):(2*length(f))) = flip(f);   % backwards freq axis for upper error
      hErr = fill(h, ShF, ShErr, zeros(1,length(ShF)), 'FaceColor',c, ...
        'FaceAlpha',0.3/C, 'LineStyle','none');
      hErr.Annotation.LegendInformation.IconDisplayStyle = 'off';
    end
  end
end

axis tight
set(h, 'FontName','Arial', 'Fontsize',10);
xlabel('Frequency (Hz)', 'FontSize',12);
if plt == 'n'
  ylabel('Coherence', 'FontSize',12);
else
  ylabel('Log Power (dB)', 'FontSize',12);
end

end   % of function