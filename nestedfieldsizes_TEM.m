function [bytes, names, largest] = nestedfieldsizes_TEM(data, limit, expected)
% NESTEDFIELDSIZES_TEM tracks down the largest components of a nested structure
% or cells, e.g., for use with FieldTrip data with many nested cfg.previous's.
% Can stop when a limited number of levels is reached, or the first time the
% largest field has an unexpected fieldname.
%
% written 4/26/17 by Teresa E. Madsen, Ph.D.

if nargin < 2 || isempty(limit)
  limit = 1000;
end

if nargin < 3   % leave empty to continue evaluating regardless of fieldname
  expected = {'.cfg','.previous'};
end

bytes = nan(limit,1);
names = cell(limit,1);

for level = 1:limit
  if isstruct(data)
    fields = fieldnames(data);
    for f = 1:numel(fields)
      names{level,f} = ['.' fields{f}];
      tmp = data.(fields{f});   %#ok<NASGU> passed as string
      S = whos('tmp');
      bytes(level,f) = S.bytes;
    end
    [bytes(level,1:numel(fields)),order] = sort(bytes(level,1:numel(fields)),'descend');
    names(level,1:numel(fields)) = names(level,order);
    data = data.(names{level,1}(2:end));
    if ~isempty(expected) && ~ismember(names{level,1}, expected)
      break
    end
  elseif iscell(data)
    for c = 1:numel(data)
      names{level,c} = ['{' num2str(c) '}'];
      tmp = data{c};            %#ok<NASGU> passed as string
      S = whos('tmp');
      bytes(level,c) = S.bytes;
    end
    [bytes(level,1:numel(data)),order] = sort(bytes(level,1:numel(data)),'descend');
    names(level,1:numel(data)) = names(level,order);
    data = data{str2double(names{level,1}(2:end-1))};
  else
    break
  end
end

if isnan(bytes(level,1))
  level = level - 1;
end

bytes = bytes(1:level,:);
names = names(1:level,:);

largest = ['data' names{:,1}];
disp(['The largest nested subfields are ' largest])
largest = data;
whos('largest')
      
end
