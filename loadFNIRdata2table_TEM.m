function [dataT] = loadFNIRdata2table_TEM(mfilein,tfileout)
%% LOADFNIRDATA2TABLE_TEM loads Katie's FNIR data and exports it as a table
%
%   EXAMPLE:
%     [dataT] = loadFNIRdata2table_TEM( ...
%       'C:\Users\Teresa\Documents\Work\fS_Exported.m', ...
%       ['S:\Katie\KatiesFNIRdata' datestr(now,30) '.xlsx']);
%
%   INPUTS:
%     mfilein   = string representing full path and filename for the m-file that
%                 Katie exports (or just filename, if it's in the current
%                 working directory), will be run as a script, so the last part
%                 of the filename can't contain any characters other than
%                 letters, numbers, and underscores (.m is optional)
%     tfileout  = (optional if output variable is given instead) full path and
%                 filename for the output file where the table should be written
%                 (or just filename, if path is the same as input), should
%                 include extension (probably .xlsx, but see help for writetable
%                 for other options)
%
%   OUTPUT:
%     dataT     = (optional if tfileout is given as input) output table written
%                 to tfileout
%
% written 11/17/17 by Teresa E. Madsen, Ph.D.

%% check inputs & outputs

if nargin < 2
  tfileout = '';
end

if isempty(tfileout)    % won't save output table to a file,
  assert(nargout == 1)  % so make sure it at least goes back to the workspace
end

assert(ischar(mfilein) & ischar(tfileout))

%% parse mfilein

lastsep = find(mfilein == filesep,1,'last');
mdir = mfilein(1:lastsep);
mfilename = mfilein(lastsep+1:end);
if any(mfilename == '.')
  mfilename = mfilename(1:find(mfilename == '.',1,'first')-1);
end

%% if mfilein isn't on matlab path, cd to its directory

if ~contains(path,mdir)
  cd(mdir)
end

%% clear unnecessary variables, then run exported .m file

disp(['running ' mfilein])

clearvars -except mfilename tfileout

eval(mfilename)

%% collect variable info & identify data matrices

vars = whos;  % all variables in workspace
numv = find(strcmp('double',{vars(:).class}));  % row #s for numerical data
sizs = vertcat(vars(numv).size);  % sizes of numerical variables
isdm = sizs(:,2) > 1;   % t/f: is data matrix (of numerical variables)
matv = numv(isdm);   % row #s of matrix variables (data)

% if sum(sizs(isdm,1)) ~= sum(sizs(~isdm,1))
%   error('number of data & time points are not equal')
% end

%% loop through each data variable, creating table

dataT = table;

for dm = matv   % for each variable representing a data matrix
  dmname = vars(dm).name;     % variable name for this data matrix
  eval(['label0 = ' dmname '_label0; label1 = ' dmname ...
    '_label1; label2 = ' dmname '_label2;']);   % corresponding labels
  dmtimv = strrep(dmname, 'Block', 'Time');     % variable name for time
  eval(['time = ' dmtimv ';']);   % timestamps for each data point
  eval(['opt = ' dmname ';']);   % optode data matrix
  if length(time) ~= length(opt)
    error('number of data & time points are not equal')
  end
  dataT = [dataT; repmat(cell2table({dmname,label0,label1,label2}),size(time)) ...
    table(time) array2table(opt)];
end   % for each data matrix

%% export table to Excel format

if isempty(tfileout)
  warning('output not saved to file')
else
  disp(['saving dataT to ' tfileout])
  writetable(dataT,tfileout)
end

%% clear output if not needed

if nargout < 1
  clearvars
end

end   % function