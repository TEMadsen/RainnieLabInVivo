function plot_matrix_TEM(X,t,f,plt,Xerr)
% Function to plot a time-frequency matrix X (as output from Chronux).
%
% Time and frequency axes are in t and f. If upper and lower confidence
% intervals for X are specified in Xerr, it also plots them.
%
%   Usage: plot_matrix_TEM(X,t,f,plt,Xerr) 
%
%   Inputs: 
%     X: input matrix as a function of time and frequency (t x f) 
%     t: t axis grid for plot. Default [1:size(X,1)] 
%     f: f axis grid for plot. Default. [1:size(X,2)] 
%     plt: 'l' for log, 'n' for no log. 
%     Xerr: lower and upper confidence intervals for X 
%           size(Xerr) == [2,numel(t),numel(f)]
%

if nargin < 1;                      error('Need data');                 end
[NT,NF,e]=size(X);
if e > 1;   error('plot each trial separately or turn on trialave');    end
if nargin < 2;                      t=1:NT;                             end
if nargin < 3;                      f=1:NF;                             end
if length(f)~=NF || length(t)~=NT;
  error('axes grid and data have incompatible lengths');
end;
if nargin < 4 || isempty(plt);      plt='l';                            end
if strcmp(plt,'l');
  X=10*log10(X);
  if nargin == 5;                 Xerr=10*log10(Xerr);                end
end;

if nargin < 5;
  imagesc(t,f,X'); axis xy; colorbar; colormap(jet);  % title('-ogram','FontSize',20);
else
  subplot(311); imagesc(t,f,squeeze(Xerr(1,:,:))');
  axis xy; colorbar; colormap(jet); title('Lower confidence');
  
  subplot(312); imagesc(t,f,X'); title('X'); axis xy; colorbar; colormap(jet);
  
  subplot(313); imagesc(t,f,squeeze(Xerr(2,:,:))');
  axis xy; colorbar; colormap(jet); title('Upper confidence');
end;

set(gca,'FontName','Arial','Fontsize',10);
xlabel('Time (Seconds)','FontSize',12);
ylabel('Frequency (Hz)','FontSize',12);