%% demo FieldTrip artifact rejection for Liz Ann

load('F:\Teresa_20170414\Chan1_dc.mat')
load('F:\Teresa_20170414\Chan5_dc.mat')
load('F:\Teresa_20170414\Chan9_dc.mat')
load('F:\Teresa_20170414\dataStructAndParams.mat')

%% structure data for FieldTrip

data = struct();
data.trial={[Chan1_dc; Chan5_dc; Chan9_dc]};
data.label={'Chan1','Chan5','Chan9'};
data.time={(1:numel(Chan1_dc))/samplerate_dc};

%% set params

cfg = struct();
cfg.interactsubj   = true;
cfg.interactch     = true;

cfg.artfctdef.clip.channel          = 'all';
cfg.artfctdef.clip.pretim           = 0.1;
cfg.artfctdef.clip.psttim           = 0.1;
cfg.artfctdef.clip.timethreshold    = 0.05;     % s
cfg.artfctdef.clip.amplthreshold    = 3e-6;     % 1000's of Volts???

cfg.artfctdef.zvalue.channel        = 'all';
cfg.artfctdef.zvalue.cutoff         = 4;  % you might want to experiment with lower values
cfg.artfctdef.zvalue.trlpadding     = 0;
cfg.artfctdef.zvalue.fltpadding     = 0;
cfg.artfctdef.zvalue.artpadding     = 0.1;
cfg.artfctdef.zvalue.rectify        = 'yes';

cfg.artfctdef.minaccepttim  = 0.1;

%% reject artifacts

[clean] = AutoArtReject_TEM(cfg,data);
