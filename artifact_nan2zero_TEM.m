function [artifact,data,badtimes] = artifact_nan2zero_TEM(data)
% ARTIFACT_NAN2ZERO_TEM marks NaNs that occur uniformly across all channels
%   as artifacts, taking FT format data structure & returning the same
%   format as ft_artifact_xxx, for input to ft_rejectartifact.  Non-uniform
%   NaNs (those not present on all channels) are replaced with 0s to avoid
%   breaking analysis functions.  Also returns times of replaced NaNs by
%   trial & channel, so they can be changed back to NaNs in freq output.
%
% written 3/2/17 by Teresa E. Madsen

artifact = [];
badtimes = cell(numel(data.trial), numel(data.label));

for tr = 1:numel(data.trial)
  % find NaNs to mark as artifacts (present uniformly across all channels)
  trlnan = isnan(data.trial{tr});   % identify NaNs by channel & timepoint
  allnan = all(trlnan,1);   % need to specify dim in case of single channel
  
  % find, save timepoints, & replace non-uniform NaNs (not on all chs) w/ 0
  replacenan = trlnan & repmat(~allnan,size(trlnan,1),1);
  for ch = 1:numel(data.label)
    badtimes{tr,ch} = data.time{tr}(replacenan(ch,:));  % ID before replacing
  end
  data.trial{tr}(replacenan) = 0;   % replace these w/ 0s
  
  if any(allnan)
    % determine the file sample #s for this trial
    trsamp = data.sampleinfo(tr,1):data.sampleinfo(tr,2);
    
    while any(allnan)
      % start from the end so sample #s don't shift
      endnan = find(allnan,1,'last');
      allnan = allnan(1:endnan);  % remove any non-NaNs after this
      
      % find last non-NaN before the NaNs
      beforenan = find(~allnan,1,'last');
      
      if isempty(beforenan)   % if no more non-NaNs
        begnan = 1;
        allnan = false;   % while loop ends
      else  % still more to remove - while loop continues
        begnan = beforenan + 1;
        allnan = allnan(1:beforenan);  % remove the identified NaNs
      end
      
      % identify file sample #s that correspond to beginning and end of
      % this chunk of NaNs and append to artifact
      artifact = [artifact; trsamp(begnan) trsamp(endnan)]; %#ok<AGROW>
    end   % while any(tnan)
  end   % if any(tnan)
end   % for tr = 1:numel(data.trial)

end