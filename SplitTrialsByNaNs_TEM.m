function [artifact,data,badtimes] = SplitTrialsByNaNs_TEM(data)
% SplitTrialsByNaNs_TEM splits the data into separate trials whenever different
%   channels have NaNs, so that one NaN at one timepoint on one channel doesn't
%   break ft_freqanalysis w/ cfg.method = 'mtmconvol';
%
% written 3/20/17 by Teresa E. Madsen

for tr = 1:numel(data.trial)
  trlnan = isnan(data.trial{tr})';   % identify NaNs by timepoint & channel

  if any(trlnan) && ...                   % there are NaNs and 
      size(unique(trlnan,'rows'),1) > 1   % there's >1 pattern across channels
    splits = [];
    for tp = 2:size(trlnan,1)         % between each timepoint and the previous,
      if ~all(trlnan(tp,:) == trlnan(tp-1,:))   % if pattern of NaNs is not the same
        splits = [splits tp];
      end
    end
  end   % if any(trlnan)
end   % for tr = 1:numel(data.trial)

end