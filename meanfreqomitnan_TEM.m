function freq = meanfreqomitnan_TEM(cfg,varargin)
%MEANFREQOMITNAN_TEM uses ft_freqgrandaverage to aggregate trials/subjects, but
% does the averaging outside of that function in order to omit NaNs.
%   Inputs are exactly like ft_freqgrandaverage.
%
% written 4/20/17 by Teresa E. Madsen, Ph.D.

%% adjust cfg for ft_freqgrandaverage

if ~isfield(cfg,'parameter') || isempty(cfg.parameter)
  cfg.parameter = 'powspctrm';
end
if ~isfield(cfg,'tol') || isempty(cfg.tol)
  cfg.tol = 0.01;   % applies to both freq & time
end
if isfield(cfg,'outputfile') && ~isempty(cfg.outputfile)
  outputfile = cfg.outputfile;
  cfg = rmfield(cfg,'outputfile');
end
cfg.keepindividual 	= 'yes';

%% load data, if necessary (more efficient to keep in memory)

if isempty(varargin)
  varargin = cell(size(cfg.inputfile));
  for k = 1:numel(cfg.inputfile)
    disp(['Loading freq from ' cfg.inputfile{k}])
    S = load(cfg.inputfile{k});
    varargin{k} = S.freq;
  end
  S = []; %#ok<NASGU> just clearing the memory
  cfg = rmfield(cfg,'inputfile');
end

%% check some details: fix cfg history, restore label field, remove rptdim, etc.

dimord = cell(size(varargin));
foi = cell(size(varargin));
toi = cell(size(varargin));
for k = 1:numel(varargin)
  % preserve cfg history (I think I did this wrong previously, so this is a fix)
  if isfield(varargin{k},'previous') && isfield(varargin{k},'cfg') && ...
      ~isfield(varargin{k}.cfg,'previous')
    if isfield(varargin{k}.previous,'cfg')
      varargin{k}.cfg.previous = varargin{k}.previous.cfg;
    else
      varargin{k}.cfg.previous = varargin{k}.previous;
    end
  end
  
  % restore label field if only labelcmb is present (breaks ft_freqgrandaverage)
  if ~isfield(varargin{k},'label') && isfield(varargin{k},'labelcmb')
    varargin{k}.label = unique(varargin{k}.labelcmb);
  end
  
  dimord{k} = strsplit(varargin{k}.dimord,'_');
  rptdim = ismember(dimord{k},'rpt');
  if any(rptdim)
    assert(find(rptdim) == 1, 'unexpected dimord')
    assert(size(varargin{k}.(cfg.parameter),1) == 1, ...   % otherwise...
      'multiple trials are not allowed in any input to ft_freqgrandaverage')
    varargin{k}.(cfg.parameter) = shiftdim(varargin{k}.(cfg.parameter),1);
    dimord{k} = dimord{k}(2:end);
    varargin{k}.dimord = strjoin(dimord{k},'_');
  end
  
  foi{k} = varargin{k}.freq;
  toi{k} = varargin{k}.time;
end

%% verify equivalence of freq & time scales

if ~isequal(foi{:})
  for k = 2:numel(foi)
    if ~all(ismembertol(foi{1}, foi{k}, cfg.tol, 'DataScale', 1))
      [LIA,LocB] = ismembertol(foi{k}, foi{1}, cfg.tol, 'DataScale', 1);
      assert(all(LIA), 'freq axes don''t match')  % if any are false, throw error w/ this message
      tmpk = varargin{k}.(cfg.parameter);
      varargin{k}.(cfg.parameter) = NaN(size(varargin{1}.(cfg.parameter)));
      frqdim = find(ismember(dimord{k},'freq'));
      if frqdim == 2
        varargin{k}.(cfg.parameter)(:,LocB,:) = tmpk;
      else
        error('unexpected dimord')
      end
      varargin{k}.freq = foi{1};
    elseif ~all(ismembertol(foi{k}, foi{1}, cfg.tol, 'DataScale', 1))
      error('go back and pad first file to match biggest freq-scale')
    end
  end
end
if ~isequal(toi{:})
  for k = 2:numel(toi)
    if ~all(ismembertol(toi{1}, toi{k}, cfg.tol, 'DataScale', 1))
      [LIA,LocB] = ismembertol(toi{k}, toi{1}, cfg.tol, 'DataScale', 1);
      assert(all(LIA), 'time axes don''t match')  % if any are false, throw error w/ this message
      tmpk = varargin{k}.(cfg.parameter);
      varargin{k}.(cfg.parameter) = NaN(size(varargin{1}.(cfg.parameter)));
      timdim = find(ismember(dimord{k},'time'));
      if timdim == 3
        varargin{k}.(cfg.parameter)(:,:,LocB) = tmpk;
      else
        error('unexpected dimord')
      end
      varargin{k}.time = toi{1};
    elseif ~all(ismembertol(toi{k}, toi{1}, cfg.tol, 'DataScale', 1))
      error('go back and pad first file to match longest timescale')
    end
  end
end

%% pass to ft_freqgrandaverage

if isempty(varargin)
  freq = ft_freqgrandaverage(cfg);
else
  freq = ft_freqgrandaverage(cfg,varargin{:});
end

%% average across subjects

dimtok = strsplit(freq.dimord,'_');

freq.(cfg.parameter) = shiftdim(mean(freq.(cfg.parameter),1,'omitnan'), 1);
freq.dimord = strjoin(dimtok(2:end),'_');

%% save outputfile

if exist('outputfile','var')
  disp(['Saving averaged data to ' outputfile])
  save(outputfile,'freq');
end

end

