function [z3d,flow,fhigh,times,pval3d,MI3d,pref3d]=sig_crossKL_MI2dvTime_TEM(data1,data2,movingwin,video)
%   2d cross-region cross-frequency phase-amplitude modulation over time,
%       using Kullback�Leibler modulation index converted to z-score.
%           From Tort et al., J Neurophysiol, 2010
%   by Teresa E. Madsen 2/20/14
%
%   Usage:
%       [z3d,flow,fhigh,times,pval3d,MI3d,pref3d] = ...
%           sig_crossKL_MI2dvTime_TEM(data1,data2,movingwin,video);
%
%   Inputs:
%       data1 & data2   = unfiltered time series, as row vectors (1xN),
%           each will be used as both the low frequency, phase-modulating
%           signal and the high frequency, amplitude-modulated signal
%       movingwin   = [datawin stepsize] in seconds
%       video       = 'y' or 'n' to indicate whether you want to see and
%                       save a visual representation of the output
%
%   Outputs (top 4 organized within cells representing source of
%           LF phase-modulating data in row index &
%           HF amplitude-modulated data in column index):
%       z3d     = Z-score transformed Kullback�Leibler modulation index
%                   across a 3d array of phase frequencies, amplitude
%                   frequencies, and time
%       pval3d  = p-values in same arrangement as z-scores
%       MI3d    = raw MI values in same arrangement as z-scores
%       pref3d  = preferred phases in same arrangement as z-scores
%
%       times   = time point for each 2d plot or "frame" of the movie
%       flow    = The frequency axis for the lower phase frequencies.
%       fhigh   = The frequency axis for the higher amplitude frequencies.
%
%       and if video = 'y'
%           An AVI video file representing that data, saved to the current
%           directory and titled "KL_MIz2dvTime[yyyymmdd]T[HHMMSS].avi"
%           with the current date and time.
%               NOTE: this code is used simply to guarantee a unique
%           output filename.  It is recommended that you immediately
%           change the filename to reflect the input data.
%
%   DEPENDENCIES:
%       eegfilt
%       sig_KL_MI_TEM

%% check inputs and set up other variables

[M,N] = size(data1);
if M~=1
    error('Data must be a row vector (1xN)');
end
if size(data1) ~= size(data2)
    error('Data vectors must be the same size');
end

Fs = 1000;          % hardcoded to assume Plexon's LFP sampling frequency
pad = [0 Fs];       % add 1 second of 0s to each end of signal
data1 = padarray(data1,pad);
data2 = padarray(data2,pad);

flow1 = 2.^(0:.25:3.5);   % The phase frequencies (low:stepsize:high)
flow2 = 2.^(.5:.25:4);          % high end of each band

fhigh1 = 2.^(4:.1:6.8);       % The amp frequencies (low:stepsize:high)
fhigh2 = 2.^(4.2:.1:7);        % width of frequency bands

flow = (flow1 + flow2) / 2;         % center of each bin for output
fhigh = (fhigh1 + fhigh2) / 2;

if flow1(1) == 0
    if movingwin(1) < 1/flow1(2)
        error('Data window must be longer than period of lowest frequency');
    end
elseif movingwin(1) < 1/flow1(1)
    error('Data window must be longer than period of lowest frequency');
end

Nwin=round(Fs*movingwin(1));    % number of samples in window
Nstep=round(movingwin(2)*Fs);   % number of samples to step through
winstart=1:Nstep:(N-Nwin+1);
winmid=winstart+Nwin/2;
times=round(winmid/Fs);         % center of window for output

z3d = cell(2);
pval3d = cell(2);
MI3d = cell(2);
pref3d = cell(2);

%% calculate MI 2d vs. Time

h = waitbar(0,'Filtering data...');
for i=1:length(flow1)
    waitbar((i-1)/length(flow1),h, ...
        ['working on low frequency ' num2str(i) ' of ' num2str(length(flow1))])
    theta{1}=eegfilt(data1,Fs,flow1(i),flow2(i)); % Filter the low freq signal.
    Phase{1} = angle(hilbert(theta{1}));          % Extract the low freq phase.
    Phase{1} = Phase{1}(1+pad(2):end-pad(2));     % Remove padding.
    theta{2}=eegfilt(data2,Fs,flow1(i),flow2(i)); % Filter the low freq signal.
    Phase{2} = angle(hilbert(theta{2}));          % Extract the low freq phase.
    Phase{2} = Phase{2}(1+pad(2):end-pad(2));     % Remove padding.
    for j=1:length(fhigh1)
        waitbar(((j-1)/length(fhigh1) + i-1)/length(flow1),h, ...
            ['working on high frequency ' num2str(j) ' of ' num2str(length(fhigh1))])
        gamma{1}=eegfilt(data1,Fs,fhigh1(j),fhigh2(j)); % Filter the high freq signal.
        Amp{1} = abs(hilbert(gamma{1}));       % Extract the high freq amplitude.
        Amp{1} = Amp{1}(1+pad(2):end-pad(2));     % Remove padding.
        gamma{2}=eegfilt(data2,Fs,fhigh1(j),fhigh2(j)); % Filter the high freq signal.
        Amp{2} = abs(hilbert(gamma{2}));       % Extract the high freq amplitude.
        Amp{2} = Amp{2}(1+pad(2):end-pad(2));     % Remove padding.
        for t=1:length(winstart)
            waitbar((((t-1)/length(winstart) + j-1)/length(fhigh1) + i-1)/length(flow1),h, ...
                ['working on time window ' num2str(t) ' of ' num2str(length(winstart))])
            indx=winstart(t):(winstart(t)+Nwin-1);
            for LFPM = 1:2          % low frequency phase-modulating data source
                for HFAM = 1:2      % high frequency amplitude-modulated data source
                    Phasewin=Phase{LFPM}(1,indx);
                    Ampwin=Amp{HFAM}(1,indx);
                    [zscore,pval,~,MI,phasepref]=sig_KL_MI_TEM(Ampwin,Phasewin);
                    if isnan(zscore)
                        error('Modulation Index Z-score = NaN ???')
                    end
                    z3d{LFPM,HFAM}(i,j,t)=zscore;
                    pval3d{LFPM,HFAM}(i,j,t)=pval;
                    MI3d{LFPM,HFAM}(i,j,t)=MI;
                    pref3d{LFPM,HFAM}(i,j,t)=phasepref;
                end
            end
        end
    end
end
close(h)

%% Create & save a movie of the 2d modulation indices over time

if video == 'y'
    scale = [min(min(min(z3d))) max(max(max(z3d)))];
    F(length(winstart))=struct('cdata',[],'colormap',[]);
    for t=1:length(winstart)
        mod2d(:,:)=z3d(:,:,t);
        imagesc(flow, fhigh, mod2d');
        colorbar; colormap(jet);
        axis xy
        set(gca, 'FontSize', 18);
        title(['KL-MI 2d @ t = ' num2str(times(t)-30) ...
            ' seconds from tone onset']);
        set(gca,'XTick',2:2:14);
        xlabel('Phase Frequency (Hz)');
        ylabel('Amplitude Frequency (Hz)');
        caxis(scale);
        F(t)=getframe(gcf);
    end
    outputfile = ['KL_MIz2dvTime' datestr(now,30) '.avi'] %#ok<NOPRT>
%     movie2avi(F,outputfile,'compression','None');
v = VideoWriter(outputfile); 
% Open the file for writing and write a 300-by-300 matrix of data to the file.
open(v)
writeVideo(v,F)
% Close the file.
close(v)
end
end
