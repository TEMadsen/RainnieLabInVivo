% When you quit MATLAB this file will be executed.

%% ask to check git status

button = questdlg('Check git status before quitting?', 'Check git?', ...
  'Yes','No','Cancel', 'Yes');

switch button
  case 'Yes'
    maintainGit
    fprintf(['\nIf there are changes that should be committed \n' ...
      'and/or pushed first, please CANCEL next dialog box.\n']);
  case 'No'
    warning('git status unknown')
  case 'Cancel'
    quit cancel   % don't exit Matlab
    return        % don't ask to save workspace variables
end

%% ask to save open figures

allfigs = findall(0,'Type','Figure');
if ~isempty(allfigs)
  button = questdlg('Save open figures before quitting?', ...
    'Save figures?', 'Yes','No','Cancel', 'Yes');
  
  switch button
    case 'Yes'
      figfile = [fileparts(userpath) filesep 'MATLAB' filesep ...
        'LastFigures.fig'];
      disp(['Saving all figures to ' figfile])
      disp(['This file will be overwritten on next shutdown - '  ...
        'rename if you want to keep it.']);
      savefig(allfigs,figfile)
      delete(allfigs)   % closes them & deletes their handles so they don't get saved with data in next section
    case 'No'
      warning('figures not saved')
      delete(allfigs)   % closes them & deletes their handles so they don't get saved with data in next section
    case 'Cancel'
      disp('Use the commands in git_CopyNPaste.m to commit and push any changes.')
      quit cancel   % don't exit Matlab
      return
  end
end

%% ask to save workspace variables

button = questdlg('Save workspace variables before quitting?', ...
  'Save workspace?', 'Yes','No','Cancel', 'Yes');

switch button
  case 'Yes'
    wkspcfile = [fileparts(userpath) filesep 'MATLAB' filesep ...
      'LastWorkspace.mat'];
    disp(['Saving all workspace variables to ' wkspcfile])
    disp(['This file will be overwritten on next shutdown - '  ...
      'rename if you want to keep it.']);
    save(wkspcfile)
  case 'No'
    wkspcvars = cellstr(who());
    wkspcvars = wkspcvars(~ismember(wkspcvars,{'ans','button','wkspcfile'}));
    if ~isempty(wkspcvars)
      warning('workspace variables not saved')
    end
  case 'Cancel'
    disp('Use the commands in git_CopyNPaste.m to commit and push any changes.')
    quit cancel   % don't exit Matlab
end
