%% info needed for analyzing fear data, including all rats filenames
%#ok<*SNASGU> just provides background info for other analysis protocols

figurefolder = 'C:\Users\tmadsen\Pictures\TempFigures\';
datafolder = 'S:\Teresa\Analyses\TempData\';

% each row # here corresponds to cell row (1st dim) in other variables
ratnums = [200; 202; 203; 204; 205; 210; 211; 212];
excluded = logical([0; 1; 0; 0; 0; 0; 1; 1]);
chmPFC = cell(numel(ratnums),1);
chBLA = cell(numel(ratnums),1);
finalch = false([numel(ratnums),1]);
ratT = table(ratnums, excluded, chmPFC, chBLA, finalch);
clearvars ratnums excluded chmPFC chBLA finalch

% each column # here corresponds to cell column (2nd dim) in other variables
phases = {'Conditioning', 'Recall', 'Extinction'}; 

blocks = {'Habituation', 'Fear Recall', 'Extinction Recall'; ...
    'Acquisition', 'Early Extinction', 'Late Extinction'}; 

% each row # here corresponds to the numerical code that will be saved in
% trl and data.trialinfo to identify which subtrials correspond to which
% interval types
intervals = {'Before'; 'During'; 'After'};

% each row # here corresponds to the numerical code that will represent
% which region's data is being used for phase or amplitude source data
regions = {'mPFC'; 'BLA'};

% only use this format if all rats were implanted on the same side, so the
% channel #s always correspond to the same regions
channels = {'AD01','AD02','AD03','AD04','AD05','AD06','AD07','AD08';...
    'AD09','AD10','AD11','AD12','AD13','AD14','AD15','AD16'};

% initialize variables
goodchs = cell(size(ratT,1),1);  % cell array of strings can't be contained within one cell of a table
notes = cell(size(ratT,1),numel(phases));
toneTS = cell(size(ratT,1),numel(phases));
nexfile = cell(size(ratT,1),numel(phases));

% any extra comments about each rat
notes{1,3} = 'tone timestamps for his Extinction are in "toneTS{1,3}"';
toneTS{1,3} = [486759 863759 1204759 1420759 1628759 1976759 2142759 ...
    2350759 2673759 2906759 3088759 3459759 3731760 3981760 4294760];
notes{2,3} = 'failed extinction';
notes{7,3} = 'lost headcap before extinction';
notes{8,2} = 'failed to consolidate & express fear learning';

% channels to use for each rat, defaults to 'AD*' if empty
goodchs{1} = {'AD*'};  % all AD channels except...
ratT.chmPFC(1) = {'AD05'};
ratT.chBLA(1) = {'AD11'};
% ratT.finalch(1) = true;
goodchs{2} = {'AD*'};  % all AD channels except...
ratT.chmPFC(2) = {'AD03'};
ratT.chBLA(2) = {'AD14'};
% ratT.finalch(2) = true;
goodchs{3} = {'AD*'};  % all AD channels except...
ratT.chmPFC(3) = {'AD02'};
ratT.chBLA(3) = {'AD10'};
% ratT.finalch(3) = true;
goodchs{4} = {'AD*'};  % all AD channels except...
ratT.chmPFC(4) = {'AD07'};
ratT.chBLA(4) = {'AD10'};
% ratT.finalch(4) = true;
goodchs{5} = {'AD*'};  % '-AD12', '-AD15', 
ratT.chmPFC(5) = {'AD03'};
ratT.chBLA(5) = {'AD12'};
% ratT.finalch(5) = true;
goodchs{6} = {'AD*'};  % all AD channels except...
ratT.chmPFC(6) = {'AD06'};
ratT.chBLA(6) = {'AD10'};
% ratT.finalch(6) = true;
goodchs{7} = {'AD*'};  % all AD channels except...
ratT.chmPFC(7) = {'AD05'};
ratT.chBLA(7) = {'AD14'};
% ratT.finalch(7) = true;
goodchs{8} = {'AD*'};  % all AD channels except...
ratT.chmPFC(8) = {'AD03'};
ratT.chBLA(8) = {'AD10'};
% ratT.finalch(8) = true;

%% filenames and invalidated trials
% Habituation & Acquisition (fear conditioning)
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\200\200_091003_0000-1mrg-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\202\202_091005_0002-aligned.nex';     
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\203\203_091204_0001-alignedLFPs.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\204\204_100313_0119-aligned.nex';
nexfile{5,1} = 'S:\Teresa\PlexonData Backup\205\205_100311_0110-alignedLFPs.nex';
nexfile{6,1} = 'S:\Teresa\PlexonData Backup\210\210_100611_0060-alignedLFPs.nex';
nexfile{7,1} = 'S:\Teresa\PlexonData Backup\211\211_100514_0000-aligned.nex';
nexfile{8,1} = 'S:\Teresa\PlexonData Backup\212\212_100604_0044-alignedLFPs.nex';

% Recall (extinction 1)
nexfile{1,2} = 'S:\Teresa\PlexonData Backup\200\200_091006_0001-aligned.nex';
nexfile{2,2} = 'S:\Teresa\PlexonData Backup\202\202_091008_0000-aligned.nex';
nexfile{3,2} = 'S:\Teresa\PlexonData Backup\203\203_091207_0000-aligned.nex';
nexfile{4,2} = 'S:\Teresa\PlexonData Backup\204\204_100316_0127-aligned.nex';
nexfile{5,2} = 'S:\Teresa\PlexonData Backup\205\205_100314_0121-aligned.nex';
nexfile{6,2} = 'S:\Teresa\PlexonData Backup\210\210_100614_0066-aligned.nex';
nexfile{8,2} = 'S:\Teresa\PlexonData Backup\212\212_100607_0049-aligned.nex';

% Extinction (extinction 2)
nexfile{1,3} = 'S:\Teresa\PlexonData Backup\200\200_091007_0000-aligned.nex';
nexfile{2,3} = 'S:\Teresa\PlexonData Backup\202\202_091009_0000-aligned.nex';     
nexfile{3,3} = 'S:\Teresa\PlexonData Backup\203\203_091208_0000-aligned.nex';
nexfile{4,3} = 'S:\Teresa\PlexonData Backup\204\204_100317_0131-aligned.nex';
nexfile{5,3} = 'S:\Teresa\PlexonData Backup\205\205_100315_0124-aligned.nex';
nexfile{6,3} = 'S:\Teresa\PlexonData Backup\210\210_100615_0068-aligned.nex';
nexfile{8,3} = 'S:\Teresa\PlexonData Backup\212\212_100608_0053-aligned.nex';
