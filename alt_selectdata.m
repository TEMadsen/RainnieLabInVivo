function [data] = alt_selectdata(cfg, data)
% ALT_SELECTDATA is a custom alternative bare-bones version of
% ft_selectdata, simplified & optimized for speed.  It makes a selection in
% the input data along specific data dimensions, such as channels, time,
% trials, etc. (modified by TEM 2/28/17)
%
% Use as
%  [data] = alt_selectdata(cfg, data, ...)
%
% The cfg argument is a configuration structure which can contain
%   cfg.trials = 1xN, trials indices to keep, or logical indexing, where
%     false(1,N) removes all the trials.  Trials are assumed to be:
%       cells of data.time & data.trial
%       dim 1 of data.sampleinfo & data.trialinfo
%
%   cfg.channel = 1xN, channel indices to keep, or logical indexing, where
%     false(1,N) removes all the channels.  Channels are assumed to be:
%       cells of data.label
%       dim 1 of data.trial{:}
%
%   cfg.latency = [beg end], Time is assumed to be:
%       dim 2 of data.time{:} & data.trial{:}

if isfield(cfg,'trials')
  data.time         = data.time(cfg.trials);
  data.trial        = data.trial(cfg.trials);
  data.sampleinfo   = data.sampleinfo(cfg.trials,:);
  data.trialinfo    = data.trialinfo(cfg.trials,:);
end

if isfield(cfg,'channel')
  data.label        = data.label(cfg.channel);
  for tr = 1:numel(data.trial)
    data.trial{tr}  = data.trial{tr}(cfg.channel,:);
  end
end

if isfield(cfg,'latency')
  for tr = 1:numel(data.trial)
    tsamp = data.sampleinfo(tr,1):data.sampleinfo(tr,2);
    tndx = find(data.time{tr} >= cfg.latency(1) & data.time{tr} < cfg.latency(2));
    data.time{tr}           = data.time{tr}(1,tndx);
    data.trial{tr}          = data.trial{tr}(:,tndx);
    data.sampleinfo(tr,:)   = [tsamp(tndx(1)) tsamp(tndx(end))];
  end
end

