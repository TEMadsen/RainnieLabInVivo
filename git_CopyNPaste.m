%% git commands to run in Matlab!  for more info, read:  
% http://www.fieldtriptoolbox.org/development/git

%% initial git setup for a new computer 
% open to read instructions & change to your name, etc.

setupGit

%% daily startup:  run this section before starting analysis
% added to startup.m & finish.m so it should ask to do it automatically
% every time Matlab is opened or closed

maintainGit

%% create new branch before making changes to the code
% don't forget to report bugs here:  http://bugzilla.fieldtriptoolbox.org/
% and follow: http://www.fieldtriptoolbox.org/development/guidelines/code

% create & switch into specified branch:
git checkout -b [branch-name]   
% replace [branch-name] with bug#### if working on a bugfix, or for an
% enhancement, use a short description of the proposed feature

%% or if continuing with a work-in-progress, switch to it & merge latest 
% changes that have been made to official FieldTrip

% Switches to the specified branch and updates the working directory
git checkout [branch-name]  

% To checkout a remote branch, creating a local copy
git checkout -b [branch-name] [remote-name]/[branch-name]

% Combines the master branch�s history into the current branch
git merge master  

%% at the end of every day that you've made changes to FieldTrip code, 
% commit changes (records file snapshots permanently in version history)
% and push to remote repository (changes made to local copy of FieldTrip
% are synced with personal fork of FieldTrip on GitHub)

% Stages the file in preparation for versioning
git add [filename or . for all unstaged files]  

% Record file snapshots permanently in version history with log entry - see
% http://www.fieldtriptoolbox.org/development/guidelines/code#svn_log_messages
% Paraphrase:  Log entries should describe the change to the file or files,
% explaining what part of the code is changed and why. Start your log
% message with a single descriptive word (e.g., �bugfix�, �enhancement�,
% �documentation�, �restructuring�) and a hyphen to separate it from the
% actual description. 
% Paraphrased from http://chris.beams.io/posts/git-commit/:  If you need
% more than 50-69 characters, use a subject line followed by an extra line
% of space, then a longer description, wrapping those lines manually at no
% more than 72 characters.
git commit -m "[descriptive message]"   

% Upload all local branch commits to GitHub
git push origin [branch-name]  

%% when ready to share with FieldTrip developers, submit a pull request at 
% https://github.com/fieldtrip/fieldtrip and note that any further commits
% pushed to this branch will also be included in the pull request, so don't
% forget to make a new branch if you want to edit something without sharing
% with the FieldTrip developers yet

%% after changes have been incorporated in official version of FieldTrip 
% (or merged into master RainnieLabInVivo repo), delete the branch

% delete bugfix/enhancement branch (big D to force even if it hasn't be merged)
git branch -d [branch-name]  

% save deletion to remote repository
git push origin :[branch-name]   

%% review changes

% Lists all new or modified files to be committed
git status  

% Shows file differences not yet staged
git diff    

% review all commits made in the last week (better with color in GitBash)
git log --all --oneline --graph --decorate --since="1 week ago"

%% undo stuff

% Unstages the file, but preserve its contents
git reset [file]  

% Undoes all commits after [commit], preserving changes locally
git reset [commit]  

% Discards all history and changes back to the specified commit
git reset --hard [commit]   

% Deletes the file from the working directory and stages the deletion
git rm [file]   

% Removes the file from version control but preserves the file locally
git rm --cached [file]  

% Changes the file name and prepares it for commit
git mv [file-original] [file-renamed]   

%% in case of merge conflicts

git merge --abort

% to keep the remote changes every time they conflict with yours
% (or "ours" to prefer the changes made to the local branch)
git pull -v -s recursive -X theirs

% to change back to the remote version altogether, keeping none of your changes
% (like if you accidentally make changes to the master branch when you don't
% have write access, but you may want to create another branch to save these
% changes first)
git reset --hard origin/master

%% to fix a repository that is broken by a computer crash
% http://stackoverflow.com/questions/15317072/git-repository-broken-after-computer-died
% try 
git reflog
git fsck

% easiest solution may be to back up the repo by renaming the folder, then
% reclone it from the server (run the relevant cell in setupGit.m) and copy
% the edited files (NOT the .git folder) back in, then commit all changes
% and push