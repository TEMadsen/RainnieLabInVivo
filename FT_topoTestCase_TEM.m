%% time-freq analysis of 200's 1st recall trial (raw) w/ topographic plot

info_fear               % load all background info (filenames, etc)

foi = 2.^(0:(1/4):7);   % 1/4-powers of 2 from 1-128 Hz
tapsmofrq = diff(foi);  % smooth each band by diff from next higher
foi = foi(1:end-1);     % trim highest freq to have same numel as tapsmofrq
for f = find(tapsmofrq < 0.5)   % don't care about narrower bands than this
  tapsmofrq(f) = 0.5;           % keeps time windows under 4 seconds
end
t_ftimwin = 2./tapsmofrq;       % for 3 tapers (K=3), T=2/W

begtim = -4;    % in seconds
endtim = 4;
overlap = 0.5;  % time windows overlap by at least 50%
toi = linspace(begtim, endtim, round((endtim-begtim) ./ ...
  (overlap * min(t_ftimwin))) + 1);

r = 1;  % Rat #200
p = 2;  % Recall
t = 1;  % Trial #1

%% calculate raw spectrograms & coherogram

% Raw LFP data divided into long, non-overlapping tone-triggered trials
inputfile = [datafolder 'RawLFPTrialData_' int2str(ratT.ratnums(r)) ...
  phases{p} '.mat'];

load(inputfile);

trials = find(data.trialinfo(:,1) == t);

cfg             = [];

cfg.method      = 'mtmconvol';
cfg.output      = 'powandcsd';
cfg.keeptrials  = 'no';
cfg.trials      = trials;

cfg.foi         = foi;
cfg.tapsmofrq   = tapsmofrq;
cfg.t_ftimwin   = t_ftimwin;
cfg.toi         = toi;
cfg.pad         = 'nextpow2';

cfg.outputfile  = [datafolder 'SpecCoherograms_' ...
  int2str(ratT.ratnums(r)) phases{p} int2str(t) 'raw.mat'];

if exist(cfg.outputfile,'file')
  load(cfg.outputfile);
else
  freq = ft_freqanalysis(cfg,data);
end

%% calculate coherence

% translate channel names into indices
indx = NaN(size(freq.labelcmb));

for i = 1:size(freq.labelcmb,1)
  % find 1st ch index
  indx(i,1) = find(strcmp(freq.labelcmb(i,1),freq.label));
  
  % find 2nd ch index
  indx(i,2) = find(strcmp(freq.labelcmb(i,2),freq.label));
end

% preallocate space
freq.coherence   = NaN(size(freq.crsspctrm));

for i = 1:size(indx,1)
  freq.coherence(i,:,:) = abs(freq.crsspctrm(i,:,:)./ ...
    sqrt(freq.powspctrm(indx(i,1),:,:).* ...
    freq.powspctrm(indx(i,2),:,:)));
end

%% visualize the standard way

h = 0.6/(numel(freq.label));
w = 0.8/(numel(freq.label));
maxpow = 0;

cfg                 = [];
cfg.ylim            = [foi(1)-tapsmofrq(1) 20];
cfg.colormap        = jet;
cfg.fontsize        = 6;

for ch = 1:numel(freq.label)
  %% plot spectrogram
  
  cfg.parameter       = 'powspctrm';
  
  %   cfg.baseline        = 'yes';
  %   cfg.baselinetype    = 'db';   
  
  %%% consider adding option to convert to db even when cfg.baseline = 'no'
  
  cfg.zlim            = 'zeromax';
  cfg.channel         = freq.label{ch};
  
  figure(1);
  subplot(4,4,ch); cla
  
  ft_singleplotTFR(cfg, freq);
  
  ax = gca;
  ax.FontSize = cfg.fontsize;
  
  maxpow = max([maxpow ax.CLim]);
  ax.CLim = [0 maxpow];
  
  xlabel('Time from Tone Onset (seconds)')
  ylabel('Frequency (Hz)')
  title([freq.label{ch} ' Power'])
  
  %% plot coherence
  
  cfg.parameter       = 'coherence';
  cfg.zlim            = [0 1];
  
  %   cfg.baselinetype    = 'absolute';
  
  figure(2)
  lft = (ch-0.3)/numel(freq.label);
  
  for ref = numel(freq.label):-1:2
    if ref > ch
      cfg.refchannel      = freq.label{ref};
      
      btm = (ref-1.1)/numel(freq.label);
      subplot('Position',[lft btm w h]); cla
      %       figure(numel(freq.label) + ref); clf
      
      ft_singleplotTFR(cfg, freq);
      
      ax = gca;
      ax.FontSize = cfg.fontsize;
      
      colorbar('off')
      
      if ref == numel(freq.label)
        title(freq.label{ch})
      else
        title('')
      end
      
      if ch == 1
        ylabel(freq.label{ref})
      end
      
      %       print([figurefolder int2str(ratT.ratnums(r)) ...
      %         phases{p} int2str(t) '_Coherogram_' ...
      %         freq.label{ch} freq.label{ref}],'-dpng')
      %       axis off
    end
  end
  xlabel('Time (s)')
end

print('-f2',[figurefolder int2str(ratT.ratnums(r)) ...
  phases{p} int2str(t) '_Coherograms'],'-dpng');

print('-f1',[figurefolder int2str(ratT.ratnums(r)) ...
    phases{p} int2str(t) '_Spectrograms'],'-dpng')

%% visualize the topography

%%% STOP MESSING AROUND WITH THE STANDARD VISUALIZATION AND DO THIS!!!!

ft_multiplotTFR

ft_topoplotTFR




