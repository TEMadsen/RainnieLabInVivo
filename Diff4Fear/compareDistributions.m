 %% display distributions, means, & stddevs, before & after log transform

info_diff4fear;
r=5; ch=13;
baselinefile = 'S:\Teresa\Analyses\TempData\Diff4Fear\347\Baseline_347AD13_z15_fasterwavSpectrogram.mat';
      
%% load baseline freq file
      
disp(['Loading baseline freq data for rat #' ...
  num2str(ratT.ratnums(r)) ', ' allchs{ch}])
S = load(baselinefile,'freq');
BLfreq = S.freq;
S = [];

%% shrink subset of frequencies

foi = 2.^(0:6);
fndx = zeros(size(foi));
for f = 1:numel(foi)
  [~,fndx(f)] = min(abs(foi(f)-BLfreq.freq));
end 

%% collect raw & log transformed power stats

rawpow = BLfreq.powspctrm(1,fndx,:);
rawmeans = mean(rawpow, 3, 'omitnan');
rawstddevs = std(rawpow, [], 3, 'omitnan');
rawbins = [-inf linspace(min(rawpow(:)),max(rawmeans+rawstddevs)*1.2,50) inf];
rawN = NaN(numel(rawbins)-1, numel(fndx));

logpow = log(rawpow);
logmeans = mean(logpow, 3, 'omitnan');
logstddevs = std(logpow, [], 3, 'omitnan');
logbins = [-inf linspace(min(logpow(:)),max(logpow(:)),50) inf];
logN = NaN(numel(logbins)-1, numel(fndx));

% freqs = repmat(BLfreq.freq(fndx), [1,1,size(rawpow,3)]);

% edges = 0;
for f = 1:numel(fndx)
  rawN(:,f) = histcounts(rawpow(1,f,:), rawbins, ...
    'Normalization', 'probability');
  logN(:,f) = histcounts(logpow(1,f,:), logbins, ...
    'Normalization', 'probability');
%   edges(end+1) = mean([edges(end) BLfreq.freq(fndx(f))]);
end
% edges(end+1) = inf;

% rawY = max(sum(rawN,2));
% logY = max(sum(logN,2));

%% plot histograms of raw power for each frequency

fig = figure(1); clf; subplot(2,1,1); hold all;

colors = winter(numel(fndx));

% yyaxis left
b1 = bar(rawbins(2:end-1), rawN(2:end,:), 1, 'grouped');
for b = 1:numel(fndx)
  b1(b).FaceColor = colors(b,:);
end
ylabel('Probability');

% yyaxis right
h1 = plot(...
... %   exp([logmeans-logstddevs; logmeans; logmeans+logstddevs]), ...
... %   repmat(1:numel(fndx), [3,1])-0.1, '-+', ...
  [rawmeans-rawstddevs; rawmeans; rawmeans+rawstddevs], ...
  repmat(max(rawN(:))*(30+(1:numel(fndx)))/30, [3,1]), '-o', ...
  'MarkerSize',4);
for h = 1:numel(fndx)
  h1(h).Color = colors(h,:);
%   h1(h).Color = (colors(h,:)+1)./2;           % lighter than bar colors
%   h1(numel(fndx)+h).Color = colors(h,:)./2;   % darker than bar colors
end
% ylabel({'Frequency (Hz)'; ...
%   'Lighter lines = exp(mean +/- std dev of log power)'; ...
%   'Darker lines = mean +/- std dev of raw power'});
% ax = gca;
% ax.YLim = [-20 numel(fndx)+0.5];
% ax.YTick = [];
% ax.YTick = 1:numel(fndx);
% ax.YTickLabel = num2str(foi');

ylim([0, max(rawN(:))*(31+numel(fndx))/30]);
xlim([min(rawmeans-rawstddevs)*1.1 rawbins(end-1)*1.02]);
xlabel('Distribution of Raw Power w/in Each Frequency Band'); 
legend([num2str(foi') repmat(' Hz',[numel(foi),1])], 'Location','northeast');

%% plot histograms of log power for each frequency

lowerr = rawmeans-rawstddevs;
lowerr(lowerr <= 0) = exp(logbins(2)-1);

subplot(2,1,2); hold all;

% yyaxis left
b2 = bar(logbins(2:end-1), logN(2:end,:), 1, 'grouped');
for b = 1:numel(fndx)
  b2(b).FaceColor = colors(b,:);
end
ylabel('Probability');

% yyaxis right
h2 = plot([logmeans-logstddevs; logmeans; logmeans+logstddevs], ...
  repmat(max(logN(:))*(30+(1:numel(fndx)))/30, [3,1]), '-o', ...
... %   log([lowerr; rawmeans; rawmeans+rawstddevs]), ...
... %   repmat(1:numel(fndx), [3,1])+0.1, '-+', ...
  'MarkerSize',4);
for h = 1:numel(fndx)
  h2(h).Color = colors(h,:);
%   h2(h).Color = (colors(h,:)+1)./2;   % log stats lighter than bar colors
%   h2(numel(fndx)+h).Color = colors(h,:)./2; % raw stats darker than bar colors
end
% ylabel({'Frequency (Hz)'; ...
%   'lines = mean +/- std dev of log power'
% ... %   'Lighter lines = mean +/- std dev of log power'; ...
% ... %   'Darker lines = log(mean +/- std dev of raw power)' ...
%   });
% ax = gca;
% ax.YLim = [-20 numel(fndx)+0.5];
% ax.YTick = [];
% ax.YTick = 1:numel(fndx);
% ax.YTickLabel = num2str(foi');

ylim([0, max(logN(:))*(31+numel(fndx))/30]);
xlim([logbins(2) logbins(end-1)]);
xlabel('Distribution of Natural Log Power w/in Each Frequency Band'); 
legend([num2str(foi') repmat(' Hz',[numel(foi),1])], 'Location','northwest');

%% save figure

print(fig, 'S:\Teresa\Figures\Diff4Fear\RawVsLogPowerDistributions', ...
  '-dpng', '-r90');
