%% Normalizes trial spectrograms & averages them in blocks of 3

CleanSlate  % provides a clean slate to start working with new data/scripts
% by clearing all variables, figures, and the command window

info_diff4fear            % load all background info & final parameters
errlog = struct('e',{[],[],[],[],[],[],[],[],[],[],[],[]}); 	% MEs saved w/in nested structure

% rats 357 & 359 need their CS+/- reversed between input & output files
revCS = false(size(ratT,1),1);
revCS([5 7]) = true;

%% to plot only specific file(s)

normtype = 'Raw';  % based on FieldTrip's cfg.baselinetype = 'absolute';
% normtype = 'standard';  % based on FieldTrip's cfg.baselinetype = 'dB';


%% initialize workers
% parpool

%% full loop
for r = 1:5
  %   try
  %% find available spectrogram files
  
  foldername = [datafolder num2str(ratT.ratnums(r))];
  filelist = what(foldername);
  
  %% preallocate variables
  %   [datafolder num2str(ratT.ratnums(r)) filesep ...
  %         stimuli{s} num2str(tn) 'of' phases{p} '_' num2str(ratT.ratnums(r)) ...
  %         '_4ch_timeless_powandcsd.mat']
  pndx = cell(numel(filelist.mat),numel(phases));
  sndx = cell(numel(filelist.mat),numel(stimuli));
  
  %% find indices of relevant info in all filenames
  
  for p = 1:numel(phases)
    pndx(:,p) = strfind(filelist.mat,phases{p});
  end
  for si = 1:numel(stimuli)
    sndx(:,si) = strfind(filelist.mat,stimuli{si});
  end
  tndx = strfind(filelist.mat,'of');      % to find tone #
  bndx = strfind(filelist.mat,'Block');   % to find block #
  ntndx = strfind(filelist.mat,normtype); % only finds desired files
  FourChndx = strfind(filelist.mat,'4ch'); % only finds 4ch_timeless files
  rndx = strfind(filelist.mat,'Rev');   % to indicate CS+/- have already been reversed
  
  %% put file info in a more legible format
  
  fileS = cell2struct(...
    [filelist.mat, pndx, sndx, tndx, bndx, ntndx, FourChndx, rndx], ...
    [{'filename'}; phases(:);  ...
    {'CSplus'; 'CSminus'; 'tndx'; 'bndx'; 'ntndx'; 'FourChndx'; 'rndx'}], 2);
  
  
  %% assume channels are bad, add 1 for each good stimulus type
  
  errlog(r).good = zeros(numel(phases),3);  % 3 blocks/phase
  
  %% identify baseline file
  
  filenums = find(~arrayfun(@(x) isempty(x.Baseline),fileS) & ...
    ~arrayfun(@(x) isempty(x.FourChndx),fileS));
  
  %% stop here to avoid continue
  
  if numel(filenums) < 1
    errlog(r).Baseline.warning = ...
      ['No baseline 4Ch freq file found for rat #' ...
      num2str(ratT.ratnums(r))];
    warning(errlog(r).Baseline.warning)
    continue
  elseif numel(filenums) > 1
    errlog(r).Baseline.warning = ...
      ['More than one baseline 4Ch freq file found for rat #' ...
      num2str(ratT.ratnums(r))];
    warning(errlog(r).Baseline.warning)
    continue
    %           keyboard  % select one? narrow criteria?
  else
    %% load baseline freq file
    
    baselinefile = [foldername filesep fileS(filenums).filename];
    disp(['Loading 4ch baseline freq data for rat #' ...
      num2str(ratT.ratnums(r))])
    S = load(baselinefile,'freq');
    BLfreq = S.freq;
    S = [];
    %% change freq labels to regions
    
    indxIL = strcmp(bestchs(r).IL,BLfreq.label);
    indxPrL = strcmp(bestchs(r).PrL,BLfreq.label);
    indxBLA = strcmp(bestchs(r).BLA,BLfreq.label);
    indxvHPC = strcmp(bestchs(r).vHPC,BLfreq.label);
    
    BLfreq.label{indxIL} = 'IL';
    BLfreq.label{indxPrL} = 'PrL';
    BLfreq.label{indxBLA} = 'BLA';
    BLfreq.label{indxvHPC} = 'vHPC';
    
    
    for i = 1:size(BLfreq.labelcmb,1)
      for j = 1:size(BLfreq.labelcmb,2)
        indxILcmb = strcmp(bestchs(r).IL,BLfreq.labelcmb(i,j));
        indxPrLcmb = strcmp(bestchs(r).PrL,BLfreq.labelcmb(i,j));
        indxBLAcmb = strcmp(bestchs(r).BLA,BLfreq.labelcmb(i,j));
        indxvHPCcmb = strcmp(bestchs(r).vHPC,BLfreq.labelcmb(i,j));
        
        if indxILcmb == 1
          BLfreq.labelcmb{i,j} = 'IL';
        elseif indxPrLcmb == 1
          BLfreq.labelcmb{i,j} = 'PrL';
        elseif indxBLAcmb == 1
          BLfreq.labelcmb{i,j} = 'BLA';
        elseif indxvHPCcmb == 1
          BLfreq.labelcmb{i,j} = 'vHPC';
        end
      end
    end
    
    for i = 1:size(BLfreq.labelcmb,1)
      switch BLfreq.labelcmb{i,1}
        case 'IL'
          switch BLfreq.labelcmb{i,2}
            case 'PrL'
              BLfreq.label{i+4} = 'PrL-IL';
            case 'BLA'
              BLfreq.label{i+4} = 'PrL-BLA';
            case 'vHPC'
              BLfreq.label{i+4} = 'PrL-vHPC';
          end
        case 'PrL'
          switch BLfreq.labelcmb{i,2}
            case 'IL'
              BLfreq.label{i+4} = 'PrL-IL';
            case 'BLA'
              BLfreq.label{i+4} = 'IL-BLA';
            case 'vHPC'
              BLfreq.label{i+4} = 'IL-vHPC';
          end
        case 'BLA'
          switch BLfreq.labelcmb{i,2}
            case 'IL'
              BLfreq.label{i+4} = 'IL-BLA';
            case 'PrL'
              BLfreq.label{i+4} = 'PrL-BLA';
            case 'vHPC'
              BLfreq.label{i+4} = 'BLA-vHPC';
          end
        case 'vHPC'
          switch BLfreq.labelcmb{i,2}
            case 'IL'
              BLfreq.label{i+4} = 'IL-vHPC';
            case 'BLA'
              BLfreq.label{i+4} = 'BLA-vHPC';
            case 'PrL'
              BLfreq.label{i+4} = 'PrL-vHPC';
          end
      end
      
      % find 1st ch index
      indx1 = strcmp(BLfreq.labelcmb(i,1),BLfreq.label);
      
      % find 2nd ch index
      indx2 = strcmp(BLfreq.labelcmb(i,2),BLfreq.label);
      
      BLfreq.powspctrm(i+4,:,:) = abs(BLfreq.crsspctrm(i,:,:)./ ...
        sqrt(BLfreq.powspctrm(indx1,:,:).* ...
        BLfreq.powspctrm(indx2,:,:)));
    end
    for i = 1:size(regions,1)
      BLfreq.label(1) = [];
      BLfreq.powspctrm(1,:) = [];
    end
  end
  
  %% check for all NaNs in any frequency
  
  if any(all(isnan(BLfreq.powspctrm),3))
    errlog(r).Baseline.warning = ...
      'At least one foi is all NaNs in 4Ch baseline file.';
    warning(errlog(r).Baseline.warning)
    continue
  end
  
  
  %% loop through all phases
  
  for p = 1:numel(phases)
    %           try
    %%  & both stimulus types
    
    for si = 1:numel(stimuli)   % input data labeled as stimulus type
      %               try
      if revCS(r)
        so = find(~ismember([1 2],si));   % output data should have reversed title
      else
        so = si;
      end
      
      filenums = find(~arrayfun(@(x) isempty(x.FourChndx),fileS) & ...
        ~arrayfun(@(x) isempty(x.(phases{p})),fileS) & ...
        ~arrayfun(@(x) isempty(x.(stimstr{si})),fileS));
      
      %% stop here to avoid continue
      
      if numel(filenums) < 3   % spectrograms don't exist for all 9 tones
        errlog(r).(phases{p}).(stimstr{si}).warning = ...
          ['Fewer than 3 ' stimuli{si} ...
          ' freq 4Ch files found during ' phases{p} ' for rat #' ...
          num2str(ratT.ratnums(r))];
        warning(errlog(r).(phases{p}).(stimstr{si}).warning)
        continue
      end
      
      %% loop through all freq files, IDing tone #
      
      tonenum = NaN(size(filenums));
      for k = 1:numel(filenums)
        tonenum(k) = str2double(fileS(filenums(k)).filename(numel(stimuli{si})+1:...
          fileS(filenums(k)).tndx-1));
      end
      
      %% check for duplicates or >9
      
      %         if numel(tonenum) > numel(unique(tonenum))
      %           errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).warning = ['Duplicate ' ...
      %             stimuli{si} ' tone #s found during ' phases{p} ' for rat #' ...
      %             num2str(ratT.ratnums(r)) ', ' allchs{ch}];
      %           warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).warning)
      %           %                   continue
      %           keyboard  % select one? narrow criteria?
      %         end
      %
      if max(tonenum) > 9
        errlog(r).(phases{p}).(stimstr{si}).warning = [stimuli{si} ...
          ' tone #s >9 found during ' phases{p} ' for rat #' ...
          num2str(ratT.ratnums(r))];
        warning(errlog(r).(phases{p}).(stimstr{si}).warning)
        %                   continue
        %           keyboard  % select one? narrow criteria?
        filenums(tonenum < max(tonenum)-8) = [];
        tonenum(tonenum < max(tonenum)-8) = [];
        tonenum = tonenum-min(tonenum)+1;
      end
      
      %% loop through blocks of 3 tones, averaging
      
      for b = 1:3
        %                   try
        if revCS(r)
          rev = 'Rev';  % to mark file as having reversed CS+/-
        else
          rev = '';     % to leave it off
        end
        
        %% check for averaged/normalized freq file
        
        groupoutput = [foldername filesep 'omitnan_Coherence' normtype ...
          'NormBlock' num2str(b) rev stimuli{so} 'of' phases{p} '_' ...
          num2str(ratT.ratnums(r)) '_4ch.mat'];
        
        %% stop here to avoid continue
        
        if exist(groupoutput,'file')
          errlog(r).(phases{p}).(stimstr{so}).block(b).warning = ...
            ['Already averaged ' normtype ' normalized 4Ch data for ' ...
            stimuli{so} ' block ' num2str(b) ' of ' phases{p} ...
            ' for rat #' num2str(ratT.ratnums(r))];
          warning(errlog(r).(phases{p}).(stimstr{so}).block(b).warning)
          continue
        end
        
        %% loop through 3 tones per block, normalizing
        
        outputfile = cell(3,1);
        %           toi = cell(3,1);
        
        for t = 1:3
          %                       try
          %% check for normalized freq file
          
          tn = t + 3*(b-1);
          
          outputfile{t} = [foldername filesep normtype 'omitnan_Coherence_Norm' rev ...
            stimuli{so} num2str(tn) 'of' phases{p} '_' ...
            num2str(ratT.ratnums(r)) '_4ch.mat'];
          
          
          %% stop here to avoid continue
          
          if exist(outputfile{t},'file')
            errlog(r).(phases{p}).(stimstr{so}).block(b).tone(t).warning = ...
              ['Already calculated ' normtype ' normalized 4Ch data for ' ...
              stimuli{so} ' ' num2str(tn) ' of ' phases{p} ' for rat #' ...
              num2str(ratT.ratnums(r))];
            warning(errlog(r).(phases{p}).(stimstr{so}).block(b).tone(t).warning)
            continue % to next tone
          end
          
          %% check for raw freq file
          
          k = filenums(tonenum == tn);
          
          if isempty(k)
            errlog(r).(phases{p}).(stimstr{si}).block(b).tone(t).warning = ...
              ['No inputfile found for 4Ch' stimuli{si} ' #' tn ' of ' ...
              phases{p} ' for rat #' num2str(ratT.ratnums(r))];
            warning(errlog(r).(phases{p}).(stimstr{si}).block(b).tone(t).warning)
            continue
          end
          
          %% load the raw freq file
          
          rawfile = [foldername filesep ...
            fileS(filenums(tonenum == tn)).filename];
          disp(['Loading 4ch trial freq data for ' stimuli{si} ' ' ...
            num2str(tn) ' of ' phases{p} ' for rat #' ...
            num2str(ratT.ratnums(r))])
          S = load(rawfile,'freq');
          freq = S.freq;
          S = [];
          
          %% if during Acquisition, verify CS+ shocks
          
          %           if p == 1 && b > 1 && so == 1
          %             if ~all(all(isnan(freq.powspctrm(1,:,abs(freq.time - 29.75) < 0.2))))
          %               errlog(r).(phases{p}).(stimstr{so}).block(b).warning = ...
          %                 ['Shock time is NOT all NaNs for 4Ch ' stimuli{so} ' block ' ...
          %                 num2str(b) ' of ' phases{p} ' for rat #' ...
          %                 num2str(ratT.ratnums(r))];
          %               warning(errlog(r).(phases{p}).(stimstr{so}).block(b).warning)
          %               keyboard  % reverse the stimuli
          %             end
          %           end
          
          %% verify sufficiently similar foi to baseline
          
          if numel(freq.freq) ~= numel(BLfreq.freq)
            errlog(r).(phases{p}).(stimstr{si}).block(b).tone(t).warning = ...
              ['Trial freq file contains ' numel(freq.freq) ...
              ' of baseline freq''s ' numel(BLfreq.freq) ' foi'];
            warning(errlog(r).(phases{p}).(stimstr{si}).block(b).tone(t).warning)
            continue
          end
          
          if max(diff([freq.freq; BLfreq.freq],1,1)) > ...
              min(min(diff([freq.freq; BLfreq.freq],1,2)))
            errlog(r).(phases{p}).(stimstr{si}).block(b).tone(t).warning = ...
              ['Trial freq foi differs from baseline freq foi by up to ' ...
              num2str(max(diff([freq.freq; BLfreq.freq],1,1)))];
            warning(errlog(r).(phases{p}).(stimstr{si}).block(b).tone(t).warning)
            continue
          end
          
          %% calculate coherence
          
          indxIL = strcmp(bestchs(r).IL,freq.label);
          indxPrL = strcmp(bestchs(r).PrL,freq.label);
          indxBLA = strcmp(bestchs(r).BLA,freq.label);
          indxvHPC = strcmp(bestchs(r).vHPC,freq.label);
          
          freq.label{indxIL} = 'IL';
          freq.label{indxPrL} = 'PrL';
          freq.label{indxBLA} = 'BLA';
          freq.label{indxvHPC} = 'vHPC';
          
          for i = 1:size(freq.labelcmb,1)
            for j = 1:size(freq.labelcmb,2)
              indxILcmb = strcmp(bestchs(r).IL,freq.labelcmb(i,j));
              indxPrLcmb = strcmp(bestchs(r).PrL,freq.labelcmb(i,j));
              indxBLAcmb = strcmp(bestchs(r).BLA,freq.labelcmb(i,j));
              indxvHPCcmb = strcmp(bestchs(r).vHPC,freq.labelcmb(i,j));
              
              if indxILcmb == 1
                freq.labelcmb{i,j} = 'IL';
              elseif indxPrLcmb == 1
                freq.labelcmb{i,j} = 'PrL';
              elseif indxBLAcmb == 1
                freq.labelcmb{i,j} = 'BLA';
              elseif indxvHPCcmb == 1
                freq.labelcmb{i,j} = 'vHPC';
              end
            end
          end
          
          for i = 1:size(freq.labelcmb,1)
            switch freq.labelcmb{i,1}
              case 'IL'
                switch freq.labelcmb{i,2}
                  case 'PrL'
                    freq.label{i+4} = 'PrL-IL';
                  case 'BLA'
                    freq.label{i+4} = 'PrL-BLA';
                  case 'vHPC'
                    freq.label{i+4} = 'PrL-vHPC';
                end
              case 'PrL'
                switch freq.labelcmb{i,2}
                  case 'IL'
                    freq.label{i+4} = 'PrL-IL';
                  case 'BLA'
                    freq.label{i+4} = 'IL-BLA';
                  case 'vHPC'
                    freq.label{i+4} = 'IL-vHPC';
                end
              case 'BLA'
                switch freq.labelcmb{i,2}
                  case 'IL'
                    freq.label{i+4} = 'IL-BLA';
                  case 'PrL'
                    freq.label{i+4} = 'PrL-BLA';
                  case 'vHPC'
                    freq.label{i+4} = 'BLA-vHPC';
                end
              case 'vHPC'
                switch freq.labelcmb{i,2}
                  case 'IL'
                    freq.label{i+4} = 'IL-vHPC';
                  case 'BLA'
                    freq.label{i+4} = 'BLA-vHPC';
                  case 'PrL'
                    freq.label{i+4} = 'PrL-vHPC';
                end
            end
            
            % find 1st ch index
            indx1 = strcmp(freq.labelcmb(i,1),freq.label);
            
            % find 2nd ch index
            indx2 = strcmp(freq.labelcmb(i,2),freq.label);
            
            freq.powspctrm(i+4,:,:) = abs(freq.crsspctrm(i,:,:)./ ...
              sqrt(freq.powspctrm(indx1,:,:).* ...
              freq.powspctrm(indx2,:,:)));
          end
          for i = 1:size(regions,1)
            freq.label(1) = [];
            freq.powspctrm(1,:) = [];
          end
          save(outputfile,'freq')
%           %% normalize the raw freq file by baseline
%           
%           cfg = [];
%           cfg.parameter = 'powspctrm';
%           cfg.baseline = 'BLfreq';
%           
%           % normtype = 'standard';  % based on FieldTrip's cfg.baselinetype = 'dB';
%           % decibel transform of power ratio
%           cfg.baselinetype = 'relative';
%           cfg.transtype = 'no';
%           cfg.transorder = [];
%           
% %           cfg.baselinetype = 'relative';
% %           cfg.transtype = 'dB';
% %           cfg.transorder = after;
% %           
%           
%           % normtype = 'experimental';  % what I think makes the most sense
%           % cfg.baselinetype = 'z-score';
%           % cfg.transtype = 'ln';
%           % cfg.transorder = 'before';
%           cfg.outputfile = outputfile{t};
%           
%           freq = BLnorm(cfg,freq,BLfreq);
%           
%           %             catch ME
%           %               errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).tone(t).error = ME;
%           %               warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).tone(t).error.message)
%           %           end
        end
        %% verify that all 3 tones were normalized & have similar toi
        
        if ~all(cellfun(@exist,outputfile,repelem({'file'},3,1)))
          errlog(r).(phases{p}).(stimstr{si}).block(b).warning = ...
            'Not all tone trials were normalized.';
          warning(errlog(r).(phases{p}).(stimstr{si}).block(b).warning)
          continue
        end
        
        %% average the block of 3 tones, omitting NaNs, & save output
        
        cfgblock                  = [];
        cfgblock.inputfile        = outputfile;
        cfgblock.outputfile       = groupoutput;
        
        freq = meanfreqomitnan(cfgblock);
        
        %% if during Acquisition, verify CS+ shocks
        
        %         if p == 1 && b > 1 && so == 1
        %           if ~all(all(isnan(freq.powspctrm(1,:,abs(freq.time - 29.75) < 0.2))))
        %             errlog(r).(phases{p}).(stimstr{so}).block(b).warning = ...
        %               ['Shock time is NOT all NaNs for 4Ch ' stimuli{so} ' block ' ...
        %               num2str(b) ' of ' phases{p} ' for rat #' ...
        %               num2str(ratT.ratnums(r))];
        %             warning(errlog(r).(phases{p}).(stimstr{so}).block(b).warning)
        %             keyboard  % reverse the stimuli
        %           end
        %         end
        %
        %         %% check for errors & if none, remember this as a good channel
        %
        %         if any(all(isnan(freq.powspctrm(1,:,freq.time > 0 & freq.time < 30)),3))
        %           errlog(r).(phases{p}).(stimstr{so}).block(b).warning = ...
        %             'After averaging, at least one foi is all NaNs during the tone.';
        %           warning(errlog(r).(phases{p}).(stimstr{so}).block(b).warning)
        %           continue
        %         end
        %
        %         if any(all(isnan(freq.powspctrm(1,:,freq.time < 0)),3)) || ...
        %             any(all(isnan(freq.powspctrm(1,:,freq.time > 30)),3))
        %           errlog(r).(phases{p}).(stimstr{so}).block(b).warning = ...
        %             'After averaging, at least one foi is all NaNs before and/or after the tone.';
        %           warning(errlog(r).(phases{p}).(stimstr{so}).block(b).warning)
        %           continue
        %         end
        
        errlog(r).good(p,b) = ...
          errlog(r).good(p,b) + 1;
        
        %                   catch ME
        %                     errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).error = ME;
        %                     warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).error.message)
        %                   end
      end
      %               catch ME
      %                 errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).error = ME;
      %                 warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).error.message)
      %               end
    end
    %           catch ME
    %             errlog(r).(allchs{ch}).(phases{p}).error = ME;
    %             warning(errlog(r).(allchs{ch}).(phases{p}).error.message)
    %           end
  end
  %       catch ME
  %         errlog(r).(allchs{ch}).error = ME;
  %         warning(errlog(r).(allchs{ch}).error.message)
  %       end
end
%   catch ME
%     errlog(r).error = ME;
%     warning(errlog(r).error.message)
%   end


%% save errlog

disp('Analysis complete!  Saving errlog.')
save([datafolder 'errlog' datestr(now,29) ...
  '_FT_NormTFA_Diff4Fear_TEM'],'errlog')
