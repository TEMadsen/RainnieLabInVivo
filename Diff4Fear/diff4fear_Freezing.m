info_diff4fear

RatNum2Import = ratT{:,1};
RatNum2Import(ratT.excluded) = [];

CSplus = {'Event007'; 'Event008'; 'Event007'; 'Event008'; ...
    'Event007'; 'Event008'; 'Event008'; ...
    'Event007'; 'Event007'; 'Event008'; 'Event008'};
CSminus = {'Event008'; 'Event007'; 'Event008'; 'Event007'; ...
    'Event008'; 'Event007'; 'Event007'; ...
    'Event008'; 'Event008'; 'Event007'; 'Event007'};

abbr(2) = [];

RawFrz = cell(numel(RatNum2Import),numel(abbr),numel(stimuli));
% stimuli 1 = CS+; stimuli 2 = CS-
for r = 1:numel(RatNum2Import)
    if strcmp(CSplus{r},'Event007') == 1
        event = {'Event 7', 'Event 8'};
    elseif strcmp(CSplus{r},'Event008') == 1
        event = {'Event 8', 'Event 7'};
    end
    for p = 1:numel(abbr)
        for s = 1:numel(stimuli)
            %% Import the data
            [~, ~, raw] = xlsread(['S:\Teresa\Behavior\FreezeScan\Diff4Fear\' int2str(RatNum2Import(r)) '_' abbr{p} '.xlsx'],event{s},'G5:H31');
            
            %% Create output variable
            data = reshape([raw{:}],size(raw));
            
            %% Create table
            fz = table;
            
            %% Allocate imported array to column variable names
            fz.Percent = data(:,1);
            fz.AverageMotion = data(:,2);
            RawFrz{r,p,s} = fz;
            
            %% Clear temporary variables
            clearvars data raw fz;
            
        end
    end
end

%% create matrices for freeziing during CS+ and CS-

FrzDurCSPlus = NaN(numel(RatNum2Import),numel(abbr), 9);
FrzDurCSMinus = NaN(numel(RatNum2Import),numel(abbr), 9);

for r = 1:numel(RatNum2Import)
    for p = 1:numel(abbr)
        for t = 1:9
            FrzDurCSPlus(r,p,t) = RawFrz{r,p,1}.Percent(t*3-1);
            FrzDurCSMinus(r,p,t) = RawFrz{r,p,2}.Percent(t*3-1);
        end
    end
end

FCPlus = squeeze(FrzDurCSPlus(:,1,:));
FCMinus = squeeze(FrzDurCSMinus(:,1,:));
EXT1Plus = squeeze(FrzDurCSPlus(:,2,:));
EXT1Minus = squeeze(FrzDurCSMinus(:,2,:));
EXT2Plus = squeeze(FrzDurCSPlus(:,3,:));
EXT2Minus = squeeze(FrzDurCSMinus(:,3,:));

%% create matrices for freeziing during CS+ and CS- normalized by pretone

FrzDurCSPlusDiff = NaN(numel(RatNum2Import),numel(abbr), 9);
FrzDurCSMinusDiff = NaN(numel(RatNum2Import),numel(abbr), 9);

for r = 1:numel(RatNum2Import)
    for p = 1:numel(abbr)
        for t = 1:9
            FrzDurCSPlusDiff(r,p,t) = RawFrz{r,p,1}.Percent(t*3-1)-RawFrz{r,p,1}.Percent(t*3-2);
            FrzDurCSMinusDiff(r,p,t) = RawFrz{r,p,2}.Percent(t*3-1)-RawFrz{r,p,2}.Percent(t*3-2);
        end
    end
end

FCPlusDiff = squeeze(FrzDurCSPlusDiff(:,1,:));
FCMinusDiff = squeeze(FrzDurCSMinusDiff(:,1,:));
EXT1PlusDiff = squeeze(FrzDurCSPlusDiff(:,2,:));
EXT1MinusDiff = squeeze(FrzDurCSMinusDiff(:,2,:));
EXT2PlusDiff = squeeze(FrzDurCSPlusDiff(:,3,:));
EXT2MinusDiff = squeeze(FrzDurCSMinusDiff(:,3,:));


