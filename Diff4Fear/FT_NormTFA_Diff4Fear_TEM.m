%% Normalizes trial spectrograms & averages them in blocks of 3

CleanSlate  % provides a clean slate to start working with new data/scripts
% by clearing all variables, figures, and the command window

info_diff4fear            % load all background info & final parameters
errlog = cell2struct(cell(size(ratT,1),numel(allchs)),allchs,2); 	% MEs saved w/in nested structure

% rats 357 & 359 need their CS+/- reversed between input & output files
revCS = false(size(ratT,1),1);
revCS([5 7]) = true;

%% to normalize only specific file(s), in a specific way

zthresh = 15;           % which artifact threshold to use the data from
spectype = 'faster';
% normtype = 'standard';  % based on FieldTrip's cfg.baselinetype = 'dB';
% normtype = 'z-score'; 	% my original proposal
normtype = 'z_dB';  % same as my original proposal, but using the familiar dB transform
redo = false;            % to ignore pre-existing outputs and overwrite them

%% full loop

for r = find(~ratT.excluded)'
  %   try
  %% find available spectrogram files
  
  ratfolder = [datafolder num2str(ratT.ratnums(r)) filesep];
  filelist = what(ratfolder);
  
  %% preallocate variables
  
  pndx = cell(numel(filelist.mat),numel(phases));
  chndx = cell(numel(filelist.mat),numel(allchs));
  sndx = cell(numel(filelist.mat),numel(stimuli));
  
  %% find indices of relevant info in all filenames
  
  for p = 1:numel(phases)
    pndx(:,p) = strfind(filelist.mat,phases{p});
  end
  for ch = 1:numel(allchs)
    chndx(:,ch) = strfind(filelist.mat,allchs{ch});
  end
  for si = 1:numel(stimuli)
    sndx(:,si) = strfind(filelist.mat,stimuli{si});
  end
  tndx = strfind(filelist.mat,'of');      % to find tone #
  bndx = strfind(filelist.mat,'Block');   % to find block #
  zndx = strfind(filelist.mat,['_z' num2str(zthresh) '_']); % only finds desired files
  stndx = strfind(filelist.mat,spectype); % only finds desired files
  ntndx = strfind(filelist.mat,normtype); % only finds desired files
  smndx = strfind(filelist.mat,'Smooth'); % to EXCLUDE already smoothed files
  rndx = strfind(filelist.mat,'Rev');   % to indicate CS+/- have already been reversed
  
  %% put file info in a more legible format
  
  fileS = cell2struct(...
    [filelist.mat, pndx, chndx, sndx, tndx, bndx, zndx, stndx, ntndx, smndx, rndx], ...
    [{'filename'}; phases(:); allchs; ...
    {'CSplus'; 'CSminus'; 'tndx'; 'bndx'; 'zndx'; 'stndx'; 'ntndx'; 'smndx'; 'rndx'}], 2);
  
  %% loop through channels
  
  %   for rrc = 1:numel(regions)
  %     for ch = find(~cellfun(@isempty,strfind(allchs,bestchs(r).(regions{rrc}))))
  for ch = 1:numel(allchs)
    %       try
    %% assume channels are bad, add 1 for each good stimulus type
    
    errlog(r).(allchs{ch}).good = zeros(numel(phases),3);  % 3 blocks/phase
    
    %% identify baseline file
    
    filenums = find(~arrayfun(@(x) isempty(x.(allchs{ch})),fileS) & ...
      ~arrayfun(@(x) isempty(x.Baseline),fileS) & ...
      ~arrayfun(@(x) isempty(x.zndx),fileS) & ...
      ~arrayfun(@(x) isempty(x.stndx),fileS));
    
    %% stop here to avoid continue
    
    if numel(filenums) < 1
      errlog(r).(allchs{ch}).Baseline.warning = ...
        ['No baseline freq file found for rat #' ...
        num2str(ratT.ratnums(r)) ', ' allchs{ch}];
      warning(errlog(r).(allchs{ch}).Baseline.warning)
      continue % to next ch
    elseif numel(filenums) > 1
      errlog(r).(allchs{ch}).Baseline.warning = ...
        ['More than one baseline freq file found for rat #' ...
        num2str(ratT.ratnums(r)) ', ' allchs{ch}];
      error(errlog(r).(allchs{ch}).Baseline.warning)
      %       continue
      %           keyboard  % select one? narrow criteria?
    else
      %% load baseline freq file - hopefully faster to load once per rat
      % and channel than to use cfg.baselinefile with BLnorm
      
      baselinefile = [ratfolder fileS(filenums).filename];
      disp(['Loading baseline freq data for rat #' ...
        num2str(ratT.ratnums(r)) ', ' allchs{ch}])
      S = load(baselinefile,'freq');
      BLfreq = S.freq;
      S = [];
      
      %       figure(ch); clf;
      %       imagesc(BLfreq.time, 1:numel(BLfreq.freq), squeeze(BLfreq.powspctrm))
      %       title('raw baseline freq input')
    end
    
    %% check for all NaNs in any frequency
    
    if any(all(isnan(BLfreq.powspctrm),3))
      errlog(r).(allchs{ch}).Baseline.warning = ...
        'At least one foi is all NaNs in baseline file.';
      warning(errlog(r).(allchs{ch}).Baseline.warning)
      continue % to next ch
    end
    
    %% loop through all phases of fear conditioning & extinction
    
    for p = [1 3 4] % 1:numel(phases)
      %           try
      %%  & both stimulus types
      
      for si = 1:numel(stimuli)   % input data labeled as stimulus type
        %               try
        if revCS(r)
          so = find(~ismember([1 2],si));   % output data should have reversed title
        else
          so = si;  % output data will be labeled with same stimulus type as input
        end
        
        % find filenames including all identifying info desired
        filenums = find(~arrayfun(@(x) isempty(x.(allchs{ch})),fileS) & ...
          ~arrayfun(@(x) isempty(x.(phases{p})),fileS) & ...
          ~arrayfun(@(x) isempty(x.(stimstr{si})),fileS) & ...
          ~arrayfun(@(x) isempty(x.zndx),fileS) & ...
          ~arrayfun(@(x) isempty(x.stndx),fileS));  % spec type
        
        %% stop here to avoid continue
        
        if numel(filenums) < 3   % spectrograms don't exist for all tones in any block
          errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).warning = ...
            ['Fewer than 3 ' stimuli{si} ...
            ' freq files found during ' phases{p} ' for rat #' ...
            num2str(ratT.ratnums(r)) ', ' allchs{ch}];
          warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).warning)
          continue % next stimtype
        end
        
        %% loop through all freq files, IDing tone #
        
        tonenum = NaN(size(filenums));
        for k = 1:numel(filenums)
          tonenum(k) = str2double(fileS(filenums(k)).filename(numel(stimuli{si})+1:...
            fileS(filenums(k)).tndx-1));
        end
        
        %% check for duplicates or >9
        
        if numel(tonenum) > numel(unique(tonenum))
          errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).warning = ['Duplicate ' ...
            stimuli{si} ' tone #s found during ' phases{p} ' for rat #' ...
            num2str(ratT.ratnums(r)) ', ' allchs{ch}];
          error(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).warning)
          %                             continue
          %           keyboard  % select one? narrow criteria?
        end
        
        if max(tonenum) > 9
          errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).warning = [stimuli{si} ...
            ' tone #s >9 found during ' phases{p} ' for rat #' ...
            num2str(ratT.ratnums(r)) ', ' allchs{ch}];
          warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).warning)
          %           continue
          %           %% alternate approach (examine data!)
          %           for tn = unique(tonenum)'
          %             %% load the raw freq file
          %
          %             rawfile = [ratfolder ...
          %               fileS(filenums(tonenum == tn)).filename];
          %             disp(['Loading trial freq data for ' stimuli{si} ' ' ...
          %               num2str(tn) ' of ' phases{p} ' for rat #' ...
          %               num2str(ratT.ratnums(r)) ', ' allchs{ch}])
          %             S = load(rawfile,'freq');
          %             freq = S.freq;
          %             S = [];
          %
          %             %% plot spectrogram
          %
          %             fig = figure(tn); clf; fig.WindowStyle = 'docked';
          %             imagesc(freq.time, 1:numel(freq.freq), squeeze(10*log10(freq.powspctrm)))
          %             axis xy
          %             title('rawfile freq input (10*log10(uv^2/Hz))')
          %
          %           end
          %           keyboard  % select one? narrow criteria?
          %% this step automatically selects 9 tones/phase, verified okay for all rats
          if p == 1 % for conditioning, last 9 tones are most important
            filenums(tonenum < max(tonenum)-8) = [];
            tonenum(tonenum < max(tonenum)-8) = [];
            tonenum = tonenum-min(tonenum)+1;
          else % otherwise, first 9 tones are most important
            filenums(tonenum > 9) = [];
            tonenum(tonenum > 9) = [];
          end
        end
        
        %% loop through blocks of 3 tones, averaging
        
        for b = 1:3 % [1 3]
          %                   try
          if revCS(r)
            rev = 'Rev';  % to mark file as having reversed CS+/-
          else
            rev = '';     % to leave it off
          end
          
          %% check for averaged/normalized freq file
          
          groupoutput = [ratfolder normtype ...
            'NormBlock' num2str(b) rev stimuli{so} 'of' phases{p} '_' ...
            num2str(ratT.ratnums(r)) allchs{ch} '.mat'];
          
          %% stop here to avoid continue
          
          if exist(groupoutput,'file')
            errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).warning = ...
              ['Already averaged ' normtype ' normalized data for ' ...
              stimuli{so} ' block ' num2str(b) ' of ' phases{p} ...
              ' for rat #' num2str(ratT.ratnums(r)) ', ' allchs{ch}];
            warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).warning)
            if ~redo
              continue % to next block
            end
          end
          
          %% loop through 3 tones per block, normalizing
          
          outputfile = cell(3,1);
          %           toi = cell(3,1);
          
          for t = 1:3
            %                       try
            %% check for normalized freq file
            
            tn = t + 3*(b-1);
            
            outputfile{t} = [ratfolder normtype 'Norm' rev ...
              stimuli{so} num2str(tn) 'of' phases{p} '_' ...
              num2str(ratT.ratnums(r)) allchs{ch} '.mat'];
            
            %% stop here to avoid continue
            
            if exist(outputfile{t},'file')
              errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).tone(t).warning = ...
                ['Already calculated ' normtype ' normalized data for ' ...
                stimuli{so} ' ' num2str(tn) ' of ' phases{p} ' for rat #' ...
                num2str(ratT.ratnums(r)) ', ' allchs{ch}];
              warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).tone(t).warning)
              if ~redo
                continue % to next tone
              end
            end
            
            %% check for raw freq file
            
            k = filenums(tonenum == tn);
            
            if isempty(k)
              errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).tone(t).warning = ...
                ['No inputfile found for ' stimuli{si} ' #' tn ' of ' ...
                phases{p} ' for rat #' num2str(ratT.ratnums(r)) ', ' ...
                allchs{ch}];
              warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).tone(t).warning)
              continue  % to next tone
            end
            
            %% load the raw freq file
            
            rawfile = [ratfolder ...
              fileS(filenums(tonenum == tn)).filename];
            disp(['Loading trial freq data for ' stimuli{si} ' ' ...
              num2str(tn) ' of ' phases{p} ' for rat #' ...
              num2str(ratT.ratnums(r)) ', ' allchs{ch}])
            S = load(rawfile,'freq');
            freq = S.freq;
            S = [];
            
            %% stop here to avoid continue
            
            if all(isnan(freq.powspctrm(:)))
              errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).tone(t).warning = ...
                ['Spectrogram is all NaNs for ' stimuli{so} ' ' ...
                num2str(tn) ' of ' phases{p} ' for rat #' ...
                num2str(ratT.ratnums(r)) ', ' allchs{ch}];
              warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).tone(t).warning)
              continue % to next tone
            elseif any(all(isnan(freq.powspctrm),3),2) % keeping this separate so I can comment it out if necessary
              errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).tone(t).warning = ...
                ['Spectrogram has all NaNs for at least one frequency during ' ...
                stimuli{so} ' ' num2str(tn) ' of ' phases{p} ...
                ' for rat #' num2str(ratT.ratnums(r)) ', ' allchs{ch}];
              warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).tone(t).warning)
              continue % to next tone
            end
            
            %               figure(tn); clf;
            %               imagesc(freq.time, 1:numel(freq.freq), squeeze(10*log10(freq.powspctrm)))
            %               axis xy
            %               title('rawfile freq input (10*log10(uv^2/Hz))')
            %             keyboard % pause to check input
            
            %% verify that shocks occur on CS+ trials during Acquisition and only then
            
            if all(all(isnan(freq.powspctrm(1,:,abs(freq.time - 29.75) < 0.5)))) % 29.25-30.25 because there's always plenty of artifact padding
              if ~(p == 1 && b > 1 && so == 1) % if there are artifacts at shock time, verify they're not shocks
                errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).tone(t).warning = ...
                  ['Shock time is all NaNs for ' stimuli{so} ' ' ...
                  num2str(tn) ' of ' phases{p} ' for rat #' ...
                  num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).tone(t).warning)
                
                %                 fig = figure(tn); clf; fig.WindowStyle = 'docked';
                %                 imagesc(freq.time, 1:numel(freq.freq), squeeze(10*log10(freq.powspctrm)))
                %                 axis xy
                %                 title('dB transformed rawfile freq input (10*log10(uv^2/Hz))')
                %                 keyboard % visual inspection may be the easiest way to verify this (raw data would be more obvious)
              end
            elseif p == 1 && b > 1 && so == 1 % if there are no shock artifacts when there should be
              errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).tone(t).warning = ...
                ['Shock time is NOT all NaNs for ' stimuli{so} ' ' ...
                num2str(tn) ' of ' phases{p} ' for rat #' ...
                num2str(ratT.ratnums(r)) ', ' allchs{ch}];
              error(errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).tone(t).warning)
              %               keyboard  % reverse the stimuli
            end
            
            %% verify sufficiently similar foi to baseline
            
            if numel(freq.freq) ~= numel(BLfreq.freq)
              errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).tone(t).warning = ...
                ['Trial freq file contains ' numel(freq.freq) ...
                ' of baseline freq''s ' numel(BLfreq.freq) ' foi'];
              warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).tone(t).warning)
              continue % to next tone
            end
            
            if max(diff([freq.freq; BLfreq.freq],1,1)) > ...
                min(min(diff([freq.freq; BLfreq.freq],1,2)))/2
              errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).tone(t).warning = ...
                ['Trial freq foi differs from baseline freq foi by up to ' ...
                num2str(max(diff([freq.freq; BLfreq.freq],1,1)))];
              warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).tone(t).warning)
              continue % to next tone
            end
            
            %% normalize the raw freq file by baseline
            
            cfg = [];
            cfg.parameter   = 'powspctrm';
            cfg.baseline    = 'BLfreq';
            cfg.tol         = 0.01; % Hz diff between fois
            
            switch normtype
              case 'standard'  % based on FieldTrip's cfg.baselinetype = 'dB';
                % decibel transform of power ratio
                cfg.baselinecent = 'mean';
                cfg.baselinetype = 'relative';
                cfg.transtype = 'dB';
                cfg.transorder = 'after';
                
              case 'z-score'  % my original proposal
                cfg.baselinecent = 'mean';
                cfg.baselinetype = 'z-score';
                cfg.transtype = 'ln';   % arbitrarily avoiding confusion with familiar dB
                cfg.transorder = 'before';
                
              case 'z_dB'  % my revised proposal
                cfg.baselinecent = 'mean';
                cfg.baselinetype = 'z-score';
                cfg.transtype = 'dB';   % capitalizes on familiarity of dB transform
                cfg.transorder = 'before';
                
              otherwise
                error('You need to define this normtype here.')
                
            end
            
            cfg.outputfile = outputfile{t};
            
            normfreq = BLnorm(cfg,freq,BLfreq);
            
            %               figure(3 + tn); clf;
            %               imagesc(normfreq.time, 1:numel(normfreq.freq), squeeze(normfreq.powspctrm))
            %               title('baseline normalized trial freq')
            %               keyboard % pause to check output
            %
            %             catch ME
            %               errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).tone(t).error = ME;
            %               warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).tone(t).error.message)
            %           end
          end % tone
          %% verify that all 3 tones were normalized & have similar toi
          
          if ~all(cellfun(@exist,outputfile,repelem({'file'},3,1)))
            errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).warning = ...
              'Not all tone trials were normalized.';
            warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).warning)
            continue % to next block
          end
          
          %% average the block of 3 tones, omitting NaNs, & save output
          
          cfgblock              = [];
          cfgblock.inputfile    = outputfile; % cell containing tone output files, which becomes block inputs
          cfgblock.outputfile   = groupoutput;
          
          freq = meanfreqomitnan(cfgblock);
          
          %             figure(7);
          %             imagesc(freq.time, 1:numel(freq.freq), squeeze(freq.powspctrm))
          %             title('3 tone blocks, averaged, normalized freq')
          %             keyboard % pause to check output
          
          % It's appropriately group-averaged, with NaNs only appearing
          % where all tone-trials are NaNs.  The negative-biased color
          % scale occurs as a result of cfg.baselinetype = 'dB'; because
          % mean(baselinedata) is unduly influenced by relatively rare,
          % extremely high power values.
          
          %% check for errors & if none, remember this as a good channel
          
          if any(all(isnan(freq.powspctrm(1,:,freq.time > 0 & freq.time < 30)),3))
            errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).warning = ...
              'After averaging, at least one foi is all NaNs during the tone.';
            warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).warning)
            continue % to next block
          end
          
          if any(all(isnan(freq.powspctrm(1,:,freq.time < 0)),3)) || ...
              any(all(isnan(freq.powspctrm(1,:,freq.time > 30)),3))
            errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).warning = ...
              'After averaging, at least one foi is all NaNs before and/or after the tone.';
            warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{so}).block(b).warning)
            continue % to next block
          end
          
          errlog(r).(allchs{ch}).good(p,b) = ...
            errlog(r).(allchs{ch}).good(p,b) + 1;
          
          %             %                   catch ME
          %             %                     errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).error = ME;
          %             %                     warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).block(b).error.message)
          %             %                   end
        end % block
        %               catch ME
        %                 errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).error = ME;
        %                 warning(errlog(r).(allchs{ch}).(phases{p}).(stimstr{si}).error.message)
        %               end
      end % stimtype
      %           catch ME
      %             errlog(r).(allchs{ch}).(phases{p}).error = ME;
      %             warning(errlog(r).(allchs{ch}).(phases{p}).error.message)
      %           end
    end % phase
    %       catch ME
    %         errlog(r).(allchs{ch}).error = ME;
    %         warning(errlog(r).(allchs{ch}).error.message)
    %       end % try
  end % channels
  %   end % regions
  
  %   for ch = 1:numel(allchs)
  %     fig = figure(ch);  fig.WindowStyle = 'docked';
  %     ax = gca; ax.CLim = [0 1e6];
  %   end
  %   keyboard % pause to review baseline freq inputs
  %
  %   catch ME
  %     errlog(r).error = ME;
  %     warning(errlog(r).error.message)
  %   end % try
end % rat

%% save errlog

disp('Analysis complete!  Saving errlog.')
save([datafolder 'errlog' datestr(now,29) ...
  '_FT_NormTFA_Diff4Fear_TEM'],'errlog')

%% display errlog (might be better to make this a separate function)
% unfold(errlog) does the same thing, but both are too much to read

for r = 1:numel(errlog)
  fields{1} = fieldnames(errlog(r));
  for ch = 1:numel(fields{1})
    if ~isstruct(errlog(r).(fields{1}{ch}))
      disp(['errlog(' num2str(r) ').' fields{1}{ch} ' = '])
      disp(errlog(r).(fields{1}{ch}))
      continue
    end
    fields{2} = fieldnames(errlog(r).(fields{1}{ch}));
    for p = 1:numel(fields{2})
      if ~isstruct(errlog(r).(fields{1}{ch}).(fields{2}{p}))
        disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} ' = '])
        disp(errlog(r).(fields{1}{ch}).(fields{2}{p}))
        continue
      end
      fields{3} = fieldnames(errlog(r).(fields{1}{ch}).(fields{2}{p}));
      for s = 1:numel(fields{3})
        if ~isstruct(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}))
          disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} ' = '])
          disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}))
          continue
        end
        fields{4} = fieldnames(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}));
        if ischar(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4}))
          disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} '.' fields{4}{f4} ' = '])
          disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4}))
          continue
        end
        % indexing changes from multiple fields of a scalar structure to one field of a non-scalar structure
        for b = 1:numel(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4}))
          if ~isstruct(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b))
            disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} '.' fields{4}{f4} '(' num2str(b) ') = '])
            disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b))
            continue
          end
          fields{5} = fieldnames(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b));
          for f5 = 1:numel(fields{5})
            if ischar(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5}))
              disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} '.' fields{4}{f4} '(' num2str(b) ').' fields{5}{f5} ' = '])
              disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5}))
              continue
            end
            for t = 1:numel(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5}))
              if ~isstruct(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5})(t))
                disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} '.' fields{4}{f4} '(' num2str(b) ').' fields{5}{f5} '(' num2str(t) ') = '])
                disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5})(t))
                continue
              end
              fields{6} = fieldnames(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5})(t));
              for f6 = 1:numel(fields{6})
                if ~isstruct(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5})(t).(fields{6}{f6}))
                  disp(['errlog(' num2str(r) ').' fields{1}{ch} '.' fields{2}{p} '.' fields{3}{s} '.' fields{4}{f4} '(' num2str(b) ').' fields{5}{f5} '(' num2str(t) ').' fields{6}{f6} ' = '])
                  disp(errlog(r).(fields{1}{ch}).(fields{2}{p}).(fields{3}{s}).(fields{4}{f4})(b).(fields{5}{f5})(t).(fields{6}{f6}))
                  continue
                end
                keyboard % dbcont if okay to continue without drilling down further into structure
              end
            end
          end
        end
      end
    end
  end
end
