%% Load all data from -aligned.nex files into FieldTrip format & merge
% into 1 dataset per rat, using new trialfun_TEM.m and supporting a cell
% array of strings for multiple filenames within one phase (to avoid merged
% NEX files, which are hard to read correctly).
%%% CURRENT AS OF 11/2/16 %%%
%%% realized I forgot to leave room for padding 10/31/16 - need to rerun

info_diff4fear  % load all background info (filenames, etc)
%#ok<*SAGROW> many variables seem to grow when they're preallocated here
redo = true;    % set true to overwrite any old versions of outputfiles

errcnt = false(size(ratT,1),numel(phases),2);
errlog = cell(size(ratT,1),numel(phases),2);

%% find phase # & tone # in columns of data.trialinfo

pndx = find(contains(infotype,'phase', 'IgnoreCase',true));
tndx = find(contains(infotype,'trial', 'IgnoreCase',true));

%% full loop

for r = 1:size(ratT,1)
  % Raw LFP data divided into long, non-overlapping tone-triggered
  % trials, merged across all recordings for each rat
  outputfile{2} = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'RawLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  if exist(outputfile{2},'file') && ~redo
    disp([outputfile{2} ' already exists.  Skipping rat.'])
  else
    data2merge = cell(1);
    next = 1;   % put 1st recording's data in 1st cell
    for p = 1:numel(phases)   % includes baseline
      if isempty(nexfile{r,p})   % in case of missing files
        continue  % skip to next phase
      end
      if ~iscell(nexfile{r,p})  % in case of multiple files per phase
        nexfile{r,p} = nexfile(r,p);  % convert to cellstr
      end
      for fn = 1:numel(nexfile{r,p})  % file number w/in a phase
        try
          %% define output filenames, etc.
          
          % Raw LFP data divided into long, non-overlapping tone-triggered
          % trials
          outputfile{1} = [datafolder num2str(ratT.ratnums(r)) ...
            filesep 'RawLFPTrialData_' ...
            num2str(ratT.ratnums(r)) phases{p} num2str(fn) '.mat'];
          
          %% load file if it exists
          
          if ~exist(outputfile{1},'file') || redo
            disp('starting from scratch');
            bestfile = 0;
            clearvars data
          else
            disp(['loading file ' outputfile{1}]);
            load(outputfile{1});
            bestfile = 1;
          end
          
          %% no processing done yet
          if bestfile == 0
            if ~exist(nexfile{r,p}{fn},'file')
              error(['Input file ' nexfile{r,p}{fn} ' does not exist.']);
            end
            %% define trials that cover whole recording
            
            cfg                         = [];
            cfg.dataset                 = nexfile{r,p}{fn};
            cfg.trialfun                = 'trialfun_TEM';
            
            if p == 2 || p == 5 % context extinction & baseline have no tones
              %% import all data as 1 continuous trial
              cfg.continuous              = 'yes';
            else
              %% import data as long tone-triggered trials
              cfg.trialdef.minsep         = 60;   % all in seconds
              cfg.trialdef.prestim        = 45;   % leave an extra 15s for padding/smoothing
              cfg.trialdef.poststim       = 75;
              cfg.trialdef.expandtrials   = true;
              
              cfg.trialdef.eventtype      = ratT.CSplus{r};
              cfg = ft_definetrial(cfg);
              trlplus = [cfg.trl ones(size(cfg.trl,1),1)];
              
              cfg                         = rmfield(cfg,'trl');
              cfg.trialdef.eventtype      = ratT.CSminus{r};
              cfg = ft_definetrial(cfg);
              trlminus = [cfg.trl 2*ones(size(cfg.trl,1),1)];
              
              % concatenate CS+ & CS- trials & sort trials by trloff
              trl = sortrows([trlplus; trlminus],3);
              
              %% remove overlaps between CS+ & CS- trials
              % change trial with largest trloff to standard pre-stim period
              change = trl(end,3) - trl(1,3); % # of samples to shift trloff
              trl(1,3) = trl(end,3);  % equivalent to trl(1,3) + change
              trl(1,1) = trl(1,1) + change;   % shift trlbeg by same amount
              % resort trials by trlbeg & add phase #
              trl = sortrows([trl p*ones(size(trl,1),1)]);
              
              % end each trial except the last 1 sample before beginning of the next
              for tr = 1:(size(trl,1)-1)
                trl(tr,2) = trl(tr+1,1)-1;
              end
              
              cfg.trl = trl;
            end
            
            %% load continuous LFP data, all channels
            
            cfg.channel     = 'AD*';
            
            data = ft_preprocessing(cfg);
            
            %% add elec positions & label trialinfo columns
            
            elecfile = [configfolder 'elec_' MWA '_' ratT.side{r} '.mat'];
            load(elecfile);
            data.elec = elec;
            
            data.infotype = infotype(1:3); % 'OrigTrialNum','StimulusType','PhaseNum'
            
            if ~isfield(data,'trialinfo') && numel(data.trial) == 1
              data.trialinfo = [1 0 p];   % place holders for non-existant trial # & stimulus type
            end
            
            %% mark NaNs as artifacts - this is way overcomplicated,
            % at least for the common case in which there is only 1 block
            % of NaNs in a dataset, but it works, so leave it alone
            
            artifact.nan = [];
            
            for tr = 1:numel(data.trial)
              % identify whether there are any NaNs at each timepoint
              tnan = any(isnan(data.trial{tr}));
              
              if any(tnan)
                % determine the file sample #s for this trial
                trsamp = data.sampleinfo(tr,1):data.sampleinfo(tr,2);
                
                while any(tnan)
                  % start from the end so sample #s don't shift
                  endnan = find(tnan,1,'last');
                  tnan = tnan(1:endnan);  % remove any non-NaNs after this
                  
                  % find last non-NaN before the NaNs
                  beforenan = find(~tnan,1,'last');
                  
                  if isempty(beforenan)   % if no more non-NaNs
                    begnan = 1;
                    tnan = false;   % while loop ends
                  else  % still more to remove - while loop continues
                    begnan = beforenan + 1;
                    tnan = tnan(1:beforenan);  % remove the identified NaNs
                  end
                  
                  % identify file sample #s that correspond to beginning
                  % and end of this chunk of NaNs and append to
                  % artifact.nan
                  artifact.nan = [artifact.nan; ...
                    trsamp(begnan) trsamp(endnan)];
                end   % while any(tnan)
              end   % if any(tnan)
            end   % for tr = 1:numel(data.trial)
            
            %% remove NaNs from data & save 1st FT format data file
            
            cfg                         = [];
            cfg.artfctdef.nan.artifact  = artifact.nan;
            cfg.artfctdef.reject        = 'partial';
            cfg.outputfile              = outputfile{1};
            
            data = ft_rejectartifact(cfg,data);
            
            bestfile = 1;
          end
          
          %% tone-triggered trials saved
          if bestfile == 1
            if fn > 1   % if this isn't the first file in this phase
              if p == data2merge{end}.trialinfo(end,pndx) % and previous trial belongs to same phase
                data.trialinfo(:,tndx) = data.trialinfo(:,tndx) + ...
                  max(data2merge{end}.trialinfo(:,tndx)); % adjust trial #s
              end
            end
            data2merge{next} = data;
            next = next + 1;  % next recording will be saved in next cell
          end
        catch ME
          errcnt(r,p,fn) = true;
          errlog{r,p,fn} = ME;
          warning(['Error for rat #' num2str(ratT.ratnums(r)) ...
            ', ' phases{p} ' ' num2str(fn) ':'])
          getReport(ME)
          continue  % skips to next file
        end
      end
      %% check for correct # of trials per phase
      
      numtrls = 0;   % to start
      
      for dn = 1:numel(data2merge)
        numtrls = numtrls + sum(data2merge{dn}.trialinfo(:,pndx) == p);
      end
      
      if ~isempty(data2merge{end}) && numtrls ~= expected(p)
        error('Incorrect # of trials for this phase')
      end
      
    end
  end
  %% merge the data from all recordings
  
  if any(any(errcnt(r,[1 3 4 5],:))) % don't worry if context (p = 2) is missing
    warning(['errors were found with rat #' ...
      num2str(ratT.ratnums(r)) ', data not merged'])
    continue  % skip to next rat
  end
  
  cfg             = [];
  cfg.outputfile  = outputfile{2};
  
  if isempty(data2merge{1})
    warning(['No data present for rat #' num2str(ratT.ratnums(r)) ...
      '.  No merge performed.']);
  elseif numel(data2merge) == 1
    warning(['Only 1 phase of data present for rat #' ...
      num2str(ratT.ratnums(r)) '.  No merge performed.']);
  else
    merged = ft_appenddata(cfg, data2merge{:});
  end
  
  clearvars data data2merge merged
end

%% display errors again

if sum(errcnt(:)) == 0
  disp('No errors found!')
else
  for r = 1:size(ratT,1)
    disp([num2str(sum(sum(errcnt(r,:,:)))) ' errors found for rat #' ...
      num2str(ratT.ratnums(r))])
    for p = 1:numel(phases)
      for fn = 1:size(errcnt,3)
        if errcnt(r,p,fn)
          warning(['Error for rat #' num2str(ratT.ratnums(r)) ', ' ...
            phases{p} ' ' num2str(fn) ':'])
          getReport(errlog{r,p,fn})
        end
      end
    end
  end
end