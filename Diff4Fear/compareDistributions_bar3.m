%% display distributions, means, & stddevs, before & after log transform

info_diff4fear;
r=5; ch=13;
baselinefile = 'S:\Teresa\Analyses\TempData\Diff4Fear\347\Baseline_347AD13_z15_fasterwavSpectrogram.mat';
      
%% load baseline freq file
      
disp(['Loading baseline freq data for rat #' ...
  num2str(ratT.ratnums(r)) ', ' allchs{ch}])
S = load(baselinefile,'freq');
BLfreq = S.freq;
S = [];

%% plot histograms of raw power for each frequency

bins = linspace(0, max(BLfreq.powspctrm(:))+1, 201);
N = NaN(numel(BLfreq.freq), numel(bins)-1);
rawmeans = mean(BLfreq.powspctrm, 3, 'omitnan');
rawstddevs = std(BLfreq.powspctrm, [], 3, 'omitnan');

for f = 1:numel(BLfreq.freq)
  N(f,:) = histcounts(BLfreq.powspctrm(1,f,:), bins);
end

figure(1); clf;
bar3(N); hold all;
