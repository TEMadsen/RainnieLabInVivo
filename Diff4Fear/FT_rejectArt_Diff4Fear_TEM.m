%% Remove artifacts via NEW METHOD:
% 1)  remove channels with more noise than neural signal (extreme variance
%     between motion artifacts and/or account for disproportionate amount
%     of clipping)
% 2)  remove times containing large, jump, and/or clipping artifacts

info_diff4fear                   % load all background info (filenames, etc)

if all(~ratT.finalch)   % none have final channel selections
  minampl = NaN(numel([lftchs rtchs]),size(ratT,1));
  maxdur = NaN(numel([lftchs rtchs]),size(ratT,1));
  totaldur = NaN(numel([lftchs rtchs]),size(ratT,1));
  z = NaN(numel([lftchs rtchs]),size(ratT,1));
end
%% full loop

next = 1;   % figure #

for r = 1:size(ratT,1)
  % Raw LFP data divided into long, non-overlapping tone-triggered
  % trials, merged across all recordings for each rat
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'RawLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  % merged LFPs with dead channels, large & clipping artifacts removed
  outputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'LFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  if ~exist(outputfile,'file')
    %% check for input data
    
    if ~exist(inputfile,'file')
      warning(['Skipping rat #' num2str(ratT.ratnums(r)) ' because ' ...
        inputfile ' was not found. Run FT_preproc_Diff4Fear_TEM.m first.'])
      continue
    end
    
    %% load input data
    
    disp(['Loading ' inputfile])
    load(inputfile)
    
    if numel(data.trial) ~= 55
      warning(['Rat #' num2str(ratT.ratnums(r)) ' has ' ...
        num2str(numel(data.trial)) ' original trials.'])
    end
    
    %% separate to avoid continue when running cells manually
    if ratT.finalch(r)
      disp('Using previously selected good channels')
    else
      %% divide 154s core of each trial into 22s chunks
      % keeping shock artifact separate from tone response
      
      cfg           = [];
      cfg.toilim    = [-60 94];
      data1 = ft_redefinetrial(cfg, data);
      
      cfg           = [];
      cfg.length    = 22;
      cfg.overlap   = 0;
      data1 = ft_redefinetrial(cfg, data1);
      
      %% loop through all rats to verify clipping parameters
      if all(~ratT.finalch)   % none have final channel selections
        %% take 1st derivative of signal
        
        cfg = [];
        cfg.absdiff = 'yes';
        
        d1dat = ft_preprocessing(cfg,data1);
        
        %   %% label shock trials
        %
        %   alltrials = 1:numel(data1.trial);
        %   shocktr = false(size(alltrials));
        %
        %   for tone = 11:17  % in phase 1
        %     trls = find(data1.trialinfo(:,1) == tone & data1.trialinfo(:,2) == 1);
        %     for tr = trls'
        %       if data1.time{tr}(1) == 28
        %         shocktr(tr) = true;
        %       end
        %     end
        %   end
        
        %% set amplthreshold for clipping artifacts
        
        trltime = vertcat(data1.time{:})';  % time x trials
        shocktime = abs(32-trltime) < 2.5; % 29.5-34.5 s  % time x trials
        diff1 = cat(3,d1dat.trial{:});  % channels x time x trials
        [nch, nt, ntrl] = size(diff1);
        diff2 = [diff(diff1,1,2) zeros(nch,1,ntrl)];
        ampl = nan(size(diff2));
        for tp = 25:(nt-25)  % sliding 50 ms time windows (default time for clipping artifacts)
          ampl(:,tp,:) = max(abs(diff2(:,(tp-24):(tp+25),:)),[],2);
        end
        res = min(ampl(ampl ~= 0));
        
        fig = figure(next); clf; fig.WindowStyle = 'docked'; hold all;
        next = next + 1;
        for ch = 1:nch
          minampl(ch,r) = min(ampl(ch,~shocktime),[],2,'omitnan');
          histogram(ampl(ch,shocktime),0:res:15);
          % easier to see difference between clipping & normal data here
        end
        axis('tight');
        title(['Shock Clipping for Rat #' num2str(ratT.ratnums(r))]);
        legend(d1dat.label, 'Location','eastoutside');
        xlabel('diff(diff(Amplitude)) between Samples (Max in 50 ms Window)');
        ylabel('# of Samples (50 ms Sliding Windows)');
        
        %% set timethreshold for clipping artifacts
        
        clipVthresh = 5;
        ident = abs(diff2) <= clipVthresh;
        % determine the number of consecutively identical samples
        dur = zeros(size(ident));
        for sgnlop=1:nch
          for trlop = 1:ntrl
            up = find(diff([0 ident(sgnlop,:,trlop)])== 1);
            dw = find(diff([ident(sgnlop,:,trlop) 0])==-1);
            for k=1:length(up)
              dur(sgnlop,up(k):dw(k),trlop) = dw(k)-up(k);
            end
          end
        end
        
        fig = figure(next); clf; fig.WindowStyle = 'docked'; hold all;
        next = next + 1;
        for ch = 1:nch
          maxpertrl = max(dur(ch,~shocktime),[],2,'omitnan');
          maxdur(ch,r) = max(maxpertrl(:));
          sumpertrl = sum(dur(ch,~shocktime) > 50,'omitnan');
          totaldur(ch,r) = sum(sumpertrl(:));
          h = histogram(dur(ch,~shocktime));
          h.Normalization = 'probability';
          h.BinWidth = 25;
        end
        title(['Non-Shock Clipping for Rat #' num2str(ratT.ratnums(r))]);
        xlim([25 25+max(maxdur(:,r))]);
        ylim([0 2*(25+max(maxdur(:,r)))/sum(~shocktime(:))]);
        legend(d1dat.label, 'Location','eastoutside');
        xlabel(['Duration of "Clipping" (Less than ' ...
          num2str(clipVthresh) ' uV Diff2) in Samples']);
        ylabel('Probability of "Clipping" per Sample');
        
        %% check "clipping" artifacts on worst channel(s)
        
        z(1:nch,r) = (totaldur(1:nch,r) - mean(totaldur(1:nch,r))) / ...
          std(totaldur(1:nch,r));
        [sortTdur,badch] = totaldur(1:nch,r);
        badch(isnan(sortTdur)) = [];
        sortTdur(isnan(sortTdur)) = [];
        bestch = badch(find(sortTdur ~= 0, 1, 'first'));
        badch(1:16) = [];   % conserve at least 16 channels
        sortTdur(1:16) = [];
        badch(sortTdur < 15000) = [];  % ignore channels with less than 15s of clipping
        badch(end+1) = bestch;
        nextfig = next;   % save to overwrite the next figures
        for ch = badch'
          clip = squeeze(dur(ch,:,:) > 50);   % time x trial
          fig = figure(next); clf; fig.WindowStyle = 'docked'; hold all;
          next = next + 1; labels = {};
          for tr = find(any(clip))
            yyaxis left
            plot(trltime(clip(:,tr),tr), data1.trial{tr}(ch,(clip(:,tr))),'.');
            labels{end+1} = ['Trial #' num2str(tr)];
            yyaxis right
            plot(trltime(clip(:,tr),tr), diff2(ch,clip(:,tr),tr),'.');
          end
          axis('tight'); % ylim([-5 5]);
          ylabel('diff(diff(Amplitude)) between Samples (uV)');
          yyaxis left; axis('tight'); ylabel('Raw Data (uV)');
          xlabel('Time from Tone Onset (seconds)');
          legend(labels, 'Location','eastoutside');
          title(['Rat #' num2str(ratT.ratnums(r)) ...
            ', Bad Channel: ' d1dat.label{ch}]);
        end
        if lowestdur > 0
          title(['Rat #' num2str(ratT.ratnums(r)) ...
            ', Best Channel: ' d1dat.label{ch}]);
        end
        
        %% separate these to avoid them when running previous cell manually
%         keyboard  % pause & check that detected artifacts are really clipping
%         next = nextfig;   % overwrite single channel data plots, keeping histograms
        continue % skip the rest of the artifact rejection process
        % until you've chosen clipping parameters that work for all
        
      end
      %     What value is just lower (amplthreshold) or higher (timethreshold) than
      %     most of the non-artifactual data? Set both thresholds in the section that
      %     runs ft_artifact_clip. If any channels stand out from the rest,
      %     invalidate them in the next section.
      
      %% reject dead channels based on extreme variance aside from shocks:
      % 1/var much greater than most channels (cutoff between 2 & 4e-4 per rat)
      % or var frequently > var during shock trials (9e4?)
      % or excessive clipping when other channels aren't (if known)
      % or unstable across recordings.
      % Try to keep at least 8 channels per region if possible.
      
      cfg             = [];
      cfg.method      = 'summary';
      cfg.keeptrial   = 'yes';  % won't invalidate trials marked "bad"
      
      % cfg.layout  = [configfolder 'layout_' MWA '_' ratT.side{r} '.mat'];
      % leave layout out to disable trial plotting because of
      % BUG IN rejectvisual_summary>display_trial (line 610)
      % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=2978
      
      % d1dat = ft_rejectvisual(cfg,d1dat);
      data1 = ft_rejectvisual(cfg,data1);
      % data = ft_rejectvisual(cfg,data);
      
      % BUG IN rejectvisual_summary>redraw (line 279) - DON'T USE MIN
      % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=1474
      
      % BUG IN rejectvisual_summary>redraw (line 309) - looks fixed at line 304
      % (same would probably work at line 279)
      % http://bugzilla.fieldtriptoolbox.org/show_bug.cgi?id=3005
      
      goodchs{r} = data1.label;  %#ok<*SAGROW>
%       goodchs{r} = data.label;  %#ok<*SAGROW>
      
      keyboard  % if satisfied, dbcont
      clearvars data1 d1dat diff* dur ampl ident trltime clip shocktime dw up labels
    end
    
    %% select good channels in full dataset & start w/ empty artifact struct
    
    cfg           = [];
    cfg.channel   = goodchs{r};
    
    data = ft_selectdata(cfg,data);
    
    artifact = [];
    
    %% take 1st derivative of signal
    % input to ft_artifact_clip is 1st derivative, so it's actually
    % thresholding the 2nd derivative, which is more consistent in clipping
    % artifacts
    
    cfg = [];
    cfg.absdiff = 'yes';
    
    d1dat = ft_preprocessing(cfg,data);
    
    %% define clipping artifacts
    
    cfg                                 = [];
    
    cfg.artfctdef.clip.channel          = 'AD*';
    cfg.artfctdef.clip.pretim           = 0.1;
    cfg.artfctdef.clip.psttim           = 0.1;
    cfg.artfctdef.clip.timethreshold    = 0.05;   % s
    cfg.artfctdef.clip.amplthreshold    = 5;      % uV
    
    [~, artifact.clip] = ft_artifact_clip(cfg,d1dat);
    
    % bug in ft_artifact_clip adds time after end of file, triggers bug in
    % convert_event that breaks ft_databrowser as noted below
    endsamp = sum(numel([d1dat.time{:}]));
    if artifact.clip(end,end) > endsamp
      artifact.clip(end,end) = endsamp;
    end
    
%     %% define clipping artifacts (alternative params)
%     
%     cfg                                 = [];
%     
%     cfg.artfctdef.clip3.channel          = 'AD*';
%     cfg.artfctdef.clip3.pretim           = 0.1;
%     cfg.artfctdef.clip3.psttim           = 0.1;
%     cfg.artfctdef.clip3.timethreshold    = 0.05;   % s
%     cfg.artfctdef.clip3.amplthreshold    = 3;      % uV
%     
%     [~, artifact.clip3] = ft_artifact_clip(cfg,d1dat);
%     
%     if artifact.clip3(end,end) > endsamp
%       artifact.clip3(end,end) = endsamp;
%     end
%     
    
    clearvars data
  end
end