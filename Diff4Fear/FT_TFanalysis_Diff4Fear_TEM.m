%% Calculate raw spectrograms and coherograms for each trial

info_diff4fear            % load all background info (filenames, etc)

foi = [];
for pow = 0:6   % linearly spaced frequencies between powers of 2
  foi = unique([foi linspace(2^pow,2^(pow+1),pow+3)]);
end

tapsmofrq = diff(foi);          % smooth each band by distance to next
foi = foi(1:numel(tapsmofrq));  % omit extra frequency
t_ftimwin = 2./tapsmofrq;       % for 3 tapers (K=3), T=2/W
toi = -30:min(t_ftimwin)/2:60;  % time windows overlap by at least 50%

%% time-freq analysis

for r = 1:size(ratT,1)
  % Tone-triggered trials with artifacts removed individually from each
  % channel by replacing with NaNs
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllCleanLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  %% check for input data
  
  if ~exist(inputfile,'file')
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ' because ' inputfile ...
      ' was not found. Run FT_AutoArtReject_Diff4Fear_TEM.m first.'])
    continue
  end
  
  %% load data
  
  load(inputfile);
  
  %% minimize trial size
  
  cfg         = [];
  cfg.trials  = find(data.trialinfo(:,1) > 0);
  cfg.toilim  = [min(toi)-max(t_ftimwin) max(toi)+max(t_ftimwin)];
  
  data = ft_redefinetrial(cfg, data);
  
  %% select channels & trials, just to keep this short for now
  
  trials = find(data.trialinfo(:,3) == 3 & data.trialinfo(:,1) <= 3);
  nancnt = NaN(numel(data.label),numel(trials));
  for tr = 1:numel(trials)
    nancnt(:,tr) = sum(isnan(data.trial{trials(tr)}),2);
  end
  [~,chI] = sort(sum(nancnt,2));
  for ch = chI'
  
  %% calculate spectrograms & coherograms
  
  cfg             = [];
  
  cfg.method      = 'mtmconvol';
  cfg.output      = 'powandcsd';
  cfg.keeptrials  = 'yes';
  cfg.trials     = trials;
  
  cfg.foi         = foi;
  cfg.tapsmofrq   = tapsmofrq;
  cfg.t_ftimwin   = t_ftimwin;
  cfg.toi         = toi;
  cfg.pad         = 'nextpow2';
  
  cfg.outputfile  = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'SpecCoherograms_' num2str(ratT.ratnums(r)) '.mat'];
  
  freq = ft_freqanalysis(cfg,data);
  
  if ~any(any(any(~isnan(freq.powspctrm))))
    error('Only NaNs output from freqanalysis');
  end
end