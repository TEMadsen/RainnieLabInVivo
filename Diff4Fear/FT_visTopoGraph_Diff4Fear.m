%% Calculate raw spectrograms and coherograms for each channel & trial

info_diff4fear            % load all background info & final parameters

plot = true;

% create electrode layout for mPFC and BLA/vHipp

%% calculate mean delta, theta and gamma power
% for each channel, each rat, and both CS+ and CS-
% using two methods: averaging raw data and transform it to decible scale
% or simply averaging the decible transfromed data.
% Delta: 1.5-4 Hz
% theta: 4-8 Hz
% Gamma: 5-55 Hz

% 0 - 30 sec : freq.time{322:641}
% Delta: 1.5-4 Hz freq.freq{6:15}
% theta: 4-8 Hz freq.freq{16:22}
% Gamma: 45-55 Hz freq.freq{41:43}

TnBlk = [1 321; 322 641; 642 961]; %[-30 - 0; 0 - 30; 30 - 60] in freq.time
FB = {'Delta';'Theta';'Gamma'};
FBoI = [6 15; 16 22; 41 43]; %[delta; theta; gamma] in freq.freq
TnoI = {'Hab3'; 'Acq6'; 'Ext1_1'; 'Ext2_9'};

FileNotExist = false(size(ratT,1),size(TnoI,1),size(stimuli,1),size(allchs,1));
% rat#/Tn/Stimuli/Channel
%
% RawAvg = NaN(size(ratT,1),size(TnoI,1),size(stimuli,1),size(allchs,1),numel(FBoI(:,1)));
% dBAvg = NaN(size(ratT,1),size(TnoI,1),size(stimuli,1),size(allchs,1),numel(FBoI(:,1)));
% % 4 tones of interest
% % 3 FreqBand of interest
%
% for r = 1:size(ratT,1)
%     for t = 1:size(TnoI,1)
%         switch t
%             case 1
%                 p = 1;
%                 tn = 3;
%             case 2
%                 p = 1;
%                 tn = 9;
%             case 3
%                 p = 3;
%                 tn = 1;
%             case 4
%                 p = 4;
%                 tn = 9;
%         end
%         for s = 1:size(stimuli,1)
%             for ch = 1:size(allchs,1)
%                 inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
%                     stimuli{s} num2str(tn) 'of' phases{p} '_' num2str(ratT.ratnums(r)) ...
%                     allchs{ch} 'Spectrogram.mat'];
%                 if exist (inputfile,'file')
%                     load(inputfile,'freq')
%                     if  size(freq.freq,2) ~= 50 || size(freq.time,2) ~= 961
%                         FileNotExist(r,t,s,ch) = 2;
%                         disp([inputfile ' has not enough entries in freq.freq or freq.time'])
%                     else
%                         % Decibel transformation
%                         newpowspctrm = nan(1, numel(freq.freq), numel(freq.time));
%
%                         for f = 1:numel(freq.freq)
%                             for toi = 1:numel(freq.time)
%                                 newpowspctrm(1, f, toi) = 10*log10(freq.powspctrm(1, f, ...
%                                     toi));
%                             end
%                         end
%                         % calculate mean power in each FBoI and using RawAvg or
%                         % dbAvg
%                         for fb = 1:numel(FBoI(:,1))
%                             RawAvg(r,t,s,ch,fb) = ...
%                                 10*log10(mean(mean(freq.powspctrm ...
%                                 (1,FBoI(fb,1):FBoI(fb,2),TnBlk(2,1):TnBlk(2,2)),'omitnan'),'omitnan'));
%
%                             dBAvg(r,t,s,ch,fb) = ...
%                                 mean(mean(newpowspctrm ...
%                                 (1,FBoI(fb,1):FBoI(fb,2):TnBlk(2,2)),'omitnan'),'omitnan');
%                         end
%                     end
%                     clear freq newpowspctrm
%                 else
%                     FileNotExist(r,t,s,ch) = true;
%                     disp([inputfile 'does not exist'])
%                 end
%             end
%         end
%     end
% end
%
% save([datafolder 'TopoAvg4Plot_Diff4Fear.mat'], 'RawAvg', 'dBAvg')

%% plot
% using imagesc or sfit
% RawAvg and dBAvg could contain NaN or []
AvgInput = [datafolder 'TopoAvg4Plot_Diff4Fear.mat'];

load(AvgInput);

%
%
% if plot
%     ArraySlice = {'lmPFC'; 'mmPFC'; 'lBLA'; 'mBLA'};
%     scale = {'dB'; 'Raw'};
%     % calculate mean power of all electrodes in each FBoI during each tone
%     AvgmPFCdBPower = NaN(size(ratT,1),size(TnoI,1),size(stimuli,1),numel(FBoI(:,1)));
%     AvgBLAdBPower = NaN(size(ratT,1),size(TnoI,1),size(stimuli,1),numel(FBoI(:,1)));
%     AvgmPFCRawPower = NaN(size(ratT,1),size(TnoI,1),size(stimuli,1),numel(FBoI(:,1)));
%     AvgBLARawPower = NaN(size(ratT,1),size(TnoI,1),size(stimuli,1),numel(FBoI(:,1)));
%
%     for r = 1:size(ratT,1)
%         for t = 1:size(TnoI,1)
%             for s = 1:size(stimuli,1)
%                 for fb = 1:numel(FBoI(:,1))
%
%                     lmPFCdB = NaN(4,8);
%                     mmPFCdB = NaN(4,8);
%                     lBLAdB = NaN(5,8);
%                     mBLAdB = NaN(5,8);
%
%                     lmPFCRaw = NaN(4,8);
%                     mmPFCRaw = NaN(4,8);
%                     lBLARaw = NaN(5,8);
%                     mBLARaw = NaN(5,8);
%
%                     if strcmp(ratT.side{r},'Left') == 1
%                         AvgmPFCdBPower(r,t,s,fb) = mean(dBAvg(r,t,s,1:16,fb),'omitnan');
%                         AvgBLAdBPower(r,t,s,fb) = mean(dBAvg(r,t,s,17:32,fb),'omitnan');
%                         AvgmPFCRawPower(r,t,s,fb) = mean(RawAvg(r,t,s,1:16,fb),'omitnan');
%                         AvgBLARawPower(r,t,s,fb) = mean(RawAvg(r,t,s,17:32,fb),'omitnan');
%
%                         lmPFCdB(1,1) = dBAvg(r,t,s,15,fb);
%                         lmPFCdB(2,2) = dBAvg(r,t,s,13,fb);
%                         lmPFCdB(3,3) = dBAvg(r,t,s,11,fb);
%                         lmPFCdB(4,4) = dBAvg(r,t,s,9,fb);
%                         lmPFCdB(1,5) = dBAvg(r,t,s,16,fb);
%                         lmPFCdB(2,6) = dBAvg(r,t,s,14,fb);
%                         lmPFCdB(3,7) = dBAvg(r,t,s,12,fb);
%                         lmPFCdB(4,8) = dBAvg(r,t,s,10,fb);
%
%                         mmPFCdB(1,1) = dBAvg(r,t,s,8,fb);
%                         mmPFCdB(2,2) = dBAvg(r,t,s,6,fb);
%                         mmPFCdB(3,3) = dBAvg(r,t,s,4,fb);
%                         mmPFCdB(4,4) = dBAvg(r,t,s,2,fb);
%                         mmPFCdB(1,5) = dBAvg(r,t,s,7,fb);
%                         mmPFCdB(2,6) = dBAvg(r,t,s,5,fb);
%                         mmPFCdB(3,7) = dBAvg(r,t,s,3,fb);
%                         mmPFCdB(4,8) = dBAvg(r,t,s,1,fb);
%
%                         lBLAdB(5,1) = dBAvg(r,t,s,24,fb);
%                         lBLAdB(4,2) = dBAvg(r,t,s,22,fb);
%                         lBLAdB(3,3) = dBAvg(r,t,s,20,fb);
%                         lBLAdB(5,4) = dBAvg(r,t,s,18,fb);
%                         lBLAdB(4,5) = dBAvg(r,t,s,23,fb);
%                         lBLAdB(3,6) = dBAvg(r,t,s,21,fb);
%                         lBLAdB(2,7) = dBAvg(r,t,s,19,fb);
%                         lBLAdB(1,8) = dBAvg(r,t,s,17,fb);
%
%                         mBLAdB(5,1) = dBAvg(r,t,s,31,fb);
%                         mBLAdB(4,2) = dBAvg(r,t,s,29,fb);
%                         mBLAdB(3,3) = dBAvg(r,t,s,27,fb);
%                         mBLAdB(5,4) = dBAvg(r,t,s,25,fb);
%                         mBLAdB(4,5) = dBAvg(r,t,s,32,fb);
%                         mBLAdB(3,6) = dBAvg(r,t,s,30,fb);
%                         mBLAdB(2,7) = dBAvg(r,t,s,28,fb);
%                         mBLAdB(1,8) = dBAvg(r,t,s,26,fb);
%
%                         lmPFCRaw(1,1) = RawAvg(r,t,s,15,fb);
%                         lmPFCRaw(2,2) = RawAvg(r,t,s,13,fb);
%                         lmPFCRaw(3,3) = RawAvg(r,t,s,11,fb);
%                         lmPFCRaw(4,4) = RawAvg(r,t,s,9,fb);
%                         lmPFCRaw(1,5) = RawAvg(r,t,s,16,fb);
%                         lmPFCRaw(2,6) = RawAvg(r,t,s,14,fb);
%                         lmPFCRaw(3,7) = RawAvg(r,t,s,12,fb);
%                         lmPFCRaw(4,8) = RawAvg(r,t,s,10,fb);
%
%                         mmPFCRaw(1,1) = RawAvg(r,t,s,8,fb);
%                         mmPFCRaw(2,2) = RawAvg(r,t,s,6,fb);
%                         mmPFCRaw(3,3) = RawAvg(r,t,s,4,fb);
%                         mmPFCRaw(4,4) = RawAvg(r,t,s,2,fb);
%                         mmPFCRaw(1,5) = RawAvg(r,t,s,7,fb);
%                         mmPFCRaw(2,6) = RawAvg(r,t,s,5,fb);
%                         mmPFCRaw(3,7) = RawAvg(r,t,s,3,fb);
%                         mmPFCRaw(4,8) = RawAvg(r,t,s,1,fb);
%
%                         lBLARaw(5,1) = RawAvg(r,t,s,24,fb);
%                         lBLARaw(4,2) = RawAvg(r,t,s,22,fb);
%                         lBLARaw(3,3) = RawAvg(r,t,s,20,fb);
%                         lBLARaw(5,4) = RawAvg(r,t,s,18,fb);
%                         lBLARaw(4,5) = RawAvg(r,t,s,23,fb);
%                         lBLARaw(3,6) = RawAvg(r,t,s,21,fb);
%                         lBLARaw(2,7) = RawAvg(r,t,s,19,fb);
%                         lBLARaw(1,8) = RawAvg(r,t,s,17,fb);
%
%                         mBLARaw(5,1) = RawAvg(r,t,s,31,fb);
%                         mBLARaw(4,2) = RawAvg(r,t,s,29,fb);
%                         mBLARaw(3,3) = RawAvg(r,t,s,27,fb);
%                         mBLARaw(5,4) = RawAvg(r,t,s,25,fb);
%                         mBLARaw(4,5) = RawAvg(r,t,s,32,fb);
%                         mBLARaw(3,6) = RawAvg(r,t,s,30,fb);
%                         mBLARaw(2,7) = RawAvg(r,t,s,28,fb);
%                         mBLARaw(1,8) = RawAvg(r,t,s,26,fb);
%
%                         lmPFCdB(isnan(lmPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
%                         lmPFCdB(isempty(lmPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
%                         mmPFCdB(isnan(mmPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
%                         mmPFCdB(isempty(mmPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
%                         lBLAdB(isnan(lBLAdB)) = AvgBLAdBPower(r,t,s,fb);
%                         lBLAdB(isempty(lBLAdB)) = AvgBLAdBPower(r,t,s,fb);
%                         mBLAdB(isnan(mBLAdB)) = AvgBLAdBPower(r,t,s,fb);
%                         mBLAdB(isempty(mBLAdB)) = AvgBLAdBPower(r,t,s,fb);
%
%                         lmPFCRaw(isnan(lmPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
%                         lmPFCRaw(isempty(lmPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
%                         mmPFCRaw(isnan(mmPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
%                         mmPFCRaw(isempty(mmPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
%                         lBLARaw(isnan(lBLARaw)) = AvgBLARawPower(r,t,s,fb);
%                         lBLARaw(isempty(lBLARaw)) = AvgBLARawPower(r,t,s,fb);
%                         mBLARaw(isnan(mBLARaw)) = AvgBLARawPower(r,t,s,fb);
%                         mBLARaw(isempty(mBLARaw)) = AvgBLARawPower(r,t,s,fb);
%
%                         for sc = 1:size(scale,1)
%                             for AS = 1:size(ArraySlice,1)
%                                 fig = gcf; clf; fig.WindowStyle = 'docked';
%                                 imagesc(eval([ArraySlice{AS} scale{sc}]));
%                                 colorbar
%                                 title([stimuli{sc} TnoI{t} ', Rat #' ...
%                                     num2str(ratT.ratnums(r)) ',' scale{sc} 'Average' FB{fb}...
%                                     ' ' ArraySlice{AS}])
%                                 print(fig,[figurefolder 'Rat #' num2str(ratT.ratnums(r)) ...
%                                     scale{sc} 'Averaged_' FB{fb} ArraySlice{AS} TnoI{t} ...
%                                     stimuli{s}],'-dpng','-r300');
%                             end
%                         end
%                     elseif strcmp(ratT.side{r},'Right') == 1
%                         AvgmPFCdBPower(r,t,s,fb) = mean(dBAvg(r,t,s,17:32,fb),'omitnan');
%                         AvgBLAdBPower(r,t,s,fb) = mean(dBAvg(r,t,s,1:16,fb),'omitnan');
%                         AvgmPFCRawPower(r,t,s,fb) = mean(RawAvg(r,t,s,17:32,fb),'omitnan');
%                         AvgBLARawPower(r,t,s,fb) = mean(RawAvg(r,t,s,1:16,fb),'omitnan');
%
%                         lmPFCdB(1,1) = dBAvg(r,t,s,31,fb);
%                         lmPFCdB(2,2) = dBAvg(r,t,s,29,fb);
%                         lmPFCdB(3,3) = dBAvg(r,t,s,27,fb);
%                         lmPFCdB(4,4) = dBAvg(r,t,s,25,fb);
%                         lmPFCdB(1,5) = dBAvg(r,t,s,32,fb);
%                         lmPFCdB(2,6) = dBAvg(r,t,s,30,fb);
%                         lmPFCdB(3,7) = dBAvg(r,t,s,28,fb);
%                         lmPFCdB(4,8) = dBAvg(r,t,s,26,fb);
%
%                         mmPFCdB(1,1) = dBAvg(r,t,s,24,fb);
%                         mmPFCdB(2,2) = dBAvg(r,t,s,22,fb);
%                         mmPFCdB(3,3) = dBAvg(r,t,s,20,fb);
%                         mmPFCdB(4,4) = dBAvg(r,t,s,18,fb);
%                         mmPFCdB(1,5) = dBAvg(r,t,s,23,fb);
%                         mmPFCdB(2,6) = dBAvg(r,t,s,21,fb);
%                         mmPFCdB(3,7) = dBAvg(r,t,s,19,fb);
%                         mmPFCdB(4,8) = dBAvg(r,t,s,17,fb);
%
%                         lBLAdB(5,1) = dBAvg(r,t,s,8,fb);
%                         lBLAdB(4,2) = dBAvg(r,t,s,6,fb);
%                         lBLAdB(3,3) = dBAvg(r,t,s,4,fb);
%                         lBLAdB(5,4) = dBAvg(r,t,s,2,fb);
%                         lBLAdB(4,5) = dBAvg(r,t,s,7,fb);
%                         lBLAdB(3,6) = dBAvg(r,t,s,5,fb);
%                         lBLAdB(2,7) = dBAvg(r,t,s,3,fb);
%                         lBLAdB(1,8) = dBAvg(r,t,s,1,fb);
%
%                         mBLAdB(5,1) = dBAvg(r,t,s,15,fb);
%                         mBLAdB(4,2) = dBAvg(r,t,s,13,fb);
%                         mBLAdB(3,3) = dBAvg(r,t,s,11,fb);
%                         mBLAdB(5,4) = dBAvg(r,t,s,9,fb);
%                         mBLAdB(4,5) = dBAvg(r,t,s,16,fb);
%                         mBLAdB(3,6) = dBAvg(r,t,s,14,fb);
%                         mBLAdB(2,7) = dBAvg(r,t,s,12,fb);
%                         mBLAdB(1,8) = dBAvg(r,t,s,10,fb);
%
%                         lmPFCRaw(1,1) = RawAvg(r,t,s,31,fb);
%                         lmPFCRaw(2,2) = RawAvg(r,t,s,29,fb);
%                         lmPFCRaw(3,3) = RawAvg(r,t,s,27,fb);
%                         lmPFCRaw(4,4) = RawAvg(r,t,s,25,fb);
%                         lmPFCRaw(1,5) = RawAvg(r,t,s,32,fb);
%                         lmPFCRaw(2,6) = RawAvg(r,t,s,30,fb);
%                         lmPFCRaw(3,7) = RawAvg(r,t,s,28,fb);
%                         lmPFCRaw(4,8) = RawAvg(r,t,s,26,fb);
%
%                         mmPFCRaw(1,1) = RawAvg(r,t,s,24,fb);
%                         mmPFCRaw(2,2) = RawAvg(r,t,s,22,fb);
%                         mmPFCRaw(3,3) = RawAvg(r,t,s,20,fb);
%                         mmPFCRaw(4,4) = RawAvg(r,t,s,18,fb);
%                         mmPFCRaw(1,5) = RawAvg(r,t,s,23,fb);
%                         mmPFCRaw(2,6) = RawAvg(r,t,s,21,fb);
%                         mmPFCRaw(3,7) = RawAvg(r,t,s,19,fb);
%                         mmPFCRaw(4,8) = RawAvg(r,t,s,17,fb);
%
%                         lBLARaw(5,1) = RawAvg(r,t,s,8,fb);
%                         lBLARaw(4,2) = RawAvg(r,t,s,6,fb);
%                         lBLARaw(3,3) = RawAvg(r,t,s,4,fb);
%                         lBLARaw(5,4) = RawAvg(r,t,s,2,fb);
%                         lBLARaw(4,5) = RawAvg(r,t,s,7,fb);
%                         lBLARaw(3,6) = RawAvg(r,t,s,5,fb);
%                         lBLARaw(2,7) = RawAvg(r,t,s,3,fb);
%                         lBLARaw(1,8) = RawAvg(r,t,s,1,fb);
%
%                         mBLARaw(5,1) = RawAvg(r,t,s,15,fb);
%                         mBLARaw(4,2) = RawAvg(r,t,s,13,fb);
%                         mBLARaw(3,3) = RawAvg(r,t,s,11,fb);
%                         mBLARaw(5,4) = RawAvg(r,t,s,9,fb);
%                         mBLARaw(4,5) = RawAvg(r,t,s,16,fb);
%                         mBLARaw(3,6) = RawAvg(r,t,s,14,fb);
%                         mBLARaw(2,7) = RawAvg(r,t,s,12,fb);
%                         mBLARaw(1,8) = RawAvg(r,t,s,10,fb);
%
%                         lmPFCdB(isnan(lmPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
%                         lmPFCdB(isempty(lmPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
%                         mmPFCdB(isnan(mmPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
%                         mmPFCdB(isempty(mmPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
%                         lBLAdB(isnan(lBLAdB)) = AvgBLAdBPower(r,t,s,fb);
%                         lBLAdB(isempty(lBLAdB)) = AvgBLAdBPower(r,t,s,fb);
%                         mBLAdB(isnan(mBLAdB)) = AvgBLAdBPower(r,t,s,fb);
%                         mBLAdB(isempty(mBLAdB)) = AvgBLAdBPower(r,t,s,fb);
%
%                         lmPFCRaw(isnan(lmPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
%                         lmPFCRaw(isempty(lmPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
%                         mmPFCRaw(isnan(mmPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
%                         mmPFCRaw(isempty(mmPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
%                         lBLARaw(isnan(lBLARaw)) = AvgBLARawPower(r,t,s,fb);
%                         lBLARaw(isempty(lBLARaw)) = AvgBLARawPower(r,t,s,fb);
%                         mBLARaw(isnan(mBLARaw)) = AvgBLARawPower(r,t,s,fb);
%                         mBLARaw(isempty(mBLARaw)) = AvgBLARawPower(r,t,s,fb);
%
%                         for sc = 1:size(scale,1)
%                             for AS = 1:size(ArraySlice,1)
%                                 fig = gcf; clf; fig.WindowStyle = 'docked';
%                                 imagesc(eval([ArraySlice{AS} scale{sc}]));
%                                 colorbar
%                                 title([stimuli{sc} TnoI{t} ', Rat #' ...
%                                     num2str(ratT.ratnums(r)) ',' scale{sc} 'Average' FB{fb}...
%                                     ' ' ArraySlice{AS}])
%                                 print(fig,[figurefolder 'Rat #' num2str(ratT.ratnums(r)) ...
%                                     scale{sc} 'Averaged_' FB{fb} ArraySlice{AS} TnoI{t} ...
%                                     stimuli{s}],'-dpng','-r300');
%                             end
%                         end
%                     end
%                 end
%             end
%         end
%     end
% end
% % make NaNs and empty entries mean power of all electrodes in each tone

if plot
    ArraySlice = {'mPFC'; 'BLA'};
    scale = {'dB'; 'Raw'};
    % calculate mean power of all electrodes in each FBoI during each tone
    AvgmPFCdBPower = NaN(size(ratT,1),size(TnoI,1),size(stimuli,1),numel(FBoI(:,1)));
    AvgBLAdBPower = NaN(size(ratT,1),size(TnoI,1),size(stimuli,1),numel(FBoI(:,1)));
    AvgmPFCRawPower = NaN(size(ratT,1),size(TnoI,1),size(stimuli,1),numel(FBoI(:,1)));
    AvgBLARawPower = NaN(size(ratT,1),size(TnoI,1),size(stimuli,1),numel(FBoI(:,1)));
    
    for r = 1:size(ratT,1)
        for t = 1:size(TnoI,1)
            for s = 1:size(stimuli,1)
                for fb = 1:numel(FBoI(:,1))
                    
                    mPFCdB = NaN(8,2);
                    BLAdB = NaN(8,2);
                    mPFCRaw = NaN(8,2);
                    BLARaw = NaN(8,2);
                    
                    
                    if strcmp(ratT.side{r},'Left') == 1
                        AvgmPFCdBPower(r,t,s,fb) = mean(dBAvg(r,t,s,1:16,fb),'omitnan');
                        AvgBLAdBPower(r,t,s,fb) = mean(dBAvg(r,t,s,17:32,fb),'omitnan');
                        AvgmPFCRawPower(r,t,s,fb) = mean(RawAvg(r,t,s,1:16,fb),'omitnan');
                        AvgBLARawPower(r,t,s,fb) = mean(RawAvg(r,t,s,17:32,fb),'omitnan');
                        
                        mPFCdB(1,1) = dBAvg(r,t,s,15,fb); %lateral/anterior
                        mPFCdB(2,1) = dBAvg(r,t,s,13,fb);
                        mPFCdB(3,1) = dBAvg(r,t,s,11,fb);
                        mPFCdB(4,1) = dBAvg(r,t,s,9,fb);
                        mPFCdB(5,1) = dBAvg(r,t,s,16,fb);
                        mPFCdB(6,1) = dBAvg(r,t,s,14,fb);
                        mPFCdB(7,1) = dBAvg(r,t,s,12,fb);
                        mPFCdB(8,1) = dBAvg(r,t,s,10,fb);%lateral/posterior
                        
                        mPFCdB(1,2) = dBAvg(r,t,s,8,fb); %medial/anterior
                        mPFCdB(2,2) = dBAvg(r,t,s,6,fb);
                        mPFCdB(3,2) = dBAvg(r,t,s,4,fb);
                        mPFCdB(4,2) = dBAvg(r,t,s,2,fb);
                        mPFCdB(5,2) = dBAvg(r,t,s,7,fb);
                        mPFCdB(6,2) = dBAvg(r,t,s,5,fb);
                        mPFCdB(7,2) = dBAvg(r,t,s,3,fb);
                        mPFCdB(8,2) = dBAvg(r,t,s,1,fb); %medial/posterior
                        
                        BLAdB(1,1) = dBAvg(r,t,s,24,fb); %lateral/anterior
                        BLAdB(2,1) = dBAvg(r,t,s,22,fb);
                        BLAdB(3,1) = dBAvg(r,t,s,20,fb);
                        BLAdB(4,1) = dBAvg(r,t,s,18,fb);
                        BLAdB(5,1) = dBAvg(r,t,s,23,fb);
                        BLAdB(6,1) = dBAvg(r,t,s,21,fb);
                        BLAdB(7,1) = dBAvg(r,t,s,19,fb);
                        BLAdB(8,1) = dBAvg(r,t,s,17,fb);%lateral/posterior
                        
                        BLAdB(1,2) = dBAvg(r,t,s,31,fb); %medial/anterior
                        BLAdB(2,2) = dBAvg(r,t,s,29,fb);
                        BLAdB(3,2) = dBAvg(r,t,s,27,fb);
                        BLAdB(4,2) = dBAvg(r,t,s,25,fb);
                        BLAdB(5,2) = dBAvg(r,t,s,32,fb);
                        BLAdB(6,2) = dBAvg(r,t,s,30,fb);
                        BLAdB(7,2) = dBAvg(r,t,s,28,fb);
                        BLAdB(8,2) = dBAvg(r,t,s,26,fb);%medial/posterior
                        
                        mPFCRaw(1,1) = RawAvg(r,t,s,15,fb); %lateral
                        mPFCRaw(2,1) = RawAvg(r,t,s,13,fb);
                        mPFCRaw(3,1) = RawAvg(r,t,s,11,fb);
                        mPFCRaw(4,1) = RawAvg(r,t,s,9,fb);
                        mPFCRaw(5,1) = RawAvg(r,t,s,16,fb);
                        mPFCRaw(6,1) = RawAvg(r,t,s,14,fb);
                        mPFCRaw(7,1) = RawAvg(r,t,s,12,fb);
                        mPFCRaw(8,1) = RawAvg(r,t,s,10,fb);
                        
                        mPFCRaw(1,2) = RawAvg(r,t,s,8,fb); %medial
                        mPFCRaw(2,2) = RawAvg(r,t,s,6,fb);
                        mPFCRaw(3,2) = RawAvg(r,t,s,4,fb);
                        mPFCRaw(4,2) = RawAvg(r,t,s,2,fb);
                        mPFCRaw(5,2) = RawAvg(r,t,s,7,fb);
                        mPFCRaw(6,2) = RawAvg(r,t,s,5,fb);
                        mPFCRaw(7,2) = RawAvg(r,t,s,3,fb);
                        mPFCRaw(8,2) = RawAvg(r,t,s,1,fb);
                        
                        BLARaw(1,1) = RawAvg(r,t,s,24,fb); %lateral
                        BLARaw(2,1) = RawAvg(r,t,s,22,fb);
                        BLARaw(3,1) = RawAvg(r,t,s,20,fb);
                        BLARaw(4,1) = RawAvg(r,t,s,18,fb);
                        BLARaw(5,1) = RawAvg(r,t,s,23,fb);
                        BLARaw(6,1) = RawAvg(r,t,s,21,fb);
                        BLARaw(7,1) = RawAvg(r,t,s,19,fb);
                        BLARaw(8,1) = RawAvg(r,t,s,17,fb);
                        
                        BLARaw(1,2) = RawAvg(r,t,s,31,fb); %medial
                        BLARaw(2,2) = RawAvg(r,t,s,29,fb);
                        BLARaw(3,2) = RawAvg(r,t,s,27,fb);
                        BLARaw(4,2) = RawAvg(r,t,s,25,fb);
                        BLARaw(5,2) = RawAvg(r,t,s,32,fb);
                        BLARaw(6,2) = RawAvg(r,t,s,30,fb);
                        BLARaw(7,2) = RawAvg(r,t,s,28,fb);
                        BLARaw(8,2) = RawAvg(r,t,s,26,fb);
                        
                        mPFCdB(isnan(mPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
                        mPFCdB(isempty(mPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
                        BLAdB(isnan(BLAdB)) = AvgBLAdBPower(r,t,s,fb);
                        BLAdB(isempty(BLAdB)) = AvgBLAdBPower(r,t,s,fb);
                        mPFCRaw(isnan(mPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
                        mPFCRaw(isempty(mPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
                        BLARaw(isnan(BLARaw)) = AvgBLARawPower(r,t,s,fb);
                        BLARaw(isempty(BLARaw)) = AvgBLARawPower(r,t,s,fb);
                        
                        for sc = 1:size(scale,1)
                            for AS = 1:size(ArraySlice,1)
                                fig = gcf; clf; fig.WindowStyle = 'docked';
                                imagesc(eval([ArraySlice{AS} scale{sc}]));
                                colorbar
                                title([stimuli{s} TnoI{t} ', Rat #' ...
                                    num2str(ratT.ratnums(r)) ',' scale{sc} 'Average' FB{fb}...
                                    ' ' ArraySlice{AS}])
                                print(fig,[figurefolder 'Rat #' num2str(ratT.ratnums(r)) ...
                                    scale{sc} 'Averaged_' FB{fb} ArraySlice{AS} TnoI{t} ...
                                    stimuli{s}],'-dpng','-r300');
                            end
                        end
                        
                        
                    elseif strcmp(ratT.side{r},'Right') == 1
                        AvgmPFCdBPower(r,t,s,fb) = mean(dBAvg(r,t,s,17:32,fb),'omitnan');
                        AvgBLAdBPower(r,t,s,fb) = mean(dBAvg(r,t,s,1:16,fb),'omitnan');
                        AvgmPFCRawPower(r,t,s,fb) = mean(RawAvg(r,t,s,17:32,fb),'omitnan');
                        AvgBLARawPower(r,t,s,fb) = mean(RawAvg(r,t,s,1:16,fb),'omitnan');
                        
                        BLAdB(1,2) = dBAvg(r,t,s,15,fb); %medial
                        BLAdB(2,2) = dBAvg(r,t,s,13,fb);
                        BLAdB(3,2) = dBAvg(r,t,s,11,fb);
                        BLAdB(4,2) = dBAvg(r,t,s,9,fb);
                        BLAdB(5,2) = dBAvg(r,t,s,16,fb);
                        BLAdB(6,2) = dBAvg(r,t,s,14,fb);
                        BLAdB(7,2) = dBAvg(r,t,s,12,fb);
                        BLAdB(8,2) = dBAvg(r,t,s,10,fb);
                        
                        BLAdB(1,1) = dBAvg(r,t,s,8,fb); %lateral
                        BLAdB(2,1) = dBAvg(r,t,s,6,fb);
                        BLAdB(3,1) = dBAvg(r,t,s,4,fb);
                        BLAdB(4,1) = dBAvg(r,t,s,2,fb);
                        BLAdB(5,1) = dBAvg(r,t,s,7,fb);
                        BLAdB(6,1) = dBAvg(r,t,s,5,fb);
                        BLAdB(7,1) = dBAvg(r,t,s,3,fb);
                        BLAdB(8,1) = dBAvg(r,t,s,1,fb);
                        
                        mPFCdB(1,2) = dBAvg(r,t,s,24,fb); %medial
                        mPFCdB(2,2) = dBAvg(r,t,s,22,fb);
                        mPFCdB(3,2) = dBAvg(r,t,s,20,fb);
                        mPFCdB(4,2) = dBAvg(r,t,s,18,fb);
                        mPFCdB(5,2) = dBAvg(r,t,s,23,fb);
                        mPFCdB(6,2) = dBAvg(r,t,s,21,fb);
                        mPFCdB(7,2) = dBAvg(r,t,s,19,fb);
                        mPFCdB(8,2) = dBAvg(r,t,s,17,fb);
                        
                        mPFCdB(1,1) = dBAvg(r,t,s,31,fb); %lateral
                        mPFCdB(2,1) = dBAvg(r,t,s,29,fb);
                        mPFCdB(3,1) = dBAvg(r,t,s,27,fb);
                        mPFCdB(4,1) = dBAvg(r,t,s,25,fb);
                        mPFCdB(5,1) = dBAvg(r,t,s,32,fb);
                        mPFCdB(6,1) = dBAvg(r,t,s,30,fb);
                        mPFCdB(7,1) = dBAvg(r,t,s,28,fb);
                        mPFCdB(8,1) = dBAvg(r,t,s,26,fb);
                        
                        BLARaw(1,2) = RawAvg(r,t,s,15,fb); %medial
                        BLARaw(2,2) = RawAvg(r,t,s,13,fb);
                        BLARaw(3,2) = RawAvg(r,t,s,11,fb);
                        BLARaw(4,2) = RawAvg(r,t,s,9,fb);
                        BLARaw(5,2) = RawAvg(r,t,s,16,fb);
                        BLARaw(6,2) = RawAvg(r,t,s,14,fb);
                        BLARaw(7,2) = RawAvg(r,t,s,12,fb);
                        BLARaw(8,2) = RawAvg(r,t,s,10,fb);
                        
                        BLARaw(1,1) = RawAvg(r,t,s,8,fb); %lateral
                        BLARaw(2,1) = RawAvg(r,t,s,6,fb);
                        BLARaw(3,1) = RawAvg(r,t,s,4,fb);
                        BLARaw(4,1) = RawAvg(r,t,s,2,fb);
                        BLARaw(5,1) = RawAvg(r,t,s,7,fb);
                        BLARaw(6,1) = RawAvg(r,t,s,5,fb);
                        BLARaw(7,1) = RawAvg(r,t,s,3,fb);
                        BLARaw(8,1) = RawAvg(r,t,s,1,fb);
                        
                        mPFCRaw(1,2) = RawAvg(r,t,s,24,fb); %medial
                        mPFCRaw(2,2) = RawAvg(r,t,s,22,fb);
                        mPFCRaw(3,2) = RawAvg(r,t,s,20,fb);
                        mPFCRaw(4,2) = RawAvg(r,t,s,18,fb);
                        mPFCRaw(5,2) = RawAvg(r,t,s,23,fb);
                        mPFCRaw(6,2) = RawAvg(r,t,s,21,fb);
                        mPFCRaw(7,2) = RawAvg(r,t,s,19,fb);
                        mPFCRaw(8,2) = RawAvg(r,t,s,17,fb);
                        
                        mPFCRaw(1,1) = RawAvg(r,t,s,31,fb); %lateral
                        mPFCRaw(2,1) = RawAvg(r,t,s,29,fb);
                        mPFCRaw(3,1) = RawAvg(r,t,s,27,fb);
                        mPFCRaw(4,1) = RawAvg(r,t,s,25,fb);
                        mPFCRaw(5,1) = RawAvg(r,t,s,32,fb);
                        mPFCRaw(6,1) = RawAvg(r,t,s,30,fb);
                        mPFCRaw(7,1) = RawAvg(r,t,s,28,fb);
                        mPFCRaw(8,1) = RawAvg(r,t,s,26,fb);
                        
                        mPFCdB(isnan(mPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
                        mPFCdB(isempty(mPFCdB)) = AvgmPFCdBPower(r,t,s,fb);
                        BLAdB(isnan(BLAdB)) = AvgBLAdBPower(r,t,s,fb);
                        BLAdB(isempty(BLAdB)) = AvgBLAdBPower(r,t,s,fb);
                        mPFCRaw(isnan(mPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
                        mPFCRaw(isempty(mPFCRaw)) = AvgmPFCRawPower(r,t,s,fb);
                        BLARaw(isnan(BLARaw)) = AvgBLARawPower(r,t,s,fb);
                        BLARaw(isempty(BLARaw)) = AvgBLARawPower(r,t,s,fb);
                        
                        for sc = 1:size(scale,1)
                            for AS = 1:size(ArraySlice,1)
                                fig = gcf; clf; fig.WindowStyle = 'docked';
                                imagesc(eval([ArraySlice{AS} scale{sc}]));
                                colorbar
                                title([stimuli{s} TnoI{t} ', Rat #' ...
                                    num2str(ratT.ratnums(r)) ',' scale{sc} 'Average' FB{fb}...
                                    ' ' ArraySlice{AS}])
                                print(fig,[figurefolder 'Rat #' num2str(ratT.ratnums(r)) ...
                                    scale{sc} 'Averaged_' FB{fb} ArraySlice{AS} TnoI{t} ...
                                    stimuli{s}],'-dpng','-r300');
                            end
                        end

                        
                    end
                end
            end
        end
    end
end

