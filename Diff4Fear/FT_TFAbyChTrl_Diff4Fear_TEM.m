%% Calculate raw spectrograms and coherograms for each channel & trial
% will take about 48 hours for all 11 rats, assuming similar parameters

CleanSlate  % provides a clean slate to start working with new data/scripts

info_diff4fear            % load all background info & final parameters

zthresh = 15;   % analyze data cleaned using this artifact threshold

errlog = cell(size(ratT,1),numel(allchs));   % MEs saved w/in tr# cells

%% full loop

for r = 1:size(ratT,1)
  %% loop through all channels
  parfor ch = 1:numel(allchs) % defined in info script
    %% define & check for outputfile
    outputfile1 = [datafolder num2str(ratT.ratnums(r)) filesep ...
      'Clean_z' num2str(zthresh) '_' allchs{ch} 'SubTrialData_' ...
      num2str(ratT.ratnums(r)) 'Merged.mat'];
    
    %% stop here to avoid continue
    if exist(outputfile1,'file')
      disp(['Loading sub-trial data for rat #' num2str(ratT.ratnums(r))])
      S = load(outputfile1,'data');
      data1ch = S.data;
      S = [];   % use empty set instead of clearvars inside parfor loop
    else
      %% load single channel file w/ NaNs for artifacts
      
      inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
        'Clean_z' num2str(zthresh) '_' allchs{ch} 'TrialData_' ...
        num2str(ratT.ratnums(r)) 'Merged.mat'];
      
      %% stop here to avoid continue
      if ~exist(inputfile,'file')
        warning(['Skipping ' num2str(ratT.ratnums(r)) ' ' allchs{ch} ...
          ' - inputfile not found:  ' inputfile]);
        continue
      end
      
      %% load single channel file w/ NaNs
      disp(['Loading single channel data for rat #' num2str(ratT.ratnums(r))])
      S = load(inputfile,'data');
      data1ch = S.data;
      S = [];
      
      %% mark NaNs as artifacts
      
      nanarts = [];
      
      for tr = 1:numel(data1ch.trial)
        % identify whether there are any NaNs at each timepoint
        tnan = any(isnan(data1ch.trial{tr}),1);   % that 1 is important in the single channel case!
        
        if any(tnan)
          % determine the file sample #s for this trial
          trsamp = data1ch.sampleinfo(tr,1):data1ch.sampleinfo(tr,2);
          
          while any(tnan)
            % start from the end so sample #s don't shift
            endnan = find(tnan,1,'last');
            tnan = tnan(1:endnan);  % remove any non-NaNs after this
            
            % find last non-NaN before the NaNs
            beforenan = find(~tnan,1,'last');
            
            if isempty(beforenan)   % if no more non-NaNs
              begnan = 1;
              tnan = false;   % while loop ends
            else  % still more to remove - while loop continues
              begnan = beforenan + 1;
              tnan = tnan(1:beforenan);  % remove the identified NaNs
            end
            
            % identify file sample #s that correspond to beginning
            % and end of this chunk of NaNs and append to
            % nanarts
            nanarts = [nanarts; trsamp(begnan) trsamp(endnan)];
          end   % while any(tnan)
        end   % if any(tnan)
      end   % for tr = 1:numel(data1ch.trial)
      
      %% remove NaNs from data & save subtrial data file
      
      cfg                         = [];
      cfg.artfctdef.nan.artifact  = nanarts;
      cfg.artfctdef.reject        = 'partial';
      cfg.outputfile              = outputfile1;
      
      data1ch = ft_rejectartifact(cfg,data1ch);
    end
    
    %% loop through all tone trials
    trlinfo = unique(data1ch.trialinfo,'rows','stable');
    for tr = 1:size(trlinfo,1)
      try
      %% define & check for outputfile
      
      tn = trlinfo(tr,1);   % tone number within phase & stimulus type
      s = trlinfo(tr,2);    % stimulus type
      p = trlinfo(tr,3);    % phase
      
      cfg             = [];
      
      if tn == 0 || s == 0  % context extinction or baseline VI-60
        outputfile2 = [datafolder num2str(ratT.ratnums(r)) filesep ...
          phases{p} '_' num2str(ratT.ratnums(r)) allchs{ch} '_z' ...
          num2str(zthresh) '_altSpectrogram.mat']; %#ok<PFBNS> each worker needs all
        cfg.toi 	= alt.BLtoi;  % toi;
      else
        outputfile2 = [datafolder num2str(ratT.ratnums(r)) filesep ...
          stimuli{s} num2str(tn) 'of' phases{p} '_' num2str(ratT.ratnums(r)) ...
          allchs{ch} '_z' num2str(zthresh) '_altSpectrogram.mat']; %#ok<PFBNS> each worker needs all
        cfg.toi   = alt.toi;    % toi;
      end
      
      %% stop here to avoid continue
      if exist(outputfile2,'file')
        disp(['freq analysis already conducted for ' stimuli{s} ' #' ...
          num2str(tn) ' of ' phases{p} ', Rat #' ...
          num2str(ratT.ratnums(r)) ', ' allchs{ch} '. Skipping.'])
        continue
      end
      
      %% calculate spectrogram
      
      cfg.method      = 'mtmconvol';
      cfg.output      = 'pow';
      cfg.keeptrials  = 'no';
      cfg.foi         = alt.foi;        % foi;
      cfg.tapsmofrq   = alt.tapsmofrq;  % tapsmofrq;
      cfg.t_ftimwin   = alt.t_ftimwin;  % t_ftimwin;
      cfg.pad         = alt.pad;        % 'nextpow2';
      cfg.outputfile  = outputfile2;
      cfg.trials = find(ismember(data1ch.trialinfo,trlinfo(tr,:),'rows'));
      
      freq = ft_freqanalysis(cfg,data1ch);
      catch ME
        warning(ME.message);
        errlog{r,ch}{tr} = ME;
      end
    end
  end
end
