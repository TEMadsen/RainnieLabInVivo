%% Accumulates all rats' freq into one per block, calculates descriptive
% statistics & averages for plotting

CleanSlate  % provides a clean slate to start working with new data/scripts
% by clearing all variables, figures, and the command window

info_diff4fear            % load all background info & final parameters
errlog = cell2struct(cell(size(ratT,1),numel(allchs)),allchs,2); 	% MEs saved w/in nested structure

% rats 357 & 359 need their CS+/- reversed between input & output files
revCS = false(size(ratT,1),1);
revCS([5 7]) = true;

%% which freq files to group

normtype = 'z-score';  % my proposal
redoavg = false;            % to ignore pre-existing outputs and overwrite them
redoplt = true;            % to ignore pre-existing outputs and overwrite them

%% preallocate variable to store max & min of each spectrogram for use as clims

clims = NaN(numel(regions), numel(phases), numel(stimuli), 3, 2);  % min & max for 3 blocks per region, phase, & stimtype

%% loop through phases

for p = [1 3 4]   % skip context & baseline
  %% ...and both stimulus types
  
  for s = 1:numel(stimuli)
    %% ...and 3 blocks of 3 tones each
    
    for b = 1:3
      cfggrp = [];  % clear group config before starting on individual rats
      
      cfggrp.inputfile = cell(1,size(ratT,1));
      
      cfggrp.outputfile = [datafolder 'GrpAve_' normtype ...
        'NormBlock' num2str(b) stimuli{s} 'of' phases{p} '.mat'];
      
      outputfig = [figurefolder 'GrpAve_' normtype 'NormBlock' ...
        num2str(b) stimuli{s} 'of' phases{p} '_Spectrograms'];
      
      if exist([outputfig '.png'],'file')
        warning(['figure already printed: ' outputfig]);
        if ~redoplt
          continue % to next block
        end
      end
      
      if exist(cfggrp.outputfile,'file')
        warning(['grpavg output file already computed: ' cfggrp.outputfile])
        if ~redoavg
          continue % to next block
        end
      end
      
      %% ...and finally through rats
      
      for r = find(~ratT.excluded)'
        ratfolder = [datafolder num2str(ratT.ratnums(r)) filesep];
        
        if revCS(r)
          rev = 'Rev';  % to find files marked as having reversed CS+/-
        else
          rev = '';     % to leave it off
        end
        
        %% aggregate the 4 region-representative channels
        
        cfgr = [];
        cfgr.parameter = 'powspctrm';
        cfgr.tolerance = 0.1;
        
        outputfile = [ratfolder 'omitnan_' normtype 'NormBlock' ...
          num2str(b) stimuli{s} 'of' phases{p} '_' ...
          num2str(ratT.ratnums(r)) '_4ch.mat']; % removed from cfgr because we don't actually want appendfreq to save this before we change channel names
        
        %% stop here to avoid continue
        
        if exist(outputfile,'file')
          warning(['outputfile already exists: ' outputfile]);
          if ~redoavg
            cfggrp.inputfile{r} = outputfile; % this output will be input to groupavg
            continue  % to next rat
          end
        end
        
        %% append single channel freqs into one freq file per rat/block
        
        for rrc = 1:numel(regions)
          cfgr.inputfile{rrc} = [ratfolder 'omitnan_' normtype ...
            'NormBlock' num2str(b) rev stimuli{s} 'of' phases{p} '_' ...
            num2str(ratT.ratnums(r)) bestchs(r).(regions{rrc}) '.mat'];
        end
        
        if ~exist(cfgr.inputfile{rrc},'file')
          error(['inputfile missing: ' cfgr.inputfile{rrc}]);
          % consider changing rrc or remove this rat
        end
        
        freq = ft_appendfreq(cfgr);
        
        %% rename each channel to the region it represents
        
        for rrc = 1:numel(regions)
          freq.label{ismember(freq.label, bestchs(r).(regions{rrc}))} = ...
            regions{rrc};
        end
        
        %% save merged rat freq file
        
        save(outputfile,'freq');
        cfggrp.inputfile{r} = outputfile;
        
        %% plot individual rat's 4-region spectrograms
        
        outputfig = [figurefolder normtype 'NormBlock' ...
          num2str(b) stimuli{s} 'of' phases{p} '_' ...
          num2str(ratT.ratnums(r)) '_4chSpectrograms'];
      
      if exist(outputfig,'file')
        warning(['figure already printed: ' outputfig]);
        if ~redoplt
          continue % to next rat
        end
      end
      
      %% plot each rat's 4-channel spectrograms (1 subplot per region)
      
      fig = gcf; clf; fig.WindowStyle = 'docked';
      
      for rrc = 1:numel(regions)
        subplot(2,2,rrc)
        imagesc(freq.time, 1:numel(freq.freq), squeeze(freq.(cfgr.parameter)(rrc,:,:)));
        
        axis xy; colorbar; colormap(jet);
        
        P = unique(nextpow2(freq.freq));
        fticks = (2.^P(1:end-1))';   % for labeling log scale frequency axes
        base = mean(freq.freq(2:end) ./ freq.freq(1:end-1));  % ratio between foi
        fticklocs = (log(fticks) ./ log(base)) + 1; 	% convert to freq.freq indices
        
        ax = gca;
        ax.XTick = -30:10:60;
        ax.YTick = fticklocs;
        ax.YTickLabel = {num2str(fticks)};
        ax.CLim = [-2 2]; % [-max(clims(:),[],'omitnant') max(clims(:),[],'omitnant')];
        xlabel('Time from Tone Onset (s)')
        ylabel('Frequency (Hz)')
        title(regions{rrc})
      end
      
      %% save figure
      
      print(fig, outputfig, '-dpng', '-r300');
        
      end
      %% use meanfreqomitnan to average all rats into 1 freq file
      
      goodfiles = logical(cellfun(@exist, cfggrp.inputfile, ...
        repelem({'file'}, size(cfggrp.inputfile,2))));
      
      if ~any(goodfiles)
        error('There are no good files for this block of tones/stimuli.')
%         continue
      end
      
      if sum(goodfiles) < sum(~ratT.excluded)
        error('Some rats are missing data.')
      end
      
      cfggrp.inputfile = cfggrp.inputfile(goodfiles);
% cfggrp.keepindividual = 'yes';  % needed to get variance in next section
% freq = ft_freqgrandaverage(cfggrp);
      freq = meanfreqomitnan(cfggrp);
      
%       %% use ft_freqdescriptives to get variance & average across all rats
%       
%       cfggrp = [];
%       
%       cfggrp.variance     = 'yes';
%       cfggrp.jackknife    = 'yes';
%       cfggrp.keeptrials   = 'no';
%       
%       freq = ft_freqdescriptives(cfggrp, freq);
      
      %% save max & min for later clims
      
      clims(p,s,b,1) = min(min(freq.powspctrm(:,:),[],'omitnan'),[],'omitnan');
      clims(p,s,b,2) = min(max(freq.powspctrm(:,:),[],'omitnan'),[],'omitnan');
      
    end
  end
end

%% plot all group averaged spectrograms, looping through phases

if ~exist('cfgr','var')
  cfgr.parameter = 'powspctrm';
end

for p = [1 3 4]
  %% ...and both stimulus types
  
  for s = 1:numel(stimuli)
    %% ...and 3 blocks of 3 tones each
    
    for b = 1:3
      cfggrp = [];
      
      groupoutput = [datafolder 'GrpAve_' normtype ...
        'NormBlock' num2str(b) stimuli{s} 'of' phases{p} '.mat'];
      
      outputfig = [figurefolder 'GrpAve_' normtype 'NormBlock' ...
        num2str(b) stimuli{s} 'of' phases{p} '_Spectrograms'];
      
      if exist(outputfig,'file')
        warning(['figure already printed: ' outputfig]);
        if ~redoplt
          continue % to next block
        end
      end
      
      if ~exist(groupoutput,'file')
        error(['group averaged output file not found: ' groupoutput])
%         continue % to next block
      end
      
      load(groupoutput)
      
      %% load & plot group average spectrograms (1 subplot per region)
      
      fig = figure; clf; fig.WindowStyle = 'docked';
      
      for rrc = 1:numel(regions)
        subplot(2,2,rrc)
        imagesc(freq.time, 1:numel(freq.freq), squeeze(freq.(cfgr.parameter)(rrc,:,:)));
        
        axis xy; colorbar; colormap(jet);
        
        P = unique(nextpow2(freq.freq));
        fticks = (2.^P(1:end-1))';   % for labeling log scale frequency axes
        base = mean(freq.freq(2:end) ./ freq.freq(1:end-1));  % ratio between foi
        fticklocs = (log(fticks) ./ log(base)) + 1; 	% convert to freq.freq indices
        
        ax = gca;
        ax.XTick = -30:10:60;
        ax.YTick = fticklocs;
        ax.YTickLabel = {num2str(fticks)};
        ax.CLim = [-1 1]; % [-max(clims(:),[],'omitnant') max(clims(:),[],'omitnant')];
        xlabel('Time from Tone Onset (s)')
        ylabel('Frequency (Hz)')
        title(regions{rrc})
      end
      
      %% save figure
      
      print(fig, outputfig, '-dpng', '-r300');
    end
  end
end
