%% Completely automated artifact rejection!
% process each channel independently, removing clipping, large, and jump
% artifacts based on an automated threshold, so no human intervention is
% required!

info_diff4fear  % load all background info (filenames, etc)

% chcolor = lines(numel([lftchs; rtchs]));
nancnt = cell(size(ratT,1), 1);   % restructure to rat-cells with ch x trl matrices
zthresh = 15;

%% full loop

for r = 1:size(ratT,1)
  % Raw LFP data divided into long, non-overlapping tone-triggered
  % trials, merged across all recordings for each rat
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'RawLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  % Clean LFP data with individual channel artifacts replaced with NaNs,
  % remerged to include all channels
  outputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllClean_z' num2str(zthresh) '_LFPTrialData_' ...
    num2str(ratT.ratnums(r)) 'Merged.mat'];
  
%   next = 1;   % overwrite figures from previous rat
  
  %% check for output file
  if exist(outputfile,'file')
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ' because ' ...
      outputfile ' already exists.'])
    continue
  end

  %% check for input data
  
  if ~exist(inputfile,'file')
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ' because ' ...
      inputfile ' was not found. Run FT_preproc_Diff4Fear_TEM.m first.'])
    continue
  end
  
  %% load input data
  
  disp(['Loading ' inputfile])
  load(inputfile)
  
  chfile = cell(numel(data.label));
  excludech = false(size(data.label));
  subfile = cell(numel(data.label));
  
  %% find std of each channel & trial's data for this rat
  
  trlstd = NaN(numel(data.label),numel(data.trial));
  for tr = 1:numel(data.trial)
    trlstd(:,tr) = std(abs(data.trial{tr}),[],2);
  end
    
  %% perform artifact detection on each channel separately
  for ch = 1:numel(data.label)
    artifact = [];
    
    %% divide data into channels
    
    cfg           = [];
    cfg.channel   = ch;
    
    data1ch = ft_selectdata(cfg,data);
    
    %% calculate ratio of overall vs. median channel on best trial's std
    % to base z-score threshold on cleanest trial
    
    totstd = std(abs([data1ch.trial{:}]));
    stdratio = min(median(trlstd))/totstd;
    
    %% identify large artifacts
    
    cfg                                 = [];
    cfg.artfctdef.zvalue.channel        = 'AD*';
    cfg.artfctdef.zvalue.cutoff         = zthresh*stdratio;   % converts zthresh based on total variance to one based on cleanest trial's variance
    cfg.artfctdef.zvalue.trlpadding     = 0;
    cfg.artfctdef.zvalue.fltpadding     = 0;
    cfg.artfctdef.zvalue.artpadding     = 0.1;
    cfg.artfctdef.zvalue.rectify        = 'yes';
%     cfg.artfctdef.zvalue.interactive    = 'yes';
    
    [~, artifact.large] = ft_artifact_zvalue(cfg,data1ch);
    
    %% identify sudden "jumps" in the data
    
    cfg                                 = [];
    
    % channel selection, cutoff and padding
    cfg.artfctdef.zvalue.channel        = 'AD*';
    cfg.artfctdef.zvalue.cutoff         = zthresh;   % this must be closer to normally distributed
    cfg.artfctdef.zvalue.trlpadding     = 0;
    cfg.artfctdef.zvalue.artpadding     = 0.1;
    cfg.artfctdef.zvalue.fltpadding     = 0;
    cfg.artfctdef.zvalue.medianfilter   = 'yes';
    cfg.artfctdef.zvalue.medianfiltord  = 23;  % wide enough not to catch anything better explained as a gamma oscillation
    cfg.artfctdef.zvalue.absdiff        = 'yes';
%     cfg.artfctdef.zvalue.interactive    = 'yes';
    
    [~, artifact.jump] = ft_artifact_zvalue(cfg,data1ch);
    
%% take 1st derivative of signal
    
    cfg = [];
    cfg.absdiff = 'yes';
    
    d1dat = ft_preprocessing(cfg,data1ch);
    
    %% define clipping artifacts
    
    cfg                                 = [];
    
    cfg.artfctdef.clip.channel          = 'AD*';
    cfg.artfctdef.clip.pretim           = 0.1;
    cfg.artfctdef.clip.psttim           = 0.1;
    cfg.artfctdef.clip.timethreshold    = 0.05;   % s
    cfg.artfctdef.clip.amplthreshold    = 5;      % uV
    
    [~, artifact.clip] = ft_artifact_clip(cfg,d1dat);
    
    % bugs in ft_artifact_clip add time before & after end of file, breaks
    % ft_rejectartifact or triggers bug in convert_event that breaks
    % ft_databrowser as noted below
    if artifact.clip(1,1) < 1
      artifact.clip(1,1) = 1;
    end
    endsamp = numel([d1dat.time{:}]);
    if artifact.clip(end,end) > endsamp
      artifact.clip(end,end) = endsamp;
    end 
    
        %% review artifacts if needed
    
    cfg                               = [];
    cfg.artfctdef.clip.artifact       = artifact.clip;
    cfg.artfctdef.large.artifact      = artifact.large;
    cfg.artfctdef.jump.artifact       = artifact.jump;
    
    % bug in convert_event, lines 180 & 181 should be swapped - prevents
    % plotting of all but the first large & jump artifacts
%     cfg = ft_databrowser(cfg,data1ch);
%     
% %     artifact.visual   = cfg.artfctdef.visual.artifact;
%     keyboard  % dbcont when satisfied
%     clearvars d1dat
    
    %% replace artifactual data with NaNs (for cross channel analyses)
    
%     cfg.artfctdef.visual.artifact = artifact.visual;
    cfg.artfctdef.reject          = 'nan';
    cfg.artfctdef.minaccepttim    = 0.25;
    
    chfile{ch} = [datafolder num2str(ratT.ratnums(r)) filesep ...
      'Clean_z' num2str(zthresh) '_' data.label{ch} 'TrialData_' ...
      num2str(ratT.ratnums(r)) 'Merged.mat'];
    
    cfg.outputfile                = chfile{ch};
    
    data1ch = ft_rejectartifact(cfg,data1ch);
    
    if numel(data1ch.trial) ~= numel(data.trial)  % if any trials were rejected completely
      excludech(ch) = true;                       % exclude this channel
    end
    
    nancnt{r,ch} = sum(isnan([data1ch.trial{:}])); %change made here by JH 10:14 11/2/16
    
    %% parse trials to remove NaNs (for individual channel analyses)
    
    cfg.artfctdef.reject          = 'partial';
    subfile{ch} = [datafolder num2str(ratT.ratnums(r)) filesep ...
      'Clean_z' num2str(zthresh) '_' data.label{ch} 'SubTrialData_' ...
      num2str(ratT.ratnums(r)) 'Merged.mat'];
    
    cfg.outputfile                = subfile{ch};
    
    data1ch = ft_rejectartifact(cfg,data1ch);
    
%     keyboard  % dbcont when satisfied
%     clearvars data1ch
  end
  
  %% remerge each channel file into one cleaned data file
  
  cfg = [];
  cfg.inputfile   = chfile(~excludech);
  cfg.outputfile  = outputfile;
  
  data = ft_appenddata(cfg);
  
%   %% visualize result
%   
%   cfg                               = [];
%   
%   cfg = ft_databrowser(cfg,data);

end

%% save count of NaNs by rats & channels

save([datafolder 'Diff4Fear_autoart_z' num2str(zthresh) '_nancnt.mat'],'nancnt');
