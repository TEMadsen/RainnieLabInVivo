%% quick fix & TF analysis

info_diff4fear            % load all background info (filenames, etc)

foi = 1.1.^(0:49);
tapsmofrq = foi/10;
t_ftimwin = 2./tapsmofrq;       % for 3 tapers (K=3), T=2/W
toi = -30:min(t_ftimwin)/2:60;  % time windows overlap by at least 50%

%% select rat, trials, & channels

load([datafolder 'Diff4Fear_autoart_nancnt.mat']);

r = 3; p = 3;
t = 3;  % how many tones to use from beginning of phase

% Tone-triggered trials with artifacts removed individually from each
% channel by replacing with NaNs
inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
  'AllCleanLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];

%% load data

load(inputfile);

%% minimize trial size

cfg         = [];
cfg.trials  = find(data.trialinfo(:,1) > 0);
cfg.toilim  = [min(toi)-max(t_ftimwin) max(toi)+max(t_ftimwin)];

data = ft_redefinetrial(cfg, data);

%% select channels & trials, just to keep this short for now

trials = find(data.trialinfo(:,3) == p & data.trialinfo(:,1) <= t);
trlnancnt = NaN(numel(data.label),numel(trials));
for tr = 1:numel(trials)
  trlnancnt(:,tr) = sum(isnan(data.trial{trials(tr)}),2);
end
[~,chI] = sort(sum(trlnancnt,2));

%% loop through channels in order of least artifacts until satisfied
for ch = chI'
  %% define & check for outputfiles
  
  outputfile{1} = [datafolder num2str(ratT.ratnums(r)) filesep 'Clean' ...
    data.label{ch} 'SubTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  if exist(outputfile{1},'file')
    disp(['Loading sub-trial data for rat #' num2str(ratT.ratnums(r))])
    S = load(outputfile{1},'data');
    data1ch = S.data;
  else
    %% select channel (don't remove extra trials, it will save time later)
    
    cfg = [];
    cfg.channel     = data.label{ch};
    
    data1ch = ft_selectdata(cfg,data);
    
    %% mark NaNs as artifacts
    
    artifact.nans = [];
    
    for tr = 1:numel(data1ch.trial)
      % identify whether there are any NaNs at each timepoint
      tnan = any(isnan(data1ch.trial{tr}),1);   % that 1 is important in the single channel case!
      
      if any(tnan)
        % determine the file sample #s for this trial
        trsamp = data1ch.sampleinfo(tr,1):data1ch.sampleinfo(tr,2);
        
        while any(tnan)
          % start from the end so sample #s don't shift
          endnan = find(tnan,1,'last');
          tnan = tnan(1:endnan);  % remove any non-NaNs after this
          
          % find last non-NaN before the NaNs
          beforenan = find(~tnan,1,'last');
          
          if isempty(beforenan)   % if no more non-NaNs
            begnan = 1;
            tnan = false;   % while loop ends
          else  % still more to remove - while loop continues
            begnan = beforenan + 1;
            tnan = tnan(1:beforenan);  % remove the identified NaNs
          end
          
          % identify file sample #s that correspond to beginning
          % and end of this chunk of NaNs and append to
          % artifact.nans
          artifact.nans = [artifact.nans; ...
            trsamp(begnan) trsamp(endnan)];
        end   % while any(tnan)
      end   % if any(tnan)
    end   % for tr = 1:numel(data1ch.trial)
    
    %% remove NaNs from data & save 1st output file
    
    cfg                         = [];
    cfg.artfctdef.nan.artifact  = artifact.nans;
    cfg.artfctdef.reject        = 'partial';
    cfg.outputfile              = outputfile{1};
    
    data1ch = ft_rejectartifact(cfg,data1ch);
  end
  
  %% loop through 1st & last blocks of 3 CS+ & CS- per phase
  for p = [1 3 4]
    for s = 1:numel(stimuli)
      trlblk{1} = find(data1ch.trialinfo(:,1) <= t & ...
        data1ch.trialinfo(:,2) == s & data1ch.trialinfo(:,3) == p);
      trlblk{2} = find(data1ch.trialinfo(:,1) > ...
        (max(data1ch.trialinfo(:,1))-t) & data1ch.trialinfo(:,2) == s ...
        & data1ch.trialinfo(:,3) == p);
      for b = 1:numel(trlblk)
        %% calculate spectrogram
        
        cfg             = [];
        
        cfg.method      = 'mtmconvol';
        cfg.output      = 'pow';
        cfg.keeptrials  = 'no';
        cfg.foi         = foi;
        cfg.tapsmofrq   = tapsmofrq;
        cfg.t_ftimwin   = t_ftimwin;
        cfg.toi         = toi;
        cfg.pad         = 'nextpow2';
        
        cfg.trials      = trlblk{b};
        
        if b == 1
          cfg.outputfile  = [datafolder num2str(ratT.ratnums(r)) filesep '1st' ...
            num2str(t) stimuli{s} 'of' phases{p} '_' num2str(ratT.ratnums(r)) ...
            data.label{ch} 'Spectrogram.mat'];
        elseif b == numel(trlblk)
          cfg.outputfile  = [datafolder num2str(ratT.ratnums(r)) filesep 'Last' ...
            num2str(t) stimuli{s} 'of' phases{p} '_' num2str(ratT.ratnums(r)) ...
            data.label{ch} 'Spectrogram.mat'];
        else
          error('Add trial rules & title for middle block')
        end
        
        freq = ft_freqanalysis(cfg,data1ch);
        
        %% plot spectrogram
        
        cfg                 = [];
        cfg.baseline        = [-30 0];
        cfg.baselinetype    = 'db';
        
        figure(10*b+s);  clf;
        
        ft_singleplotTFR(cfg, freq);
        
        colormap(jet);
        xlabel('Time from Tone Onset (seconds)');
        ylabel('Frequency (Hz)');
        
        if b == 1
          title(['Spectrogram of 1st ' num2str(t) ' ' stimuli{s} 's of ' ...
            phases{p} ' for ' num2str(ratT.ratnums(r)) '''s ' data.label{ch}]);
        elseif b == numel(trlblk)
          title(['Spectrogram of Last ' num2str(t) ' ' stimuli{s} 's of ' ...
            phases{p} ' for ' num2str(ratT.ratnums(r)) '''s ' data.label{ch}]);
        end
        
%         keyboard; 	% pause to let me decide if this is enough channels
        
        if b == 1
          print([figurefolder '1st' ...
            num2str(t) stimuli{s} 'of' phases{p} '_' num2str(ratT.ratnums(r)) ...
            data.label{ch} 'Spectrogram'],'-dpng');
        elseif b == numel(trlblk)
          print([figurefolder 'Last' ...
            num2str(t) stimuli{s} 'of' phases{p} '_' num2str(ratT.ratnums(r)) ...
            data.label{ch} 'Spectrogram'],'-dpng');
        end
      end
    end
  end
end
