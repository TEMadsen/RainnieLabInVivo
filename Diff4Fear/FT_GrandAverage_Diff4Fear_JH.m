%% Average and merge spectrograms and coherograms from different subjects
% input can be raw/PTnorm/BLnorm
close all; clearvars; clc;    % clean slate

info_diff4fear               % load all background info (filenames, etc)

plt = true;

% rats 357 & 359 need their CS+/- reversed between input & output files
revCS = false(size(ratT,1),1);
revCS([5 7]) = true;
          if revCS(r)
            rev = 'Rev';  % to mark file as having reversed CS+/-
          else
            rev = '';     % to leave it off
          end
          

%% Average and Merge PT normalized data
for r = 1:(size(ratT,1),1)

inputfile = [foldername filesep 'omitnan_' normtype ...
            'NormBlock' num2str(b) rev stimuli{so} 'of' phases{p} '_' ...
            num2str(ratT.ratnums(r)) allchs{ch} '.mat'];


for g = 1:size(treatment,1)
  for b = 1:numel(bk2cal)
    freq2merge = cell(numel(eval([treatment{g} 'group'])),1);
    for rn = 1:numel(eval([treatment{g} 'group']))
      gr = eval([treatment{g} 'group' '(' int2str(rn) ')']);
      r = find(ratT.ratnums == gr);
      inputfile = [datafolder 'SpecCoherograms_' ...
        int2str(ratT.ratnums(r)) blocks{bk2cal(b)} '_PTnorm.mat'];
      load(inputfile);
      if strcmp(ratT.side{r},'Right') == 1
        freq.label{1} = 'NAc';
        freq.label{2} = 'BLA';
      elseif strcmp(ratT.side{r},'Left') == 1
        freq.label{1} = 'BLA';
        freq.label{2} = 'NAc';
      end
      freq.label{3} = 'BLA-NAc';
      freq.powspctrm = cat(1, freq.powspctrm, freq.coherence);
      outputfile = [datafolder 'SpecCoherograms_' ...
        int2str(ratT.ratnums(r)) blocks{bk2cal(b)} '_PTnorm_ElcTargetC.mat'];
      freq2merge{rn} = outputfile;
      save(outputfile, 'freq', '-v6');
      clear freq
    end
    %% grand average of spectrograms
    
    cfg                = [];
    cfg.parameter      = {'powspctrm'};
    cfg.inputfile      = freq2merge;
    %     cfg.channel        = {'NAc';'BLA'};
    freq = ft_freqgrandaverage(cfg);
    outputfile = [datafolder 'AvgSpectrograms_' ...
      treatment{g} blocks{bk2cal(b)} '_PTnorm.mat'];
    save(outputfile, 'freq', '-v6');
    %     freq1 = freq;
    %     clear freq
    %     %% grand average of coherograms
    %
    %     cfg                = [];
    %     cfg.parameter      = {'coherence'};
    %     cfg.inputfile      = freq2merge;
    %     cfg.channel        = {'NAc';'BLA'};
    %     freq = ft_freqgrandaverage(cfg);
    %     outputfile = [datafolder 'AvgSpectrograms_' ...
    %       treatment{g} blocks{bk2cal(b)} '_PTnorm.mat'];
    %     save(outputfile, 'freq', '-v6');
    %     freq2 = freq;
    %     clear file2merge freq
    
    if plt
      %% display raw spectrograms & coherograms
      
      for target = 1:3 %#ok<UNRCH> can be reached if plt = true is set in top cell
        cfg                 = [];
        
        cfg.parameter       = 'powspctrm';
        cfg.channel         = target;
        cfg.zlim           = 'maxabs';
        figure(target);  clf;
        ft_singleplotTFR(cfg, freq);
        colormap(jet);
        xlabel('Time from Tone Onset (seconds)');
        ylabel('Frequency (Hz)');
        
        if target < 3
          title([treatment{g} 'Averaged ' ...
            blocks{bk2cal(b)} ', ' ...
            regions{target} ' PTnorm_Power']);
          print([figurefolder treatment{g}...
            blocks{bk2cal(b)} '_Averaged_PTnorm_Spectrogram_'...
            regions{target}],'-dpng');
        else
          title([treatment{g} 'Averaged' ...
            blocks{bk2cal(b)}  ', PTnorm_Coherence']);
          print([figurefolder treatment{g} ...
            blocks{bk2cal(b)} freq.label{target} '_Averaged_PTnorm_Coherogram'],'-dpng');
        end
      end
      clear freq %1 freq2
    end
  end
  clear freq2merge freq
end