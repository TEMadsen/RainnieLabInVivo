%% Plots normalized spectrograms for each rat, channel, & block of 3 trials

CleanSlate  % provides a clean slate to start working with new data/scripts
% by clearing all variables, figures, and the command window

info_diff4fear            % load all background info & final parameters
errlog = cell2struct(cell(size(ratT,1),numel(allchs)),allchs,2); 	% MEs saved w/in nested structure

%% to select data normalized by custom BLnorm function

recalc = false;   % don't redo analysis unless there's a problem

cfg = [];
cfg.parameter = 'powspctrm';
cfg.baseline = 'BLfreq';

normtype = 'standard';  % based on FieldTrip's cfg.baselinetype = 'dB';
% decibel transform of power ratio
cfg.baselinetype = 'relative';
cfg.transtype = 'dB';
cfg.transorder = 'after';

% normtype = 'experimental';  % what I think makes the most sense
% cfg.baselinetype = 'z-score';
% cfg.transtype = 'ln';
% cfg.transorder = 'before';

%% to plot only specific file(s)

zthresh = 15;       % which artifact threshold to use the data from
spectype = 'faster';

%% retrieve old errlog

S = load([datafolder 'errlog' datestr(now,29) ...
  '_FT_NormTFAbyChTrl_Diff4Fear_TEM'],'errlog');
olderrlog = S.errlog;
S = [];
good = NaN(numel(allchs),size(ratT,1));

%% full loop

for r = 1:size(ratT,1)
  %   try
  %% check for old errors/warnings
  
  if isfield(olderrlog(r),'error')
    warning(['Rat #' num2str(ratT.ratnums(r)) ' Error: ' olderrlog(r).error.message])
  end
  if isfield(olderrlog(r),'warning')
    warning(['Rat #' num2str(ratT.ratnums(r)) ' Warning: ' olderrlog(r).warning.message])
  end
  
  %% collect good channel/phase/block info
  
  for ch = 1:numel(allchs)
    if ~isfield(olderrlog(r).(allchs{ch}),'good')
      errlog(r).(allchs{ch}).warning = ...
        ['No previous info on good blocks for r = ' num2str(r) ...
        ', ch = ' allchs{ch}];
      warning(errlog(r).(allchs{ch}).warning)
      recalc = true;
    else
      good(ch,r) = sum(olderrlog(r).(allchs{ch}).good(:));
    end
  end
  
  %% find available spectrogram files
  
  foldername = [datafolder num2str(ratT.ratnums(r))];
  filelist = what(foldername);
  
  %% preallocate variables
  
  pndx = cell(numel(filelist.mat),numel(phases));
  chndx = cell(numel(filelist.mat),numel(allchs));
  sndx = cell(numel(filelist.mat),numel(stimuli));
  
  %% find indices of relevant info in all filenames
  
  for p = 1:numel(phases)
    pndx(:,p) = strfind(filelist.mat,phases{p});
  end
  for ch = 1:numel(allchs)
    chndx(:,ch) = strfind(filelist.mat,allchs{ch});
  end
  for s = 1:numel(stimuli)
    sndx(:,s) = strfind(filelist.mat,stimuli{s});
  end
  tndx = strfind(filelist.mat,'of');      % to find tone #
  bndx = strfind(filelist.mat,'Block');   % to find block #
  zndx = strfind(filelist.mat,['_z' num2str(zthresh) '_']); % only finds desired files
  stndx = strfind(filelist.mat,spectype); % only finds desired files
  ntndx = strfind(filelist.mat,normtype); % only finds desired files
  
  %% put file info in a more legible format
  
  fileS = cell2struct(...
    [filelist.mat, pndx, chndx, sndx, tndx, bndx, zndx, stndx, ntndx], ...
    [{'filename'}; phases(:); allchs; ...
    {'CSplus'; 'CSminus'; 'tndx'; 'bndx'; 'zndx'; 'stndx'; 'ntndx'}], 2);
  
  %% loop through channels
  
  for ch = 1:numel(allchs)
    %       try
    %% don't bother if it's in the bottom half of channels for this rat
    
    if good(ch) < median(good)
      errlog(r).(allchs{ch}).warning = ...
        ['Fewer useable tone-blocks than most channels for ' ...
        allchs{ch} ' - skipping.'];
      warning(errlog(r).(allchs{ch}).warning)
      continue
    end
    
    %% loop through all phases
    
    for p = 1:numel(phases)
      %       try
      %%  & both stimulus types
      
      for s = 1:numel(stimuli)
        %           try
        if s == 1
          st = 'CSplus';
        elseif s == 2
          st = 'CSminus';
        end
        
        filenums = find(~arrayfun(@(x) isempty(x.(allchs{ch})),fileS) & ...
          ~arrayfun(@(x) isempty(x.(phases{p})),fileS) & ...
          ~arrayfun(@(x) isempty(x.(st)),fileS) & ...
          ~arrayfun(@(x) isempty(x.bndx),fileS) & ...
          ~arrayfun(@(x) isempty(x.ntndx),fileS));
        
        %% stop here to avoid continue
        
        if isempty(filenums)
          errlog(r).(allchs{ch}).(phases{p}).(st).warning = ...
            ['Averaged, ' normtype ' normalized data for ' phases{p} ...
            ' ' stimuli{s} ' not found for rat #' ...
            num2str(ratT.ratnums(r)) ', ' allchs{ch}];
          warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning)
          continue
        end
        
        %% loop through all freq files, IDing block #
        
        blocknum = NaN(size(filenums));
        for k = 1:numel(filenums)
          blocknum(k) = str2double(fileS(filenums(k)).filename(...
            fileS(filenums(k)).bndx+5:fileS(filenums(k)).(st)-1));
        end
        
        %% loop through blocks of 3 tones, plotting
        
        for b = blocknum'
          %                   try
          %% define input & output files
          
          inputfile = [foldername filesep ...
            filelist.mat{filenums(blocknum == b)}];
          
          outputfile = [foldername filesep 'Smooth' ...
            filelist.mat{filenums(blocknum == b)}];
          
          outputfig = [figurefolder normtype 'NormBlock' ...
            num2str(b) stimuli{s} 'of' phases{p} '_' ...
            num2str(ratT.ratnums(r)) allchs{ch} '_Spectrogram'];
          
          %% stop here to avoid continue
          
          if exist([outputfig '.png'],'file') && ...
              exist(outputfile,'file')
            errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning = ...
              ['Already plotted & reseaved averaged ' normtype ...
              ' normalized data for ' stimuli{s} ' block ' ...
              num2str(b) ' of ' phases{p} ' for rat #' ...
              num2str(ratT.ratnums(r)) ', ' allchs{ch}];
            warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning)
            continue
          end
          
          %% if it made it this far, load it
          
          disp(['Loading freq data from ' inputfile])
          S = load(inputfile,'grandavg');
          freq = S.grandavg;
          S = [];
          
          %% reduce time axis for easier visualization & smaller file size
          
          newpowspctrm = nan(1, numel(freq.freq), numel(smtoi));
          
          for f = 1:numel(freq.freq)
            for t = 1:numel(smtoi)
              newpowspctrm(1, f, t) = mean(freq.powspctrm(1, f, ...
                abs(freq.time - smtoi(t)) < tshift), 'omitnan');
            end
          end
          
          %% stop here to avoid continue
          
          if any(all(isnan(newpowspctrm),3))
            errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning = ...
              'All NaNs are found for at least one frequency after smoothing over time.';
            warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning)
            continue
          end
          
          %% finish smoothing & resave data
          % prep for remerging channels & plotting topographically
          
          freq.time = smtoi;
          freq.powspctrm = newpowspctrm;
          
          save(outputfile,'freq');
          
          %% display averaged normalized spectrogram
          
          fig = gcf; clf; fig.WindowStyle = 'docked';
          
          imagesc(freq.time, 1:numel(freq.freq), squeeze(freq.(cfg.parameter)));
          
          axis xy; colorbar; colormap(jet);
          
          P = unique(nextpow2(freq.freq));
          fticks = (2.^P(1:end-1))';   % for labeling log scale frequency axes
          base = mean(freq.freq(2:end) ./ freq.freq(1:end-1));  % ratio between foi
          fticklocs = (log(fticks) ./ log(base)) + 1; 	% convert to freq.freq indices
          
          ax = gca;
          ax.XTick = -30:10:60;
          ax.YTick = fticklocs;
          ax.YTickLabel = {num2str(fticks)};
          clim = max(abs(newpowspctrm(:)));
          ax.CLim = [-clim clim];
          xlabel('Time from Tone Onset (s)')
          ylabel('Frequency (Hz)')
          title(['Block #' num2str(b) ' of 3 ' stimuli{s} 's during ' ...
            phases{p} ' for Rat #' num2str(ratT.ratnums(r)) ', ' normtype ...
            ' Normalized Spectrogram for ' allchs{ch}])
          
          %% save figure
          
          print(fig, outputfig, '-dpng', '-r300');
          
          %                   catch ME
          %                     errlog(r).(allchs{ch}).(phases{p}).(st).block(b).error = ME;
          %                     warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).error.message)
          %                   end
        end
        %               catch ME
        %                 errlog(r).(allchs{ch}).(phases{p}).(st).error = ME;
        %                 warning(errlog(r).(allchs{ch}).(phases{p}).(st).error.message)
        %               end
      end
      %           catch ME
      %             errlog(r).(allchs{ch}).(phases{p}).error = ME;
      %             warning(errlog(r).(allchs{ch}).(phases{p}).error.message)
      %           end
    end
    %       catch ME
    %         errlog(r).(allchs{ch}).error = ME;
    %         warning(errlog(r).(allchs{ch}).error.message)
    %       end
  end
  %   catch ME
  %     errlog(r).error = ME;
  %     warning(errlog(r).error.message)
  %   end
end
