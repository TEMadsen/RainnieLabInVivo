info_diff4fear            % load all background info & final parameters
errlog = cell2struct(cell(size(ratT,1),numel(allchs)),allchs,2); 	% MEs saved w/in nested structure

% rats 357 & 359 need their CS+/- reversed between input & output files
% revCS = false(size(ratT,1),1);
% revCS([5 7]) = true;
blocks = [1, 3, 1, 3; 1,1 ,3 4; 3, 9, 1, 9];
% blocks = {'Acq1'; 'Acq3'; 'Exp1'; 'Ext3'};
% Raw data + log transformeed data+ 2 descriptive stats: Mean and SD

%% to plot only specific file(s)

zthresh = 15;       % which artifact threshold to use the data from
spectype = 'faster';
normtype = 'standard';  % based on FieldTrip's cfg.baselinetype = 'dB';
%%

% stmulus/block/channel
delta = cell(2,4); %1.5-5.5 Hz
gamma = cell(2,4); %45 - 59 Hz

for si = 1:size(stimuli,1)
    for rgn= 1:size(regions,1)
        delta{si,rgn} = zeros(size(ratT,1),size(blocks,2));
        gamma{si,rgn} = zeros(size(ratT,1),size(blocks,2));
    end
end


for rgn = 1:size(regions,1)
    for si = 1:size(stimuli,1)
        for bl = 1:size(blocks,2)
            for r = 1:size(ratT,1)
                
                %                 if revCS(r)
                %                     rev = 'Rev';  % to mark file as having reversed CS+/-
                %                 else
                %                     rev = '';     % to leave it off
                %                 end
                
                %                 inputfile = [datafolder num2str(ratT.ratnums(r)) filesep 'omitnan_' normtype ...
                %                     'NormBlock' num2str(blocks(1,bl)) rev stimuli{si} 'of' phases{blocks(2,bl)} '_' ...
                %                     num2str(ratT.ratnums(r)) eval(['bestchs(r).' regions{rgn}]) '.mat'];
                % result of normalization is weird, so try raw data
                %                 inputfile = [datafolder num2str(ratT.ratnums(r)) filesep stimuli{si} ...
                %                     num2str(blocks(3,bl)) 'of' phases{blocks(2,bl)} '_' num2str(ratT.ratnums(r)) ...
                %                     eval(['bestchs(r).' regions{rgn}]) '_z15_fasterwavSpectrogram.mat'];
                
                inputfile = [datafolder num2str(ratT.ratnums(r)) filesep 'omitnan_' normtype ...
                    'NormBlock'  num2str(blocks(1,bl)) stimuli{si} 'of' phases{blocks(2,bl)} '_' ...
                    num2str(ratT.ratnums(r)) '_4ch.mat'];

                
                if ~exist(inputfile,'file')
                    warning([inputfile ...
                        ' was not found.'])
                    continue
                else
                    load(inputfile);
                    
                end
                
                
                delta{si,rgn}(r,bl) = mean(mean(freq.powspctrm(rgn,6:20,315:628),'omitnan'),'omitnan');
                gamma{si,rgn}(r,bl) = mean(mean(freq.powspctrm(rgn,45:48,315:628),'omitnan'),'omitnan');
                
            end
            
        end
    end
end