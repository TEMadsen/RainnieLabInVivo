%% Plots & saves normalized spectrograms for each channel & trial
%% OLD!!!  DON'T USE!!!  Replaced by FT_NormTFA_Diff4Fear_TEM.m

CleanSlate  % provides a clean slate to start working with new data/scripts
% by clearing all variables, figures, and the command window

info_diff4fear            % load all background info & final parameters
errlog = cell2struct(cell(size(ratT,1),numel(allchs)),allchs,2); 	% MEs saved w/in nested structure

%% to normalize data using custom BLnorm function before plotting

cfg = [];
cfg.parameter = 'powspctrm';
cfg.baseline = 'BLfreq';

normtype = 'standard';  % based on FieldTrip's cfg.baselinetype = 'dB';
% decibel transform of power ratio
cfg.baselinetype = 'relative';
cfg.transtype = 'dB';
cfg.transorder = 'after';

% normtype = 'experimental';  % what I think makes the most sense
% cfg.baselinetype = 'z-score';
% cfg.transtype = 'ln';
% cfg.transorder = 'before';

%% to plot only specific file(s)

zthresh = 15;       % which artifact threshold to use the data from
spectype = 'faster';

%% full loop

for r = 1:size(ratT,1)
  try
    %% find available spectrogram files
    
    foldername = [datafolder num2str(ratT.ratnums(r))];
    filelist = what(foldername);
    
    %% preallocate variables
    
    pndx = cell(numel(filelist.mat),numel(phases));
    chndx = cell(numel(filelist.mat),numel(allchs));
    sndx = cell(numel(filelist.mat),numel(stimuli));
    
    %% find indices of relevant info in all filenames
    
    for p = 1:numel(phases)
      pndx(:,p) = strfind(filelist.mat,phases{p});
    end
    for ch = 1:numel(allchs)
      chndx(:,ch) = strfind(filelist.mat,allchs{ch});
    end
    for s = 1:numel(stimuli)
      sndx(:,s) = strfind(filelist.mat,stimuli{s});
    end
    tndx = strfind(filelist.mat,'of');
    zndx = strfind(filelist.mat,['_z' num2str(zthresh) '_']); % only finds desired files
    stndx = strfind(filelist.mat,spectype); % only finds desired files
    
    %% put file info in a more legible format
    
    fileS = cell2struct([filelist.mat, pndx, chndx, sndx, tndx, zndx, stndx], ...
      [{'filename'}; phases(:); allchs; {'CSplus'; 'CSminus'; 'tndx'; 'zndx'; 'stndx'}], 2);
    
    %% loop through channels
    
    for ch = 1:numel(allchs)
      try
        %% identify baseline file
        
        filenums = find(~arrayfun(@(x) isempty(x.(allchs{ch})),fileS) & ...
          ~arrayfun(@(x) isempty(x.Baseline),fileS) & ...
          ~arrayfun(@(x) isempty(x.zndx),fileS) & ...
          ~arrayfun(@(x) isempty(x.stndx),fileS));
        
        %% stop here to avoid continue
        
        if numel(filenums) < 1
          errlog(r).(allchs{ch}).Baseline = ...
            ['No baseline freq file found for rat #' ...
            num2str(ratT.ratnums(r)) ', ' allchs{ch}];
          warning(errlog(r).(allchs{ch}).Baseline)
          continue
        elseif numel(filenums) > 1
          errlog(r).(allchs{ch}).Baseline = ...
            ['More than one baseline freq file found for rat #' ...
            num2str(ratT.ratnums(r)) ', ' allchs{ch}];
          warning(errlog(r).(allchs{ch}).Baseline)
          continue
%           keyboard  % select one? narrow criteria?
        else
          %% load baseline freq file
          
          baselinefile = [foldername filesep fileS(filenums).filename];
          disp(['Loading baseline freq data for rat #' ...
            num2str(ratT.ratnums(r)) ', ' allchs{ch}])
          S = load(baselinefile,'freq');
          BLfreq = S.freq;
          S = [];
        end
        
        %% check for all NaNs in any frequency
        
        if any(all(isnan(BLfreq.powspctrm),3))
          errlog(r).(allchs{ch}).Baseline = ...
            'At least one foi is all NaNs in baseline file.';
          warning(errlog(r).(allchs{ch}).Baseline)
          continue
        end
        
        %% assume channels are bad, add 1 for each good stimulus type
        
        errlog(r).(allchs{ch}).good = zeros(numel(phases),3);  % 3 blocks/phase
        
        %% loop through all phases
        
        for p = 1:numel(phases)
          try
            %%  & both stimulus types
            
            for s = 1:numel(stimuli)
              try
                if s == 1
                  st = 'CSplus';
                elseif s == 2
                  st = 'CSminus';
                end
                
                filenums = find(~arrayfun(@(x) isempty(x.(allchs{ch})),fileS) & ...
                  ~arrayfun(@(x) isempty(x.(phases{p})),fileS) & ...
                  ~arrayfun(@(x) isempty(x.(st)),fileS) & ...
                  ~arrayfun(@(x) isempty(x.zndx),fileS) & ...
                  ~arrayfun(@(x) isempty(x.stndx),fileS));
                
                %% stop here to avoid continue
                
                if numel(filenums) < 9   % spectrograms don't exist for all 9 tones
                  errlog(r).(allchs{ch}).(phases{p}).(st) = ['Fewer than 9 ' ...
                    stimuli{s} ' freq files found during ' phases{p} ' for rat #' ...
                    num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                  warning(errlog(r).(allchs{ch}).(phases{p}).(st))
                  continue
                end
                
                %% loop through all freq files, IDing tone #
                
                tonenum = NaN(size(filenums));
                for k = 1:numel(filenums)
                  tonenum(k) = str2double(fileS(filenums(k)).filename(numel(stimuli{s})+1:...
                    fileS(filenums(k)).tndx-1));
                end
                
                %% check for duplicates or >9
                
                if numel(tonenum) > numel(unique(tonenum))
                  errlog(r).(allchs{ch}).(phases{p}).(st) = ['Duplicate ' ...
                    stimuli{s} ' tone #s found during ' phases{p} ' for rat #' ...
                    num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                  warning(errlog(r).(allchs{ch}).(phases{p}).(st))
                  continue
%                   keyboard  % select one? narrow criteria?
                end
                
                if max(tonenum) > 9
                  errlog(r).(allchs{ch}).(phases{p}).(st) = [stimuli{s} ...
                    ' tone #s >9 found during ' phases{p} ' for rat #' ...
                    num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                  warning(errlog(r).(allchs{ch}).(phases{p}).(st))
                  continue
%                   keyboard  % select one? narrow criteria?
                end
                
                %% loop through blocks of 3 tones, averaging
                
                for b = 1:3
                  try
                    %% check for averaged/normalized freq file
                    
                    groupoutput = [foldername filesep normtype 'NormBlock' ...
                      num2str(b) stimuli{s} 'of' phases{p} '_' ...
                      num2str(ratT.ratnums(r)) allchs{ch} '.mat'];
                    
                    %% stop here to avoid continue
                    
                    if exist(groupoutput,'file')
                      errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning = ...
                        ['Already averaged ' normtype ' normalized data for ' ...
                        stimuli{s} ' block ' num2str(b) ' of ' phases{p} ...
                        ' for rat #' num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                      warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning)
                      continue
                    end
                    
                    %% loop through 3 tones per block, normalizing
                    
                    outputfile = cell(3,1);
                    toi = cell(3,1);
                    
                    for t = 1:3
                      try
                        %% check for normalized freq file
                        
                        tn = t + 3*(b-1);
                        
                        outputfile{t} = [foldername filesep normtype 'Norm' ...
                          stimuli{s} num2str(tn) 'of' phases{p} '_' ...
                          num2str(ratT.ratnums(r)) allchs{ch} '.mat'];
                        
                        %% stop here to avoid continue
                        
                        if exist(outputfile{t},'file')
                          errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).warning = ...
                            ['Already calculated ' normtype ' normalized data for ' ...
                            stimuli{s} ' ' num2str(tn) ' of ' phases{p} ' for rat #' ...
                            num2str(ratT.ratnums(r)) ', ' allchs{ch}];
                          warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).warning)
                          continue
                        end
                        
                        %% load the raw freq file
                        
                        rawfile = [foldername filesep ...
                          fileS(filenums(tonenum(k))).filename];
                        disp(['Loading trial freq data for ' stimuli{s} ' ' ...
                          num2str(tn) ' of ' phases{p} ' for rat #' ...
                          num2str(ratT.ratnums(r)) ', ' allchs{ch}])
                        load(rawfile,'freq');
                        
                        %% verify sufficiently similar foi to baseline
                        
                        if numel(freq.freq) ~= numel(BLfreq.freq)
                          errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).warning = ...
                            ['Trial freq file contains ' numel(freq.freq) ...
                            ' of baseline freq''s ' numel(BLfreq.freq) ' foi'];
                          warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).warning)
                          continue
                        end
                        
                        if max(diff([freq.freq; BLfreq.freq],1,1)) > ...
                            min(min(diff([freq.freq; BLfreq.freq],1,2)))
                          errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).warning = ...
                            ['Trial freq foi differs from baseline freq foi by up to ' ...
                            num2str(max(diff([freq.freq; BLfreq.freq],1,1)))];
                          warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).warning)
                          continue
                        end
                        
                        %% remember toi to compare before averaging across trials
                        
                        toi{t} = freq.time;
                        
                        %% check for all NaNs for any frequency before, during, or after tone
                        
                        if any(all(isnan(freq.powspctrm(1,:,freq.time > 0 & freq.time < 30)),3))
                          errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).warning = ...
                            'At least one foi is all NaNs during the tone.';
                          warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).warning)
                          continue
                        end
                        
                        if any(all(isnan(freq.powspctrm(1,:,freq.time < 0)),3)) || ...
                            any(all(isnan(freq.powspctrm(1,:,freq.time > 30)),3))
                          errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).warning = ...
                            'At least one foi is all NaNs before and/or after the tone.';
                          warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).warning)
                          continue
                        end
                        
                        %% normalize the raw freq file by baseline
                        
                        freq = BLnorm(cfg,freq,BLfreq);
                        
                        %% save the output
                        
                        disp(['Saving ' normtype ' normalized data for ' ...
                          stimuli{s} ' ' num2str(tn) ' of ' phases{p} ...
                          ' for rat #' num2str(ratT.ratnums(r)) ', ' allchs{ch}])
                        save(outputfile{t},'freq');
                        
                      catch ME
                        errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).error = ME;
                        warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).tone(t).error.message)
                      end
                    end
                    %% verify that all 3 tones were normalized & have similar toi
                    
                    if ~all(cellfun(@exist,outputfile,repelem({'file'},3,1)))
                      errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning = ...
                        'Not all tone trials were normalized.';
                      warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning)
                      continue
                    end
                    
                    if ~isequal(toi{:})
                      errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning = ...
                        'Tone trials have unequal toi.';
                      warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning)
                      continue
                    end
                    
                    %% average the block of 3 tones
                    
                    cfgblock              = [];
                    cfgblock.inputfile    = outputfile;
                    cfgblock.outputfile   = groupoutput;
                    
                    grandavg = ft_freqgrandaverage(cfgblock);
                    
                    %% check for errors & if none, remember this as a good channel
                    
                    if any(all(isnan(grandavg.powspctrm(1,:,grandavg.time > 0 & grandavg.time < 30)),3))
                      errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning = ...
                        'After averaging, at least one foi is all NaNs during the tone.';
                      warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning)
                      continue
                    end
                    
                    if any(all(isnan(grandavg.powspctrm(1,:,grandavg.time < 0)),3)) || ...
                        any(all(isnan(grandavg.powspctrm(1,:,grandavg.time > 30)),3))
                      errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning = ...
                        'After averaging, at least one foi is all NaNs before and/or after the tone.';
                      warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).warning)
                      continue
                    end
                    
                    errlog(r).(allchs{ch}).good(p,b) = ...
                      errlog(r).(allchs{ch}).good(p,b) + 1;
                    
                  catch ME
                    errlog(r).(allchs{ch}).(phases{p}).(st).block(b).error = ME;
                    warning(errlog(r).(allchs{ch}).(phases{p}).(st).block(b).error.message)
                  end
                end
              catch ME
                errlog(r).(allchs{ch}).(phases{p}).(st).error = ME;
                warning(errlog(r).(allchs{ch}).(phases{p}).(st).error.message)
              end
            end
          catch ME
            errlog(r).(allchs{ch}).(phases{p}).error = ME;
            warning(errlog(r).(allchs{ch}).(phases{p}).error.message)
          end
        end
      catch ME
        errlog(r).(allchs{ch}).error = ME;
        warning(errlog(r).(allchs{ch}).error.message)
      end
    end
  catch ME
    errlog(r).error = ME;
    warning(errlog(r).error.message)
  end
end

%% save errlog

disp('Analysis complete!  Saving errlog.')
save([datafolder 'errlog' datestr(now,29) ...
  '_FT_NormTFAbyChTrl_Diff4Fear_TEM'],'errlog')
