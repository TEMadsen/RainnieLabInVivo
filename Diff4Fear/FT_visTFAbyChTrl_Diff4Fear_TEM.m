%% Plots & saves raw or normalized spectrograms for each channel & trial

CleanSlate  % provides a clean slate to start working with new data/scripts
% by clearing all variables, figures, and the command window

info_diff4fear            % load all background info & final parameters
errlog = cell(size(ratT,1),1);   % MEs saved w/in k# cells

%% to find specific file(s)

zthresh = 15;       % which artifact threshold to use the data from
spectype = {'faster','optwav'};

%% full loop

for r = 10:size(ratT,1)
  %% find available spectrogram files
  
  foldername = [datafolder num2str(ratT.ratnums(r))];
  filelist = what(foldername);
  
  %% loop through all files
  
  for k = 1:numel(filelist.mat)
    try
      %% parse filename to ID data
      
      s = find(strncmp(filelist.mat{k}, stimuli, ...
        min(cellfun(@numel,stimuli))));
      if isempty(s)
        continue
      end
      remain = filelist.mat{k}(numel(stimuli{s})+1:end);
      indx = strfind(remain,'of');
      if isempty(indx)
        continue
      end
      tn = str2double(remain(1:indx-1));
      if isnan(tn)
        continue
      end
      remain = remain(indx+2:end);  % skip '_'
      p = find(strncmp(remain, phases, min(cellfun(@numel,phases))));
      if isempty(p)
        continue
      end
      remain = remain(numel(phases{p})+5:end);  % skip '_' & 3-digit ratnum
      ch = find(strncmp(remain, allchs, min(cellfun(@numel,allchs))));
      if numel(ch) ~= 1
        continue
      end
      remain = remain(numel(allchs{ch})+1:end);
      indx = strfind(remain,'_');
      if numel(indx) < 2
        z = 10; % starting value, before I added zthresh to filename
      else
        z = str2double(remain(indx(1)+2:indx(2)-1));  % skip '_z', stop before '_'
        remain = remain(indx(2)+1:end);   % skip after 2nd '_'
      end
      indx = strfind(remain,'.'); % save the last part (except file
      remain = remain(1:indx-1);  % extension) to use in titles and filenames
      
      %% determine whether this file meets criteria
      
      if ~any(strncmpi(remain, spectype, min(cellfun(@numel,spectype)))) ...
          || z ~= zthresh
        continue
      end
      
      %% check for printed figure
      
      outputfig = [figurefolder stimuli{s} num2str(tn) 'of' phases{p} '_' ...
        num2str(ratT.ratnums(r)) allchs{ch} remain '_logf'];
      
      if exist([outputfig '.png'],'file')
        continue
      end
      
      %% if it made it this far, this is a relevant freq file, so load it
      
      disp(['Loading freq data from ' filelist.mat{k}])
      S = load([foldername filesep filelist.mat{k}],'freq');
      freq = S.freq;
      S = [];
      
      %% check for altered foi, warn & flag image
      
      orig.foi = foi;
      switch remain(1:11)
        case 'fasterwavSp'
          foi = fasterwav.foi;
        case 'fastwavSpec'
          foi = fastwav.foi;
        case 'optwavSpect'
          foi = optwav.foi;
        case 'wavSpectrog'
          foi = wav.foi;
        case 'altSpectrog'
          foi = alt.foi;
        case 'Spectrogram'
          foi = orig.foi;
        otherwise
          warning('unknown label -> comparing freq.freq to freq.cfg.foi')
          foi = freq.cfg.foi;
      end
      
      if numel(freq.freq) ~= numel(foi)
        warning([remain ' output has ' num2str(numel(freq.freq)) ...
          ' of ' num2str(numel(foi)) ' requested foi.'])
        maxfdiff = inf;
      else
        maxfdiff = max(abs(freq.freq - foi));
      end
      
      %% reduce time axis & convert to dB for easier visualization
      
      newpowspctrm = nan(1, numel(freq.freq), numel(smtoi));
      
      for f = 1:numel(freq.freq)
        for t = 1:numel(smtoi)
          newpowspctrm(1, f, t) = mean(10*log10(freq.powspctrm(1, f, ...
            abs(freq.time - smtoi(t)) < tshift)), 'omitnan');
        end
      end
      
      if any(all(isnan(newpowspctrm),3))
        continue  % skip this file
      end
      
      freq.time = smtoi;
      freq.powspctrm = newpowspctrm;
      
      %     %% save color scales
      %
      %     if tr > 10 && tr < 18   % during shocks
      %       powrange(tr,:) = [min(min(newpowspctrm(1,:,[1:29 33:51]))) ...
      %         max(max(newpowspctrm(1,:,[1:29 33:51])))];
      %     else
      %       powrange(tr,:) = [min(min(newpowspctrm(1,:,:))) ...
      %         max(max(newpowspctrm(1,:,:)))];
      %     end
      
      %% plot spectrogram (maybe change to keep log axis?)
      % can plot_matrix_TEM do that already?
      
      % how about adding z-score normalization, either to pre-tone:
      % [-30+(tshift+max(t_ftimwin)) 0-(tshift+max(t_ftimwin))]
      % or true baseline (calculated with another run through freq_analysis):
      % [data1ch.time{1}(1)+(tshift+max(t_ftimwin)) -30-(tshift+max(t_ftimwin))]
      
      cfg                 = [];
      cfg.parameter       = 'powspctrm';
      cfg.colormap        = jet;
      
      fig = gcf; clf; fig.WindowStyle = 'docked';
      
%       ft_singleplotTFR(cfg, freq);
%       plot_matrix_TEM(freq.(cfg.parameter), freq.time, freq.freq, 'n') 
      imagesc(freq.time, 1:numel(freq.freq), squeeze(freq.(cfg.parameter))); 
      axis xy; colorbar; %colormap(cfg.colormap);
      P = unique(nextpow2(freq.freq));
      fticks = (2.^P(1:end-1))';   % for labeling log scale frequency axes
      fticklocs = (log(fticks) ./ log(base)) + 1; 	% convert to freq.freq indices
      ax = gca;
      ax.XTick = -30:10:60;
      ax.YTick = fticklocs;
      ax.YTickLabel = {num2str(fticks)};
      xlabel('Time from Tone Onset (s)')
      ylabel('Frequency (Hz)')
      title([stimuli{s} ' #' num2str(tn) ' of ' phases{p} ', Rat #' ...
        num2str(ratT.ratnums(r)) ', Simple ' allchs{ch} ...
        ' ' remain ', maxfdiff = ' num2str(maxfdiff)])
      print(fig, outputfig, '-dpng', '-r300');
    catch ME
      warning(ME.message);
      errlog{r}(k,:) = {ME,filelist.mat{k}};
    end
  end
  
  %   %% normalize scales across trials
  %
  %   powrange = [min(powrange(:,1)) max(powrange(:,2))];
  %   for tr = 1:size(trls,1)
  %     fig = figure(tr+1);
  %     for ch = 1:ncomp
  %       subplot(nw,nh,ch);
  %
  %     end
  %
  %     %% save figure
  %
  %     print(fig,[figurefolder num2str(ratT.ratnums(r)) 'Merged_t' ...
  %       num2str(tr) 'ICA_Spectrograms'],'-dpng','-r300')
end
