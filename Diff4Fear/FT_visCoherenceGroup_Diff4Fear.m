% Average, Calculating and plot coherence

info_diff4fear
% 2 type of CS
% 4 blocks :  Acq1 Acq3 Exp1 Ext3
blocks = [1, 3, 1, 3; 1,1 ,3 4];
% blocks = {'Acq1'; 'Acq3'; 'Exp1'; 'Ext3'};
% Raw data + log transformeed data+ 2 descriptive stats: Mean and SD

revCS = false(size(ratT,1),1);
revCS([5 7]) = true;

normtype = 'absolute';

%% input data
PrL_IL = cell(2,4,4);
PrL_BLA = cell(2,4,4);
PrL_vHPC = cell(2,4,4);

IL_BLA = cell(2,4,4);
IL_vHPC = cell(2,4,4);
BLA_vHPC = cell(2,4,4);

CohPr = {'PrL-IL'; 'PrL-BLA'; 'PrL-vHPC'; 'IL-BLA'; 'IL-vHPC'; 'BLA-vHPC'};

for si = 1:size(stimuli,1)
    for bl = 1:size(blocks,1)
        for dt= 1:4
            switch dt
                case 1 %dB Raw
                    PrL_IL{si,bl,dt} = zeros(size(ratT,1),9808);
                    PrL_BLA{si,bl,dt} = zeros(size(ratT,1),9808);
                    PrL_vHPC{si,bl,dt} = zeros(size(ratT,1),9808);
                    IL_BLA{si,bl,dt} = zeros(size(ratT,1),9808);
                    IL_vHPC{si,bl,dt} = zeros(size(ratT,1),9808);
                    BLA_vHPC{si,bl,dt} = zeros(size(ratT,1),9808);
                case 2 % dB Mean
                    PrL_IL{si,bl,dt} = zeros(1,9808);
                    PrL_BLA{si,bl,dt} = zeros(1,9808);
                    PrL_vHPC{si,bl,dt} = zeros(1,9808);
                    IL_BLA{si,bl,dt} = zeros(1,9808);
                    IL_vHPC{si,bl,dt} = zeros(1,9808);
                    BLA_vHPC{si,bl,dt} = zeros(1,9808);
                case 3 % dB SD
                    PrL_IL{si,bl,dt} = zeros(1,9808);
                    PrL_BLA{si,bl,dt} = zeros(1,9808);
                    PrL_vHPC{si,bl,dt} = zeros(1,9808);
                    IL_BLA{si,bl,dt} = zeros(1,9808);
                    IL_vHPC{si,bl,dt} = zeros(1,9808);
                    BLA_vHPC{si,bl,dt} = zeros(1,9808);
                case 4 % dB CI
                    PrL_IL{si,bl,dt} = zeros(2,9808);
                    PrL_BLA{si,bl,dt} = zeros(2,9808);
                    PrL_vHPC{si,bl,dt} = zeros(2,9808);
                    IL_BLA{si,bl,dt} = zeros(2,9808);
                    IL_vHPC{si,bl,dt} = zeros(2,9808);
                    BLA_vHPC{si,bl,dt} = zeros(2,9808);
            end
        end
    end
end

for si = 1:size(stimuli,1)
    for bl = 1:size(blocks,2)
        for r= 1:size(ratT,1)
            if revCS(r)
                rev = 'Rev';  % to mark file as having reversed CS+/-
            else
                rev = '';     % to leave it off
            end
            
            inputfile = [datafolder num2str(ratT.ratnums(r)) filesep 'omitnan_Coherence' normtype ...
                'NormBlock' num2str(blocks(1,bl)) rev stimuli{si} 'of' phases{blocks(2,bl)} '_' ...
                num2str(ratT.ratnums(r)) '_4ch.mat'];
            
            if ~exist(inputfile,'file')
                warning([inputfile ...
                    ' was not found.'])
                continue
            else
                load(inputfile);
                
                indxPrL_IL= strcmp('PrL-IL',freq.label);
                indxPrL_BLA= strcmp('PrL-BLA',freq.label);
                indxPrL_vHPC= strcmp('PrL-vHPC',freq.label);
                indxIL_BLA= strcmp('IL-BLA',freq.label);
                indxIL_vHPC= strcmp('IL-vHPC',freq.label);
                indxBLA_vHPC= strcmp('BLA-vHPC',freq.label);
                
                PrL_IL{si,bl,1}(r,:)= freq.powspctrm(indxPrL_IL,:);
                PrL_BLA{si,bl,1}(r,:)= freq.powspctrm(indxPrL_BLA,:);
                PrL_vHPC{si,bl,1}(r,:)= freq.powspctrm(indxPrL_vHPC,:);
                IL_BLA{si,bl,1}(r,:)= freq.powspctrm(indxIL_BLA,:);
                IL_vHPC{si,bl,1}(r,:)= freq.powspctrm(indxIL_vHPC,:);
                BLA_vHPC{si,bl,1}(r,:)= freq.powspctrm(indxBLA_vHPC,:);
                
            end
        end
        
        while ~isempty(find(~PrL_IL{si,bl,1}(:,1),1)) >0
            z = find(~PrL_IL{si,bl,1}(:,1),1);
            PrL_IL{si,bl,1}(z,:)= [];
            PrL_BLA{si,bl,1}(z,:)= [];
            PrL_vHPC{si,bl,1}(z,:)= [];
            IL_BLA{si,bl,1}(z,:)= [];
            IL_vHPC{si,bl,1}(z,:)= [];
            BLA_vHPC{si,bl,1}(z,:)= [];
        end
        
        PrL_IL{si,bl,2}(1,:)= mean(PrL_IL{si,bl,1},1);
        PrL_BLA{si,bl,2}(1,:)= mean(PrL_BLA{si,bl,1},1);
        PrL_vHPC{si,bl,2}(1,:)= mean(PrL_vHPC{si,bl,1},1);
        IL_BLA{si,bl,2}(1,:)= mean(IL_BLA{si,bl,1},1);
        IL_vHPC{si,bl,2}(1,:)= mean(IL_vHPC{si,bl,1},1);
        BLA_vHPC{si,bl,2}(1,:)= mean(BLA_vHPC{si,bl,1},1);
        
        PrL_IL{si,bl,3}(1,:)= std(PrL_IL{si,bl,1},0,1);
        PrL_BLA{si,bl,3}(1,:)= std(PrL_BLA{si,bl,1},0,1);
        PrL_vHPC{si,bl,3}(1,:)= std(PrL_vHPC{si,bl,1},0,1);
        IL_BLA{si,bl,3}(1,:)= std(IL_BLA{si,bl,1},0,1);
        IL_vHPC{si,bl,3}(1,:)= std(IL_vHPC{si,bl,1},0,1);
        BLA_vHPC{si,bl,3}(1,:)= std(BLA_vHPC{si,bl,1},0,1);
        
        for f = 1:9808
            PrL_IL{si,bl,4}(1,f) = PrL_IL{si,bl,2}(1,f)-1.96*PrL_IL{si,bl,3}(1,f);
            PrL_IL{si,bl,4}(2,f) = PrL_IL{si,bl,2}(1,f)+1.96*PrL_IL{si,bl,3}(1,f);
            PrL_BLA{si,bl,4}(1,f)= PrL_BLA{si,bl,2}(1,f)-1.96*PrL_BLA{si,bl,3}(1,f);
            PrL_BLA{si,bl,4}(2,f)= PrL_BLA{si,bl,2}(1,f)+1.96*PrL_BLA{si,bl,3}(1,f);
            PrL_vHPC{si,bl,4}(1,f)=PrL_vHPC{si,bl,2}(1,f)-1.96*PrL_vHPC{si,bl,3}(1,f);
            PrL_vHPC{si,bl,4}(2,f)= PrL_vHPC{si,bl,2}(1,f)+1.96*PrL_vHPC{si,bl,3}(1,f);
            IL_BLA{si,bl,4}(1,f)= IL_BLA{si,bl,2}(1,f)-1.96*IL_BLA{si,bl,3}(1,f);
            IL_BLA{si,bl,4}(2,f)= IL_BLA{si,bl,2}(1,f)+1.96*IL_BLA{si,bl,3}(1,f);
            IL_vHPC{si,bl,4}(1,f)= IL_vHPC{si,bl,2}(1,f)-1.96*IL_vHPC{si,bl,3}(1,f);
            IL_vHPC{si,bl,4}(2,f)= IL_vHPC{si,bl,2}(1,f)+1.96*IL_vHPC{si,bl,3}(1,f);
            BLA_vHPC{si,bl,4}(1,f)= BLA_vHPC{si,bl,2}(1,f)-1.96*BLA_vHPC{si,bl,3}(1,f);
            BLA_vHPC{si,bl,4}(2,f)= BLA_vHPC{si,bl,2}(1,f)+1.96*BLA_vHPC{si,bl,3}(1,f);
        end
    end
end


freqN = freq.freq;

LiClr = {'r'; 'g'; 'b'; 'm'};

for si = 1:size(stimuli,1)
    outputfig = [figurefolder normtype 'NormBlock' stimuli{si}...
        'PrL_IL_Coherence'];
    fig = gcf;
    hold on
    for bl = 1:size(blocks,2)
        input = NaN(3,3254);
        input(1,:) = PrL_IL{si,bl,2}(1,1:3254);
        input(2,:) = PrL_IL{si,bl,4}(1,1:3254);
        input(3,:) = PrL_IL{si,bl,4}(2,1:3254);
        
        plot_ci(freqN(1:3254),input , 'PatchColor', LiClr{bl}, 'PatchAlpha', 0.1, ...
            'MainLineWidth', 0.5, 'MainLineStyle', '-', 'MainLineColor', LiClr{bl}, ...
            'LineWidth', 0.03, 'LineStyle','--', 'LineColor', 'k');

    end
    ylabel('Relative coherence to baseline')
    xlabel('Frequency (Hz)')
    title('PrL-IL')
    
    hold off
    print(fig, outputfig, '-dpng', '-r300');
    clf;
end

for si = 1:size(stimuli,1)
    outputfig = [figurefolder normtype 'NormBlock' stimuli{si}...
        'PrL_BLA_Coherence'];
    fig = gcf;
    hold on
    for bl = 1:size(blocks,2)
        input = NaN(3,3254);
        input(1,:) = PrL_BLA{si,bl,2}(1,1:3254);
        input(2,:) = PrL_BLA{si,bl,4}(1,1:3254);
        input(3,:) = PrL_BLA{si,bl,4}(2,1:3254);
        
        plot_ci(freqN(1:3254),input , 'PatchColor', LiClr{bl}, 'PatchAlpha', 0.1, ...
            'MainLineWidth', 0.5, 'MainLineStyle', '-', 'MainLineColor', LiClr{bl}, ...
            'LineWidth', 0.03, 'LineStyle','--', 'LineColor', 'k');

    end
    ylabel('Relative coherence to baseline')
    xlabel('Frequency (Hz)')
    title('PrL-BLA')
    
    hold off
    legend('Hab','Acq-2','Exp-1','Ext-3')
    print(fig, outputfig, '-dpng', '-r300');
    clf;
end

for si = 1:size(stimuli,1)
    outputfig = [figurefolder normtype 'NormBlock' stimuli{si}...
        'PrL_vHPC_Coherence'];
    fig = gcf;
    hold on
    for bl = 1:size(blocks,2)
        input = NaN(3,3254);
        input(1,:) = PrL_vHPC{si,bl,2}(1,1:3254);
        input(2,:) = PrL_vHPC{si,bl,4}(1,1:3254);
        input(3,:) = PrL_vHPC{si,bl,4}(2,1:3254);
        
        plot_ci(freqN(1:3254),input , 'PatchColor', LiClr{bl}, 'PatchAlpha', 0.1, ...
            'MainLineWidth', 0.5, 'MainLineStyle', '-', 'MainLineColor', LiClr{bl}, ...
            'LineWidth', 0.03, 'LineStyle','--', 'LineColor', 'k');

    end
    ylabel('Relative coherence to baseline')
    xlabel('Frequency (Hz)')
    title('PrL-vHPC')
    
    hold off
    legend('Hab','Acq-2','Exp-1','Ext-3')
    print(fig, outputfig, '-dpng', '-r300');
    clf;
end

for si = 1:size(stimuli,1)
    outputfig = [figurefolder normtype 'NormBlock' stimuli{si}...
        'IL_BLA_Coherence'];
    fig = gcf;
    hold on
    for bl = 1:size(blocks,2)
        input = NaN(3,3254);
        input(1,:) = IL_BLA{si,bl,2}(1,1:3254);
        input(2,:) = IL_BLA{si,bl,4}(1,1:3254);
        input(3,:) = IL_BLA{si,bl,4}(2,1:3254);
        
        plot_ci(freqN(1:3254),input , 'PatchColor', LiClr{bl}, 'PatchAlpha', 0.1, ...
            'MainLineWidth', 0.5, 'MainLineStyle', '-', 'MainLineColor', LiClr{bl}, ...
            'LineWidth', 0.03, 'LineStyle','--', 'LineColor', 'k');

    end
    ylabel('Relative coherence to baseline')
    xlabel('Frequency (Hz)')
    title('IL-BLA')
    
    hold off
    legend('Hab','Acq-2','Exp-1','Ext-3')
    print(fig, outputfig, '-dpng', '-r300');
    clf;
end

for si = 1:size(stimuli,1)
    outputfig = [figurefolder normtype 'NormBlock' stimuli{si}...
        'IL_vHPC_Coherence'];
    fig = gcf;
    hold on
    for bl = 1:size(blocks,2)
                input = NaN(3,3254);
        input(1,:) = IL_vHPC{si,bl,2}(1,1:3254);
        input(2,:) = IL_vHPC{si,bl,4}(1,1:3254);
        input(3,:) = IL_vHPC{si,bl,4}(2,1:3254);
        
        plot_ci(freqN(1:3254),input , 'PatchColor', LiClr{bl}, 'PatchAlpha', 0.1, ...
            'MainLineWidth', 0.5, 'MainLineStyle', '-', 'MainLineColor', LiClr{bl}, ...
            'LineWidth', 0.03, 'LineStyle','--', 'LineColor', 'k');

    end
    ylabel('Relative coherence to baseline')
    xlabel('Frequency (Hz)')
    title('IL-vHPC')
    
    hold off
    legend('Hab','Acq-2','Exp-1','Ext-3')
    print(fig, outputfig, '-dpng', '-r300');
    clf;
end

for si = 1:size(stimuli,1)
    outputfig = [figurefolder normtype 'NormBlock' stimuli{si}...
        'BLA_vHPC_Coherence'];
    fig = gcf;
    hold on
    for bl = 1:size(blocks,2)
        input = NaN(3,3254);
        input(1,:) = BLA_vHPC{si,bl,2}(1,1:3254);
        input(2,:) = BLA_vHPC{si,bl,4}(1,1:3254);
        input(3,:) = BLA_vHPC{si,bl,4}(2,1:3254);
        
        plot_ci(freqN(1:3254),input , 'PatchColor', LiClr{bl}, 'PatchAlpha', 0.1, ...
            'MainLineWidth', 0.5, 'MainLineStyle', '-', 'MainLineColor', LiClr{bl}, ...
            'LineWidth', 0.03, 'LineStyle','--', 'LineColor', 'k');

    end
    ylabel('Relative coherence to baseline')
    xlabel('Frequency (Hz)')
    title('BLA-vHPC')
    
    hold off
    legend('Hab','Acq-2','Exp-1','Ext-3')
    print(fig, outputfig, '-dpng', '-r300');
    clf;
end



% 
% 
% 
% si = 1;
% bl = 3;
% semilogy(freqN(1:3254),IL_BLA{si,bl,2}(1,1:3254))
% 
% input = NaN(3,3254);
% input(1,:) = IL_BLA{si,bl,3}(1,1:3254);
% input(2,:) = IL_BLA{si,bl,4}(1,1:3254);
% input(3,:) = IL_BLA{si,bl,4}(2,1:3254);
% plot_ci(freqN(1:3254),input , 'PatchColor', 'r', 'PatchAlpha', 0.1, ...
%     'MainLineWidth', 2, 'MainLineStyle', '-', 'MainLineColor', 'b', ...
%     'LineWidth', 1, 'LineStyle','--', 'LineColor', 'k');
% ax = gca;
% 
% ax.ylim = [-10 10];
% ylabel('Relative coherence to baseline')
% xlabel('Frequency (Hz)')
% title('IL-BLA')
% 
% a = [IL_BLA{si,bl,3}(1,1:3254); IL_BLA{si,bl,4}(1,1:3254); IL_BLA{si,bl,4}(2,1:3254)];
% size([IL_BLA{si,bl,3}(1,1:3254); IL_BLA{si,bl,4}(1,1:3254); IL_BLA{si,bl,4}(2,1:3254)])
% 
% plot(IL_BLA{si,bl,2}(1,500:3254));

% % Get some random data
% x       = linspace(0.3, pi-0.3, 10);
% Data    = sin(x) + randn(1, 10)/10;
% Data_sd = 0.1+randn(1,10)/30;
%
% % prepare it for the fill function
% x_ax    = 1:10;
% X_plot  = [x_ax, fliplr(x_ax)];
% Y_plot  = [Data-1.96.*Data_sd, fliplr(Data+1.96.*Data_sd)];
%
% % plot a line + confidence bands
% hold on
% plot(x_ax, Data, 'blue', 'LineWidth', 1.2)
% fill(X_plot, Y_plot , 1,....
%     'facecolor','blue', ...
%     'edgecolor','none', ...
%     'facealpha', 0.3);
% hold off
%
%
% x = 0:0.1:10;
% y = exp(x);
%
% figure
% semilogy(x,y)