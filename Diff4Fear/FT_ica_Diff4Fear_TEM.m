%% perform ICA decomposition of diff4fear data & assess results

info_diff4fear            % load all background info (filenames, etc)

foi = [];
for pow = 0:6   % linearly spaced frequencies between powers of 2
  foi = unique([foi linspace(2^pow,2^(pow+1),pow+3)]);
end

tapsmofrq = diff(foi);          % smooth each band by distance to next
foi = foi(1:numel(tapsmofrq));  % omit extra frequency
t_ftimwin = 2./tapsmofrq;       % for 3 tapers (K=3), T=2/W
toi = -30:min(t_ftimwin)/2:60;  % time windows overlap by at least 50%

% for smoothed visualization & less memory use
tshift = max(t_ftimwin)/2;  % shift time windows by tshift
newtime = -60:tshift:90;    % average powspctrm within +/- tshift of each

%% full loop

for r = 1:size(ratT,1)
  %% define input & output filenames
  
  % Tone-triggered trials with artifacts removed individually from each
  % channel by replacing with NaNs
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllCleanLFPTrialData_' num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  % initial ICA components
    outputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
      'ICAcomp' num2str(ratT.ratnums(r)) 'Merged.mat'];
    
    %% load precomputed components if available
    if exist(outputfile,'file')
      disp(['loading ICA components from file: ' outputfile])
      load(outputfile)
    else
  %% check for input data
  
  if ~exist(inputfile,'file')
    warning(['Skipping rat #' num2str(ratT.ratnums(r)) ' because ' inputfile ...
      ' was not found. Run FT_AutoArtReject_Diff4Fear_TEM.m first.'])
    continue
  end
  
  %% perform ica decomposition
      
      cfg = [];
      
      cfg.method      = 'fastica';
      cfg.inputfile   = inputfile;
      cfg.outputfile  = outputfile;
      
      [comp] = ft_componentanalysis(cfg);
    end
    
    if all(any(isnan([comp.trial{:}])))
      error('Something went wrong in ft_componentanalysis.')
    end
    
    %% visualize results (spatial topography)
    
    fig1 = figure(1); clf;
    cfg = [];
    cfg.component = 1:numel(comp.label);
    cfg.layout    = [configfolder 'layout_' MWA '_' ratT.side{r} '.mat'];
    cfg.colormap  = jet;
    cfg.zlim      = 'maxabs';
    cfg.comment   = 'no';
    
    ft_topoplotIC(cfg, comp)
    fig1.WindowStyle = 'docked';
    print(fig1,[figurefolder num2str(ratT.ratnums(r)) ...
      'Merged_topoplotIC'],'-dpng','-r300')
    
    %     %% visualize results (over time)
    %
    %     cfg = [];
    %     cfg.viewmode    = 'component';
    %     cfg.continuous  = 'yes';      % instead of commented cell above
    %     cfg.blocksize   = 180;
    %     cfg.layout      = [configfolder 'layout_' MWA '_' ratT.side{r} '.mat'];
    %     cfg.colormap    = jet;        % ignored?
    %     cfg.zlim        = 'maxabs';   % ignored?
    %
    %     %   cfg.event = struct('type',repmat('ToneOnset',47,1), ...
    %     %     'sample',unique(comp.sampleinfo(:,1)-round(comp.time{:}(1)*comp.fsample)), ...
    %     %     'value',unique(comp.trialinfo,'rows','stable'), ...
    %     %     'duration',ones(47,1), 'offset',zeros(47,1));
    %     %   This doesn't work, but someday I should go back and restore the events
    %     %   after merging files.
    %
    %     cfg = ft_databrowser(cfg, comp);
    %
    %     artifact.visual   = cfg.artfctdef.visual.artifact;
    %     keyboard  % dbcont when satisfied
    %
    %% calculate spectrograms
    
    trls = unique(comp.trialinfo,'rows','stable');  % individual tones
    powrange = NaN(size(trls));
    
    for tr = 1:size(trls,1)
      % spectrograms of ICA components
      outputfile = [datafolder 'ICAcomp_Spectrograms_' ...
        num2str(ratT.ratnums(r)) 'Merged_t' num2str(tr) '.mat'];
      
      if exist(outputfile,'file')
        disp(['loading spectrograms from file: ' outputfile])
        load(outputfile)
      else
        cfg             = [];
        
        cfg.method      = 'mtmconvol';
        cfg.output      = 'pow';
        cfg.keeptrials  = 'no';   % groups subtrials back together
        cfg.trials      = ismember(comp.trialinfo,trls(tr,:),'rows');
        
        cfg.foi         = foi;
        cfg.tapsmofrq   = tapsmofrq;
        cfg.t_ftimwin   = t_ftimwin;
        cfg.toi         = '50%'; 	% overlap between smallest time windows
        cfg.pad   = 'nextpow2'; % max # samples, rounded up to next power of 2
        
        cfg.outputfile  = outputfile;
        
        freq = ft_freqanalysis(cfg,comp);
      end
      
      if all(any(any(isnan(freq.powspctrm))))
        error('Something went wrong in ft_freqanalysis.')
      end
      
      [ncomp, nf, nt] = size(freq.powspctrm);
      
      %% average max(t_ftimwin) chunks & convert to dB for easier visualization
      
      newpowspctrm = nan(ncomp, nf, numel(newtime));
      
      for ch = 1:ncomp
        for f = 1:nf
          for t = 1:numel(newtime)
            newpowspctrm(ch, f, t) = 10*log10(mean(freq.powspctrm(...
              ch, f, abs(freq.time - newtime(t)) < tshift), ...
              'omitnan'));
          end
        end
      end
      
      %% put back into freq structure & save color scales
      
      freq.time = newtime;
      freq.powspctrm = newpowspctrm;
      
      if tr > 10 && tr < 18   % during shocks
        powrange(tr,:) = [min(min(newpowspctrm(1,:,[1:29 33:51]))) ...
          max(max(newpowspctrm(1,:,[1:29 33:51])))];
      else
        powrange(tr,:) = [min(min(newpowspctrm(1,:,:))) ...
          max(max(newpowspctrm(1,:,:)))];
      end
      
      %% visualize results (by frequency)
      
      nw = ceil(sqrt(ncomp));  % # of plots wide
      nh = ceil(ncomp/nw);     % # of plots high
      
      cfg                 = [];
      cfg.parameter       = 'powspctrm';
      cfg.colormap        = jet;
      cfg.fontsize        = 8;
      
      fig = figure(tr+1); clf; fig.WindowStyle = 'docked';
      
      for ch = 1:ncomp
        cfg.channel        = ch;
        subplot(nw,nh,ch); cla
        
        ft_singleplotTFR(cfg, freq);
      end
    end
    
    %% normalize scales across trials
    
    powrange = [min(powrange(:,1)) max(powrange(:,2))];
    for tr = 1:size(trls,1)
      fig = figure(tr+1);
      for ch = 1:ncomp
        subplot(nw,nh,ch);
        ax = gca;
        ax.FontSize = cfg.fontsize;
        ax.CLim = powrange;
        %         ax.YScale = 'log';  % changes axis, but not image data
        %         ax.YTick = foi(1:4:end);
        ax.XTick = newtime(1:5:end);
        xlabel('Time (s)')
        ylabel('Frequency (Hz)')
        title(['Rat #' num2str(ratT.ratnums(r)) ', ' ...
          freq.label{ch} ' Power (dB), Trial #' num2str(tr)])
      end
      
      %% save figure
      
      print(fig,[figurefolder num2str(ratT.ratnums(r)) 'Merged_t' ...
        num2str(tr) 'ICA_Spectrograms'],'-dpng','-r300')
    end
    
    %     %% reject components
    %
    %     cfg.method    = 'summary';
    %
    %     % don't know what this does, but it's supposed to be the default and
    %     % seems to avoid an error!
    %     cfg.viewmode  = 'remove';
    %
    %     comp = ft_rejectvisual(cfg,comp);
    %
    %     keyboard
    %     %% reject artifactual components?
    %
    %     %       ft_rejectcomponent
    %
end
