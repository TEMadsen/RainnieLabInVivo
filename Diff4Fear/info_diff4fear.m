%% info needed for analyzing differential fear, 4 region recording data
%#ok<*SNASGU> just provides background info for other analysis protocols

%% experiment metadata

% each row # here corresponds to cell row (1st dim) in other variables
ratnums = [347; 348; 349; 350; ...
  357; 358; 359; 360; ...
  373; 374; 375; 376];

excluded = logical([0; 0; 0; 0; ...   % see notes for why
  0; 1; 0; 0;...
  0; 0; 0; 0]);
side = {'Right'; 'Right'; 'Left'; 'Left';...
  'Left'; 'Right'; 'Left'; 'Right';...
  'Right'; 'Left'; 'Right'; 'Left'};
finalch = false(size(ratnums));

% Event007 for 2 kHz, Event008 for 6 kHz
CSplus = {'Event007'; 'Event008'; 'Event007'; 'Event008'; ...
  'Event008'; 'Event007'; 'Event007'; 'Event008'; ...
  'Event007'; 'Event007'; 'Event008'; 'Event008'};
CSminus = {'Event008'; 'Event007'; 'Event008'; 'Event007'; ...
  'Event007'; 'Event008'; 'Event008'; 'Event007'; ...
  'Event008'; 'Event008'; 'Event007'; 'Event007'};

ratT = table(ratnums, excluded, side, CSplus, CSminus, finalch);
clearvars ratn* excluded side finalch CS*

% column labels for data.trialinfo
% (4th column of cfg.trl = 1st column of data.trialinfo)
infotype = {'OrigTrialNum','StimulusType','PhaseNum','Intervals'};

% each row # here corresponds to the numerical code that will be saved in
% cfg.trl and data.trialinfo to identify which subtrials correspond to
% which interval types (0 means no stimulus)
stimuli = {'CS+'; 'CS-'};   % 2nd column of data.trialinfo
stimstr = {'CSplus'; 'CSminus'};  % alternate strings for variable names, fieldnames, etc.
intervals = {'Before'; 'During'; 'After'};  % when divided into subtrials relative to tone

% each column # here corresponds to cell column (2nd dim) in other variables
% also 3rd column of data.trialinfo
phases = {'Conditioning', 'Context', 'Recall', 'Extinction', 'Baseline'};
% added Baseline last to avoid re-numbering files, etc.
abbr = {'FC', 'Cont', 'Ext1', 'Ext2', 'BL'};  % for filenames

% expected # of trials per phase, for error checking
expected = [18 1 18 18 1];

% each row # here corresponds to the numerical code that will represent
% which region's data is being used for phase or amplitude source data
regions = {'PrL'; 'IL'; 'BLA'; 'vHPC'};

% add table columns for single best channel in each region to use for
% cross-regional measures
ratT = [ratT table(cell(size(ratT,1)),cell(size(ratT,1)), ...
  cell(size(ratT,1)),cell(size(ratT,1)), ...
  'VariableNames',regions)];

% initialize variables
goodchs = cell(size(ratT,1),1);  % cell array of strings can't be contained within one cell of a table
notes = cell(size(ratT,1),numel(phases));
nexfile = cell(size(ratT,1),numel(phases));
bestchs = cell2struct(cell(size(ratT,1),4),regions,2);

% any extra comments about each rat/session
notes{2,1} = '348 received 5 extra CS+ & 6 extra CS- Habituation tones';
% use CS+ 6:14, CS- 7:15 (but this omits clear alerting response to 1st CS-)
notes{6,1} = '358 had broken connector, so we never recorded from him';
notes{6,2} = '358 lost headcap during first shock in FC';
notes{7,4} = '359 received 1 extra of each CS during Extinction';
% use 1st 9 of each

%% final frequency analysis parameters

foi = 1.1.^(0:49);  % 50 frequencies from 1 to 107, increasing by 10%/step
tapsmofrq = foi/10;     % smoothed by +/-10%
for f = find(tapsmofrq < 0.5)
  tapsmofrq(f) = 0.5;  % keeps max window length under 4
end
t_ftimwin = 2./tapsmofrq;       % for 3 tapers (K=3), T=2/W
toi = -30:min(t_ftimwin)/2:60;  % time windows overlap by at least 50%
fsample = 1000;   % Hz
pad = 2^nextpow2(range(toi)*fsample)/fsample;  % 'nextpow2' is too inconsistent

%% alternative parameters for most complete spectrogram (least averaging)
alt.foi = 1:(1/3):300;
alt.tapsmofrq = sqrt(alt.foi)/3;
alt.t_ftimwin = 2./alt.tapsmofrq;       % for 3 tapers (K=3), T=2/W
alt.toi = -30:min(alt.t_ftimwin)/2:60;  % time windows overlap by at least 50%
alt.BLtoi = 0:min(alt.t_ftimwin)/2:600; 	% for baseline & context files

%% wavelet parameters for most complete spectrogram (least averaging)
wav.foi = logspace(log10(alt.foi(1)),log10(alt.foi(end)),numel(alt.foi));
wav.width = 17;  % cycles per frequency
wav.gwidth = 11;   % length of wavelet in std of implicit gaussian?
sf = wav.foi ./ wav.width;   st = 1 ./ (2*pi .* sf);
wav.toi = -30:min(st)*wav.gwidth:60;  % time windows don't overlap!
wav.BLtoi = 0:min(st)*wav.gwidth:600;   % for baseline & context files

%% wavelet parameters for faster spectrogram (wav had unnecessary frequency
% resolution & excessive wavelet length) - started at 4pm on 11/5, but
% flawed, replaced by fasterwav below @10pm
foilim = [0.7,180];
fastwav.width = 17;  % cycles per frequency (also, freqs have stdev of 100/width percent & increase by 1 stdev per step)
base = (fastwav.width+1)/fastwav.width;
fastwav.foi = base.^(fix(log(foilim(1))/log(base)):fix(log(foilim(2))/log(base)));
sf = fastwav.foi ./ fastwav.width;   st = 1 ./ (2*pi .* sf);
fastwav.gwidth = 7;   % 2*pi here makes the wavelet length = the period of each frequency * the # of cycles specified in width
fastwav.toi = -30:min(st)*fastwav.gwidth:60;  % time windows don't overlap!
fastwav.BLtoi = 0:min(st)*fastwav.gwidth:600; % for baseline & context files

%% wavelet parameters for faster/better spectrogram
% (wav had unnecessary frequency resolution & excessive wavelet length -
% wav & fastwav had non-overlapping time steps in toi)
fasterwav.width = 11;  % cycles per frequency?
% freqs have stdev of 100/width percent & increase by 1 stdev per step)
base = (fasterwav.width+1)/fasterwav.width;
fasterwav.foi = base.^(fix(log(foi(1))/log(base)):fix(log(foi(end))/log(base)));
% sf = fasterwav.foi ./ fasterwav.width;   st = 1 ./ (2*pi .* sf);
fasterwav.gwidth = 11;   % 2*pi here makes the wavelet length = the period of each frequency * the # of cycles specified in width
wavlen = fasterwav.gwidth ./ (2*pi .* fasterwav.foi ./ fasterwav.width);
fasterwav.toi = -30:min(wavlen)/2:60;  % time windows overlap by at least 50%
fasterwav.BLtoi = 0:min(wavlen)/2:600; % for baseline & context files

%% optimal (I hope!) wavelet parameters
% (wavelet length is apparently twice as long as I thought, and total numel
% actually decreases w/ increasing freq resolution using these formulas!)
% minres = 2;   % using res = 1 makes width = 1, so each f is smoothed all the way down to 0
% maxres = 21; 	% highest # of values per power of 2 that keeps maxwavlen < 29.5 with gwidth = 3 (min)
% clearvars totnumel maxwavlen sm60 aspectr
res = 10;     % 9 is lowest integer (to include rounded powers of 2) value w/ sm60 < +/-5 Hz
% for res = minres:maxres  % values per power of 2
optwav.foi = 2.^(0:(1/res):7);  % 1-128 Hz
optwav.width = floor(1/((optwav.foi(2)/optwav.foi(1))-1));  % floor is just to be able to report an integer, and for freqs to smooth at least as far as next step
%   sf = optwav.foi ./ optwav.width;   st = 1 ./ (2*pi .* sf);
%   for gw = 1:100
%     optwav.gwidth = (gw/10)+2.9;   % pi here makes the wavelet length = 1/foi * width # of cycles (higher values increase temporal smoothing w/o decreasing frequency res)
optwav.gwidth = 7; % 7.4 is highest value (minimizing numel) w/ maxwavlen < 29.5 @ res = 9 (6.6 @ res = 10, up to 7.1 with floor(width))
wavlen = (optwav.gwidth .* optwav.width) ./ (pi .* optwav.foi); 	% original calculation appeared to be +/- from toi
optwav.toi = -30:min(wavlen)/2:60;  % time windows overlap by at least 50%
optwav.BLtoi = 0:min(wavlen)/2:600; % for baseline & context files
%     totnumel(gw) = numel(optwav.foi)*numel(optwav.toi);  % total # of elements in calculated spectrogram
%     maxwavlen(gw) = max(wavlen);   % duration of noise-free signal required to show lowest frequency
%     aspectr(gw) = numel(optwav.toi)/numel(optwav.foi);
%   end
%   sm60(res) = 60/optwav.width;  % smoothing bandwidth @ 60 Hz, preferrably < 5 Hz (width >= 12)
% end
% totnumel(maxwavlen > 29.5) = NaN;
% % totnumel(sm60 > 5,:) = NaN;
% gwidth = ((1:100)./10)+2.9;
% clf; yyaxis right; plot(gwidth,totnumel);  hold all;
% yyaxis left; plot(gwidth,maxwavlen); plot(gwidth,aspectr);  % plot(sm60);
% legend('maxwavlen','aspectr','totnumel'); xlabel('gwidth');
% clf; imagesc(1:res,1:gw,totnumel');

%% timeless params for calculation of power & coherence only during tones
% wavelet seems to only have time-frequency representations
timeless.method   = 'mtmfft';
timeless.output   = 'powandcsd';
timeless.foilim   = [0.7, 300];   % full frequency range of preamp filters
timeless.toilim   = [0, 29.5];
timeless.pad      = (2^nextpow2(range(timeless.toilim)*fsample))/fsample;
timeless.tapsmofrq = max(0.25, 2./range(timeless.toilim));  % don't care about bands narrower than 2*this
timeless.minlength = 8;   % in each artifact-free subtrial, ndatsample must be >= 2/W

%% for smoothed visualization & less memory use (may be unnecessary w/ optwav)

tshift = 0.25;          % shift time windows by tshift
smtoi = -30:tshift:60;  % average powspctrm within +/- tshift of each

%% temp storage locations

if ~isdir(tserver)
  disp('You do not appear to be connected to the S drive.')
  warning('Inputs may not be accessible, and outputs will be stored locally.')
  
  datafolder = [fileparts(userpath) filesep 'TempData' filesep];
  
  if ~exist(datafolder,'dir')
    mkdir(datafolder)
  end
  
  figurefolder  = [fileparts(userpath) filesep 'TempFigures' filesep];
else
  datafolder    = [tserver 'Analyses' filesep 'TempData' filesep ...
    'Diff4Fear' filesep];
  
  if ~exist(datafolder,'dir')
    mkdir(datafolder)
  end
  
  for r = 1:size(ratT,1)
    % put intermediate data files in a folder named by rat #
    ratfolder  = [datafolder num2str(ratT.ratnums(r)) filesep];
    if ~exist(ratfolder,'dir')
      mkdir(ratfolder)
    end
  end
  
  configfolder  = [tserver 'Analyses' filesep 'MWAconfigs' filesep];
  
  % put new figures in a folder named by date, in the format 'yyyy-mm-dd'
  figurefolder  = [tserver 'Figures' filesep 'TempFigures' filesep ...
    datestr(now,29) filesep];
end

if ~exist(figurefolder,'dir')
  mkdir(figurefolder)
end

%% define MWA configuration(s)

MWA = 'Diff4Fear';        % name for this microwire array configuration

lftchs = {'AD24','AD31'; ...  % farthest left array configuration, where
  'AD22','AD29'; ...  % increasing rows indicate electrodes
  'AD20','AD27'; ...  % ordered A -> P, and increasing columns
  'AD18','AD25'; ...  % indicate those ordered L -> R
  'AD23','AD32'; ...
  'AD21','AD30'; ...  % mPFC if implanted on right,
  'AD19','AD28'; ...  % BLA/vHPC if implanted on left
  'AD17','AD26'};

rtchs  = {'AD15','AD08'; ...  % farthest right array configuration, where
  'AD13','AD06'; ...  % increasing rows indicate electrodes
  'AD11','AD04'; ...  % ordered A -> P, and increasing columns
  'AD09','AD02'; ...  % indicate those ordered L -> R
  'AD16','AD07'; ...
  'AD14','AD05'; ...  % BLA/vHPC if implanted on right,
  'AD12','AD03'; ...  % mPFC if implanted on left
  'AD10','AD01'};

allchs = sort([lftchs(:); rtchs(:)]);

if ~isdir(tserver)
  warning('MWA config files are not currently accessible.  They can be recreated locally if necessary.')
else
  if exist([configfolder 'elec_' MWA '_Right.mat'],'file') && ...
      exist([configfolder 'layout_' MWA '_Left.mat'],'file')
    disp('using existing elec & layout files')
  else
    cfg = [];
    
    % array & target info
    cfg.atlascoord = [3.2, 0.40, -5.50; ... 	% AP/ML/DV relative to bregma
      -3.6, 4.80, -9.00];      % dim 1/2/3 for below
    cfg.angle     = [-6; 6];  % + angle means approach from medial, in degrees
    cfg.angledim  = [2; 2];   % both probes angled along ML dim
    cfg.elecspac  = [0.25, 0.375; ... 	% in AP dim, then ML dim, for each probe
      0.25, 0.375];      % in mm
    
    cfg.shorter = {[1.5, 1.5; ...  % mPFC array
      1.0, 1.0; ...
      0.5, 0.5; ...  % how much shorter (in mm) each wire is than
      0.0, 0.0; ...  % the longest used for measuring bregma
      1.5, 1.5; ...
      1.0, 1.0; ...  % in same arrangement as labels
      0.5, 0.5; ...
      0.0, 0.0]; ...
      [0.0, 0.0; ...  % BLA array
      0.5, 0.5; ...
      1.0, 1.0; ...  % how much shorter (in mm) each wire is than
      0.0, 0.0; ...  % the longest used for measuring bregma
      0.5, 0.5; ...
      1.0, 1.0; ...  % in same arrangement as labels
      1.5, 1.5; ...
      2.0, 2.0]};
    
    cfg.bregmaPt = [4, 1.5; ... 	% in row #, then column # for each array
      4, 1.5];      % 1.5 means centered btwn columns 1 & 2
    
    %% for rats implanted on right side of brain
    
    cfg.label = {lftchs; rtchs}; 	% mPFC array on left side of connector & vv
    cfg.side = {'Right';'Right'}; % which side of the brain each probe was on
    
    % calculate sensor positions & define layout
    MWAlayout(cfg,[MWA '_Right'],configfolder);
    
    %% for rats implanted on left side of brain
    
    cfg.label = {rtchs; lftchs}; 	% mPFC array on right side of connector & vv
    cfg.side = {'Left';'Left'};   % which side of the brain each probe was on
    
    % calculate sensor positions & define layout
    MWAlayout(cfg,[MWA '_Left'],configfolder);
  end
end

%% channels to use for each rat, defaults to 'AD*' if empty
goodchs{1} = {'AD*', '-AD02', '-AD03', '-AD07', ...
  '-AD20', '-AD22', '-AD25', '-AD27'};
bestchs(1).IL = 'AD03';   % terrible connection on conditioning day, so
bestchs(1).PrL = 'AD16';  % channels to invalidate were selected based on
bestchs(1).BLA = 'AD24';  % extinction recordings, and z-score thresholds
bestchs(1).vHPC = 'AD19';  % for artifacts are lower than usual
ratT.finalch(1) = false;

goodchs{2} = {'AD*', '-AD26', '-AD28', '-AD30', '-AD31', '-AD32'};
bestchs(2).IL = 'AD16';
bestchs(2).PrL = 'AD03';
bestchs(2).BLA = 'AD24';
bestchs(2).vHPC = 'AD17';
ratT.finalch(2) = false;

goodchs{3} = {'AD*', '-AD04', '-AD06', '-AD16', ...
  '-AD21', '-AD30'};
bestchs(3).IL = 'AD26';
bestchs(3).PrL = 'AD20';
bestchs(3).BLA = 'AD06';
bestchs(3).vHPC = 'AD01';
ratT.finalch(3) = false;

goodchs{4} = {'AD*', };
bestchs(4).IL = 'AD32';     % all but 5 channels (10-15?) are pretty equally
bestchs(4).PrL = 'AD26';    % terrible
bestchs(4).BLA = 'AD08';
bestchs(4).vHPC = 'AD01';
ratT.finalch(4) = false;

goodchs{5} = {'AD*', '-AD15', ...
  '-AD25', '-AD31'};
bestchs(5).IL = 'AD12';
bestchs(5).PrL = 'AD13';
bestchs(5).BLA = 'AD29';
bestchs(5).vHPC = 'AD17';
ratT.finalch(5) = false;

goodchs{7} = {'AD*', ...
  '-AD02', '-AD04', '-AD06', '-AD09', '-AD11', '-AD13', '-AD14', '-AD15', ...
  '-AD21', '-AD25', '-AD27', '-AD29', '-AD31'};
bestchs(7).IL = 'AD02';     % more artifacts at <3 than 3-5
bestchs(7).PrL = 'AD06';    % need to compare the 2 values on this rat - DONE
bestchs(7).BLA = 'AD18';    % threshold @ 3 does a terrible job, so keep 5
bestchs(7).vHPC = 'AD26';
ratT.finalch(7) = false;

goodchs{8} = {'AD*', '-AD09', '-AD11', '-AD13', '-AD15', ...
  '-AD18', '-AD27', '-AD29', '-AD31'};
bestchs(8).IL = 'AD19';
bestchs(8).PrL = 'AD23';
bestchs(8).BLA = 'AD06';
bestchs(8).vHPC = 'AD03';
ratT.finalch(8) = false;

goodchs{9} = {'AD*', '-AD15', ...
  '-AD17', '-AD18', '-AD25', '-AD27', '-AD29', '-AD32'};
bestchs(9).IL = 'AD19';
bestchs(9).PrL = 'AD23';
bestchs(9).BLA = 'AD08';
bestchs(9).vHPC = 'AD03';
ratT.finalch(9) = false;

goodchs{10} = {'AD*', '-AD11', '-AD15', ...
  '-AD18', '-AD20', '-AD22', '-AD29'};
bestchs(10).IL = 'AD12';
bestchs(10).PrL = 'AD16';
bestchs(10).BLA = 'AD24';   % sorry, you're right :)
bestchs(10).vHPC = 'AD19';
ratT.finalch(10) = false;

goodchs{11} = {'AD*', '-AD01', '-AD02', '-AD03', ...
  '-AD17', '-AD18', '-AD22', '-AD30', '-AD32'};
bestchs(11).IL = 'AD26';  % nvm.  I'm clearly sleep deprived.  ;-P
bestchs(11).PrL = 'AD32';
bestchs(11).BLA = 'AD13';
bestchs(11).vHPC = 'AD03';
ratT.finalch(11) = false;

goodchs{12} = {'AD*', '-AD01', '-AD02', '-AD04', '-AD06', '-AD08', ...
  '-AD10', '-AD12', '-AD14', '-AD15', '-AD16', ... % 7, 11, & 25 also pretty bad
  '-AD17', '-AD18', '-AD20', '-AD22', '-AD24', ...  % Changed PrL to AD13
  '-AD26', '-AD27', '-AD28', '-AD29', '-AD30', '-AD31', '-AD32'};
bestchs(12).IL = 'AD03';    % determine these based on spectrograms
bestchs(12).PrL = 'AD13';   % plotted in a topographical layout
bestchs(12).BLA = 'AD31';
bestchs(12).vHPC = 'AD19';
ratT.finalch(12) = false;

%% filenames

% Baseline @VI-60 recording before FC day
nexfile{1,5} = [tserver 'PlexonData Backup' filesep '347' filesep ...
  '347_151102_0001-aligned.nex'];
nexfile{2,5} = [tserver 'PlexonData Backup' filesep '348' filesep ...
  '348_151030_0000-aligned.nex'];
nexfile{3,5} = [tserver 'PlexonData Backup' filesep '349' filesep ...
  '349_151102_0003-aligned.nex'];
nexfile{4,5} = [tserver 'PlexonData Backup' filesep '350' filesep ...
  '350_151102_0002-aligned.nex'];
nexfile{5,5} = [tserver 'PlexonData Backup' filesep '357' filesep ...
  '357_160314_0002-aligned.nex'];
nexfile{7,5} = [tserver 'PlexonData Backup' filesep '359' filesep ...
  '359_160314_0000-aligned.nex'];
nexfile{8,5} = [tserver 'PlexonData Backup' filesep '360' filesep ...
  '360_160314_0001-aligned.nex'];
nexfile{9,5} = [tserver 'PlexonData Backup' filesep '373' filesep ...
  '373_160808_0000-aligned.nex'];
nexfile{10,5} = [tserver 'PlexonData Backup' filesep '374' filesep ...
  '374_160808_0001-aligned.nex'];
nexfile{11,5} = {[tserver 'PlexonData Backup' filesep '375' filesep ...
  '375_160808_0002-aligned.nex'];[tserver 'PlexonData Backup' filesep '375' filesep ...
  '375_160808_0003-aligned.nex']};
nexfile{12,5} = [tserver 'PlexonData Backup' filesep '376' filesep ...
  '376_160808_0004-aligned.nex'];


% Habituation & Acquisition (fear conditioning)
nexfile{1,1} = {[tserver 'PlexonData Backup' filesep '347' filesep ...
  '347_151103_0001-aligned.nex']};
nexfile{2,1} = {[tserver 'PlexonData Backup' filesep '348' filesep ...
  '348_151031_0001-aligned.nex']};
nexfile{3,1} = {[tserver 'PlexonData Backup' filesep '349' filesep ...
  '349_151103_0002-aligned.nex']};
nexfile{4,1} = {[tserver 'PlexonData Backup' filesep '350' filesep ...
  '350_151103_0010-aligned.nex']};
nexfile{5,1} = {[tserver 'PlexonData Backup' filesep '357' filesep ...
  '357_160315_0002-aligned_wTS.nex']};
% nexfile{6,1} = {[tserver 'PlexonData Backup' filesep '358' filesep ...
%   '358_160316_0003-aligned.nex']};  % only original file, not aligned
nexfile{7,1} = {[tserver 'PlexonData Backup' filesep '359' filesep ...
  '359_160315_0000-aligned_wTS.nex']};
nexfile{8,1} = {[tserver 'PlexonData Backup' filesep '360' filesep ...
  '360_160315_0001-aligned_wTS.nex']};
nexfile{9,1} = {[tserver 'PlexonData Backup' filesep '373' filesep ...
  '373_160809_0000-aligned.nex']};
nexfile{10,1} = {[tserver 'PlexonData Backup' filesep '374' filesep ...
  '374_160809_0001LFPs-aligned.nex']};
nexfile{11,1} = {[tserver 'PlexonData Backup' filesep '375' filesep ...
  '375_160809_0003-aligned.nex']};
nexfile{12,1} = {[tserver 'PlexonData Backup' filesep '376' filesep ...
  '376_160809_0004LFPs-aligned.nex']};

% Context Extinction
nexfile{1,2} = {[tserver 'PlexonData Backup' filesep '347' filesep ...
  '347_151104_0002-aligned.nex']};
nexfile{2,2} = {[tserver 'PlexonData Backup' filesep '348' filesep ...
  '348_151101_0000-aligned.nex']};
nexfile{3,2} = {[tserver 'PlexonData Backup' filesep '349' filesep ...
  '349_151104_0003-aligned.nex']};
nexfile{4,2} = {[tserver 'PlexonData Backup' filesep '350' filesep ...
  '350_151104_0004-aligned.nex']};
nexfile{5,2} = {[tserver 'PlexonData Backup' filesep '357' filesep ...
  '357_160316_0002-aligned.nex']};
nexfile{7,2} = {[tserver 'PlexonData Backup' filesep '359' filesep ...
  '359_160316_0000-aligned.nex']};
nexfile{8,2} = {[tserver 'PlexonData Backup' filesep '360' filesep ...
  '360_160316_0001-aligned.nex']};
nexfile{9,2} = {[tserver 'PlexonData Backup' filesep '373' filesep ...
  '373_160810_0000-aligned.nex']};
nexfile{10,2} = {[tserver 'PlexonData Backup' filesep '374' filesep ...
  '374_160810_0005-aligned.nex']};
nexfile{11,2} = {[tserver 'PlexonData Backup' filesep '375' filesep ...
  '375_160810_0006-aligned.nex']};
nexfile{12,2} = {[tserver 'PlexonData Backup' filesep '376' filesep ...
  '376_160810_0007LFPs-aligned.nex']};

% Recall (extinction 1)
nexfile{1,3} = {[tserver 'PlexonData Backup' filesep '347' filesep ...
  '347_151105_0002-aligned.nex']};
nexfile{2,3} = {[tserver 'PlexonData Backup' filesep '348' filesep ...
  '348_151102_0000-aligned.nex']};
nexfile{3,3} = {[tserver 'PlexonData Backup' filesep '349' filesep ...
  '349_151105_0003-aligned.nex']};
nexfile{4,3} = {[tserver 'PlexonData Backup' filesep '350' filesep ...
  '350_151105_0000-aligned.nex']; [tserver 'PlexonData Backup' ...
  filesep '350' filesep '350_151105_0001-aligned.nex']};
nexfile{5,3} = {[tserver 'PlexonData Backup' filesep '357' filesep ...
  '357_160317_0002-aligned_wTS.nex']};
nexfile{7,3} = {[tserver 'PlexonData Backup' filesep '359' filesep ...
  '359_160317_0000-aligned_wTS.nex']};
nexfile{8,3} = {[tserver 'PlexonData Backup' filesep '360' filesep ...
  '360_160317_0001-aligned_wTS.nex']};
nexfile{9,3} = {[tserver 'PlexonData Backup' filesep '373' filesep ...
  '373_160811_0000-aligned.nex']};
nexfile{10,3} = {[tserver 'PlexonData Backup' filesep '374' filesep ...
  '374_160811_0002LFPs-aligned.nex']};
nexfile{11,3} = {[tserver 'PlexonData Backup' filesep '375' filesep ...
  '375_160811_0003-aligned.nex']};
nexfile{12,3} = {[tserver 'PlexonData Backup' filesep '376' filesep ...
  '376_160811_0004LFPs-aligned.nex']};

% Extinction (extinction 2)
nexfile{1,4} = {[tserver 'PlexonData Backup' filesep '347' filesep ...
  '347_151106_0001-aligned.nex']};
nexfile{2,4} = {[tserver 'PlexonData Backup' filesep '348' filesep ...
  '348_151103_0000-aligned.nex']};
nexfile{3,4} = {[tserver 'PlexonData Backup' filesep '349' filesep ...
  '349_151106_0002-aligned.nex']};
nexfile{4,4} = {[tserver 'PlexonData Backup' filesep '350' filesep ...
  '350_151106_0000-aligned.nex']};
nexfile{5,4} = {[tserver 'PlexonData Backup' filesep '357' filesep ...
  '357_160318_0003-aligned_wTS.nex']};
nexfile{7,4} = {[tserver 'PlexonData Backup' filesep '359' filesep ...
  '359_160318_0001-aligned_wTS.nex']};
nexfile{8,4} = {[tserver 'PlexonData Backup' filesep '360' filesep ...
  '360_160318_0002-aligned_wTS.nex']};
nexfile{9,4} = {[tserver 'PlexonData Backup' filesep '373' filesep ...
  '373_160812_0000-aligned.nex']};
nexfile{10,4} = {[tserver 'PlexonData Backup' filesep '374' filesep ...
  '374_160812_0001-aligned.nex']};
nexfile{11,4} = {[tserver 'PlexonData Backup' filesep '375' filesep ...
  '375_160812_0004LFPs-aligned.nex']};
nexfile{12,4} = {[tserver 'PlexonData Backup' filesep '376' filesep ...
  '376_160812_0003LFPs-aligned.nex']};

%% complete workflow:
% 0)  align original .plx files, save as -aligned.nex
% 1)  FT_preproc_Diff4Fear_TEM - Load all data from -aligned.nex files into
%     FieldTrip format & merge into 1 dataset per rat, using new
%     trialfun_TEM.m and supporting a cell array of strings for multiple
%     filenames within one phase (to avoid merged NEX files, which are hard
%     to read correctly).
% 2)  FT_AutoArtReject_Diff4Fear_TEM - consider adding interpolation to
%     "repair" data lost to artifacts
% 3)  FT_ica_Diff4Fear_TEM !!!!!skipped - come back here & add top
%     components to channel list for subsequent analyses!!!!!
% 4)  FT_TFAbyChTrl_Diff4Fear_TEM.m - calculates spectrograms for each
%     channel & trial separately
% 5)  FT_NormTFA_Diff4Fear_TEM.m - normalizes & plots spectrograms
% 6)  FT_GrpDescNormTFA_Diff4Fear_TEM.m - calculates group statistics on
%     normalized spectrograms
% 7)  FT_timelessfreq_Diff4Fear_TEM.m - calculates multitaper spectrum &
%     coherence for specific chunks of time (e.g., during tones)
% 8)  statistical testing between phases or time points
% 9)  compare to behavior
% 10) cross-frequency
% 11) granger causality?
% 12) spikes??