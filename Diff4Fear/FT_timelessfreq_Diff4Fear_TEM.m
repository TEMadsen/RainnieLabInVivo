%% Calculate raw power spectra and coherence during tones for each channel

CleanSlate  % provides a clean slate to start working with new data/scripts

info_diff4fear        % load all background info & final parameters

zthresh = 15;         % analyze data cleaned using this artifact threshold
spectype = 'timeless';

errlog = cell(size(ratT,1),1); % only way that works with parfor

%% loop through all rats

parfor r = 1:size(ratT,1)
  %% define & check for 1st outputfile
  
  outputfile1 = [datafolder num2str(ratT.ratnums(r)) filesep ...
    '4ch29sData_' num2str(ratT.ratnums(r)) '.mat'];
  
  if exist(outputfile1,'file')
    disp(['loading ' outputfile1])
    S = load(outputfile1,'data');
    data4ch = S.data;
    S = [];
  else
    %% define inputfile
  
  inputfile = [datafolder num2str(ratT.ratnums(r)) filesep ...
    'AllClean_z' num2str(zthresh) '_LFPTrialData_' ...
    num2str(ratT.ratnums(r)) 'Merged.mat'];
  
  %% stop here to avoid continue
  
  if ~exist(inputfile,'file')
    errlog{r}{1} = ['clean merged data file not found: ' inputfile];
    warning(errlog{r}{1})
    continue
  end
  
  %% load inputfile
  
  disp(['Loading clean merged data for rat #' num2str(ratT.ratnums(r))])
  S = load(inputfile,'data');
  data = S.data;
  S = [];
  
  %% isolate 4 preferred channels
  
  cfg         = [];
  
  cfg.channel = {bestchs.IL{r}; bestchs.PrL{r}; bestchs.BLA{r}; bestchs.vHPC{r}};
  
  data4ch = ft_selectdata(cfg,data);
  
  %% mark NaNs as artifacts
  
  nanarts = [];
  
  for tr = 1:numel(data4ch.trial)
    % identify whether there are any NaNs at each timepoint
    tnan = any(isnan(data4ch.trial{tr}),1);
    
    if any(tnan)
      % determine the file sample #s for this trial
      trsamp = data4ch.sampleinfo(tr,1):data4ch.sampleinfo(tr,2);
      
      while any(tnan)
        % start from the end so sample #s don't shift
        endnan = find(tnan,1,'last');
        tnan = tnan(1:endnan);  % remove any non-NaNs after this
        
        % find last non-NaN before the NaNs
        beforenan = find(~tnan,1,'last');
        
        if isempty(beforenan)   % if no more non-NaNs
          begnan = 1;
          tnan = false;   % while loop ends
        else  % still more to remove - while loop continues
          begnan = beforenan + 1;
          tnan = tnan(1:beforenan);  % remove the identified NaNs
        end
        
        % identify file sample #s that correspond to beginning
        % and end of this chunk of NaNs and append to
        % nanarts
        nanarts = [nanarts; ...
          trsamp(begnan) trsamp(endnan)];
      end   % while any(tnan)
    end   % if any(tnan)
  end   % for tr = 1:numel(data.trial)
  
  %% remove NaNs from data
  
  cfg                         = [];
  
  cfg.artfctdef.nan.artifact  = nanarts;
  cfg.artfctdef.reject        = 'partial';
  
  data4ch = ft_rejectartifact(cfg,data4ch);
  
  %% redefine baseline trials (break into arbitrary 29.5s chunks)
  
  cfg           = [];
  
  cfg.trials    = find(data4ch.trialinfo(:,3) == 5);
  cfg.length    = range(timeless.toilim);
  cfg.overlap   = 0;
  
  BLdata = ft_redefinetrial(cfg, data4ch);
  
  %% change start times of each trial to 0
  
  cfg                 = [];
  
  for tr = 1:numel(BLdata.trial)
    cfg.offset(tr,1) 	= -BLdata.time{tr}(1) .* BLdata.fsample;
  end
  
  BLdata = ft_redefinetrial(cfg, BLdata);
  
  %% limit tone trials to toilim times
  
  cfg           = [];
  
  cfg.trials    = find(data4ch.trialinfo(:,1) ~= 0);
  cfg.toilim    = timeless.toilim;
  cfg.minlength = timeless.minlength;
  
  tonedata = ft_redefinetrial(cfg, data4ch);
  
  %% remerge baseline & tone data
  
  cfg             = [];
  cfg.outputfile  = outputfile1;
  
  data4ch = ft_appenddata(cfg,tonedata,BLdata);
  
  end
  
  %% loop through all trials
  
  trlinfo = unique(data4ch.trialinfo,'rows','stable');
  for tr = 1:size(trlinfo,1)
    %       try
    %% define & check for outputfile
    
    tn = trlinfo(tr,1);   % tone number within phase & stimulus type
    s = trlinfo(tr,2);    % stimulus type
    p = trlinfo(tr,3);    % phase
    
    cfg             = [];
    
    %% stop here to avoid continue
    
    if p == 2  % context extinction
      errlog{r}{tr} = ['skipping context trial with trlinfo: ' ...
        num2str(trlinfo(tr,:))];
      warning(errlog{r}{tr})
      continue
    elseif p == 5   % baseline VI-60
      outputfile2 = [datafolder num2str(ratT.ratnums(r)) filesep ...
        phases{p} '_' num2str(ratT.ratnums(r)) '_4ch_timeless_powandcsd.mat'];
    else
      outputfile2 = [datafolder num2str(ratT.ratnums(r)) filesep ...
        stimuli{s} num2str(tn) 'of' phases{p} '_' num2str(ratT.ratnums(r)) ...
        '_4ch_timeless_powandcsd.mat'];
    end
    
    %% stop here to avoid continue
    
    if exist(outputfile2,'file')
      disp(['4ch freq analysis already conducted for trial #' ...
        num2str(tr) ', Rat #' num2str(ratT.ratnums(r)) '. Skipping.'])
      continue
    end
    
    %% calculate spectrogram
    
    cfg.method      = 'mtmfft';
    cfg.output      = 'powandcsd';
    cfg.keeptrials  = 'no';
    cfg.foilim      = timeless.foilim;
    cfg.pad         = timeless.pad;
    cfg.tapsmofrq   = timeless.tapsmofrq;
    cfg.outputfile  = outputfile2;
    cfg.trials = find(ismember(data4ch.trialinfo,trlinfo(tr,:),'rows'));
    
    freq = ft_freqanalysis(cfg,data4ch);
    
    %% test coherence plot (off the top of my head...double-check the formula!)
    
    figure;
    plot(freq.freq,sqrt(freq.crsspctrm(1,:).^2 ./ ...
      (freq.powspctrm(1,:) .* freq.powspctrm(2,:))))
    
    %       catch ME
    %         warning(ME.message);
    %         errlog{r,ch}{tr} = ME;
    %       end
  end
end
