function [connected] = wait4server_TEM(dir, maxt)
%WAIT4SERVER attempts to verify connection to a specified directory path (dir).
%   Displays waitbar with time elapsed since connection was lost / maximum time
%   (maxt).  Cancel button allows user to return status = false without further
%   attempts to verify connection.  If dir is a complete filename, this checks
%   for the last foldername.
%
% written 4/11/2017 by Teresa E. Madsen, Ph.D.

if nargin < 1 || isempty(dir)
  dir = tserver;  % my folder on the S drive
end

connected = exist(dir,'file');  % works for files or directories

if ~connected
  if contains(dir, '.')  % in case this is a specific filename
    k = strfind(dir, filesep);
    dir = dir(1:k(end));  % truncate to last filesep
    connected = exist(dir,'dir');
    if connected
      return  % directory exists, but file may not
    end
  end
  
  if nargin < 2 || isempty(maxt)
    maxt = 60;  % default 60 minutes before timeout
  end

  h = waitbar(0, ...
    ['Waiting up to ' num2str(maxt) ' minutes for server connection...'], ...
    'CreateCancelBtn', 'setappdata(gcbf,''canceling'',1)');
  setappdata(h,'canceling',0)
  
  for a = 1:(maxt*10)   % attempt #, checking 10x/min
    % Check for Cancel button press
    if getappdata(h,'canceling') || connected
      break   % terminate execution of for loop (no more attempts)
    end
    
    waitbar(a/(maxt*10), h);
    pause(6);  % wait 6s before next attempt (length of delay here affects responsiveness to cancel button)
    connected = exist(dir,'dir');
  end
  
  delete(h);  % close(h) would just setappdata to 'canceling'
end

end
