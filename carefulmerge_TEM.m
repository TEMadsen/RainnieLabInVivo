function [cmdout, statout] = carefulmerge_TEM(branchname)
% CAREFULMERGE_TEM merges the named branch into the current branch without
% committing the result so individual changes can be reviewed.

[cmdout, statout] = git(['merge ' branchname ...
  ' -v -s recursive -Xpatience --no-commit --no-ff']);
end

