function [freq] = art2nan_freqanalysis(cfg,data)
% ART2NAN_FREQANALYSIS runs ft_freqanalysis and then turns per-channel
%   artifacts back into NaNs in the output.  Must be run one real trial at
%   a time, i.e., the subtrials created to separate artifacts will be
%   merged back together, so they shouldn't have data in the same time
%   bins.
%
% Required cfg fields include:
%   badtimes = output from artifact_nan2zero_TEM
%
% written 3/2/17 by Teresa E. Madsen

%% calculate trial spectrogram
  
  cfg.keeptrials = 'no';  % refers to sub-trials
  
  freq = ft_freqanalysis(cfg,data);
  
  %% where t_ftimwin overlaps with artifact, replace powspctrum values with NaNs
  
  for ch = 1:numel(freq.label)
    badt = [cfg.badtimes{ch}];
    if ~isempty(badt) && any(...
        badt > (min(freq.time) - max(freq.cfg.t_ftimwin)) & ...
        badt < (max(freq.time) + max(freq.cfg.t_ftimwin)))
      ci = find(any(strcmp(freq.label{ch}, freq.labelcmb)));
      for t = 1:numel(freq.time)
        for f = 1:numel(freq.freq)
          mint = freq.time(t) - freq.cfg.t_ftimwin(f);
          maxt = freq.time(t) + freq.cfg.t_ftimwin(f);
          if any(badt > mint & badt < maxt)
            freq.powspctrm(ch,f,t) = NaN;
            freq.crsspctrm(ci,f,t) = NaN;
          end
        end
      end
    end
  end
end

