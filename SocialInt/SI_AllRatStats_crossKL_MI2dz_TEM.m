%% calculates statistics on 2d cross-frequency phase-amplitude modulation
% for each interval type of multiple social interaction tests
% across multiple rats

close all force; clearvars; clc;  % clean slate

info_SI                         % load all background info (filenames, etc)
plt = 'y';                      % plot CFC

outputfile = [datafolder 'crossKL_MI2dz_fullint_Agg.mat'];

%% load or aggregate data

if exist(outputfile,'file')
    disp('Data already aggregated!');
    if plt == 'y'
        disp('Loading previous analysis results from file.');
        load(outputfile);
    end
else
    
    %% preallocate memory for variables with cells for intervals (dim 1) &
    % sources of phase (dim 2) & amplitude (dim 3) data, while allowing
    % averaging & statistics across the dimensions representing rats (dim 3 of
    % matrices in each cell) & test sessions (dim 4 of matrices in each cell)
    
    zscore2dAgg = cell(numel(intervals),2,2);
    pval2dAgg = cell(numel(intervals),2,2);
    h2dAgg = cell(numel(intervals),2,2);
    MI2dAgg = cell(numel(intervals),2,2);
    phasepref2dAgg = cell(numel(intervals),2,2);        % average phase pref for significant trials only
    meanPPrats2dAgg = cell(numel(intervals),2,2);       % average phase pref for all rats
    meanPPsessions2dAgg = cell(numel(intervals),2,2);   % average phase pref for all sessions
    meanPPall2dAgg = cell(numel(intervals),2,2);        % average phase pref for all rats & sessions
    
    %% average across trials for each rat/session/interval/region pair
    
    for r = 1:numel(ratnums)
        for p = 1:numel(phases)
            inputfile = [datafolder 'crossKL_MI2dz_'...
                num2str(ratnums(r)) phases{p} '_fullint.mat'];
            
            if ~exist(inputfile,'file')
                warning('inputfile does not exist.  Filling with NaNs.');
                for int = 1:numel(intervals)
                    for pr = 1:2        % each phase modulating region
                        for ar = 1:2    % each amp modulated region
                            zscore2dAgg{int,pr,ar}(:,:,r,p) = NaN(...
                                [numel(foi_phase) numel(foi_amp)]);
                            pval2dAgg{int,pr,ar}(:,:,r,p) = NaN(...
                                [numel(foi_phase) numel(foi_amp)]);
                            h2dAgg{int,pr,ar}(:,:,r,p) = NaN(...
                                [numel(foi_phase) numel(foi_amp)]);
                            MI2dAgg{int,pr,ar}(:,:,r,p) = NaN(...
                                [numel(foi_phase) numel(foi_amp)]);
                            phasepref2dAgg{int,pr,ar}(:,:,r,p) = NaN(...
                                [numel(foi_phase) numel(foi_amp)]);
                        end
                    end
                end
            else
                load(inputfile);
                
                for int = 1:numel(intervals)
                    if isempty(zscore2d{int})
                        warning(['No ' intervals{int} ...
                            ' intervals found for Rat # ' ...
                            num2str(ratnums(r)) ', ' phases{p} ...
                            '.  Filling with NaNs.']);
                        for pr = 1:2        % each phase modulating region
                            for ar = 1:2    % each amp modulated region
                                zscore2dAgg{int,pr,ar}(:,:,r,p) = NaN(...
                                    [numel(foi_phase) numel(foi_amp)]);
                                pval2dAgg{int,pr,ar}(:,:,r,p) = NaN(...
                                    [numel(foi_phase) numel(foi_amp)]);
                                h2dAgg{int,pr,ar}(:,:,r,p) = zeros(...
                                    [numel(foi_phase) numel(foi_amp)]);
                                MI2dAgg{int,pr,ar}(:,:,r,p) = NaN(...
                                    [numel(foi_phase) numel(foi_amp)]);
                                phasepref2dAgg{int,pr,ar}(:,:,r,p) = NaN(...
                                    [numel(foi_phase) numel(foi_amp)]);
                            end
                        end
                    else
                        for pr = 1:2        % each phase modulating region
                            for ar = 1:2    % each amp modulated region
                                zscore2dAgg{int,pr,ar}(:,:,r,p) = ...
                                    mean(zscore2d{int}{pr,ar},3);
                                pval2dAgg{int,pr,ar}(:,:,r,p) = ...
                                    mean(pval2d{int}{pr,ar},3);
                                h2dAgg{int,pr,ar}(:,:,r,p) = ...
                                    mean(h2d{int}{pr,ar},3);
                                MI2dAgg{int,pr,ar}(:,:,r,p) = ...
                                    mean(MI2d{int}{pr,ar},3);
                                for pf = 1:numel(foi_phase)
                                    for af = 1:numel(foi_amp)
                                        if sum(h2d{int}{pr,ar}(pf,af,:)) > 0
                                            % average the preferred phases only
                                            % from the significant trials
                                            phasepref2dAgg{int,pr,ar}...
                                                (pf,af,r,p) = circ_mean(...
                                                circ_ang2rad(squeeze(phasepref2d...
                                                {int}{pr,ar}(pf,af,logical(...
                                                h2d{int}{pr,ar}(pf,af,:))))));
                                        else
                                            % warning(['No significant trials'...
                                            %     ' found for this frequency'...
                                            %     ' pair (phasepref = NaN)']);
                                            phasepref2dAgg{int,pr,ar}...
                                                (pf,af,r,p) = NaN;
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    
    %% find max mean z-score to scale figures & average phasepref of only
    % the frequency pairs showing significant coupling
    
    cmax = 0;
    
    for int = 1:numel(intervals)
        for pr = 1:2        % each phase modulating region
            for ar = 1:2    % each amp modulated region
                cmax = max(max(max(mean(mean(zscore2dAgg{int,pr,ar},4,...
                    'omitnan'),3,'omitnan'))),cmax);
                for pf = 1:numel(foi_phase)
                    for af = 1:numel(foi_amp)
                        for p = 1:numel(phases)
                            if sum(h2dAgg{int,pr,ar}(pf,af,:,p)) > 0
                                meanPPrats2dAgg{int,pr,ar}(pf,af,p) = ...
                                    circ_mean(squeeze(...
                                    phasepref2dAgg{int,pr,ar}(pf,af,...
                                    squeeze(~isnan(phasepref2dAgg...
                                    {int,pr,ar}(pf,af,:,p))),p)));
                            else
                                meanPPrats2dAgg{int,pr,ar}(pf,af,p) = NaN;
                            end
                        end
                        for r = 1:numel(ratnums)
                            if sum(h2dAgg{int,pr,ar}(pf,af,r,:)) > 0
                                meanPPsessions2dAgg{int,pr,ar}(pf,af,r) = ...
                                    circ_mean(squeeze(...
                                    phasepref2dAgg{int,pr,ar}(pf,af,r,...
                                    squeeze(~isnan(phasepref2dAgg...
                                    {int,pr,ar}(pf,af,r,:))))));
                            else
                                meanPPsessions2dAgg{int,pr,ar}(pf,af,r) = NaN;
                            end
                        end
                        meanPPall2dAgg{int,pr,ar}(pf,af) = circ_mean(...
                            squeeze(meanPPrats2dAgg{int,pr,ar}(pf,af,...
                            squeeze(~isnan(meanPPrats2dAgg{int,pr,ar}...
                            (pf,af,:))))));
                    end
                end
            end
        end
    end
    
    %% save analysis results
    
    disp(['writing results to file ' outputfile]);
    save(outputfile, '*2dAgg', 'cmax', 'foi*', 'w*', '-v6');
    
end

if plt == 'y'
    %% plot results
    for int = 1:numel(intervals)
        for pr = 1:2 % each phase modulating region
            if pr == 1
                prlabel = 'NAcc';
            else
                prlabel = 'BLA';
            end
            for ar = 1:2 % each amp modulated region
                if ar == 1
                    arlabel = 'NAcc';
                else
                    arlabel = 'BLA';
                end
                %% plot average z-score
                figure(pr+(ar-1)*2); clf;
                imagesc(foi_phase, foi_amp, mean(mean(zscore2dAgg...
                    {int,pr,ar},4,'omitnan'),3,'omitnan')',[0 cmax]);
                axis xy;
                set(gca, 'FontName', 'Arial', 'FontSize', 18);
                xlabel('Phase Frequency (Hz)');
                ylabel('Envelope Frequency (Hz)');
                title({['Average z-score across all rats & phases, ' ...
                    intervals{int}]; [prlabel ' phase modulating ' ...
                    arlabel ' amplitude']});
                colormap(jet); colorbar;
                set(gcf,'WindowStyle','docked');
                saveas(gcf,[figurefolder 'crossKL_MI2dz_Agg_' ...
                    intervals{int} '_' prlabel 'PhaseMod' ...
                    arlabel 'Amp.png'])
                
                %% plot phase preference
                figure(pr+(ar-1)*2+4); clf;
                imagesc(foi_phase, foi_amp, meanPPall2dAgg{int,pr,ar}');
                axis xy;
                set(gca, 'FontName', 'Arial', 'FontSize', 18);
                xlabel('Phase Frequency (Hz)');
                ylabel('Envelope Frequency (Hz)');
                title({['Average phasepref across all rats & phases, ' ...
                    intervals{int}]; [prlabel ' phase modulating ' ...
                    arlabel ' amplitude']});
                caxis([-pi pi]); colormap(hsv); colorbar;
                set(gcf,'WindowStyle','docked');
                saveas(gcf,[figurefolder 'crossKL_MI2dphase_Agg_' ...
                    intervals{int} '_' prlabel 'PhaseMod' ...
                    arlabel 'Amp.png'])
            end
        end
    end
end
