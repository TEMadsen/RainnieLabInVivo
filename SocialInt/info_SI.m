%% info needed for analyzing fear data, including all rats filenames
%#ok<*SNASGU> just provides background info for other analysis protocols

figurefolder = 'C:\Users\tmadsen\Pictures\TempFigures\';
datafolder = 'S:\Teresa\Analyses\TempData\';

% each row # here corresponds to cell row (1st dim) in other variables
ratnums = [333; 334; 335; 336];  

% each column # here corresponds to cell column (2nd dim) in other variables
phases = {'Session1', 'Session2', 'Session3', 'Session4'}; 

% each row # here corresponds to the numerical code that will be saved in
% trl and data.trialinfo to identify which trials correspond to which
% interval types
intervals = {'CagesObject'; 'AllRat'};

% each row # here corresponds to the numerical code that will represent
% which region's data is being used for phase or amplitude source data
regions = {'NAc'; 'BLA'};

% initialize variables
channels = cell(numel(ratnums),1);
targets = cell(numel(ratnums),1);
notes = cell(numel(ratnums),numel(phases));
nexfile = cell(numel(ratnums),numel(phases));
finalch = false([numel(ratnums),1]);

% any extra comments about each rat
notes{2,4} = 'subject rat knocked down USV microphone';
notes{4,3} = 'subject rat froze throughout phase 2';

% channels to use for each rat, defaults to 'AD*' if empty
channels{1} = {'AD05','AD13'};  
targets{1} = {'BLA','NAc'};  
finalch(1) = true;
channels{2} = {'AD05','AD13'};  
targets{2} = {'NAc','BLA'};  
finalch(2) = true;
channels{3} = {'AD05','AD13'};  
targets{3} = {'BLA','NAc'};  
finalch(3) = true;
channels{4} = {'AD05','AD13'};  
targets{4} = {'NAc','BLA'};  
finalch(4) = true;

%% filenames and invalidated trials
% Session 1
nexfile{1,1} = 'S:\Teresa\PlexonData Backup\333\social interaction\333_150326_0012_social1-aligned.nex';
nexfile{2,1} = 'S:\Teresa\PlexonData Backup\334\social interaction\334_150326_0013_social1-aligned.nex';
nexfile{3,1} = 'S:\Teresa\PlexonData Backup\335\social interaction\335_150327_0016_social1-aligned.nex';
nexfile{4,1} = 'S:\Teresa\PlexonData Backup\336\social interaction\336_150327_0017_social1-aligned.nex';

% Session 2
nexfile{1,2} = 'S:\Teresa\PlexonData Backup\333\social interaction\333_150401_0000_social2-aligned.nex';
nexfile{2,2} = 'S:\Teresa\PlexonData Backup\334\social interaction\334_150401_0001_social2-aligned.nex';
nexfile{3,2} = 'S:\Teresa\PlexonData Backup\335\social interaction\335_150402_0000_social2-aligned.nex';
nexfile{4,2} = 'S:\Teresa\PlexonData Backup\336\social interaction\336_150402_0001_social2-aligned.nex';

% Session 3
nexfile{1,3} = 'S:\Teresa\PlexonData Backup\333\social interaction\333_150408_0009_social3-aligned.nex';
nexfile{2,3} = 'S:\Teresa\PlexonData Backup\334\social interaction\334_150408_0010_social3-aligned.nex';
nexfile{3,3} = 'S:\Teresa\PlexonData Backup\335\social interaction\335_150409_0000_social3-aligned.nex';
nexfile{4,3} = 'S:\Teresa\PlexonData Backup\336\social interaction\336_150409_0001_social3-aligned.nex';

% Session 4
nexfile{1,4} = 'S:\Teresa\PlexonData Backup\333\social interaction\333_150415_0005_social4-aligned.nex';
nexfile{2,4} = 'S:\Teresa\PlexonData Backup\334\social interaction\334_150415_0006_social4-aligned.nex';
nexfile{3,4} = 'S:\Teresa\PlexonData Backup\335\social interaction\335_150416_0004_social4-aligned.nex';
nexfile{4,4} = 'S:\Teresa\PlexonData Backup\336\social interaction\336_150416_0005_social4-aligned.nex';
