%% calculates 2d cross-frequency phase-amplitude modulation
% for each interval type of multiple social interaction tests
% across multiple rats

close all force; clearvars; clc;  % clean slate

info_SI                     % load all background info (filenames, etc)

plt = 'y';                  % plot CFC
hx = waitbar(0,'Getting started...');

%% set analysis parameters

cfg = [];
cfg.Fs = 1000;          % Plexon's LFP sampling frequency
cfg.foi_mod = 20:300;   % central frequencies of modulated signals
cfg.wFactor = 8;        % scale factor
cfg.powerTh = 2;        % power threshold (S.D.)
cfg.winLen = 0.6*cfg.Fs;    % OTC window (+/- 0.6 s from oscillatory event)
cfg.nfft = 2^nextpow2(2*cfg.winLen);            % # of frequencies for fft
cfg.f_fft = cfg.Fs/2*linspace(0,1,cfg.nfft/2+1);    % actual freqs of fft
cfg.f_plot = cfg.f_fft<cfg.foi_mod(1);          % which frequencies to plot

% wavelet frequencies to find oscillations
cfg.foi_osc = floor(cfg.foi_mod(1)-cfg.foi_mod(1)/cfg.wFactor):...
    ceil(cfg.foi_mod(end)+cfg.foi_mod(end)/cfg.wFactor);

%% create set of Morlet wavelets - check this for accuracy!

wavelets = cell(size(cfg.foi_osc));
for bI = 1:length(cfg.foi_osc)
    f = cfg.foi_osc(bI);
    sigmaF = f/cfg.wFactor; % practical setting for spectral bandwidth (Tallon-Baudry)
    sigmaT = 1/(sigmaF*pi*2); % wavelet duration
    t = -4*sigmaT*cfg.Fs:4*sigmaT*cfg.Fs;
    t = t/cfg.Fs;
    if rem(length(t),2) == 0; t = t(1:end-1); end
    S1 = exp((-1*(t.^2)) / (2*sigmaT^2));
    S2 = exp(2*1i*pi*f*t);
    A = (sigmaT * sqrt(pi))^(-0.5); % normalization
    psi = A*S1.*S2;
    wavelets{bI} = psi;
end

%% make 2d Cross-Frequency Modulation plots for each rat/session/interval

for r = 1 %:numel(ratnums)
    chNAc = find(strcmp(targets{r},'NAc'));
    chBLA = find(strcmp(targets{r},'BLA'));
    for p = 1:numel(phases)
        if ~isempty(nexfile{r,p})
            % Full, artifact-free interaction intervals marked with
            % data.trialinfo(:,1) = original trial number, and
            % data.trialinfo(:,2) = number representing condition
            inputfile = [datafolder 'CleanLFPTrialData_'...
                num2str(ratnums(r)) phases{p} '.mat'];
            outputfile1 = [datafolder 'WaveletTransform_'...
                num2str(ratnums(r)) phases{p} '_fullint.mat'];
            outputfile2 = [datafolder 'crossOTCave_'...
                num2str(ratnums(r)) phases{p} '_fullint.mat'];
            
            if ~exist(inputfile,'file')
                error('inputfile does not exist');
            end
            
            if exist(outputfile1,'file') && exist(outputfile2,'file')
                disp(['Data for Rat # ' int2str(ratnums(r)) ', ' ...
                    phases{p} ' already analyzed!']);
                if plt == 'y'
                    disp('Loading previous analysis results from file.');
                    load(outputfile2);
                end
            else
                %% preallocate memory for variables
                
                zscore = cell(numel(intervals),2,2);
                pval = cell(numel(intervals),2,2);
                h = cell(numel(intervals),2,2);
                modS = cell(numel(intervals),2,2);
                otc = cell(numel(intervals),2,2);
                fft_otc = cell(numel(intervals),2,2);
                pwrN = cell(numel(intervals),2);
                
                fpeak = cell(numel(intervals),2,2);
                otcpeak = cell(numel(intervals),2,2);
                L = cell(numel(intervals),1);
                Lsort = cell(numel(intervals),1);
                otcmean = cell(numel(intervals),2,2);
                fftmean = cell(numel(intervals),2,2);
                modSmean = cell(numel(intervals),2,2);
                cmax = 0;
                
                %% load data
                load(inputfile);
                
                %% analyze data
                for int = 1:numel(intervals)
                    trials = find(data.trialinfo(:,2) == int & ...
                        data.sampleinfo(:,2)-data.sampleinfo(:,1) >= 1200);
%                     trials = 1:length(pwrN{int,1});   % if loading previous
                    % results for further analysis
                    if isempty(trials)
                        warning(['No ' intervals{int} ...
                            ' intervals found for Rat # ' ...
                            num2str(ratnums(r)) ', ' phases{p} ...
                            '.  Skipping.']);
                    else
                        trlen = zeros(numel(trials),1);
                        for t = 1:numel(trials)
                            waitbar(t/numel(trials),hx,...
                                {['Calculating OTC for Rat # ' ...
                                num2str(ratnums(r)) ',']; ...
                                [phases{p} ', ' intervals{int}]});
                            trdata{1} = data.trial{trials(t)}(chNAc,:);
                            trdata{2} = data.trial{trials(t)}(chBLA,:);
                            [zscoretemp,pvaltemp,htemp,modStemp,otctemp,...
                                fft_otctemp,pwrNtemp] = ...
                                crossOTCave_TEM(trdata,cfg,wavelets);
                            for pr = 1:2 % each phase modulating region
                                pwrN{int,pr}{t}(:,:) = ...
                                    pwrNtemp{pr};
                                for ar = 1:2 % each amp modulated region
                                    zscore{int,pr,ar}(:,t) = ...
                                        zscoretemp{pr,ar};
                                    pval{int,pr,ar}(:,t) = ...
                                        pvaltemp{pr,ar};
                                    h{int,pr,ar}(:,t) = ...
                                        htemp{pr,ar};
                                    modS{int,pr,ar}(:,t) = ...
                                        modStemp{pr,ar};
                                    otc{int,pr,ar}(:,:,t) = ...
                                        otctemp{pr,ar};
                                    fft_otc{int,pr,ar}(:,:,t) = ...
                                        fft_otctemp{pr,ar};
                                end
                            end
                            trlen(t) = size(pwrN{int,1}{t},2);
                        end
                        [L{int},Lsort{int}] = sort(trlen);
                        for pr = 1:2 % each phase modulating region
                            for ar = 1:2 % each amp modulated region
                                M = max(nanmean(zscore{int,pr,ar},2),[],...
                                    'omitnan');
                                cmax = max(M,cmax);
                                % CONSIDER WEIGHTING TRIALS BY # OF EVENTS
                                otcmean{int,pr,ar} = nanmean(...
                                    otc{int,pr,ar}(:,:,:),3);
                                fftmean{int,pr,ar} = fft(...
                                    otcmean{int,pr,ar},cfg.nfft,2);
                                modSmean{int,pr,ar} = range(...
                                    otcmean{int,pr,ar},2);
                                [~,I] = max(modSmean{int,pr,ar},[],...
                                    'omitnan');
                                fpeak{int,pr,ar} = cfg.foi_mod(I);
                                otcpeak{int,pr,ar} = ...
                                    otcmean{int,pr,ar}(I,:);
                            end
                        end
                    end
                end
                
                %% save analysis results
%                 keyboard; % double-check result before saving!
                disp(['writing wavelet transform to file ' outputfile1]);
                save(outputfile1, 'pwrN', '-v7.3');
                
                disp(['writing results to file ' outputfile2]);
                save(outputfile2, 'cfg', 'zscore', 'pval', 'h', ...
                    'modS*', 'fft*', 'otc*', 'L*', 'fpeak', 'cmax', ...
                    '-v7.3');
                
            end
            if plt == 'y'
                %% plot results - needs updating!!!
                for int = 1:numel(intervals)
                    if isempty(zscore{int,1,1})
                        warning(['No ' intervals{int} ...
                            ' intervals found for Rat # ' ...
                            num2str(ratnums(r)) ', ' phases{p} ...
                            '.  Skipping.']);
                    else
                        for pr = 1:2 % each phase modulating region
                            for ar = 1:2 % each amp modulated region
                                %% plot z-scores for each trial + mean
                                figure(pr+(ar-1)*2); clf;
                                imagesc([], cfg.foi_mod, ...
                                    [zscore{int,pr,ar}(:,Lsort{int}) ...
                                    nanmean(zscore{int,pr,ar},2)],...
                                    [0 max(max(zscore{int,pr,ar}))]);
                                axis xy tight; hold on;
                                plot([size(zscore{int,pr,ar},2)+0.5 ...
                                    size(zscore{int,pr,ar},2)+0.5],...
                                    [cfg.foi_mod(1) cfg.foi_mod(end)],...
                                    'w','LineWidth',2);
                                set(gca, 'FontName', 'Arial', 'FontSize', 12);
                                xlabel('Interaction Interval (sorted by duration) + Mean');
                                ylabel('Modulated Frequency (Hz)');
                                title({['Rat # ' num2str(ratnums(r)) ', ' ...
                                    char(phases(p)) ', ' intervals{int}]; ...
                                    [regions{pr} ' phase modulating ' ...
                                    regions{ar} ' amplitude']; ...
                                    [num2str(size(zscore{int,pr,ar},2)) ...
                                    ' Interactions']});
                                colormap(jet); colorbar;
                                set(gcf,'WindowStyle','docked');
                                saveas(gcf,[figurefolder 'crossOTCavez_'...
                                    num2str(ratnums(r)) phases{p} '_' ...
                                    intervals{int} '_' regions{pr} 'PhaseMod' ...
                                    regions{ar} 'Amp_fullint.png'])
                                
                                %% plot mean OTCG
                                figure(pr+(ar-1)*2+4); clf;
                                subplot(1,5,1:3);
                                imagesc([-cfg.winLen cfg.winLen], ...
                                    cfg.foi_mod, otcmean{int,pr,ar});
                                axis xy tight; hold on;
                                plot([0 0],[cfg.foi_mod(1) cfg.foi_mod(end)],...
                                    'w','LineWidth',2);
                                plot(-cfg.winLen:cfg.winLen-1,otcpeak{int,pr,ar}...
                                    +fpeak{int,pr,ar},'k');
                                set(gca, 'FontName', 'Arial', 'FontSize', 12);
                                xlabel('Time from Oscillatory Peak (ms)');
                                ylabel('Modulated Frequency (Hz)');
                                title({['Rat # ' num2str(ratnums(r)) ', ' ...
                                    char(phases(p)) ', ' intervals{int}]; ...
                                    [regions{pr} ' phase modulating ' ...
                                    regions{ar} ' amplitude']; ...
                                    ['Average of  ' ...
                                    num2str(size(zscore{int,pr,ar},2)) ...
                                    ' Interactions']});
                                colormap(jet); colorbar;
                                set(gcf,'WindowStyle','docked');
                                
                                %% plot modulation strength of mean OTCG
                                subplot(1,5,4);
                                plot(modSmean{int,pr,ar},cfg.foi_mod,'k');
                                axis xy tight
                                xlabel('Modulation Strength');
                                
                                %% plot FFT of mean OTCG
                                subplot(1,5,5);
                                imagesc(cfg.f_fft(cfg.f_plot), cfg.foi_mod, ...
                                    2*abs(fftmean{int,pr,ar}(:,cfg.f_plot)));
                                axis xy tight; hold on;
                                set(gca, 'FontName', 'Arial', 'FontSize', 12);
                                xlabel('Modulating Frequency (Hz)');
                                title({['Rat # ' num2str(ratnums(r)) ', ' ...
                                    char(phases(p)) ', ' intervals{int}]; ...
                                    [regions{pr} ' phase modulating ' ...
                                    regions{ar} ' amplitude']; ...
                                    ['Average of  ' ...
                                    num2str(size(zscore{int,pr,ar},2)) ...
                                    ' Interactions']});
                                colormap(jet); colorbar;
                                set(gcf,'WindowStyle','docked');
                                saveas(gcf,[figurefolder 'crossOTCGave_'...
                                    num2str(ratnums(r)) phases{p} '_' ...
                                    intervals{int} '_' regions{pr} 'PhaseMod' ...
                                    regions{ar} 'Amp_fullint.png'])
                            end
                        end
                    end
                end
            end
        end
    end
end
close(hx)
