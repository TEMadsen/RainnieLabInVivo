function mod2d = KL_MI2d_TEM(data,foi_phase,w_phase,foi_amp,w_amp,plt)
%%   Compute the two-dimensional Kullback�Leibler MI and plot the result.
%       From Tort et al., J Neurophysiol, 2010
%
%   USAGE:
%       mod2d = KL_MI2d_TEM(data,foi_phase,w_phase,foi_amp,w_amp,plt);
%
%   INPUTS:
%       data        = The unfiltered data, assumes 1000 Hz sampling rate
%       foi_phase   = central frequencies for phase modulation
%       w_phase     = width of phase band (foi +/- w)
%       foi_amp     = central frequencies for modulated amplitude
%       w_amp       = width of amplitude band (foi +/- w)
%       plt         = 'y' or 'n' to plot data
%
%   OUTPUTS:
%       A plot of the two-dimensional modulation index.
%       mod2d       = The two-dim modulation index.
%
%   DEPENDENCIES:
%    KL_MI_TEM.m
%    eegfilt.m (From EEGLAB Toolbox, http://www.sccn.ucsd.edu/eeglab/)
%
%   original function programmed by MAK.  Nov 12, 2007.
%   modified by TEM 2015

if nargin < 6 || isempty(plt);  plt = 'n';  end

mod2d = zeros(length(foi_phase), length(foi_amp));
epochframes = length(data);
h = waitbar(0,'Filtering data...');

for i=1:length(foi_phase)
    filtorder = 3*fix(1000/(foi_phase(i)-w_phase));
    if filtorder*3 >= epochframes
        filtorder = fix(epochframes/3)-1;
    end
    % Filter the low freq signal & extract its phase.
    theta=eegfilt(data,1000,foi_phase(i)-w_phase,foi_phase(i)+w_phase,...
        epochframes,filtorder); 
    phase = angle(hilbert(theta));
    waitbar(i/length(foi_phase))
    for j=1:length(foi_amp)
        % Filter the high freq signal & extract its amplitude envelope.
        gamma=eegfilt(data,1000,foi_amp(j)-w_amp,foi_amp(j)+w_amp);     
        amp = abs(hilbert(gamma));
        [mi] = KL_MI_TEM(amp, phase);   % Compute the modulation index.
        mod2d(i,j) = mi;
    end
end
close(h)

if plt == 'y'
    %Plot the two-dimensional modulation index.
    figure(gcf); imagesc(foi_phase, foi_amp, mod2d');  colorbar; axis xy
    set(gca, 'FontName', 'Arial', 'FontSize', 14); title('KL-MI 2d');
    xlabel('Phase Frequency (Hz)');  ylabel('Envelope Frequency (Hz)');
end
end
