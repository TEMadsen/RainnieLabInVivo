%% calculates cross-frequency phase-amplitude modulation
% for each interval type of multiple social interaction tests
% across multiple rats

close all force; clearvars; clc;  % clean slate

info_SI                         % load all background info (filenames, etc)

hx = waitbar(0,'Getting started...');

%% set analysis parameters

foi_phase = 7.5;            % central frequencies for phase modulation
w_phase = 1.5;              % width of phase band (foi +/- w)
foi_amp = 85;               % central frequencies for modulated amplitude
w_amp = 15;                 % width of amplitude band (foi +/- w)

%% calculate Cross-Frequency Modulation for each rat/session/interval

for r = 1:numel(ratnums)
    chNAc = find(strcmp(targets{r},'NAc'));
    chBLA = find(strcmp(targets{r},'BLA'));
    for p = 1:numel(phases)
        if ~isempty(nexfile{r,p})
            % Full, artifact-free interaction intervals marked with
            % data.trialinfo(:,1) = original trial number, and
            % data.trialinfo(:,2) = number representing condition
            inputfile = [datafolder 'CleanLFPTrialData_'...
                num2str(ratnums(r)) phases{p} '.mat'];
            outputfile = [datafolder 'crossKL_MIz_'...
                num2str(ratnums(r)) phases{p} '_fullint.mat'];
            
            if ~exist(inputfile,'file')
                error('inputfile does not exist');
            end
            
            if exist(outputfile,'file')
                disp(['Data for Rat # ' int2str(ratnums(r)) ', ' ...
                    phases{p} ' already analyzed!']);
            else
                %% preallocate memory for variables
                
                zscore = cell(numel(intervals),2,2);
                pval = cell(numel(intervals),2,2);
                h = cell(numel(intervals),2,2);
                MI = cell(numel(intervals),2,2);
                phasepref = cell(numel(intervals),2,2);
                
                cmax = 0;
                
                %% load data
                load(inputfile);
                
                %% analyze data
                for int = 1:numel(intervals)
                    trials = find(data.trialinfo(:,2) == int & ...
                        data.sampleinfo(:,2)-data.sampleinfo(:,1) >= 3000);
                    if isempty(trials)
                        warning(['No ' intervals{int} ...
                            ' intervals found for Rat # ' ...
                            num2str(ratnums(r)) ', ' phases{p} ...
                            '.  Skipping.']);
                    else
                        for t = 1:numel(trials)
                            waitbar(t/numel(trials),hx,...
                                {['Calculating cross-region MI z-score' ...
                                ' for Rat # ' num2str(ratnums(r)) ',']; ...
                                [phases{p} ', ' intervals{int}]});
                            [zscoreTemp,pvalTemp,hTemp,MITemp,...
                                phaseprefTemp] = sig_crossKL_MI2d_TEM(...
                                data.trial{trials(t)}(chNAc,:),...
                                data.trial{trials(t)}(chBLA,:),...
                                foi_phase,w_phase,foi_amp,w_amp);
                            for pr = 1:2    % each phase modulating region
                                for ar = 1:2    % each amp modulated region
                                    zscore{int,pr,ar}(t) = ...
                                        zscoreTemp{pr,ar};
                                    pval{int,pr,ar}(t) = ...
                                        pvalTemp{pr,ar};
                                    h{int,pr,ar}(t) = ...
                                        hTemp{pr,ar};
                                    if any(isnan(h{int,pr,ar}))
                                        error(['NaN(s) found in h{' ...
                                            num2str(int) ',' num2str(pr)...
                                            ',' num2str(ar) '}']);
                                    end
                                    MI{int,pr,ar}(t) = ...
                                        MITemp{pr,ar};
                                    phasepref{int,pr,ar}(t) = ...
                                        phaseprefTemp{pr,ar};
                                    cmax = max(mean(zscore{int,pr,ar}),...
                                        cmax);
                                end
                            end
                        end
                    end
                end
                
                %% save analysis results
                
                disp(['writing results to file ' outputfile]);
                save(outputfile,'zscore','pval','h','MI','phasepref',...
                    'cmax','foi*','w*','-v6');
                
            end 
        end
    end
end
close(hx)
