%% calculates 2d cross-frequency phase-amplitude modulation
% for each interval type of multiple social interaction tests
% across multiple rats

close all force; clearvars; clc;  % clean slate

info_SI                     % load all background info (filenames, etc)

plt = 'y';                  % plot CFC
hx = waitbar(0,'Getting started...');

%% set analysis parameters

foi_phase = 1.5:0.5:20;   % central frequencies for phase modulation
w_phase = 0.5;              % width of phase band (foi +/- w)
foi_amp = 15:5:180;         % central frequencies for modulated amplitude
w_amp = 5;                  % width of amplitude band (foi +/- w)

%% make 2d Cross-Frequency Modulation plots for each rat/session/interval

for r = 1:numel(ratnums)
    chNAc = find(strcmp(targets{r},'NAc'));
    chBLA = find(strcmp(targets{r},'BLA'));
    for p = 1:numel(phases)
        if ~isempty(nexfile{r,p})
            % Full, artifact-free interaction intervals marked with
            % data.trialinfo(:,1) = original trial number, and
            % data.trialinfo(:,2) = number representing condition
            inputfile = [datafolder 'CleanLFPTrialData_'...
                num2str(ratnums(r)) phases{p} '.mat'];
            outputfile = [datafolder 'crossKL_MI2dz_'...
                num2str(ratnums(r)) phases{p} '_fullint.mat'];
            
            if ~exist(inputfile,'file')
                error('inputfile does not exist');
            end
            
            if exist(outputfile,'file')
                disp(['Data for Rat # ' int2str(ratnums(r)) ', ' ...
                    phases{p} ' already analyzed!']);
                if plt == 'y'
                    disp('Loading previous analysis results from file.');
                    load(outputfile);
                end
            else
                %% preallocate memory for variables
                
                zscore2d = cell(numel(intervals),1);
                pval2d = cell(numel(intervals),1);
                h2d = cell(numel(intervals),1);
                MI2d = cell(numel(intervals),1);
                phasepref2d = cell(numel(intervals),1);
                
                cmax = 0;
                
                %% load data
                load(inputfile);
                
                %% analyze data
                for int = 1:numel(intervals)
                    trials = find(data.trialinfo(:,2) == int & ...
                        data.sampleinfo(:,2)-data.sampleinfo(:,1) >= 3000);
                    if isempty(trials)
                        warning(['No ' intervals{int} ...
                            ' intervals found for Rat # ' ...
                            num2str(ratnums(r)) ', ' phases{p} ...
                            '.  Skipping.']);
                    else
                        for t = 1:numel(trials)
                            waitbar(t/numel(trials),hx,...
                                {['Calculating 2d cross-region MI z-score for Rat # ' ...
                                num2str(ratnums(r)) ',']; ...
                                [phases{p} ', ' intervals{int}]});
                            [zscore2dtemp,pval2dtemp,h2dtemp,MI2dtemp,...
                                phasepref2dtemp] = sig_crossKL_MI2d_TEM(...
                                data.trial{trials(t)}(chNAc,:),...
                                data.trial{trials(t)}(chBLA,:),...
                                foi_phase,w_phase,foi_amp,w_amp);
                            for pr = 1:2 % each phase modulating region
                                for ar = 1:2 % each amp modulated region
                                    zscore2d{int}{pr,ar}(:,:,t) = ...
                                        zscore2dtemp{pr,ar};
                                    pval2d{int}{pr,ar}(:,:,t) = ...
                                        pval2dtemp{pr,ar};
                                    h2d{int}{pr,ar}(:,:,t) = ...
                                        h2dtemp{pr,ar};
                                    MI2d{int}{pr,ar}(:,:,t) = ...
                                        MI2dtemp{pr,ar};
                                    phasepref2d{int}{pr,ar}(:,:,t) = ...
                                        phasepref2dtemp{pr,ar};
                                end
                            end
                        end
                        cmax = max(max(max(mean(...
                            zscore2d{int}{pr,ar},3))),cmax);
                    end
                end
                
                %% save analysis results
                
                disp(['writing results to file ' outputfile]);
                save(outputfile, '*2d', 'cmax', 'foi*', 'w*', '-v6');
                
            end
            if plt == 'y'
                %% plot results
                for int = 1:numel(intervals)
                    if isempty(zscore2d{int})
                        warning(['No ' intervals{int} ...
                            ' intervals found for Rat # ' ...
                            num2str(ratnums(r)) ', ' phases{p} ...
                            '.  Skipping.']);
                    else
                        for pr = 1:2 % each phase modulating region
                            if pr == 1
                                prlabel = 'NAc';
                            else
                                prlabel = 'BLA';
                            end
                            for ar = 1:2 % each amp modulated region
                                if ar == 1
                                    arlabel = 'NAc';
                                else
                                    arlabel = 'BLA';
                                end
                                %% plot average z-score
                                figure(pr+(ar-1)*2); clf;
                                imagesc(foi_phase, foi_amp, mean(...
                                    zscore2d{int}{pr,ar},3)');
                                axis xy;
                                set(gca, 'FontName', 'Arial', 'FontSize', 18);
                                xlabel('Phase Frequency (Hz)');
                                ylabel('Envelope Frequency (Hz)');
                                title({['Rat # ' num2str(ratnums(r)) ', ' ...
                                    char(phases(p)) ', ' intervals{int}]; ...
                                    [prlabel ' phase modulating ' ...
                                    arlabel ' amplitude']; ...
                                    ['Average of  ' ...
                                    num2str(size(zscore2d{int}{pr,ar},3)) ...
                                    ' Interactions']});
                                caxis([0 cmax]); colormap(jet); colorbar;
                                set(gcf,'WindowStyle','docked');
                                saveas(gcf,[figurefolder 'crossKL_MI2dz_'...
                                    num2str(ratnums(r)) phases{p} '_' ...
                                    intervals{int} '_' prlabel 'PhaseMod' ...
                                    arlabel 'Amp_fullint.png'])
                                
                                %% plot phase preference
                                figure(pr+(ar-1)*2+4); clf;
                                imagesc(foi_phase, foi_amp, circ_mean(circ_ang2rad(...
                                    phasepref2d{int}{pr,ar}),[],3)');
                                axis xy;
                                set(gca, 'FontName', 'Arial', 'FontSize', 18);
                                xlabel('Phase Frequency (Hz)');
                                ylabel('Envelope Frequency (Hz)');
                                title({['Rat # ' num2str(ratnums(r)) ', ' ...
                                    char(phases(p)) ', ' intervals{int}]; ...
                                    [prlabel ' phase modulating ' ...
                                    arlabel ' amplitude']; ...
                                    ['Average of  ' ...
                                    num2str(size(zscore2d{int}{pr,ar},3)) ...
                                    ' Interactions']});
                                caxis([-pi pi]); colormap(hsv); colorbar;
                                set(gcf,'WindowStyle','docked');
                                saveas(gcf,[figurefolder 'crossKL_MI2dphase_'...
                                    num2str(ratnums(r)) phases{p} '_' ...
                                    intervals{int} '_' prlabel 'PhaseMod' ...
                                    arlabel 'Amp_fullint.png'])
                            end
                        end
                    end
                end
            end
        end
    end
end
close(hx)
