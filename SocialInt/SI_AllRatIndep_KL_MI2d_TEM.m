%% calculates 2d cross-frequency phase-amplitude modulation
% for each interval type of multiple social interaction tests
% across multiple rats

close all; clearvars; clc;  % clean slate

info_SI                     % load all background info (filenames, etc)

plt = 'n';                  % plot CFC
hx = waitbar(0,'Getting started...');

%% set analysis parameters

foi_phase = 1.5:0.5:15.5;   % central frequencies for phase modulation
w_phase = 0.5;              % width of phase band (foi +/- w)
foi_amp = 12:4:118;         % central frequencies for modulated amplitude
w_amp = 8;                  % width of amplitude band (foi +/- w)

%% make 2d Cross-Frequency Modulation plots for each rat/session/interval

for r = 1:numel(ratnums)
    chNAcc = find(strcmp(targets{r},'NAcc'));
    chBLA = find(strcmp(targets{r},'BLA'));
    for p = 1:numel(phases)
        try   % use try-catch when running unsupervised batches
            if ~isempty(nexfile{r,p})
                % Artifact-free 3-second subtrials marked with
                % data.trialinfo(:,1) = original trial number, and
                % data.trialinfo(:,2) = number representing condition
                inputfile = [datafolder 'CleanLFPSubtrialData_'...
                    num2str(ratnums(r)) phases{p} '.mat'];
                outputfile = [datafolder 'KL_MI2d_'...
                    num2str(ratnums(r)) phases{p} '.mat'];
                
                if ~exist(inputfile,'file')
                    error('inputfile does not exist');
                end
                
                if exist(outputfile,'file')
                    disp(['Data for Rat # ' int2str(ratnums(r)) ', ' ...
                        phases{p} ' already analyzed! Skipping.']);
                else
                    %% preallocate memory for variables
                    
                    MI2d_NAcc = cell(numel(intervals),1);
                    MI2d_BLA = cell(numel(intervals),1);
                    cmax_NAcc = zeros(numel(intervals),1);
                    cmax_BLA = zeros(numel(intervals),1);
                    
                    %% load data
                    load(inputfile);
                    
                    %% analyze data
                    for int = 1:numel(intervals)
                        trials = find(data.trialinfo(:,2) == int);
                        if isempty(trials)
                            warning(['No ' intervals{int} ...
                                ' intervals found for Rat # ' ...
                                num2str(ratnums(r)) ', ' phases{p} ...
                                '.  Skipping.']);
                        else
                            for t = 1:numel(trials)
                                waitbar((1+((t-1)+((int-1)+((p-1)+(r-1)*numel(phases))*...
                                    numel(intervals))*numel(trials))*2)/(2*...
                                    numel(ratnums)*numel(phases)*numel(intervals)*...
                                    numel(trials)),hx,...
                                    ['Calculating 2d MI for Rat # ' num2str(ratnums(r)) ...
                                    ', ' phases{p} ', ' intervals{int} ', NAcc']);
                                [MI2d_NAcc{int}(:,:,t)] = KL_MI2d_TEM(...
                                    data.trial{trials(t)}(chNAcc,:),foi_phase,...
                                    w_phase,foi_amp,w_amp,plt);
                                
                                waitbar((2+((t-1)+((int-1)+((p-1)+(r-1)*numel(phases))*...
                                    numel(intervals))*numel(trials))*2)/(2*...
                                    numel(ratnums)*numel(phases)*numel(intervals)*...
                                    numel(trials)),hx,...
                                    ['Calculating 2d MI for Rat # ' num2str(ratnums(r)) ...
                                    ', ' phases{p} ', ' intervals{int} ', BLA']);
                                [MI2d_BLA{int}(:,:,t)] = KL_MI2d_TEM(...
                                    data.trial{trials(t)}(chBLA,:),foi_phase,...
                                    w_phase,foi_amp,w_amp);
                                
                            end
                            if plt == 'y'
                                figure(1+(int-1)*2);
                                %%% plot mean!!!
                                title({['Rat # ' num2str(ratnums(r)) ', ' ...
                                    char(phases(p)) ', ' intervals{int}]; ...
                                    ['NAcc, Average of  ' ...
                                    num2str(numel(trials)) ' Trials']});
                                figure(int*2);
                                %%% plot mean!!!
                                title({['Rat # ' num2str(ratnums(r)) ', ' ...
                                    char(phases(p)) ', ' intervals{int}]; ...
                                    ['BLA, Average of  ' ...
                                    num2str(numel(trials)) ' Trials']});
                            end
                            cmax_NAcc(int)=max(max(max(mean(...
                                MI2d_NAcc{int},3))),cmax_NAcc(int));
                            cmax_BLA(int)=max(max(max(mean(...
                                MI2d_BLA{int},3))),cmax_BLA(int));
                        end
                    end
                    if plt == 'y'
                        for fig = 1:2:(int*2)
                            figure(fig); caxis([0 cmax_NAcc(int)]);
                            set(gcf,'WindowStyle','docked');
                        end
                        for fig = 2:2:(int*2)
                            figure(fig); caxis([0 cmax_BLA(int)]);
                            set(gcf,'WindowStyle','docked');
                        end
                        % pause execution to save & close figures (crashes otherwise)
                        keyboard; % type "return" to continue loop or "dbquit" to exit
                    end
                    %% save analysis results
                    
                    disp(['writing results to file ' outputfile]);
                    
                    save(outputfile, 'MI2d*', 'cmax*', 'foi*', 'w*', '-v6');
                    
                end
            end
        catch ME
            warning(['Error (' ME.message ') while processing Rat # ' ...
                int2str(ratnums(r)) ', ' phases{p} ...
                '! Continuing with next in line.']);
        end
    end
end
close(hx)
