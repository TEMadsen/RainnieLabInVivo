%% calculates statistics on 2d cross-frequency phase-amplitude modulation
% for each interval type of multiple social interaction tests
% across multiple rats

close all force; clearvars; clc;  % clean slate

info_SI                         % load all background info (filenames, etc)
stats = 'y';                    % run stats

outputfile1 = [datafolder 'crossKL_MIz_fullint_Agg.mat'];   % aggregated results
outputfile2 = [datafolder 'crossKL_MIz_fullint_Stats.mat']; % statistical results

%% load or aggregate data

if exist(outputfile2,'file')
    disp('Statistics already calculated!');
else
    if exist(outputfile1,'file')
        disp('Data already aggregated!');
        if stats == 'y'
            disp('Loading previous analysis results from file.');
            load(outputfile1);
        end
    else
        %% preallocate memory for variables with cells for intervals (dim 1) &
        % sources of phase (dim 2) & amplitude (dim 3) data, while allowing
        % averaging & statistics across the dimensions representing rats (dim
        % 4) & test sessions (dim 5)
        
        zscoreAgg = NaN(numel(intervals),2,2,numel(ratnums),numel(phases));
        pvalAgg = NaN(numel(intervals),2,2,numel(ratnums),numel(phases));
        hAgg = zeros(numel(intervals),2,2,numel(ratnums),numel(phases));
        MIAgg = NaN(numel(intervals),2,2,numel(ratnums),numel(phases));
        phaseprefAgg = NaN(numel(intervals),2,2,numel(ratnums),numel(phases));
        meanPPratsAgg = NaN(numel(intervals),2,2,numel(phases));
        meanPPsessionsAgg = NaN(numel(intervals),2,2,numel(ratnums));
        meanPPallAgg = NaN(numel(intervals),2,2);
        
        %% average across trials for each rat/session/interval/region pair
        
        for r = 1:numel(ratnums)
            for p = 1:numel(phases)
                inputfile = [datafolder 'crossKL_MIz_'...
                    num2str(ratnums(r)) phases{p} '_fullint.mat'];
                
                if ~exist(inputfile,'file')
                    warning('inputfile does not exist.  Filled with NaNs.');
                else
                    load(inputfile);
                    
                    for int = 1:numel(intervals)
                        if isempty(zscore{int})
                            warning(['No ' intervals{int} ...
                                ' intervals found for Rat # ' ...
                                num2str(ratnums(r)) ', ' phases{p} ...
                                '.  Filled with NaNs.']);
                        else
                            for pr = 1:2        % each phase modulating region
                                for ar = 1:2    % each amp modulated region
                                    if any(isnan(h{int,pr,ar}))
                                        error(['NaN(s) found in h{' num2str(int) ',' num2str(pr) ',' num2str(ar) '}']);
                                    end
                                    zscoreAgg(int,pr,ar,r,p) = ...
                                        mean(zscore{int,pr,ar});
                                    pvalAgg(int,pr,ar,r,p) = ...
                                        mean(pval{int,pr,ar});
                                    hAgg(int,pr,ar,r,p) = ...
                                        mean(h{int,pr,ar});
                                    MIAgg(int,pr,ar,r,p) = ...
                                        mean(MI{int,pr,ar});
                                    if sum(h{int,pr,ar}) > 0
                                        % average the preferred phases only
                                        % from the significant trials
                                        phaseprefAgg(int,pr,ar,r,p) = ...
                                            circ_mean(circ_ang2rad(...
                                            phasepref{int,pr,ar}...
                                            (logical(h{int,pr,ar}))),[],2);
                                    else
                                        % warning(['No significant trials'...
                                        %     ' found for this frequency'...
                                        %     ' pair (phasepref = NaN)']);
                                        phaseprefAgg(int,pr,ar,r,p) = NaN;
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
        
        %% find max mean z-score to scale figures & average phasepref of only
        % the frequency pairs showing significant coupling
        
        cmax = 0;
        
        for int = 1:numel(intervals)
            for pr = 1:2        % each phase modulating region
                for ar = 1:2    % each amp modulated region
                    cmax = max(mean(mean(zscoreAgg(int,pr,ar,:,:),5,...
                        'omitnan'),4,'omitnan'),cmax);
                    for p = 1:numel(phases)
                        if sum(hAgg(int,pr,ar,:,p)) > 0
                            meanPPratsAgg(int,pr,ar,p) = ...
                                circ_mean(phaseprefAgg(int,pr,ar,...
                                ~isnan(phaseprefAgg(int,pr,ar,:,p)),p),[],4);
                        else
                            meanPPratsAgg(int,pr,ar,p) = NaN;
                        end
                    end
                    for r = 1:numel(ratnums)
                        if sum(hAgg(int,pr,ar,r,:)) > 0
                            meanPPsessionsAgg(int,pr,ar,r) = ...
                                circ_mean(...
                                phaseprefAgg(int,pr,ar,r,...
                                ~isnan(phaseprefAgg...
                                (int,pr,ar,r,:))),[],5);
                        else
                            meanPPsessionsAgg(int,pr,ar,r) = NaN;
                        end
                    end
                    if sum(sum(hAgg(int,pr,ar,:,:),5),4) > 0
                        meanPPallAgg(int,pr,ar) = circ_mean(...
                            meanPPratsAgg(int,pr,ar,...
                            ~isnan(meanPPratsAgg(int,pr,ar,:))),[],4);
                    else
                        meanPPallAgg(int,pr,ar) = NaN;
                    end
                end
            end
        end
        
        %% save analysis results
        
        disp(['writing aggregated results to file ' outputfile1]);
        save(outputfile1, '*Agg', 'cmax', 'foi*', 'w*', '-v6');
        
    end
    
    %% 3-way repeated measures ANOVA using regionpair, session, & interval
    
    next = 0;
    regionpair = cell(32,1);
    session = cell(32,1);
    interval = cell(32,1);
    varnames = cell(1,32);
    rdata = NaN(numel(ratnums),32);     % data organized by rats for ranova
    
    for pr = 1:2            % It may be possible to do this more efficiently
        for ar = 1:2        % using the unstack function, but this works.
            for p = 1:numel(phases)
                for int = 1:numel(intervals)
                    next = next + 1;
                    regionpair{next} = [regions{pr} 'Mod' regions{ar}];
                    session{next} = phases{p};
                    interval{next} = intervals{int};
                    varnames{next} = [regionpair{next} '_' session{next} '_'...
                        interval{next}];
                    for r = 1:numel(ratnums)
                        rdata(r,next) = zscoreAgg(int,pr,ar,r,p);
                    end
                end
            end
        end
    end
    
    winS = table(regionpair,session,interval);  % within subjects design
    winS.regionpair = categorical(winS.regionpair);
    winS.session = categorical(winS.session);
    winS.interval = categorical(winS.interval);
    winS.regionpair_session = winS.regionpair .* winS.session;
    winS.regionpair_interval = winS.regionpair .* winS.interval;
    winS.session_interval = winS.session .* winS.interval;
    winS.regionpair_session_interval = winS.regionpair .* winS.session .* ...
        winS.interval;
    
    rm = fitrm(array2table(rdata),'rdata1-rdata32~1','WithinDesign',winS);
    ranovatbl = ranova(rm,'WithinModel','regionpair*session*interval');
    
    disp(ranovatbl);
    
    %% posthoc tests
    
    if ranovatbl.pValue(3) < 0.05
        tblr = multcompare(rm,'regionpair');
        disp(tblr(tblr.pValue < 0.05 & tblr.Difference > 0,:));
    end
    
    if ranovatbl.pValue(5) < 0.05
        tbls = multcompare(rm,'session');
        disp(tbls(tbls.pValue < 0.05 & tbls.Difference > 0,:));
    end
    
    if ranovatbl.pValue(7) < 0.05
        tbli = multcompare(rm,'interval');
        disp(tbli(tbli.pValue < 0.05 & tbli.Difference > 0,:));
    end
    
    if ranovatbl.pValue(9) < 0.05
        %     tblrs = multcompare(rm,'regionpair','by','session');
        %     tblsr = multcompare(rm,'session','by','regionpair');
        tbl_rs = multcompare(rm,'regionpair_session');      % more conservative
        disp(tbl_rs(tbl_rs.pValue < 0.05 & tbl_rs.Difference > 0,:));
    end
    
    if ranovatbl.pValue(11) < 0.05
        %     tblri = multcompare(rm,'regionpair','by','interval');
        %     tblir = multcompare(rm,'interval','by','regionpair');
        tbl_ri = multcompare(rm,'regionpair_interval');     % more conservative
        disp(tbl_ri(tbl_ri.pValue < 0.05 & tbl_ri.Difference > 0,:));
    end
    
    if ranovatbl.pValue(13) < 0.05
        %     tblsi = multcompare(rm,'session','by','interval');
        %     tblis = multcompare(rm,'interval','by','session');
        tbl_si = multcompare(rm,'session_interval');        % more conservative
        disp(tbl_si(tbl_si.pValue < 0.05 & tbl_si.Difference > 0,:));
    end
    
    if ranovatbl.pValue(15) < 0.05
        tbl3way = multcompare(rm,'regionpair_session_interval');
        disp(tbl3way(tbl3way.pValue < 0.05 & tbl3way.Difference > 0,:));
    end
    
    %% save statistical results
    
    disp(['writing statistical results to file ' outputfile2]);
        save(outputfile2, '*tbl*', 'foi*', 'w*', '-v6');
        
end