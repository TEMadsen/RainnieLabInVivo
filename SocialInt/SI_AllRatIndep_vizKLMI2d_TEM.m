%% plots 2d cross-frequency phase-amplitude modulation
% for each interval type of multiple social interaction tests
% across multiple rats

close all; clearvars; clc;  % clean slate

info_SI                     % load all background info (filenames, etc)

%% make 2d Cross-Frequency Modulation plots for each rat/session/interval

for r = 1:numel(ratnums)
    for p = 1:numel(phases)
        if ~isempty(nexfile{r,p})
            % CFC analysis result file
            inputfile = [datafolder 'KL_MI2d_'...
                num2str(ratnums(r)) phases{p} '.mat'];
            
            if ~exist(inputfile,'file')
                error('inputfile does not exist');
            end
            
            %% load analysis result
            load(inputfile);
            
            if ~exist('foi_amp','var')
                foi_phase = 1.5:0.5:15.5;   % central frequencies for phase modulation
                w_phase = 0.5;              % width of phase band (foi +/- w)
                foi_amp = 12:4:118;         % central frequencies for modulated amplitude
                w_amp = 8;                  % width of amplitude band (foi +/- w)
            end
            
            %% plot result
            for int = 1:numel(intervals)
                if isempty(MI2d_BLA{int})
                    warning(['No ' intervals{int} ...
                        ' intervals found for Rat # ' ...
                        num2str(ratnums(r)) ', ' phases{p} ...
                        '.  Skipping.']);
                else
                    figure(1); clf;
                    imagesc(foi_phase, foi_amp, mean(MI2d_NAcc{int},3)');
                    axis xy;
                    set(gca, 'FontName', 'Arial', 'FontSize', 14);
                    xlabel('Phase Frequency (Hz)');
                    ylabel('Envelope Frequency (Hz)');
                    title({['Rat # ' num2str(ratnums(r)) ', ' ...
                        char(phases(p)) ', ' intervals{int}]; ...
                        ['NAcc, Average of  ' ...
                        num2str(size(MI2d_NAcc{int},3)) ' Trials']});
                    caxis([0 cmax_NAcc(int)]); colormap(jet); colorbar;
                    set(gcf,'WindowStyle','docked');
                    saveas(gcf,[figurefolder 'KL_MI2d_'...
                        num2str(ratnums(r)) phases{p} '_' ...
                        intervals{int} '_NAcc.png'])
                    
                    figure(2); clf;
                    imagesc(foi_phase, foi_amp, mean(MI2d_BLA{int},3)');
                    axis xy; 
                    set(gca, 'FontName', 'Arial', 'FontSize', 14);
                    xlabel('Phase Frequency (Hz)');
                    ylabel('Envelope Frequency (Hz)');
                    title({['Rat # ' num2str(ratnums(r)) ', ' ...
                        char(phases(p)) ', ' intervals{int}]; ...
                        ['BLA, Average of  ' ...
                        num2str(size(MI2d_BLA{int},3)) ' Trials']});
                    caxis([0 cmax_BLA(int)]); colormap(jet); colorbar;
                    set(gcf,'WindowStyle','docked');
                    saveas(gcf,[figurefolder 'KL_MI2d_'...
                        num2str(ratnums(r)) phases{p} '_' ...
                        intervals{int} '_BLA.png'])
                end
            end
        end
    end
end