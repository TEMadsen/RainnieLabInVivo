%% info needed for analyzing stim data, including all rats filenames
%#ok<*SNASGU> just provides background info for other analysis protocols

figurefolder = 'C:\Users\drlab\Pictures\TempFigures\';
datafolder = 'C:\Users\drlab\Documents\TempAnalysisData\';

% each row # here corresponds to cell row (1st dim) in other variables,
% cell column (2nd dim) will be order of recordings (1st, 2nd, 3rd, etc)
ratnums = [251;252;267;278];  

% rat-specific notes
notes{1,1} = 'linear LLNL stim, TDT record';
notes{2,1} = 'Hamani stim, linear LLNL record';
notes{3,1} = 'linear LLNL stim and record';
notes{4,1} = 'custom 2D LLNL stim and record';

% Rat #278
% 1st baseline recording
nexfile{4,1} = 'C:\Users\drlab\Documents\TempAnalysisData\278_130321_0026.plx';
% 1st stim expt, random order, all monopolar, ramp current
nexfile{4,2} = 'C:\Users\drlab\Documents\TempAnalysisData\278_130322_0000-1_mrg_stim.nex';
% 2 Hz stim using bottom as active & varying ground, 
% or using top as ground & varying active
nexfile{4,3} = 'C:\Users\drlab\Documents\TempAnalysisData\278_130329_0003-4_mrg.plx';
% stim @ 4v5(g), i = 0.2, freq range 2-130 Hz
nexfile{4,4} = 'C:\Users\drlab\Documents\TempAnalysisData\278_130405_0005.plx';
% stim @ 3v5(g), i = 0.2, freq range 2-130 Hz
nexfile{4,5} = 'C:\Users\drlab\Documents\TempAnalysisData\278_130405_0006_sorted.nex';
% high freq at low current, 3v5(g)
nexfile{4,6} = 'C:\Users\drlab\Documents\TempAnalysisData\278_041213_0007.nex';
% 4v5(g), starting @ 130 Hz going down to 2 Hz, i = 0.2
nexfile{4,7} = 'C:\Users\drlab\Documents\TempAnalysisData\278_130419_0012_LFPs_stim.nex';
% retesting 4v5(g), starting @ 130 Hz going down to 2 Hz, i = 0.2
nexfile{4,8} = 'C:\Users\drlab\Documents\TempAnalysisData\278_130419_0018.plx';
% retesting 4v5(g), starting @ 130 Hz going down to 2 Hz, i = 0.2
nexfile{4,9} = 'C:\Users\drlab\Documents\TempAnalysisData\278_130503_0019.plx';
% 4v5(g), alternating btwn 2 & 130 Hz @ 0.3 mA
nexfile{4,9} = 'C:\Users\drlab\Documents\TempAnalysisData\278_130503_0020.plx';
% 4v5(g), alternating btwn 2 & 130 Hz @ 0.3 mA
nexfile{4,9} = 'C:\Users\drlab\Documents\TempAnalysisData\278_130503_0021stimLFPs.nex';
