function [data] = AutoArtReject_TEM(cfg,data)
% AutoArtReject_TEM performs automated artifact rejection, processing each
%   channel independently, removing clipping & large zvalue artifacts based
%   on automated thresholds (best to run on all data from a given subject
%   in one data structure, so the same threshold is applied consistently
%   across conditions), and returning the data structure re-merged across
%   channels, with NaNs in place of artifacts.
%
% Input cfg structure should contain:
%   interactsubj   = true or false, whether to select visual artifacts per
%     subject (i.e., per call to this function) & review all channels after
%     automated detection
%   interactch     = true or false, whether to preview detected artifacts
%     & select visual artifacts per channel
%   artfctdef.clip = struct as defined in ft_artifact_clip, but absdiff is
%     applied before the data is passed to that function, so it's actually
%     comparing these thresholds to the 2nd derivative of the original data
%   artfctdef.zvalue        = struct as defined in ft_artifact_zvalue
%   artfctdef.minaccepttim  = scalar as defined in ft_rejectartifact
%
% Data should be ft_datatype_raw
%
% To facilitate data-handling and distributed computing you can use
%   cfg.inputfile   =  ...
%   cfg.outputfile  =  ...
% If you specify one of these (or both) the input data will be read from a
% *.mat file on disk and/or the output data will be written to a *.mat
% file. These *.mat files should contain only a single variable 'data',
% corresponding with ft_datatype_raw.
%
% written 3/2/17 by Teresa E. Madsen

%% load data, if needed

if nargin < 2 && isfield(cfg,'inputfile')
  load(cfg.inputfile);
end

%% verify that the input data is in the correct structure

data = ft_datatype_raw(data,'hassampleinfo','yes');

if abs(data.fsample - round(data.fsample)) < 10^-3
  data.fsample = round(data.fsample);
end

%% preview data & mark unusual cross-channel artifacts

if cfg.interactsubj
  cfgtmp  = [];
  
  cfgtmp = ft_databrowser(cfgtmp,data);
  visual = cfgtmp.artfctdef.visual.artifact;  % not a field of artifact
  % because this will be reused for all channels, while the rest of
  % artifact is cleared when starting each new channel
else
  visual = [];
end

%% perform artifact detection on each channel separately

excludech = false(size(data.label));
datch = cell(size(data.label));

for ch = 1:numel(data.label)
  artifact = [];
  
  %% divide data into channels
  
  cfgtmp           = [];
  cfgtmp.channel   = data.label{ch};
  
  datch{ch} = ft_selectdata(cfgtmp,data);
  
  %% identify large zvalue artifacts
  
  cfgtmp                    = [];
  cfgtmp.artfctdef.zvalue   = cfg.artfctdef.zvalue;
  if ~isfield(cfgtmp.artfctdef.zvalue,'interactive')
    if cfg.interactch
      cfgtmp.artfctdef.zvalue.interactive = 'yes';
    else
      cfgtmp.artfctdef.zvalue.interactive = 'no';
    end
  end
  
  [~, artifact.zvalue] = ft_artifact_zvalue(cfgtmp,datch{ch});
  
  %% take 1st derivative of signal
  
  cfgtmp          = [];
  cfgtmp.absdiff  = 'yes';
  
  datd1 = ft_preprocessing(cfgtmp,datch{ch});
  
  %% define clipping artifacts
  % applies absdiff again, so it's actually working on 2nd derivative data
  
  cfgtmp                  = [];
  cfgtmp.artfctdef.clip   = cfg.artfctdef.clip;
  
  [~, artifact.clip] = ft_artifact_clip(cfgtmp,datd1);
  
  % bugs in ft_artifact_clip add time before & after end of file, breaks
  % ft_rejectartifact or triggers bug in convert_event that breaks
  % ft_databrowser.  non-integer #s just give annoying warnings.
  artifact.clip(artifact.clip < 1) = 1;
  endsamp = data.sampleinfo(end,2);
  if artifact.clip(end,2) > endsamp
    artifact.clip(end,2) = endsamp;
  end
  artifact.clip = round(artifact.clip);
  
  %% review artifacts if needed
  
  cfgtmp                               = [];
  cfgtmp.artfctdef.clip.artifact       = artifact.clip;
  cfgtmp.artfctdef.zvalue.artifact     = artifact.zvalue;
  cfgtmp.artfctdef.visual.artifact     = visual;
  
  if cfg.interactch
    cfgtmp = ft_databrowser(cfgtmp,datch{ch});
    artifact.visual = cfgtmp.artfctdef.visual.artifact;   % channel specific
    keyboard  % dbcont when satisfied
    % excludech(ch) = true;   % exclude this channel if desired
  end
  
  clearvars d1dat
  
  %% parse trials to remove NaNs - must do this first for minaccepttim to work, 
  % but don't replace the data in memory
    
    cfgtmp = [];   % keeps down size of historical metadata stored in data files
    cfgtmp.artfctdef.visual.artifact = artifact.visual;
    cfgtmp.artfctdef.clip.artifact   = artifact.clip;
    cfgtmp.artfctdef.zvalue.artifact = artifact.zvalue;
    cfgtmp.artfctdef.reject          = 'partial';
    cfgtmp.artfctdef.minaccepttim    = cfg.artfctdef.minaccepttim;
    
    datasubtrls = ft_rejectartifact(cfgtmp,datch{ch});
    
    %% retrieve artifact definitions from the metadata
    
    if datasubtrls.sampleinfo(1,1) > 1
      artifact.all = [1 datasubtrls.sampleinfo(1,1)-1];
    else
      artifact.all = zeros(0,2);
    end
    for tr = 2:numel(datasubtrls.trial)
      if datasubtrls.sampleinfo(tr,1) > datasubtrls.sampleinfo(tr-1,2)+1
        artifact.all(end+1,:) = [datasubtrls.sampleinfo(tr-1,2)+1 ...
          datasubtrls.sampleinfo(tr,1)];
      end
    end
    if data.sampleinfo(end,2) > datasubtrls.sampleinfo(end,2)
      artifact.all(end+1,:) = [datasubtrls.sampleinfo(end,2)+1 ...
        datch{ch}.sampleinfo(end,2)];
    end
    
    %% replace artifactual data with NaNs (for cross channel analyses)
    
    cfgtmp = [];
    cfgtmp.artfctdef.all.artifact  = artifact.all;
    cfgtmp.artfctdef.reject        = 'nan';
    
    datch{ch} = ft_rejectartifact(cfgtmp,datch{ch});
    
    if numel(datch{ch}.trial) ~= numel(data.trial)  % if any trials were rejected completely
      excludech(r, ch) = true;  % exclude this channel, or it won't merge properly
    end
    
end   % for ch = 1:numel(data.label)

%% remerge each channel file into one cleaned data file

cfgtmp  = [];

if isfield(cfg,'outputfile')
  cfgtmp.outputfile   = cfg.outputfile;
end

data = ft_appenddata(cfgtmp,datch{~excludech});

%% visualize result

if cfg.interactsubj
  cfgtmp  = [];
  
  cfgtmp = ft_databrowser(cfgtmp,data); %#ok<NASGU> just for debugging
  keyboard % dbcont when satisfied
end
end
