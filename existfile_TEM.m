function [existTF] = existfile_TEM(filename)
% EXISTFILE_TEM verifies that the directory exists (in case of server
% disconnection event) before deciding whether the file exists.  Throws an error
% if the directory does not exist.
%   Use in place of exist(filename,'file').
%
% Written 4/20/17 by Teresa E. Madsen, Ph.D.

if isempty(filename)  % in case script didn't even include a filename
  existTF = false;
  return
end

k = strfind(filename, filesep);
dir = filename(1:k(end));  % truncate to last filesep
connected = wait4server_TEM(dir);

if ~connected
  error('Directory does not exist or is not connected')
end

existTF = logical(exist(filename,'file'));
end