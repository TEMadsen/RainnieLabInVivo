function [zscore,pval,h,MI,phasepref] = sig_KL_MI_TEM(Amp,Phase,fig)
%%  Compute the significance of the KL-MI
% using the shuffling procedure from Hentschke et al, 2007, 
% and parallel processing
%
%   INPUTS:
%    Amp    = time series representing amplitude of high frequency range
%                   already filtered and hilbert transformed
%    Phase  = time series representing phase of low frequency range
%                   already filtered and hilbert transformed
%    fig    = 'y'/'n' to creating 3 figures to display the result
%
%   OUTPUTS:
%    zscore     = the z-score of the MI of the raw data versus surrogates
%    pval       = the corresponding p-value.
%    h          = the result of the z-test: 1 if the null hypothesis is
%                   rejected, 0 if it is not
%    MI         = modulation index from Tort et al., J Neurophysiol, 2010
%    phasepref  = the "preferred" phase (center of bin with highest
%                       gamma amplitude) expressed in degrees relative to
%                       delta/theta peak
%
%   DEPENDENCIES:
%    shuffle_envelope_signal_TEM.m
%    KL_MI_TEM.m
%
%   by Teresa Madsen, 2015
%   modified from correlate_envelope_signal.m by MAK.  Nov 12, 2007.

if nargin < 3 || isempty(fig);    fig = 'n';      end
[MI,phasepref,MeanAmp,center]=KL_MI_TEM(Amp,Phase); % Compute MI of raw data

nsurrogate = 100;             % Use 100 surrogates.
MI_surrogate = zeros(nsurrogate,1);
PP_surrogate = NaN(nsurrogate,1);
MA_surrogate = zeros(nsurrogate,18);

gcp;  % open a parallel pool if necessary
parfor k=1:nsurrogate                       % For each surrogate,
  sf = shuffle_envelope_signal_TEM(Amp);    % shuffle the envelope, and
  % compute the MI and preferred phase
  [MI_surrogate(k),PP_surrogate(k),MA_surrogate(k,:)] = ...
    KL_MI_TEM(sf,Phase);
end

if fig == 'y'
  figure; rose(PP_surrogate); hold all; rose(phasepref*ones(size(PP_surrogate)));
  figure; hist(MI_surrogate); line([MI MI],get(gca,'ylim')); xlim([0 0.04]);
  figure; plot([center center+360],[MA_surrogate MA_surrogate],...
    'color',[0.3 0.3 0.3]); hold on;
  plot([center center+360],[MeanAmp MeanAmp],'LineWidth',2);
end

[mean_surrogate, std_surrogate] = normfit(MI_surrogate);   % Fit surr. data

% Compute z-score.  Right sided because we only care if the actual data is
% significantly MORE modulated than the surrogate data.
[h,pval,~,zscore] = ztest(MI,mean_surrogate,std_surrogate,'tail','right');

if h == 1 && zscore < 0
  error('Significant result for negative z-score');
end

if isnan(h)
  error('h is NaN');
end

end
