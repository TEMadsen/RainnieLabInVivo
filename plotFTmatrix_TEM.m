function [cfg] = plotFTmatrix_TEM(cfg, freq)
% PLOTFTMATRIX_TEM is a simplified custom implementation of ft_singleplotTFR or
%   ft_multiplotTFR, so inputs should be similar, but the graphs will have
%   properly labeled axes.
%
% Additional optional cfg fields include:
%   fticks  = frequency values where tick marks should be placed (>= 3 elements)
%   tticks  = time values where tick marks should be placed (>= 3 elements)
%   cticks  = color axis values where tick marks should be placed (>= 3 elements)
%     or any of the above can be replaced by a 1x2 vector of integers
%     representing the min & max # of tick marks to be generated automatically
%     (defaults depend on # of subplots)
%   dBtransform   = boolean flag for whether to transform power into dB
%                   (default false)
%
% written 5/3/17 by Teresa E. Madsen, Ph.D.

%% check inputs / assign defaults

if nargin == 1 || isempty(freq)
  if isstruct(cfg) 
    if isfield(cfg,'inputfile')
      freq = rmvlargefields_TEM(cfg.inputfile);
    else  % assume freq struct was accidentally given as cfg
      freq = cfg;
      cfg = [];
    end
  elseif ~isempty(cfg) && isnumeric(cfg)  % assume freq matrix was given as cfg
    freq.powspctrm = cfg;
    cfg = [];
  else
    error('no freq data to plot')
  end
end

if isempty(cfg) || ~isfield(cfg,'channel') || isempty(cfg.channel)
  cfg.channel = 'all';
end

if ~isfield(cfg,'parameter') || isempty(cfg.parameter)
  cfg.parameter = fieldnames(freq);
  fbytes = zeros(size(cfg.parameter));
  for f = 1:numel(cfg.parameter)
    if isnumeric(freq.(cfg.parameter{f}))
      tmpfield = freq.(cfg.parameter{f});   %#ok<NASGU> passed as string
      S = whos('tmpfield');
      fbytes(f) = S.bytes;
    end
  end
  [~,bigI] = sort(fbytes,'descend');
  cfg.parameter = cfg.parameter{bigI(1)};
end

if ~isreal(freq.(cfg.parameter))
  freq.(cfg.parameter) = abs(freq.(cfg.parameter));
end

if isfield(cfg,'dBtransform') && ~isempty(cfg.dBtransform) && cfg.dBtransform
  freq.(cfg.parameter) = 10*log10(freq.(cfg.parameter));
end

if ismember(cfg.parameter,{'cohspctrm','crsspctrm'})
  chnames = 'labelcmb';
else
  chnames = 'label';
end

assert(isfield(freq, chnames), 'channel name field is not present in freq')

if ischar(cfg.channel) && strcmp(cfg.channel,'all')
  chsel = 1:size(freq.(chnames),1);
elseif ~isnumeric(cfg.channel)
  switch chnames
    case 'label'
      [~,chsel] = ismember(cfg.channel, freq.(chnames));
    case 'labelcmb'   % both members of ch pair must be members of cfg.channel
      chsel = find(all(ismember(freq.(chnames), cfg.channel),2));
  end
else  % assume they're indices
  chsel = cfg.channel;
end
if isempty(chsel) || all(chsel == 0)
  error('no channels selected')
end

%% verify dimord & make data selections

dimord = strsplit(freq.dimord,'_');
chdim = find(startsWith(dimord,'chan'));
fdim = find(strcmp(dimord,'freq'));
tdim = find(strcmp(dimord,'time'));
if numel([chdim fdim tdim]) ~= 3 || any([chdim fdim tdim] ~= 1:3)
  error('not fully implemented yet')
end

if isfield(cfg,'xlim')
  tsel = freq.time >= cfg.xlim(1) & freq.time <= cfg.xlim(2);
else
  tsel = true(size(freq.time));
end

if isfield(cfg,'ylim')
  fsel = freq.freq >= cfg.ylim(1) & freq.freq <= cfg.ylim(2);
else
  fsel = true(size(freq.freq));
end

freq.(cfg.parameter) = freq.(cfg.parameter)(chsel,fsel,tsel);
freq.(chnames) = freq.(chnames)(chsel,:);

%% check whether mask is needed (e.g., outline areas of significance)

if isfield(cfg,'maskparameter')
  mask = true;
  freq.(cfg.maskparameter) = freq.(cfg.maskparameter)(chsel,fsel,tsel);
  if ~isfield(cfg,'maskstyle')
    cfg.maskstyle = 'outline';  
  elseif ~strcmp(cfg.maskstyle,'outline')
    error('not fully implemented yet')
  end
else
  mask = false;
end

%% set plot parameters

numplot = size(freq.(chnames),1);
nrows = floor(sqrt(numplot));
ncols = ceil(numplot/nrows);

%% decide on ideal # of ticks per axis

if nrows > 6
  warning('This many subplots will be illegible - breaking into multiple figures instead.')
  nfigs = ceil(numplot/36);     % # of figures
  nppf = ceil(numplot/nfigs);   % # of subplots per figure
  nrows = floor(sqrt(nppf));  % base tick choices on number of subplots per fig
  fig = gcf;
  fign = fig.Number*100;
else
  nfigs = 1;
end

if ~isfield(cfg,'fticks') || isempty(cfg.fticks)
  switch nrows
    case 1;     cfg.fticks = [8 15];
    case 2;     cfg.fticks = [7 13];
    case 3;     cfg.fticks = [6 11];
    case 4;     cfg.fticks = [5 9];
    case 5;     cfg.fticks = [4 7];
    otherwise;  cfg.fticks = [3 5]; 
      warning('This many subplots may be too small to read.')
  end
end

if ~isfield(cfg,'tticks') || isempty(cfg.tticks)
  switch ncols
    case 1;     cfg.tticks = [8 15];
    case 2;     cfg.tticks = [7 13];
    case 3;     cfg.tticks = [6 11];
    case 4;     cfg.tticks = [5 9];
    case 5;     cfg.tticks = [4 7];
    otherwise;  cfg.tticks = [3 5]; 
      warning('This many subplots may be too small to read.')
  end
end

if ~isfield(cfg,'cticks') || isempty(cfg.cticks)
  switch nrows
    case 1;     cfg.cticks = [6 11];
    case 2;     cfg.cticks = [5 9];
    case 3;     cfg.cticks = [4 7];
    otherwise;  cfg.cticks = [3 5]; 
  end
end

%% choose ticks for freqency axis

if numel(cfg.fticks) < 3
  nticks = cfg.fticks;
  linspc = diff(freq.freq);
  logspc = freq.freq(2:end)./freq.freq(1:end-1);
  if ~isfield(cfg,'tol') || isempty(cfg.tol)
    cfg.tol = 0.01;
  end
  if ~any(abs(linspc-mean(linspc)) > cfg.tol) % linear spacing
    faxis = freq.freq;
    cfg.fticks = [];  % let Matlab decide
  else
    faxis = 1:numel(freq.freq);
    if ~any(abs(logspc-mean(logspc)) > cfg.tol) % log spacing
      P = unique(nextpow2(freq.freq));
      while numel(P) < nticks(1)
        P = unique([P-diff(P(1:2))/2; P]);
      end
      while numel(P) > nticks(2)
        P = P(1:2:end);
      end
      cfg.fticks = unique(round(2.^P,3,'significant'));
    else  % linearly space nticks indices throughout freq.freq
      ftickI = round(linspace(nearest(freq.freq,1),numel(freq.freq), ...
        round(mean(nticks))));
      cfg.fticks = unique(round(freq.freq(ftickI),3,'significant'));
    end
  end
else  % if ticks have been predefined
  faxis = 1:numel(freq.freq);
end

ftickI = zeros(size(cfg.fticks));
for f = 1:numel(cfg.fticks)
  ftickI(f) = nearest(freq.freq,cfg.fticks(f));
end
cfg.fticks = unique(round(freq.freq(ftickI),3,'significant'));
ftickI = unique(ftickI);

%% choose ticks for time axis

if numel(cfg.tticks) < 3
  nticks = cfg.tticks;
  if freq.time(1) < 0 && freq.time(end) > 0   % 0 should be a tick
    if ~isfield(cfg,'tlabel')
      cfg.tlabel = 'Time from Tone Onset (s)';
    end
    t = gcd(round(freq.time(end)), round(-freq.time(1)));  % might be stimulus duration
    if range(freq.time)/t < (nticks(2) - 1)  % should also be a tick
      if range(freq.time)/t < (nticks(1) - 1)
        td = floor(t/(range(freq.time)/(nticks(2) - 1)));
        while rem(t,td) ~= 0 && td > 1
          td = td - 1;
        end
        t = t/td;
      end
      cfg.tticks = (ceil(freq.time(1)/t):floor(freq.time(end)/t))*t;
    else
      cfg.tticks = unique(round(linspace(0,round(freq.time(end),3,'significant'), ...
        round(mean(nticks)*freq.time(end)/range(freq.time),3,'significant')), ...
        3,'significant'));
      cfg.tticks = unique(round(...
        [0:-mean(diff(cfg.tticks)):round(freq.time(1),3,'significant') ...
        cfg.tticks],3,'significant'));
    end
  else
    trange = round(range(freq.time),3,'significant');
    tfac = unique([1 factor(trange) trange].*[1; factor(trange)'; trange]);
    nt = tfac(tfac >= nticks(1)-1 & tfac < nticks(2));  % need to add one for linspace
    nt = nt(rem(trange,nt) == 0);  % #s within ntick range that divide evenly into trange
    if isempty(nt)  % in case nothing meets the criteria for nt
      warning(['unable to select ticks for time axis - ' ME.message])
      cfg.tticks = [];  % let Matlab decide
    else
      nt = nt(round(median(1:numel(nt))));  % choose the middle #, if multiple
      cfg.tticks = unique(round(linspace(round(freq.time(1),3,'significant'), ...
        round(freq.time(end),3,'significant'), nt+1),3,'significant'));
    end
  end
end

%% choose ticks for color axis (power, coherence, etc.)

if numel(cfg.cticks) < 3
  nticks = cfg.cticks;
  if ~isfield(cfg,'zlim') || isempty(cfg.zlim)
    cfg.zlim = [min(min(freq.(cfg.parameter),[],3),[],2) ...
      max(max(freq.(cfg.parameter),[],3),[],2)];  % range for each channel
  
    if any(cfg.zlim(:,2) < abs(cfg.zlim(:,1)))  % if any decreases are more extreme than increases,
      cfg.zlim(:,1) = -cfg.zlim(:,2);   % coloraxis should be symmetrical around 0
    end
  
    scale = 2^nextpow2(max(diff(cfg.zlim,1,2))/round(mean(nticks)));
    srcr = [floor(cfg.zlim(:,1)/scale) ceil(cfg.zlim(:,2)/scale)];   % scaled and rounded color range
    cfg.cticks = (min(srcr(:,1)):max(srcr(:,2)))*scale;
  else
    cfg.cticks = [];  % let Matlab decide
    if ischar(cfg.zlim)
      switch cfg.zlim
        case {'maxmin','minmax'}
          cfg.zlim = [min(freq.(cfg.parameter)(:)) max(freq.(cfg.parameter)(:))];
        case 'maxabs'
          cfg.zlim = [-max(abs(freq.(cfg.parameter)(:))) max(freq.(cfg.parameter)(:))];
        case 'zeromax'
          cfg.zlim = [0 max(freq.(cfg.parameter)(:))];
        case 'minzero'
          cfg.zlim = [min(freq.(cfg.parameter)(:)) 0];
        case 'max-max'  % for when positive values are more interesting than negative skew
          cfg.zlim = [-max(freq.(cfg.parameter)(:)) max(freq.(cfg.parameter)(:))];
        otherwise   % percent to trim from each end of distribution
          pct = str2double(cfg.zlim(1:strfind(cfg.zlim,'%')-1));
          if isempty(pct)
            warning('zlim value not understood, reverting to Matlab''s default')
            cfg.zlim = [];
          else
            if contains(cfg.zlim,'norm')  % based on normal distribution
              [muhat,sigmahat] = normfit(freq.(cfg.parameter)(:));
              cfg.zlim = norminv([pct/100 1-pct/100],muhat,sigmahat);
            else  % based on actual data
              cfg.zlim = quantile(freq.(cfg.parameter)(:),[pct/100 1-pct/100]);
            end
          end
      end
    end
  end
end

%% if too many subplots, split them after selecting ticks so they'll be uniform

if nfigs > 1
  chsel = 1:numplot;
  part = cell(nfigs,1);   % cfg for each partial figure
  for f = 1:nfigs
    part{f} = cfg;
    if numel(chsel) > nppf
      part{f}.channel = freq.(chnames)(chsel(1:nppf),:);
      chsel = chsel(nppf+1:end);
      fig = figure(fign+f);
      fig.Name = [strjoin({part{f}.channel{1,:}},' vs. ') ...
        ' through ' strjoin({part{f}.channel{end,:}},' vs. ')];
      part{f} = plotFTmatrix_TEM(part{f}, freq);
    else
      part{f}.channel = chsel;
      fig = figure(fign+f);
      fig.Name = [strjoin(freq.(chnames)(chsel(1),:),' vs. ') ...
        ' through ' strjoin(freq.(chnames)(chsel(end),:),' vs. ')];
      part{f} = plotFTmatrix_TEM(part{f}, freq);
      cfg.sepfigs = part;   % output of this main function will include outputs of all recursive calls
      return
    end
  end
end

%% plot

clf;

for ch = 1:numplot
  subplot(nrows,ncols,ch);
  imagesc(freq.time, faxis, squeeze(freq.(cfg.parameter)(ch,:,:)));
  axis xy; colormap(jet); 
  if isempty(cfg.cticks)
    colorbar; caxis(cfg.zlim);
  else
    colorbar('Ticks',cfg.cticks); caxis([cfg.cticks(1) cfg.cticks(end)]);
  end
  if ~isempty(ftickI)
    yticks(ftickI)
    yticklabels(cellstr(num2str(cfg.fticks')));
  end
  if ~isempty(cfg.tticks)
    xticks(cfg.tticks); 
  end
  set(gca,'TickDir','out')
  title(strjoin({freq.(chnames){ch,:}},' vs. '))
  if isfield(cfg,'tlabel')
    xlabel(cfg.tlabel)
  else
    xlabel('Time (s)')
  end
  ylabel('Frequency (Hz)')
  switch nrows
    case 1;     set(gca,'FontSize',10)
    case 2;     set(gca,'FontSize',9)
    case 3;     set(gca,'FontSize',8)
    case 4;     set(gca,'FontSize',7)
    case 5;     set(gca,'FontSize',6)
    otherwise;  set(gca,'FontSize',5);  
  end
  if mask
    hold all; 
    contour(freq.time, faxis, squeeze(freq.(cfg.maskparameter)(ch,:,:)), 1, 'w');
  end
end
end
