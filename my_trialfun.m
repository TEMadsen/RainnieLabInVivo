function [trl,event] = my_trialfun(cfg)

% MY_TRIALFUN determines trials/segments in the data that are
% interesting for analysis, using manually entered timestamps, 
% read_nex_events_TEM or plx_event_ts.  Also checks for and rejects 
% overlapping trials, and those that extend beyond the beginning 
% or end of the file.  Finally, if not using strobes with numeric values,
% adds original trial numbers as 4th column of "trl" (saved in output of 
% ft_preprocessing as data.trialinfo).
%
% The trialdef structure must contain the following specifications
%   cfg.trialdef.eventtype  = 'string' ***add support for cell array of strings
%   cfg.trialdef.prestim    = latency in seconds 
%   cfg.trialdef.poststim   = latency in seconds 
%
% Pre and poststim can be "Inf" to include all data before and/or after the
% first event (others will be excluded because their trials would overlap
% with the first).  The time of the first event will then be t=0.
%
% See also FT_DEFINETRIAL, FT_PREPROCESSING

% Copyright (C) 2005-2012, Robert Oostenveld
%   2015 modified from ft_trialfun_general by Teresa Madsen
%
% This file is part of FieldTrip, see http://www.ru.nl/neuroimaging/fieldtrip
% for the documentation and details.
%
%    FieldTrip is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    FieldTrip is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with FieldTrip. If not, see <http://www.gnu.org/licenses/>.
%
% $Id: ft_trialfun_general.m 7123 2012-12-06 21:21:38Z roboos $

% read the events
fprintf('reading the events from ''%s''\n', cfg.dataset);

if iscell(cfg.trialdef.eventtype)   % will add support for multiple events later
%   nextevent = cfg.trialdef.eventtype; 
  cfg.trialdef.eventtype = cfg.trialdef.eventtype{1};
end

if strcmpi(cfg.trialdef.eventtype, 'spike')
    spks = true;
    if ~isfield(cfg.trialdef,'eventvalue') || isempty(cfg.trialdef.eventvalue)
        error('When eventtype = spike, eventvalue must be the neuron name.')
    else
        name = cfg.trialdef.eventvalue;
    end
else
    spks = false;
    name = '';
end

if strcmpi(cfg.trialdef.eventtype, 'manual')
    if ~isfield(cfg.trialdef,'eventTS') || isempty(cfg.trialdef.eventTS)
        error('When eventtype = manual, eventTS must be timestamps in samples.')
    else
        event = struct('sample',{},'value',{},'timestamp',{}, ...
            'type',{},'duration',{},'offset',{});
        for sev = 1:numel(cfg.trialdef.eventTS) % each single event occurrence
            event(end+1).sample = cfg.trialdef.eventTS(sev); %#ok<AGROW>
            event(end).value = [];
            event(end).timestamp = cfg.trialdef.eventTS(sev)*40;
            event(end).type = 'manual';
            event(end).duration = 1;
            event(end).offset = 0;
        end
    end
else
    if strcmp(cfg.headerformat,'plexon_nex')
        [event] = read_nex_events_TEM(cfg.dataset,spks,name);
    elseif strcmp(cfg.headerformat,'plexon_plx')
        [~,names] = plx_event_names(cfg.dataset);
        event = struct('sample',{},'value',{},'timestamp',{}, ...
            'type',{},'duration',{},'offset',{});
        [nevs,evchans] = plx_event_chanmap(cfg.dataset);
        for evch = 1:nevs   % each event channel (type)
            [n,ts,sv] = plx_event_ts(cfg.dataset,evchans(evch));
            for sev = 1:n   % each single event occurrence
                event(end+1).sample = round(ts(sev)*1000); %#ok<AGROW>
                event(end).value = sv(sev);
                event(end).timestamp = round(ts(sev)*40000);
                event(end).type = names(evch,:);
                event(end).duration = 1;
                event(end).offset = 0;
            end
        end
    else
        error('Headerformat not supported.')
    end
end

hdr = ft_read_header(cfg.dataset);

trl = [];
val = [];
if strcmp(cfg.trialdef.eventtype, '?')
    % no trials should be added, show event information using subfunction and exit
    show_event(event);
    listspks = input('List spike channels? (y/n)  ','s');
    if listspks == 'y'
        spkvarnum = find(strcmpi(hdr.chantype,'spike')); % find spike channels
        for sp = 1:numel(spkvarnum)
            name = hdr.label(spkvarnum(sp));
            disp(name);
        end
    end
    return
end

if ~isfield(cfg.trialdef, 'eventvalue')
    cfg.trialdef.eventvalue = [];
elseif ischar(cfg.trialdef.eventvalue)
    % convert single string into cell-array, otherwise the intersection does not work as intended
    cfg.trialdef.eventvalue = {cfg.trialdef.eventvalue};
end

% select all events of the specified type and with the specified value
if ~isempty(cfg.trialdef.eventtype)
    sel = strncmpi({event.type}, cfg.trialdef.eventtype, length(cfg.trialdef.eventtype));
else
    sel = true(size(event));
end

if ~isempty(cfg.trialdef.eventvalue)
    % this cannot be done robustly in a single line of code
    if ~iscell(cfg.trialdef.eventvalue)
        valchar    = ischar(cfg.trialdef.eventvalue);
        valnumeric = isnumeric(cfg.trialdef.eventvalue);
    else
        valchar    = ischar(cfg.trialdef.eventvalue{1});
        valnumeric = isnumeric(cfg.trialdef.eventvalue{1});
    end
    for i=1:numel(event)
        if (ischar(event(i).value) && valchar) || (isnumeric(event(i).value) && valnumeric)
            sel(i) = sel(i) & ~isempty(intersect(event(i).value, cfg.trialdef.eventvalue));
        end
    end
end

% convert from boolean vector into a list of indices
sel = find(sel);  % selected events

for i=sel
    % catch empty fields in the event table and interpret them meaningfully
    if isempty(event(i).offset)
        % time axis has no offset relative to the event
        event(i).offset = 0;
    end
    if isempty(event(i).duration)
        % the event does not specify a duration
        event(i).duration = 0;
    end
    % determine where the trial starts with respect to the event
    if ~isfield(cfg.trialdef, 'prestim')
        trloff = event(i).offset;
        trlbeg = event(i).sample;
    elseif isinf(cfg.trialdef.prestim) 
      % keep all pre-event time from beginning of file
      trloff = 1-event(i).sample;
      trlbeg = 1; % first sample of file
    else
        % override the offset of the event
        trloff = round(-cfg.trialdef.prestim*1000);
        % also shift the begin sample with the specified amount
        trlbeg = event(i).sample + trloff;
    end
    % determine the number of samples that has to be read (excluding the begin sample)
    if ~isfield(cfg.trialdef, 'poststim')
        trldur = max(event(i).duration - 1, 0);
    elseif isinf(cfg.trialdef.poststim) 
      % keep all post-event time to end of file
      trldur = hdr.nSamples-trlbeg;
    else
        % this will not work if prestim was not defined, the code will then crash
        trldur = round((cfg.trialdef.poststim+cfg.trialdef.prestim)*1000) - 1;
    end
    trlend = trlbeg + trldur;
    % add the beginsample, endsample and offset of this trial to the list
    % if all samples are in the dataset
    if trlbeg < 1
        trloff = trloff - 1 + trlbeg;
        trlbeg = 1;
        warning('trial truncated by beginning of file. offset adjusted');
    end
    if trlend > hdr.nSamples
        trlend = hdr.nSamples;
        warning('trial truncated by end of file');
    end
    if trlbeg < trlend
        if isempty(trl) || trlbeg > trl(end,2)
            trl = [trl; [trlbeg trlend trloff]]; %#ok<AGROW>
            if isnumeric(event(i).value),
                val = [val; event(i).value]; %#ok<AGROW>
            elseif ischar(event(i).value) && (event(i).value(1)=='S'|| event(i).value(1)=='R')
                % on brainvision these are called 'S  1' for stimuli or 'R  1' for responses
                val = [val; str2double(event(i).value(2:end))]; %#ok<AGROW>
            else
                val = [val; nan]; %#ok<AGROW>
            end
        else
            warning('trial skipped because it would overlap with previous');
        end
    else
        warning('trial skipped due to duration = 0')
    end
end

trlnum = 1:size(trl,1);      % original trial numbers

% append the vector with values
if ~isempty(val) && ~all(isnan(val))
    trl = [trl val];
else
    trl = [trl trlnum'];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTION that shows event table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function show_event(event)
if isempty(event)
    fprintf('no events were found in the datafile\n');
    return
end
eventtype = unique({event.type});
Neventtype = length(eventtype);
if Neventtype==0
    fprintf('no events were found in the datafile\n');
else
    fprintf('the following events were found in the datafile\n');
    for i=1:Neventtype
        sel = find(strcmp(eventtype{i}, {event.type}));
        try
            eventvalue = unique({event(sel).value});            % cell-array with string value
            eventvalue = sprintf('''%s'' ', eventvalue{:});     % translate into a single string
        catch
            eventvalue = unique(cell2mat({event(sel).value}));  % array with numeric values or empty
            eventvalue = num2str(eventvalue);                   % translate into a single string
        end
        fprintf('event type: ''%s'' ', eventtype{i});
        fprintf('with event values: %s', eventvalue);
        fprintf('\n');
    end
end
