%% Execute this file every time Matlab starts up!
% renamed from startup.m to avoid running on parallel pool workers

disp('Setting defaults, please wait...')  % before the questdlg shows up

dbstop if error             % preferred debugging options:
warning('on')               % all warnings on by default
warning('off','verbose')    % don't offer instructions to disable warning
warning('on','backtrace')   % link to line where warning was generated

opengl software             % to avoid crashing when I start Matlab locally and 
                            % try to continue the same session on remote desktop

rng('shuffle')  % seeds the random number generator based on the current time,
% so it will always be different (default is to reset seed to 0 every time
% Matlab restarts, so same sequence of random #s would be generated)

cd([fileparts(userpath) filesep 'MATLAB'])  % so Matlab can find pathdef
path(pathdef)               % restore path saved at end of maintainGit

% global ft_default   % init global variable for use in all FT functions
% ft_default.debug = 'saveonerror';   % saves workspace upon errors in FT
% ^ this saves too much and takes too long

%% ask to check git status

button = questdlg('Update Git repositories now?', 'Check git?', ...
  'Yes','No', 'Yes');

switch button
  case 'Yes'
    maintainGit
  case 'No'
    warning('git status unknown')
    % included in maintainGit:
    cd([fileparts(userpath) filesep 'GitHub' filesep 'RainnieLabInVivo'])
    ft_defaults
end

%% ask to open previous figures

lastfiles = dir([fileparts(userpath) filesep 'MATLAB' filesep 'Last*']);
if isempty(lastfiles)
  return  % neither of the next 2 questions will be asked
end

figfile = find(ismember({lastfiles.name},'LastFigures.fig'));

if numel(figfile) == 1
  button = questdlg(['Open figures from ' lastfiles(figfile).date ' ?'], ...
    'Open figures?', 'Yes','No', 'No');
  
  if strcmp(button,'Yes')
    disp(['Opening all figures from ' lastfiles(figfile).date])
    openfig([lastfiles(figfile).folder filesep lastfiles(figfile).name])
  end
end

%% ask to load workspace variables

wkspcfile = find(ismember({lastfiles.name},'LastWorkspace.mat'));

if numel(wkspcfile) == 1
  button = questdlg(['Load workspace variables from ' ...
    lastfiles(wkspcfile).date ' ?'], 'Load workspace?', 'Yes','No', 'No');

  switch button
    case 'Yes'
      disp(['Loading all workspace variables from ' lastfiles(wkspcfile).date])
      load([lastfiles(wkspcfile).folder filesep lastfiles(wkspcfile).name])
    case 'No'
      clearvars   % gets rid of any leftover variables from this script
  end
end

%% this should be copied into a QuickAccess Shortcut (icon 1)

% % Execute this file every time Matlab starts
% % up to set debug defaults and offer to
% % refresh git and/or load previously saved
% % workspace variables & figures
% RUN_ME_FIRST