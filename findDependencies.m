%% find script/function dependencies

scriptname = 'S:\Teresa\Analyses\code\KL-MIz\KL_MIvTime_TEM.m';

[fList,pList] = matlab.codetools.requiredFilesAndProducts(scriptname);

dependencies = table(cell(numel(fList),1), cell(numel(fList),1), ...
  cell(numel(fList),1), cell(numel(fList),1), ...
  'VariableNames', {'Path','Repo','Folder','Filename'});

for f = 1:numel(fList)
  fparts = split(fList{f},filesep);
  if numel(fparts) < 7
    dependencies.Path{f} = join(fparts(1:end-1),filesep);
  else
    dependencies.Path{f} = join(fparts(1:5),filesep);
    dependencies.Repo{f} = fparts(6);
    if numel(fparts) > 7
      dependencies.Folder{f} = join(fparts(7:end-1),filesep);
    end
  end
  dependencies.Filename{f} = fparts(end);
end

