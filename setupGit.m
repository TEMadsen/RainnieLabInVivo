%% initial set-up for Git
% Remember not to remove the ! from each !git command because git won't
% work without it until the MATLAB-git wrapper is installed and added to
% the Matlab path.

% you can run this command to check whether git is already installed on
% your machine:
!git

%% (1) install git according to Matlab's instructions:
disp('<a href = "http://www.mathworks.com/help/matlab/matlab_prog/set-up-git-source-control.html">Set up git source control (Matlab)</a>')
disp('Restart Matlab after installing Git!')

%% (2) copy files from S:\Teresa\Software\Git\userdir\ 
%       into C:\Users\[YOUR USER NAME]\

if ispc
    tdir = 'S:\Teresa\';
elseif ismac
    tdir = '/Volumes/yerkes2/DRLab/Teresa/';
else
  error('operating system not supported')
end

serverdir = [tdir 'Software' filesep 'Git' filesep 'userdir' filesep];
    
userdir = fileparts(userpath);
userdir = userdir(1:(end-9));   % removes 'Documents'

[status,message] = copyfile([serverdir '.gitattributes'], userdir);
if status
  disp(['Successfully copied ''.gitattributes'' to ' userdir])
else
  error(message)
end

[status,message] = copyfile([serverdir '.gitignore'], userdir);
if status
  disp(['Successfully copied ''.gitignore'' to ' userdir])
else
  error(message)
end

%% (3) do the first time set-up according to GitHub's instructions:
disp('<a href = "https://help.github.com/articles/set-up-git/">Set up git (GitHub)</a>')

% for example, establish your identity
%% Teresa / Windows
!git config --global user.name "Teresa Madsen"
!git config --global user.email braingirl@gmail.com
!git config --global credential.helper wincred

%% Jeffrey / Mac
!git config --global user.name "Chia-Chun Hsu"
!git config --global user.email hurdleman@gmail.com
!git config --global credential.helper osxkeychain

%% (4) install MATLAB-git wrapper
% move into your git folder (creating it if it doesn't exist yet):
gitdir = [fileparts(userpath) filesep 'GitHub'];
if ~exist(gitdir,'dir')
  mkdir(gitdir)
end
cd(gitdir)

% Clone your remote repository here:
%% Teresa
disp('     running "git clone https://github.com/TEMadsen/MATLAB-git.git"')
!git clone https://github.com/TEMadsen/MATLAB-git.git

%% move into the newly created directory:
cd MATLAB-git

% connect with upstream repository. You add it with:
disp('     running "git remote add upstream https://github.com/manur/MATLAB-git.git"')
!git remote add upstream https://github.com/manur/MATLAB-git.git

% verify configuration
disp('     running "git remote -v"')
!git remote -v

%% (5) Set up FieldTrip
disp('<a href = "http://www.fieldtriptoolbox.org/development/git">FieldTrip development with git</a>')

% Fork and clone FieldTrip - Go with your web browser to
disp('<a href = "https://github.com/fieldtrip/fieldtrip">FieldTrip on GitHub</a>')
% and click on the right-hand side, towards the top, on fork. This will
% create a new remote repository in your github account at the address:
% https://github.com/USERNAME/fieldtrip.

% move into your git folder:
cd([fileparts(userpath) filesep 'GitHub'])

% Clone your remote repository here:
%% Teresa
disp('     running "git clone https://github.com/TEMadsen/fieldtrip.git"')
!git clone https://github.com/TEMadsen/fieldtrip.git

%% Jeffrey
disp('     running "git clone https://github.com/hurdleman/fieldtrip.git"')
!git clone https://github.com/hurdleman/fieldtrip.git

%% move into the newly created directory:
cd fieldtrip

% To synchronize with the official fieldtrip repository on github, you
% should also add it as a remote. Best practice is to call it the upstream
% repository. You add it with:
disp('     running "git remote add upstream https://github.com/fieldtrip/fieldtrip.git"')
!git remote add upstream https://github.com/fieldtrip/fieldtrip.git

% verify configuration
disp('     running "git remote -v"')
!git remote -v

%% (6) Clone our private repository to your local hard-drive

% move into your git folder:
cd([fileparts(userpath) filesep 'GitHub'])

% Clone our lab repository here:
disp('     running "git clone https://gitlab.com/TEMadsen/RainnieLabInVivo.git"')
!git clone https://gitlab.com/TEMadsen/RainnieLabInVivo.git

% move into the newly created directory:
cd RainnieLabInVivo

% verify configuration
disp('     running "git remote -v"')
!git remote -v

%% (7) Edit the Matlab search path

restoredefaultpath

% add RainnieLabInVivo with all subfolders (including .git), others without
addpath(genpath([fileparts(userpath) filesep 'GitHub' filesep ...
  'RainnieLabInVivo']), [fileparts(userpath) filesep 'GitHub' filesep ...
  'fieldtrip'], [fileparts(userpath) filesep 'GitHub' filesep ...
  'MATLAB-git'])

% then remove .git and its subfolders
rmpath(genpath([fileparts(userpath) filesep 'GitHub' filesep ...
  'RainnieLabInVivo' filesep '.git']))

savepath([fileparts(userpath) filesep 'MATLAB' filesep 'pathdef.m'])

disp('          DONE SETTING UP 3 REPOS!')
