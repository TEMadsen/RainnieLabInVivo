%% Script demonstrating use of single-channel artifacts
% Use any data you like, but I recommend merging all trials/conditions for
% a given subject into a single data structure, so the artifacts are
% detected uniformly.  The trials should have unique identifiers in
% data.trialinfo
%
% Dependencies:  FieldTrip, AutoArtReject_TEM, artifact_nan2zero_TEM
%
% written 3/3/17 by Teresa E. Madsen

load(YourInputData);
fsample = data.fsample;

%% plan configuration for TFA

mtmc = [];
mtmc.method     = 'mtmconvol';
mtmc.output     = 'powandcsd';
mtmc.foi        = 2.^(0:0.1:log2(300));   % 83 freqs, log2 spaced, 1-300 Hz
mtmc.tapsmofrq  = mtmc.foi/10;            % smooth by 10%
mtmc.t_ftimwin  = 2./mtmc.tapsmofrq;      % for 3 tapers (K=3), T=2/W
mtmc.toi = -30:min(mtmc.t_ftimwin)/2:60;  % time windows overlap by >= 50%
mtmc.pad = (2^nextpow2((range(mtmc.toi)+max(mtmc.t_ftimwin))*fsample))/fsample;

%% configure artifact rejection

cfg               = [];
cfg.interactsubj  = true;   % to select visual artifacts per subject & review all channels after automated detection
cfg.interactch    = false; 	% to preview z-thresholds & select visual artifacts per channel

cfg.artfctdef.minaccepttim = min(mtmc.t_ftimwin);

%% define large z-value artifacts

cfg.artfctdef.zvalue.channel        = 'all';
cfg.artfctdef.zvalue.cutoff         = 15;
cfg.artfctdef.zvalue.trlpadding     = 0;
cfg.artfctdef.zvalue.fltpadding     = 0;
cfg.artfctdef.zvalue.artpadding     = 0.1;
cfg.artfctdef.zvalue.rectify        = 'yes';

%% define clipping artifacts

cfg.artfctdef.clip.channel          = 'all';
cfg.artfctdef.clip.pretim           = 0.1;
cfg.artfctdef.clip.psttim           = 0.1;
cfg.artfctdef.clip.timethreshold    = 0.05;   % s
cfg.artfctdef.clip.amplthreshold    = 3;      % uV

%% replace artifacts with NaNs using custom function

[data] = AutoArtReject_TEM(cfg,data);

%% mark uniform NaNs & replace non-uniform NaNs w/ 0s, custom function

[artifact,data,times] = artifact_nan2zero_TEM(data);

%% reject artifacts by breaking into sub-trials

cfg = [];
cfg.artfctdef.nan2zero.artifact = artifact;
cfg.artfctdef.reject            = 'partial';

data = ft_rejectartifact(cfg,data);

%% identify real trials & loop through

trlinfo = unique(data.trialinfo,'rows','stable');

for tr = 1:size(trlinfo,1)
  %% calculate trial spectrogram
  
  cfg = mtmc;   % defined above
  cfg.trials = ismember(data.trialinfo, trlinfo(tr,:), 'rows');
  cfg.keeptrials = 'no';  % refers to sub-trials
  
  freq = ft_freqanalysis(cfg,data);
  
  %% where t_ftimwin overlaps with artifact, replace powspctrum values with NaNs
  
  for ch = 1:numel(freq.label)
    badt = [times{tr,ch}];
    if ~isempty(badt) && any(...
        badt > (min(freq.time) - max(freq.cfg.t_ftimwin)) & ...
        badt < (max(freq.time) + max(freq.cfg.t_ftimwin)))
      ci = find(any(strcmp(freq.label{ch}, freq.labelcmb)));
      for t = 1:numel(freq.time)
        for f = 1:numel(freq.freq)
          mint = freq.time(t) - freq.cfg.t_ftimwin(f);
          maxt = freq.time(t) + freq.cfg.t_ftimwin(f);
          if any(badt > mint & badt < maxt)
            freq.powspctrm(ch,f,t) = NaN;
            freq.crsspctrm(ci,f,t) = NaN;
          end
        end
      end
    end
  end
  
  %% save corrected output
  save(['trial' num2str(tr) 'mtmconvolTFA.mat'], 'freq', '-v7.3');
end