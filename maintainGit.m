function maintainGit
% MAINTAINGIT updates both git repos (FieldTrip & RainnieLabInVivo) ...
%   by pulling remote changes into current & master branches, merging master
%   into current branch, and pushing all local changes to the remote servers.
%   It also updates the Matlab path, in case the folder structure within the
%   repo has changed.  It is intended for daily use by all developers.
%
% written by Teresa E. Madsen

%% update FieldTrip

% change directory to local copy of FieldTrip
fprintf('\n          UPDATING FIELDTRIP\n')
cd([fileparts(userpath) filesep 'GitHub' filesep 'fieldtrip'])

% update local repo with all remote changes (fetches all, but only merges
% changes into current branch) - errors here may indicate that you have made
% local changes that need to be committed first
disp('     running "git pull --all -v"')
git pull --all -v

% update local master branch to the latest official version of FieldTrip
disp('     running "git pull upstream master:master -v"')
git pull upstream master:master -v

% update current WIP branch with any changes made to the master branch
disp('     running "git merge master -v"')
git merge master -v

% update all branches of personal forked copy on GitHub (aka remote repo)
disp('     running "git push origin --all -v"')
git push origin --all -v

% list all local & remote branches
disp('     running "git branch -a -v"')
git branch -a -v

% check status of current branch
disp('     running "git status"')
git status

%% update private repo
% On a Windows machine, "error: cannot stat 'filename': Permission denied"
% may indicate that you have made local changes to the folder structure
% that can't be updated because the folder is open in Windows Explorer or
% Matlab.  Try closing both, then run the needed commands (push/pull, etc.)
% in Git Bash before reopening Matlab and rerunning maintainGit.

% change directory back to general code folder
fprintf('\n          UPDATING RAINNIELABINVIVO\n')
cd([fileparts(userpath) filesep 'GitHub' filesep 'RainnieLabInVivo'])

% update local repo with all remote changes (fetches all, but only merges
% changes into current branch) - errors here may indicate that you have made
% local changes that need to be committed first
disp('     running "git pull --all -v"')
git pull --all -v

% update local master branch with changes made on another computer - If you're
% not the repo owner and accidentally made changes to the master branch, create
% a new branch from it, then revert the local master branch back to the remote
% version, using:
% git reset --hard origin/master
disp('     running "git pull origin master:master -v"')
git pull origin master:master -v

% update current WIP branch with any changes made to the master branch
disp('     running "git merge master -v"')
git merge master -v

% send changes on current branch to GitLab - don't push all because master
% branch is protected - repo owner should push to it manually, using:
% git push origin --all -v
disp('     running "git push origin -v"')
git push origin -v

% list all local & remote branches
disp('     running "git branch -a -v"')
git branch -a -v

% check status of current branch
disp('     running "git status"')
git status

%% if the GitLab repo ever starts giving 403 "Forbidden" errors...
% % like this -
% %             remote: Forbidden
% %             fatal: unable to access
% %               'https://gitlab.com/TEMadsen/RainnieLabInVivo.git/':
% %               The requested URL returned error: 403"
% % - it means GitLab has banned your IP address temporarily.  It is
% % triggered by something trying to contact the server without a proper
% % password or with some other bad configuration.  This time I made the
% % mistake of using the Matlab source control menu to push, so let's just
% % not do that anymore!  Use Matlab's git interface only for local
% % operations like commit, revert, manage branches, compare versions...
% % basically anything but push & fetch.
% %
% % To fix this error, wait an hour or so and make sure you can reach
% % gitlab.com in a web browser before trying something innocuous like:
% git remote show origin
% % If it locks you out again, you may have to take one or more of the
% % troubleshooting steps I tried yesterday:
% git remote rm origin
% git remote add origin https://gitlab.com/TEMadsen/RainnieLabInVivo.git
% % and/or delete all gitlab-related credentials in:
% %             Control Panel\User Accounts\Credential Manager
% % Then wait another hour+ and try again.  It should prompt you for your
% % password once and then remember it.

%% update the Matlab search path in case new subfolders have been added

restoredefaultpath

% add RainnieLabInVivo with all subfolders (including .git), others without
addpath(genpath([fileparts(userpath) filesep 'GitHub' filesep ...
  'RainnieLabInVivo']), [fileparts(userpath) filesep 'GitHub' filesep ...
  'fieldtrip'], [fileparts(userpath) filesep 'GitHub' filesep ...
  'MATLAB-git'])

% then remove .git and its subfolders
rmpath(genpath([fileparts(userpath) filesep 'GitHub' filesep ...
  'RainnieLabInVivo' filesep '.git']))

% and let FieldTrip set up its custom path
ft_defaults

savepath([fileparts(userpath) filesep 'MATLAB' filesep 'pathdef.m'])

disp('Matlab search path updated')
end   % function