function [zscore2d,pval2d,h2d,MI2d,phasepref2d] = sig_crossKL_MI2d_TEM(data1,data2,foi_phase,w_phase,foi_amp,w_amp,plt)
%   2d cross-region cross-frequency phase-amplitude modulation,
%       using Kullback�Leibler modulation index converted to z-score.
%           From Tort et al., J Neurophysiol, 2010
%   by Teresa E. Madsen 2015
%
%   Usage:
%       [zscore2d,pval2d,h2d,MI2d,phasepref2d] = sig_crossKL_MI2d_TEM(...
%           data1,data2,foi_phase,w_phase,foi_amp,w_amp,plt);
%
%   Inputs:
%       data1 & data2   = unfiltered time series, as row vectors (1xN),
%           each will be used as both the low frequency, phase-modulating
%           signal and the high frequency, amplitude-modulated signal
%       foi_phase   = central frequencies for phase modulation
%       w_phase     = width of phase band (foi +/- w)
%       foi_amp     = central frequencies for modulated amplitude
%       w_amp       = width of amplitude band (foi +/- w)
%       plt         = 'y' or 'n' to plot data
%
%   Outputs (each organized within cells representing source of
%           LF phase-modulating data in row index &
%           HF amplitude-modulated data in column index):
%       zscore2d        = the z-score of the MI of raw data vs surrogates
%       pval2d          = the corresponding p-value.
%       h2d             = the result of the z-test: 1 if the null
%                           hypothesis is rejected, 0 if it is not
%       MI2d            = modulation index from Tort et al., J Neurophysiol, 2010
%       phasepref2d     = the "preferred" phase (center of bin with highest
%                           gamma amplitude) expressed in degrees relative
%                           to delta/theta peak
%       and 4 plots of the 2d z-score transformed modulation index.
%
%   DEPENDENCIES:
%       eegfilt
%       sig_KL_MI_TEM

%% check inputs and set up other variables

if nargin < 7 || isempty(plt);  plt = 'n';  end

[M,~] = size(data1);
if M~=1
    error('Data must be a row vector (1xN)');
end
if size(data1) ~= size(data2)
    error('Data vectors must be the same size');
end

Fs = 1000;          % hardcoded to assume Plexon's LFP sampling frequency
pad = [0 Fs];       % add 1 second of 0s to each end of signal
pdata1 = padarray(data1,pad);
pdata2 = padarray(data2,pad);

epochframes = length(pdata1);

zscore2d        = cell(2);
pval2d          = cell(2);
h2d             = cell(2);
MI2d            = cell(2);
phasepref2d     = cell(2);

%% calculate MI 2d across regions

h = waitbar(0,'Calculating 2d MI z-score across regions...');
for i = 1:length(foi_phase)
    filtorder = 3*fix(1000/(foi_phase(i)-w_phase));
    if filtorder*3 >= epochframes
        filtorder = fix(epochframes/3)-1;
    end
    % Filter the 1st low freq signal.
    theta{1} = eegfilt(pdata1,Fs,foi_phase(i)-w_phase,...
        foi_phase(i)+w_phase,epochframes,filtorder,0,'fir1');
    Phase{1} = angle(hilbert(theta{1}));      % Extract the low freq phase.
    Phase{1} = Phase{1}(1+pad(2):end-pad(2)); % Remove padding.
    % Filter the 2nd low freq signal.
    theta{2} = eegfilt(pdata2,Fs,foi_phase(i)-w_phase,...
        foi_phase(i)+w_phase,epochframes,filtorder,0,'fir1');
    Phase{2} = angle(hilbert(theta{2}));      % Extract the low freq phase.
    Phase{2} = Phase{2}(1+pad(2):end-pad(2)); % Remove padding.
    waitbar(i/length(foi_phase),h);
    for j=1:length(foi_amp)
        filtorder = 3*fix(1000/(foi_amp(j)-w_amp));
        if filtorder*3 >= epochframes
            filtorder = fix(epochframes/3)-1;
        end% Filter the 1st high freq signal.
        gamma{1} = eegfilt(pdata1,Fs,foi_amp(j)-w_amp,foi_amp(j)+w_amp,...
            epochframes,filtorder,0,'fir1');
        Amp{1} = abs(hilbert(gamma{1})); % Extract the high freq amplitude.
        Amp{1} = Amp{1}(1+pad(2):end-pad(2)); % Remove padding.
        % Filter the 2nd high freq signal.
        gamma{2} = eegfilt(pdata2,Fs,foi_amp(j)-w_amp,foi_amp(j)+w_amp,...
            epochframes,filtorder,0,'fir1');
        Amp{2} = abs(hilbert(gamma{2})); % Extract the high freq amplitude.
        Amp{2} = Amp{2}(1+pad(2):end-pad(2)); % Remove padding.
        for LFPM = 1:2     % low frequency phase-modulating data source
            for HFAM = 1:2 % high frequency amplitude-modulated data source
                % Compute & test significance of the modulation index.
                [zscore2d{LFPM,HFAM}(i,j),pval2d{LFPM,HFAM}(i,j),...
                    h2d{LFPM,HFAM}(i,j),MI2d{LFPM,HFAM}(i,j),...
                    phasepref2d{LFPM,HFAM}(i,j)] = ...
                    sig_KL_MI_TEM(Amp{HFAM},Phase{LFPM});
                if any(isnan(h2d{LFPM,HFAM}))
                    error(['NaN(s) found in h2d{' num2str(LFPM) ',' num2str(HFAM) '}']);
                end
            end
        end
    end
end
close(h)

if plt == 'y'
    for LFPM = 1:2     % low frequency phase-modulating data source
        for HFAM = 1:2 % high frequency amplitude-modulated data source
            % Plot the two-dimensional modulation index.
            figure(LFPM+2*(HFAM-1));
            imagesc(foi_phase, foi_amp, zscore2d{LFPM,HFAM}');
            colormap(jet); colorbar; axis xy;
            set(gca, 'FontName', 'Arial', 'FontSize', 14);
            title('KL-MI 2d');
            xlabel('Phase Frequency (Hz)');
            ylabel('Envelope Frequency (Hz)');
        end
    end
end
end
