%% Quick test of ft_channelrepair
% to deal with artifacts restricted in both time and channels
% and/or to replace missing/dead channels

info_fear
r = 1;  tr = 8;   ch = 'AD13';  % a known, isolated, bad trial-channel pair

%% load the data

% Raw LFP data divided into long, non-overlapping tone-triggered
% trials, merged across all recordings for each rat
inputfile = [datafolder 'RawLFPTrialData_' ...
  int2str(ratT.ratnums(r)) 'Merged.mat'];

load(inputfile);

%% prepare neighbors
% TO DO LATER: add ft_prepare_neighbours to MWAlayout and then use the
% 'template' method, since it will be the same for all of this type of
% array, and it will avoid the channel selection problem mentioned below.

cfg = [];
cfg.elecfile        = [configfolder 'elec_' MWA '_' ratT.side{r} '.mat'];

cfg.channel         = 'all';  % must include all channels to find all 
% possible neighbors, but it's too fast to bother fixing

cfg.method          = 'distance';  % uses 3D sensor data,
% whereas 'triangulation' uses a spherical projection that distorts the
% distances, so just don't use it!

cfg.neighbourdist   = 1;  % in mm (same units as elec) 
% If using cfg.method = 'average'; for ft_channelrepair, choose value
% that selects all immediately adjacent electrodes, including diagonals.
% If using cfg.method = 'weighted'; for ft_channelrepair, choose value
% that selects all electrodes within the same array, and it will scale by
% distance.

cfg.feedback        = 'yes';

neighbours = ft_prepare_neighbours(cfg);
save([configfolder 'neighb_' MWA '_' ratT.side{r} '.mat'],'neighbours');
% Side doesn't matter, but I'll leave it in for consistency in the
% configuration naming convention for when I add it to MWAlayout.m

%% repair channel

cfg             = [];
cfg.method      = 'weighted';
cfg.badchannel  = 'AD13';
cfg.neighbours  = neighbours;
cfg.trials      = 8;

interp = ft_channelrepair(cfg, data);

%% compare data with interpolated data

assert(isequal(data.time{8},interp.time{1}))

for ch = 1:numel(data.label)
  if ~isequal(data.trial{8}(ch,:),interp.trial{1}(ch,:))
    figure(ch); clf; 
    plot(data.time{8},data.trial{8}(ch,:), ...
      data.time{8},data.trial{8}(ch-4,:)-1000, ...
      data.time{8},data.trial{8}(ch+1,:)+1000, ...
      interp.time{1},interp.trial{1}(ch,:));
    legend({'orig','4up','1down','interp'});
  end
end

% this one has near opposite phase!  need to be careful about this, only
% doing it with short chunks of time around really bad artifacts on
% otherwise good channels
