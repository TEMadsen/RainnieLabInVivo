%% attempt at simple frequency decomposition
% given a timeseries x(t) with N points sampled at Fs

Fs = 1000;
N = length(data);
times = (1/Fs):(1/Fs):(N/Fs);

if N == 2^nextpow2(N)       % N is a power of 2
  Nf = N;                   % all N are useable => # of frequencies
else
  Nf = 2^(nextpow2(N)-1);   % round down to next lower power of 2
end

for T = 2:Nf                % period of measurable oscillations
  idx = Nf-T+1;             % frequency index, so they increase in value
  F(idx) = Fs/T;            % corresponding frequency
  k = T/2;                  % opposite phase point
  S(idx) = 0;               % initiate variable for running sum (spectrum?)
  for n = 1:(N-T)           % for all data points where x(n+T) exists,
    % add the absolute value of (the difference between 2 points at the
    % same phase minus the diff. between 2 points at opposite phases)
    S(idx) = S(idx) + abs(diff(x(n),x(n+T))-diff(x(n),x(n+k)));
  end
end

Sp = S./sum(S);  % express as a proportion of total
figure; plot(F,S)
figure; plot(F,Sp)